library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity floppy_if is
	generic (
		num_tracks_g	: integer := 40
	);
	port (
		clock_i			: in  std_logic;								-- Clock >= 20 MHz
		clock_en_i		: in  std_logic;								-- Clock enable 20Mhz if Clock > 20MHz
		reset_i			: in  std_logic;
		-- drive select lines
		drv_ena_i		: in  std_logic_vector(4 downto 1);
		drv_sel_i		: in  std_logic_vector(4 downto 1);
		step_i			: in  std_logic;
		dirc_i			: in  std_logic;								-- '1' = in, '0' = out
		rclk_o			: out std_logic;								-- Read Clock
		raw_read_n_o	: out std_logic;
		write_gate_i	: in  std_logic;								-- Write Gate
		raw_write_n_i	: in  std_logic;								-- Write Data
		tr00_n_o			: out std_logic;
		ip_n_o			: out std_logic;								-- Index Pulse
		-- media interface
		track_num_o		: out std_logic_vector( 5 downto 0);
		offset_o			: out std_logic_vector(12 downto 0);
		data_i			: in  std_logic_vector( 7 downto 0);
		data_o			: out std_logic_vector( 7 downto 0);
		write_ready_o	: out std_logic;
		-- fifo control
		fifo_rd_o		: out std_logic;
		fifo_flush_o	: out std_logic
	);
end entity floppy_if;

architecture SYN of floppy_if is

	signal clk_1m_en_s	: std_logic := '0';

	type track_a is array (natural range <>) of std_logic_vector(5 downto 0);
	signal track_r			: track_a(4 downto 0);
	signal offset_s		: std_logic_vector(offset_o'range);
	signal ena_s			: std_logic := '0';
	signal drv_s			: integer range 0 to 4 := 0;

begin

	ena_s <= drv_ena_i(1) when drv_sel_i(1) = '1' else
				drv_ena_i(2) when drv_sel_i(2) = '1' else
				drv_ena_i(3) when drv_sel_i(3) = '1' else
				drv_ena_i(4) when drv_sel_i(4) = '1' else
				'0';

	drv_s <= 1 when drv_sel_i(1) = '1' else
				2 when drv_sel_i(2) = '1' else
				3 when drv_sel_i(3) = '1' else
				4 when drv_sel_i(4) = '1' else
				0;

	-- 1MHz clock (enable) generate
	process (clock_i, clock_en_i, reset_i)
		subtype count_t is integer range 0 to 19;
		variable count_v : count_t := 0;
	begin
		if reset_i = '1' then
			count_v := 0;
		elsif rising_edge(clock_i) and clock_en_i = '1' then
			clk_1m_en_s <= '0';
			if count_v = count_t'high then
				clk_1m_en_s <= '1';
				count_v := 0;
			else
				count_v := count_v + 1;
			end if;
		end if;
	end process;

	-- handle track register, stepping
	process (clock_i, clock_en_i, reset_i)
		variable step_r : std_logic := '0';
	begin
		if reset_i = '1' then
			step_r  := '0';
			track_r <= (others => (others => '0'));
		elsif rising_edge(clock_i) and clock_en_i = '1' then
			fifo_flush_o <= '0';
			if ena_s = '1' then
				if step_r = '0' and step_i = '1' then				-- leading edge of step
					if dirc_i = '0' then
						if track_r(drv_s) /= 0 then					-- step out (decrement track)
							track_r(drv_s) <= track_r(drv_s) - 1;
						end if;
					else
						if track_r(drv_s) < num_tracks_g-1 then	-- step in (increment track)
							track_r(drv_s) <= track_r(drv_s) + 1;
						end if;
					end if;
					fifo_flush_o <= '1';									-- flush FIFO
				end if;
			end if;
			step_r := step_i;
		end if;
	end process;

	-- track 0 indicator 
	-- - works even when not "enabled" (no floppy)
	tr00_n_o <= '0' when track_r(drv_s) = 0 else '1';

	BLK_READ : block
	begin
  
		-- we'll start with 4us per bit, 6272 bytes/track = 200ms per track
		PROC_RD: process (clock_i, clk_1m_en_s, reset_i)
			variable count_v		: std_logic_vector(17 downto 0) := (others => '0');
			alias phase				: std_logic_vector( 1 downto 0) is count_v( 1 downto 0);
			alias bbit				: std_logic_vector( 2 downto 0) is count_v( 4 downto 2);
			alias byte				: std_logic_vector(12 downto 0) is count_v(17 downto 5);
			variable read_data_r	: std_logic_vector( 7 downto 0) := (others => '0');
		begin
			if reset_i = '1' then
				count_v			:= (others => '0');
				rclk_o			<= '0';
				fifo_rd_o		<= '0';
				raw_read_n_o	<= '1';
				ip_n_o			<= '1';
			elsif rising_edge(clock_i) and clk_1m_en_s = '1' then
				fifo_rd_o		<= '0';							-- default
				raw_read_n_o	<= '1';							-- default
				if phase = "00" and bbit = "000" then		-- memory address
					offset_s <= byte;
				end if;
				if phase = "01" then								-- rclk
					rclk_o <= '1';
				elsif phase = "11" then
					rclk_o <= '0';
				end if;
				if phase = "01" and bbit = "000" then		-- data latch (1us memory assumed)
					read_data_r	:= data_i;
					fifo_rd_o	<= '1';
				end if;
				if phase = "10" then								-- handle reads (from the media)
					raw_read_n_o	<= ena_s and not read_data_r(read_data_r'left);
					read_data_r		:= read_data_r(read_data_r'left-1 downto 0) & '0';
					--raw_read_n_o	<= ena and not read_data_r(read_data_r'right);
					--read_data_r	:= '0' & read_data_r(read_data_r'left downto 1);
				end if;
				ip_n_o <= '1';										-- default
				if count_v < 1000 then								-- generate index pulse (min 10us)
					ip_n_o <= not ena_s;								-- no index pulse if no floppy
				end if;
				if count_v = 6272*8*4-1 then
					count_v := (others => '0');
				else
					count_v := count_v + 1;
				end if;
			end if;
		end process PROC_RD;

	end block BLK_READ;

	BLK_WRITE : block

		signal raw_data_rdy   : std_logic := '0';
		signal write_data_r   : std_logic_vector(7 downto 0) := (others => '0');
		-- fudge
		alias wclk            : std_logic is raw_write_n_i;

	begin

		-- reads raw data from drive continuously
		-- note that there is no bit/byte synchronisation atm
		-- so drive emulation must be 'in sync'
		PROC_RAW_WRITE: process (clock_i, clock_en_i, reset_i)
			variable wclk_r	: std_logic := '0';
			variable count		: std_logic_vector(2 downto 0) := (others => '0');
			variable data_v	: std_logic_vector(7 downto 0) := (others => '0');
		begin
			if reset_i = '1' then
				write_ready_o <= '0';
				wclk_r      := '0';
				count       := (others => '0');
				data_v      := (others => '0');
			elsif rising_edge(clock_i) and clock_en_i = '1' then
				raw_data_rdy <= '0';											-- default
				write_ready_o  <= '0';										-- default
				if wclk_r = '0' and wclk = '1' then						-- leading edge WCLK
					data_v := data_v(data_v'left-1 downto 0) & '0';
				elsif wclk_r = '1' and wclk = '0' then					-- trailing edge rclk
					if count = "111" then
						write_data_r <= data_v;								-- finished a byte
						raw_data_rdy <= '1';
						write_ready_o  <= write_gate_i;
					end if;
					count := count + 1;
				end if;
				if wclk = '1' then											-- sample RAW_DATA_n during WCLK high
					if raw_write_n_i = '0' then
						data_v(0) := '1';
					end if;
				end if;
				wclk_r := wclk;
			end if;
		end process PROC_RAW_WRITE;

		data_o	<= write_data_r;

	end block BLK_WRITE;

	-- assign outputs
	track_num_o <= track_r(drv_s);
	offset_o <= offset_s;

end architecture SYN;
