-------------------------------------------------------------------------------
--
-- SD/MMC interface (SPI-style) for the Apple ][ Emulator
--
-- Michel Stempin (michel.stempin@wanadoo.fr)
-- Working with MMC/SD/SDHC cards
-- Adapted by Fabio Belavenuto (2017)
--
-- From previous work by:
-- Stephen A. Edwards (sedwards@cs.columbia.edu)
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity spi_controller is
	generic (
		BLOCK_SIZE_g		: natural := 512;
		BLOCK_BITS_g		: natural := 9
	);
	port (
		-- System Interface
		clock_i				: in  std_logic;								-- System clock
		reset_i				: in  std_logic;
		-- Track buffer Interface
		track_num_i			: in  std_logic_vector( 5 downto 0);	-- Track number (0-63)
		image_num_i			: in  std_logic_vector( 9 downto 0);	-- Which disk image to read
		ram_write_addr_o	: out std_logic_vector(12 downto 0);
		ram_data_o			: out std_logic_vector( 7 downto 0);
		ram_we_o				: out std_logic;
		-- Card Interface
		sd_cs_n_o			: out std_logic;
		sd_mosi_o			: out std_logic;
		sd_miso_i			: in  std_logic;
		sd_sclk_o			: out std_logic;
		-- Debug
		D_error_o			: out std_logic
	);

end entity;

architecture rtl of spi_controller is

	-----------------------------------------------------------------------------
	-- States of the combined MMC/SD/SDHC reset, track and command FSM
	--
	type states_t is (
		-- Reset FSM
		POWER_UP,
		RAMP_UP,
		CHECK_CMD0,
		CHECK_CMD8,
		CHECK_CMD55,
		CHECK_ACMD41,
		CHECK_CMD1,
		CHECK_CMD58,
		CHECK_SET_BLOCKLEN,
		ERROR,
		-- Track read FSM
		IDLE,
		READ_TRACK,
		READ_BLOCK_WAIT,
		READ_BLOCK_DATA,
		READ_BLOCK_CRC,
		-- SD command embedded FSM
		WAIT_NRC,
		SEND_CMD,
		RECEIVE_BYTE_WAIT,
		RECEIVE_BYTE
	);
	--
	signal state_s, return_state_s : states_t;
	--
	-----------------------------------------------------------------------------

	signal slow_clk_s			: boolean := true;
	signal spi_clk_s			: std_logic;
	signal sclk_sig_s			: std_logic;

	signal current_track_s	: unsigned( 5 downto 0);
	signal current_image_s	: unsigned( 9 downto 0);
	signal write_addr_s		: unsigned(12 downto 0);

	signal command_s			: std_logic_vector( 5 downto 0);
	signal argument_s			: std_logic_vector(31 downto 0);
	signal crc7_s				: std_logic_vector( 6 downto 0);
	signal command_out_s		: std_logic_vector(55 downto 0);
	signal recv_bytes_s		: unsigned(39 downto 0);
	type versions_t is (MMC, SD1x, SD2x);
	signal version_s			: versions_t;
	signal high_capacity_s	: boolean;
	signal error_s				: boolean;

begin

	ram_write_addr_o <= std_logic_vector(write_addr_s);

	-----------------------------------------------------------------------------
	-- Process var_clkgen
	--
	-- Purpose:
	--   Implements the variable speed clock for MMC compatibility.
	--   If slow_clk_s is false, spi_clk_s == clock_i, thus SCLK = 7M
	--   If slow_clk_s is true, spi_clk_s = clock_i / 32 and SCLK = 223.214kHz, which
	--   is between 100kHz and 400kHz, as required for MMC compatibility.
	--
	var_clkgen : process (clock_i, slow_clk_s)
		variable var_clk_v : unsigned(4 downto 0) := (others => '0');
	begin
		if slow_clk_s then
			spi_clk_s <= var_clk_v(4);
			if rising_edge(clock_i) then
				var_clk_v := var_clk_v + 1;
			end if;
		else
			spi_clk_s <= clock_i;
		end if;
	end process;

	sd_sclk_o <= sclk_sig_s;
	--
	-----------------------------------------------------------------------------

	-----------------------------------------------------------------------------
	-- Process sd_fsm
	--
	-- Purpose:
	--   Implements the combined "SD Card init", "track read" and "command" FSMs.
	--
	sd_fsm : process(spi_clk_s)
		subtype cmd_t is std_logic_vector(5 downto 0);
		constant CMD0				: cmd_t := std_logic_vector(to_unsigned(0, 6));
		constant CMD1				: cmd_t := std_logic_vector(to_unsigned(1, 6));
		constant CMD8				: cmd_t := std_logic_vector(to_unsigned(8, 6));
		constant CMD16				: cmd_t := std_logic_vector(to_unsigned(16, 6));
		constant CMD17				: cmd_t := std_logic_vector(to_unsigned(17, 6));
		constant CMD55				: cmd_t := std_logic_vector(to_unsigned(55, 6));
		constant CMD58				: cmd_t := std_logic_vector(to_unsigned(58, 6));
		constant ACMD41			: cmd_t := std_logic_vector(to_unsigned(41, 6));
		variable counter_v		: unsigned(7 downto 0);
		variable byte_cnt_v		: unsigned(BLOCK_BITS_g - 1 downto 0);
		variable lba_v				: unsigned(31 downto 0);
		variable track_num_v		: unsigned( 5 downto 0);
		variable image_num_v		: unsigned( 9 downto 0);

	begin
		if rising_edge(spi_clk_s) then
			ram_we_o <= '0';
			if reset_i = '1' then
				error_s				<= false;
				state_s				<= POWER_UP;
				-- Deliberately out of range
				current_track_s	<= (others => '1');
				current_image_s	<= (others => '1');
				sclk_sig_s			<= '0';
				slow_clk_s			<= true;
				sd_cs_n_o			<= '1';
				command_s			<= (others => '0');
				argument_s			<= (others => '0');
				crc7_s				<= (others => '0');
				command_out_s		<= (others => '1');
				counter_v			:= TO_UNSIGNED(0, 8);
				byte_cnt_v			:= TO_UNSIGNED(0, BLOCK_BITS_g);
				write_addr_s		<= (others => '0');
				high_capacity_s	<= false;
				version_s			<= MMC;
				lba_v					:= (others => '0');
			else
				image_num_v := unsigned(image_num_i);
				track_num_v := unsigned(track_num_i);
				case state_s is
					---------------------------------------------------------------------
					-- SD Card init FSM
					---------------------------------------------------------------------
					when POWER_UP =>
						counter_v := TO_UNSIGNED(224, 8);
						state_s <= RAMP_UP;

					-- Output a series of 74 clock signals (or 1ms delay, whichever is
					-- greater) to wake up the card
					when RAMP_UP =>
						if counter_v = 0 then
							sd_cs_n_o		<= '0';
							command_s		<= CMD0;
							argument_s		<= (others => '0');
							crc7_s			<= "1001010";
							return_state_s	<= CHECK_CMD0;
							state_s			<= WAIT_NRC;
						else
							counter_v := counter_v - 1;
							sclk_sig_s		<= not sclk_sig_s;
						end if;

					-- CMD0: GO_IDLE_STATE ----------------------------------------------
					when CHECK_CMD0 =>
						if recv_bytes_s(7 downto 0) = x"01" then
							command_s <= CMD8;
							-- Propose 2.7-3.6V operating voltage and a "10101010" test pattern
							argument_s		<= x"000001AA";
							crc7_s			<= "1000011";
							return_state_s	<= CHECK_CMD8;
							state_s			<= WAIT_NRC;
						else
							state_s			<= ERROR;
						end if;

					-- CMD8: SEND_IF_COND -----------------------------------------------
					when CHECK_CMD8 =>
						argument_s		<= (others => '0');
						crc7_s			<= (others => '0');
						if recv_bytes_s(39 downto 32) <= x"01" then
							-- This is an SD 2.x/3.x Card
							version_s	<= SD2x;
							if recv_bytes_s(11 downto 8) /= "0001" or recv_bytes_s(7 downto 0) /= x"aa" then
								-- Operating voltage or pattern check failure
								state_s	<= ERROR;
							else
								command_s			<= CMD55;
								high_capacity_s	<= true;
								return_state_s		<= CHECK_CMD55;
								state_s				<= WAIT_NRC;
							end if;
						else
							-- This is an MMC Card or an SD 1.x Card
							version_s				<= SD1x;
							high_capacity_s		<= false;
							command_s				<= CMD55;
							return_state_s			<= CHECK_CMD55;
							state_s					<= WAIT_NRC;
						end if;

					-- CMD55: APP_CMD ---------------------------------------------------
					when CHECK_CMD55 =>
						if recv_bytes_s(7 downto 0) = x"01" then
							-- This is an SD Card
							command_s				<= ACMD41;
							if high_capacity_s then
								-- Ask for HCS (High Capacity Support)
								argument_s			<= x"40000000";
							end if;
							return_state_s			<= CHECK_ACMD41;
							state_s					<= WAIT_NRC;
						else
							-- This is an MMC Card
							version_s				<= MMC;
							command_s				<= CMD1;
							return_state_s			<= CHECK_CMD1;
							state_s					<= WAIT_NRC;
						end if;

					-- ACMD41: SEND_OP_CMD (SD Card) ------------------------------------
					when CHECK_ACMD41 =>
						if recv_bytes_s(7 downto 0) = x"00" then
							if version_s = SD2x then
								-- This is an SD 2.x/3.x Card, read OCR
								command_s			<= CMD58;
								argument_s			<= (others => '0');
								return_state_s		<= CHECK_CMD58;
								state_s				<= WAIT_NRC;
							else
								-- This is an SD 1.x Card, no HCS
								command_s			<= CMD16;
								argument_s			<= std_logic_vector(to_unsigned(BLOCK_SIZE_g, 32));
								return_state_s		<= CHECK_SET_BLOCKLEN;
								state_s				<= WAIT_NRC;
							end if;
						elsif recv_bytes_s(7 downto 0) = x"01" then
							-- Wait until the card goes out of idle state
							command_s				<= CMD55;
							argument_s				<= (others => '0');
							return_state_s			<= CHECK_CMD55;
							state_s					<= WAIT_NRC;
						else
							-- Found an MMC card that understands CMD55, but not ACMD41
							command_s				<= CMD1;
							return_state_s			<= CHECK_CMD1;
							state_s					<= WAIT_NRC;
						end if;

					-- CMD1: SEND_OP_CMD (MMC Card) -------------------------------------
					when CHECK_CMD1 =>
						if recv_bytes_s(7 downto 0) <= x"01" then
							command_s				<= CMD16;
							argument_s				<= std_logic_vector(to_unsigned(BLOCK_SIZE_g, 32));
							return_state_s			<= CHECK_SET_BLOCKLEN;
							state_s					<= WAIT_NRC;
						else
							-- Wait until the card goes out of idle state
							command_s				<= CMD1;
							return_state_s			<= CHECK_CMD1;
							state_s					<= WAIT_NRC;
						end if;

					-- CMD58: READ_OCR --------------------------------------------------
					when CHECK_CMD58 =>
						if recv_bytes_s(7 downto 0) = x"00" then
							if recv_bytes_s(30) = '1' then
								high_capacity_s <= true;
							else
								high_capacity_s <= false;
							end if;
							command_s				<= CMD16;
							argument_s				<= std_logic_vector(to_unsigned(BLOCK_SIZE_g, 32));
							return_state_s			<= CHECK_SET_BLOCKLEN;
							state_s					<= WAIT_NRC;
						else
							state_s					<= ERROR;
						end if;

					-- CMD16: SET_BLOCKLEN (BLOCK_SIZE) ---------------------------------
					when CHECK_SET_BLOCKLEN =>
						if recv_bytes_s(7 downto 0) = x"00" then
							slow_clk_s				<= false;
							state_s					<= IDLE;
						else
							state_s					<= ERROR;
						end if;

					-- Error state ------------------------------------------------------
					when ERROR =>
						sclk_sig_s					<= '0';
						slow_clk_s					<= true;
						sd_cs_n_o					<= '1';
						error_s						<= true;

					---------------------------------------------------------------------
					-- Embedded "read track" FSM
					---------------------------------------------------------------------
					-- Idle state where we sit waiting for user image/track requests ----
					when IDLE =>
						if track_num_v /= current_track_s or image_num_v /= current_image_s then
							-- Compute the LBA (Logical Block Address) from the given
							-- image/track numbers.
							-- So: lba_v = image * 0x3D400 + track * 0x1880
							-- In order to avoid multiplications by constants, we replace
							-- them by direct add/sub of shifted image/track values:
							-- 0x3D400 = 0x40000 - 0x4000 + 0x1000 + 0x400
							-- 0x01880 =  0x1000 +  0x800 +   0x80
							-- A=40000, B=20000, C=10000, D=8000, E=4000, F=2000
							-- G=1000, H=800, I=400, J=200, K=100, L=80
							--                                ABCDEFGHIJKL
							lba_v := ("0000" & image_num_v & "000000000000000000") -		-- 40000 -
										(         image_num_v &     "00000000000000") +		--  4000 +
										(         image_num_v &       "000000000000") +		--  1000 +
										(         image_num_v &         "0000000000") +		--   400 +
							-------------------------------------------------------------
							--                                ABCDEFGHIJKL
										(         track_num_v &       "000000000000") +		--  1000 +
										(         track_num_v &        "00000000000") +		--   800 +
										(         track_num_v &            "0000000");		--    80
							if high_capacity_s then
								-- For SDHC, blocks are addressed by blocks, not bytes
								lba_v := lba_v srl BLOCK_BITS_g;
							end if;
							write_addr_s			<= (others => '0');
							sd_cs_n_o				<= '0';
							state_s					<= READ_TRACK;
							current_track_s		<= track_num_v;
							current_image_s		<= image_num_v;
						else
							sd_cs_n_o				<= '1';
							sclk_sig_s				<= '1';
						end if;

					-- Read in a whole track into buffer memory -------------------------
					when READ_TRACK =>
						if write_addr_s = 16#1880# then
							state_s				<= IDLE;
						else
							command_s			<= CMD17;
							argument_s			<= std_logic_vector(lba_v);
							return_state_s		<= READ_BLOCK_WAIT;
							state_s				<= WAIT_NRC;
						end if;

					-- Wait for a 0 bit to signal the start of the block ----------------
					when READ_BLOCK_WAIT =>
						if sclk_sig_s = '1' and sd_miso_i = '0' then
							state_s				<= READ_BLOCK_DATA;
							byte_cnt_v			:= TO_UNSIGNED(BLOCK_SIZE_g - 1, BLOCK_BITS_g);
							counter_v			:= TO_UNSIGNED(7, 8);
							return_state_s		<= READ_BLOCK_DATA;
							state_s				<= RECEIVE_BYTE;
						end if;
						sclk_sig_s				<= not sclk_sig_s;

					-- Read a block of data ---------------------------------------------
					when READ_BLOCK_DATA =>
						if write_addr_s < 16#1880# then
							ram_we_o				<= '1';
							write_addr_s		<= write_addr_s + 1;
						end if;
						if byte_cnt_v = 0 then
							counter_v			:= TO_UNSIGNED(7, 8);
							return_state_s		<= READ_BLOCK_CRC;
							state_s				<= RECEIVE_BYTE;
						else
							byte_cnt_v			:= byte_cnt_v - 1;
							counter_v			:= TO_UNSIGNED(7, 8);
							return_state_s		<= READ_BLOCK_DATA;
							state_s				<= RECEIVE_BYTE;
						end if;

					-- Read the block CRC -----------------------------------------------
					when READ_BLOCK_CRC =>
						counter_v					:= TO_UNSIGNED(7, 8);
						return_state_s			<= READ_TRACK;
						if high_capacity_s then
							lba_v					:= lba_v + 1;
						else
							lba_v					:= lba_v + BLOCK_SIZE_g;
						end if;
						state_s					<= RECEIVE_BYTE;

					---------------------------------------------------------------------
					-- Embedded "command" FSM
					---------------------------------------------------------------------
					-- Wait for card response in front of host command ------------------
					when WAIT_NRC =>
						counter_v				:= TO_UNSIGNED(63, 8);
						command_out_s			<= "11111111" & "01" & command_s & argument_s & crc7_s & "1";
						sclk_sig_s				<= not sclk_sig_s;
						state_s					<= SEND_CMD;

					-- Send a command to the card ---------------------------------------
					when SEND_CMD =>
						if sclk_sig_s = '1' then
							if counter_v = 0 then
								state_s			<= RECEIVE_BYTE_WAIT;
							else
								counter_v		:= counter_v - 1;
								command_out_s	<= command_out_s(54 downto 0) & "1";
							end if;
						end if;
						sclk_sig_s				<= not sclk_sig_s;

					-- Wait for a "0", indicating the first bit of a response -----------
					when RECEIVE_BYTE_WAIT =>
						if sclk_sig_s = '1' then
							if sd_miso_i = '0' then
								recv_bytes_s	<= (others => '0');
								if command_s = CMD8 or command_s = CMD58 then
									-- This is an R7 response, but we already have read bit 39
									counter_v	:= TO_UNSIGNED(38,8);
								else
									-- This is a data byte or an r1 response, but we already read
									-- bit 7
									counter_v	:= TO_UNSIGNED(6, 8);
								end if;
								state_s			<= RECEIVE_BYTE;
							end if;
						end if;
						sclk_sig_s				<= not sclk_sig_s;

					-- Receive a byte ---------------------------------------------------
					when RECEIVE_BYTE =>
						if sclk_sig_s = '1' then
							recv_bytes_s		<= recv_bytes_s(38 downto 0) & sd_miso_i;
							if counter_v = 0 then
								state_s			<= return_state_s;
								ram_data_o		<= std_logic_vector(recv_bytes_s(6 downto 0)) & sd_miso_i;
							else
								counter_v		:= counter_v - 1;
							end if;
						end if;
						sclk_sig_s				<= not sclk_sig_s;

					when others => 
						null;

				end case;
			end if;
		end if;
	end process sd_fsm;

	sd_mosi_o <= command_out_s(55);

	-- Debug
	D_error_o <= '1' when error_s else '0';

end architecture;
