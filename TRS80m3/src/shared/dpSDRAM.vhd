--
-- Emulacao memoria dual-port para SDRAMs
--

library ieee;
use ieee.std_logic_1164.all;

entity dpSDRAM is
	generic (
		freq	: integer := 100
	);
	port (
		memclock			: in    std_logic;								-- Clock da SDRAM
		reset				: in    std_logic;								-- Reset geral
		ram_wait			: out   std_logic;								-- RAM em uso
		-- Porta 0 = ULA
		port0_cs			: in    std_logic;								-- Chip select porta 0
		port0_oe			: in    std_logic;								-- Leitura
		port0_we			: in    std_logic;								-- Escrita
		port0_addr			: in    std_logic_vector(21 downto 0);		-- Endereco da memoria
		port0_din			: in    std_logic_vector(7 downto 0);		-- Dados de entrada
		port0_dout			: out   std_logic_vector(7 downto 0);		-- Dados de saida
		-- Porta 1 = CPU
		port1_cs			: in    std_logic;								-- Chip select porta 1
		port1_oe			: in    std_logic;								-- Leitura
		port1_we			: in    std_logic;								-- Escrita
		port1_addr			: in    std_logic_vector(21 downto 0);		-- Endereco da memoria
		port1_din			: in    std_logic_vector(7 downto 0);		-- Dados de entrada
		port1_dout			: out   std_logic_vector(7 downto 0);		-- Dados de saida
		-- SD-RAM ports
		pMemClk			: out   std_logic;								-- SD-RAM Clock
		pMemCke			: out   std_logic;								-- SD-RAM Clock enable
		pMemCs_n			: out   std_logic;								-- SD-RAM Chip select
		pMemRas_n		: out   std_logic;								-- SD-RAM Row/RAS
		pMemCas_n		: out   std_logic;								-- SD-RAM /CAS
		pMemWe_n			: out   std_logic;								-- SD-RAM /WE
		pMemUdq			: out   std_logic;								-- SD-RAM UDQM
		pMemLdq			: out   std_logic;								-- SD-RAM LDQM
		pMemBa1			: out   std_logic;								-- SD-RAM Bank select address 1
		pMemBa0			: out   std_logic;								-- SD-RAM Bank select address 0
		pMemAdr			: out   std_logic_vector(11 downto 0);		-- SD-RAM Address
		pMemDat			: inout std_logic_vector(15 downto 0)		-- SD-RAM Data
	);
end entity;

architecture Behavior of dpSDRAM is

	component sdram is
		generic(
			freq : integer
		);
		port(
			clk				: in    std_logic;		
			refresh			: in    std_logic;
			
			memAddress1		: in    std_logic_vector(21 downto 0);
			memDataIn1		: in    std_logic_vector(15 downto 0);
			memDataOut1		: out   std_logic_vector(15 downto 0);
			memDataMask1	: in    std_logic_vector(1 downto 0);
			memWr1			: in    std_logic;
			memReq1			: in    std_logic;
			memAck1			: out   std_logic := '0';

			memAddress2		: in    std_logic_vector(21 downto 0);
			memDataIn2		: in    std_logic_vector(15 downto 0);
			memDataOut2		: out   std_logic_vector(15 downto 0);
			memDataMask2	: in    std_logic_vector(1 downto 0);
			memWr2			: in    std_logic;
			memReq2			: in    std_logic;
			memAck2			: out   std_logic := '0';

			-- SD-RAM ports
			pMemClk			: out   std_logic;								-- SD-RAM Clock
			pMemCke			: out   std_logic;								-- SD-RAM Clock enable
			pMemCs_n			: out   std_logic;								-- SD-RAM Chip select
			pMemRas_n		: out   std_logic;								-- SD-RAM Row/RAS
			pMemCas_n		: out   std_logic;								-- SD-RAM /CAS
			pMemWe_n			: out   std_logic;								-- SD-RAM /WE
			pMemUdq			: out   std_logic;								-- SD-RAM UDQM
			pMemLdq			: out   std_logic;								-- SD-RAM LDQM
			pMemBa1			: out   std_logic;								-- SD-RAM Bank select address 1
			pMemBa0			: out   std_logic;								-- SD-RAM Bank select address 0
			pMemAdr			: out   std_logic_vector(11 downto 0);		-- SD-RAM Address
			pMemDat			: inout std_logic_vector(15 downto 0)		-- SD-RAM Data
		);
	end component;

	signal ram1_addr			: std_logic_vector(21 downto 0);
	signal ram1_din			: std_logic_vector(15 downto 0);
	signal ram1_dout			: std_logic_vector(15 downto 0);
	signal ram1_we				: std_logic;
	signal ram1_req			: std_logic;
	signal ram1_ack			: std_logic;
	signal ram2_addr			: std_logic_vector(21 downto 0);
	signal ram2_din			: std_logic_vector(15 downto 0);
	signal ram2_dout			: std_logic_vector(15 downto 0);
	signal ram2_we				: std_logic;
	signal ram2_req			: std_logic;
	signal ram2_ack			: std_logic;

begin

	-- SDRAM
	ram: sdram
	generic map (
		freq => freq													-- Frequencia de trabalho
	)
	port map(
		clk						=> memclock,
		refresh					=> '1',								-- Controlador insere ciclos de refresh automaticamente

		memAddress1				=> ram1_addr,
		memDataIn1				=> ram1_din,
		memDataOut1				=> ram1_dout,
		memDataMask1			=> "01",								-- Enable das saidas dividido em 8 bits, se 0 saida = Z
		memWr1					=> ram1_we,							-- 1 para gravacao, 0 para leitura
		memReq1					=> ram1_req,						-- 1 ativa, 0 desativa
		memAck1					=> ram1_ack,						-- 1 significa que dado foi lido ou gravado

		memAddress2				=> ram2_addr,
		memDataIn2				=> ram2_din,
		memDataOut2				=> ram2_dout,
		memDataMask2			=> "01",								-- Enable das saidas dividido em 8 bits, se 0 saida = Z
		memWr2					=> ram2_we,							-- 1 para gravacao, 0 para leitura
		memReq2					=> ram2_req,						-- 1 ativa, 0 desativa
		memAck2					=> ram2_ack,						-- 1 significa que dado foi lido ou gravado

		-- SD-RAM ports
		pMemClk					=> pMemClk,
		pMemCke					=> pMemCke,
		pMemCs_n					=> pMemCs_n,
		pMemRas_n				=> pMemRas_n,
		pMemCas_n				=> pMemCas_n,
		pMemWe_n					=> pMemWe_n,
		pMemUdq					=> pMemUdq,
		pMemLdq					=> pMemLdq,
		pMemBa1					=> pMemBa1,
		pMemBa0					=> pMemBa0,
		pMemAdr					=> pMemAdr,
		pMemDat					=> pMemDat
	);


	-- Ligacao do barramento de dados da CPU/ULA com a temporizacao exigida pela SDRAM
	process (reset, memclock)
		variable ulacs		: std_logic_vector(1 downto 0);			-- Salva estado anterior e atual do pedido de acesso
		variable acesso	: std_logic;									-- Acesso (1 = pedindo acesso de gravacao ou leitura)
	begin
		if reset = '1' then
			port0_dout		<= (others => '1');
			ram1_we		<= '0';
			ram1_req		<= '0';
			ulacs			:= "00";
		elsif rising_edge(memclock) then

			if ram1_req = '1' and ram1_ack = '1' then					-- Se pedimos solicitacao ao controlador e recebemos reconhecimento
				if ram1_we = '0' then										-- Se nao foi gravacao que pedimos
					port0_dout <= ram1_dout(7 downto 0);					-- Pegamos o dado que o controlador leu da SDRAM
				end if;
				ram1_req <= '0';												-- Limpamos a flag de solicitacao, assim liberamos essa porta
			end if;

			if ulacs = "01" then												-- Detectar rising edge do acesso
				ram1_addr	<= port0_addr;									-- ULA requerendo acesso, definimos o endereco para o controlador
				ram1_req		<= '1';											-- Pedimos uma solicitacao ao controlador
				if port0_we = '1' then										-- Se a ULA pediu gravacao:
					ram1_din		<= port0_din & port0_din;				-- Pegamos o dado que a ULA quer gravar e mandamos pro controlador
					ram1_we		<= '1';									-- Informamos ao controlador que e gravacao
				else
					ram1_we		<= '0';									-- Caso nao seja gravacao, informamos que e leitura
				end if;
			end if;
			
			acesso	:= port0_cs and (port0_oe or port0_we);			-- ULA requer acesso quando vram_cs = 1 e (vram_oe = 1 ou vram_we = 1)
			-- Desloca
			ulacs		:= ulacs(0) & acesso;							-- Deslocamos o acesso, assim temos a informacao do acesso anterior e atual

		end if;
	end process;

	process (reset, memclock)											-- Aqui e a mesma coisa que o controle da porta 1 para a ULA
		variable ramcs		: std_logic_vector(1 downto 0);		-- Explicacoes acima
		variable acesso	: std_logic;
	begin
		if reset = '1' then
			port1_dout	<= (others => '1');
			ram2_we		<= '0';
			ram2_req		<= '0';
			ramcs			:= "00";
			ram_wait		<= '0';
		elsif rising_edge(memclock) then
			if ram2_req = '1' and ram2_ack = '1' then
				if ram2_we = '0' then
					port1_dout <= ram2_dout(7 downto 0);
				end if;
				ram2_req <= '0';
				ram_wait	<= '0';
			end if;

			if ramcs = "01" then
				ram2_addr	<= port1_addr;
				ram2_req		<= '1';
				if port1_we = '1' then
					ram2_din		<= port1_din & port1_din;
					ram2_we		<= '1';
				else
					ram2_we		<= '0';
				end if;
				ram_wait	<= '1';
			end if;

			acesso	:= port1_cs and (port1_oe or port1_we);
			-- Desloca
			ramcs 	:= ramcs(0) & acesso;

		end if;
	end process;

end architecture;