-------------------------------------------------------------------------------
--
-- TRS80 M3 FPGA project
--
-- Copyright (c) 2016, Fabio Belavenuto (belavenuto@gmail.com)
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- Please report bugs to the author, but before you do so, please
-- make sure that this is not a derivative work and that
-- you have the latest version of this file.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity multicore_top is
	port (
		-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= '0'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
	);
end entity;

architecture behavior of multicore_top is

	-- PLL
	signal clock_master_s	: std_logic;
	signal pll_locked_s		: std_logic;
	signal clock_vga_s		: std_logic;
	signal clock_hdmi_s		: std_logic;

	-- Resets
	signal reset_por_s		: std_logic;
	signal reset_s				: std_logic;

	-- RAM
	signal ram_rd_s			: std_logic;
	signal ram_wr_s			: std_logic;
	signal ram_addr_s			: std_logic_vector(15 downto 0);
	signal ram_data_from_s	: std_logic_vector( 7 downto 0);
	signal ram_data_to_s		: std_logic_vector( 7 downto 0);

	-- ROM
	signal rom_addr_s			: std_logic_vector(13 downto 0);
	signal rom_data_from_s	: std_logic_vector( 7 downto 0);

	-- Audio
	signal sound_s				: std_logic_vector( 1 downto 0);
	signal sound_hdmi_s		: std_logic_vector(15 downto 0);

	-- Video and scandoubler
	signal video_bit_s		: std_logic;
	signal video_bit_2x_s	: std_logic;
	signal video_3bit_s		: std_logic_vector(2 downto 0);
	signal video_hs_n_s		: std_logic;
	signal video_vs_n_s		: std_logic;
	signal vga_hsync_n_s		: std_logic;
	signal vga_vsync_n_s		: std_logic;
	signal pix_hcnt_s			: std_logic_vector( 9 downto 0);
	signal pix_vcnt_s			: std_logic_vector( 8 downto 0);
	signal btn_scan_s			: std_logic;
	signal scanlines_en_s	: std_logic;
	signal odd_line_s			: std_logic;
	signal vga_blank_s		: std_logic;
	signal tdms_s				: std_logic_vector( 7 downto 0);

	-- Teclado
	signal kb_rows_s			: std_logic_vector( 7 downto 0);
	signal kb_columns_s		: std_logic_vector( 7 downto 0);
	signal FKeys_s				: std_logic_vector(12 downto 1);

	-- HDMI		
	signal tdms_r_s			: std_logic_vector( 9 downto 0);
	signal tdms_g_s			: std_logic_vector( 9 downto 0);
	signal tdms_b_s			: std_logic_vector( 9 downto 0);
	signal hdmi_p_s			: std_logic_vector( 3 downto 0);
	signal hdmi_n_s			: std_logic_vector( 3 downto 0);
	
begin

	--------------------------------
	-- PLL
	--  50 MHz input
	--------------------------------
	U00: entity work.pll1
	port map (
		inclk0		=> clock_50_i,
		c0				=> clock_master_s,		-- 20.238095 MHz
		c1				=> clock_vga_s,			-- 25.111
		c2				=> clock_hdmi_s,			-- 125.555
		locked		=> pll_locked_s
	);

	----------------
	-- TRS-80 Model 3
	----------------
	trs80_inst: entity work.trs80
	generic map (
		use_az80_g			=> false
	)
	port map (
		clock_i				=> clock_master_s,
		por_i					=> reset_por_s,
		reset_i				=> reset_s,
		-- Options
		opt_floppy_i		=> '0',
		-- RAM
		ram_addr_o			=> ram_addr_s,
		ram_data_to_o		=> ram_data_to_s,
		ram_data_from_i	=> ram_data_from_s,
		ram_rd_o				=> ram_rd_s,
		ram_wr_o				=> ram_wr_s,
		-- ROM
		rom_addr_o			=> rom_addr_s,
		rom_data_from_i	=> rom_data_from_s,
		rom_rd_o				=> open,
		-- Video
		video_bit_o			=> video_bit_s,
		video_hs_n_o		=> video_hs_n_s,
		video_vs_n_o		=> video_vs_n_s,
		pix_hcnt_o			=> pix_hcnt_s,
		pix_vcnt_o			=> pix_vcnt_s,
		-- Audio
		sound_o				=> sound_s,
		-- Cassete
		cas500_i				=> ear_i,
		cas1500_i			=> ear_i,
		-- Keyboard
		kb_rows_o			=> kb_rows_s,
		kb_columns_i		=> kb_columns_s,
		-- Bus
		bus_a_o				=> open,
		bus_d_io				=> open,
		bus_in_n_o			=> open,
		bus_out_n_o			=> open,
		bus_reset_n_o		=> open,
		bus_int_n_i			=> '1',
		bus_wait_n_i		=> '1',
		bus_extiosel_n_i	=> '1',
		bus_m1_n_o			=> open,
		bus_iorq_n_o		=> open,	
		bus_enextio_n_o	=> open,
		-- Img and SD card
		image_num_i			=> (others => '0'),
		sd_cs_n_o			=> sd_cs_n_o,
		sd_miso_i			=> sd_miso_i,
		sd_mosi_o			=> sd_mosi_o,
		sd_sclk_o			=> sd_sclk_o,
		-- Debug
		D_cpu_a_o			=> open
	);

	-- Audio
	audioout: entity work.Audio_DAC
	port map (
		clock_i	=> clock_master_s,
		reset_i	=> reset_s,
		spk_i		=> sound_s,
		dac_r_o	=> dac_r_o,
		dac_l_o	=> dac_l_o
	);

	---------------
	-- ROM
	---------------
	m3_rom: entity work.rom_cp300
	port map (
		clk	=> clock_master_s,
		addr	=> rom_addr_s,
		data	=> rom_data_from_s
	);

	---------------
	-- Teclado
	---------------
	teclado: entity work.keyboard
	generic map (
		clkfreq		=> 20238
	)
	port map (
		clock_i		=> clock_master_s,
		por_i			=> reset_por_s,
		reset_i		=> reset_s,
		ps2_clk_io	=> ps2_clk_io,
		ps2_data_io	=> ps2_data_io,
		rows_i		=> kb_rows_s,
		cols_o		=> kb_columns_s,
		teclasF_o	=> FKeys_s
	);

	---------------------------------
	-- scanlines
	btnscl: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clock_master_s,
		button_i			=> btn_n_i(1) or btn_n_i(2),
		result_o			=> btn_scan_s
	);

	-- glue
	reset_por_s	<= not pll_locked_s;
	reset_s		<= '1'	when pll_locked_s = '0' or (btn_n_i(3) = '0' and btn_n_i(4) = '0') or FKeys_s(4) = '1'		else '0';

	-- RAM
	sram_addr_o			<= "000" & ram_addr_s;
	sram_data_io		<= ram_data_to_s			when ram_wr_s = '1' 	else (others => 'Z');
	ram_data_from_s	<= sram_data_io			when ram_rd_s = '1' 	else (others => '1');
	sram_oe_n_o			<= not ram_rd_s;
	sram_we_n_o			<= not ram_wr_s;

	-- Video
	-- Scanlines
	process (pll_locked_s, btn_scan_s)
	begin
		if pll_locked_s = '0' then
			scanlines_en_s <= '0';
		elsif falling_edge(btn_scan_s) then
			scanlines_en_s <= not scanlines_en_s;
		end if;
	end process;

	-- VGA framebuffer
	vga: entity work.vga
	port map (
		I_CLK			=> clock_master_s,
		I_CLK_VGA	=> clock_vga_s,
		I_COLOR(0)	=> video_bit_s,
		I_HCNT		=> pix_hcnt_s,
		I_VCNT		=> pix_vcnt_s,
		O_HSYNC		=> vga_hsync_n_s,
		O_VSYNC		=> vga_vsync_n_s,
		O_COLOR(0)	=> video_bit_2x_s,
		O_ODDLINE	=> odd_line_s,
		O_BLANK		=> vga_blank_s
	);
	
	video_3bit_s <= video_bit_2x_s & '0' & video_bit_2x_s		when scanlines_en_s = '1' and odd_line_s = '1'	else (others => video_bit_2x_s);




		vga_r_o			<= video_3bit_s & "00";
		vga_g_o			<= video_3bit_s & "00";
		vga_b_o			<= video_3bit_s & "00";
		vga_hsync_n_o	<= vga_hsync_n_s;
		vga_vsync_n_o	<= vga_vsync_n_s;
		
		-- HDMI
		inst_dvid: entity work.hdmi
		generic map (
			FREQ	=> 25200000,	-- pixel clock frequency 
			FS		=> 48000,		-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
			CTS	=> 25200,		-- CTS = Freq(pixclk) * N / (128 * Fs)
			N		=> 6144			-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
		) 
		port map (
			I_CLK_VGA		=> clock_vga_s,
			I_CLK_TMDS		=> clock_hdmi_s,
			I_RED				=> video_3bit_s & video_3bit_s & video_3bit_s(1 downto 0),
			I_GREEN			=> video_3bit_s & video_3bit_s & video_3bit_s(1 downto 0),
			I_BLUE			=> video_3bit_s & video_3bit_s & video_3bit_s(1 downto 0),
			I_BLANK			=> vga_blank_s,
			I_HSYNC			=> vga_hsync_n_s,
			I_VSYNC			=> vga_vsync_n_s,
			I_AUDIO_PCM_L 	=> sound_hdmi_s,
			I_AUDIO_PCM_R	=> sound_hdmi_s,
			O_TMDS			=> tmds_o
		);
		
		sound_hdmi_s <= '0' & sound_s & sound_s & sound_s & sound_s & sound_s & sound_s & sound_s & '0';
		




	-- Debug
--	leds_n_o(0) <= ear_i;

end architecture;
