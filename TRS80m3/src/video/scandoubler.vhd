-- -----------------------------------------------------------------------
--
--                                 FPGA 64
--
--     A fully functional commodore 64 implementation in a single FPGA
--
-- -----------------------------------------------------------------------
-- Copyright 2005-2008 by Peter Wendrich (pwsoft@syntiac.com)
-- http://www.syntiac.com/fpga64.html
-- -----------------------------------------------------------------------
--
-- fpga64_scandoubler.vhd
--
-- -----------------------------------------------------------------------
--
-- Converts 15.6 Khz PAL/NTSC screen to 31 Khz VGA screen by doubling
-- each scanline.
--
-- -----------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.ALL;
use IEEE.numeric_std.all;

entity scandoubler is
	generic (
		hsync_polarity_g	: std_logic		:= '0';
		vsync_polarity_g	: std_logic		:= '0'
	);
	port (
		clock_i				: in    std_logic;
		enable_i				: in    std_logic;
		scanlines_i			: in    std_logic := '0';
		video_i				: in    std_logic;
		vsync_i				: in    std_logic;
		hsync_i				: in    std_logic;
		video_o				: out   std_logic;
		vsync_o				: out   std_logic;
		hsync_o				: out   std_logic
	);
end entity;

architecture rtl of scandoubler is

	constant hSyncLength : integer := 63;
	constant lineLengthBits : integer := 11;

	signal prescale		: unsigned(0 downto 0);
	signal impar			: std_logic;

	signal startIndex		: unsigned(10 downto 0) := (others => '0');
	signal endIndex		: unsigned(10 downto 0) := (others => '0');
	signal readIndex		: unsigned(10 downto 0) := (others => '0');
	signal writeIndex 	: unsigned(10 downto 0) := (others => '0');
	signal oldHSync		: std_logic             := '0';
	signal oldVSync		: std_logic             := '0';
	signal hSyncCount		: integer range 0 to hSyncLength;
	signal vSyncCount		: integer range 0 to 15;
	signal lineLength		: unsigned(lineLengthBits downto 0);
	signal lineLengthCnt	: unsigned((lineLengthBits+1) downto 0);
	signal nextLengthCnt	: unsigned((lineLengthBits+1) downto 0);

	signal ramQ          : std_logic_vector(0 downto 0);
	signal ramQReg       : std_logic;

	component dpram is
	generic (
		addr_width_g : integer := 8;
		data_width_g : integer := 8
	);
	port (
		clk_a_i  : in  std_logic;
		we_i     : in  std_logic;
		addr_a_i : in  std_logic_vector(addr_width_g-1 downto 0);
		data_a_i : in  std_logic_vector(data_width_g-1 downto 0);
		data_a_o : out std_logic_vector(data_width_g-1 downto 0);

		clk_b_i  : in  std_logic;
		addr_b_i : in  std_logic_vector(addr_width_g-1 downto 0);
		data_b_o : out std_logic_vector(data_width_g-1 downto 0)
  );
	end component;

begin

	lineRam: dpram
	generic map (
		addr_width_g => 11,
		data_width_g => 1
	)
	port map (
		clk_a_i  		=> clock_i,
		we_i     		=> '1',
		addr_a_i 		=> std_logic_vector(writeIndex),
		data_a_i 		=> (0 => video_i),
		data_a_o			=> open,
		clk_b_i  		=> clock_i,
		addr_b_i 		=> std_logic_vector(readIndex),
		data_b_o			=> ramQ
	);

	nextLengthCnt	<= lineLengthCnt + 1;
	
	process (clock_i)
	begin
		if rising_edge(clock_i) then
			prescale <= prescale + 1;
			lineLengthCnt <= nextLengthCnt;

			if prescale(0) = '0' and hsync_i = '1' then
				if enable_i = '1' then
					writeIndex <= writeIndex + 1;
				end if;
			end if;

			if hSyncCount /= 0 then
				hSyncCount <= hSyncCount - 1;
			end if;

			if hSyncCount = 0 then
				readIndex <= readIndex + 1;
			end if;

			if lineLengthCnt = lineLength then
				readIndex <= startIndex;
				hSyncCount <= hSyncLength;
				prescale <= (others => '0');
				impar <= '1';
			end if;

			oldHSync <= hsync_i;
			if (oldHSync = '1') and (hsync_i = '0') then
				-- Calculate length of the scanline/2
				-- The scandoubler adds a second sync half way to double the lines.
				lineLength <= lineLengthCnt((lineLengthBits+1) downto 1);
				lineLengthCnt <= to_unsigned(0, lineLengthBits+2);

				readIndex   <= endIndex;
				startIndex  <= endIndex;
				endIndex    <= writeIndex;
				hSyncCount  <= hSyncLength;
				prescale    <= (others => '0');
				impar			<= '0';

				oldVSync <= vsync_i;
				if (vsync_i = '1') and (oldVSync = '0') then
					vSyncCount <= 15;
				elsif vSyncCount /= 0 then
					vSyncCount <= vSyncCount - 1;
				end if;
			end if;
		end if;
	end process;

	-- Video out
	process (clock_i)
	begin
		if rising_edge(clock_i) then
			ramQReg		<= ramQ(0);
			video_o		<= ramQReg;
			if vSyncCount /= 0 or (scanlines_i = '1' and impar = '1') then
				video_o	<= '0';
			end if;
		end if;
	end process;

	-- Horizontal sync
	process (clock_i)
	begin
		if rising_edge(clock_i) then
			hsync_o  <= not hsync_polarity_g;
			if hSyncCount /= 0 then
				hsync_o <= hsync_polarity_g;
			end if;
		end if;
	end process;

	-- Vertical sync
	process (clock_i)
	begin
		if rising_edge(clock_i) then
			vsync_o <= not vsync_polarity_g;
			if (vSyncCount = 9) or (vSyncCount = 10) then
				vsync_o <= vsync_polarity_g;
			end if;
		end if;
	end process;

end architecture;
