
#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {clk_50} -period 20.000 -waveform { 0.000 0.500 } [get_ports {clock_50_i}]


#**************************************************************
# Create Generated Clock
#**************************************************************

derive_pll_clocks 
create_generated_clock -name sysclk_slow 	-source [get_pins {U00|altpll_component|auto_generated|pll1|clk[0]}]
create_generated_clock -name sysclk_vga 	-source [get_pins {U00|altpll_component|auto_generated|pll1|clk[1]}]
create_generated_clock -name sysclk_hdmi 	-source [get_pins {U00|altpll_component|auto_generated|pll1|clk[2]}]

#U00|altpll_component|auto_generated|pll1|clk[0]

#**************************************************************
# Set Clock Latency
#**************************************************************


#**************************************************************
# Set Clock Uncertainty
#**************************************************************

derive_clock_uncertainty;

#**************************************************************
# Set Input Delay
#**************************************************************

set_input_delay -clock sd1clk_pin -max 5.8 [get_ports sdram_da*]
set_input_delay -clock sd1clk_pin -min 3.2 [get_ports sdram_da*]




set_input_delay  -clock sysclk_slow  -min 0.0 [get_ports {sd_miso_i}]
set_input_delay  -clock sysclk_slow  -min 0.5 [get_ports {sd_miso_i}]


#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -clock sd1clk_pin -max 1.5 [get_ports sdram_*]
set_output_delay -clock sd1clk_pin -min -0.8 [get_ports sdram_*]
set_output_delay -clock sd1clk_pin -max 0.5 [get_ports sdram_clk_o]
set_output_delay -clock sd1clk_pin -min 0.5 [get_ports sdram_clk_o]

# Delays for async signals - not necessary, but might as well avoid
# having unconstrained ports in the design
set_output_delay -clock sysclk_slow -min 0.0 [get_ports stm_tx_o]
set_output_delay -clock sysclk_slow -max 0.5 [get_ports stm_tx_o]

set_output_delay -clock sysclk_slow -min 0.0 [get_ports sd_sclk_o]
set_output_delay -clock sysclk_slow -max 0.5 [get_ports sd_sclk_o]
set_output_delay -clock sysclk_slow -min 0.0 [get_ports sd_cs_n_o]
set_output_delay -clock sysclk_slow -max 0.5 [get_ports sd_cs_n_o]
set_output_delay -clock sysclk_slow -min 0.0 [get_ports sd_mosi_o]
set_output_delay -clock sysclk_slow -max 0.5 [get_ports sd_mosi_o]





#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************

set_false_path -from {btn*} -to {*}
set_false_path -from {ps2_*} -to {*}
set_false_path -to {ps2_*} -from {*}


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************
