

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;


entity C16_de1 is
port (
	-- Clocks
	CLOCK_24	:	in	std_logic_vector(1 downto 0);
	CLOCK_27	:	in	std_logic_vector(1 downto 0);
	CLOCK_50	:	in	std_logic;
	EXT_CLOCK	:	in	std_logic;
	
	-- Switches
	SW			:	in	std_logic_vector(9 downto 0);
	-- Buttons
	KEY			:	in	std_logic_vector(3 downto 0);
	
	-- 7 segment displays
	HEX0		:	out	std_logic_vector(6 downto 0);
	HEX1		:	out	std_logic_vector(6 downto 0);
	HEX2		:	out	std_logic_vector(6 downto 0);
	HEX3		:	out	std_logic_vector(6 downto 0);
	-- Red LEDs
	LEDR		:	out	std_logic_vector(9 downto 0);
	-- Green LEDs
	LEDG		:	out	std_logic_vector(7 downto 0);
	
	-- VGA
	VGA_R		:	out	std_logic_vector(3 downto 0);
	VGA_G		:	out	std_logic_vector(3 downto 0);
	VGA_B		:	out	std_logic_vector(3 downto 0);
	VGA_HS		:	out	std_logic;
	VGA_VS		:	out	std_logic;
	
	-- Serial
	UART_RXD	:	in	std_logic;
	UART_TXD	:	out	std_logic;
	
	-- PS/2 Keyboard
	PS2_CLK		:	inout	std_logic;
	PS2_DAT		:	inout	std_logic;
	
	-- I2C
	I2C_SCLK	:	inout	std_logic;
	I2C_SDAT	:	inout	std_logic;
	
	-- Audio
	AUD_XCK		:	out		std_logic;
	AUD_BCLK	:	out		std_logic;
	AUD_ADCLRCK	:	out		std_logic;
	AUD_ADCDAT	:	in		std_logic;
	AUD_DACLRCK	:	out		std_logic;
	AUD_DACDAT	:	out		std_logic;
	
	-- SRAM
	SRAM_ADDR	:	out		std_logic_vector(17 downto 0);
	SRAM_DQ		:	inout	std_logic_vector(15 downto 0);
	SRAM_CE_N	:	out		std_logic;
	SRAM_OE_N	:	out		std_logic;
	SRAM_WE_N	:	out		std_logic;
	SRAM_UB_N	:	out		std_logic;
	SRAM_LB_N	:	out		std_logic;
	
	-- SDRAM
	DRAM_ADDR	:	out		std_logic_vector(11 downto 0);
	DRAM_DQ		:	inout	std_logic_vector(15 downto 0);
	DRAM_BA_0	:	in		std_logic;
	DRAM_BA_1	:	in		std_logic;
	DRAM_CAS_N	:	in		std_logic;
	DRAM_CKE	:	in		std_logic;
	DRAM_CLK	:	in		std_logic;
	DRAM_CS_N	:	in		std_logic;
	DRAM_LDQM	:	in		std_logic;
	DRAM_RAS_N	:	in		std_logic;
	DRAM_UDQM	:	in		std_logic;
	DRAM_WE_N	:	in		std_logic;
	
	-- Flash
	FL_ADDR		:	out		std_logic_vector(21 downto 0);
	FL_DQ		:	inout	std_logic_vector(7 downto 0);
	FL_RST_N	:	out		std_logic;
	FL_OE_N		:	out		std_logic;
	FL_WE_N		:	out		std_logic;
	FL_CE_N		:	out		std_logic;
	
	-- SD card (SPI mode)
	SD_nCS		:	out		std_logic;
	SD_MOSI		:	out		std_logic;
	SD_SCLK		:	out		std_logic;
	SD_MISO		:	in		std_logic;
	
	-- GPIO
	GPIO_0		:	inout	std_logic_vector(35 downto 0);
	GPIO_1		:	inout	std_logic_vector(35 downto 0)
	);
end entity;

architecture rtl of C16_de1 is

-------------
-- Signals
-------------

	component C16 is
	port
	(
		CLK28 		: 	in 	std_logic;
		RESET			: 	in 	std_logic;
		HSYNC			:	out	std_logic;
		VSYNC			:	out	std_logic;
		RED			:	out	std_logic_vector(3 downto 0);
		GREEN			:	out	std_logic_vector(3 downto 0);
		BLUE			:	out	std_logic_vector(3 downto 0);
		RAS			:	out	std_logic;
		CAS			:	out	std_logic;
		RW				:	out	std_logic;
		A				:	out	std_logic_vector(7 downto 0);
		D				:	inout	std_logic_vector(7 downto 0);
		PS2DAT 		: 	in 	std_logic;
		PS2CLK 		: 	in 	std_logic;
		IEC_DATAOUT	:	out	std_logic;
		IEC_DATAIN 	: 	in 	std_logic;
		IEC_CLKOUT	:	out	std_logic;
		IEC_CLKIN 	: 	in 	std_logic;
		IEC_ATNOUT	:	out	std_logic;
		IEC_RESET	:	out	std_logic;
		AUDIO_L		:	out	std_logic;
		AUDIO_R		:	out	std_logic;
		RS232_TX		:	out	std_logic;
		RGBS			:	out	std_logic
	);
	end component;


signal clock_s      	:   std_logic;
signal reset_n      	:   std_logic;

signal vga_r_s       :   std_logic_vector(3 downto 0);
signal vga_g_s       :   std_logic_vector(3 downto 0);
signal vga_b_s       :   std_logic_vector(3 downto 0);
signal vga_hs_s      :   std_logic;
signal vga_vs_s      :   std_logic;

signal c16_a_s      	:   std_logic_vector(7 downto 0);
signal c16_d_s      	:   std_logic_vector(7 downto 0);
signal c16_ras_s     :   std_logic;
signal c16_cas_s     :   std_logic;
signal c16_rw_s      :   std_logic;
signal c16_a_hi_s    :   std_logic_vector(7 downto 0);
signal c16_a_low_s   :   std_logic_vector(7 downto 0);

begin

	C16_inst : C16 
	port map
	(
		CLK28 			=> clock_s,
		RESET				=> reset_n,
		HSYNC				=> vga_hs_s,
		VSYNC				=>	vga_vs_s,	
		RED				=>	vga_r_s,	
		GREEN				=> vga_g_s,
		BLUE				=> vga_b_s,
		RAS				=> c16_ras_s,
		CAS				=> c16_cas_s,
		RW					=> c16_rw_s,
		A					=> c16_a_s,
		D					=> SRAM_DQ(7 downto 0),
		PS2DAT 			=> PS2_DAT,
		PS2CLK 			=> PS2_CLK,
		IEC_DATAOUT		=> open,
		IEC_DATAIN 		=> '0',
		IEC_CLKOUT		=> open,
		IEC_CLKIN 		=> '0',
		IEC_ATNOUT		=> open,
		IEC_RESET		=> open,
		AUDIO_L			=> open,
		AUDIO_R			=> open,
		RS232_TX			=> open,
		RGBS				=> open
	);


	pll: entity work.pll1
	port map
   (
			inclk0 => CLOCK_50,
			c0 => clock_s --28
	 );
	 
	 
	 
	 -- latch dram address
	process (c16_ras_s)
	begin
		if falling_edge (c16_ras_s) then
			c16_a_low_s <= c16_a_s;
		end if;
	end process;
	
	process (c16_cas_s)
	begin
		if falling_edge (c16_cas_s) then
			c16_a_hi_s <= c16_a_s;
		end if;
	end process;
	 
	 
	 
	 
	 reset_n <= not key(0);
	 
	 VGA_R 	<= vga_r_s;
	 VGA_G 	<= vga_g_s;
	 VGA_B 	<= vga_b_s;
	 VGA_HS 	<= vga_hs_s;
	 VGA_VS 	<= vga_vs_s;


	SRAM_ADDR <= "00" & c16_a_hi_s & c16_a_low_s;
	SRAM_UB_N <= '1';		-- UB = 0 ativa bits 15..8
	SRAM_LB_N <= '0';		-- LB = 0 ativa bits 7..0
	
	SRAM_WE_N <= c16_cas_s or c16_rw_s;
	SRAM_OE_N <= c16_cas_s or (not c16_rw_s);
	SRAM_CE_N <= '0';
	
	---------------------------------------------------------------
	
	
	
end architecture;
