
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
  
-- Generic top-level for Altera DE1 board
entity de1_top is
	generic (
		usar_sdram		: boolean	:= FALSE
	);
	port (
		-- Clocks
		CLOCK_24       : in    std_logic_vector(1 downto 0);
		CLOCK_27       : in    std_logic_vector(1 downto 0);
		CLOCK_50       : in    std_logic;
		EXT_CLOCK      : in    std_logic;

		-- Switches
		SW             : in    std_logic_vector(9 downto 0);
		-- Buttons
		KEY            : in    std_logic_vector(3 downto 0);

		-- 7 segment displays
		HEX0           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX1           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX2           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX3           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		-- Red LEDs
		LEDR           : out   std_logic_vector(9 downto 0)		:= (others => '0');
		-- Green LEDs
		LEDG           : out   std_logic_vector(7 downto 0)		:= (others => '0');

		-- VGA
		VGA_R          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_G          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_B          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_HS         : out   std_logic									:= '0';
		VGA_VS         : out   std_logic									:= '0';

		-- Serial
		UART_RXD       : in    std_logic;
		UART_TXD       : out   std_logic									:= '0';

		-- PS/2 Keyboard
		PS2_CLK        : inout std_logic									:= '1';
		PS2_DAT        : inout std_logic									:= '1';

		-- I2C
		I2C_SCLK       : inout std_logic									:= '1';
		I2C_SDAT       : inout std_logic									:= '1';

		-- Audio
		AUD_XCK        : out   std_logic									:= '0';
		AUD_BCLK       : out   std_logic									:= '0';
		AUD_ADCLRCK    : out   std_logic									:= '0';
		AUD_ADCDAT     : in    std_logic;
		AUD_DACLRCK    : out   std_logic									:= '0';
		AUD_DACDAT     : out   std_logic									:= '0';

		-- SRAM
		SRAM_ADDR      : out   std_logic_vector(17 downto 0)		:= (others => '0');
		SRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => '0');
		SRAM_CE_N      : out   std_logic									:= '1';
		SRAM_OE_N      : out   std_logic									:= '1';
		SRAM_WE_N      : out   std_logic									:= '1';
		SRAM_UB_N      : out   std_logic									:= '1';
		SRAM_LB_N      : out   std_logic									:= '1';

		-- SDRAM
		DRAM_ADDR      : out   std_logic_vector(11 downto 0)		:= (others => '0');
		DRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => '0');
		DRAM_BA_0      : out   std_logic									:= '1';
		DRAM_BA_1      : out   std_logic									:= '1';
		DRAM_CAS_N     : out   std_logic									:= '1';
		DRAM_CKE       : out   std_logic									:= '1';
		DRAM_CLK       : out   std_logic									:= '1';
		DRAM_CS_N      : out   std_logic									:= '1';
		DRAM_LDQM      : out   std_logic									:= '1';
		DRAM_RAS_N     : out   std_logic									:= '1';
		DRAM_UDQM      : out   std_logic									:= '1';
		DRAM_WE_N      : out   std_logic									:= '1';

		-- Flash
		FL_ADDR        : out   std_logic_vector(21 downto 0)		:= (others => '0');
		FL_DQ          : inout std_logic_vector(7 downto 0)		:= (others => '0');
		FL_RST_N       : out   std_logic									:= '1';
		FL_OE_N        : out   std_logic									:= '1';
		FL_WE_N        : out   std_logic									:= '1';
		FL_CE_N        : out   std_logic									:= '1';

		-- SD card (SPI mode)
		SD_nCS         : out   std_logic									:= '1';
		SD_MOSI        : out   std_logic									:= '1';
		SD_SCLK        : out   std_logic									:= '1';
		SD_MISO        : in    std_logic;

		-- GPIO
		GPIO_0         : inout std_logic_vector(35 downto 0)		:= (others => '0');
		GPIO_1         : inout std_logic_vector(35 downto 0)		:= (others => '0')
	);
end entity;


architecture behavior of de1_top is

	-- Resets
	signal pll_locked_s		: std_logic;
	signal reset_s				: std_logic;
	signal soft_reset_s		: std_logic;
	signal por_n_s				: std_logic;

	-- Clocks
	signal clock_master_s	: std_logic;
	signal clock_mem_s		: std_logic;
	signal clock_sdram_s		: std_logic;
	signal clk_cnt_q			: unsigned(1 downto 0);
	signal clk_en_10m7_q		: std_logic;
	signal clk_en_5m37_q		: std_logic;

	-- SRAM
	signal sram_addr_s		: std_logic_vector(18 downto 0);		-- 512K
	signal sram_data_o_s		: std_logic_vector(7 downto 0);
	signal sram_ce_s			: std_logic;
	signal sram_oe_s			: std_logic;
	signal sram_we_s			: std_logic;

	-- ROM bios e loader
	signal bios_loader_s		: std_logic;
	signal bios_addr_s		: std_logic_vector(12 downto 0);		-- 8K
	signal bios_data_s		: std_logic_vector(7 downto 0);
	signal loader_data_s		: std_logic_vector(7 downto 0);
	signal bios_ce_s			: std_logic;
	signal bios_oe_s			: std_logic;
	signal bios_we_s			: std_logic;

	-- Cartridge
	signal cart_multcart_s	: std_logic;
	signal cart_addr_s		: std_logic_vector(14 downto 0);		-- 32K
	signal cart_do_s			: std_logic_vector(7 downto 0);
	signal cart_oe_s			: std_logic;
	signal cart_ce_s			: std_logic;
	signal cart_we_s			: std_logic;
	signal cart_en_80_n_s	: std_logic;
	signal cart_en_A0_n_s	: std_logic;
	signal cart_en_C0_n_s	: std_logic;
	signal cart_en_E0_n_s	: std_logic;

	-- RAM memory
	signal ram_addr_s			: std_logic_vector(12 downto 0);		-- 8K
	signal ram_mirr_addr_s	: std_logic_vector(9 downto 0);		-- 1K (mirrored)
	signal ram_do_s			: std_logic_vector(7 downto 0);
	signal ram_di_s			: std_logic_vector(7 downto 0);
	signal ram_ce_s			: std_logic;
	signal ram_oe_s			: std_logic;
	signal ram_we_s			: std_logic;

	-- VRAM memory
	signal vram_addr_s		: std_logic_vector(13 downto 0);		-- 16K
	signal vram_do_s			: std_logic_vector(7 downto 0);
	signal vram_di_s			: std_logic_vector(7 downto 0);
	signal vram_ce_s			: std_logic;
	signal vram_oe_s			: std_logic;
	signal vram_we_s			: std_logic;

	-- Audio
	signal audio_s				: std_logic_vector(7 downto 0);

	-- Video
	signal btn_dblscan_s		: std_logic;
	signal dblscan_en_s		: std_logic;
	signal rgb_col_s			: std_logic_vector( 3 downto 0);		-- 15KHz
	signal rgb_hsync_n_s		: std_logic;								-- 15KHz
	signal rgb_vsync_n_s		: std_logic;								-- 15KHz
	signal vga_col_s			: std_logic_vector( 3 downto 0);		-- 31KHz
	signal vga_hsync_n_s		: std_logic;								-- 31KHz
	signal vga_vsync_n_s		: std_logic;								-- 31KHz

	-- Keyboard
	signal ps2_keys_s			: std_logic_vector(15 downto 0);
	signal ps2_joy_s			: std_logic_vector(15 downto 0);

	-- Controller
	signal ctrl_p1_s			: std_logic_vector( 2 downto 1);
	signal ctrl_p2_s			: std_logic_vector( 2 downto 1);
	signal ctrl_p3_s			: std_logic_vector( 2 downto 1);
	signal ctrl_p4_s			: std_logic_vector( 2 downto 1);
	signal ctrl_p5_s			: std_logic_vector( 2 downto 1);
	signal ctrl_p6_s			: std_logic_vector( 2 downto 1);
	signal ctrl_p7_s			: std_logic_vector( 2 downto 1);
	signal ctrl_p8_s			: std_logic_vector( 2 downto 1);
	signal ctrl_p9_s			: std_logic_vector( 2 downto 1);
	signal but_up_s			: std_logic;
	signal but_down_s			: std_logic;
	signal but_left_s			: std_logic;
	signal but_right_s		: std_logic;
	signal but_a_s				: std_logic;
	signal but_b_s				: std_logic;
	signal but_c_s				: std_logic;
	signal but_x_s				: std_logic;
	signal but_y_s				: std_logic;
	signal but_z_s				: std_logic;
	signal but_start_s		: std_logic;
	signal but_mode_s			: std_logic;

	-- SD
	signal spi_cs_s			: std_logic;
	signal spi_data_in_s		: std_logic_vector(7 downto 0);
	signal spi_data_out_s	: std_logic_vector(7 downto 0);

	-- Debug
	signal D_display			: std_logic_vector(15 downto 0);
	signal D_cpu_addr			: std_logic_vector(15 downto 0);


	
	signal video_r_s			: std_logic_vector( 3 downto 0);
	signal video_g_s			: std_logic_vector( 3 downto 0);
	signal video_b_s			: std_logic_vector( 3 downto 0);
	signal video_hsync_s		: std_logic;
	signal video_vsync_s		: std_logic;
	signal video_hsync_n_s	: std_logic;
	signal video_vsync_n_s	: std_logic;
	signal video_blank_s		: std_logic;
	
	 signal clk_4              : std_logic;
    signal clk_cnt            : std_logic_vector(2 downto 0) := "000";

begin

	-- PLL
	pll_1: entity work.pll1
	port map (
		inclk0	=> CLOCK_50,
		c0			=> clock_master_s,		-- 35.44 MHz
		locked	=> pll_locked_s
	);

 vic : work.VIC20_VIC
    port map (
      RW_L            => '0',

      ADDR_IN         => (others=>'0'),
      ADDR_OUT        => open,

      DATA_OE_OUT_L   => open,
      DATA_IN         => (others=>'0'),
      DATA_OUT        => open,
      --
      AUDIO_OUT       => open,

      VIDEO_R_OUT     => video_r_s,
      VIDEO_G_OUT     => video_g_s,
      VIDEO_B_OUT     => video_b_s,

      HSYNC_OUT       => video_hsync_s,
      VSYNC_OUT       => video_vsync_s,
      COMP_SYNC_L_OUT => open,
		VIDEO_BLANK		 => video_blank_s,
      --
      --
      LIGHT_PEN_IN    => '0',
      POTX_IN         => '0',
      POTY_IN         => '0',

      ENA_1MHZ        => open,
      P2_H            => open,
      CLK_4           => clk_4
      );

	video_hsync_n_s <= not video_hsync_s;
	 video_vsync_n_s <= not video_vsync_s;
	 
	VGA_R <= video_r_s ;
	VGA_G <= video_g_s ;
	VGA_B <= video_b_s ;
	VGA_HS <= video_hsync_n_s;
	VGA_VS <= video_vsync_n_s;
	
	process (clock_master_s)
  begin
		if rising_edge(clock_master_s) then
			clk_cnt <= clk_cnt + '1';
		end if;
  end process;
  
  clk_4 <= clk_cnt(2);




	--D_display		<= "00000000" & std_logic_vector(audio_s);
	D_display	<= (others=>'0');

	ld3: entity work.seg7
	port map(
		D		=> D_display(15 downto 12),
		Q		=> HEX3
	);

	ld2: entity work.seg7
	port map(
		D		=> D_display(11 downto 8),
		Q		=> HEX2
	);

	ld1: entity work.seg7
	port map(
		D		=> D_display(7 downto 4),
		Q		=> HEX1
	);

	ld0: entity work.seg7
	port map(
		D		=> D_display(3 downto 0),
		Q		=> HEX0
	);

end architecture;