
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity multicore_top is
	port (
	-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= '0'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
	);
end entity;

architecture datapath of multicore_top is


  
  signal CLK_28M, CLK_14M, CLK_2M, PRE_PHASE_ZERO : std_logic;
  signal IO_SELECT, DEVICE_SELECT : std_logic_vector(7 downto 0);
  signal ADDR : unsigned(15 downto 0);
  signal D, PD : unsigned(7 downto 0);


  signal VIDEO, HBL, VBL, LD194 : std_logic;
  signal COLOR_LINE : std_logic;
  signal COLOR_LINE_CONTROL : std_logic;
  signal GAMEPORT : std_logic_vector(7 downto 0);
  signal cpu_pc : unsigned(15 downto 0);

  signal K : unsigned(7 downto 0);
  signal read_key : std_logic;

  signal flash_clk : unsigned(22 downto 0) := (others => '0');
  signal power_on_reset : std_logic := '1';
  signal reset : std_logic;
	signal hard_reset :  std_logic := '0';
  
  signal speaker : std_logic;
  signal audio_l_s : std_logic;
  signal audio_r_s : std_logic;

  
  

  signal track : unsigned(5 downto 0);
  signal image : unsigned(9 downto 0) := (others=>'0');
  signal trackmsb : unsigned(3 downto 0);
  signal D1_ACTIVE, D2_ACTIVE : std_logic;
  signal track_addr : unsigned(13 downto 0);
  signal TRACK_RAM_ADDR : unsigned(13 downto 0);
  signal tra : unsigned(15 downto 0);
  signal TRACK_RAM_DI : unsigned(7 downto 0);
  signal TRACK_RAM_WE : std_logic;
  signal R_10 : unsigned(9 downto 0);
  signal G_10 : unsigned(9 downto 0);
  signal B_10 : unsigned(9 downto 0);

  signal CS_N, MOSI, MISO, SCLK : std_logic;
  
  --ram
  signal ram_addr : std_logic_vector(17 downto 0) := (OTHERS=>'0');
  signal ram_we : std_logic;
  
  signal btn_reset_s : std_logic;
  signal clean_r_vector : std_logic := '0';
  
  --video output
   signal vga_hsync_n_s : std_logic;
	signal vga_vsync_n_s : std_logic;
	signal vga_blank_s : std_logic;
  
  --OSD
   signal CLK_OSD				: std_logic;
	signal osd_visible_s		: std_logic := '1';
   signal S_osd_pixel		: std_logic;
   signal S_osd_green		: std_logic_vector(2 downto 0); -- OSD byte signal
   signal btn_up_s			: std_logic := '1'; 
   signal btn_down_s			: std_logic := '1'; 
	signal btn_up_state_s 	: std_logic;
	signal btn_down_state_s : std_logic;
	signal timer_osd_s		: unsigned(21 downto 0) := (OTHERS=>'1');

  
begin

  reset <=  (not (btn_n_i(3))) or hard_reset or power_on_reset;

  hard_reset <= (not (btn_n_i(3) or btn_n_i(4)));
  
  power_on : process(CLK_14M)
  begin
    if rising_edge(CLK_14M) then
      if flash_clk(22) = '1' then
        power_on_reset <= '0';
      end if;
    end if;
  end process;

  -- In the Apple ][, this was a 555 timer
  flash_clkgen : process (CLK_14M)
  begin
    if rising_edge(CLK_14M) then
      flash_clk <= flash_clk + 1;
    end if;     
  end process;

  -- Use a PLL to divide the 50 MHz down to 28 MHz and 14 MHz
  pll : work.pll port map (
    inclk0 => clock_50_i,
    c0     => CLK_28M,
    c1     => CLK_14M
	-- c2	  => CLK_OSD
    );

  -- Paddle buttons
--  GAMEPORT <=  "0000" & (not KEY(2 downto 0)) & "0";
 GAMEPORT <=  "00000000";

  COLOR_LINE_CONTROL <= COLOR_LINE;-- and SW(9);  -- Color or B&W mode
  
  core : entity work.apple2 port map (
    CLK_14M        => CLK_14M,
    CLK_2M         => CLK_2M,
    PRE_PHASE_ZERO => PRE_PHASE_ZERO,
    FLASH_CLK      => flash_clk(22),
    reset          => reset,
    ADDR           => ADDR,
    ram_addr       => ram_addr,
    D              => D,
    ram_do         => sram_data_io(7 downto 0),
    PD             => PD,
    ram_we         => ram_we,
    VIDEO          => VIDEO,
    COLOR_LINE     => COLOR_LINE,
    HBL            => HBL,
    VBL            => VBL,
    LD194          => LD194,
    K              => K,
    read_key       => read_key,
    AN             => open, --LEDG(7 downto 4),
    GAMEPORT       => GAMEPORT,
    IO_SELECT      => IO_SELECT,
    DEVICE_SELECT  => DEVICE_SELECT,
    pcDebugOut     => cpu_pc,
	 laudio			 => audio_l_s,
	 raudio			 => audio_r_s,
	 mb_enabled		 => '1',
    speaker        => speaker
    );
	 
	 dac_l_o <= audio_l_s or speaker;
	 dac_r_o <= audio_r_s or speaker;
	 

  vga : entity work.vga_controller port map (
    CLK_28M    => CLK_28M,
    VIDEO      => VIDEO,
    COLOR_LINE => COLOR_LINE_CONTROL,
	 SCREEN_MODE=> "00", -- 00: Color, 01: B&W, 10: Green, 11: Amber
    HBL        => HBL,
    VBL        => VBL,
    LD194      => LD194,
    VGA_HS     => vga_hsync_n_s,
    VGA_VS     => vga_vsync_n_s,
    VGA_R      => R_10,
    VGA_G      => G_10,
    VGA_B      => B_10,
	 VGA_BLANK  => vga_blank_s
    );



  keyboard : entity work.keyboard port map (
    PS2_Clk  => ps2_clk_io,
    PS2_Data => ps2_data_io,
    CLK_14M  => CLK_14M,
    reset    => reset,
    read     => read_key,
    K        => K
    );

  disk : entity work.disk_ii port map (
    CLK_14M        => CLK_14M,
    CLK_2M         => CLK_2M,
    PRE_PHASE_ZERO => PRE_PHASE_ZERO,
    IO_SELECT      => IO_SELECT(6),
    DEVICE_SELECT  => DEVICE_SELECT(6),
    RESET          => reset,
    A              => ADDR,
    D_IN           => D,
    D_OUT          => PD,
    TRACK          => TRACK,
    TRACK_ADDR     => TRACK_ADDR,
    D1_ACTIVE      => D1_ACTIVE,
    D2_ACTIVE      => D2_ACTIVE,
    ram_write_addr => TRACK_RAM_ADDR,
    ram_di         => TRACK_RAM_DI,
    ram_we         => TRACK_RAM_WE
    );

  sdcard_interface : entity work.spi_controller port map (
    CLK_14M        => CLK_14M,
    RESET          => RESET,

    CS_N           => CS_N,
    MOSI           => MOSI,
    MISO           => MISO,
    SCLK           => SCLK,
    
    track          => TRACK,
    image          => image,
    
    ram_write_addr => TRACK_RAM_ADDR,
    ram_di         => TRACK_RAM_DI,
    ram_we         => TRACK_RAM_WE
    );


	 
	 
 -- OSD overlay for the green channel
  I_osd: entity work.osd
  generic map -- workaround for wrong video size
  (
		C_digits => 3, -- number of hex digits to show
		C_resolution_x => 565
  )
  port map
  (
    clk_pixel => CLK_28M,
    vsync => not vga_vsync_n_s, --positive sync
    fetch_next => vga_blank_s, -- '1' when video_active
    probe_in(9 downto 0) =>  std_logic_vector(image),
    osd_out => S_osd_pixel
  );
  S_osd_green <= (others => (S_osd_pixel  and osd_visible_s));
	 
	 
	 
	VGA_R_o <= std_logic_vector(R_10(9 downto 5));
	VGA_G_o <= std_logic_vector(G_10(9 downto 5)) or S_osd_green;
	VGA_B_o <= std_logic_vector(B_10(9 downto 5));
	vga_hsync_n_o <= vga_hsync_n_s;
	vga_vsync_n_o <= vga_vsync_n_s;
	 


  sd_cs_n_o <= CS_N;
  sd_mosi_o  <= MOSI;
  MISO  <= sd_miso_i;
  sd_sclk_o  <= SCLK;
 

 
 	-- Audio
--	audioout: entity work.dac
--	generic map (
--		msbi_g		=> 7
--	)
--	port map (
--		clk_i		=> CLK_28M,
--		res_i		=> reset,
--		dac_i		=> speaker & "0000000",
--		dac_o		=> audio_dac_s
--	);
--	
--	dac_l_o	<= audio_dac_s;
--	dac_r_o	<= audio_dac_s;


  -- Current disk track on right two digits 
  trackmsb <= "00" & track(5 downto 4);
 -- digit0 : entity work.hex7seg port map (track(3 downto 0), HEX0);
 -- digit1 : entity work.hex7seg port map (trackmsb, HEX1);

  -- Current disk image on left two digits
 -- digit2 : entity work.hex7seg port map (image(3 downto 0), HEX2);
 -- digit3 : entity work.hex7seg port map (image(7 downto 4), HEX3);

 
  
  
  --Limpa o reset vector no endereço 0x03f4
  process (hard_reset)
	begin
		if hard_reset = '1' then
		
			sram_addr_o <= "000" & X"03f4";
			sram_data_io(7 downto 0) <= (others => '0');
		  sram_we_n_o <= '0';
		  sram_oe_n_o <= '0';
			
		else
			sram_addr_o <= "0" & ram_addr;
			
			if ram_we = '1' then
				sram_data_io(7 downto 0) <= std_logic_vector(D);  
			else 
				sram_data_io(7 downto 0) <=  (others => 'Z');
			end if;
			
		  sram_we_n_o <= not ram_we;
		  sram_oe_n_o <= ram_we;

		end if;
	end process;

  
 	btndw: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> CLK_14M,
		button_i			=> btn_n_i(2),
		result_o			=> btn_up_s
	);
	
	btnup: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> CLK_14M,
		button_i			=> btn_n_i(1),
		result_o			=> btn_down_s
	);


	-- dectect falling edge of the buttons
	process (CLK_14M)
	begin
		if rising_edge(CLK_14M) then  
            if (btn_up_s = '0' and btn_up_state_s ='1' and btn_down_s = '1') then
                 image <= image + 1;   
            elsif (btn_down_s = '0' and btn_down_state_s = '1' and btn_up_s = '1') then  
                image <= image - 1;   
           end if;
          btn_up_state_s <= btn_up_s;
          btn_down_state_s <= btn_down_s;
       end if;  
	end process;

	
	process (CLK_2M, btn_up_s,btn_down_s )
	begin
		if rising_edge(CLK_2M) then  
			if btn_up_s = '0' or btn_down_s = '0' then
				timer_osd_s <= (others=>'1');
				osd_visible_s <= '1';
         elsif timer_osd_s > 0 then
				timer_osd_s <= timer_osd_s - 1;
				osd_visible_s <= '1';
			else
				osd_visible_s <= '0';
        end if;
      end if;  
	end process;

	
	
	

--  -- Decode the PC on the red LEDs
--  LEDR(9) <= cpu_pc(15);
--  LEDR(8) <= cpu_pc(14);
--  LEDR(7) <= cpu_pc(13);
--  LEDR(6) <= cpu_pc(12);
--  LEDR(5) <= cpu_pc(11);
--  LEDR(4) <= cpu_pc(10);
--  LEDR(3) <= cpu_pc(9);
--  LEDR(2) <= cpu_pc(8);
--  LEDR(1) <= cpu_pc(7);
--  LEDR(0) <= cpu_pc(6);
--
--  -- LEDG(7 downto 4) are AN outputs
--  LEDG(3) <= '0';
--  LEDG(2) <= D2_ACTIVE;
--  LEDG(1) <= D1_ACTIVE;
--  LEDG(0) <= speaker;
--  
--  UART_TXD <= '0';
--  DRAM_ADDR <= (others => '0');
--  DRAM_LDQM <= '0';
--  DRAM_UDQM <= '0';
--  DRAM_WE_N <= '1';
--  DRAM_CAS_N <= '1';
--  DRAM_RAS_N <= '1';
--  DRAM_CS_N <= '1';
--  DRAM_BA_0 <= '0';
--  DRAM_BA_1 <= '0';
--  DRAM_CLK <= '0';
--  DRAM_CKE <= '0';
--  FL_ADDR <= (others => '0');
--  FL_WE_N <= '1';
--  FL_RST_N <= '0';
--  FL_OE_N <= '1';
--  FL_CE_N <= '1';
--
--  TDO <= '0';
--
--  -- Set all other unused bidirectional ports to tri-state
--  DRAM_DQ     <= (others => 'Z');
--  FL_DQ       <= (others => 'Z');
--  SRAM_DQ(15 downto 8) <= (others => 'Z');
--  GPIO_0      <= (others => 'Z');
--  GPIO_1      <= (others => 'Z');

end datapath;
