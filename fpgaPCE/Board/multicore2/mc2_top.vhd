
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity top is
	port (
		-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= '0'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
	);
end entity;

architecture Behavior of top is

	-- Sigma Delta audio
	COMPONENT hybrid_pwm_sd
		PORT
		(
			clk		:	 IN STD_LOGIC;
			n_reset		:	 IN STD_LOGIC;
			din		:	 IN STD_LOGIC_VECTOR(15 DOWNTO 0);
			dout		:	 OUT STD_LOGIC
		);
	END COMPONENT;


	----------------------------------------
	-- internal signals                   --
	----------------------------------------

	-- clock
	signal pll_in_clk : std_logic;
	signal clk_114 	: std_logic;
	signal clk_28 		: std_logic;
	signal pll_locked : std_logic;
	signal clk_7 		: std_logic;
	signal c1 			: std_logic;
	signal c3 			: std_logic;
	signal cck 			: std_logic;
	signal eclk 		: std_logic_vector (10-1 downto 0);
	signal clk_50 		: std_logic;
	signal sysclk 		: std_logic;
	signal memclk 		: std_logic;
	signal clk_vga 		: std_logic;
 
	-- reset
	signal pll_rst 	: std_logic;
	signal sdctl_rst 	: std_logic;
	signal rst_50 		: std_logic;
	signal rst_minimig: std_logic;
 
	-- ctrl 
	signal rom_status : std_logic;
	signal ram_status : std_logic;
	signal reg_status : std_logic;
 
	-- tg68
	signal tg68_rst	 	: std_logic;
	signal tg68_dat_in 	: std_logic_vector (16-1 downto 0);
	signal tg68_dat_out 	: std_logic_vector (16-1 downto 0);
	signal tg68_adr 		: std_logic_vector (32-1 downto 0);
	signal tg68_IPL 		: std_logic_vector (3-1 downto 0);
	signal tg68_dtack 	: std_logic;
	signal tg68_as 		: std_logic;
	signal tg68_uds 		: std_logic;
	signal tg68_lds 		: std_logic;
	signal tg68_rw 		: std_logic;
	signal tg68_ena7RD 	: std_logic;
	signal tg68_ena7WR 	: std_logic;
	signal tg68_enaWR 	: std_logic;
	signal tg68_cout 		: std_logic_vector (16-1 downto 0);
	signal tg68_cpuena 	: std_logic;
	signal cpu_config 	: std_logic_vector (2-1 downto 0);
	signal memcfg 			: std_logic_vector (6-1 downto 0);
	signal tg68_cad 		: std_logic_vector (32-1 downto 0);
	signal tg68_cpustate : std_logic_vector (6-1 downto 0);
	signal tg68_cdma 		: std_logic;
	signal tg68_clds 		: std_logic;
	signal tg68_cuds 		: std_logic;
 
	-- minimig
	signal ram_data 		: std_logic_vector (16-1 downto 0);      -- sram data bus
	signal ramdata_in 	: std_logic_vector (16-1 downto 0);    -- sram data bus in
	signal ram_address 	: std_logic_vector (22-1 downto 0);   -- sram address bus
	signal ram_bhe 		: std_logic;      							-- sram upper byte select
	signal ram_ble 		: std_logic;      							-- sram lower byte select
	signal ram_we 			: std_logic;      							 -- sram write enable
	signal ram_oe 			: std_logic;      						 -- sram output enable
	signal s15khz 			: std_logic;      						  -- scandoubler disable
	signal joy_emu_en 	: std_logic;   							 -- joystick emulation enable
	signal sdo 				: std_logic;         					  -- SPI data output
	signal ldata 			: std_logic_vector (15-1 downto 0);   -- left DAC data
	signal rdata 			: std_logic_vector (15-1 downto 0);         -- right DAC data
	signal audio_left 	: std_logic_vector (15 downto 0);
	signal audio_right 	: std_logic_vector (15 downto 0);
	signal floppy_fwr 	: std_logic;
	signal floppy_frd 	: std_logic;
	signal hd_fwr 			: std_logic;
	signal hd_frd 			: std_logic;
 
	-- sdram
	signal reset_out	: std_logic;
	signal sdram_cs 	: std_logic_vector (4-1 downto 0);
	signal sdram_dqm 	: std_logic_vector (2-1 downto 0);
	signal sdram_ba 	: std_logic_vector (2-1 downto 0);
 
	-- audio
	signal audio_lr_switch	: std_logic;
	signal audio_lr_mix		: std_logic;
	
	signal audio_l 			: signed(15 downto 0);
	signal audio_r 			: signed(15 downto 0);
 
	-- ctrl
	signal SRAM_DAT_W		: std_logic_vector (16-1 downto 0);
	signal SRAM_DAT_R		: std_logic_vector (16-1 downto 0);
	signal FL_DAT_W		: std_logic_vector (8-1 downto 0);
	signal FL_DAT_R		: std_logic_vector (8-1 downto 0);
	signal SPI_CS_N		: std_logic_vector (4-1 downto 0);
	signal SPI_DI			: std_logic;
	signal rst_ext			: std_logic;
	signal boot_sel		: std_logic;
	signal ctrl_cfg		: std_logic_vector (4-1 downto 0);
	signal ctrl_status	: std_logic_vector (4-1 downto 0);

	-- indicators
	signal track	: std_logic_vector (8-1 downto 0);
 
	--audio
	signal audio_LR	: std_logic;
 
	signal vga_red		: unsigned (7 downto 0);
	signal vga_green	: unsigned (7 downto 0);
	signal vga_blue	: unsigned (7 downto 0);	
	signal vga_hsync_n_s : std_logic;
	signal vga_vsync_n_s : std_logic;
	signal vga_blank_s : std_logic;
		
	signal lpf1_wave_L	: std_logic_vector (15 downto 0);
	signal lpf5_wave_L	: std_logic_vector (15 downto 0);
	signal lpf1_wave_R	: std_logic_vector (15 downto 0);
	signal lpf5_wave_R	: std_logic_vector (15 downto 0);

	-- PS/2 keyboard
	signal PS2K_DAT_IN		: std_logic;
	signal PS2K_CLK_IN		: std_logic;
	signal PS2K_DAT_OUT		: std_logic;
	signal PS2K_CLK_OUT		: std_logic;
 
	-- PS/2 Mouse
	signal PS2M_DAT_IN		: std_logic;
	signal PS2M_CLK_IN		: std_logic;
	signal PS2M_DAT_OUT		: std_logic;
	signal PS2M_CLK_OUT		: std_logic;
	
	-- joystick
	signal joy1_s			: std_logic_vector(11 downto 0) := (others => '1'); -- MS ZYX CBA RLDU	
	signal joy2_s			: std_logic_vector(11 downto 0) := (others => '1'); -- MS ZYX CBA RLDU	
	signal joyP7_s			: std_logic;
	



begin

PS2K_DAT_IN <= ps2_data_io;
PS2K_CLK_IN <= ps2_clk_io;
PS2M_DAT_IN <= ps2_mouse_data_io;
PS2M_CLK_IN <= ps2_mouse_clk_io;

ps2_data_io 		<= '0' when (PS2K_DAT_OUT = '0') else 'Z'; 
ps2_clk_io 			<= '0' when (PS2K_CLK_OUT = '0') else 'Z'; 
ps2_mouse_data_io <= '0' when (PS2M_DAT_OUT = '0') else 'Z'; 
ps2_mouse_clk_io 	<= '0' when (PS2M_CLK_OUT = '0') else 'Z'; 

--vga_r_o <= vga_red(7 downto 4);
--vga_g_o <= vga_green(7 downto 4);
--vga_b_o <= vga_blue(7 downto 4);
vga_vsync_n_o <= vga_vsync_n_s;
vga_hsync_n_o <= vga_hsync_n_s;



	U00: work.pll 
	port map 
	(
		inclk0		=> clock_50_i,		-- external Clock 50 MHz 
		c0				=> sysclk,			-- 41.667
		c1				=> memclk,			-- 125 mhz
		c2				=> sdram_clk_o,	-- 125 mhz shifted
		c3				=> clk_vga			-- 25 mhz
	);
	


--	u_interpo_L: work.INTERPO 
--	generic map
--	(
--		msbi => 15
--	)
--	port map 
--	(
--		clk21m  => sysclk,
--		reset   => '0',
--		clkena  => '1',
--		idata   => "00" & audio_left(15 downto 2),
--		odata   => lpf1_wave_L
--	);
--	
--	u_lpf2_L: work.lpf2 
--	generic map
--	(
--		msbi => 15
--	)
--	port map 
--	(
--		clk21m  => sysclk,
--		reset   => '0',
--		clkena  => '1',
--		idata   => lpf1_wave_L,
--		odata   => lpf5_wave_L
--	);
--
--	u_interpo_R: work.INTERPO 
--	generic map
--	(
--		msbi => 15
--	)
--	port map 
--	(
--		clk21m  => sysclk,
--		reset   => '0',
--		clkena  => '1',
--		idata   => "00" & audio_right(15 downto 2),
--		odata   => lpf1_wave_R
--	);
--	
--	u_lpf2_R: work.lpf2 
--	generic map
--	(
--		msbi => 15
--	)
--	port map 
--	(
--		clk21m  => sysclk,
--		reset   => '0',
--		clkena  => '1',
--		idata   => lpf1_wave_R,
--		odata   => lpf5_wave_R
--	);
--
--
--	dacL: work.dac 
--	generic map
--	(
--		msbi_g => 15
--	)
--	port map 
--	(
--		clk_i  	=> sysclk,
--		res_n_i  => '1',
--		dac_i   	=> lpf5_wave_L,
--		dac_o   	=> dac_l_o
--	);
--	 
--	dacR: work.dac 
--	generic map
--	(
--		msbi_g => 15
--	)
--	port map 
--	(
--		clk_i  	=> sysclk,
--		res_n_i  => '1',
--		dac_i   	=> lpf5_wave_R,
--		dac_o   	=> dac_r_o
--	);


	myvt: work.Virtual_Toplevel 
	generic map
	(
		rowAddrBits => 13,
		colAddrBits => 9
	)
	port map 
	(
		reset => btn_n_i(1),
		CLK => sysclk,
		SDR_CLK => memclk,
		
		--SW => SW,
		
		DRAM_ADDR => sdram_ad_o,
		DRAM_DQ => sdram_da_io,
		DRAM_BA_1 => sdram_ba_o(1),
		DRAM_BA_0 => sdram_ba_o(0),
		DRAM_CKE => sdram_cke_o,
		DRAM_UDQM => sdram_dqm_o(1),
		DRAM_LDQM => sdram_dqm_o(0),
		DRAM_CS_N => sdram_cs_o,
		DRAM_WE_N => sdram_we_o,
		DRAM_CAS_N => sdram_cas_o,
		DRAM_RAS_N => sdram_ras_o,
		
--		DAC_LDATA => audio_left,
--		DAC_RDATA => audio_right,
		signed(DAC_LDATA) => audio_l,
		signed(DAC_RDATA) => audio_r,
		
		unsigned(VGA_R) => vga_red,
		unsigned(VGA_G) => vga_green,
		unsigned(VGA_B) => vga_blue,
		VGA_VS => vga_vsync_n_s,
		VGA_HS => vga_hsync_n_s,
		VGA_BLANK => vga_blank_s,
		
		RS232_RXD => stm_tx_i,
		RS232_TXD => stm_rx_o,
		
		-- PS/2
		ps2k_clk_in => PS2K_CLK_IN,
		ps2k_dat_in => PS2K_DAT_IN,
		ps2k_clk_out => PS2K_CLK_OUT,
		ps2k_dat_out => PS2K_DAT_OUT,
		
		-- Joystick
		--joya => "11" & joy1_p9_i & joy1_p6_i & joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i,
		--joyb => "11" & joy2_p9_i & joy2_p6_i & joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i,

		-- joy_s format MXYZ SACB RLDU
		joya => joy1_s(7 downto 0), 
		joyb => joy2_s(7 downto 0), 	
		
		-- SD card
		spi_cs => sd_cs_n_o,
		spi_miso => sd_miso_i,
		spi_mosi => sd_mosi_o,
		spi_clk => sd_sclk_o

	);
	

	mydither : work.video_vga_dither
		generic map(
			outbits => 5
	)
		port map(
			clk=>memclk,
			hsync=>vga_hsync_n_s,
			vsync=>vga_vsync_n_s,
			vid_ena=>'1',
			iRed => vga_red,
			iGreen => vga_green,
			iBlue => vga_blue,
			std_logic_vector(oRed) => vga_r_o,
			std_logic_vector(oGreen) => vga_g_o,
			std_logic_vector(oBlue) => vga_b_o
		);


	leftsd: component hybrid_pwm_sd
	port map
	(
		clk => memclk,
		n_reset => '1',
		din(15) => not audio_l(15),
		din(14 downto 0) => std_logic_vector(audio_l(14 downto 0)),
		dout => dac_l_o
	);
	
	rightsd: component hybrid_pwm_sd
	port map
	(
		clk => memclk,
		n_reset => '1',
		din(15) => not audio_r(15),
		din(14 downto 0) => std_logic_vector(audio_r(14 downto 0)),
		dout => dac_r_o
	);

--- Joystick read with sega 6 button support----------------------

	process(vga_hsync_n_s)
		variable state_v : unsigned(7 downto 0) := (others=>'0');
		variable j1_sixbutton_v : std_logic := '0';
		variable j2_sixbutton_v : std_logic := '0';
	begin
		if falling_edge(vga_hsync_n_s) then
		
			state_v := state_v + 1;
			
			case state_v is
				-- joy_s format MXYZ SACB RLDU
			
				when X"00" =>  
					joyP7_s <= '0';
					
				when X"01" =>
					joyP7_s <= '1';

				when X"02" => 
					joy1_s(3 downto 0) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- R, L, D, U
					joy2_s(3 downto 0) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- R, L, D, U
					joy1_s(5 downto 4) <= joy1_p9_i & joy1_p6_i; -- C, B
					joy2_s(5 downto 4) <= joy2_p9_i & joy2_p6_i; -- C, B					
					joyP7_s <= '0';
					j1_sixbutton_v := '0'; -- Assume it's not a six-button controller
					j2_sixbutton_v := '0'; -- Assume it's not a six-button controller

				when X"03" =>
					joy1_s(7 downto 6) <= joy1_p9_i & joy1_p6_i; -- Start, A
					joy2_s(7 downto 6) <= joy2_p9_i & joy2_p6_i; -- Start, A
					joyP7_s <= '1';
			
				when X"04" =>  
					joyP7_s <= '0';

				when X"05" =>
					if joy1_right_i = '0' and joy1_left_i = '0' and joy1_down_i = '0' and joy1_up_i = '0' then 
						j1_sixbutton_v := '1'; --it's a six button
					end if;
					
					if joy2_right_i = '0' and joy2_left_i = '0' and joy2_down_i = '0' and joy2_up_i = '0' then 
						j2_sixbutton_v := '1'; --it's a six button
					end if;
					
					joyP7_s <= '1';
					
				when X"06" =>
					if j1_sixbutton_v = '1' then
						joy1_s(11 downto 8) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- Mode, X, Y e Z
					end if;
					
					if j2_sixbutton_v = '1' then
						joy2_s(11 downto 8) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- Mode, X, Y e Z
					end if;
					
					joyP7_s <= '0';

				when others =>
					joyP7_s <= '1';
					
			end case;

		end if;
	end process;
	
	joyX_p7_o <= joyP7_s;
---------------------------

------ NO HDMI --------------------------
	no_hdmi_block : block 

			component no_hdmi is
			port
			(
				-- pixel clock
				pclk			: in std_logic;
				
				-- output to VGA screen
				hs	: out std_logic;
				vs	: out std_logic;
				pixel_o	: out std_logic;
				blank 	: out std_logic
			);
			end component;
			  
			signal no_hdmi_vs_s : std_logic;
			signal no_hdmi_hs_s : std_logic;
			signal no_hdmi_blank_s : std_logic;
			signal no_hdmi_pixel_s : std_logic;
			
			signal tdms_r_s			: std_logic_vector( 9 downto 0);
			signal tdms_g_s			: std_logic_vector( 9 downto 0);
			signal tdms_b_s			: std_logic_vector( 9 downto 0);
			signal hdmi_p_s			: std_logic_vector( 3 downto 0);
			signal hdmi_n_s			: std_logic_vector( 3 downto 0);
			
	begin 			
			no_hdmi1 : no_hdmi 
			port map
			(
				pclk     => clk_vga,
				
				hs    	=> no_hdmi_hs_s,
				vs    	=> no_hdmi_vs_s,
				pixel_o  => no_hdmi_pixel_s,
				blank 	=> no_hdmi_blank_s
			);
		
		
		---------
		
		-- HDMI
			no_hdmi_dvid: entity work.hdmi
			generic map (
				FREQ	=> 25200000,	-- pixel clock frequency 
				FS		=> 48000,		-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
				CTS	=> 25200,		-- CTS = Freq(pixclk) * N / (128 * Fs)
				N		=> 6144			-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
			) 
			port map (
				I_CLK_PIXEL		=> clk_vga,
					
				I_R				=> no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s,
				I_G				=> no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s,
				I_B				=> no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s & no_hdmi_pixel_s,
				
				I_BLANK			=> no_hdmi_blank_s,
				I_HSYNC			=> no_hdmi_hs_s,
				I_VSYNC			=> no_hdmi_vs_s,
				-- PCM audio
				I_AUDIO_ENABLE	=> '1',
				I_AUDIO_PCM_L 	=> (others=>'0'),
				I_AUDIO_PCM_R	=> (others=>'0'),
				-- TMDS parallel pixel synchronous outputs (serialize LSB first)
				O_RED				=> tdms_r_s,
				O_GREEN			=> tdms_g_s,
				O_BLUE			=> tdms_b_s
			);
			

			no_hdmi_io: entity work.hdmi_out_altera
			port map (
				clock_pixel_i		=> clk_vga,
				clock_tdms_i		=> memclk,
				red_i					=> tdms_r_s,
				green_i				=> tdms_g_s,
				blue_i				=> tdms_b_s,
				tmds_out_p			=> hdmi_p_s,
				tmds_out_n			=> hdmi_n_s
			);
			
			
			tmds_o(7)	<= hdmi_p_s(2);	-- 2+		
			tmds_o(6)	<= hdmi_n_s(2);	-- 2-		
			tmds_o(5)	<= hdmi_p_s(1);	-- 1+			
			tmds_o(4)	<= hdmi_n_s(1);	-- 1-		
			tmds_o(3)	<= hdmi_p_s(0);	-- 0+		
			tmds_o(2)	<= hdmi_n_s(0);	-- 0-	
			tmds_o(1)	<= hdmi_p_s(3);	-- CLK+	
			tmds_o(0)	<= hdmi_n_s(3);	-- CLK-	
		
	end block;
	

	
			

end architecture;

