-------------------------------------------------------------------------------
--
-- CP/M 3 FPGA project
--
-- Copyright (c) 2016, Fabio Belavenuto (belavenuto@gmail.com)
--
-- All rights reserved
--
-- Some files is copyright by Grant Searle 2014
-- You are free to use this files in your own projects but must never charge for it nor use it without
-- acknowledgement.
-- Please ask permission from Grant Searle before republishing elsewhere.
-- If you use this file or any part of it, please add an acknowledgement to myself and
-- a link back to my main web site http://searle.hostei.com/grant/    
-- and to the "multicomp" page at http://searle.hostei.com/grant/Multicomp/index.html
--
-- Please check on the above web pages to see if there are any updates before using this file.
-- If for some reason the page is no longer available, please search for "Grant Searle"
-- on the internet to see if I have moved to another web hosting service.
--
-- Grant Searle
-- eMail address available on my main web page link above.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity multicore_top is
	generic (
		hdmi_output_g		: boolean	:= true
	);
	port (
		-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);
		btn_oe_n_i			: in    std_logic;
		btn_clr_n_i			: in    std_logic;

		-- SRAM (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_ce_n_o			: out   std_logic_vector(1 downto 0)	:= (others => '1');
		sram_oe_n_o			: out   std_logic								:= '1';

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joystick
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_io			: inout std_logic								:= 'Z';
		joy1_p7_o			: out   std_logic								:= '1';
		joy1_p9_io			: inout std_logic								:= 'Z';
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_io			: inout std_logic;
		joy2_p7_o			: out   std_logic								:= '1';
		joy2_p9_io			: inout std_logic;

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';
		
		-- Debug
		leds_n_o				: out   std_logic_vector(7 downto 0)	:= (others => '0')
	);
end entity;

architecture behavior of multicore_top is

	signal pll_locked			: std_logic;
	signal reset_n_s			: std_logic;

	signal clock_master		: std_logic;
	signal clock_vga_s		: std_logic;
	signal clock_dvi_s		: std_logic;

	signal sramAddr			: std_logic_vector(16 downto 0);
	signal sramWE_n			: std_logic;
	signal sramCS_n			: std_logic;
	signal sramOE_n			: std_logic;
	signal serRX				: std_logic;
	signal serTX				: std_logic;
	signal serRTS				: std_logic;
	signal hsync_n				: std_logic;
	signal vsync_n				: std_logic;
	signal blank_s				: std_logic;
	signal vgar					: std_logic_vector(1 downto 0);
	signal vgag					: std_logic_vector(1 downto 0);
	signal vgab					: std_logic_vector(1 downto 0);
	signal tdms_s				: std_logic_vector(7 downto 0);
	signal led					: std_logic;
	
	-- scanlines
	signal scanlines_en_s	: std_logic := '0';
	signal btn_scan_s			: std_logic;
	signal odd_line_s			: std_logic := '0';
	signal vga_out_s			: std_logic_vector (7 downto 0);

begin

	pll_inst: entity work.pll1
	port map (
		inclk0	=> clock_50_i,
		c0			=> clock_master,		-- 50.000 MHz
		c1			=> clock_vga_s,		-- 25.000
		c2			=> clock_dvi_s,		-- 125.000
		locked	=> pll_locked
	);

	-- Virtual TOP
	v_top: entity work.virtual_top
	port map (
		n_reset			=> reset_n_s,				--: in std_logic;
		clk				=> clock_master,			--: in std_logic;
		sramData			=> sram_data_io,			--: inout std_logic_vector(7 downto 0);
		sramAddress		=> sramAddr,				--: out std_logic_vector(15 downto 0);
		n_sRamWE			=> sramWE_n,				--: out std_logic;
		n_sRamCS			=> sramCS_n,				--: out std_logic;
		n_sRamOE			=> sramOE_n,				--: out std_logic;
		rxd1				=> '0',						--: in std_logic;
		txd1				=> open,						--: out std_logic;
		rts1				=> open,						--: out std_logic;
		rxd2				=> serRX,					--: in std_logic;
		txd2				=> serTX,					--: out std_logic;
		rts2				=> serRTS,					--: out std_logic;
		videoSync		=> open,						--: out std_logic;
		video				=> open,						--: out std_logic;
		videoR0			=> vgar(0),					--: out std_logic;
		videoG0			=> vgag(0),					--: out std_logic;
		videoB0			=> vgab(0),					--: out std_logic;
		videoR1			=> vgar(1),					--: out std_logic;
		videoG1			=> vgag(1),					--: out std_logic;
		videoB1			=> vgab(1),					--: out std_logic;
		hSync				=> hsync_n,					--: out std_logic;
		vSync				=> vsync_n,					--: out std_logic;
		blank_o			=> blank_s,
		ps2Clk			=> ps2_clk_io,				--: inout std_logic;
		ps2Data			=> ps2_data_io,			--: inout std_logic;
		sdCS				=> sd_cs_n_o,				--: out std_logic;
		sdMOSI			=> sd_mosi_o,				--: out std_logic;
		sdMISO			=> sd_miso_i,				--: in std_logic;
		sdSCLK			=> sd_sclk_o,				--: out std_logic;
		driveLED			=> led,						--: out std_logic :='1'	
		cpu_a				=> open,
		cpu_rd			=> open,
		cpu_wr			=> open,
		cpu_ioreq		=> open,
		cpu_mreq			=> open,
		cpu_rfsh			=> open
	);

	-- Glue
	reset_n_s	<= pll_locked and (btn_n_i(3) or btn_n_i(4));

	sram_addr_o		<= "00" & sramAddr;
	sram_ce_n_o(0)	<= sramCS_n;
	sram_we_n_o		<= sramWE_n;
	sram_oe_n_o		<= sramOE_n;
	serRX				<= '0';

	uh: if hdmi_output_g generate

		-- HDMI
		inst_dvid: entity work.hdmi
		generic map (
			FREQ	=> 25200000,	-- pixel clock frequency 
			FS		=> 48000,		-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
			CTS	=> 25200,		-- CTS = Freq(pixclk) * N / (128 * Fs)
			N		=> 6144			-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
		) 
		port map (
			I_CLK_VGA		=> clock_vga_s,
			I_CLK_TMDS		=> clock_dvi_s,
			I_RED				=> vga_out_s(7 downto 5) & vga_out_s(7 downto 5) & vga_out_s(7 downto 6),
			I_GREEN			=> vga_out_s(4 downto 2) & vga_out_s(4 downto 2) & vga_out_s(4 downto 3),
			I_BLUE			=> vga_out_s(1 downto 0) & vga_out_s(1 downto 0) & vga_out_s(1 downto 0) & vga_out_s(1 downto 0),
			I_BLANK			=> blank_s,
			I_HSYNC			=> hsync_n,
			I_VSYNC			=> vsync_n,
			I_AUDIO_PCM_L 	=> (others => '0'),
			I_AUDIO_PCM_R	=> (others => '0'),
			O_TMDS			=> tdms_s
		);
		vga_hsync_n_o	<= tdms_s(7);	-- 2+		10
		vga_vsync_n_o	<= tdms_s(6);	-- 2-		11
		vga_b_o(2)		<= tdms_s(5);	-- 1+		144	
		vga_b_o(1)		<= tdms_s(4);	-- 1-		143
		vga_r_o(0)		<= tdms_s(3);	-- 0+		133
		vga_g_o(2)		<= tdms_s(2);	-- 0-		132
		vga_r_o(1)		<= tdms_s(1);	-- CLK+	113
		vga_r_o(2)		<= tdms_s(0);	-- CLK-	112
	end generate;

	nuh: if not hdmi_output_g generate
		vga_r_o			<= vga_out_s(7 downto 5);
		vga_g_o			<= vga_out_s(4 downto 2);
		vga_b_o			<= vga_out_s(1 downto 0) & '0';
		vga_hsync_n_o	<= hsync_n;
		vga_vsync_n_o	<= vsync_n;
	end generate;

	-- Debug
	leds_n_o(0) <= led;
	
	---------------------------------
	-- scanlines
	btnscl: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clock_vga_s,
		button_i			=> btn_n_i(1) or btn_n_i(2),
		result_o			=> btn_scan_s
	);
	
	
	process (btn_scan_s)
	begin
		if falling_edge(btn_scan_s) then
			scanlines_en_s <= not scanlines_en_s;
		end if;
	end process;
	

	vga_out_s <= '0' & vgar  & '0'  & vgar & '0' & vgab(1) when scanlines_en_s = '1' and odd_line_s = '1' else vgar & "0" & vgag & "0" & vgab;
	
	
	
	process(hsync_n,vsync_n)
	begin
		if vsync_n = '0' then
			odd_line_s <= '0';
		elsif rising_edge(hsync_n) then
			odd_line_s <= not odd_line_s;
		end if;
	end process;

end architecture;