-- BBC Master / BBC B for the Spectrum Next
--
-- Copright (c) 2017 David Banks
--
-- Based on previous work by Mike Stirling
--
-- Copyright (c) 2011 Mike Stirling
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- * Redistributions of source code must retain the above copyright notice,
--   this list of conditions and the following disclaimer.
--
-- * Redistributions in synthesized form must reproduce the above copyright
--   notice, this list of conditions and the following disclaimer in the
--   documentation and/or other materials provided with the distribution.
--
-- * Neither the name of the author nor the names of other contributors may
--   be used to endorse or promote products derived from this software without
--   specific prior written agreement from the author.
--
-- * License is granted for non-commercial use only.  A fee may not be charged
--   for redistributions as source code or in synthesized/hardware form without
--   specific prior written agreement from the author.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- Multicore 2 top-level
--
-- (c) 2017 David Banks
-- (C) 2011 Mike Stirling

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;


-- Generic top-level entity for Multicore 2 board (Victor Trucco)
entity bbc_micro_mc2 is
    generic (
        IncludeAMXMouse    : boolean := false;
        IncludeSID         : boolean := true;
        IncludeMusic5000   : boolean := true;
        IncludeICEDebugger : boolean := false;
        IncludeCoPro6502   : boolean := false; -- The co pro options are mutually exclusive
        IncludeCoProExt    : boolean := false   -- (i.e. select just one)
    );
    port (
     -- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= 'Z'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: in std_logic		:= 'Z';
		stm_b13_io			: in std_logic		:= 'Z';
		stm_b14_io			: out std_logic		:= 'Z';
		stm_b15_io			: in std_logic		:= 'Z'
    );
end entity;

architecture rtl of bbc_micro_mc2 is

	function to_slv(s: string) return std_logic_vector is 
        constant ss: string(1 to s'length) := s; 
        variable answer: std_logic_vector(1 to 8 * s'length); 
        variable p: integer; 
        variable c: integer; 
    begin 
        for i in ss'range loop
            p := 8 * i;
            c := character'pos(ss(i));
            answer(p - 7 to p) := std_logic_vector(to_unsigned(c,8)); 
        end loop; 
        return answer; 
    end function; 
	 
	 	component sdram is 
		port
		(
			-- interface to the SDRAM IC
			sd_data      : inout   std_logic_vector(15 downto 0);
			sd_addr      : out   std_logic_vector(12 downto 0);
			sd_dqm       : out   std_logic_vector(1 downto 0);
			sd_cs        : out   std_logic;
			sd_ba        : out   std_logic_vector(1 downto 0);
			sd_we        : out   std_logic;
			sd_ras       : out   std_logic;
			sd_cas       : out   std_logic;

			-- system interface
			clk          : in std_logic;
			clkref       : in std_logic;
			init         : in std_logic;

			-- cpu interface
			din          : in std_logic_vector(15 downto 0);
			addr         : in std_logic_vector(24 downto 0);
			we           : in std_logic;
			oe           : in std_logic;
			ds           : in std_logic_vector(1 downto 0);
			dout         : out   std_logic_vector(15 downto 0)
		);
		end component;

-------------
-- Signals
-------------

    signal fx_clk_24       : std_logic;
    signal fx_clk_32       : std_logic;
    signal clock_24        : std_logic;
    signal clock_27        : std_logic;
    signal clock_32        : std_logic;
    signal clock_48        : std_logic;
	 signal sdram_clock		: std_logic; 
			
	 signal pll_reset       : std_logic;
	 signal pll_locked      : std_logic;

    signal dac_l_in        : std_logic_vector(9 downto 0);
    signal dac_r_in        : std_logic_vector(9 downto 0);
    signal audio_l         : std_logic_vector(15 downto 0);
    signal audio_r         : std_logic_vector(15 downto 0);
    signal powerup_reset_n : std_logic;
    signal hard_reset_n    : std_logic;
    signal reset_counter   : std_logic_vector(9 downto 0);
    signal ram_addr        : std_logic_vector(20 downto 0);
    signal RAM_A           : std_logic_vector(18 downto 0);
    signal RAM_Din         : std_logic_vector(7 downto 0);
    signal RAM_Dout        : std_logic_vector(7 downto 0);
    signal RAM_nWE         : std_logic;
    signal RAM_nOE         : std_logic;
    signal RAM_nCS         : std_logic;
    signal keyb_dip        : std_logic_vector(7 downto 0);
    signal vid_mode        : std_logic_vector(3 downto 0);
    signal m128_counter    : std_logic_vector(24 downto 0);
    signal m128_mode       : std_logic := '0';
    signal m128_mode_1     : std_logic;
    signal m128_mode_2     : std_logic;
    signal copro_mode      : std_logic;
    signal red             : std_logic_vector(3 downto 0);
    signal green           : std_logic_vector(3 downto 0);
    signal blue            : std_logic_vector(3 downto 0);
    signal joystick1       : std_logic_vector(4 downto 0);
    signal joystick2       : std_logic_vector(4 downto 0);
    signal ext_tube_r_nw   : std_logic;
    signal ext_tube_nrst   : std_logic;
    signal ext_tube_ntube  : std_logic;
    signal ext_tube_phi2   : std_logic;
    signal ext_tube_a      : std_logic_vector(6 downto 0);
    signal ext_tube_di     : std_logic_vector(7 downto 0);
    signal ext_tube_do     : std_logic_vector(7 downto 0);

	 -- Video
	 signal video_r_s				: std_logic_vector(3 downto 0)	:= (others => '0');
	 signal video_g_s				: std_logic_vector(3 downto 0)	:= (others => '0');
	 signal video_b_s				: std_logic_vector(3 downto 0)	:= (others => '0');
	 signal video_hsync_n_s		: std_logic								:= '1';
	 signal video_vsync_n_s		: std_logic								:= '1';
	 signal vga_hsync_n_s		: std_logic								:= '1';
	 signal vga_vsync_n_s		: std_logic								:= '1';

	 -- OSD
	 signal pump_active_s 	 : std_logic := '0';
	 signal osd_s  		 : std_logic_vector(7 downto 0) := "00111111";
	 signal clock_div_q	: unsigned(7 downto 0) 				:= (others => '0');
	 signal keys_s			: std_logic_vector( 7 downto 0) := (others => '1');	
	 signal power_on_reset     : std_logic := '0';
	 
	 signal sram_addr_s : std_logic_vector(18 downto 0);
	 signal sram_data_s : std_logic_vector(7 downto 0);
	 signal sram_we_n_s : std_logic := '1';
	 
	 signal cpu_addr_s : std_logic_vector(14 downto 0);
	 signal rom_data_s : std_logic_vector(7 downto 0);
	 
    signal reset_n         : std_logic  := '1';
	 
	 -- config string
	 constant STRLEN			: integer := 14;
	 signal CONF_STR		: std_logic_vector((STRLEN * 8)-1 downto 0) := to_slv("P,beeb_rom.dat");
	 
	 

begin

--------------------------------------------------------
-- BBC Micro Core
--------------------------------------------------------

    -- TODO, make this optional
    copro_mode     <= '1';

    -- As per the Beeb keyboard DIP switches
    keyb_dip       <= "00000000";

    -- Bit 3 inverts vsync
    -- Bit 2 inverts hsync
    -- Bit 1 selects between Mist (0) and RGBtoVGA (1) scan doublers
    -- Bit 0 selecte between sRGB (0) and VGA (1)
    vid_mode       <= "0001";

    bbc_micro : entity work.bbc_micro_core
    generic map (
        IncludeAMXMouse    => IncludeAMXMouse,
        IncludeSID         => IncludeSID,
        IncludeMusic5000   => IncludeMusic5000,
        IncludeICEDebugger => IncludeICEDebugger,
        IncludeCoPro6502   => IncludeCoPro6502,
        IncludeCoProSPI    => false,
        IncludeCoProExt    => IncludeCoProExt,
        UseOrigKeyboard    => false,
        UseT65Core         => false,
        UseAlanDCore       => true
        )
    port map (
        clock_32       => clock_32,
        clock_24       => clock_24,
        clock_27       => clock_27,
        hard_reset_n   => hard_reset_n,
        ps2_kbd_clk    => ps2_clk_io,
        ps2_kbd_data   => ps2_data_io,
        ps2_mse_clk    => ps2_mouse_clk_io,
        ps2_mse_data   => ps2_mouse_data_io,
        video_red      => video_r_s,
        video_green    => video_g_s,
        video_blue     => video_b_s,
        video_vsync    => video_vsync_n_s,
        video_hsync    => video_hsync_n_s,
        audio_l        => audio_l,
        audio_r        => audio_r,
        ext_nOE        => RAM_nOE,
        ext_nWE        => RAM_nWE,
        ext_nCS        => RAM_nCS,
        ext_A          => RAM_A,
        ext_Dout       => RAM_Dout,
        ext_Din        => RAM_Din,
        SDMISO         => sd_miso_i,
        SDSS           => sd_cs_n_o,
        SDCLK          => sd_sclk_o,
        SDMOSI         => sd_mosi_o,
        caps_led       => open,
        shift_led      => open,
        keyb_dip       => keyb_dip,
        vid_mode       => vid_mode,
        joystick1      => joystick1,
        joystick2      => joystick2,
        avr_RxD        => stm_tx_i,
        avr_TxD        => stm_rx_o,
        cpu_addr       => open,
        m128_mode      => m128_mode,
        copro_mode     => copro_mode,
        p_spi_ssel     => '0',
        p_spi_sck      => '0',
        p_spi_mosi     => '0',
        p_spi_miso     => open,
        p_irq_b        => open,
        p_nmi_b        => open,
        p_rst_b        => open,
        ext_tube_r_nw  => ext_tube_r_nw,
        ext_tube_nrst  => ext_tube_nrst,
        ext_tube_ntube => ext_tube_ntube,
        ext_tube_phi2  => ext_tube_phi2,
        ext_tube_a     => ext_tube_a,
        ext_tube_di    => ext_tube_di,
        ext_tube_do    => ext_tube_do,
        test           => open,
        -- original keyboard not yet supported on the Spec Next
        ext_keyb_led1  => open,
        ext_keyb_led2  => open,
        ext_keyb_led3  => open,
        ext_keyb_1mhz  => open,
        ext_keyb_en_n  => open,
        ext_keyb_pa    => open,
        ext_keyb_rst_n => '1',
        ext_keyb_ca2   => '1',
        ext_keyb_pa7   => '1'
    );

    -- Joystick 1
    --   Bit 0 - Up (active low)
    --   Bit 1 - Down (active low)
    --   Bit 2 - Left (active low)
    --   Bit 3 - Right (active low)
    --   Bit 4 - Fire (active low)
    joystick1 <= joy1_p6_i & joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i;

    -- Joystick 2
    --   Unused
    joystick2 <= "11111";



--------------------------------------------------------
-- Clock Generation
--------------------------------------------------------

    -- 32 MHz master clock from input clock
    -- plus intermediate 48MHz clock
    pll32: entity work.pll32
        port map (
            inclk0         => clock_50_i,   -- 50 MHz input clock
            c0             => clock_32,     -- 32 MHz master clock for the Beeb
            c1             => clock_48,     -- 48 MHz intermediate clock, see below
				c2					=> sdram_clock,   -- clock with offset to SDRAM
            locked         => pll_locked
        );

    -- 24MHz teletext clock, generated from 48MHz intermediate clock
    --
    -- Important: this must be phase locked to the 32MHz clock
    -- i.e. they must be generated from the same clock source
    --
    -- This is also the case on a real BBC, where the 6MHz teletext
    -- clock is generted by some dubious delays from the 16MHz
    -- system clock.
    clock_24_gen : process(clock_48)
    begin
        if rising_edge(clock_48) then
            clock_24 <= not clock_24;
        end if;
    end process;
	
--------------------------------------------------------
-- Power Up Reset Generation
--------------------------------------------------------

    m128_gen : process(clock_32)
    begin
        if rising_edge(clock_32) then
            if btn_n_i(2) = '0' then
                m128_counter <= m128_counter + 1;
            else
                m128_counter <= (others => '0');
            end if;
				
            if m128_counter = 32000000 then -- 1s
               
					
					  
					 reset_n <= '0';
					
					if m128_mode = '1' then
						CONF_STR		<= to_slv("P,beeb_rom.dat");
					else
						CONF_STR		<= to_slv("P,b128_rom.dat");
					end if;
			
					m128_mode <= not m128_mode;
					
            end if;
				
				if m128_counter > 32000010 then 
					 
					 reset_n <= '1';
					 
				end if;
				
				
				 
        end if;
    end process;

    -- Generate a reliable power up reset
    -- Also, perform a power up reset if the master/beeb mode switch is changed
    reset_gen : process(clock_32)
    begin
        if rising_edge(clock_32) then
            m128_mode_1 <= m128_mode;
            m128_mode_2 <= m128_mode_1;
            if (m128_mode_1 /= m128_mode_2) then
                reset_counter <= (others => '0');
            elsif (reset_counter(reset_counter'high) = '0') then
                reset_counter <= reset_counter + 1;
            end if;
            powerup_reset_n <= btn_n_i(1) and reset_counter(reset_counter'high);
        end if;
    end process;

   -- extend the version seen by the core to hold the 6502 reset during bootstrap
   hard_reset_n <= powerup_reset_n and not pump_active_s;
	
--------------------------------------------------------
-- Audio DACs
--------------------------------------------------------

    -- Convert from signed to unsigned
    dac_l_in <= (not audio_l(15)) & audio_l(14 downto 6);
    dac_r_in <= (not audio_r(15)) & audio_r(14 downto 6);

    dac_l : entity work.pwm_sddac
    generic map (
        msbi_g => 9
    )
    port map (
        clk_i => clock_32,
        reset => '0',
        dac_i => dac_l_in,
        dac_o => dac_l_o
    );

    dac_r : entity work.pwm_sddac
    generic map (
        msbi_g => 9
    )
    port map (
        clk_i => clock_32,
        reset => '0',
        dac_i => dac_r_in,
        dac_o => dac_r_o
    );


OSB_BLOCK: block 

	type config_array is array(natural range 15 downto 0) of std_logic_vector(7 downto 0);

		component osd is
		generic
		(
			STRLEN 		 : integer := 0;
			OSD_X_OFFSET : std_logic_vector(9 downto 0) := (others=>'0');
			OSD_Y_OFFSET : std_logic_vector(9 downto 0) := (others=>'0');
			OSD_COLOR    : std_logic_vector(2 downto 0) := (others=>'0')
		);
		port
		(
			-- OSDs pixel clock, should be synchronous to cores pixel clock to
			-- avoid jitter.
			pclk		: in std_logic;

			-- SPI interface
			sck		: in std_logic;
			ss			: in std_logic;
			sdi		: in std_logic;
			sdo		: out std_logic;

			-- VGA signals coming from core
			red_in 	: in std_logic_vector(3 downto 0);
			green_in : in std_logic_vector(3 downto 0);
			blue_in 	: in std_logic_vector(3 downto 0);
			hs_in		: in std_logic;
			vs_in		: in std_logic;
			
			-- VGA signals going to video connector
			red_out	: out std_logic_vector(3 downto 0);
			green_out: out std_logic_vector(3 downto 0);
			blue_out	: out std_logic_vector(3 downto 0);
			hs_out 	: out std_logic;
			vs_out 	: out std_logic;
			
			-- external data in to the microcontroller
			data_in 	: in std_logic_vector(7 downto 0);
			conf_str : in std_logic_vector( (STRLEN * 8)-1 downto 0);
			
			-- data pump to sram
			pump_active_o	: out std_logic := '0';
			sram_a_o 		: out std_logic_vector(18 downto 0);
			sram_d_o 		: out std_logic_vector(7 downto 0);
			sram_we_n_o 	: out std_logic := '1';
			
			config_buffer_o: out config_array
		);
		end component;
		
		alias SPI_DI  : std_logic is stm_b15_io;
		alias SPI_DO  : std_logic is stm_b14_io;
		alias SPI_SCK : std_logic is stm_b13_io;
		alias SPI_SS3 : std_logic is stm_b12_io;
		
		signal vga_r_out_s : std_logic_vector(3 downto 0);
		signal vga_g_out_s : std_logic_vector(3 downto 0);
		signal vga_b_out_s : std_logic_vector(3 downto 0);
		
		signal sram_addr_s : std_logic_vector(18 downto 0) := (others=>'1');
		signal sram_data_s : std_logic_vector(7 downto 0);
		signal sram_we_s 	 : std_logic := '1';
		
		signal power_on_s		: std_logic_vector(24 downto 0)	:= (others => '1');
		
		signal config_buffer_s : config_array;

		---------------------
		

		signal sdram_din  : std_logic_vector(15 downto 0);
		signal sdram_addr : std_logic_vector(24 downto 0);
		signal sdram_wr   : std_logic; 
		signal sdram_oe   : std_logic; 
		signal sdram_dout	: std_logic_vector(15 downto 0); 
		
	begin
		
		
		osd1 : osd 
		generic map
		(
			STRLEN => STRLEN,
			OSD_COLOR => "001", -- RGB
			OSD_X_OFFSET => "0000010010", -- 50
			OSD_Y_OFFSET => "0000001111"  -- 15
		)
		port map
		(
			pclk       => clock_32,

			-- spi for OSD
			sdi        => SPI_DI,
			sck        => SPI_SCK,
			ss         => SPI_SS3,
			sdo        => SPI_DO,
			
			red_in     => video_r_s,
			green_in   => video_g_s,
			blue_in    => video_b_s,
			hs_in      => video_hsync_n_s,
			vs_in      => video_vsync_n_s,

			red_out    => vga_r_o,
			green_out  => vga_g_o,
			blue_out   => vga_b_o,
			hs_out     => vga_hsync_n_s,
			vs_out     => vga_vsync_n_s,

			data_in		=> osd_s,
			conf_str		=> CONF_STR,
						
			pump_active_o	=> pump_active_s,
			sram_a_o			=> sram_addr_s,
			sram_d_o			=> sram_data_s,
			sram_we_n_o		=> sram_we_n_s,
			config_buffer_o=> config_buffer_s		
		);
			

		vga_hsync_n_o	<= vga_hsync_n_s;
		vga_vsync_n_o	<= vga_vsync_n_s;
		
		
--		sdram1 :  sdram 
--		port map
--		(
--			-- interface to the SDRAM IC
--			sd_data      => sdram_da_io ,
--			sd_addr      => sdram_ad_o  ,
--			sd_dqm       => sdram_dqm_o ,
--			sd_cs        => sdram_cs_o  ,
--			sd_ba        => sdram_ba_o  ,
--			sd_we        => sdram_we_o  ,
--			sd_ras       => sdram_ras_o ,
--			sd_cas       => sdram_cas_o ,
--
--			-- system interface
--			clk            => clock_32      ,
--			clkref         => clock_32       ,
--			init           => not pll_locked ,
--
--			-- cpu interface
--			din         => sdram_din   ,
--			addr        => sdram_addr  ,
--			we          => sdram_wr    ,
--			oe          => sdram_oe    ,
--			ds          => "01"		  , --just the lower 8 bits
--			dout        => sdram_dout  
--		);
--		
--		
--		sdram_clk_o <= sdram_clock;
--		sdram_cke_o <= '1';
--		
--		sdram_din   <= "00000000" & sram_data_s when pump_active_s = '1' and sram_we_n_s = '0' else "00000000" & RAM_Din when RAM_nWE = '0' else (others=>'Z'); 
--		sdram_addr  <= "000000" & sram_addr_s when pump_active_s = '1' else "000000" & RAM_A;
--		sdram_wr    <= (not sram_we_n_s) when pump_active_s = '1' else (not RAM_nWE);
--		sdram_oe    <= '0' when pump_active_s = '1' else (not RAM_nOE);
--		RAM_Dout 	<= sdram_dout(7 downto 0);
		
--		sdram : entity work.dpSDRAM256Mb 
--		generic map (
--			freq_g			=> 128
--		)
--		port map
--		(
--			clock_i			=> sdram_clock,
--			reset_i			=> not pll_reset,
--			refresh_i		=> '1',
--			
--			-- Porta 0
--			port0_cs_i		=> sdram_oe or sdram_wr,
--			port0_oe_i		=> sdram_oe,
--			port0_we_i		=> sdram_wr,
--			port0_addr_i	=> sdram_addr,
--			port0_data_i	=> sdram_din(7 downto 0),
--			port0_data_o	=> RAM_Dout,
--			
--			-- Porta 1
--			port1_cs_i		=> '0',
--			port1_oe_i		=> '0',
--			port1_we_i		=> '0',
--			port1_addr_i	=> (others=>'0'),
--			port1_data_i	=> (others=>'0'),
--			port1_data_o	=> open,
--			
--			-- SD-RAM ports
--			mem_cke_o		=> sdram_cke_o,
--			mem_cs_n_o		=> sdram_cs_o,
--			mem_ras_n_o		=> sdram_ras_o,
--			mem_cas_n_o		=> sdram_cas_o,
--			mem_we_n_o		=> sdram_we_o,
--			mem_udq_o		=> sdram_dqm_o(1),
--			mem_ldq_o		=> sdram_dqm_o(0),
--			mem_ba_o			=> sdram_ba_o,
--			mem_addr_o		=> sdram_ad_o,
--			mem_data_io		=> sdram_da_io
--		);
		
		sram_addr_o   <= sram_addr_s when pump_active_s = '1' else RAM_A;
		sram_data_io  <= sram_data_s when pump_active_s = '1' and sram_we_n_s = '0' else RAM_Din when RAM_nWE = '0' else (others=>'Z');
		RAM_Dout 	   <= sram_data_io;
		sram_oe_n_o   <= '0' when pump_active_s = '1' else RAM_nOE;
		sram_we_n_o   <= sram_we_n_s when pump_active_s = '1' else RAM_nWE;


		--start the microcontroller OSD after the power on
		process (clock_32, reset_n, osd_s)
		variable edge : std_logic_vector(1 downto 0);
		begin
			if rising_edge(clock_32) then
				if reset_n = '0' then
					power_on_s <= (others=>'1');
					stm_rst_o <= 'Z'; -- release the microcontroller reset line
					osd_s <= "11111111";
					power_on_reset <= '1';
				elsif power_on_s = x"00010" then
					osd_s <= "00111111";
				elsif power_on_s = x"00000" then
					power_on_reset <= '0';
				end if;
				
				if power_on_s /= x"00000" then
					power_on_s <= power_on_s - 1;
				end if;
				
				if pump_active_s = '1' and osd_s <= "00111111" then
					osd_s <= "11111111";
				end if;
				
				edge := edge(0) & pump_active_s;
				
				if edge = "10" then
					stm_rst_o	<= '0'; -- '0' hold the microcontroller on reset, to free the SD card
				end if;
				
			end if;
		end process;

	end block;	



end architecture;
