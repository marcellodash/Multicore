@echo off
rem --- 'pld_collector.cmd' v3.6.2 by KdL (2018.07.27)

set TIMEOUT=1
set VERSION=362
set OUTDIR=PLD%VERSION%
if "%1"=="" color 1f&title PLD collector tool
if not exist pldflash.com goto err_msg
rd /s /q %OUTDIR% >nul 2>nul
md %OUTDIR% >nul 2>nul
copy pldflash.com %OUTDIR%\PLDFLASH.COM >nul 2>nul
copy flash.bat %OUTDIR%\FLASH.BAT >nul 2>nul
rem --- 1chipMSX ---
set FNAME=OCM
call :collect_all
rem --- Zemmix Neo ---
set FNAME=ZEM
call :collect_all
if "%1"=="" cls&echo.&echo All done!
goto quit

:collect_all
set YENSLASH=backslash
set LAYOUT=BR
call :collect_%FNAME%
set LAYOUT=ES
call :collect_%FNAME%
set LAYOUT=FR
call :collect_%FNAME%
set LAYOUT=UK
call :collect_%FNAME%
set YENSLASH=yen
set LAYOUT=JP
call :collect_%FNAME%
goto:eof

:collect_OCM
copy 1chipmsx_%LAYOUT%_layout\bios_msx2plus_%YENSLASH%\emsx_top.pld %OUTDIR%\%FNAME%2P-%LAYOUT%.PLD >nul 2>nul
copy 1chipmsx_%LAYOUT%_layout\bios_msx3_%YENSLASH%\emsx_top.pld %OUTDIR%\%FNAME%X3-%LAYOUT%.PLD >nul 2>nul
goto:eof

:collect_ZEM
copy zemmixneo_%LAYOUT%_layout\bios_zemmixneo_%YENSLASH%\emsx_top.pld %OUTDIR%\%FNAME%KR-%LAYOUT%.PLD >nul 2>nul
copy zemmixneo_%LAYOUT%_layout\bios_zemmixneobr_%YENSLASH%\emsx_top.pld %OUTDIR%\%FNAME%BR-%LAYOUT%.PLD >nul 2>nul
copy zemmixneo_%LAYOUT%_layout\bios_sx1_%YENSLASH%\emsx_top.pld %OUTDIR%\SX1ES-%LAYOUT%.PLD >nul 2>nul
goto:eof

:err_msg
if "%1"=="" color f0
echo.&echo 'pldflash.com' not found!

:quit
if "%1"=="" waitfor /T %TIMEOUT% pause 2>nul
