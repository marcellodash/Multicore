Firmware Collection for 1chipMSX and Zemmix Neo
===============================================
- Choose a firmware by machine type, keyboard layout and BIOS type from the available variants.
- Copy the 'emsx_top.pld' file in the same folder of 'flash.bat' and 'pldflash.com'.
- Run these command lines from MSX-DOS 2 to go:

  A:\>FLASH EMSX_TOP.PLD

  or

  A:\>SET EXPERT ON
  A:\>PLDFLASH EMSX_TOP.PLD


WARNING!! Do NOT turn off the device until the Flash Process is done
--------------------------------------------------------------------
A corrupted PLD file might cause your system to fail!
Any damages caused by improper operations of updating the firmware is at your own risk. Please be responsible.

Extra note: 'pld_collector.cmd' is a simple collector tool ready to use.


Recovery Guide
==============
Here is how to flash the device after a failed update using a personal computer with Windows OS.

1. Buy and install a compatible download cable for 'EPCS4' like USB ByteBlaster.

2. Download and install Altera Programmer for Windows (freeware):
http://download.altera.com/akdlm/software/acds/11.0sp1/208/standalone/11.0sp1_programmer_windows.exe

3. Open the case of your 1chipMSX or Zemmix Neo and attach the cable on the JTAG connector with the pin-1 towards the front of device.

4. Run the programmer and power on the device, select active serial programming method to load the 'recovery.pof' file.

______________
KdL 2018.07.27
