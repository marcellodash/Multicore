# SNTPSM v0.20
(c)2019 Oduvaldo Pavan Junior - ducasp@gmail.com

Based on SNTP client for UNAPI By Konamiman - konamiman@konamiman.com

It is a simple SNTP client that will retrieve the actual time and date from a
SNTP server and update your MSX-SM clock with it.

Usage: 
sntpsm time zone [/r]

time zone: Formatted as +|-hh:mm where hh=00-12, mm=00-59

/r Force AP/Router listing and selection even if connected

Example to set the time to GMT-3 (Brasilia):

sntpsm -03:00

If you haven't configured ESP8266 to connect to an Access Point/Router, this
software will list available access points (up to 10, ordered starting with the
strongest signal first), supporting both Open or Encrypted (password required)
connections. It will save that connection in ESP8266 flash memory so it will 
connect automatically next time.