# WiFi Utilities for MSX-SM

This directory contains utilities that uses the ESP8266 available in MSX-SM.

Those binaries are not supposed to be used on other MSX's as those do not have
the ESP8266.

Source code of those utilities can be found at DucaSP github:

https://github.com/ducasp/MSX-Development