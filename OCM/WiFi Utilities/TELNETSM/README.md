# TELNETSM v0.70

(c)2019 Oduvaldo Pavan Junior - ducasp@gmail.com

It is a TELNET client that allow you to connect to a TELNET server and interact
with it. If used along with JANSI/J_ON/MEMMAN it will be able to receive and 
show ANSI escape codes / colors.

It supports receiving files through XMODEM CRC, XMODEM 1K CRC, YMODEM and
YMODEM-G), including file batch in YMODEM/YMODEM-G.

Once the server is ready to send files, press CTRL+B and then type the filename
if XMODEM is being used, or Y if it is a YMODEM transfer, or G for YMODEM-G.

My recommendation is YMODEM-G, it is way faster due to not having to wait for
client confirmation before sending the next packet.

In order to allow YMODEM-G high performance, this program will check for a 
RAMDISK or create a 2MB RAMDISK. It will be used to store the files. This is 
needed as MSX-DOS2/NEXTOR takes several seconds to calculate free disk space
on large partitions (in my 4GB Card it takes more than 4s!). The first write
to any file or whenever the disk buffer is fully flushed will check for 
available disk space, that means, in YMODEM-G when several 1K packages are
being sent, we are locked waiting the first block to be written. This make it
impossible to use such partitions to store YMODEM-G transfers (and make other
transfers to suffer delays) as the server will send packets, those will go to
the ESP, ESP will transfer to MSX-SM FIFO and it will be filled even before
the first block has been written to disk. As most MSX-SM users will have large
SD-CARDs with large partitions, this is the best option to overcome this. As
such, transfer file size or total batch transfer size is limited to 2MB.

***WARNING***

Make sure that after exiting the program, you copy data from RAMDISK (H:) to
your local disk. This program will be kind enough to warn you about that when
you exit.

Usage: 
telnetsm server:port [/sSPEED] [/r]

server:port: 192.168.0.1:23 or bbs.hispamsx.org:23

/s force using a given baudrate to communicate with ESP:
/s0 - 115200, /s1 - 57600, /s2 - 38400 /s3 - 31250 /s4 - 19200
/s5 - 9600 /s6 - 4800 /s7 - 2400

***WARNING***

FIFO being FULL will most likely occur if you change speed. This program will
perform about 3000 I/O per sec. More than that, FIFO can be filled before we
can get its data out. 31250 for turbo CPU and 19200 for regular CPU is my
recommendation

/r will list available access points so you can connect to a different one even
if ESP has connected automatically to other access point.

Example to connect to rainmaker:

telnetsm rainmaker.wunderground.com:23

If you haven't configured ESP8266 to connect to an Access Point/Router, this
software will list available access points (up to 10, ordered starting with the
strongest signal first), supporting both Open or Encrypted (password required)
connections. It will save that connection in ESP8266 flash memory so it will 
connect automatically next time. If calling the program with /r as parameter, it
will force the AP/Router listing to show even if there is WiFi connection.

My recommendation is to use TURBO CPU Speed (switch 1 on or hit F12 to change
it).

**Known issues**

- Support only receiving files, not sending