

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity top is
	port 
	(
		-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(2 downto 1);
		dip_i					: in    std_logic_vector(8 downto 1);
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: inout std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: inout std_logic;
		sd_sw_i				: in    std_logic;

		-- Joysticks
		joy1_up_io			: inout std_logic;
		joy1_down_io		: inout std_logic;
		joy1_left_io		: inout std_logic;
		joy1_right_io		: inout std_logic;
		joy1_p6_io			: inout std_logic;
		joy1_p7_io			: inout std_logic;
		joy1_p8_io			: inout std_logic;
		
		joy2_up_io			: inout std_logic;
		joy2_down_io		: inout std_logic;
		joy2_left_io		: inout std_logic;
		joy2_right_io		: inout std_logic;
		joy2_p6_io			: inout std_logic;
		joy2_p7_io			: inout std_logic;
		joy2_p8_io			: inout std_logic;
	
	
		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- External Slots
		slot_A_o				: inout   std_logic_vector(15 downto 0)	:= (others => 'Z');
		slot_D_io			: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		slot_CS1_o			: inout   std_logic 							:= 'Z';
		slot_CS2_o			: inout   std_logic 							:= 'Z';
		slot_CLOCK_o		: inout   std_logic 							:= 'Z';		
		slot_M1_o			: inout   std_logic 							:= 'Z';		
		slot_MREQ_o			: inout   std_logic 							:= 'Z';		
		slot_IOREQ_o		: inout   std_logic 							:= 'Z';		
		slot_RD_o			: inout   std_logic 							:= 'Z';		
		slot_WR_o			: inout   std_logic 							:= 'Z';		
		slot_RESET_io		: inout std_logic 							:= 'Z';		
		slot_SLOT1_o		: inout   std_logic 							:= 'Z';		
		slot_SLOT2_o		: inout   std_logic 							:= 'Z';			
		slot_SLOT3_o		: inout   std_logic 							:= 'Z';	
		slot_BUSDIR_i		: inout    std_logic 						:= 'Z';	
		slot_RFSH_i			: inout    std_logic 						:= 'Z';	
		slot_INT_i			: inout    std_logic 						:= 'Z';	
		slot_WAIT_i			: inout    std_logic 						:= 'Z';	
		
		slot_DATA_OE_o		: out std_logic 								:= 'Z';	
		slot_DATA_DIR_o	: out std_logic 								:= 'Z';	

		-- HDMI video
		hdmi_pclk			: out std_logic		:= 'Z';  
		hdmi_de				: out std_logic		:= 'Z';  
		hdmi_int				: in  std_logic		:= 'Z';  
		hdmi_rst				: out std_logic		:= '1';  
		
		-- HDMI audio
		aud_sck				: out std_logic		:= 'Z';  
		aud_ws				: out std_logic		:= 'Z';  
		aud_i2s				: out std_logic		:= 'Z';  
		
		-- HDMI programming
		hdmi_sda				: inout std_logic		:= 'Z';  
		hdmi_scl				: inout std_logic		:= 'Z';  
		
		--ESP
		esp_rx_o				: out std_logic		:= 'Z'; 
		esp_tx_i				: in  std_logic		:= 'Z'
		

	);
end entity;

architecture Behavior of top is
	
	

	component hdmi_config is
	port
	(
		--	Host Side
		iCLK 		: in std_logic;
		iRST_N 	: in std_logic;
		
		dvi_mode : in std_logic;
		audio_96k : in std_logic;

		--	I2C Side
		I2C_SCL : out std_logic;
		I2C_SDA : inout std_logic
	);
	end component;
	
	component i2s is
	--generic(
	--	CLK_RATE   <= 50000000,
	--	AUDIO_DW   <= 16,
	--	AUDIO_RATE <= 96000
	--)
	port
	(
		reset	: in std_logic;
		clk_sys	: in std_logic;
		half_rate	: in std_logic;

		sclk : out std_logic;
		lrclk : out std_logic;
		sdata : out std_logic;

		left_chan : in std_logic_vector(15 downto 0);
		right_chan : in std_logic_vector(15 downto 0)
	);
	end component;
	
	-- clocks	
	signal clk_hdmi			: std_logic;		
	signal clk_sdram			: std_logic;		
	signal clk21m				: std_logic;

	-- Reset signal
	signal reset_n_s			: std_logic;		-- Reset geral
	signal power_on_s			: std_logic_vector(27 downto 0)	:= (others => '1');
	signal power_on_reset 	: std_logic := '1';
	
	-- DIPS: similar to Zemmix
	-- bit 7 - 0 = SD enable, 1 = disable
	-- bit 6 - 0 = 4096kb mapper, 1 = 2048kb mapper 
	-- bit 5-4 - config slot 2 "00" - Cart in Slot 2; "10" -SCC2; "01" -ASC8K; "11" -ASC16K
	-- bit 3 - config slot 1 = "0" = cart in slot 1, '1' = megaSCC+ 1024kb
	-- bit 2-1 - video 
	-- bit 0 - cpu speed = 1 = normal - 0 = turbo
	
	
	signal dip_s				: std_logic_vector(7 downto 0) := "00100001";		-- Attention! Inverted bits (0 = enabled)
	
	-- VGA
	signal vga_r_s				: std_logic_vector(5 downto 0)	:= (others => '0');
	signal vga_g_s				: std_logic_vector(5 downto 0)	:= (others => '0');
	signal vga_b_s				: std_logic_vector(5 downto 0)	:= (others => '0');
	signal vga_hsync_n_s		: std_logic								:= '1';
	signal vga_vsync_n_s		: std_logic								:= '1';
	signal blank_s				: std_logic;
	
	--audio
	signal SL_s					: std_logic_vector(5 downto 0)	:= (others => '0');
	signal SR_s					: std_logic_vector(5 downto 0)	:= (others => '0');
	signal pcm_s				: std_logic_vector(15 downto 0)	:= (others => '0');	
	signal hdmi_snd_s			: std_logic_vector(15 downto 0)	:= (others => '0');	
		
	-- slot
	signal cpu_ioreq_s		: std_logic;
	signal cpu_mreq_s			: std_logic;
	signal cpu_rd_s			: std_logic;
	signal slot_SLOT1_s		: std_logic;
	signal slot_SLOT2_s		: std_logic;
	signal BusDir_s			: std_logic;
	
	-- MIDI
	signal midi_o_s 			: std_logic := 'Z';
	signal midi_active_s 	: std_logic := 'Z';
	signal joy2_up_s 			: std_logic := 'Z';

	-- scanlines
	signal scanlines_en_s		: std_logic := '0';
	signal btn_scan_s				: std_logic;
	signal odd_line_s				: std_logic := '0';
	signal vga_r_out_s			: std_logic_vector( 4 downto 0);
	signal vga_g_out_s			: std_logic_vector( 4 downto 0);
	signal vga_b_out_s			: std_logic_vector( 4 downto 0);

	
begin

	joy1_p8_io <= '0';
	joy2_p8_io <= '0';
		  

 	reset_n_s <= btn_n_i(1) and power_on_reset;
  
--	U00 : work.pll
--	  port map(
--		inclk0   => clock_50_i,              
--		c0       => clk21m,                 -- 21.48MHz internal
--		c1       => memclk,                 -- 85.92MHz = 21.48MHz x 4
--		c2       => pMemClk,                -- 85.92MHz external
--		c3			=> clk_hdmi						-- 107.40Mhz = 21.48MHz x 5
--	  );

	ocm: work.emsx_top
	generic map
	(
		use_wifi_g			=> true,
		ZemmixNeo   		=> '1'
	)
	port map
	(
        -- Clock, Reset ports
        pClk21m         => clock_50_i,
        pExtClk         => '0',


        -- SD-RAM ports
		  pMemClk				=> clk_sdram,			-- SD-RAM Clock
        pMemCke         	=> sdram_cke_o,   -- SD-RAM Clock enable
        pMemCs_n        	=> sdram_cs_o,    -- SD-RAM Chip select
        pMemRas_n       	=> sdram_ras_o,   -- SD-RAM Row/RAS
        pMemCas_n       	=> sdram_cas_o,   -- SD-RAM /CAS
        pMemWe_n        	=> sdram_we_o,    -- SD-RAM /WE
        pMemUdq         	=> sdram_dqm_o(1),-- SD-RAM UDQM
        pMemLdq         	=> sdram_dqm_o(0),-- SD-RAM LDQM
        pMemBa1         	=> sdram_ba_o(1), -- SD-RAM Bank select address 1
        pMemBa0         	=> sdram_ba_o(0), -- SD-RAM Bank select address 0
        pMemAdr         	=> sdram_ad_o,		-- SD-RAM Address
        pMemDat         	=> sdram_da_io,   -- SD-RAM Data
		
        -- PS/2 keyboard ports
        pPs2Clk         	=> ps2_clk_io,
        pPs2Dat         	=> ps2_data_io,
		
        -- Joystick ports (Port_A, Port_B)
		  pJoyA(5)			=>	joy1_p7_io,
		  pJoyA(4)			=> joy1_p6_io,
		  pJoyA(3)			=>	joy1_right_io,
		  pJoyA(2)			=>	joy1_left_io,
		  pJoyA(1)			=>	joy1_down_io,
		  pJoyA(0)			=>	joy1_up_io,
	
		  pJoyB(5)			=>	joy2_p7_io,
		  pJoyB(4)			=> joy2_p6_io,
		  pJoyB(3)			=>	joy2_right_io,
		  pJoyB(2)			=>	joy2_left_io,
		  pJoyB(1)			=>	joy2_down_io,
		  pJoyB(0)			=>	joy2_up_s,
		
        pStrA           => open,
        pStrB           => open,
	  
        -- SD/MMC slot ports
		  pSd_Ck          => sd_sclk_o,             	-- pin 5 Clock
        pSd_Cm          => sd_mosi_o,             	-- pin 2 Datain
		  pSd_Dt(3)			=> sd_cs_n_o,					-- pin 1 CS
		  pSd_Dt(2)			=> open,
		  pSd_Dt(1)			=> open,
		  pSd_Dt(0)			=> sd_miso_i,					-- pin 7 Dataout


        -- DIP switch, Lamp ports
        pDip            => dip_i,

        -- Video, Audio/CMT ports
        pDac_VR         => vga_r_s,
        pDac_VG         => vga_g_s,
        pDac_VB         => vga_b_s,
		  
        pDac_SL   	   => SL_s,
        pDac_SR	      => SR_s,


        pVideoHS_n      => vga_hsync_n_s,
        pVideoVS_n      => vga_vsync_n_s,
		  

		  
		  -- MSX cartridge slot ports
        pCpuClk         => slot_CLOCK_o, 
        pSltRst_n       => reset_n_s,-- slot_RESET_io,
		  
        pSltAdr         => slot_A_o,
        pSltDat         => slot_D_io,        

        pSltMerq_n      => cpu_mreq_s,
        pSltIorq_n      => cpu_ioreq_s,
        pSltRd_n        => cpu_rd_s,
        pSltWr_n        => slot_WR_o,
		  
        pSltRfsh_n      => slot_RFSH_i,
        pSltWait_n      => slot_WAIT_i,
        pSltInt_n       => slot_INT_i,
        pSltM1_n        => slot_M1_o,	
		  
        pSltBdir_n      => slot_BUSDIR_i, -- not used
		  pSltSltsl_n     => slot_SLOT1_s,
        pSltSlts2_n     => slot_SLOT2_s,
        pSltCs1_n       => slot_CS1_o,
        pSltCs2_n       => slot_CS2_o,
		  
		  BusDir_o 			=> BusDir_s,

		  --others
		  pSltClk			=> '0',
		  pIopRsv14       => '0',
        pIopRsv15       => '0',
        pIopRsv16       => '0',
        pIopRsv17       => '0',
        pIopRsv18       => '0',
        pIopRsv19       => '0',
        pIopRsv20       => '0',
        pIopRsv21       => '0',
		  
		  --SM-X and Multicore 2
		  clk21m_out 		=> clk21m,
		  clk_hdmi 			=> clk_hdmi,
		  esp_rx_o			=> esp_rx_o,
		  esp_tx_i			=> esp_tx_i,	  
		  pcm_o				=> PCM_s,
		  blank_o			=> blank_s,
		  ear_i				=> ear_i,
		  mic_o				=> mic_o,
		  midi_o				=> midi_o_s,
		  midi_active_o	=> midi_active_s
		  	
    );
	 
	 joy2_up_s <= joy2_up_io; 
	 joy2_up_io <= midi_o_s when midi_active_s = '1' else 'Z';
	 
	slot_IOREQ_o <= cpu_ioreq_s; 
	slot_MREQ_o	 <= cpu_mreq_s; 
	slot_RD_o	 <= cpu_rd_s;
	slot_SLOT1_o <= slot_SLOT1_s;
	slot_SLOT2_o <= slot_SLOT2_s;
	slot_SLOT3_o <= slot_SLOT1_s;
	
	-- RESET to the SLOT pins
	slot_RESET_io <= reset_n_s;

	-- 74LVC4245
	slot_DATA_OE_o	 <= '0' when slot_SLOT1_s = '0' else
							 '0' when slot_SLOT2_s = '0' else
							 '0' when cpu_ioreq_s = '0' and BusDir_s = '0' else	
							 '1';
	slot_DATA_DIR_o <= not cpu_rd_s; -- port A=SLOT, B=FPGA     DIR(1)=A to B 	
	 
	sdram_clk_o <= clk_sdram;
	
	vga_r_o				<= vga_r_out_s;
	vga_g_o				<= vga_g_out_s;
	vga_b_o				<= vga_b_out_s;
	vga_hsync_n_o		<= vga_hsync_n_s;
	vga_vsync_n_o		<= vga_vsync_n_s;
	
	--saida left gera pequena interferencia no video
	dac_l_o <= SL_s(0); --and btn_n_i(2);
	dac_r_o <= SR_s(0); --and btn_n_i(2);
	
		-- power on reset
		process (clk_sdram)
		begin
			if rising_edge(clk_sdram) then
				if power_on_s /= x"00" then
					power_on_s <= power_on_s - 1;
					power_on_reset <= '0';
				else
					power_on_reset <= '1';
				end if;
				
			end if;
		end process;
		


		------------------------------------------------------------------
		-- HDMI 
		------------------------------------------------------------------

		hdmi_pclk			<= clk21m;  
		hdmi_de				<= (not blank_s);
		hdmi_rst				<= power_on_reset; 


		-- HDMI configuration
		hdmi_config1 : hdmi_config
		port map
		(
			iCLK		=> clk21m,
			iRST_N	=> '1',

			I2C_SCL	=> hdmi_scl,
			I2C_SDA	=> hdmi_sda,

			dvi_mode	=> '0',
			audio_96k=> '0'
		);
	
	i2s1 : i2s
	port map
	(
		clk_sys		=> clk21m,
		reset			=> not  power_on_reset, --reseta em 1

		half_rate	=>  '0' ,

		sclk			=> aud_sck,
		lrclk			=> aud_ws,
		sdata			=> aud_i2s,


		left_chan  => hdmi_snd_s,
		right_chan => hdmi_snd_s

	);
	
	hdmi_snd_s <= "0" & PCM_s(15 downto 1);
	
	---------------------------------
	-- scanlines
	---------------------------------
	
	btnscl: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clk21m,
		button_i			=> btn_n_i(2),
		result_o			=> btn_scan_s
	);
	
	process (btn_scan_s)
	begin
		if falling_edge(btn_scan_s) then
			scanlines_en_s <= not scanlines_en_s;
		end if;
	end process;
	
	vga_r_out_s <=  '0' & vga_r_s(5 downto 2) when scanlines_en_s = '1' and odd_line_s = '1' else vga_r_s(5 downto 1);
	vga_g_out_s <=  '0' & vga_g_s(5 downto 2) when scanlines_en_s = '1' and odd_line_s = '1' else vga_g_s(5 downto 1);
	vga_b_out_s <=  '0' & vga_b_s(5 downto 2) when scanlines_en_s = '1' and odd_line_s = '1' else vga_b_s(5 downto 1);

	
	process(vga_hsync_n_s,vga_vsync_n_s)
	begin
		if vga_vsync_n_s = '0' then
			odd_line_s <= '0';
		elsif rising_edge(vga_hsync_n_s) then
			odd_line_s <= not odd_line_s;
		end if;
	end process;



end architecture;
