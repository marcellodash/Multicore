
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.all;

-- -----------------------------------------------------------------------

entity genlock_top is
	generic 
	(	
		desloc_x_i 	 	: in signed (8 downto 0);
		desloc_y_i 	 	: in signed (8 downto 0)
	--	pixel_size_i 	: integer
	);
	port 
	(
	
	CLOCK_SYNC		: in std_logic; -- 126mhz
	CLOCK_VGA		: in std_logic; -- 25.2 Mhz
	CLOCK_PIXEL		: in std_logic; -- 126 MHz 45o
		
	clock_system	: in std_logic; 

	rgb_15 			: in std_logic_vector(14 downto 0);
	hsync_15 		: in std_logic;
   vsync_15 		: in std_logic;
	
	-- OUTs
	VGA_R			: out std_logic_vector(4 downto 0);
	VGA_G			: out std_logic_vector(4 downto 0);
	VGA_B			: out std_logic_vector(4 downto 0);
	VGA_HS		: out std_logic;
	VGA_VS		: out std_logic;
	VGA_BLANK	: out std_logic;
	
	-- to external SDRAM 
	SDRAM_AD		: out std_logic_vector(12 downto 0);
	SDRAM_DA		: inout std_logic_vector(15 downto 0);

	SDRAM_BA		: out std_logic_vector(1 downto 0);
	SDRAM_DQM	: out std_logic_vector(1 downto 0);

	SDRAM_RAS	: out std_logic;
	SDRAM_CAS	: out std_logic;
	SDRAM_CKE	: out std_logic;
	SDRAM_CLK	: out std_logic;
	SDRAM_CS		: out std_logic;
	SDRAM_WE		: out std_logic
	
	);
end entity;

-- -----------------------------------------------------------------------

architecture rtl of genlock_top is



	COMPONENT sdram
	PORT
	(
			clk : IN STD_LOGIC;
		 rowStoreReq : IN STD_LOGIC;
		 rowLoadReq : IN STD_LOGIC;
		 pixelIn : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 pMemDat : INOUT STD_LOGIC_VECTOR(15 DOWNTO 0);
		 rowLoadNr : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 rowStoreNr : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 rowStoreAck : OUT STD_LOGIC;
		 rowLoadAck : OUT STD_LOGIC;
		 pMemClk : OUT STD_LOGIC;
		 pMemCke : OUT STD_LOGIC;
		 pMemCs_n : OUT STD_LOGIC;
		 pMemRas_n : OUT STD_LOGIC;
		 pMemCas_n : OUT STD_LOGIC;
		 pMemWe_n : OUT STD_LOGIC;
		 pMemUdq : OUT STD_LOGIC;
		 pMemLdq : OUT STD_LOGIC;
		 pMemBa1 : OUT STD_LOGIC;
		 pMemBa0 : OUT STD_LOGIC;
		 wren_sdr : OUT STD_LOGIC;
		 colLoadNr : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 colStoreNr : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 pixelOut : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
		 pMemAdr : OUT STD_LOGIC_VECTOR(12 DOWNTO 0)
	);
	END COMPONENT;

	COMPONENT genlock
	generic 
	(	
		desloc_x_i 	 	: in signed (8 downto 0);
		desloc_y_i 	 	: in signed (8 downto 0)--;
		--pixel_size_i 	: integer
	);
	PORT
	(
		clock_pixel : IN STD_LOGIC;
		clock_system	: in std_logic; 
		 vsync : IN STD_LOGIC;
		 hblank : IN STD_LOGIC;
		 store_ack : IN STD_LOGIC;
		 clock_dram : IN STD_LOGIC;
		 store_req : OUT STD_LOGIC;
		 wren_pixel : OUT STD_LOGIC;
		 col_number : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 row_number : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
			hcount_o 	: out unsigned(13 downto 0);
			column_i		: integer
		 
	);
	END COMPONENT;





COMPONENT vgaout
GENERIC (
			hor_active_video : INTEGER;
			hor_back_porch : INTEGER;
			hor_front_porch : INTEGER;
			hor_sync_pulse : INTEGER;
			vert_active_video : INTEGER;
			vert_back_porch : INTEGER;
			vert_front_porch : INTEGER;
			vert_sync_pulse : INTEGER
			);
	PORT(clock_vga : IN STD_LOGIC;
		 load_ack : IN STD_LOGIC;
		 scanline : IN STD_LOGIC;
		 clock_dram : IN STD_LOGIC;
		 pixel_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 load_req : OUT STD_LOGIC;
		 col_number : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 row_number : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 vga_rgb_out : OUT STD_LOGIC_VECTOR(14 DOWNTO 0);
 		 vga_hs_out : OUT STD_LOGIC;
 		 vga_vs_out : OUT STD_LOGIC;
		 vga_blank : OUT STD_LOGIC
		 
	);
END COMPONENT;


	-- clocks
	signal atari_vid_clk: std_logic := '0';
	signal atari_sys_clk : std_logic := '0';
	signal clock_vga_s : std_logic := '0';
	signal clock_sdram_s : std_logic := '0';
	

	
	--
	signal vga_blank_s	: std_logic;
	signal clk_dvdr : std_logic := '0';
	signal vid_clk_dvdr: unsigned(3 downto 0) := "0000";





SIGNAL	COL_LOAD_NR :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	COL_SDR_RD :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	COL_SDR_WR :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	COL_STORE_NR :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	HBLANK :  STD_LOGIC;
SIGNAL	MAIN_CLK :  STD_LOGIC;
SIGNAL	PIXEL_IN :  STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL	PIXEL_OUT :  STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL	ROW_LOAD_ACK :  STD_LOGIC;
SIGNAL	ROW_LOAD_NR :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	ROW_LOAD_REQ :  STD_LOGIC;
SIGNAL	ROW_STORE_ACK :  STD_LOGIC;
SIGNAL	ROW_STORE_NR :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	ROW_STORE_REQ :  STD_LOGIC;
SIGNAL	SCANLINE :  STD_LOGIC;
SIGNAL	SDR_DATA :  STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL	SDR_Q :  STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL	VGA_OUT :  STD_LOGIC_VECTOR(14 DOWNTO 0);
SIGNAL	VIDEO_ACTIVE :  STD_LOGIC;
SIGNAL	WREN_PIXEL :  STD_LOGIC;
SIGNAL	WREN_SDR :  STD_LOGIC;


signal sound_left		: std_logic_vector(15 downto 0);
signal sound_right	: std_logic_vector(15 downto 0);



signal btn_up_state_s 	: std_logic;
signal btn_down_state_s 	: std_logic;
signal btn_left_state_s 	: std_logic;
signal btn_right_state_s 	: std_logic;
signal btn_up_s 			: std_logic;
signal btn_down_s 			: std_logic;
signal btn_left_s 			: std_logic;
signal btn_right_s 			: std_logic;

signal vga_out_s	: std_logic_vector(8 downto 0);
signal OSDBit_s: std_logic;
signal videoConfigShow: std_logic := '1';
signal videoConfigDim: std_logic := '1';


signal osd_mode : unsigned(1 downto 0) := (others=>'0');
signal osd_value : signed (8 downto 0) := (others=>'0');	

signal bright_s : integer := 0;

signal temp_R	: std_logic_vector(7 downto 0);
signal temp_G	: std_logic_vector(7 downto 0);
signal temp_B	: std_logic_vector(7 downto 0);

signal vga_hs_s	: std_logic;
signal vga_vs_s	: std_logic;

signal column_s : integer;
signal hcount_s 	: unsigned(13 downto 0);

BEGIN 


b2v_input_detect : work.input_detect
PORT MAP
(
	clock_pixel_i => CLOCK_SYNC,
	hsync_i => hsync_15, 
	column_o => column_s,
	hblank_o => HBLANK,
	hcount_i	=> hcount_s
);



b2v_dram : sdram
PORT MAP
(
		clk => CLOCK_PIXEL,
		rowStoreReq => ROW_STORE_REQ,
		rowLoadReq => ROW_LOAD_REQ,
		rowLoadNr => ROW_LOAD_NR,
		rowStoreNr => ROW_STORE_NR,
		rowStoreAck => ROW_STORE_ACK,
		rowLoadAck => ROW_LOAD_ACK,
		--
		colStoreNr => COL_SDR_RD,
		pixelIn => SDR_DATA,
		--
		wren_sdr => WREN_SDR,
		colLoadNr => COL_SDR_WR,
		pixelOut => SDR_Q,
		--
		pMemClk => SDRAM_CLK,
		pMemCke => SDRAM_CKE,
		pMemAdr => SDRAM_AD,
		pMemDat => SDRAM_DA,
		pMemCs_n => SDRAM_CS,
		pMemWe_n => SDRAM_WE,
		pMemRas_n => SDRAM_RAS,
		pMemCas_n => SDRAM_CAS,
		pMemUdq => SDRAM_DQM(1),
		pMemLdq => SDRAM_DQM(0), 
		pMemBa1 => SDRAM_BA(1), 
		pMemBa0 => SDRAM_BA(0)  

);

	

b2v_genlock : genlock
generic map
(
	desloc_x_i 	 => desloc_x_i,
	desloc_y_i 	 => desloc_y_i
--	pixel_size_i => pixel_size_i 
)
PORT MAP
(
	clock_pixel => CLOCK_PIXEL,
	clock_dram 	=> CLOCK_PIXEL,
	clock_system	=> clock_system,
	--
	vsync 		=> vsync_15, --VSYNC, --vsync input
	hblank 		=> HBLANK, -- hblank input
	--
	store_req 	=> ROW_STORE_REQ, -- out: signal to send the data to sdram at hblank period
	store_ack 	=> ROW_STORE_ACK, -- in: signal that sdram tell the store is done
	--
	wren_pixel 	=> WREN_PIXEL,		-- out: write signal
	col_number 	=> COL_STORE_NR,	-- out: write addr
	row_number 	=> ROW_STORE_NR ,  -- out: current row number
	hcount_o		=> hcount_s,
	column_i		=> column_s

);




from_sdram : work.ram2
PORT MAP
(
	wrclock 		=> CLOCK_PIXEL,
	wraddress 	=> COL_SDR_WR,
	data 			=> SDR_Q,
	wren 			=> WREN_SDR,
	--
	rdclock 		=> CLOCK_VGA,
	rdaddress 	=> COL_LOAD_NR,
	q 				=> PIXEL_IN
);

to_sdram : work.ram2
PORT MAP
(
	wrclock 		=> clock_system,
	wraddress 	=> COL_STORE_NR,
	data 			=> '0' & rgb_15(14 downto 10) & rgb_15(9 downto 5) & rgb_15(4 downto 0), --PIXEL_OUT,
	wren 			=> WREN_PIXEL,
	---
	rdclock 		=> CLOCK_PIXEL,
	rdaddress 	=> COL_SDR_RD,
	q 				=> SDR_DATA
);


b2v_vgaout : vgaout
GENERIC MAP
(
	hor_active_video => 640,
	hor_back_porch => 48,
	hor_front_porch => 16,
	hor_sync_pulse => 96,
	vert_active_video => 480,
	vert_back_porch => 33,
	vert_front_porch => 10,
	vert_sync_pulse => 2
)
PORT MAP
(
	clock_vga => CLOCK_VGA,
	load_ack => ROW_LOAD_ACK,
	scanline => SCANLINE,
	clock_dram => CLOCK_PIXEL,
	pixel_in => PIXEL_IN,
	load_req => ROW_LOAD_REQ,
	col_number => COL_LOAD_NR,
	row_number => ROW_LOAD_NR,
	vga_rgb_out => VGA_OUT,
	vga_hs_out => vga_hs_s,
	vga_vs_out => vga_vs_s,
	vga_blank => vga_blank_s
);


SCANLINE <= '1'; --FP0; -- optional scanlines





VGA_R 	<= VGA_OUT(14 downto 10);
VGA_G		<= VGA_OUT(9 downto 5);
VGA_B 	<= VGA_OUT(4 downto 0);
VGA_HS 	<= vga_hs_s;
VGA_VS 	<= vga_vs_s;
VGA_BLANK <= vga_blank_s;



-- -----------------------------------------------------------------------
-- A2601 core
-- -----------------------------------------------------------------------

			--vid_clk => atari_vid_clk,

	


end architecture;
