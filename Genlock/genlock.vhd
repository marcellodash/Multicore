library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity genlock is
	generic 
	(	
		desloc_x_i 	 	: in signed (8 downto 0);
		desloc_y_i 	 	: in signed (8 downto 0)
		--pixel_size_i 	: integer
	);
    port
	 (
			clock_pixel : in std_logic;
			clock_system	: in std_logic; 
			vsync  		: in std_logic; -- digital vsync
			hblank 		: in std_logic; -- digital hsync
			row_number	: out unsigned(9 downto 0); 
			col_number	: buffer unsigned(9 downto 0); 
			store_req	: out std_logic := '0';
			store_ack 	: in std_logic;
			wren_pixel	: out std_logic;
			clock_dram  : in std_logic;
			hcount_o 	: out unsigned(13 downto 0);
			column_i		: integer
);
			
end genlock;

architecture behavioral of genlock is

signal vblank: 	std_logic;

signal hcount, vcount							: unsigned(13 downto 0);
signal top_border									: integer := 32;
signal front_porch								: integer := 181;






signal column: 	integer range 0 to 2048;



begin

hcount_o <= hcount;

hraster: process (clock_pixel, hblank, vblank)
begin
	if (hblank = '0' or vblank = '0') then
		hcount <= (others => '0');
	elsif (rising_edge(clock_pixel)) then
		hcount <= hcount + 1;		
	end if;
end process;
		

vsync_lock: process(clock_pixel, vsync)
variable sync: std_logic;
begin	
	if (rising_edge(clock_pixel)) then		

		vblank <= '1'; 					
		if (vsync = '0' and vcount > 261) then
		
			if (vcount > 290) then
				top_border <= 42 + to_integer(desloc_y_i);
			else
				top_border <= 16 + to_integer(desloc_y_i);
			end if;	
			
			vblank <= '0';
		end if;								
	end if;	

end process;

vraster: process (clock_pixel, vblank)
begin
	if (vblank = '0') then 
			vcount <= (others => '0');
	elsif(rising_edge(clock_pixel)) then
		if hblank = '0' then
			vcount <= vcount + 1;
		end if;
	end if;
end process;


process_col_nr: process(clock_system, hcount, column, hblank) 
variable row, col: integer range 0 to 2048;
variable p_count : integer range 0 to 64;
begin
	if (hblank = '0')then-- or vblank = '0') then
		column <= 0;
		p_count := 0;
	elsif (rising_edge(clock_system)) then
	
		--if (hcount(2 downto 0) = "111") then --pixel width larger number smaller pixel
		--	column <= column + 1;
		--end if;
		
	--	if p_count /= pixel_size_i then
	--		p_count := p_count + 1;
	--	else
			column <= column + 1;
	--		p_count := 0;
	--	end if;
		
		
		wren_pixel <= '0';
		
		if (column >= front_porch and column < 641+front_porch and vcount >= top_border and vcount < top_border+241) then
			-- user active window
			col := column - front_porch;
			row := to_integer(vcount) - top_border;					

			row_number <= to_unsigned(row, row_number'length);
			col_number <= to_unsigned(col, col_number'length);	
			
			wren_pixel <= '1';
		end if;
		
	end if;
end process;


store_row: process(clock_dram, hblank, store_ack)
begin	
	if (store_ack = '1') then -- store_ack is asynchronous
			store_req <= '0';
	elsif (rising_edge(clock_dram)) then
		if (hblank = '0') then
			store_req <= '1'; -- store_req is on clock_pixel
		end if;
	end if;	
end process;


front_porch <= 128 + to_integer(desloc_x_i);
		
									
end behavioral;