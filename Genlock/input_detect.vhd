library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity input_detect is
	
    port
	 (
			clock_pixel_i : in std_logic;
			hsync_i	 		: in std_logic; 
			column_o		: out integer;
			hblank_o 	: out std_logic;
			hcount_i		: in unsigned (13 downto 0)
);
			
end input_detect;

architecture behavioral of input_detect is

signal hblank_s : std_logic;
signal column: 	integer range 0 to 2048;

begin

hblank_o <= hblank_s;
column_o <= column;

horizontal: process(clock_pixel_i, hsync_i)
variable horsync : std_logic;

begin
	if (rising_edge(clock_pixel_i)) then
		
		hblank_s <= '1';		
		
		
		if (hsync_i /= horsync ) then
		
			horsync := hsync_i;
			
			if (hsync_i = '0') then
				hblank_s <= '0';
			end if;
		
					
		end if;
		
	end if;
end process;

process_column: process(clock_pixel_i, hcount_i, column, hblank_s) 
variable p_count : integer range 0 to 64;
begin
	if (hblank_s = '0') then
		column <= 0;
	elsif (rising_edge(clock_pixel_i)) then
	
		if (hcount_i(2 downto 0) = "111") then --pixel width larger number smaller pixel
			column <= column + 1;
		end if;
		
--		if p_count /= pixel_size_i then
--			p_count := p_count + 1;
--		else
--			column <= column + 1;
--			p_count := 0;
--		end if;

	end if;
end process;		


end behavioral;
