library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity vgaout is
	generic
	(
		hor_active_video		: integer := 640;
		hor_front_porch		: integer := 16;
		hor_sync_pulse			: integer := 96;
		hor_back_porch			: integer := 48;

		vert_active_video		: integer := 480;
		vert_front_porch		: integer := 10;
		vert_sync_pulse		: integer := 2;
		vert_back_porch		: integer := 33		
	);
	port
	(
		clock_vga  : in std_logic;
		vga_rgb_out	  : out std_logic_vector(14 downto 0); 
		vga_hs_out	  : out std_logic; 
		vga_vs_out	  : out std_logic;
							
		pixel_in		: in std_logic_vector(15 downto 0);		
		row_number	: buffer unsigned(9 downto 0);
		col_number	: buffer unsigned(9 downto 0); 
		load_req		: out std_logic := '0';
		load_ack  	: in std_logic;

		scanline	: in std_logic;
		
		clock_dram: std_logic;
		
		vga_blank : out std_logic
		
	 );
end vgaout;

architecture behavioral of vgaout is

signal hcount												: unsigned(13 downto 0);
signal vcount												: unsigned(9 downto 0);
signal videov, videoh, hsync, vsync					: std_logic;
signal pixel		  										: std_logic_vector(14 downto 0);
	
begin

		vcounter: process (clock_vga, hcount, vcount)
		begin
			if(rising_edge(clock_vga)) then

				if hcount = (hor_active_video + hor_front_porch + hor_sync_pulse + hor_back_porch - 1) then
					vcount <= vcount + 1;
				end if;
				
				if vcount = (vert_active_video + vert_front_porch + vert_sync_pulse + vert_back_porch - 1) and hcount = (hor_active_video + hor_front_porch + hor_sync_pulse + hor_back_porch - 1) then 
					vcount <= (others => '0');
				end if;
				
			end if;
		end process;

		v_sync: process(clock_vga, vcount)
		begin
			if(rising_edge(clock_vga)) then
				vsync <= '1';
				if (vcount <= (vert_active_video + vert_front_porch + vert_sync_pulse - 1) and vcount >= (vert_active_video + vert_front_porch - 1)) then
					vsync <= '0';
				end if;
			end if;
		end process;

		hcounter: process (clock_vga, hcount, vcount)
		begin
			if (rising_edge(clock_vga)) then				
				hcount <= hcount + 1;
				
				if hcount = (hor_active_video + hor_front_porch + hor_sync_pulse + hor_back_porch - 1)	then 
				  hcount <= (others => '0');
				  col_number <= to_unsigned(0, col_number'length);
				else
					col_number <= to_unsigned(to_integer(hcount(9 downto 0)) + 1, col_number'length);
				end if;	
				
				if (hcount > hor_active_video or vcount > vert_active_video) then
					vga_blank <='1';
				else
					vga_blank <='0';
				end if;
				
			end if;
		end process;


		h_sync: process (clock_vga, hcount)
		variable row : integer range 0 to 1024;
		begin
			if (rising_edge(clock_vga)) then     
				hsync <= '1';				
				
				row := to_integer(vcount(9 downto 1)) + 1;		
				row_number <= to_unsigned(row, row_number'length);
				
				if (hcount <= (hor_active_video + hor_front_porch + hor_sync_pulse - 1) and hcount >= (hor_active_video + hor_front_porch - 1)) then
				  hsync <= '0';
				end if;
			end if;		
		end process;

		load_row: process(clock_dram, load_ack, hsync)
		begin
			if (load_ack = '1') then
				load_req <= '0';
			elsif (rising_edge(clock_dram)) then
				if (hcount = (hor_active_video + hor_front_porch - 1)) then --hsync = '0') then
					load_req <= '1';
				end if;
			end if;
		end process;


		video: process(clock_vga, hcount, vcount) 
		variable blank: std_logic;
		variable vga_pixel: std_logic_vector(14 downto 0);
		begin
			if (rising_edge(clock_vga)) then

				blank := videoh and videov;		
				
				vga_pixel := pixel_in(14 downto 0); 

				if (scanline = '0' and vcount(0) = '0') then
					vga_pixel := '0' & vga_pixel(14 downto 11) & '0' & vga_pixel(9 downto 6) & '0' & vga_pixel(4 downto 1);
				end if;		

				if (blank = '1') then
					pixel <= vga_pixel;
				else
					pixel <= (others=>'0');
				end if;

			end if;
		end process;



		process (clock_vga, vcount)
		begin
			if (rising_edge(clock_vga)) then
				videov <= '1'; 
				if vcount > vert_active_video-1 or vcount = 0 then 
					videov <= '0';
				end if;	
			end if;
		end process;


		process (clock_vga, hcount)
		begin
			if (rising_edge(clock_vga)) then
				videoh <= '1';
				if hcount > hor_active_video or hcount < 3 then
					videoh <= '0';
				end if;
			end if;
		end process;
		
		vga_rgb_out <= pixel;
		vga_hs_out	<= hsync;
      vga_vs_out	<= vsync;

end behavioral;