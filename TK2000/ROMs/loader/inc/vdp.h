
#ifndef _VDP_H
#define _VDP_H

#define POKE(addr,val)     (*(unsigned char*) (addr) = (val))
#define POKEW(addr,val)    (*(unsigned*) (addr) = (val))
#define PEEK(addr)         (*(unsigned char*) (addr))
#define PEEKW(addr)        (*(unsigned*) (addr))

#define VRAM_ADDR	0x2000

void vdp_cls(void);
void vdp_gotoxy(unsigned char x, unsigned char y);
void vdp_putcharxy(unsigned char x, unsigned char y, unsigned char c);
void vdp_putchar(unsigned char c);
void vdp_putstring(char *s);

#endif	/* _VDP_H */