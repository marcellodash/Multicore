-------------------------------------------------------------------------------
--
-- TK2000 project
--
-- Copyright (c) 2016, Fabio Belavenuto (belavenuto@gmail.com)
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- Please report bugs to the author, but before you do so, please
-- make sure that this is not a derivative work and that
-- you have the latest version of this file.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity multicore_top is
	port (
	-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= '0'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
	);
end entity;

architecture behavior of multicore_top is

	-- Clocks
	signal clock_28_s			: std_logic;		-- Dobro para scandoubler
	signal clock_14_s			: std_logic;		-- Master
	signal phi0_s				: std_logic;		-- phase 0
	signal phi1_s				: std_logic;		-- phase 1
	signal phi2_s				: std_logic;		-- phase 2
	signal clock_2M_s			: std_logic;		-- Clock Q3

	-- Resets
	signal pll_locked_s		: std_logic;
	signal por_reset_s		: std_logic;
	signal reset_s				: std_logic;

	-- ROM
	signal rom_addr_s			: std_logic_vector(13 downto 0);
	signal rom_data_from_s	: std_logic_vector(7 downto 0);
--	signal rom_oe_s			: std_logic;
--	signal rom_we_s			: std_logic;

	-- RAM
	signal ram_addr_s			: std_logic_vector(15 downto 0);
	signal ram_data_to_s		: std_logic_vector(7 downto 0);
	signal ram_data_from_s	: std_logic_vector(7 downto 0);
	signal ram_oe_s			: std_logic;
	signal ram_we_s			: std_logic;

	-- Keyboard
	signal kbd_ctrl_s			: std_logic;
	signal kbd_rows_s			: std_logic_vector(7 downto 0);
	signal kbd_cols_s			: std_logic_vector(5 downto 0);
	signal FKeys_s				: std_logic_vector(12 downto 1);

	-- Audio
	signal spk_s				: std_logic;

	-- K7
	signal cas_o_s				: std_logic;
--	signal cas_motor_s		: std_logic_vector(1 downto 0);

	-- Video
	signal video_r_s			: std_logic_vector(7 downto 0);
	signal video_g_s			: std_logic_vector(7 downto 0);
	signal video_b_s			: std_logic_vector(7 downto 0);
	signal video_ro_s			: std_logic_vector(7 downto 0);
	signal video_go_s			: std_logic_vector(7 downto 0);
	signal video_bo_s			: std_logic_vector(7 downto 0);
	signal video_color_s		: std_logic;
	signal video_bit_s		: std_logic;
	signal video_hsync_n_s	: std_logic;
	signal video_vsync_n_s	: std_logic;
	signal video_blank_s		: std_logic;
	signal video_hbl_s		: std_logic;
	signal video_vbl_s		: std_logic;
	signal video_ld194_s		: std_logic;

	-- Periferico
	signal per_iosel_n_s		: std_logic;
	signal per_devsel_n_s	: std_logic;
	signal per_we_s			: std_logic;
	signal per_addr_s			: std_logic_vector(7 downto 0);
	signal per_data_from_s	: std_logic_vector(7 downto 0)	:= (others => '0');
	signal per_data_to_s		: std_logic_vector(7 downto 0);

	-- Disk II
	signal image_num_s		: unsigned(9 downto 0)				:= (others => '0');
	signal track_num_s		: unsigned(5 downto 0);
	signal track_addr_s		: unsigned(13 downto 0);
	signal disk1_en_s			: std_logic;
	signal disk2_en_s			: std_logic;
	signal track_ram_addr_s	: unsigned(13 downto 0);
	signal track_ram_data_s	: unsigned(7 downto 0);
	signal track_ram_we_s	: std_logic;


	-- OSD
	signal osd_visible_s		: std_logic								:= '1';
   signal osd_pixel_s		: std_logic;
   signal osd_green_s		: std_logic_vector(2 downto 0);								-- OSD byte signal
   signal btn_up_s			: std_logic := '1'; 
   signal btn_down_s			: std_logic := '1'; 
	signal timer_osd_s		: unsigned(21 downto 0)				:= (others => '1');

	-- Debug
	signal D_cpu_pc_s			: std_logic_vector(15 downto 0);

begin

	-- PLL
	pll: work.pll1
	port map (
		inclk0		=> clock_50_i,
		c0				=> clock_28_s,
		c1				=> clock_14_s,
		locked		=> pll_locked_s
	);

	-- TK2000
	tk2000_inst: work.tk2000
	port map (
		clock_14_i			=> clock_14_s,
		reset_i				=> reset_s,
		-- RAM
		ram_addr_o			=> ram_addr_s,
		ram_data_to_o		=> ram_data_to_s,
		ram_data_from_i	=> ram_data_from_s,
		ram_oe_o				=> ram_oe_s,
		ram_we_o				=> ram_we_s,
		-- ROM
		rom_addr_o			=> rom_addr_s,
		rom_data_from_i	=> rom_data_from_s,
		rom_oe_o				=> open,--rom_oe_s,
		rom_we_o				=> open,--rom_we_s,
		-- Keyboard
		kbd_rows_o			=> kbd_rows_s,
		kbd_cols_i			=> kbd_cols_s,
		kbd_ctrl_o			=> kbd_ctrl_s,
		-- Audio
		spk_o					=> spk_s,
		-- Video
		video_color_o		=> video_color_s,
		video_bit_o			=> video_bit_s,
		video_hsync_n_o	=> open,
		video_vsync_n_o	=> open,
		video_hbl_o			=> video_hbl_s,
		video_vbl_o			=> video_vbl_s,
		video_ld194_o		=> video_ld194_s,
		-- Cassete
		cas_i					=> ear_i,
		cas_o					=> cas_o_s,
		cas_motor_o			=> open,--cas_motor_s,
		-- LPT
		lpt_stb_o			=> open,
		lpt_busy_i			=> '0',
		-- Periferico
		phi0_o				=> phi0_s,					-- fase 0 __|---|___|---
		phi1_o				=> phi1_s,					-- fase 1 ---|___|---|___
		phi2_o				=> phi2_s,					-- fase 2 ___|---|___|---
		clock_2m_o			=> clock_2M_s,
		read_write_o		=> per_we_s,
		irq_n_i				=> '1',
		nmi_n_i				=> '1',
		dis_rom_i			=> '1',						-- 1 enable peripheral
		io_select_n_o		=> per_iosel_n_s,
		dev_select_n_o		=> per_devsel_n_s,
		per_addr_o			=> per_addr_s,
		per_data_from_i	=> per_data_from_s,
		per_data_to_o		=> per_data_to_s,
		-- Debug
		D_cpu_pc_o			=> D_cpu_pc_s
	);

	-- Keyboard
	kb: work.keyboard
	generic map (
		clkfreq_g			=> 28000
	)
	port map (
		clock_i				=> clock_28_s,
		reset_i				=> por_reset_s,
		ps2_clk_io			=> ps2_clk_io,
		ps2_data_io			=> ps2_data_io,
		rows_i				=> kbd_rows_s,
		row_ctrl_i			=> kbd_ctrl_s,
		cols_o				=> kbd_cols_s,
		FKeys_o				=> FKeys_s
	);

	-- Audio
	audioout: entity work.Audio_DAC
	port map (
		clock_i	=> clock_28_s,
		reset_i	=> reset_s,
		spk_i		=> spk_s,
		mic_i		=> cas_o_s,
		ear_i		=> ear_i,
		dac_r_o	=> dac_r_o,
		dac_l_o	=> dac_l_o
	);
	
	-- ROM
	rom: entity work.tk2000_rom
	port map (
		clock		=> clock_28_s,
		address	=> rom_addr_s,
		q			=> rom_data_from_s
	);

	-- VGA
	vga : work.vga_controller
	port map (
		clock_28_i		=> clock_28_s,
		video_i			=> video_bit_s,
		color_i			=> video_color_s,
		hbl_i				=> video_hbl_s,
		vbl_i				=> video_vbl_s,
		ld194_i			=> video_ld194_s,
		color_type_i	=> '0',
		vga_hs_n_o		=> video_hsync_n_s,
		vga_vs_n_o		=> video_vsync_n_s,
		vga_blank_n_o	=> video_blank_s,
		vga_r_o			=> video_r_s,
		vga_g_o			=> video_g_s,
		vga_b_o			=> video_b_s
	);

	-- OSD overlay for the green channel
	osd_inst: entity work.osd
	generic map (									-- workaround for wrong video size
		C_digits			=> 3,						-- number of hex digits to show
		C_resolution_x	=> 565
	)
	port map (
		clk_pixel		=> clock_28_s,
		vsync				=> not video_vsync_n_s,	-- positive sync
		fetch_next		=> video_blank_s,			-- '1' when video_active
		probe_in			=> "00" & std_logic_vector(image_num_s),
		osd_out			=> osd_pixel_s
	);

	osd_green_s		<= (others => (osd_pixel_s and osd_visible_s));

	vga_hsync_n_o	<= video_hsync_n_s;
	vga_vsync_n_o	<= video_vsync_n_s;
	vga_r_o			<= video_r_s(7 downto 3);
	vga_g_o			<= video_g_s(7 downto 3) or osd_green_s;
	vga_b_o			<= video_b_s(7 downto 3);

	-- Button Down
	btndw: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clock_14_s,
		button_i			=> btn_n_i(1),
		result_o			=> btn_down_s
	);

	-- Button Up
 	btnup: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clock_14_s,
		button_i			=> btn_n_i(2),
		result_o			=> btn_up_s
	);

	disk : entity work.disk_ii
	port map (
		CLK_14M			=> clock_14_s,
		CLK_2M			=> clock_2M_s,
		PRE_PHASE_ZERO	=> phi0_s,
		IO_SELECT		=> not per_iosel_n_s,
		DEVICE_SELECT	=> not per_devsel_n_s,
		RESET				=> reset_s,
		A					=> unsigned(per_addr_s),
		D_IN				=> unsigned(per_data_to_s),
		std_logic_vector(D_OUT)	=> per_data_from_s,
		TRACK				=> track_num_s,
		TRACK_ADDR		=> track_addr_s,
		D1_ACTIVE		=> disk1_en_s,
		D2_ACTIVE		=> disk2_en_s,
		ram_write_addr	=> track_ram_addr_s,
		ram_di			=> track_ram_data_s,
		ram_we			=> track_ram_we_s
	);

	sdcard_interface : entity work.spi_controller
	port map (
		CLK_14M        => clock_14_s,
		RESET          => reset_s,
		CS_N           => sd_cs_n_o,
		MOSI           => sd_mosi_o,
		MISO           => sd_miso_i,
		SCLK           => sd_sclk_o,
		track          => track_num_s,
		image          => image_num_s,
		ram_write_addr => track_ram_addr_s,
		ram_di         => track_ram_data_s,
		ram_we         => track_ram_we_s
	);

	-- Glue Logic

	por_reset_s	<= '1' when pll_locked_s = '0' or (btn_n_i(2) = '0' and btn_n_i(4) = '0')								else '0';
	reset_s		<= '1' when por_reset_s = '1'  or (btn_n_i(3) = '0' and btn_n_i(4) = '0') or FKeys_s(4) = '1'	else '0';

	-- RAM
	ram_data_from_s	<= sram_data_io;

	process (por_reset_s, ram_addr_s, ram_we_s, ram_oe_s, ram_data_to_s)
	begin
		if por_reset_s = '1' then
			sram_addr_o		<= "000" & X"03F4";
			sram_we_n_o		<= '0';
			sram_oe_n_o		<= '0';
			sram_data_io	<= X"00";
		else
			sram_addr_o		<= "000" & ram_addr_s;
			sram_we_n_o		<= not ram_we_s;
			sram_oe_n_o		<= not ram_oe_s;
			if ram_we_s = '1' then
				sram_data_io	<= ram_data_to_s;
			else
				sram_data_io	<= (others => 'Z');
			end if;
		end if;
	end process;

	-- K7
	mic_o		<= cas_o_s;

	-- Image and OSD
	-- dectect falling edge of the buttons
	process (clock_14_s)
		variable btn_up_de_v		: std_logic_vector(1 downto 0);
		variable btn_down_de_v	: std_logic_vector(1 downto 0);
	begin
		if rising_edge(clock_14_s) then  
			if    btn_up_de_v = "01" and btn_down_s = '1' then
				image_num_s <= image_num_s + 1;
			elsif btn_down_de_v = "01" and btn_up_s = '1' then  
				image_num_s <= image_num_s - 1;
			end if;
			btn_up_de_v 	:= btn_up_de_v(0)   & btn_up_s;
			btn_down_de_v	:= btn_down_de_v(0) & btn_down_s;
		end if;
	end process;

	-- OSD timer
	process (clock_2M_s, btn_up_s, btn_down_s)
	begin
		if rising_edge(clock_2M_s) then
			if btn_up_s = '0' or btn_down_s = '0' then
				timer_osd_s		<= (others => '1');
				osd_visible_s	<= '1';
			elsif timer_osd_s > 0 then
				timer_osd_s		<= timer_osd_s - 1;
				osd_visible_s	<= '1';
			else
				osd_visible_s	<= '0';
			end if;
		end if;
	end process;

	-- Debug

end architecture;