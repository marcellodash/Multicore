/*
TBBlue / ZX Spectrum Next project

Copyright (c) 2015 Fabio Belavenuto & Victor Trucco

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include "hardware.h"
#include "vdp.h"
#include "mmc.h"
#include "fat.h"

/* Variables */

static char cursor;
static char keys;

#define MAX_FILES	18

static int romindex;
static char romfilenames[MAX_FILES][30];
static char extension[10];


#define BANK00 0x00
#define BANKF8 0x01
#define BANKF6 0x02
#define BANKFE 0x03
#define BANKE0 0x04
#define BANK3F 0x05
#define BANKF4 0x06

#define BANKF4_SC 0x16
#define BANKF6_SC 0x12
#define BANKF8_SC 0x11

#define DEBOUNCE_LOOP 18000

#define NUMBER_OF_DIGITS 16   /* space for NUMBER_OF_DIGITS + '\0' */
/*
void uitoa(unsigned int value, char* string, int radix)
{
unsigned char index, i;

  index = NUMBER_OF_DIGITS;
  i = 0;

  do {
    string[--index] = '0' + (value % radix);
    if ( string[index] > '9') string[index] += 'A' - ':';   
    value /= radix;
  } while (value != 0);

  do {
    string[i++] = string[index++];
  } while ( index < NUMBER_OF_DIGITS );

  string[i] = 0; 
}

void itoa(int value, char* string, int radix)
{
  if (value < 0 && radix == 10) {
    *string++ = '-';
    uitoa(-value, string, radix);
  }
  else {
    uitoa(value, string, radix);
  }
}*/

/*******************************************************************************/
static void display_error(unsigned char *message)
{
	unsigned int i,j,k;
	//DisableCard();
	vdp_cls();
	ULAPORT = COLOR_RED;
	vdp_gotoxy( 0, 12 );
	vdp_prints(message);
	
	//delay to show the message
	for(i=0; i<65000; i++);
	{
		for(k=0; k<65000; k++);
		{	
			j=i;
		}
	}
}

static void draw_cursor()
{
	vdp_gotoxy( 0, cursor + 4 );
	vdp_prints( "-->" );
}

static void hide_cursor()
{
	vdp_gotoxy( 0, cursor + 4 );
	vdp_prints( "   " );
}

static void readkeyb()
{
	unsigned char t;
	unsigned int i;
	
	keys = 0;

	while(1) {
		//if ((HROW2 & 0x04) == 0) {
			//button_e = 1;
			//while(!(HROW2 & 0x04));
			//return;
		//}
		
		if ((HROW3 & 0x10) == 0) //left
		{
			keys = 0x02;
			while(!(HROW3 & 0x10));
			return;
		}
		t = HROW4;
		if ((t & 0x10) == 0) //down
		{
			keys = 0x04;
			while(!(HROW4 & 0x10));
			return;
		}
		if ((t & 0x08) == 0) //up
		{
			keys = 0x08;
			while(!(HROW4 & 0x08));
			return;
		}
		if ((t & 0x04) == 0) //right
		{
			keys = 0x01;
			while(!(HROW4 & 0x04));
			return;
		}
		if ((HROW7 & 0x01) == 0) //space
		{
			keys = 0x10;
			while(!(HROW7 & 0x01));
			return;
		}
		if ((HROW6 & 0x01) == 0) //enter
		{
			keys = 0x10;
			while(!(HROW6 & 0x01));
			return;
		}
		
		//---------------------------------------
		//Joystick
		
		if (JOYPORT & 0x08) //up
		{
			for ( i = 0; i < DEBOUNCE_LOOP; i++ ){} //debounce
			keys = 0x08;
			while(JOYPORT & 0x08);
			return;
		}
		
		if (JOYPORT & 0x04) //down
		{
			for ( i = 0; i < DEBOUNCE_LOOP; i++ ){} //debounce
			if (JOYPORT & 0x04) 
			{
				keys = 0x04;
				while(JOYPORT & 0x04);
				return;
			}
		}
		
		
		if (JOYPORT & 0x02) //left
		{
			for ( i = 0; i < DEBOUNCE_LOOP; i++ ){} //debounce
			keys = 0x02;
			while(JOYPORT & 0x02);
			return;
		}
		
		
		if (JOYPORT & 0x01) //right
		{
			for ( i = 0; i < DEBOUNCE_LOOP; i++ ){} //debounce
			keys = 0x01;
			while(JOYPORT & 0x01);
			return;
		}
		
		
		if (JOYPORT & 0x10) //action
		{
			for ( i = 0; i < DEBOUNCE_LOOP; i++ ){} //debounce
			keys = 0x10;
			while(JOYPORT & 0x10);
			return;
		}
		
	}
}


/*******************************************************************************/
static void copyname( char *dst, const unsigned char *src, int l )
{
	int i;
	
	for( i = 0; i < l ; ++i )
		*dst++ = *src++;
		
	*dst++ = 0;
}

static DIRENTRY *nthfile( int n )
{
	int i,j;
	DIRENTRY *p;
	
	p = 0;
	j = 0;
	
	for( i = 0; ( j <= n ) && ( i < dir_entries ); ++i )
	{
		p = NextDirEntry( i );
		
		if( p )
			++j;
	}
	return( p );
}


static void selectrom(int row)
{
	DIRENTRY *p=nthfile(romindex+row);
	if(p)
	{
		copyname(longfilename,p->Name,11);	// Make use of the long filename buffer to store a temporary copy of the filename,
											// since loading it by name will overwrite the sector buffer which currently contains it!
		copyname(extension,p->Extension,3);
	}
}

static int selectdir( int row )
{
	DIRENTRY *p = nthfile( romindex + row );
	if(p)
	{
		if(p->Attributes&ATTR_DIRECTORY)
		{		
			ChangeDirectory(p);
			romindex=0;
			cursor=0;
			return 1;
		}
	}
		
	return 0;
}

static void listroms()
{
	int i,j;
	
	j = 0;
	
	for( i = 0; ( j < romindex ) && ( i < dir_entries ); ++i )
	{
		DIRENTRY *p = NextDirEntry( i );
		
		if( p )
			++j;
	}

	for(j=0;(j<MAX_FILES) && (i<dir_entries);++i)
	{
		DIRENTRY *p=NextDirEntry(i);
		if(p)
		{
			// FIXME declare a global long file name buffer.
			if(p->Attributes&ATTR_DIRECTORY)
			{
				//rommenu[j].action=MENU_ACTION(&selectdir);
				romfilenames[j][0]='<'; // Right arrow
				romfilenames[j][1]='D';
				romfilenames[j][2]='I';
				romfilenames[j][3]='R';
				romfilenames[j][4]='>';
				romfilenames[j][5]=' ';
				
				if(longfilename[0])
				{
					copyname(romfilenames[j]+6,longfilename,28);
				}
				else
				{
					copyname(romfilenames[j]+6,p->Name,11);
				}
				
				j++;
			}
			else
			{
			
				//rommenu[j].action=MENU_ACTION(&selectrom);
				if(longfilename[0])
				{
					copyname(romfilenames[j],longfilename,28);
				}	
				else
				{
					copyname(romfilenames[j],p->Name,8);
					copyname(romfilenames[j]+8,".",1);
					copyname(romfilenames[j]+9,p->Extension,3);
					
				}
				
				j++;
				
			}
		}
		else
			romfilenames[j][0]=0;
	}
	for(;j<MAX_FILES;++j)
		romfilenames[j][0]=0;
}
void draw_menu()
{
	unsigned char i;
	
	cursor = 0;

	vdp_cls();
		
	vdp_setcolor(COLOR_BLUE, COLOR_RED, COLOR_WHITE);
	vdp_prints("           SD LOADER            ");
	
	vdp_setbg(COLOR_BLACK);	
	vdp_prints("  Up/Down - Select L/R - Page   ");
	vdp_prints("     Action Button to load      ");
	
	listroms();
	
			
	//for(i=0;(j<romindex) && (i<dir_entries);++i)
	for( i = 0; i < MAX_FILES; ++i )
	{
		vdp_gotoxy( 3, i + 4 );
		vdp_prints( romfilenames[ i ] );
		vdp_prints( " " );
		
	}
	
	
	draw_cursor();
}

void main()
{
	unsigned char *mem   = (unsigned char *)0x8000; 
	unsigned char *mem_ini = (unsigned char *)0x8000; 
	int c = 0, blocks;
	int i,j;
	unsigned char bs_method;
	int repeat;
	unsigned int addr;
	//unsigned char temp[10];
	
	//char temp[100];
	fileTYPE file;	
	

	c = 0;

	
	vdp_init();

	
	REG_NUM = 0; //inital state
	
	
	
	if (!MMC_Init()) {
		//           01234567890123456789012345678901
		display_error("Error initializing SD card!");
		while(1){}
	}
	
	if (!FindDrive()) {
		//           01234567890123456789012345678901
		display_error("  Error mounting SD card!");
		while(1){}
	}

	while( 1 ) 
	{
		keys = 0;
		romindex = 0;
		
		while( 1 ) 
		{
		
			draw_menu();
		
		
			while( 1 ) 
			{
			
				readkeyb();
				
				if ( keys == 0x08 ) //up
				{
					hide_cursor();
					
					if ( cursor > 0 ) cursor--;
					
					draw_cursor();
					
				} 
				else if ( keys == 0x04 ) //down
				{
					hide_cursor();
					
					if ( romfilenames[ cursor + 1 ][ 0 ] != 0 ) cursor++;
					
					draw_cursor();
				
				} 
				else if ( keys == 0x02 ) //left
				{
					if ( romindex > 0 ) 
					{
						romindex -= MAX_FILES;
						draw_menu();
					}
				} 
				else if ( keys == 0x01 ) //right
				{		
					//if ( ((romindex + MAX_FILES) * MAX_FILES) <= dir_entries ) 
					//{
						romindex += MAX_FILES;
						draw_menu();
					//}
				} 
				else if ( keys == 0x10 ) // action button 
				{
					break;
				}
			}
		
			if (selectdir ( cursor ) != 1) // try to open as folder
			{
				selectrom( cursor ); //open as file
				break;
			}
		}
	
		vdp_cls();
		vdp_gotoxy( 0, 12 );

		vdp_prints("          Loading...            ");

		if (!FileOpen(&file, longfilename)) {
			//             01234567890123456789012345678901
			display_error("    Error opening the file     ");
			continue;
		}
		
		repeat = 0;
		bs_method = file.size / 512;
		blocks = bs_method;

		/*
		//funciona no odyssey
		//align the memory block
		if ( blocks == 4) //2kb
		{
			mem += 6144;
		}
		if ( blocks == 8) //4kb
		{
			mem += 4096;
		}*/
	
		c = 0;
		// Read blocks
		while (c < blocks) {
			if (!FileRead(&file, mem)) {
				//             01234567890123456789012345678901
				display_error("     Error reading the file     ");
				continue;
			}
			c++;
			mem += 512;
		}
		
		break;
	}
	
	DisableCard();
	
	//if it´s a 2k or 4kb game, we fill the memory with copies of the blocks
	// It´s needed for Atari and Odyssey
	if ( blocks == 4) //2kb
	{
		repeat = 3;
	}
	if ( blocks == 8) //4kb
	{
		repeat = 2;
	}
	 
	addr = 0;
	i = 0;
	for ( i = 0 ; i < repeat; ++i )
	{	
		for ( j = 0 ; j < blocks * 512; ++j )
		{
			mem[j] = mem_ini[addr];
			addr++;
		}
		
		mem += blocks * 512;	
	}
	

	
	//mem -= (512 * blocks);
	

	

/*
	itoa(mem[2047],temp,10);  
	vdp_prints(temp);
	vdp_prints(" ");

	itoa(mem[2046],temp,10);  
	vdp_prints(temp);
	vdp_prints(" ");

	itoa(mem[2045],temp,10);  
	vdp_prints(temp);
	vdp_prints(" ");
*/


	ULAPORT = COLOR_GREEN;
	
	/*
	    From Harmony Cart Manual
		
		.2K Atari 2K
		.4K Atari 4K (default)
		.F8 Atari F8
		.F8S Atari F8 with Superchip
		.F6 Atari F6
		.F6S Atari F6 with Superchip
		.F4 Atari F4
		.F4S Atari F4 with Superchip
		.FA CBS RAM +
		.FE Activision FE
		.3F Tigervision 3F
		.3E 3E (3F with up to 4K RAM)
		.E0 Parker Brothers E0
		.E7 M-Network E7
		.CV CommaVid
		.UA UA Limited
		.AR Arcadia Supercharger
		.DPC DPC (Pitfall 2)
		.084 0840 Econobanking
		.CU Custom *
*/
	
	//rewrite the bs_method, according to extension
	
	if (extension[0]=='F' && extension[1]=='4'&& extension[2]=='S')
	{
		bs_method = BANKF4_SC;
	}
	else if (extension[0]=='F' && extension[1]=='4')
	{
		bs_method = BANKF4;
	}
	else if (extension[0]=='F' && extension[1]=='6' && extension[2]=='S')
	{
		bs_method = BANKF6_SC;
	}
	else if (extension[0]=='F' && extension[1]=='6')
	{
		bs_method = BANKF6;
	}
	else if (extension[0]=='F' && extension[1]=='8' && extension[2]=='S')
	{
		bs_method = BANKF8_SC;
	}
	else if (extension[0]=='F' && extension[1]=='8')
	{
		bs_method = BANKF8;
	}
	else if (extension[0]=='F' && extension[1]=='E')
	{
		bs_method = BANKFE;
	}
	else if (extension[0]=='E' && extension[1]=='0')
	{
		bs_method = BANKE0;
	}	
	else if (extension[0]=='3' && extension[1]=='F')
	{
		bs_method = BANK3F;
	}	
	else 
	{
		if ( blocks == 16) //se tem 8kb, sem extensao, supomos que é um F8
		{
			bs_method = BANKF8;
		}
		else
		{
			bs_method = 0;
		}
	}

	//add the magic bit to start the machine on the bs_method
	
	bs_method = bs_method | 0x80;
	
	//send the BS Method
	REG_NUM = bs_method;

	//send magic number to start de machine
	//REG_NUM = 0xaa;
	
	
	while(1){}
	//__asm__("jp 0x6000");	// Start firmware
}
