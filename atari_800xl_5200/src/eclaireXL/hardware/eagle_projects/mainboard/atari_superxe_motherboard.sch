<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="11" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="rcl">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
http://www.secc.co.jp/pdf/os_e/2004/e_os_all.pdf &lt;b&gt;(SANYO)&lt;/b&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0&gt;
&lt;tr valign="top"&gt;

&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1005">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk MetalÂ® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk MetalÂ® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk MetalÂ® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk MetalÂ® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk MetalÂ® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk MetalÂ® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk MetalÂ® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="HPC0201">
<description>&lt;b&gt; &lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/10129/hpc0201a.pdf</description>
<smd name="1" x="-0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<smd name="2" x="0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<text x="-0.75" y="0.74" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.785" y="-1.865" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.305" y1="-0.15" x2="0.305" y2="0.15" layer="51"/>
</package>
<package name="C0201">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C1808">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-1.4732" y1="0.9502" x2="1.4732" y2="0.9502" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-0.9502" x2="1.4732" y2="-0.9502" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-2.233" y="1.827" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.233" y="-2.842" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.275" y1="-1.015" x2="-1.225" y2="1.015" layer="51"/>
<rectangle x1="1.225" y1="-1.015" x2="2.275" y2="1.015" layer="51"/>
</package>
<package name="C3640">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-3.8322" y1="5.0496" x2="3.8322" y2="5.0496" width="0.1016" layer="51"/>
<wire x1="-3.8322" y1="-5.0496" x2="3.8322" y2="-5.0496" width="0.1016" layer="51"/>
<smd name="1" x="-4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<smd name="2" x="4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<text x="-4.647" y="6.465" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.647" y="-7.255" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.57" y1="-5.1" x2="-3.05" y2="5.1" layer="51"/>
<rectangle x1="3.05" y1="-5.1" x2="4.5688" y2="5.1" layer="51"/>
</package>
<package name="L2012C">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.864" y1="0.54" x2="0.864" y2="0.54" width="0.1016" layer="51"/>
<wire x1="-0.864" y1="-0.553" x2="0.864" y2="-0.553" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1" y="0" dx="1" dy="1" layer="1"/>
<smd name="2" x="1" y="0" dx="1" dy="1" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.143" y1="-0.6096" x2="-0.843" y2="0.5903" layer="51"/>
<rectangle x1="0.8382" y1="-0.6096" x2="1.1382" y2="0.5903" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="L2825P">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
precision wire wound</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.762" y1="1.2" x2="0.762" y2="1.2" width="0.1016" layer="51"/>
<wire x1="-0.762" y1="-1.213" x2="0.762" y2="-1.213" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.34" y1="-0.965" x2="1.34" y2="0.965" width="0.1016" layer="51"/>
<wire x1="-1.34" y1="0.965" x2="-1.34" y2="-0.965" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="0.6604" width="0.1524" layer="51"/>
<smd name="1" x="-1.2" y="0" dx="1.4" dy="2.4" layer="1"/>
<smd name="2" x="1.2" y="0" dx="1.4" dy="2.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.7366" y1="-1.27" x2="1.3208" y2="1.27" layer="51"/>
<rectangle x1="-1.3208" y1="-1.27" x2="-0.7366" y2="1.27" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="L3216C">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.27" y1="0.896" x2="1.27" y2="0.896" width="0.1016" layer="51"/>
<wire x1="-1.27" y1="-0.883" x2="1.27" y2="-0.883" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7526" y1="-0.9525" x2="-1.2525" y2="0.9474" layer="51"/>
<rectangle x1="1.2446" y1="-0.9525" x2="1.7447" y2="0.9474" layer="51"/>
<rectangle x1="-0.4001" y1="-0.5999" x2="0.4001" y2="0.5999" layer="35"/>
</package>
<package name="L3225M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-1.575" y1="1.27" x2="1.575" y2="1.27" width="0.1524" layer="51"/>
<wire x1="1.575" y1="1.27" x2="1.575" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.575" y1="-1.27" x2="-1.575" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.575" y1="-1.27" x2="-1.575" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0.94" x2="-1.651" y2="-0.94" width="0.1524" layer="51"/>
<wire x1="1.651" y1="0.94" x2="1.651" y2="-0.94" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="L3225P">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
precision wire wound</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.676" y1="0.845" x2="1.676" y2="0.845" width="0.1524" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-1.676" y1="0.838" x2="-1.676" y2="-0.838" width="0.1524" layer="51"/>
<wire x1="-1.168" y1="0.838" x2="-1.168" y2="-0.838" width="0.1524" layer="51"/>
<wire x1="1.168" y1="0.838" x2="1.168" y2="-0.838" width="0.1524" layer="51"/>
<wire x1="1.676" y1="0.838" x2="1.676" y2="-0.838" width="0.1524" layer="51"/>
<wire x1="1.676" y1="-0.845" x2="-1.676" y2="-0.845" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.7117" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.8" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.8" dy="2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="L3230M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-1.575" y1="1.27" x2="1.575" y2="1.27" width="0.1524" layer="51"/>
<wire x1="1.575" y1="1.27" x2="1.575" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.575" y1="-1.27" x2="-1.575" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.575" y1="-1.27" x2="-1.575" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0.94" x2="-1.651" y2="-0.94" width="0.1524" layer="51"/>
<wire x1="1.651" y1="0.94" x2="1.651" y2="-0.94" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="L4035M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.083" y1="0.686" x2="-2.083" y2="-0.686" width="0.1524" layer="51"/>
<wire x1="2.083" y1="0.686" x2="2.083" y2="-0.686" width="0.1524" layer="51"/>
<wire x1="-1.981" y1="1.524" x2="-1.981" y2="-1.524" width="0.1524" layer="51"/>
<wire x1="-1.981" y1="-1.524" x2="1.981" y2="-1.524" width="0.1524" layer="51"/>
<wire x1="1.981" y1="-1.524" x2="1.981" y2="1.524" width="0.1524" layer="51"/>
<wire x1="1.981" y1="1.524" x2="-1.981" y2="1.524" width="0.1524" layer="51"/>
<smd name="1" x="-1.6" y="0" dx="2.2" dy="1.4" layer="1"/>
<smd name="2" x="1.6" y="0" dx="2.2" dy="1.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="L4516C">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-1.626" y1="0.54" x2="1.626" y2="0.54" width="0.1016" layer="51"/>
<wire x1="-1.626" y1="-0.527" x2="1.626" y2="-0.527" width="0.1016" layer="51"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.1" y="0" dx="1" dy="1.6" layer="1"/>
<smd name="2" x="2.1" y="0" dx="1" dy="1.6" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4003" y1="-0.5969" x2="-1.6002" y2="0.603" layer="51"/>
<rectangle x1="1.6002" y1="-0.603" x2="2.4003" y2="0.5969" layer="51"/>
<rectangle x1="-0.7" y1="-0.3" x2="0.7" y2="0.3" layer="35"/>
</package>
<package name="L4532M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-3.473" y1="1.983" x2="3.473" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.983" x2="-3.473" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.983" x2="-3.473" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.983" x2="3.473" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.21" y1="-1.626" x2="2.21" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="2.21" y1="1.626" x2="-2.21" y2="1.626" width="0.1524" layer="51"/>
<wire x1="-2.337" y1="1.041" x2="-2.337" y2="-1.041" width="0.1524" layer="51"/>
<wire x1="2.337" y1="1.041" x2="2.337" y2="-1.041" width="0.1524" layer="51"/>
<wire x1="-2.21" y1="1.626" x2="-2.21" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="2.21" y1="1.626" x2="2.21" y2="-1.626" width="0.1524" layer="51"/>
<smd name="1" x="-1.9" y="0" dx="2" dy="2.4" layer="1"/>
<smd name="2" x="1.9" y="0" dx="2" dy="2.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="L4532P">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
precision wire wound</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-2.311" y1="1.675" x2="2.311" y2="1.675" width="0.1524" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.311" y1="-1.653" x2="2.311" y2="-1.653" width="0.1524" layer="51"/>
<wire x1="-2.311" y1="1.626" x2="-2.311" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="2.311" y1="1.675" x2="2.311" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="-1.448" y1="1.651" x2="-1.448" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="1.448" y1="1.626" x2="1.448" y2="-1.626" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="1.3211" width="0.1524" layer="51"/>
<smd name="1" x="-2" y="0" dx="1.8" dy="3.6" layer="1"/>
<smd name="2" x="2" y="0" dx="1.8" dy="3.6" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="L5038P">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
precision wire wound</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-2.286" y1="1.853" x2="2.311" y2="1.853" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.286" y1="-1.856" x2="2.311" y2="-1.856" width="0.1016" layer="51"/>
<wire x1="2.389" y1="-1.27" x2="2.389" y2="1.27" width="0.1016" layer="51"/>
<wire x1="-2.386" y1="-1.27" x2="-2.386" y2="1.27" width="0.1016" layer="51"/>
<wire x1="1.602" y1="-1.854" x2="1.602" y2="1.854" width="0.1016" layer="51"/>
<wire x1="-1.624" y1="-1.854" x2="-1.624" y2="1.854" width="0.1016" layer="51"/>
<wire x1="-2.31" y1="-1.854" x2="-2.31" y2="1.854" width="0.1016" layer="51"/>
<wire x1="2.313" y1="-1.854" x2="2.313" y2="1.854" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="1.4732" width="0.1524" layer="51"/>
<smd name="1" x="-2.2" y="0" dx="1.4" dy="2.8" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.4" dy="2.8" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="L5650M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-3.973" y1="2.983" x2="3.973" y2="2.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-2.983" x2="-3.973" y2="-2.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-2.983" x2="-3.973" y2="2.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="2.983" x2="3.973" y2="-2.983" width="0.0508" layer="39"/>
<wire x1="-2.108" y1="-2.591" x2="2.083" y2="-2.591" width="0.1524" layer="51"/>
<wire x1="2.083" y1="2.591" x2="-2.108" y2="2.591" width="0.1524" layer="51"/>
<wire x1="2.184" y1="2.032" x2="2.184" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="-2.21" y1="2.032" x2="-2.21" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="-2.108" y1="2.591" x2="-2.108" y2="-2.591" width="0.1524" layer="51"/>
<wire x1="2.083" y1="2.591" x2="2.083" y2="-2.591" width="0.1524" layer="51"/>
<smd name="1" x="-2.5" y="0" dx="1.8" dy="4" layer="1"/>
<smd name="2" x="2.5" y="0" dx="1.8" dy="4" layer="1"/>
<text x="-1.905" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="L8530M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-5.473" y1="1.983" x2="5.473" y2="1.983" width="0.0508" layer="39"/>
<wire x1="5.473" y1="-1.983" x2="-5.473" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-5.473" y1="-1.983" x2="-5.473" y2="1.983" width="0.0508" layer="39"/>
<wire x1="5.473" y1="1.983" x2="5.473" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-4.191" y1="-1.524" x2="-4.191" y2="1.524" width="0.1524" layer="51"/>
<wire x1="4.191" y1="1.524" x2="-4.191" y2="1.524" width="0.1524" layer="51"/>
<wire x1="4.191" y1="-1.524" x2="-4.191" y2="-1.524" width="0.1524" layer="51"/>
<wire x1="-4.293" y1="-0.66" x2="-4.293" y2="0.66" width="0.1524" layer="51"/>
<wire x1="4.293" y1="-0.66" x2="4.293" y2="0.66" width="0.1524" layer="51"/>
<wire x1="4.191" y1="-1.524" x2="4.191" y2="1.524" width="0.1524" layer="51"/>
<smd name="1" x="-3.7" y="0" dx="2.4" dy="1.4" layer="1"/>
<smd name="2" x="3.7" y="0" dx="2.4" dy="1.4" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="L1812">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
</package>
<package name="TJ3-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-3.65" y1="8.15" x2="3.65" y2="8.15" width="0.2032" layer="21"/>
<wire x1="3.65" y1="-8.15" x2="-3.65" y2="-8.15" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="-8.15" x2="-3.65" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="-1.1" x2="-3.65" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="1.1" x2="-3.65" y2="8.15" width="0.2032" layer="21"/>
<wire x1="3.65" y1="8.15" x2="3.65" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.65" y1="1.1" x2="3.65" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="3.65" y1="-1.1" x2="3.65" y2="-8.15" width="0.2032" layer="21"/>
<pad name="1" x="-3.3" y="0" drill="0.9"/>
<pad name="2" x="3.3" y="0" drill="0.9"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ3-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="8.3" width="0.2032" layer="27"/>
<pad name="1" x="-7.35" y="0" drill="0.9"/>
<pad name="2" x="7.35" y="0" drill="0.9"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TJ4-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-4.95" y1="11.05" x2="4.95" y2="11.05" width="0.2032" layer="21"/>
<wire x1="4.95" y1="11.05" x2="4.95" y2="-11.05" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-11.05" x2="-4.95" y2="-11.05" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-11.05" x2="-4.95" y2="11.05" width="0.2032" layer="21"/>
<pad name="1" x="-3.935" y="0" drill="0.9"/>
<pad name="2" x="3.935" y="0" drill="0.9"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ4-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="11.1" width="0.2032" layer="27"/>
<pad name="1" x="-9.9" y="0" drill="0.9"/>
<pad name="2" x="9.9" y="0" drill="0.9"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TJ5-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-5.55" y1="12.55" x2="5.55" y2="12.55" width="0.2032" layer="21"/>
<wire x1="5.55" y1="12.55" x2="5.55" y2="-12.55" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-12.55" x2="-5.55" y2="-12.55" width="0.2032" layer="21"/>
<wire x1="-5.55" y1="-12.55" x2="-5.55" y2="12.55" width="0.2032" layer="21"/>
<pad name="1" x="-4.7" y="0" drill="0.9"/>
<pad name="2" x="4.7" y="0" drill="0.9"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ5-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="12.6" width="0.2032" layer="27"/>
<pad name="1" x="-11.45" y="0" drill="0.9"/>
<pad name="2" x="11.45" y="0" drill="0.9"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TJ6-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-10.95" y1="17.45" x2="10.95" y2="17.45" width="0.2032" layer="21"/>
<wire x1="10.95" y1="17.45" x2="10.95" y2="-17.45" width="0.2032" layer="21"/>
<wire x1="10.95" y1="-17.45" x2="-10.95" y2="-17.45" width="0.2032" layer="21"/>
<wire x1="-10.95" y1="-17.45" x2="-10.95" y2="17.45" width="0.2032" layer="21"/>
<pad name="1" x="-9.25" y="0" drill="1.3"/>
<pad name="2" x="9.25" y="0" drill="1.3"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ6-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="17.5" width="0.2032" layer="27"/>
<pad name="1" x="-15.5" y="0" drill="1.3"/>
<pad name="2" x="15.5" y="0" drill="1.3"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TJ7-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-9.45" y1="20.85" x2="9.43" y2="20.85" width="0.2032" layer="21"/>
<wire x1="9.43" y1="20.85" x2="9.45" y2="-20.85" width="0.2032" layer="21"/>
<wire x1="9.45" y1="-20.85" x2="-9.45" y2="-20.85" width="0.2032" layer="21"/>
<wire x1="-9.45" y1="-20.85" x2="-9.45" y2="20.85" width="0.2032" layer="21"/>
<pad name="1" x="-7.9" y="0" drill="1.3"/>
<pad name="2" x="7.9" y="0" drill="1.3"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ7-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="20.9" width="0.2032" layer="27"/>
<pad name="1" x="-18.8" y="0" drill="1.3"/>
<pad name="2" x="18.8" y="0" drill="1.3"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TJ8-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-12.55" y1="24.25" x2="12.55" y2="24.25" width="0.2032" layer="21"/>
<wire x1="12.55" y1="24.25" x2="12.55" y2="-24.25" width="0.2032" layer="21"/>
<wire x1="12.55" y1="-24.25" x2="-12.55" y2="-24.25" width="0.2032" layer="21"/>
<wire x1="-12.55" y1="-24.25" x2="-12.55" y2="24.25" width="0.2032" layer="21"/>
<pad name="1" x="-10.4" y="0" drill="1.5"/>
<pad name="2" x="10.4" y="0" drill="1.5"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ8-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="24.6" width="0.2032" layer="27"/>
<pad name="1" x="-22.35" y="0" drill="1.5"/>
<pad name="2" x="22.35" y="0" drill="1.5"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TJ9-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-17.95" y1="33.75" x2="17.95" y2="33.75" width="0.2032" layer="21"/>
<wire x1="17.95" y1="33.75" x2="17.95" y2="-33.75" width="0.2032" layer="21"/>
<wire x1="17.95" y1="-33.75" x2="-17.95" y2="-33.75" width="0.2032" layer="21"/>
<wire x1="-17.95" y1="-33.75" x2="-17.95" y2="33.75" width="0.2032" layer="21"/>
<pad name="1" x="-15.9" y="0" drill="1.8"/>
<pad name="2" x="15.9" y="0" drill="1.8"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ9-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="34.5" width="0.2032" layer="27"/>
<pad name="1" x="-31.6" y="0" drill="1.8"/>
<pad name="2" x="31.6" y="0" drill="1.8"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="POWER-CHOKE_WE-TPC">
<description>&lt;b&gt;POWER-CHOKE WE-TPC&lt;/b&gt;&lt;p&gt;
WÃ¼rth Elektronik, Partnumber: 744053220&lt;br&gt;
Source: WE-TPC 744053220.pdf</description>
<wire x1="-2.8" y1="2.3" x2="-2.3" y2="2.8" width="0.2" layer="51" curve="-90"/>
<wire x1="-2.3" y1="2.8" x2="2.3" y2="2.8" width="0.2" layer="51"/>
<wire x1="2.3" y1="2.8" x2="2.8" y2="2.3" width="0.2" layer="51" curve="-90"/>
<wire x1="2.8" y1="2.3" x2="2.8" y2="-2.3" width="0.2" layer="51"/>
<wire x1="2.8" y1="-2.3" x2="2.3" y2="-2.8" width="0.2" layer="51" curve="-90"/>
<wire x1="2.3" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2" layer="51"/>
<wire x1="-2.3" y1="-2.8" x2="-2.8" y2="-2.3" width="0.2" layer="51" curve="-90"/>
<wire x1="-2.8" y1="-2.3" x2="-2.8" y2="2.3" width="0.2" layer="51"/>
<wire x1="-2.8" y1="0.65" x2="-2.8" y2="-0.65" width="0.2" layer="21"/>
<wire x1="-2" y1="0.65" x2="-2" y2="-0.65" width="0.2" layer="21" curve="36.008323"/>
<wire x1="2.8" y1="-0.65" x2="2.8" y2="0.65" width="0.2" layer="21"/>
<wire x1="2" y1="-0.65" x2="2" y2="0.65" width="0.2" layer="21" curve="36.008323"/>
<circle x="0" y="0" radius="2.1" width="0.2" layer="51"/>
<smd name="1" x="0" y="2.05" dx="6.3" dy="2.2" layer="1" roundness="25"/>
<smd name="2" x="0" y="-2.05" dx="6.3" dy="2.2" layer="1" roundness="25" rot="R180"/>
<text x="-3.5" y="3.5" size="1.778" layer="25">&gt;NAME</text>
<text x="-3.5" y="-5.3" size="1.778" layer="27">&gt;VALUE</text>
</package>
<package name="2200-12.7">
<description>&lt;b&gt;newport components&lt;/b&gt; 2200 Serie RM 12.7 mm&lt;p&gt;
Miniatur Axial Lead Inductors&lt;br&gt;
Source: www.rsonline.de&lt;br&gt;
Order code 240-517</description>
<wire x1="-4.9" y1="1.9" x2="4.9" y2="1.9" width="0.2032" layer="51"/>
<wire x1="4.9" y1="1.9" x2="4.9" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="4.9" y1="-1.9" x2="-4.9" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="-1.9" x2="-4.9" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.5" layer="51"/>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.5" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="0.8" diameter="1.27"/>
<pad name="2" x="6.35" y="0" drill="0.8" diameter="1.27"/>
<text x="-4.445" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="2200-15.24">
<description>&lt;b&gt;newport components&lt;/b&gt; 2200 Serie RM 15.24 mm&lt;p&gt;
Miniatur Axial Lead Inductors&lt;br&gt;
Source: www.rsonline.de&lt;br&gt;
Order code 240-517</description>
<wire x1="-4.9" y1="1.9" x2="4.9" y2="1.9" width="0.2032" layer="51"/>
<wire x1="4.9" y1="1.9" x2="4.9" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="4.9" y1="-1.9" x2="-4.9" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="-1.9" x2="-4.9" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="0" x2="-5.08" y2="0" width="0.5" layer="51"/>
<wire x1="7.62" y1="0" x2="5.08" y2="0" width="0.5" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.8" diameter="1.27"/>
<pad name="2" x="7.62" y="0" drill="0.8" diameter="1.27"/>
<text x="-4.445" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="2200-11.43">
<description>&lt;b&gt;newport components&lt;/b&gt; 2200 Serie RM 11.43 mm&lt;p&gt;
Miniatur Axial Lead Inductors&lt;br&gt;
Source: www.rsonline.de&lt;br&gt;
Order code 240-517</description>
<wire x1="-4.9" y1="1.9" x2="4.9" y2="1.9" width="0.2032" layer="51"/>
<wire x1="4.9" y1="1.9" x2="4.9" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="4.9" y1="-1.9" x2="-4.9" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="-1.9" x2="-4.9" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="0" x2="-5.08" y2="0" width="0.5" layer="51"/>
<wire x1="5.715" y1="0" x2="5.08" y2="0" width="0.5" layer="51"/>
<pad name="1" x="-5.715" y="0" drill="0.8" diameter="1.27"/>
<pad name="2" x="5.715" y="0" drill="0.8" diameter="1.27"/>
<text x="-4.445" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CEP125">
<description>&lt;b&gt;POWER INDUCTORS&lt;/b&gt; (SMT Type)&lt;p&gt;
Source: www.sumida.com/products/pdf/CEP125.pdf</description>
<wire x1="-1.5796" y1="6.3448" x2="-1.5533" y2="6.3448" width="0.2032" layer="21"/>
<wire x1="-1.5533" y1="6.3448" x2="-1.29" y2="6.0815" width="0.2032" layer="21"/>
<wire x1="-1.29" y1="6.0815" x2="-1.1584" y2="6.0816" width="0.2032" layer="21"/>
<wire x1="-1.1584" y1="6.0816" x2="-1.1584" y2="6.2922" width="0.2032" layer="21"/>
<wire x1="-1.1584" y1="6.2922" x2="-1.1583" y2="6.2922" width="0.2032" layer="21"/>
<wire x1="-1.1583" y1="6.2922" x2="-1.1057" y2="6.3448" width="0.2032" layer="21"/>
<wire x1="-1.1057" y1="6.3448" x2="1.1057" y2="6.3448" width="0.2032" layer="21"/>
<wire x1="1.1057" y1="6.3448" x2="1.1057" y2="6.0815" width="0.2032" layer="21"/>
<wire x1="1.1057" y1="6.0815" x2="1.211" y2="6.0815" width="0.2032" layer="21"/>
<wire x1="1.211" y1="6.0815" x2="1.4217" y2="6.3448" width="0.2032" layer="21"/>
<wire x1="1.4217" y1="6.3448" x2="6.1079" y2="6.3448" width="0.2032" layer="51"/>
<wire x1="6.1079" y1="6.3448" x2="6.3448" y2="6.1079" width="0.2032" layer="21" curve="-96.645912"/>
<wire x1="6.3448" y1="6.1079" x2="6.3448" y2="1.8166" width="0.2032" layer="21"/>
<wire x1="6.3448" y1="1.8166" x2="6.2658" y2="1.7376" width="0.2032" layer="21"/>
<wire x1="6.2658" y1="1.7376" x2="6.2658" y2="-1.7376" width="0.2032" layer="21"/>
<wire x1="6.2658" y1="-1.7376" x2="6.3448" y2="-1.8166" width="0.2032" layer="21"/>
<wire x1="6.3448" y1="-1.8166" x2="6.3448" y2="-6.0289" width="0.2032" layer="21"/>
<wire x1="6.3448" y1="-6.0289" x2="6.0289" y2="-6.3448" width="0.2032" layer="21" curve="-91.024745"/>
<wire x1="6.0289" y1="-6.3448" x2="-6.0289" y2="-6.3448" width="0.2032" layer="51"/>
<wire x1="-6.3448" y1="-6.0289" x2="-6.3448" y2="6.1342" width="0.2032" layer="21"/>
<wire x1="-6.3448" y1="6.1342" x2="-6.1342" y2="6.3448" width="0.2032" layer="21" curve="-83.297108"/>
<wire x1="-6.1342" y1="6.3448" x2="-1.5533" y2="6.3448" width="0.2032" layer="51"/>
<wire x1="-6.2395" y1="5.7393" x2="-2.0535" y2="5.7393" width="0.2032" layer="51"/>
<wire x1="-2.0535" y1="5.7393" x2="-1.5534" y2="3.9754" width="0.2032" layer="51"/>
<wire x1="-1.5534" y1="3.9754" x2="1.527" y2="3.9754" width="0.2032" layer="21"/>
<wire x1="1.527" y1="3.9754" x2="2.0535" y2="5.7393" width="0.2032" layer="51"/>
<wire x1="2.0535" y1="5.7393" x2="6.2395" y2="5.7393" width="0.2032" layer="51"/>
<wire x1="6.2395" y1="-3.4752" x2="-6.2658" y2="-3.4752" width="0.2032" layer="21"/>
<wire x1="-5.6077" y1="-3.5805" x2="-5.6077" y2="-6.2395" width="0.2032" layer="21"/>
<wire x1="-4.8968" y1="-6.2395" x2="-4.8968" y2="-3.5805" width="0.2032" layer="21"/>
<wire x1="-4.7915" y1="-6.0552" x2="-2.2115" y2="-6.0552" width="0.2032" layer="21"/>
<wire x1="-2.9486" y1="-5.9499" x2="-2.9223" y2="-5.9499" width="0.2032" layer="21"/>
<wire x1="-2.9223" y1="-5.9499" x2="-2.9223" y2="-3.5805" width="0.2032" layer="21"/>
<wire x1="-2.1588" y1="-6.2395" x2="-2.1588" y2="-3.8701" width="0.2032" layer="21"/>
<wire x1="2.1325" y1="-6.2395" x2="2.1325" y2="-3.8701" width="0.2032" layer="21"/>
<wire x1="2.2378" y1="-6.0289" x2="4.8968" y2="-6.0289" width="0.2032" layer="21"/>
<wire x1="4.8968" y1="-3.5805" x2="4.8968" y2="-6.2395" width="0.2032" layer="21"/>
<wire x1="2.9486" y1="-5.9236" x2="2.9486" y2="-3.5805" width="0.2032" layer="21"/>
<wire x1="5.5813" y1="-6.2395" x2="5.5813" y2="-3.5805" width="0.2032" layer="21"/>
<wire x1="2.5011" y1="-3.5805" x2="-2.4747" y2="-3.5805" width="0.2032" layer="51" curve="-63.906637"/>
<wire x1="1.1057" y1="6.0815" x2="-1.1584" y2="6.0815" width="0.2032" layer="21"/>
<wire x1="-1.7902" y1="5.1601" x2="1.7376" y2="5.1863" width="0.2032" layer="21" curve="-37.134171"/>
<wire x1="-6.0289" y1="-6.3448" x2="-6.3448" y2="-6.0289" width="0.2032" layer="21" curve="-90.91408"/>
<smd name="1" x="-3.5" y="5.375" dx="3" dy="2.75" layer="1"/>
<smd name="2" x="3.5" y="5.375" dx="3" dy="2.75" layer="1"/>
<smd name="3" x="0" y="-5.48" dx="3" dy="2.55" layer="1"/>
<text x="-5.08" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="L0201">
<description>&lt;b&gt;NIS02 Chip Inductor&lt;/b&gt;&lt;p&gt;
Source: http://www.niccomp.com/Catalog/nis.pdf</description>
<smd name="1" x="-0.275" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.275" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.05" layer="51"/>
</package>
<package name="PIS2816">
<description>&lt;b&gt;PIS 2826&lt;/b&gt; Inductor&lt;p&gt;
Source: http://www.stetco.com/products/inductors/pdf/PIS2816.pdf</description>
<wire x1="-3.65" y1="-3.65" x2="3.65" y2="-3.65" width="0.2032" layer="51"/>
<wire x1="3.65" y1="-3.65" x2="3.65" y2="3.65" width="0.2032" layer="21"/>
<wire x1="3.65" y1="3.65" x2="-3.65" y2="3.65" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="3.65" x2="-3.65" y2="-3.65" width="0.2032" layer="21"/>
<wire x1="2.75" y1="1.125" x2="-2.875" y2="-0.625" width="0.1016" layer="21"/>
<wire x1="-2.75" y1="-1.125" x2="2.875" y2="0.625" width="0.1016" layer="21"/>
<wire x1="-2.875" y1="-0.625" x2="2.75" y2="1.125" width="0.1016" layer="51" curve="-169.840784"/>
<wire x1="2.875" y1="0.625" x2="-2.75" y2="-1.125" width="0.1016" layer="51" curve="-169.840784"/>
<wire x1="-2.875" y1="-0.625" x2="-1.375" y2="2.6" width="0.1016" layer="21" curve="-74.384165"/>
<wire x1="2.75" y1="1.125" x2="1.275" y2="2.675" width="0.1016" layer="21" curve="42.122709"/>
<wire x1="2.875" y1="0.625" x2="1.375" y2="-2.6" width="0.1016" layer="21" curve="-74.384165"/>
<wire x1="-2.75" y1="-1.125" x2="-1.275" y2="-2.675" width="0.1016" layer="21" curve="42.122709"/>
<wire x1="-1.35" y1="3.65" x2="-3.65" y2="3.65" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="-3.65" x2="-1.35" y2="-3.65" width="0.2032" layer="21"/>
<wire x1="1.35" y1="-3.65" x2="3.65" y2="-3.65" width="0.2032" layer="21"/>
<wire x1="3.65" y1="3.65" x2="1.35" y2="3.65" width="0.2032" layer="21"/>
<circle x="1.5" y="1.375" radius="0.25" width="0" layer="21"/>
<smd name="1" x="0" y="3.2" dx="2.2" dy="1.6" layer="1"/>
<smd name="2" x="0" y="-3.2" dx="2.2" dy="1.6" layer="1"/>
<text x="-3.625" y="4.5" size="1.778" layer="25">&gt;NAME</text>
<text x="-3.625" y="-6.25" size="1.778" layer="27">&gt;VALUE</text>
</package>
<package name="IR-2">
<description>&lt;B&gt;Vishay Dale Inductor&lt;/b&gt;&lt;p&gt;
Source: www.vishay.com .. ir.pdf</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="1.164" x2="-2.9" y2="1.425" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-1.164" x2="-2.925" y2="-1.425" width="0.1524" layer="21" curve="90"/>
<wire x1="2.925" y1="-1.425" x2="3.175" y2="-1.164" width="0.1524" layer="21" curve="90"/>
<wire x1="2.925" y1="1.425" x2="3.175" y2="1.164" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-1.164" x2="-3.175" y2="1.164" width="0.1524" layer="21"/>
<wire x1="2.925" y1="1.425" x2="-2.9" y2="1.425" width="0.1524" layer="21"/>
<wire x1="2.925" y1="-1.425" x2="-2.925" y2="-1.425" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.164" x2="3.175" y2="1.164" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1" diameter="1.6764"/>
<pad name="2" x="5.08" y="0" drill="1" diameter="1.6764"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="IR-4">
<description>&lt;B&gt;Vishay Dale Inductor&lt;/b&gt;&lt;p&gt;
Source: www.vishay.com .. ir.pdf</description>
<wire x1="6.73" y1="0" x2="5.714" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.705" y1="0" x2="-5.689" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.8" y1="1.939" x2="-4.525" y2="2.2" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.8" y1="-1.939" x2="-4.55" y2="-2.2" width="0.1524" layer="21" curve="90"/>
<wire x1="4.575" y1="-2.2" x2="4.825" y2="-1.939" width="0.1524" layer="21" curve="90"/>
<wire x1="4.575" y1="2.2" x2="4.825" y2="1.939" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.8" y1="-1.939" x2="-4.8" y2="1.939" width="0.1524" layer="21"/>
<wire x1="4.575" y1="2.2" x2="-4.525" y2="2.2" width="0.1524" layer="21"/>
<wire x1="4.575" y1="-2.2" x2="-4.55" y2="-2.2" width="0.1524" layer="21"/>
<wire x1="4.825" y1="-1.939" x2="4.825" y2="1.939" width="0.1524" layer="21"/>
<pad name="1" x="-6.705" y="0" drill="1" diameter="1.6764"/>
<pad name="2" x="6.73" y="0" drill="1" diameter="1.6764"/>
<text x="-3.81" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.825" y1="-0.3048" x2="5.6886" y2="0.3048" layer="21"/>
<rectangle x1="-5.6636" y1="-0.3048" x2="-4.8" y2="0.3048" layer="21"/>
</package>
<package name="IRF-1">
<description>&lt;B&gt;Vishay Dale Inductor&lt;/b&gt;&lt;p&gt;
Source: www.vishay.com .. irf.pdf</description>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="1.164" x2="-2.9" y2="1.425" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-1.164" x2="-2.925" y2="-1.425" width="0.1524" layer="21" curve="90"/>
<wire x1="2.925" y1="-1.425" x2="3.175" y2="-1.164" width="0.1524" layer="21" curve="90"/>
<wire x1="2.925" y1="1.425" x2="3.175" y2="1.164" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-1.164" x2="-3.175" y2="1.164" width="0.1524" layer="21"/>
<wire x1="2.925" y1="1.425" x2="-2.9" y2="1.425" width="0.1524" layer="21"/>
<wire x1="2.925" y1="-1.425" x2="-2.925" y2="-1.425" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.164" x2="3.175" y2="1.164" width="0.1524" layer="21"/>
<pad name="1" x="-5.715" y="0" drill="1" diameter="1.6764"/>
<pad name="2" x="5.715" y="0" drill="1" diameter="1.6764"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.2" y2="0.3048" layer="21"/>
<rectangle x1="-4.2" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="IRF-3">
<description>&lt;B&gt;Vishay Dale Inductor&lt;/b&gt;&lt;p&gt;
Source: www.vishay.com .. irf.pdf</description>
<wire x1="6.73" y1="0" x2="5.2" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.705" y1="0" x2="-5.225" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.8" y1="1.739" x2="-4.525" y2="2" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.8" y1="-1.764" x2="-4.55" y2="-2.025" width="0.1524" layer="21" curve="90"/>
<wire x1="4.575" y1="-2.025" x2="4.825" y2="-1.764" width="0.1524" layer="21" curve="90"/>
<wire x1="4.575" y1="2" x2="4.825" y2="1.739" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.8" y1="-1.764" x2="-4.8" y2="1.739" width="0.1524" layer="21"/>
<wire x1="4.575" y1="2" x2="-4.525" y2="2" width="0.1524" layer="21"/>
<wire x1="4.575" y1="-2.025" x2="-4.55" y2="-2.025" width="0.1524" layer="21"/>
<wire x1="4.825" y1="-1.764" x2="4.825" y2="1.739" width="0.1524" layer="21"/>
<pad name="1" x="-6.705" y="0" drill="1" diameter="1.6764"/>
<pad name="2" x="6.73" y="0" drill="1" diameter="1.6764"/>
<text x="-3.81" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.825" y1="-0.3048" x2="5.2" y2="0.3048" layer="21"/>
<rectangle x1="-5.2" y1="-0.3048" x2="-4.8" y2="0.3048" layer="21"/>
</package>
<package name="IRF24">
<description>&lt;B&gt;Vishay Dale Inductor&lt;/b&gt;&lt;p&gt;
Source: www.vishay.com .. irf24.pdf</description>
<wire x1="5.825" y1="0" x2="6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.915" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="-1.925" y1="1.164" x2="-1.65" y2="1.425" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.925" y1="-1.164" x2="-1.675" y2="-1.425" width="0.1524" layer="21" curve="90"/>
<wire x1="1.675" y1="-1.425" x2="1.925" y2="-1.164" width="0.1524" layer="21" curve="90"/>
<wire x1="1.675" y1="1.425" x2="1.925" y2="1.164" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.925" y1="-1.164" x2="-5.725" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.725" y1="0" x2="-1.925" y2="1.164" width="0.1524" layer="21"/>
<wire x1="1.675" y1="1.425" x2="-1.65" y2="1.425" width="0.1524" layer="21"/>
<wire x1="1.675" y1="-1.425" x2="-1.675" y2="-1.425" width="0.1524" layer="21"/>
<wire x1="1.925" y1="-1.164" x2="5.725" y2="0" width="0.1524" layer="21"/>
<wire x1="5.725" y1="0" x2="1.925" y2="1.164" width="0.1524" layer="21"/>
<pad name="1" x="-6.985" y="0" drill="1" diameter="1.6764"/>
<pad name="2" x="6.985" y="0" drill="1" diameter="1.6764"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5" y1="-0.3048" x2="5.825" y2="0.3048" layer="21"/>
<rectangle x1="-5.85" y1="-0.3048" x2="-5" y2="0.3048" layer="21"/>
</package>
<package name="IRF36">
<description>&lt;B&gt;Vishay Dale Inductor&lt;/b&gt;&lt;p&gt;
Source: www.vishay.com .. irf36.pdf</description>
<wire x1="7.825" y1="0" x2="8.89" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.915" y1="0" x2="-8.89" y2="0" width="0.6096" layer="51"/>
<wire x1="-2.925" y1="1.639" x2="-2.65" y2="1.9" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.925" y1="-1.664" x2="-2.675" y2="-1.925" width="0.1524" layer="21" curve="90"/>
<wire x1="2.675" y1="-1.925" x2="2.925" y2="-1.664" width="0.1524" layer="21" curve="90"/>
<wire x1="2.675" y1="1.9" x2="2.925" y2="1.639" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.925" y1="-1.664" x2="-7.95" y2="0" width="0.1524" layer="21"/>
<wire x1="-7.95" y1="0" x2="-2.925" y2="1.639" width="0.1524" layer="21"/>
<wire x1="2.675" y1="1.9" x2="-2.65" y2="1.9" width="0.1524" layer="21"/>
<wire x1="2.675" y1="-1.925" x2="-2.675" y2="-1.925" width="0.1524" layer="21"/>
<wire x1="2.925" y1="-1.664" x2="7.95" y2="0" width="0.1524" layer="21"/>
<wire x1="7.95" y1="0" x2="2.925" y2="1.639" width="0.1524" layer="21"/>
<pad name="1" x="-8.89" y="0" drill="1" diameter="1.6764"/>
<pad name="2" x="8.89" y="0" drill="1" diameter="1.6764"/>
<text x="-3.175" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7" y1="-0.3048" x2="8.05" y2="0.3048" layer="21"/>
<rectangle x1="-8.05" y1="-0.3048" x2="-7" y2="0.3048" layer="21"/>
</package>
<package name="IRF46">
<description>&lt;B&gt;Vishay Dale Inductor&lt;/b&gt;&lt;p&gt;
Source: www.vishay.com .. irf46.pdf</description>
<wire x1="5.715" y1="0" x2="4.445" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.715" y1="0" x2="-4.445" y2="0" width="0.6096" layer="51"/>
<wire x1="-2.925" y1="2.164" x2="-2.65" y2="2.425" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.925" y1="-2.164" x2="-2.675" y2="-2.425" width="0.1524" layer="21" curve="90"/>
<wire x1="2.675" y1="-2.425" x2="2.925" y2="-2.164" width="0.1524" layer="21" curve="90"/>
<wire x1="2.675" y1="2.425" x2="2.925" y2="2.164" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.925" y1="-2.164" x2="-2.925" y2="2.164" width="0.1524" layer="21"/>
<wire x1="2.675" y1="2.425" x2="-2.65" y2="2.425" width="0.1524" layer="21"/>
<wire x1="2.675" y1="-2.425" x2="-2.675" y2="-2.425" width="0.1524" layer="21"/>
<wire x1="2.925" y1="-2.164" x2="2.925" y2="2.164" width="0.1524" layer="21"/>
<pad name="1" x="-5.715" y="0" drill="1" diameter="1.6764"/>
<pad name="2" x="5.715" y="0" drill="1" diameter="1.6764"/>
<text x="-3.81" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.925" y1="-0.3048" x2="4.9" y2="0.3048" layer="21"/>
<rectangle x1="-4.925" y1="-0.3048" x2="-2.925" y2="0.3048" layer="21"/>
</package>
<package name="LAL02">
<description>&lt;b&gt;TAYO YUDEN Inductor&lt;/b&gt;&lt;p&gt;
Source: je999f5.pdf</description>
<wire x1="2.54" y1="0" x2="1.905" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.508" layer="51"/>
<wire x1="-1.615" y1="0.812" x2="-1.361" y2="1.066" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.615" y1="-0.812" x2="-1.361" y2="-1.066" width="0.1524" layer="21" curve="90"/>
<wire x1="1.361" y1="-1.066" x2="1.615" y2="-0.812" width="0.1524" layer="21" curve="90"/>
<wire x1="1.361" y1="1.066" x2="1.615" y2="0.812" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.615" y1="-0.812" x2="-1.615" y2="0.812" width="0.1524" layer="21"/>
<wire x1="-1.361" y1="1.066" x2="-0.98" y2="1.066" width="0.1524" layer="21"/>
<wire x1="-0.853" y1="0.939" x2="-0.98" y2="1.066" width="0.1524" layer="21"/>
<wire x1="-1.361" y1="-1.066" x2="-0.98" y2="-1.066" width="0.1524" layer="21"/>
<wire x1="-0.853" y1="-0.939" x2="-0.98" y2="-1.066" width="0.1524" layer="21"/>
<wire x1="0.853" y1="0.939" x2="0.98" y2="1.066" width="0.1524" layer="21"/>
<wire x1="0.853" y1="0.939" x2="-0.853" y2="0.939" width="0.1524" layer="21"/>
<wire x1="0.853" y1="-0.939" x2="0.98" y2="-1.066" width="0.1524" layer="21"/>
<wire x1="0.853" y1="-0.939" x2="-0.853" y2="-0.939" width="0.1524" layer="21"/>
<wire x1="1.361" y1="1.066" x2="0.98" y2="1.066" width="0.1524" layer="21"/>
<wire x1="1.361" y1="-1.066" x2="0.98" y2="-1.066" width="0.1524" layer="21"/>
<wire x1="1.615" y1="-0.812" x2="1.615" y2="0.812" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8" diameter="1.3"/>
<pad name="2" x="2.54" y="0" drill="0.8" diameter="1.3"/>
<text x="-2.54" y="1.27" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.615" y1="-0.254" x2="1.9" y2="0.254" layer="21"/>
<rectangle x1="-1.9" y1="-0.254" x2="-1.615" y2="0.254" layer="21"/>
</package>
<package name="LAL03">
<description>&lt;b&gt;TAYO YUDEN Inductor&lt;/b&gt;&lt;p&gt;
Source: je999f5.pdf</description>
<wire x1="5.08" y1="0" x2="3.705" y2="0" width="0.508" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.705" y2="0" width="0.508" layer="51"/>
<wire x1="-3.415" y1="0.962" x2="-3.161" y2="1.216" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.415" y1="-0.962" x2="-3.161" y2="-1.216" width="0.1524" layer="21" curve="90"/>
<wire x1="3.161" y1="-1.216" x2="3.415" y2="-0.962" width="0.1524" layer="21" curve="90"/>
<wire x1="3.161" y1="1.216" x2="3.415" y2="0.962" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.415" y1="-0.962" x2="-3.415" y2="0.962" width="0.1524" layer="21"/>
<wire x1="-3.161" y1="1.216" x2="-2.78" y2="1.216" width="0.1524" layer="21"/>
<wire x1="-2.653" y1="1.089" x2="-2.78" y2="1.216" width="0.1524" layer="21"/>
<wire x1="-3.161" y1="-1.216" x2="-2.78" y2="-1.216" width="0.1524" layer="21"/>
<wire x1="-2.653" y1="-1.089" x2="-2.78" y2="-1.216" width="0.1524" layer="21"/>
<wire x1="2.653" y1="1.089" x2="2.78" y2="1.216" width="0.1524" layer="21"/>
<wire x1="2.653" y1="1.089" x2="-2.653" y2="1.089" width="0.1524" layer="21"/>
<wire x1="2.653" y1="-1.089" x2="2.78" y2="-1.216" width="0.1524" layer="21"/>
<wire x1="2.653" y1="-1.089" x2="-2.653" y2="-1.089" width="0.1524" layer="21"/>
<wire x1="3.161" y1="1.216" x2="2.78" y2="1.216" width="0.1524" layer="21"/>
<wire x1="3.161" y1="-1.216" x2="2.78" y2="-1.216" width="0.1524" layer="21"/>
<wire x1="3.415" y1="-0.962" x2="3.415" y2="0.962" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8" diameter="1.3"/>
<pad name="2" x="5.08" y="0" drill="0.8" diameter="1.3"/>
<text x="-3.175" y="1.905" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-0.635" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.415" y1="-0.254" x2="3.7" y2="0.254" layer="21"/>
<rectangle x1="-3.7" y1="-0.254" x2="-3.415" y2="0.254" layer="21"/>
</package>
<package name="LAL03KH">
<description>&lt;b&gt;TAYO YUDEN Inductor&lt;/b&gt;&lt;p&gt;
Source: je999f5.pdf</description>
<wire x1="5" y1="0" x2="4.405" y2="0" width="0.508" layer="51"/>
<wire x1="-5" y1="0" x2="-4.405" y2="0" width="0.508" layer="51"/>
<wire x1="-4.115" y1="0.962" x2="-3.861" y2="1.216" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.115" y1="-0.962" x2="-3.861" y2="-1.216" width="0.1524" layer="21" curve="90"/>
<wire x1="3.861" y1="-1.216" x2="4.115" y2="-0.962" width="0.1524" layer="21" curve="90"/>
<wire x1="3.861" y1="1.216" x2="4.115" y2="0.962" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.115" y1="-0.962" x2="-4.115" y2="0.962" width="0.1524" layer="21"/>
<wire x1="-3.861" y1="1.216" x2="-3.48" y2="1.216" width="0.1524" layer="21"/>
<wire x1="-3.353" y1="1.089" x2="-3.48" y2="1.216" width="0.1524" layer="21"/>
<wire x1="-3.861" y1="-1.216" x2="-3.48" y2="-1.216" width="0.1524" layer="21"/>
<wire x1="-3.353" y1="-1.089" x2="-3.48" y2="-1.216" width="0.1524" layer="21"/>
<wire x1="3.353" y1="1.089" x2="3.48" y2="1.216" width="0.1524" layer="21"/>
<wire x1="3.353" y1="1.089" x2="-3.353" y2="1.089" width="0.1524" layer="21"/>
<wire x1="3.353" y1="-1.089" x2="3.48" y2="-1.216" width="0.1524" layer="21"/>
<wire x1="3.353" y1="-1.089" x2="-3.353" y2="-1.089" width="0.1524" layer="21"/>
<wire x1="3.861" y1="1.216" x2="3.48" y2="1.216" width="0.1524" layer="21"/>
<wire x1="3.861" y1="-1.216" x2="3.48" y2="-1.216" width="0.1524" layer="21"/>
<wire x1="4.115" y1="-0.962" x2="4.115" y2="0.962" width="0.1524" layer="21"/>
<pad name="1" x="-5" y="0" drill="0.8" diameter="1.3"/>
<pad name="2" x="5" y="0" drill="0.8" diameter="1.3"/>
<text x="-3.81" y="1.905" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.115" y1="-0.254" x2="4.4" y2="0.254" layer="21"/>
<rectangle x1="-4.4" y1="-0.254" x2="-4.115" y2="0.254" layer="21"/>
</package>
<package name="LAL04">
<description>&lt;b&gt;TAYO YUDEN Inductor&lt;/b&gt;&lt;p&gt;
Source: je999f5.pdf</description>
<wire x1="6.35" y1="0" x2="5.13" y2="0" width="0.508" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.105" y2="0" width="0.508" layer="51"/>
<wire x1="-4.815" y1="1.662" x2="-4.561" y2="1.916" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.815" y1="-1.662" x2="-4.561" y2="-1.916" width="0.1524" layer="21" curve="90"/>
<wire x1="4.586" y1="-1.916" x2="4.84" y2="-1.662" width="0.1524" layer="21" curve="90"/>
<wire x1="4.586" y1="1.916" x2="4.84" y2="1.662" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.815" y1="-1.662" x2="-4.815" y2="1.662" width="0.1524" layer="21"/>
<wire x1="-4.561" y1="1.916" x2="-4.18" y2="1.916" width="0.1524" layer="21"/>
<wire x1="-4.053" y1="1.789" x2="-4.18" y2="1.916" width="0.1524" layer="21"/>
<wire x1="-4.561" y1="-1.916" x2="-4.18" y2="-1.916" width="0.1524" layer="21"/>
<wire x1="-4.053" y1="-1.789" x2="-4.18" y2="-1.916" width="0.1524" layer="21"/>
<wire x1="4.078" y1="1.789" x2="4.205" y2="1.916" width="0.1524" layer="21"/>
<wire x1="4.078" y1="1.789" x2="-4.053" y2="1.789" width="0.1524" layer="21"/>
<wire x1="4.078" y1="-1.789" x2="4.205" y2="-1.916" width="0.1524" layer="21"/>
<wire x1="4.078" y1="-1.789" x2="-4.053" y2="-1.789" width="0.1524" layer="21"/>
<wire x1="4.586" y1="1.916" x2="4.205" y2="1.916" width="0.1524" layer="21"/>
<wire x1="4.586" y1="-1.916" x2="4.205" y2="-1.916" width="0.1524" layer="21"/>
<wire x1="4.84" y1="-1.662" x2="4.84" y2="1.662" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8" diameter="1.3"/>
<pad name="2" x="6.35" y="0" drill="0.8" diameter="1.3"/>
<text x="-4.445" y="2.54" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.84" y1="-0.254" x2="5.125" y2="0.254" layer="21"/>
<rectangle x1="-5.1" y1="-0.254" x2="-4.815" y2="0.254" layer="21"/>
</package>
<package name="LAL04KB">
<description>&lt;b&gt;TAYO YUDEN Inductor&lt;/b&gt;&lt;p&gt;
Source: je999f5.pdf</description>
<wire x1="6.25" y1="0" x2="5.13" y2="0" width="0.508" layer="51"/>
<wire x1="-6.25" y1="0" x2="-5.105" y2="0" width="0.508" layer="51"/>
<wire x1="-4.815" y1="1.662" x2="-4.561" y2="1.916" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.815" y1="-1.662" x2="-4.561" y2="-1.916" width="0.1524" layer="21" curve="90"/>
<wire x1="4.586" y1="-1.916" x2="4.84" y2="-1.662" width="0.1524" layer="21" curve="90"/>
<wire x1="4.586" y1="1.916" x2="4.84" y2="1.662" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.815" y1="-1.662" x2="-4.815" y2="1.662" width="0.1524" layer="21"/>
<wire x1="-4.561" y1="1.916" x2="-4.18" y2="1.916" width="0.1524" layer="21"/>
<wire x1="-4.053" y1="1.789" x2="-4.18" y2="1.916" width="0.1524" layer="21"/>
<wire x1="-4.561" y1="-1.916" x2="-4.18" y2="-1.916" width="0.1524" layer="21"/>
<wire x1="-4.053" y1="-1.789" x2="-4.18" y2="-1.916" width="0.1524" layer="21"/>
<wire x1="4.078" y1="1.789" x2="4.205" y2="1.916" width="0.1524" layer="21"/>
<wire x1="4.078" y1="1.789" x2="-4.053" y2="1.789" width="0.1524" layer="21"/>
<wire x1="4.078" y1="-1.789" x2="4.205" y2="-1.916" width="0.1524" layer="21"/>
<wire x1="4.078" y1="-1.789" x2="-4.053" y2="-1.789" width="0.1524" layer="21"/>
<wire x1="4.586" y1="1.916" x2="4.205" y2="1.916" width="0.1524" layer="21"/>
<wire x1="4.586" y1="-1.916" x2="4.205" y2="-1.916" width="0.1524" layer="21"/>
<wire x1="4.84" y1="-1.662" x2="4.84" y2="1.662" width="0.1524" layer="21"/>
<pad name="1" x="-6.25" y="0" drill="0.8" diameter="1.3"/>
<pad name="2" x="6.25" y="0" drill="0.8" diameter="1.3"/>
<text x="-4.445" y="2.54" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.84" y1="-0.254" x2="5.125" y2="0.254" layer="21"/>
<rectangle x1="-5.1" y1="-0.254" x2="-4.815" y2="0.254" layer="21"/>
</package>
<package name="LAN02KR">
<description>&lt;b&gt;TAYO YUDEN Inductor&lt;/b&gt;&lt;p&gt;
Source: je999f5.pdf</description>
<wire x1="2.5" y1="0" x2="1.905" y2="0" width="0.508" layer="51"/>
<wire x1="-2.5" y1="0" x2="-1.905" y2="0" width="0.508" layer="51"/>
<wire x1="-1.615" y1="0.862" x2="-1.361" y2="1.116" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.615" y1="-0.862" x2="-1.361" y2="-1.116" width="0.1524" layer="21" curve="90"/>
<wire x1="1.361" y1="-1.116" x2="1.615" y2="-0.862" width="0.1524" layer="21" curve="90"/>
<wire x1="1.361" y1="1.116" x2="1.615" y2="0.862" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.615" y1="-0.862" x2="-1.615" y2="0.862" width="0.1524" layer="21"/>
<wire x1="-1.361" y1="1.116" x2="-0.98" y2="1.116" width="0.1524" layer="21"/>
<wire x1="-0.853" y1="0.989" x2="-0.98" y2="1.116" width="0.1524" layer="21"/>
<wire x1="-1.361" y1="-1.116" x2="-0.98" y2="-1.116" width="0.1524" layer="21"/>
<wire x1="-0.853" y1="-0.989" x2="-0.98" y2="-1.116" width="0.1524" layer="21"/>
<wire x1="0.853" y1="0.989" x2="0.98" y2="1.116" width="0.1524" layer="21"/>
<wire x1="0.853" y1="0.989" x2="-0.853" y2="0.989" width="0.1524" layer="21"/>
<wire x1="0.853" y1="-0.989" x2="0.98" y2="-1.116" width="0.1524" layer="21"/>
<wire x1="0.853" y1="-0.989" x2="-0.853" y2="-0.989" width="0.1524" layer="21"/>
<wire x1="1.361" y1="1.116" x2="0.98" y2="1.116" width="0.1524" layer="21"/>
<wire x1="1.361" y1="-1.116" x2="0.98" y2="-1.116" width="0.1524" layer="21"/>
<wire x1="1.615" y1="-0.862" x2="1.615" y2="0.862" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.8" diameter="1.3"/>
<pad name="2" x="2.5" y="0" drill="0.8" diameter="1.3"/>
<text x="-2.54" y="1.27" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.615" y1="-0.254" x2="1.9" y2="0.254" layer="21"/>
<rectangle x1="-1.9" y1="-0.254" x2="-1.615" y2="0.254" layer="21"/>
</package>
<package name="LAP02KR">
<description>&lt;b&gt;TAYO YUDEN Inductor&lt;/b&gt;&lt;p&gt;
Source: je999f5.pdf</description>
<wire x1="2.5" y1="0" x2="1.905" y2="0" width="0.508" layer="51"/>
<wire x1="-2.5" y1="0" x2="-1.905" y2="0" width="0.508" layer="51"/>
<wire x1="-1.615" y1="0.812" x2="-1.361" y2="1.066" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.615" y1="-0.812" x2="-1.361" y2="-1.066" width="0.1524" layer="21" curve="90"/>
<wire x1="1.361" y1="-1.066" x2="1.615" y2="-0.812" width="0.1524" layer="21" curve="90"/>
<wire x1="1.361" y1="1.066" x2="1.615" y2="0.812" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.615" y1="-0.812" x2="-1.615" y2="0.812" width="0.1524" layer="21"/>
<wire x1="-1.361" y1="1.066" x2="-0.98" y2="1.066" width="0.1524" layer="21"/>
<wire x1="-0.853" y1="0.939" x2="-0.98" y2="1.066" width="0.1524" layer="21"/>
<wire x1="-1.361" y1="-1.066" x2="-0.98" y2="-1.066" width="0.1524" layer="21"/>
<wire x1="-0.853" y1="-0.939" x2="-0.98" y2="-1.066" width="0.1524" layer="21"/>
<wire x1="0.853" y1="0.939" x2="0.98" y2="1.066" width="0.1524" layer="21"/>
<wire x1="0.853" y1="0.939" x2="-0.853" y2="0.939" width="0.1524" layer="21"/>
<wire x1="0.853" y1="-0.939" x2="0.98" y2="-1.066" width="0.1524" layer="21"/>
<wire x1="0.853" y1="-0.939" x2="-0.853" y2="-0.939" width="0.1524" layer="21"/>
<wire x1="1.361" y1="1.066" x2="0.98" y2="1.066" width="0.1524" layer="21"/>
<wire x1="1.361" y1="-1.066" x2="0.98" y2="-1.066" width="0.1524" layer="21"/>
<wire x1="1.615" y1="-0.812" x2="1.615" y2="0.812" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.8" diameter="1.3"/>
<pad name="2" x="2.5" y="0" drill="0.8" diameter="1.3"/>
<text x="-2.54" y="1.27" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.615" y1="-0.254" x2="1.9" y2="0.254" layer="21"/>
<rectangle x1="-1.9" y1="-0.254" x2="-1.615" y2="0.254" layer="21"/>
</package>
<package name="TFI0204">
<description>&lt;b&gt;Axial Conformal Coated Inductor&lt;/b&gt;&lt;p&gt;
Source: TOP MAGNETICS CORPORATION .. tfi.pdf</description>
<wire x1="3.81" y1="0" x2="2.54" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.54" y2="0" width="0.508" layer="51"/>
<wire x1="-2.165" y1="0.812" x2="-1.911" y2="1.066" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.165" y1="-0.812" x2="-1.911" y2="-1.066" width="0.1524" layer="21" curve="90"/>
<wire x1="1.911" y1="-1.066" x2="2.165" y2="-0.812" width="0.1524" layer="21" curve="90"/>
<wire x1="1.911" y1="1.066" x2="2.165" y2="0.812" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.165" y1="-0.812" x2="-2.165" y2="0.812" width="0.1524" layer="21"/>
<wire x1="-1.911" y1="1.066" x2="-1.53" y2="1.066" width="0.1524" layer="21"/>
<wire x1="-1.403" y1="0.939" x2="-1.53" y2="1.066" width="0.1524" layer="21"/>
<wire x1="-1.911" y1="-1.066" x2="-1.53" y2="-1.066" width="0.1524" layer="21"/>
<wire x1="-1.403" y1="-0.939" x2="-1.53" y2="-1.066" width="0.1524" layer="21"/>
<wire x1="1.403" y1="0.939" x2="1.53" y2="1.066" width="0.1524" layer="21"/>
<wire x1="1.403" y1="0.939" x2="-1.403" y2="0.939" width="0.1524" layer="21"/>
<wire x1="1.403" y1="-0.939" x2="1.53" y2="-1.066" width="0.1524" layer="21"/>
<wire x1="1.403" y1="-0.939" x2="-1.403" y2="-0.939" width="0.1524" layer="21"/>
<wire x1="1.911" y1="1.066" x2="1.53" y2="1.066" width="0.1524" layer="21"/>
<wire x1="1.911" y1="-1.066" x2="1.53" y2="-1.066" width="0.1524" layer="21"/>
<wire x1="2.165" y1="-0.812" x2="2.165" y2="0.812" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9" diameter="1.5"/>
<pad name="2" x="3.81" y="0" drill="0.9" diameter="1.5"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.165" y1="-0.254" x2="2.546" y2="0.254" layer="21"/>
<rectangle x1="-2.546" y1="-0.254" x2="-2.165" y2="0.254" layer="21"/>
</package>
<package name="TFI0305">
<description>&lt;b&gt;Axial Conformal Coated Inductor&lt;/b&gt;&lt;p&gt;
Source: TOP MAGNETICS CORPORATION .. tfi.pdf</description>
<wire x1="3.81" y1="0" x2="2.79" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.79" y2="0" width="0.508" layer="51"/>
<wire x1="-2.415" y1="1.162" x2="-2.161" y2="1.416" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.415" y1="-1.162" x2="-2.161" y2="-1.416" width="0.1524" layer="21" curve="90"/>
<wire x1="2.161" y1="-1.416" x2="2.415" y2="-1.162" width="0.1524" layer="21" curve="90"/>
<wire x1="2.161" y1="1.416" x2="2.415" y2="1.162" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.415" y1="-1.162" x2="-2.415" y2="1.162" width="0.1524" layer="21"/>
<wire x1="-2.161" y1="1.416" x2="-1.78" y2="1.416" width="0.1524" layer="21"/>
<wire x1="-1.653" y1="1.289" x2="-1.78" y2="1.416" width="0.1524" layer="21"/>
<wire x1="-2.161" y1="-1.416" x2="-1.78" y2="-1.416" width="0.1524" layer="21"/>
<wire x1="-1.653" y1="-1.289" x2="-1.78" y2="-1.416" width="0.1524" layer="21"/>
<wire x1="1.653" y1="1.289" x2="1.78" y2="1.416" width="0.1524" layer="21"/>
<wire x1="1.653" y1="1.289" x2="-1.653" y2="1.289" width="0.1524" layer="21"/>
<wire x1="1.653" y1="-1.289" x2="1.78" y2="-1.416" width="0.1524" layer="21"/>
<wire x1="1.653" y1="-1.289" x2="-1.653" y2="-1.289" width="0.1524" layer="21"/>
<wire x1="2.161" y1="1.416" x2="1.78" y2="1.416" width="0.1524" layer="21"/>
<wire x1="2.161" y1="-1.416" x2="1.78" y2="-1.416" width="0.1524" layer="21"/>
<wire x1="2.415" y1="-1.162" x2="2.415" y2="1.162" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9" diameter="1.5"/>
<pad name="2" x="3.81" y="0" drill="0.9" diameter="1.5"/>
<text x="-2.54" y="1.6454" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.8756" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.415" y1="-0.254" x2="2.796" y2="0.254" layer="21"/>
<rectangle x1="-2.796" y1="-0.254" x2="-2.415" y2="0.254" layer="21"/>
</package>
<package name="TFI0307">
<description>&lt;b&gt;Axial Conformal Coated Inductor&lt;/b&gt;&lt;p&gt;
Source: TOP MAGNETICS CORPORATION .. tfi.pdf</description>
<wire x1="4.445" y1="0" x2="3.79" y2="0" width="0.508" layer="51"/>
<wire x1="-4.445" y1="0" x2="-3.79" y2="0" width="0.508" layer="51"/>
<wire x1="-3.415" y1="1.162" x2="-3.161" y2="1.416" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.415" y1="-1.162" x2="-3.161" y2="-1.416" width="0.1524" layer="21" curve="90"/>
<wire x1="3.161" y1="-1.416" x2="3.415" y2="-1.162" width="0.1524" layer="21" curve="90"/>
<wire x1="3.161" y1="1.416" x2="3.415" y2="1.162" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.415" y1="-1.162" x2="-3.415" y2="1.162" width="0.1524" layer="21"/>
<wire x1="-3.161" y1="1.416" x2="-2.78" y2="1.416" width="0.1524" layer="21"/>
<wire x1="-2.653" y1="1.289" x2="-2.78" y2="1.416" width="0.1524" layer="21"/>
<wire x1="-3.161" y1="-1.416" x2="-2.78" y2="-1.416" width="0.1524" layer="21"/>
<wire x1="-2.653" y1="-1.289" x2="-2.78" y2="-1.416" width="0.1524" layer="21"/>
<wire x1="2.653" y1="1.289" x2="2.78" y2="1.416" width="0.1524" layer="21"/>
<wire x1="2.653" y1="1.289" x2="-2.653" y2="1.289" width="0.1524" layer="21"/>
<wire x1="2.653" y1="-1.289" x2="2.78" y2="-1.416" width="0.1524" layer="21"/>
<wire x1="2.653" y1="-1.289" x2="-2.653" y2="-1.289" width="0.1524" layer="21"/>
<wire x1="3.161" y1="1.416" x2="2.78" y2="1.416" width="0.1524" layer="21"/>
<wire x1="3.161" y1="-1.416" x2="2.78" y2="-1.416" width="0.1524" layer="21"/>
<wire x1="3.415" y1="-1.162" x2="3.415" y2="1.162" width="0.1524" layer="21"/>
<pad name="1" x="-4.445" y="0" drill="0.9" diameter="1.5"/>
<pad name="2" x="4.445" y="0" drill="0.9" diameter="1.5"/>
<text x="-2.54" y="1.6454" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.8756" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.415" y1="-0.254" x2="3.796" y2="0.254" layer="21"/>
<rectangle x1="-3.796" y1="-0.254" x2="-3.415" y2="0.254" layer="21"/>
</package>
<package name="TFI0410">
<description>&lt;b&gt;Axial Conformal Coated Inductor&lt;/b&gt;&lt;p&gt;
Source: TOP MAGNETICS CORPORATION .. tfi.pdf</description>
<wire x1="6.35" y1="0" x2="5.54" y2="0" width="0.508" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.54" y2="0" width="0.508" layer="51"/>
<wire x1="-5.165" y1="1.662" x2="-4.911" y2="1.916" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.165" y1="-1.662" x2="-4.911" y2="-1.916" width="0.1524" layer="21" curve="90"/>
<wire x1="4.911" y1="-1.916" x2="5.165" y2="-1.662" width="0.1524" layer="21" curve="90"/>
<wire x1="4.911" y1="1.916" x2="5.165" y2="1.662" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.165" y1="-1.662" x2="-5.165" y2="1.662" width="0.1524" layer="21"/>
<wire x1="-4.911" y1="1.916" x2="-4.53" y2="1.916" width="0.1524" layer="21"/>
<wire x1="-4.403" y1="1.789" x2="-4.53" y2="1.916" width="0.1524" layer="21"/>
<wire x1="-4.911" y1="-1.916" x2="-4.53" y2="-1.916" width="0.1524" layer="21"/>
<wire x1="-4.403" y1="-1.789" x2="-4.53" y2="-1.916" width="0.1524" layer="21"/>
<wire x1="4.403" y1="1.789" x2="4.53" y2="1.916" width="0.1524" layer="21"/>
<wire x1="4.403" y1="1.789" x2="-4.403" y2="1.789" width="0.1524" layer="21"/>
<wire x1="4.403" y1="-1.789" x2="4.53" y2="-1.916" width="0.1524" layer="21"/>
<wire x1="4.403" y1="-1.789" x2="-4.403" y2="-1.789" width="0.1524" layer="21"/>
<wire x1="4.911" y1="1.916" x2="4.53" y2="1.916" width="0.1524" layer="21"/>
<wire x1="4.911" y1="-1.916" x2="4.53" y2="-1.916" width="0.1524" layer="21"/>
<wire x1="5.165" y1="-1.662" x2="5.165" y2="1.662" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="1" diameter="1.6"/>
<pad name="2" x="6.35" y="0" drill="1" diameter="1.6"/>
<text x="-2.54" y="2.1454" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.8756" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.165" y1="-0.254" x2="5.546" y2="0.254" layer="21"/>
<rectangle x1="-5.546" y1="-0.254" x2="-5.165" y2="0.254" layer="21"/>
</package>
<package name="TFI0510">
<description>&lt;b&gt;Axial Conformal Coated Inductor&lt;/b&gt;&lt;p&gt;
Source: TOP MAGNETICS CORPORATION .. tfi.pdf</description>
<wire x1="7.62" y1="0" x2="6.315" y2="0" width="0.508" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.29" y2="0" width="0.508" layer="51"/>
<wire x1="-5.915" y1="2.162" x2="-5.661" y2="2.416" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.915" y1="-2.187" x2="-5.661" y2="-2.441" width="0.1524" layer="21" curve="90"/>
<wire x1="5.686" y1="-2.441" x2="5.94" y2="-2.187" width="0.1524" layer="21" curve="90"/>
<wire x1="5.686" y1="2.416" x2="5.94" y2="2.162" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.915" y1="-2.187" x2="-5.915" y2="2.162" width="0.1524" layer="21"/>
<wire x1="-5.661" y1="2.416" x2="-5.28" y2="2.416" width="0.1524" layer="21"/>
<wire x1="-5.153" y1="2.289" x2="-5.28" y2="2.416" width="0.1524" layer="21"/>
<wire x1="-5.661" y1="-2.441" x2="-5.28" y2="-2.441" width="0.1524" layer="21"/>
<wire x1="-5.153" y1="-2.314" x2="-5.28" y2="-2.441" width="0.1524" layer="21"/>
<wire x1="5.178" y1="2.289" x2="5.305" y2="2.416" width="0.1524" layer="21"/>
<wire x1="5.178" y1="2.289" x2="-5.153" y2="2.289" width="0.1524" layer="21"/>
<wire x1="5.178" y1="-2.314" x2="5.305" y2="-2.441" width="0.1524" layer="21"/>
<wire x1="5.178" y1="-2.314" x2="-5.153" y2="-2.314" width="0.1524" layer="21"/>
<wire x1="5.686" y1="2.416" x2="5.305" y2="2.416" width="0.1524" layer="21"/>
<wire x1="5.686" y1="-2.441" x2="5.305" y2="-2.441" width="0.1524" layer="21"/>
<wire x1="5.94" y1="-2.187" x2="5.94" y2="2.162" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1" diameter="1.6"/>
<pad name="2" x="7.62" y="0" drill="1" diameter="1.6"/>
<text x="-2.54" y="2.6454" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.8756" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.94" y1="-0.254" x2="6.321" y2="0.254" layer="21"/>
<rectangle x1="-6.296" y1="-0.254" x2="-5.915" y2="0.254" layer="21"/>
</package>
<package name="6000-XXXX-RC">
<description>&lt;b&gt;Radial Lead RF Chokes&lt;/b&gt;&lt;p&gt;
Source: www.bourns.com .. 6000_series.pdf</description>
<circle x="0" y="0" radius="4.4" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.9" diameter="1.4224"/>
<pad name="2" x="2.5" y="0" drill="0.9" diameter="1.4224"/>
<text x="-4.5056" y="4.6326" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.5056" y="-6.4106" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="L-EU">
<text x="-1.4986" y="-3.81" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.302" y="-3.81" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.016" y1="-3.556" x2="1.016" y2="3.556" layer="94"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-EU_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1005" package="R1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-EU" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1005" package="C1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HPC0201" package="HPC0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3640" package="C3640">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L-EU" prefix="L" uservalue="yes">
<description>&lt;B&gt;INDUCTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="L-EU" x="0" y="0"/>
</gates>
<devices>
<device name="L2012C" package="L2012C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L2825P" package="L2825P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L3216C" package="L3216C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L3225M" package="L3225M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L3225P" package="L3225P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L3230M" package="L3230M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L4035M" package="L4035M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L4516C" package="L4516C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L4532C" package="L4532M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L4532P" package="L4532P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L5038P" package="L5038P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L5650M" package="L5650M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L8530M" package="L8530M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L1812" package="L1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ3-U1" package="TJ3-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ3-U2" package="TJ3-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ4-U1" package="TJ4-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ4-U2" package="TJ4-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ5-U1" package="TJ5-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ5-U2" package="TJ5-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ6-U1" package="TJ6-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ6-U2" package="TJ6-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ7-U1" package="TJ7-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ7-U2" package="TJ7-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ8-U1" package="TJ8-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ8-U2" package="TJ8-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ9-U1" package="TJ9-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ9-U2" package="TJ9-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WE-TPC" package="POWER-CHOKE_WE-TPC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2200-12.7" package="2200-12.7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2200-15.24" package="2200-15.24">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2200-11.43" package="2200-11.43">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CEP125" package="CEP125">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L0201" package="L0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PIS2816" package="PIS2816">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IR-2" package="IR-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IR-4" package="IR-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRF-1" package="IRF-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRF-3" package="IRF-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRF-24" package="IRF24">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRF-36" package="IRF36">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRF-46" package="IRF46">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LAL02" package="LAL02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LAL03" package="LAL03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LAL03KB" package="LAL03KH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LAL04" package="LAL04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LAL04KB" package="LAL04KB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LAN02KR" package="LAN02KR">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LAP02KR" package="LAP02KR">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TFI0204" package="TFI0204">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TFI0305" package="TFI0305">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TFI0307" package="TFI0307">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TFI0410" package="TFI0410">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TFI0510" package="TFI0510">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6000-XXX-RC" package="6000-XXXX-RC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-headers-jp">
<description>&lt;b&gt;Male(J), Female(P) and Pin Headers&lt;/b&gt;&lt;p&gt;Version 1.02 - 03/10/2009&lt;p&gt;
This library is a collection of various pin headers and AMP, Molex and 3M board-to-board connectors. 
This library assumes the notion of JACKS(male) and PLUGS(female) as represented by the J and P symbols.
Standard male/female pin header types are available in common 0.1" and 2 mm pitch types with alternate mirrored footprints as well.
&lt;p&gt;THIS LIBRARY IS PROVIDED AS IS AND WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED.&lt;br&gt;
USE AT YOUR OWN RISK!&lt;p&gt;
&lt;author&gt;Copyright (C) 2008, Bob Starr&lt;br&gt; http://www.bobstarr.net&lt;br&gt;&lt;/author&gt;</description>
<packages>
<package name="DIL-100-2X05">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 0.1"</description>
<wire x1="-6.35" y1="-2.54" x2="-6.35" y2="0" width="0.254" layer="21"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="2.54" width="0.254" layer="21"/>
<wire x1="-6.35" y1="-2.54" x2="-3.81" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="6.35" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-6.35" y1="2.54" x2="6.35" y2="2.54" width="0.254" layer="21"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-3.81" y1="0" x2="-6.35" y2="0" width="0.254" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-2.54" width="0.254" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1.016" shape="square"/>
<pad name="2" x="-5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<text x="-6.985" y="-2.54" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="7.62" y="-2.54" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="1.016" x2="-4.826" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
</package>
<package name="DIL-100-2X05_M">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - MIRRORED 0.1"</description>
<wire x1="-6.35" y1="-2.54" x2="-6.35" y2="0" width="0.254" layer="21"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="2.54" width="0.254" layer="21"/>
<wire x1="-6.35" y1="-2.54" x2="6.35" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-6.35" y1="2.54" x2="-3.81" y2="2.54" width="0.254" layer="21"/>
<wire x1="-3.81" y1="2.54" x2="6.35" y2="2.54" width="0.254" layer="21"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="2.54" width="0.254" layer="21"/>
<wire x1="-3.81" y1="0" x2="-6.35" y2="0" width="0.254" layer="21"/>
<pad name="1" x="-5.08" y="1.27" drill="1.016" shape="square"/>
<pad name="2" x="-5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<text x="-6.985" y="-2.54" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="7.62" y="-2.54" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.334" y1="1.016" x2="-4.826" y2="1.524" layer="51"/>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
</package>
<package name="DIL-2MM-2X05">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2MM"</description>
<wire x1="5.25" y1="2.25" x2="5.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.25" x2="-3" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.25" x2="-5.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="-2.25" x2="-5.25" y2="0" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="0" x2="-5.25" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="2.25" x2="5.25" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3" y1="0" x2="-5.25" y2="0" width="0.2032" layer="21"/>
<wire x1="-3" y1="0" x2="-3" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-4" y="-1" drill="0.7112" shape="square"/>
<pad name="2" x="-4" y="1" drill="0.7112"/>
<pad name="3" x="-2" y="-1" drill="0.7112"/>
<pad name="4" x="-2" y="1" drill="0.7112"/>
<pad name="5" x="0" y="-1" drill="0.7112"/>
<pad name="6" x="0" y="1" drill="0.7112"/>
<pad name="7" x="2" y="-1" drill="0.7112"/>
<pad name="8" x="2" y="1" drill="0.7112"/>
<pad name="9" x="4" y="-1" drill="0.7112"/>
<pad name="10" x="4" y="1" drill="0.7112"/>
<text x="-6" y="-2" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="7" y="-2" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.25" y1="0.75" x2="-3.75" y2="1.25" layer="51"/>
<rectangle x1="-4.25" y1="-1.25" x2="-3.75" y2="-0.75" layer="51"/>
<rectangle x1="-2.25" y1="0.75" x2="-1.75" y2="1.25" layer="51"/>
<rectangle x1="-2.25" y1="-1.25" x2="-1.75" y2="-0.75" layer="51"/>
<rectangle x1="-0.25" y1="0.75" x2="0.25" y2="1.25" layer="51"/>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="-0.75" layer="51"/>
<rectangle x1="1.75" y1="0.75" x2="2.25" y2="1.25" layer="51"/>
<rectangle x1="1.75" y1="-1.25" x2="2.25" y2="-0.75" layer="51"/>
<rectangle x1="3.75" y1="0.75" x2="4.25" y2="1.25" layer="51"/>
<rectangle x1="3.75" y1="-1.25" x2="4.25" y2="-0.75" layer="51"/>
</package>
<package name="DIL-2MM-2X05_M">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - MIRRORED 2MM</description>
<wire x1="5.25" y1="2.25" x2="5.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.25" x2="-5.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="-2.25" x2="-5.25" y2="0" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="0" x2="-5.25" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="2.25" x2="-3" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.25" x2="5.25" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3" y1="0" x2="-3" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3" y1="0" x2="-5.25" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-4" y="1" drill="0.7112" shape="square"/>
<pad name="2" x="-4" y="-1" drill="0.7112"/>
<pad name="3" x="-2" y="1" drill="0.7112"/>
<pad name="4" x="-2" y="-1" drill="0.7112"/>
<pad name="5" x="0" y="1" drill="0.7112"/>
<pad name="6" x="0" y="-1" drill="0.7112"/>
<pad name="7" x="2" y="1" drill="0.7112"/>
<pad name="8" x="2" y="-1" drill="0.7112"/>
<pad name="9" x="4" y="1" drill="0.7112"/>
<pad name="10" x="4" y="-1" drill="0.7112"/>
<text x="-6" y="-2" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="7" y="-2" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.25" y1="-1.25" x2="-3.75" y2="-0.75" layer="51"/>
<rectangle x1="-4.25" y1="0.75" x2="-3.75" y2="1.25" layer="51"/>
<rectangle x1="-2.25" y1="-1.25" x2="-1.75" y2="-0.75" layer="51"/>
<rectangle x1="-2.25" y1="0.75" x2="-1.75" y2="1.25" layer="51"/>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="-0.75" layer="51"/>
<rectangle x1="-0.25" y1="0.75" x2="0.25" y2="1.25" layer="51"/>
<rectangle x1="1.75" y1="-1.25" x2="2.25" y2="-0.75" layer="51"/>
<rectangle x1="1.75" y1="0.75" x2="2.25" y2="1.25" layer="51"/>
<rectangle x1="3.75" y1="-1.25" x2="4.25" y2="-0.75" layer="51"/>
<rectangle x1="3.75" y1="0.75" x2="4.25" y2="1.25" layer="51"/>
</package>
<package name="87759-10">
<description>&lt;b&gt;Molex PCB Pin Headers&lt;/b&gt;&lt;p&gt; 
2.00mm (.079") Pitch Milli-Grid Header, Vertical SMD</description>
<wire x1="5.5" y1="2.5" x2="5.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-2.5" x2="-5.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="-2.5" x2="-5.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="2.5" x2="5.5" y2="2.5" width="0.2032" layer="21"/>
<smd name="2" x="-4" y="2.15" dx="1" dy="2.75" layer="1"/>
<smd name="4" x="-2" y="2.15" dx="1" dy="2.75" layer="1"/>
<smd name="1" x="-4" y="-2.15" dx="1" dy="2.75" layer="1"/>
<smd name="3" x="-2" y="-2.15" dx="1" dy="2.75" layer="1"/>
<smd name="5" x="0" y="-2.15" dx="1" dy="2.75" layer="1"/>
<smd name="6" x="0" y="2.15" dx="1" dy="2.75" layer="1"/>
<smd name="7" x="2" y="-2.15" dx="1" dy="2.75" layer="1"/>
<smd name="8" x="2" y="2.15" dx="1" dy="2.75" layer="1"/>
<smd name="9" x="4" y="-2.15" dx="1" dy="2.75" layer="1"/>
<smd name="10" x="4" y="2.15" dx="1" dy="2.75" layer="1"/>
<text x="-6.0325" y="-2.4925" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="6.985" y="-2.4925" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-5.25" y="-1.75" size="0.6096" layer="51" ratio="10">1</text>
<rectangle x1="-4.5" y1="0.5" x2="-3.5" y2="1.5" layer="51"/>
<rectangle x1="-2.5" y1="0.5" x2="-1.5" y2="1.5" layer="51"/>
<rectangle x1="-0.5" y1="0.5" x2="0.5" y2="1.5" layer="51"/>
<rectangle x1="1.5" y1="0.5" x2="2.5" y2="1.5" layer="51"/>
<rectangle x1="-4.5" y1="-1.5" x2="-3.5" y2="-0.5" layer="51"/>
<rectangle x1="-2.5" y1="-1.5" x2="-1.5" y2="-0.5" layer="51"/>
<rectangle x1="-0.5" y1="-1.5" x2="0.5" y2="-0.5" layer="51"/>
<rectangle x1="1.5" y1="-1.5" x2="2.5" y2="-0.5" layer="51"/>
<rectangle x1="3.5" y1="-1.5" x2="4.5" y2="-0.5" layer="51"/>
<rectangle x1="3.5" y1="0.5" x2="4.5" y2="1.5" layer="51"/>
<hole x="-3" y="0" drill="1"/>
<hole x="3" y="0" drill="1"/>
</package>
<package name="87759-10_M">
<description>&lt;b&gt;Molex PCB Pin Headers&lt;/b&gt; -  MIRRORED PADS&lt;p&gt; 
2.00mm (.079") Pitch Milli-Grid Header, Vertical SMD</description>
<wire x1="5.5" y1="2.5" x2="5.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-2.5" x2="-5.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="-2.5" x2="-5.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="2.5" x2="5.5" y2="2.5" width="0.2032" layer="21"/>
<smd name="2" x="-4" y="-2.15" dx="1" dy="2.75" layer="1" rot="R180"/>
<smd name="4" x="-2" y="-2.15" dx="1" dy="2.75" layer="1" rot="R180"/>
<smd name="1" x="-4" y="2.15" dx="1" dy="2.75" layer="1" rot="R180"/>
<smd name="3" x="-2" y="2.15" dx="1" dy="2.75" layer="1" rot="R180"/>
<smd name="5" x="0" y="2.15" dx="1" dy="2.75" layer="1" rot="R180"/>
<smd name="6" x="0" y="-2.15" dx="1" dy="2.75" layer="1" rot="R180"/>
<smd name="7" x="2" y="2.15" dx="1" dy="2.75" layer="1" rot="R180"/>
<smd name="8" x="2" y="-2.15" dx="1" dy="2.75" layer="1" rot="R180"/>
<smd name="9" x="4" y="2.15" dx="1" dy="2.75" layer="1" rot="R180"/>
<smd name="10" x="4" y="-2.15" dx="1" dy="2.75" layer="1" rot="R180"/>
<text x="-6.0325" y="-2.4925" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="6.985" y="-2.4925" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-5.25" y="1.25" size="0.6096" layer="51" ratio="10">1</text>
<rectangle x1="-4.5" y1="0.5" x2="-3.5" y2="1.5" layer="51"/>
<rectangle x1="-2.5" y1="0.5" x2="-1.5" y2="1.5" layer="51"/>
<rectangle x1="-0.5" y1="0.5" x2="0.5" y2="1.5" layer="51"/>
<rectangle x1="1.5" y1="0.5" x2="2.5" y2="1.5" layer="51"/>
<rectangle x1="-4.5" y1="-1.5" x2="-3.5" y2="-0.5" layer="51"/>
<rectangle x1="-2.5" y1="-1.5" x2="-1.5" y2="-0.5" layer="51"/>
<rectangle x1="-0.5" y1="-1.5" x2="0.5" y2="-0.5" layer="51"/>
<rectangle x1="1.5" y1="-1.5" x2="2.5" y2="-0.5" layer="51"/>
<rectangle x1="3.5" y1="-1.5" x2="4.5" y2="-0.5" layer="51"/>
<rectangle x1="3.5" y1="0.5" x2="4.5" y2="1.5" layer="51"/>
<hole x="-3" y="0" drill="1"/>
<hole x="3" y="0" drill="1"/>
</package>
<package name="DIL-100-2X05-BRD">
<wire x1="-6.35" y1="-2.54" x2="-6.35" y2="0" width="0.254" layer="21"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="2.54" width="0.254" layer="21"/>
<wire x1="-6.35" y1="-2.54" x2="-3.81" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="6.35" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-6.35" y1="2.54" x2="6.35" y2="2.54" width="0.254" layer="21"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-3.81" y1="0" x2="-6.35" y2="0" width="0.254" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-2.54" width="0.254" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1.016" shape="square"/>
<pad name="2" x="-5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<text x="-10.795" y="-2.7178" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="11.4554" y="-2.2352" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="1.016" x2="-4.826" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
<wire x1="-10.0076" y1="4.5212" x2="10.0076" y2="4.5212" width="0.127" layer="21" style="shortdash"/>
<wire x1="-10.0076" y1="-4.5212" x2="10.0076" y2="-4.5212" width="0.127" layer="21" style="shortdash"/>
<wire x1="-10.0076" y1="4.5212" x2="-10.0076" y2="-4.5212" width="0.127" layer="21" style="shortdash"/>
<wire x1="10.0076" y1="-4.5212" x2="10.0076" y2="4.5212" width="0.127" layer="21" style="shortdash"/>
</package>
</packages>
<symbols>
<symbol name="M2X05">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.2225" y2="5.715" width="0.254" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.2225" y2="4.445" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="1.27" y2="5.08" width="0.1524" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.2225" y2="4.445" width="0.254" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.2225" y2="5.715" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.2225" y2="3.175" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.2225" y2="1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.2225" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.2225" y2="3.175" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.2225" y2="0.635" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.2225" y2="-0.635" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="2.2225" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.2225" y2="0.635" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.2225" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.2225" y2="-3.175" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.2225" y2="-3.175" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.2225" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.2225" y2="-4.445" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.2225" y2="-5.715" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="1.27" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.2225" y2="-5.715" width="0.254" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.2225" y2="-4.445" width="0.254" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="9" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M-2X05-" prefix="P">
<description>&lt;b&gt;MALE HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="M2X05" x="0" y="0"/>
</gates>
<devices>
<device name="DIL-100" package="DIL-100-2X05">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DIL-100_M" package="DIL-100-2X05_M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DIL-2MM" package="DIL-2MM-2X05">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DIL-2MM_M" package="DIL-2MM-2X05_M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="87759" package="87759-10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="87759_M" package="87759-10_M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BORDER" package="DIL-100-2X05-BRD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="crystal">
<description>&lt;b&gt;Crystals and Crystal Resonators&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1100S">
<description>&lt;b&gt;CRYSTAL RESONATOR&lt;/b&gt;</description>
<wire x1="1.143" y1="4.318" x2="1.778" y2="4.318" width="0.1524" layer="51"/>
<wire x1="1.778" y1="4.318" x2="3.302" y2="4.318" width="0.1524" layer="51"/>
<wire x1="3.302" y1="4.318" x2="3.937" y2="4.318" width="0.1524" layer="51"/>
<wire x1="3.937" y1="3.683" x2="1.143" y2="3.683" width="0.0508" layer="51"/>
<wire x1="1.143" y1="-3.683" x2="3.937" y2="-3.683" width="0.0508" layer="51"/>
<wire x1="1.143" y1="-4.318" x2="1.778" y2="-4.318" width="0.1524" layer="51"/>
<wire x1="1.778" y1="-4.318" x2="3.302" y2="-4.318" width="0.1524" layer="51"/>
<wire x1="3.302" y1="-4.318" x2="3.937" y2="-4.318" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="-4.318" x2="-6.985" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="3.683" x2="-6.35" y2="-3.683" width="0.0508" layer="21"/>
<wire x1="-6.35" y1="-3.683" x2="-3.937" y2="-3.683" width="0.0508" layer="21"/>
<wire x1="-6.985" y1="-4.318" x2="-3.937" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-3.683" x2="-6.985" y2="-4.318" width="0.0508" layer="21"/>
<wire x1="-6.3" y1="-1.016" x2="-6.3" y2="1.016" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.143" y1="-3.683" x2="1.143" y2="-3.683" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="-4.318" x2="1.143" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-4.318" x2="-3.302" y2="-4.953" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="-4.953" x2="-1.778" y2="-4.953" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="-4.953" x2="-1.778" y2="-4.318" width="0.1524" layer="51"/>
<wire x1="-3.937" y1="-3.683" x2="-1.143" y2="-3.683" width="0.0508" layer="51"/>
<wire x1="-3.937" y1="-4.318" x2="-1.143" y2="-4.318" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="4.318" x2="-3.937" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="3.683" x2="-6.35" y2="3.683" width="0.0508" layer="21"/>
<wire x1="-6.35" y1="3.683" x2="-6.985" y2="4.318" width="0.0508" layer="21"/>
<wire x1="-3.302" y1="4.318" x2="-3.302" y2="4.953" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="4.318" x2="-1.778" y2="4.953" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="4.318" x2="1.143" y2="4.318" width="0.1524" layer="21"/>
<wire x1="1.143" y1="3.683" x2="-1.143" y2="3.683" width="0.0508" layer="21"/>
<wire x1="-3.937" y1="4.318" x2="-1.143" y2="4.318" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="3.683" x2="-3.937" y2="3.683" width="0.0508" layer="51"/>
<wire x1="-3.302" y1="4.953" x2="-1.778" y2="4.953" width="0.1524" layer="51"/>
<wire x1="6.985" y1="4.318" x2="6.985" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-3.683" x2="6.35" y2="3.683" width="0.0508" layer="21"/>
<wire x1="6.35" y1="-3.683" x2="6.985" y2="-4.318" width="0.0508" layer="21"/>
<wire x1="3.937" y1="-4.318" x2="6.985" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-3.683" x2="6.35" y2="-3.683" width="0.0508" layer="21"/>
<wire x1="1.778" y1="-4.318" x2="1.778" y2="-4.953" width="0.1524" layer="51"/>
<wire x1="1.778" y1="-4.953" x2="3.302" y2="-4.953" width="0.1524" layer="51"/>
<wire x1="3.302" y1="-4.953" x2="3.302" y2="-4.318" width="0.1524" layer="51"/>
<wire x1="6.35" y1="3.683" x2="6.985" y2="4.318" width="0.0508" layer="21"/>
<wire x1="3.937" y1="4.318" x2="6.985" y2="4.318" width="0.1524" layer="21"/>
<wire x1="1.778" y1="4.318" x2="1.778" y2="4.953" width="0.1524" layer="51"/>
<wire x1="3.302" y1="4.318" x2="3.302" y2="4.953" width="0.1524" layer="51"/>
<wire x1="6.35" y1="3.683" x2="3.937" y2="3.683" width="0.0508" layer="21"/>
<wire x1="1.778" y1="4.953" x2="3.302" y2="4.953" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="-0.254" x2="-3.048" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="-0.254" x2="-3.048" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="0.254" x2="-4.572" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="0.254" x2="-4.572" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.048" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.048" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="1.016" width="0.1524" layer="21"/>
<smd name="1" x="-2.54" y="-4.191" dx="2.1844" dy="3.048" layer="1"/>
<smd name="2" x="2.54" y="-4.191" dx="2.1844" dy="3.048" layer="1"/>
<smd name="3" x="2.54" y="4.191" dx="2.1844" dy="3.048" layer="1"/>
<smd name="4" x="-2.54" y="4.191" dx="2.1844" dy="3.048" layer="1"/>
<text x="-7.366" y="-4.318" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SG51">
<description>&lt;b&gt;CRYSTAL RESONATOR&lt;/b&gt;</description>
<wire x1="-9.906" y1="3.175" x2="-9.906" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="9.906" y1="-3.175" x2="9.906" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-5.4102" y1="1.27" x2="-5.4102" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-5.4102" y1="-1.27" x2="-4.7752" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-4.7752" y1="-1.27" x2="-4.7752" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-4.7752" y1="1.27" x2="-5.4102" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-4.1402" y1="1.27" x2="-4.1402" y2="0" width="0.3048" layer="21"/>
<wire x1="-6.0198" y1="1.27" x2="-6.0198" y2="0" width="0.3048" layer="21"/>
<wire x1="-4.1402" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.1402" y1="0" x2="-4.1402" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-6.0198" y1="0" x2="-6.985" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.0198" y1="0" x2="-6.0198" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="9.906" y1="-3.175" x2="8.255" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-3.175" x2="6.985" y2="-3.175" width="0.1524" layer="51"/>
<wire x1="8.255" y1="3.175" x2="6.985" y2="3.175" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="-3.175" x2="-8.255" y2="-3.175" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="3.175" x2="-8.255" y2="3.175" width="0.1524" layer="51"/>
<wire x1="9.906" y1="3.175" x2="8.255" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-9.906" y1="-3.175" x2="-8.255" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.175" x2="6.985" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-9.906" y1="3.175" x2="-8.255" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.175" x2="6.985" y2="3.175" width="0.1524" layer="21"/>
<circle x="-7.62" y="-1.651" radius="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="-3.81" drill="0.8128"/>
<pad name="7" x="7.62" y="-3.81" drill="0.8128"/>
<pad name="8" x="7.62" y="3.81" drill="0.8128"/>
<pad name="14" x="-7.62" y="3.81" drill="0.8128"/>
<text x="-10.6934" y="-2.54" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-8.636" y="1.1176" size="1.27" layer="21" ratio="10">14</text>
<text x="7.2898" y="-2.3876" size="1.27" layer="21" ratio="10">7</text>
<text x="7.2898" y="0.9398" size="1.27" layer="21" ratio="10">8</text>
<text x="-2.54" y="-2.3622" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-3.937" x2="8.128" y2="-3.175" layer="51"/>
<rectangle x1="7.112" y1="3.175" x2="8.128" y2="3.937" layer="51"/>
<rectangle x1="-8.128" y1="-3.937" x2="-7.112" y2="-3.175" layer="51"/>
<rectangle x1="-8.128" y1="3.175" x2="-7.112" y2="3.937" layer="51"/>
</package>
<package name="SG-636">
<wire x1="-5.25" y1="-2.5" x2="5.25" y2="-2.5" width="0.127" layer="21"/>
<wire x1="5.25" y1="-2.5" x2="5.25" y2="2.5" width="0.127" layer="21"/>
<wire x1="5.25" y1="2.5" x2="-5.25" y2="2.5" width="0.127" layer="21"/>
<smd name="P$1" x="-2.54" y="-2.3" dx="2.1" dy="1.3" layer="1" rot="R90"/>
<smd name="P$2" x="2.54" y="-2.3" dx="2.1" dy="1.3" layer="1" rot="R90"/>
<smd name="P$3" x="2.54" y="2.3" dx="2.1" dy="1.3" layer="1" rot="R90"/>
<smd name="P$4" x="-2.54" y="2.3" dx="2.1" dy="1.3" layer="1" rot="R90"/>
<wire x1="-5.25" y1="2.5" x2="-5.25" y2="1" width="0.127" layer="21"/>
<wire x1="-5.25" y1="1" x2="-5.25" y2="-1" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-1" x2="-5.25" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-5.25" y1="1" x2="-5.25" y2="-1" width="0.127" layer="21" curve="-180"/>
</package>
</packages>
<symbols>
<symbol name="QG2">
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="0" width="0.4064" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="2.54" y2="2.54" width="0.4064" layer="94"/>
<wire x1="2.54" y1="2.54" x2="6.35" y2="0" width="0.4064" layer="94"/>
<wire x1="6.35" y1="0" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="-3.175" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-3.175" x2="-1.27" y2="3.175" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="3.175" x2="-1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="3.175" x2="-1.27" y2="3.175" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-3.175" x2="-1.27" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="7.62" y1="5.08" x2="2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.826" y1="-0.381" x2="-4.826" y2="0.381" width="0.254" layer="94"/>
<wire x1="-4.826" y1="0.381" x2="-2.794" y2="0.381" width="0.254" layer="94"/>
<wire x1="-2.794" y1="0.381" x2="-2.794" y2="-0.381" width="0.254" layer="94"/>
<wire x1="-4.826" y1="-0.381" x2="-2.794" y2="-0.381" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.016" x2="-3.81" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.016" x2="-2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.016" x2="-3.81" y2="3.175" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-3.175" x2="-3.81" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="1.016" x2="-2.54" y2="1.016" width="0.254" layer="94"/>
<text x="-7.62" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-6.985" y="4.318" size="1.524" layer="95">VDD</text>
<text x="-6.985" y="-5.842" size="1.524" layer="95">VSS</text>
<text x="2.54" y="-5.842" size="1.524" layer="95">OUT</text>
<text x="2.54" y="5.588" size="1.524" layer="95">CON</text>
<pin name="VSS" x="-12.7" y="-5.08" visible="pad" length="middle" direction="pwr"/>
<pin name="VDD" x="-12.7" y="5.08" visible="pad" length="middle" direction="pwr"/>
<pin name="OUT" x="12.7" y="0" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="CON" x="12.7" y="5.08" visible="pad" length="middle" direction="in" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRYSTAL_RES-" prefix="QG" uservalue="yes">
<description>&lt;b&gt;CRYSTAL RESONATOR&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="QG2" x="0" y="0"/>
</gates>
<devices>
<device name="1100S" package="1100S">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="3"/>
<connect gate="A" pin="VDD" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="SG51" package="SG51">
<connects>
<connect gate="A" pin="CON" pad="1"/>
<connect gate="A" pin="OUT" pad="8"/>
<connect gate="A" pin="VDD" pad="14"/>
<connect gate="A" pin="VSS" pad="7"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="SG636" package="SG-636">
<connects>
<connect gate="A" pin="CON" pad="P$1"/>
<connect gate="A" pin="OUT" pad="P$3"/>
<connect gate="A" pin="VDD" pad="P$4"/>
<connect gate="A" pin="VSS" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="DB15">
<wire x1="-8.128" y1="12.079" x2="-7.62" y2="12.587" width="0.2032" layer="51" curve="-90"/>
<wire x1="7.62" y1="12.587" x2="8.128" y2="12.079" width="0.2032" layer="51" curve="-90"/>
<wire x1="-15.4" y1="-9.65" x2="-15.4" y2="2.38" width="0.2032" layer="21"/>
<wire x1="-15.4" y1="2.38" x2="-15.4" y2="6.35" width="0.2032" layer="51"/>
<wire x1="15.4" y1="-9.65" x2="15.4" y2="2.38" width="0.2032" layer="21"/>
<wire x1="15.4" y1="2.38" x2="15.4" y2="6.35" width="0.2032" layer="51"/>
<wire x1="-8.128" y1="6.999" x2="-8.128" y2="12.079" width="0.2032" layer="51"/>
<wire x1="-8.763" y1="6.364" x2="-8.128" y2="6.999" width="0.2032" layer="51" curve="90"/>
<wire x1="-15.4" y1="6.35" x2="15.4" y2="6.35" width="0.2032" layer="51"/>
<wire x1="8.128" y1="6.999" x2="8.128" y2="12.079" width="0.2032" layer="51"/>
<wire x1="8.128" y1="6.999" x2="8.763" y2="6.364" width="0.2032" layer="51" curve="90"/>
<wire x1="-7.62" y1="12.587" x2="7.62" y2="12.587" width="0.2032" layer="51"/>
<wire x1="-15.4" y1="-9.65" x2="15.4" y2="-9.65" width="0.2032" layer="21"/>
<pad name="7" x="-3.17" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="8" x="-0.88" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="9" x="1.41" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="10" x="3.7" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="11" x="-4.32" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="12" x="-2.03" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="13" x="0.26" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="14" x="2.55" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="G1" x="-12.5" y="-5.08" drill="3.302" diameter="5.08"/>
<pad name="G2" x="12.5" y="-5.08" drill="3.302" diameter="5.08"/>
<text x="-0.795" y="-1.19" size="0.4064" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.69" y="-0.395" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<pad name="6" x="-5.46" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="15" x="4.84" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="1" x="-4.32" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="2" x="-2.03" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="3" x="0.26" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.55" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="5" x="4.84" y="-7.62" drill="1.016" diameter="1.8796"/>
</package>
<package name="1X02">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X2">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-SMD">
<description>2mm SMD side-entry connector. tDocu layer indicates the actual physical plastic housing. +/- indicate SparkFun standard batteries and wiring.</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="0" size="0.4064" layer="27">&gt;Value</text>
<text x="1.905" y="-6.35" size="1.778" layer="21" font="vector">+</text>
<text x="-3.175" y="-6.35" size="1.778" layer="21" font="vector">-</text>
</package>
<package name="1X02_BIG">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
</package>
<package name="JST-2-SMD-VERT">
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="2.54" y="-5.08" size="1.27" layer="25">&gt;Name</text>
<text x="2.24" y="3.48" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="R_SW_TH">
<wire x1="-1.651" y1="19.2532" x2="-1.651" y2="-1.3716" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-1.3716" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="19.2532" x2="13.589" y2="19.2532" width="0.2032" layer="21"/>
<wire x1="13.589" y1="19.2532" x2="13.589" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="13.589" y1="-2.2352" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.6002"/>
<pad name="P$2" x="0" y="16.9926" drill="1.6002"/>
<pad name="P$3" x="12.0904" y="15.494" drill="1.6002"/>
<pad name="P$4" x="12.0904" y="8.4582" drill="1.6002"/>
</package>
<package name="SCREWTERMINAL-5MM-2">
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LOCK">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="MOLEX-1X2_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="1X02_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS">
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
</package>
<package name="1X02_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-2-PTH">
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<wire x1="-2.95" y1="-1.6" x2="-2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="6" x2="2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="6" x2="2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-1.6" x2="-2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-1.6" x2="2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.6" x2="-2.3" y2="0" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.6" x2="2.3" y2="0" width="0.2032" layer="21"/>
</package>
<package name="1X02_XTRA_BIG">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
</package>
<package name="1X02_PP_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS">
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT">
<description>&lt;H3&gt;JST-2-PTH-KIT&lt;/h3&gt;
2-Pin JST, through-hole connector&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2">
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM">
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.127" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.127" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="JST-2-PTH-VERT">
<wire x1="-2.95" y1="-2.25" x2="-2.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="2.25" x2="2.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="2.95" y1="2.25" x2="2.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-2.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-1.984" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="2.016" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="0.616" y="0.75" size="1.27" layer="51">+</text>
<text x="-1.384" y="0.75" size="1.27" layer="51">-</text>
</package>
</packages>
<symbols>
<symbol name="DB15">
<wire x1="-1.651" y1="3.429" x2="-1.651" y2="1.651" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-0.508" y1="4.191" x2="-0.508" y2="5.969" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-1.651" y1="8.509" x2="-1.651" y2="6.731" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-0.508" y1="-5.969" x2="-0.508" y2="-4.191" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-1.651" y1="-1.651" x2="-1.651" y2="-3.429" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-0.508" y1="-0.889" x2="-0.508" y2="0.889" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-1.651" y1="-6.731" x2="-1.651" y2="-8.509" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-0.508" y1="-11.049" x2="-0.508" y2="-9.271" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-1.651" y1="-11.811" x2="-1.651" y2="-13.589" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-4.064" y1="12.0112" x2="-2.5226" y2="13.252" width="0.4064" layer="94" curve="-102.324066" cap="flat"/>
<wire x1="-2.5226" y1="13.2518" x2="0" y2="12.7" width="0.4064" layer="94"/>
<wire x1="0" y1="12.7" x2="3.0654" y2="12.0294" width="0.4064" layer="94"/>
<wire x1="3.0654" y1="12.0295" x2="4.0642" y2="10.7888" width="0.4064" layer="94" curve="-77.655139" cap="flat"/>
<wire x1="4.064" y1="-13.3288" x2="4.064" y2="10.7888" width="0.4064" layer="94"/>
<wire x1="3.0654" y1="-14.5694" x2="4.064" y2="-13.3288" width="0.4064" layer="94" curve="77.657889"/>
<wire x1="-4.064" y1="-14.5512" x2="-4.064" y2="12.0112" width="0.4064" layer="94"/>
<wire x1="-2.5226" y1="-15.7918" x2="0" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="0" y1="-15.24" x2="3.0654" y2="-14.5694" width="0.4064" layer="94"/>
<wire x1="-4.064" y1="-14.5512" x2="-2.5226" y2="-15.7919" width="0.4064" layer="94" curve="102.337599" cap="flat"/>
<text x="-3.81" y="-18.415" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="13.97" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas"/>
<pin name="7" x="7.874" y="5.08" visible="pad" direction="pas" rot="R180"/>
<pin name="2" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas"/>
<pin name="8" x="7.874" y="0" visible="pad" direction="pas" rot="R180"/>
<pin name="3" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas"/>
<pin name="9" x="7.874" y="-5.08" visible="pad" direction="pas" rot="R180"/>
<pin name="4" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas"/>
<pin name="10" x="7.874" y="-10.16" visible="pad" direction="pas" rot="R180"/>
<pin name="5" x="-7.62" y="-12.7" visible="pad" length="middle" direction="pas"/>
<wire x1="-0.508" y1="9.271" x2="-0.508" y2="11.049" width="0.254" layer="94" curve="180" cap="flat"/>
<pin name="6" x="7.874" y="10.16" visible="pad" direction="pas" rot="R180"/>
<wire x1="1.778" y1="1.651" x2="1.778" y2="3.429" width="0.254" layer="94" curve="180" cap="flat"/>
<pin name="12" x="7.874" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="1.778" y1="-3.429" x2="1.778" y2="-1.651" width="0.254" layer="94" curve="180" cap="flat"/>
<pin name="13" x="7.874" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="1.778" y1="-8.509" x2="1.778" y2="-6.731" width="0.254" layer="94" curve="180" cap="flat"/>
<pin name="14" x="7.874" y="-7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="1.778" y1="-13.589" x2="1.778" y2="-11.811" width="0.254" layer="94" curve="180" cap="flat"/>
<pin name="15" x="7.874" y="-12.7" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="1.778" y1="6.731" x2="1.778" y2="8.509" width="0.254" layer="94" curve="180" cap="flat"/>
<pin name="11" x="7.874" y="7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="M02">
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DB15">
<description>DB15 / VGA right angle through-hole female connector.</description>
<gates>
<gate name="G$1" symbol="DB15" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DB15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M02" prefix="JP" uservalue="yes">
<description>Standard 2-pin 0.1" header. Use with &lt;br&gt;
- straight break away headers ( PRT-00116)&lt;br&gt;
- right angle break away headers (PRT-00553)&lt;br&gt;
- swiss pins (PRT-00743)&lt;br&gt;
- machine pins (PRT-00117)&lt;br&gt;
- female headers (PRT-00115)&lt;br&gt;&lt;br&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08432</description>
<gates>
<gate name="G$1" symbol="M02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11443"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROCKER" package="R_SW_TH">
<connects>
<connect gate="G$1" pin="1" pad="P$3"/>
<connect gate="G$1" pin="2" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="STOREFRONT_ID" value="PRT-08432" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09863" constant="no"/>
<attribute name="SKU" value="PRT-09914" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-2-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Analog Devices_By_element14_Batch_1">
<description>Developed by element14 :&lt;br&gt;
element14 CAD Library consolidation.ulp
at 31/07/2012 09:37:13</description>
<packages>
<package name="QFN50P700X700X100-49N">
<smd name="1" x="-3.4544" y="2.7432" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="2" x="-3.4544" y="2.2606" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="3" x="-3.4544" y="1.7526" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="4" x="-3.4544" y="1.2446" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="5" x="-3.4544" y="0.762" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="6" x="-3.4544" y="0.254" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="7" x="-3.4544" y="-0.254" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="8" x="-3.4544" y="-0.762" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="9" x="-3.4544" y="-1.2446" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="10" x="-3.4544" y="-1.7526" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="11" x="-3.4544" y="-2.2606" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="12" x="-3.4544" y="-2.7432" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="13" x="-2.7432" y="-3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="14" x="-2.2606" y="-3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="15" x="-1.7526" y="-3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="16" x="-1.2446" y="-3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="17" x="-0.762" y="-3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="18" x="-0.254" y="-3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="19" x="0.254" y="-3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="20" x="0.762" y="-3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="21" x="1.2446" y="-3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="22" x="1.7526" y="-3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="23" x="2.2606" y="-3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="24" x="2.7432" y="-3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="25" x="3.4544" y="-2.7432" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="26" x="3.4544" y="-2.2606" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="27" x="3.4544" y="-1.7526" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="28" x="3.4544" y="-1.2446" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="29" x="3.4544" y="-0.762" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="30" x="3.4544" y="-0.254" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="31" x="3.4544" y="0.254" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="32" x="3.4544" y="0.762" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="33" x="3.4544" y="1.2446" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="34" x="3.4544" y="1.7526" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="35" x="3.4544" y="2.2606" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="36" x="3.4544" y="2.7432" dx="0.3048" dy="0.8128" layer="1" rot="R270"/>
<smd name="37" x="2.7432" y="3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="38" x="2.2606" y="3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="39" x="1.7526" y="3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="40" x="1.2446" y="3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="41" x="0.762" y="3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="42" x="0.254" y="3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="43" x="-0.254" y="3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="44" x="-0.762" y="3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="45" x="-1.2446" y="3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="46" x="-1.7526" y="3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="47" x="-2.2606" y="3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="48" x="-2.7432" y="3.4544" dx="0.3048" dy="0.8128" layer="1" rot="R180"/>
<smd name="49" x="0" y="0" dx="5.5118" dy="5.5118" layer="1"/>
<wire x1="0.762" y1="-4.1656" x2="0.762" y2="-5.1562" width="0.1524" layer="21"/>
<wire x1="1.2446" y1="5.1562" x2="1.2446" y2="4.1656" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="-1.7526" x2="-5.1816" y2="-1.7526" width="0.1524" layer="21"/>
<wire x1="5.1816" y1="-0.2286" x2="4.191" y2="-0.2286" width="0.1524" layer="21"/>
<wire x1="3.556" y1="3.2258" x2="3.556" y2="3.556" width="0.1524" layer="21"/>
<wire x1="3.2258" y1="-3.556" x2="3.556" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-3.2258" y1="3.556" x2="-3.556" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-3.556" x2="-3.2258" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="3.556" y1="-3.556" x2="3.556" y2="-3.2258" width="0.1524" layer="21"/>
<wire x1="3.556" y1="3.556" x2="3.2258" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="3.556" x2="-3.556" y2="3.2258" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-3.2258" x2="-3.556" y2="-3.556" width="0.1524" layer="21"/>
<text x="-5.0546" y="2.7432" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="-3.556" y1="2.286" x2="-2.286" y2="3.556" width="0" layer="51"/>
<wire x1="2.8956" y1="3.556" x2="2.5908" y2="3.556" width="0" layer="51"/>
<wire x1="2.3876" y1="3.556" x2="2.1082" y2="3.556" width="0" layer="51"/>
<wire x1="1.905" y1="3.556" x2="1.6002" y2="3.556" width="0" layer="51"/>
<wire x1="1.397" y1="3.556" x2="1.0922" y2="3.556" width="0" layer="51"/>
<wire x1="0.889" y1="3.556" x2="0.6096" y2="3.556" width="0" layer="51"/>
<wire x1="0.4064" y1="3.556" x2="0.1016" y2="3.556" width="0" layer="51"/>
<wire x1="-0.1016" y1="3.556" x2="-0.4064" y2="3.556" width="0" layer="51"/>
<wire x1="-0.6096" y1="3.556" x2="-0.889" y2="3.556" width="0" layer="51"/>
<wire x1="-1.0922" y1="3.556" x2="-1.397" y2="3.556" width="0" layer="51"/>
<wire x1="-1.6002" y1="3.556" x2="-1.905" y2="3.556" width="0" layer="51"/>
<wire x1="-2.1082" y1="3.556" x2="-2.3876" y2="3.556" width="0" layer="51"/>
<wire x1="-2.5908" y1="3.556" x2="-2.8956" y2="3.556" width="0" layer="51"/>
<wire x1="-3.556" y1="2.8956" x2="-3.556" y2="2.5908" width="0" layer="51"/>
<wire x1="-3.556" y1="2.3876" x2="-3.556" y2="2.1082" width="0" layer="51"/>
<wire x1="-3.556" y1="1.905" x2="-3.556" y2="1.6002" width="0" layer="51"/>
<wire x1="-3.556" y1="1.397" x2="-3.556" y2="1.0922" width="0" layer="51"/>
<wire x1="-3.556" y1="0.889" x2="-3.556" y2="0.6096" width="0" layer="51"/>
<wire x1="-3.556" y1="0.4064" x2="-3.556" y2="0.1016" width="0" layer="51"/>
<wire x1="-3.556" y1="-0.1016" x2="-3.556" y2="-0.4064" width="0" layer="51"/>
<wire x1="-3.556" y1="-0.6096" x2="-3.556" y2="-0.889" width="0" layer="51"/>
<wire x1="-3.556" y1="-1.0922" x2="-3.556" y2="-1.397" width="0" layer="51"/>
<wire x1="-3.556" y1="-1.6002" x2="-3.556" y2="-1.905" width="0" layer="51"/>
<wire x1="-3.556" y1="-2.1082" x2="-3.556" y2="-2.3876" width="0" layer="51"/>
<wire x1="-3.556" y1="-2.5908" x2="-3.556" y2="-2.8956" width="0" layer="51"/>
<wire x1="-2.8956" y1="-3.556" x2="-2.5908" y2="-3.556" width="0" layer="51"/>
<wire x1="-2.3876" y1="-3.556" x2="-2.1082" y2="-3.556" width="0" layer="51"/>
<wire x1="-1.905" y1="-3.556" x2="-1.6002" y2="-3.556" width="0" layer="51"/>
<wire x1="-1.397" y1="-3.556" x2="-1.0922" y2="-3.556" width="0" layer="51"/>
<wire x1="-0.889" y1="-3.556" x2="-0.6096" y2="-3.556" width="0" layer="51"/>
<wire x1="-0.4064" y1="-3.556" x2="-0.1016" y2="-3.556" width="0" layer="51"/>
<wire x1="0.1016" y1="-3.556" x2="0.4064" y2="-3.556" width="0" layer="51"/>
<wire x1="0.6096" y1="-3.556" x2="0.889" y2="-3.556" width="0" layer="51"/>
<wire x1="1.0922" y1="-3.556" x2="1.397" y2="-3.556" width="0" layer="51"/>
<wire x1="1.6002" y1="-3.556" x2="1.905" y2="-3.556" width="0" layer="51"/>
<wire x1="2.1082" y1="-3.556" x2="2.3876" y2="-3.556" width="0" layer="51"/>
<wire x1="2.5908" y1="-3.556" x2="2.8956" y2="-3.556" width="0" layer="51"/>
<wire x1="3.556" y1="-2.8956" x2="3.556" y2="-2.5908" width="0" layer="51"/>
<wire x1="3.556" y1="-2.3876" x2="3.556" y2="-2.1082" width="0" layer="51"/>
<wire x1="3.556" y1="-1.905" x2="3.556" y2="-1.6002" width="0" layer="51"/>
<wire x1="3.556" y1="-1.397" x2="3.556" y2="-1.0922" width="0" layer="51"/>
<wire x1="3.556" y1="-0.889" x2="3.556" y2="-0.6096" width="0" layer="51"/>
<wire x1="3.556" y1="-0.4064" x2="3.556" y2="-0.1016" width="0" layer="51"/>
<wire x1="3.556" y1="0.1016" x2="3.556" y2="0.4064" width="0" layer="51"/>
<wire x1="3.556" y1="0.6096" x2="3.556" y2="0.889" width="0" layer="51"/>
<wire x1="3.556" y1="1.0922" x2="3.556" y2="1.397" width="0" layer="51"/>
<wire x1="3.556" y1="1.6002" x2="3.556" y2="1.905" width="0" layer="51"/>
<wire x1="3.556" y1="2.1082" x2="3.556" y2="2.3876" width="0" layer="51"/>
<wire x1="3.556" y1="2.5908" x2="3.556" y2="2.8956" width="0" layer="51"/>
<wire x1="-3.556" y1="-3.556" x2="3.556" y2="-3.556" width="0" layer="51"/>
<wire x1="3.556" y1="-3.556" x2="3.556" y2="3.556" width="0" layer="51"/>
<wire x1="3.556" y1="3.556" x2="-3.556" y2="3.556" width="0" layer="51"/>
<wire x1="-3.556" y1="3.556" x2="-3.556" y2="-3.556" width="0" layer="51"/>
<text x="-5.0546" y="2.7432" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="6.985" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-8.255" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
<package name="QFP50P900X900X120-49N">
<smd name="1" x="-4.1148" y="2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="2" x="-4.1148" y="2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="3" x="-4.1148" y="1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="4" x="-4.1148" y="1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="5" x="-4.1148" y="0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="6" x="-4.1148" y="0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="7" x="-4.1148" y="-0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="8" x="-4.1148" y="-0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="9" x="-4.1148" y="-1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="10" x="-4.1148" y="-1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="11" x="-4.1148" y="-2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="12" x="-4.1148" y="-2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="13" x="-2.7432" y="-4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="14" x="-2.2606" y="-4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="15" x="-1.7526" y="-4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="16" x="-1.2446" y="-4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="17" x="-0.762" y="-4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="18" x="-0.254" y="-4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="19" x="0.254" y="-4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="20" x="0.762" y="-4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="21" x="1.2446" y="-4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="22" x="1.7526" y="-4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="23" x="2.2606" y="-4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="24" x="2.7432" y="-4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="25" x="4.1148" y="-2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="26" x="4.1148" y="-2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="27" x="4.1148" y="-1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="28" x="4.1148" y="-1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="29" x="4.1148" y="-0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="30" x="4.1148" y="-0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="31" x="4.1148" y="0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="32" x="4.1148" y="0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="33" x="4.1148" y="1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="34" x="4.1148" y="1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="35" x="4.1148" y="2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="36" x="4.1148" y="2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="37" x="2.7432" y="4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="38" x="2.2606" y="4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="39" x="1.7526" y="4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="40" x="1.2446" y="4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="41" x="0.762" y="4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="42" x="0.254" y="4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="43" x="-0.254" y="4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="44" x="-0.762" y="4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="45" x="-1.2446" y="4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="46" x="-1.7526" y="4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="47" x="-2.2606" y="4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="48" x="-2.7432" y="4.1148" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="49" x="0" y="0" dx="3.5052" dy="3.5052" layer="1"/>
<wire x1="1.2446" y1="6.223" x2="1.2446" y2="5.207" width="0.1524" layer="21"/>
<wire x1="6.1976" y1="-0.2286" x2="5.1816" y2="-0.2286" width="0.1524" layer="21"/>
<wire x1="-5.1816" y1="-1.7526" x2="-6.1976" y2="-1.7526" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-6.1976" x2="0.762" y2="-5.1816" width="0.1524" layer="21"/>
<wire x1="-3.2258" y1="3.5052" x2="-3.5052" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="3.5052" y1="3.2258" x2="3.5052" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="3.2258" y1="-3.5052" x2="3.5052" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-3.0734" y1="2.667" x2="-2.667" y2="3.0734" width="0.1524" layer="21"/>
<wire x1="-3.5052" y1="-3.5052" x2="-3.2258" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="3.5052" y1="-3.5052" x2="3.5052" y2="-3.2258" width="0.1524" layer="21"/>
<wire x1="3.5052" y1="3.5052" x2="3.2258" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-3.5052" y1="3.5052" x2="-3.5052" y2="3.2258" width="0.1524" layer="21"/>
<wire x1="-3.5052" y1="-3.2258" x2="-3.5052" y2="-3.5052" width="0.1524" layer="21"/>
<text x="-6.0706" y="2.7432" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="2.6162" y1="3.5052" x2="2.8956" y2="3.5052" width="0" layer="51"/>
<wire x1="2.8956" y1="3.5052" x2="2.8956" y2="4.4958" width="0" layer="51"/>
<wire x1="2.8956" y1="4.4958" x2="2.6162" y2="4.4958" width="0" layer="51"/>
<wire x1="2.6162" y1="4.4958" x2="2.6162" y2="3.5052" width="0" layer="51"/>
<wire x1="2.1082" y1="3.5052" x2="2.3876" y2="3.5052" width="0" layer="51"/>
<wire x1="2.3876" y1="3.5052" x2="2.3876" y2="4.4958" width="0" layer="51"/>
<wire x1="2.3876" y1="4.4958" x2="2.1082" y2="4.4958" width="0" layer="51"/>
<wire x1="2.1082" y1="4.4958" x2="2.1082" y2="3.5052" width="0" layer="51"/>
<wire x1="1.6256" y1="3.5052" x2="1.8796" y2="3.5052" width="0" layer="51"/>
<wire x1="1.8796" y1="3.5052" x2="1.8796" y2="4.4958" width="0" layer="51"/>
<wire x1="1.8796" y1="4.4958" x2="1.6256" y2="4.4958" width="0" layer="51"/>
<wire x1="1.6256" y1="4.4958" x2="1.6256" y2="3.5052" width="0" layer="51"/>
<wire x1="1.1176" y1="3.5052" x2="1.397" y2="3.5052" width="0" layer="51"/>
<wire x1="1.397" y1="3.5052" x2="1.397" y2="4.4958" width="0" layer="51"/>
<wire x1="1.397" y1="4.4958" x2="1.1176" y2="4.4958" width="0" layer="51"/>
<wire x1="1.1176" y1="4.4958" x2="1.1176" y2="3.5052" width="0" layer="51"/>
<wire x1="0.6096" y1="3.5052" x2="0.889" y2="3.5052" width="0" layer="51"/>
<wire x1="0.889" y1="3.5052" x2="0.889" y2="4.4958" width="0" layer="51"/>
<wire x1="0.889" y1="4.4958" x2="0.6096" y2="4.4958" width="0" layer="51"/>
<wire x1="0.6096" y1="4.4958" x2="0.6096" y2="3.5052" width="0" layer="51"/>
<wire x1="0.127" y1="3.5052" x2="0.381" y2="3.5052" width="0" layer="51"/>
<wire x1="0.381" y1="3.5052" x2="0.381" y2="4.4958" width="0" layer="51"/>
<wire x1="0.381" y1="4.4958" x2="0.127" y2="4.4958" width="0" layer="51"/>
<wire x1="0.127" y1="4.4958" x2="0.127" y2="3.5052" width="0" layer="51"/>
<wire x1="-0.381" y1="3.5052" x2="-0.127" y2="3.5052" width="0" layer="51"/>
<wire x1="-0.127" y1="3.5052" x2="-0.127" y2="4.4958" width="0" layer="51"/>
<wire x1="-0.127" y1="4.4958" x2="-0.381" y2="4.4958" width="0" layer="51"/>
<wire x1="-0.381" y1="4.4958" x2="-0.381" y2="3.5052" width="0" layer="51"/>
<wire x1="-0.889" y1="3.5052" x2="-0.6096" y2="3.5052" width="0" layer="51"/>
<wire x1="-0.6096" y1="3.5052" x2="-0.6096" y2="4.4958" width="0" layer="51"/>
<wire x1="-0.6096" y1="4.4958" x2="-0.889" y2="4.4958" width="0" layer="51"/>
<wire x1="-0.889" y1="4.4958" x2="-0.889" y2="3.5052" width="0" layer="51"/>
<wire x1="-1.397" y1="3.5052" x2="-1.1176" y2="3.5052" width="0" layer="51"/>
<wire x1="-1.1176" y1="3.5052" x2="-1.1176" y2="4.4958" width="0" layer="51"/>
<wire x1="-1.1176" y1="4.4958" x2="-1.397" y2="4.4958" width="0" layer="51"/>
<wire x1="-1.397" y1="4.4958" x2="-1.397" y2="3.5052" width="0" layer="51"/>
<wire x1="-1.8796" y1="3.5052" x2="-1.6256" y2="3.5052" width="0" layer="51"/>
<wire x1="-1.6256" y1="3.5052" x2="-1.6256" y2="4.4958" width="0" layer="51"/>
<wire x1="-1.6256" y1="4.4958" x2="-1.8796" y2="4.4958" width="0" layer="51"/>
<wire x1="-1.8796" y1="4.4958" x2="-1.8796" y2="3.5052" width="0" layer="51"/>
<wire x1="-2.3876" y1="3.5052" x2="-2.2352" y2="3.5052" width="0" layer="51"/>
<wire x1="-2.2352" y1="3.5052" x2="-2.1082" y2="3.5052" width="0" layer="51"/>
<wire x1="-2.1082" y1="3.5052" x2="-2.1082" y2="4.4958" width="0" layer="51"/>
<wire x1="-2.1082" y1="4.4958" x2="-2.3876" y2="4.4958" width="0" layer="51"/>
<wire x1="-2.3876" y1="4.4958" x2="-2.3876" y2="3.5052" width="0" layer="51"/>
<wire x1="-2.8956" y1="3.5052" x2="-2.6162" y2="3.5052" width="0" layer="51"/>
<wire x1="-2.6162" y1="3.5052" x2="-2.6162" y2="4.4958" width="0" layer="51"/>
<wire x1="-2.6162" y1="4.4958" x2="-2.8956" y2="4.4958" width="0" layer="51"/>
<wire x1="-2.8956" y1="4.4958" x2="-2.8956" y2="3.5052" width="0" layer="51"/>
<wire x1="-3.5052" y1="2.6162" x2="-3.5052" y2="2.8956" width="0" layer="51"/>
<wire x1="-3.5052" y1="2.8956" x2="-4.4958" y2="2.8956" width="0" layer="51"/>
<wire x1="-4.4958" y1="2.8956" x2="-4.4958" y2="2.6162" width="0" layer="51"/>
<wire x1="-4.4958" y1="2.6162" x2="-3.5052" y2="2.6162" width="0" layer="51"/>
<wire x1="-3.5052" y1="2.1082" x2="-3.5052" y2="2.2352" width="0" layer="51"/>
<wire x1="-3.5052" y1="2.2352" x2="-3.5052" y2="2.3876" width="0" layer="51"/>
<wire x1="-3.5052" y1="2.3876" x2="-4.4958" y2="2.3876" width="0" layer="51"/>
<wire x1="-4.4958" y1="2.3876" x2="-4.4958" y2="2.1082" width="0" layer="51"/>
<wire x1="-4.4958" y1="2.1082" x2="-3.5052" y2="2.1082" width="0" layer="51"/>
<wire x1="-3.5052" y1="1.6256" x2="-3.5052" y2="1.8796" width="0" layer="51"/>
<wire x1="-3.5052" y1="1.8796" x2="-4.4958" y2="1.8796" width="0" layer="51"/>
<wire x1="-4.4958" y1="1.8796" x2="-4.4958" y2="1.6256" width="0" layer="51"/>
<wire x1="-4.4958" y1="1.6256" x2="-3.5052" y2="1.6256" width="0" layer="51"/>
<wire x1="-3.5052" y1="1.1176" x2="-3.5052" y2="1.397" width="0" layer="51"/>
<wire x1="-3.5052" y1="1.397" x2="-4.4958" y2="1.397" width="0" layer="51"/>
<wire x1="-4.4958" y1="1.397" x2="-4.4958" y2="1.1176" width="0" layer="51"/>
<wire x1="-4.4958" y1="1.1176" x2="-3.5052" y2="1.1176" width="0" layer="51"/>
<wire x1="-3.5052" y1="0.6096" x2="-3.5052" y2="0.889" width="0" layer="51"/>
<wire x1="-3.5052" y1="0.889" x2="-4.4958" y2="0.889" width="0" layer="51"/>
<wire x1="-4.4958" y1="0.889" x2="-4.4958" y2="0.6096" width="0" layer="51"/>
<wire x1="-4.4958" y1="0.6096" x2="-3.5052" y2="0.6096" width="0" layer="51"/>
<wire x1="-3.5052" y1="0.127" x2="-3.5052" y2="0.381" width="0" layer="51"/>
<wire x1="-3.5052" y1="0.381" x2="-4.4958" y2="0.381" width="0" layer="51"/>
<wire x1="-4.4958" y1="0.381" x2="-4.4958" y2="0.127" width="0" layer="51"/>
<wire x1="-4.4958" y1="0.127" x2="-3.5052" y2="0.127" width="0" layer="51"/>
<wire x1="-3.5052" y1="-0.381" x2="-3.5052" y2="-0.127" width="0" layer="51"/>
<wire x1="-3.5052" y1="-0.127" x2="-4.4958" y2="-0.127" width="0" layer="51"/>
<wire x1="-4.4958" y1="-0.127" x2="-4.4958" y2="-0.381" width="0" layer="51"/>
<wire x1="-4.4958" y1="-0.381" x2="-3.5052" y2="-0.381" width="0" layer="51"/>
<wire x1="-3.5052" y1="-0.889" x2="-3.5052" y2="-0.6096" width="0" layer="51"/>
<wire x1="-3.5052" y1="-0.6096" x2="-4.4958" y2="-0.6096" width="0" layer="51"/>
<wire x1="-4.4958" y1="-0.6096" x2="-4.4958" y2="-0.889" width="0" layer="51"/>
<wire x1="-4.4958" y1="-0.889" x2="-3.5052" y2="-0.889" width="0" layer="51"/>
<wire x1="-3.5052" y1="-1.397" x2="-3.5052" y2="-1.1176" width="0" layer="51"/>
<wire x1="-3.5052" y1="-1.1176" x2="-4.4958" y2="-1.1176" width="0" layer="51"/>
<wire x1="-4.4958" y1="-1.1176" x2="-4.4958" y2="-1.397" width="0" layer="51"/>
<wire x1="-4.4958" y1="-1.397" x2="-3.5052" y2="-1.397" width="0" layer="51"/>
<wire x1="-3.5052" y1="-1.8796" x2="-3.5052" y2="-1.6256" width="0" layer="51"/>
<wire x1="-3.5052" y1="-1.6256" x2="-4.4958" y2="-1.6256" width="0" layer="51"/>
<wire x1="-4.4958" y1="-1.6256" x2="-4.4958" y2="-1.8796" width="0" layer="51"/>
<wire x1="-4.4958" y1="-1.8796" x2="-3.5052" y2="-1.8796" width="0" layer="51"/>
<wire x1="-3.5052" y1="-2.3876" x2="-3.5052" y2="-2.1082" width="0" layer="51"/>
<wire x1="-3.5052" y1="-2.1082" x2="-4.4958" y2="-2.1082" width="0" layer="51"/>
<wire x1="-4.4958" y1="-2.1082" x2="-4.4958" y2="-2.3876" width="0" layer="51"/>
<wire x1="-4.4958" y1="-2.3876" x2="-3.5052" y2="-2.3876" width="0" layer="51"/>
<wire x1="-3.5052" y1="-2.8956" x2="-3.5052" y2="-2.6162" width="0" layer="51"/>
<wire x1="-3.5052" y1="-2.6162" x2="-4.4958" y2="-2.6162" width="0" layer="51"/>
<wire x1="-4.4958" y1="-2.6162" x2="-4.4958" y2="-2.8956" width="0" layer="51"/>
<wire x1="-4.4958" y1="-2.8956" x2="-3.5052" y2="-2.8956" width="0" layer="51"/>
<wire x1="-2.6162" y1="-3.5052" x2="-2.8956" y2="-3.5052" width="0" layer="51"/>
<wire x1="-2.8956" y1="-3.5052" x2="-2.8956" y2="-4.4958" width="0" layer="51"/>
<wire x1="-2.8956" y1="-4.4958" x2="-2.6162" y2="-4.4958" width="0" layer="51"/>
<wire x1="-2.6162" y1="-4.4958" x2="-2.6162" y2="-3.5052" width="0" layer="51"/>
<wire x1="-2.1082" y1="-3.5052" x2="-2.3876" y2="-3.5052" width="0" layer="51"/>
<wire x1="-2.3876" y1="-3.5052" x2="-2.3876" y2="-4.4958" width="0" layer="51"/>
<wire x1="-2.3876" y1="-4.4958" x2="-2.1082" y2="-4.4958" width="0" layer="51"/>
<wire x1="-2.1082" y1="-4.4958" x2="-2.1082" y2="-3.5052" width="0" layer="51"/>
<wire x1="-1.6256" y1="-3.5052" x2="-1.8796" y2="-3.5052" width="0" layer="51"/>
<wire x1="-1.8796" y1="-3.5052" x2="-1.8796" y2="-4.4958" width="0" layer="51"/>
<wire x1="-1.8796" y1="-4.4958" x2="-1.6256" y2="-4.4958" width="0" layer="51"/>
<wire x1="-1.6256" y1="-4.4958" x2="-1.6256" y2="-3.5052" width="0" layer="51"/>
<wire x1="-1.1176" y1="-3.5052" x2="-1.397" y2="-3.5052" width="0" layer="51"/>
<wire x1="-1.397" y1="-3.5052" x2="-1.397" y2="-4.4958" width="0" layer="51"/>
<wire x1="-1.397" y1="-4.4958" x2="-1.1176" y2="-4.4958" width="0" layer="51"/>
<wire x1="-1.1176" y1="-4.4958" x2="-1.1176" y2="-3.5052" width="0" layer="51"/>
<wire x1="-0.6096" y1="-3.5052" x2="-0.889" y2="-3.5052" width="0" layer="51"/>
<wire x1="-0.889" y1="-3.5052" x2="-0.889" y2="-4.4958" width="0" layer="51"/>
<wire x1="-0.889" y1="-4.4958" x2="-0.6096" y2="-4.4958" width="0" layer="51"/>
<wire x1="-0.6096" y1="-4.4958" x2="-0.6096" y2="-3.5052" width="0" layer="51"/>
<wire x1="-0.127" y1="-3.5052" x2="-0.381" y2="-3.5052" width="0" layer="51"/>
<wire x1="-0.381" y1="-3.5052" x2="-0.381" y2="-4.4958" width="0" layer="51"/>
<wire x1="-0.381" y1="-4.4958" x2="-0.127" y2="-4.4958" width="0" layer="51"/>
<wire x1="-0.127" y1="-4.4958" x2="-0.127" y2="-3.5052" width="0" layer="51"/>
<wire x1="0.381" y1="-3.5052" x2="0.127" y2="-3.5052" width="0" layer="51"/>
<wire x1="0.127" y1="-3.5052" x2="0.127" y2="-4.4958" width="0" layer="51"/>
<wire x1="0.127" y1="-4.4958" x2="0.381" y2="-4.4958" width="0" layer="51"/>
<wire x1="0.381" y1="-4.4958" x2="0.381" y2="-3.5052" width="0" layer="51"/>
<wire x1="0.889" y1="-3.5052" x2="0.6096" y2="-3.5052" width="0" layer="51"/>
<wire x1="0.6096" y1="-3.5052" x2="0.6096" y2="-4.4958" width="0" layer="51"/>
<wire x1="0.6096" y1="-4.4958" x2="0.889" y2="-4.4958" width="0" layer="51"/>
<wire x1="0.889" y1="-4.4958" x2="0.889" y2="-3.5052" width="0" layer="51"/>
<wire x1="1.397" y1="-3.5052" x2="1.1176" y2="-3.5052" width="0" layer="51"/>
<wire x1="1.1176" y1="-3.5052" x2="1.1176" y2="-4.4958" width="0" layer="51"/>
<wire x1="1.1176" y1="-4.4958" x2="1.397" y2="-4.4958" width="0" layer="51"/>
<wire x1="1.397" y1="-4.4958" x2="1.397" y2="-3.5052" width="0" layer="51"/>
<wire x1="1.8796" y1="-3.5052" x2="1.6256" y2="-3.5052" width="0" layer="51"/>
<wire x1="1.6256" y1="-3.5052" x2="1.6256" y2="-4.4958" width="0" layer="51"/>
<wire x1="1.6256" y1="-4.4958" x2="1.8796" y2="-4.4958" width="0" layer="51"/>
<wire x1="1.8796" y1="-4.4958" x2="1.8796" y2="-3.5052" width="0" layer="51"/>
<wire x1="2.3876" y1="-3.5052" x2="2.1082" y2="-3.5052" width="0" layer="51"/>
<wire x1="2.1082" y1="-3.5052" x2="2.1082" y2="-4.4958" width="0" layer="51"/>
<wire x1="2.1082" y1="-4.4958" x2="2.3876" y2="-4.4958" width="0" layer="51"/>
<wire x1="2.3876" y1="-4.4958" x2="2.3876" y2="-3.5052" width="0" layer="51"/>
<wire x1="2.8956" y1="-3.5052" x2="2.6162" y2="-3.5052" width="0" layer="51"/>
<wire x1="2.6162" y1="-3.5052" x2="2.6162" y2="-4.4958" width="0" layer="51"/>
<wire x1="2.6162" y1="-4.4958" x2="2.8956" y2="-4.4958" width="0" layer="51"/>
<wire x1="2.8956" y1="-4.4958" x2="2.8956" y2="-3.5052" width="0" layer="51"/>
<wire x1="3.5052" y1="-2.6162" x2="3.5052" y2="-2.8956" width="0" layer="51"/>
<wire x1="3.5052" y1="-2.8956" x2="4.4958" y2="-2.8956" width="0" layer="51"/>
<wire x1="4.4958" y1="-2.8956" x2="4.4958" y2="-2.6162" width="0" layer="51"/>
<wire x1="4.4958" y1="-2.6162" x2="3.5052" y2="-2.6162" width="0" layer="51"/>
<wire x1="3.5052" y1="-2.1082" x2="3.5052" y2="-2.3876" width="0" layer="51"/>
<wire x1="3.5052" y1="-2.3876" x2="4.4958" y2="-2.3876" width="0" layer="51"/>
<wire x1="4.4958" y1="-2.3876" x2="4.4958" y2="-2.1082" width="0" layer="51"/>
<wire x1="4.4958" y1="-2.1082" x2="3.5052" y2="-2.1082" width="0" layer="51"/>
<wire x1="3.5052" y1="-1.6256" x2="3.5052" y2="-1.8796" width="0" layer="51"/>
<wire x1="3.5052" y1="-1.8796" x2="4.4958" y2="-1.8796" width="0" layer="51"/>
<wire x1="4.4958" y1="-1.8796" x2="4.4958" y2="-1.6256" width="0" layer="51"/>
<wire x1="4.4958" y1="-1.6256" x2="3.5052" y2="-1.6256" width="0" layer="51"/>
<wire x1="3.5052" y1="-1.1176" x2="3.5052" y2="-1.397" width="0" layer="51"/>
<wire x1="3.5052" y1="-1.397" x2="4.4958" y2="-1.397" width="0" layer="51"/>
<wire x1="4.4958" y1="-1.397" x2="4.4958" y2="-1.1176" width="0" layer="51"/>
<wire x1="4.4958" y1="-1.1176" x2="3.5052" y2="-1.1176" width="0" layer="51"/>
<wire x1="3.5052" y1="-0.6096" x2="3.5052" y2="-0.889" width="0" layer="51"/>
<wire x1="3.5052" y1="-0.889" x2="4.4958" y2="-0.889" width="0" layer="51"/>
<wire x1="4.4958" y1="-0.889" x2="4.4958" y2="-0.6096" width="0" layer="51"/>
<wire x1="4.4958" y1="-0.6096" x2="3.5052" y2="-0.6096" width="0" layer="51"/>
<wire x1="3.5052" y1="-0.127" x2="3.5052" y2="-0.381" width="0" layer="51"/>
<wire x1="3.5052" y1="-0.381" x2="4.4958" y2="-0.381" width="0" layer="51"/>
<wire x1="4.4958" y1="-0.381" x2="4.4958" y2="-0.127" width="0" layer="51"/>
<wire x1="4.4958" y1="-0.127" x2="3.5052" y2="-0.127" width="0" layer="51"/>
<wire x1="3.5052" y1="0.381" x2="3.5052" y2="0.127" width="0" layer="51"/>
<wire x1="3.5052" y1="0.127" x2="4.4958" y2="0.127" width="0" layer="51"/>
<wire x1="4.4958" y1="0.127" x2="4.4958" y2="0.381" width="0" layer="51"/>
<wire x1="4.4958" y1="0.381" x2="3.5052" y2="0.381" width="0" layer="51"/>
<wire x1="3.5052" y1="0.889" x2="3.5052" y2="0.6096" width="0" layer="51"/>
<wire x1="3.5052" y1="0.6096" x2="4.4958" y2="0.6096" width="0" layer="51"/>
<wire x1="4.4958" y1="0.6096" x2="4.4958" y2="0.889" width="0" layer="51"/>
<wire x1="4.4958" y1="0.889" x2="3.5052" y2="0.889" width="0" layer="51"/>
<wire x1="3.5052" y1="1.397" x2="3.5052" y2="1.1176" width="0" layer="51"/>
<wire x1="3.5052" y1="1.1176" x2="4.4958" y2="1.1176" width="0" layer="51"/>
<wire x1="4.4958" y1="1.1176" x2="4.4958" y2="1.397" width="0" layer="51"/>
<wire x1="4.4958" y1="1.397" x2="3.5052" y2="1.397" width="0" layer="51"/>
<wire x1="3.5052" y1="1.8796" x2="3.5052" y2="1.6256" width="0" layer="51"/>
<wire x1="3.5052" y1="1.6256" x2="4.4958" y2="1.6256" width="0" layer="51"/>
<wire x1="4.4958" y1="1.6256" x2="4.4958" y2="1.8796" width="0" layer="51"/>
<wire x1="4.4958" y1="1.8796" x2="3.5052" y2="1.8796" width="0" layer="51"/>
<wire x1="3.5052" y1="2.3876" x2="3.5052" y2="2.1082" width="0" layer="51"/>
<wire x1="3.5052" y1="2.1082" x2="4.4958" y2="2.1082" width="0" layer="51"/>
<wire x1="4.4958" y1="2.1082" x2="4.4958" y2="2.3876" width="0" layer="51"/>
<wire x1="4.4958" y1="2.3876" x2="3.5052" y2="2.3876" width="0" layer="51"/>
<wire x1="3.5052" y1="2.8956" x2="3.5052" y2="2.6162" width="0" layer="51"/>
<wire x1="3.5052" y1="2.6162" x2="4.4958" y2="2.6162" width="0" layer="51"/>
<wire x1="4.4958" y1="2.6162" x2="4.4958" y2="2.8956" width="0" layer="51"/>
<wire x1="4.4958" y1="2.8956" x2="3.5052" y2="2.8956" width="0" layer="51"/>
<wire x1="-3.5052" y1="2.2352" x2="-2.2352" y2="3.5052" width="0" layer="51"/>
<wire x1="-3.5052" y1="-3.5052" x2="3.5052" y2="-3.5052" width="0" layer="51"/>
<wire x1="3.5052" y1="-3.5052" x2="3.5052" y2="3.5052" width="0" layer="51"/>
<wire x1="3.5052" y1="3.5052" x2="-3.5052" y2="3.5052" width="0" layer="51"/>
<wire x1="-3.5052" y1="3.5052" x2="-3.5052" y2="-3.5052" width="0" layer="51"/>
<text x="-6.0706" y="2.7432" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="6.985" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-9.525" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ADV7125BCPZ170">
<pin name="VAA_2" x="-17.78" y="58.42" length="middle" direction="pwr"/>
<pin name="VAA_3" x="-17.78" y="55.88" length="middle" direction="pwr"/>
<pin name="VAA" x="-17.78" y="53.34" length="middle" direction="pwr"/>
<pin name="R0" x="-17.78" y="48.26" length="middle" direction="in"/>
<pin name="R1" x="-17.78" y="45.72" length="middle" direction="in"/>
<pin name="R2" x="-17.78" y="43.18" length="middle" direction="in"/>
<pin name="R3" x="-17.78" y="40.64" length="middle" direction="in"/>
<pin name="R4" x="-17.78" y="38.1" length="middle" direction="in"/>
<pin name="R5" x="-17.78" y="35.56" length="middle" direction="in"/>
<pin name="R6" x="-17.78" y="33.02" length="middle" direction="in"/>
<pin name="R7" x="-17.78" y="30.48" length="middle" direction="in"/>
<pin name="G0" x="-17.78" y="25.4" length="middle" direction="in"/>
<pin name="G1" x="-17.78" y="22.86" length="middle" direction="in"/>
<pin name="G2" x="-17.78" y="20.32" length="middle" direction="in"/>
<pin name="G3" x="-17.78" y="17.78" length="middle" direction="in"/>
<pin name="G4" x="-17.78" y="15.24" length="middle" direction="in"/>
<pin name="G5" x="-17.78" y="12.7" length="middle" direction="in"/>
<pin name="G6" x="-17.78" y="10.16" length="middle" direction="in"/>
<pin name="G7" x="-17.78" y="7.62" length="middle" direction="in"/>
<pin name="B0" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="B1" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="B2" x="-17.78" y="-2.54" length="middle" direction="in"/>
<pin name="B3" x="-17.78" y="-5.08" length="middle" direction="in"/>
<pin name="B4" x="-17.78" y="-7.62" length="middle" direction="in"/>
<pin name="B5" x="-17.78" y="-10.16" length="middle" direction="in"/>
<pin name="B6" x="-17.78" y="-12.7" length="middle" direction="in"/>
<pin name="B7" x="-17.78" y="-15.24" length="middle" direction="in"/>
<pin name="~BLANK" x="-17.78" y="-20.32" length="middle" direction="in"/>
<pin name="~PSAVE" x="-17.78" y="-22.86" length="middle" direction="in"/>
<pin name="~SYNC" x="-17.78" y="-25.4" length="middle" direction="in"/>
<pin name="CLOCK" x="-17.78" y="-27.94" length="middle" direction="in"/>
<pin name="VREF" x="-17.78" y="-30.48" length="middle" direction="in"/>
<pin name="RSET" x="-17.78" y="-35.56" length="middle" direction="in"/>
<pin name="COMP" x="-17.78" y="-38.1" length="middle" direction="pas"/>
<pin name="GND_2" x="-17.78" y="-43.18" length="middle" direction="pas"/>
<pin name="GND_3" x="-17.78" y="-45.72" length="middle" direction="pas"/>
<pin name="GND_4" x="-17.78" y="-48.26" length="middle" direction="pas"/>
<pin name="GND_5" x="-17.78" y="-50.8" length="middle" direction="pas"/>
<pin name="GND_6" x="-17.78" y="-53.34" length="middle" direction="pas"/>
<pin name="GND_7" x="-17.78" y="-55.88" length="middle" direction="pas"/>
<pin name="GND_8" x="-17.78" y="-58.42" length="middle" direction="pas"/>
<pin name="GND" x="-17.78" y="-60.96" length="middle" direction="pas"/>
<pin name="EP" x="-17.78" y="-63.5" length="middle" direction="pas"/>
<pin name="IOR" x="17.78" y="55.88" length="middle" direction="out" rot="R180"/>
<pin name="IOG" x="17.78" y="53.34" length="middle" direction="out" rot="R180"/>
<pin name="IOB" x="17.78" y="50.8" length="middle" direction="out" rot="R180"/>
<pin name="~IOR" x="17.78" y="45.72" length="middle" direction="out" rot="R180"/>
<pin name="~IOG" x="17.78" y="43.18" length="middle" direction="out" rot="R180"/>
<pin name="~IOB" x="17.78" y="40.64" length="middle" direction="out" rot="R180"/>
<wire x1="-12.7" y1="63.5" x2="-12.7" y2="-68.58" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-68.58" x2="12.7" y2="-68.58" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-68.58" x2="12.7" y2="63.5" width="0.4064" layer="94"/>
<wire x1="12.7" y1="63.5" x2="-12.7" y2="63.5" width="0.4064" layer="94"/>
<text x="-5.3594" y="67.5386" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.7846" y="-74.3204" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADV7125BCPZ170" prefix="U">
<description>8-Bit High Speed Video DAC</description>
<gates>
<gate name="A" symbol="ADV7125BCPZ170" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN50P700X700X100-49N">
<connects>
<connect gate="A" pin="B0" pad="16"/>
<connect gate="A" pin="B1" pad="17"/>
<connect gate="A" pin="B2" pad="18"/>
<connect gate="A" pin="B3" pad="19"/>
<connect gate="A" pin="B4" pad="20"/>
<connect gate="A" pin="B5" pad="21"/>
<connect gate="A" pin="B6" pad="22"/>
<connect gate="A" pin="B7" pad="23"/>
<connect gate="A" pin="CLOCK" pad="24"/>
<connect gate="A" pin="COMP" pad="35"/>
<connect gate="A" pin="EP" pad="49"/>
<connect gate="A" pin="G0" pad="3"/>
<connect gate="A" pin="G1" pad="4"/>
<connect gate="A" pin="G2" pad="5"/>
<connect gate="A" pin="G3" pad="6"/>
<connect gate="A" pin="G4" pad="7"/>
<connect gate="A" pin="G5" pad="8"/>
<connect gate="A" pin="G6" pad="9"/>
<connect gate="A" pin="G7" pad="10"/>
<connect gate="A" pin="GND" pad="40"/>
<connect gate="A" pin="GND_2" pad="1"/>
<connect gate="A" pin="GND_3" pad="2"/>
<connect gate="A" pin="GND_4" pad="14"/>
<connect gate="A" pin="GND_5" pad="15"/>
<connect gate="A" pin="GND_6" pad="25"/>
<connect gate="A" pin="GND_7" pad="26"/>
<connect gate="A" pin="GND_8" pad="39"/>
<connect gate="A" pin="IOB" pad="28"/>
<connect gate="A" pin="IOG" pad="32"/>
<connect gate="A" pin="IOR" pad="34"/>
<connect gate="A" pin="R0" pad="41"/>
<connect gate="A" pin="R1" pad="42"/>
<connect gate="A" pin="R2" pad="43"/>
<connect gate="A" pin="R3" pad="44"/>
<connect gate="A" pin="R4" pad="45"/>
<connect gate="A" pin="R5" pad="46"/>
<connect gate="A" pin="R6" pad="47"/>
<connect gate="A" pin="R7" pad="48"/>
<connect gate="A" pin="RSET" pad="37"/>
<connect gate="A" pin="VAA" pad="30"/>
<connect gate="A" pin="VAA_2" pad="13"/>
<connect gate="A" pin="VAA_3" pad="29"/>
<connect gate="A" pin="VREF" pad="36"/>
<connect gate="A" pin="~BLANK" pad="11"/>
<connect gate="A" pin="~IOB" pad="27"/>
<connect gate="A" pin="~IOG" pad="31"/>
<connect gate="A" pin="~IOR" pad="33"/>
<connect gate="A" pin="~PSAVE" pad="38"/>
<connect gate="A" pin="~SYNC" pad="12"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="ADV7125BCPZ170" constant="no"/>
<attribute name="OC_FARNELL" value="-" constant="no"/>
<attribute name="OC_NEWARK" value="85R6422" constant="no"/>
<attribute name="PACKAGE" value="LFCSP-48" constant="no"/>
<attribute name="SUPPLIER" value="Analog Devices" constant="no"/>
</technology>
</technologies>
</device>
<device name="TQFP-48" package="QFP50P900X900X120-49N">
<connects>
<connect gate="A" pin="B0" pad="16"/>
<connect gate="A" pin="B1" pad="17"/>
<connect gate="A" pin="B2" pad="18"/>
<connect gate="A" pin="B3" pad="19"/>
<connect gate="A" pin="B4" pad="20"/>
<connect gate="A" pin="B5" pad="21"/>
<connect gate="A" pin="B6" pad="22"/>
<connect gate="A" pin="B7" pad="23"/>
<connect gate="A" pin="CLOCK" pad="24"/>
<connect gate="A" pin="COMP" pad="35"/>
<connect gate="A" pin="EP" pad="49"/>
<connect gate="A" pin="G0" pad="3"/>
<connect gate="A" pin="G1" pad="4"/>
<connect gate="A" pin="G2" pad="5"/>
<connect gate="A" pin="G3" pad="6"/>
<connect gate="A" pin="G4" pad="7"/>
<connect gate="A" pin="G5" pad="8"/>
<connect gate="A" pin="G6" pad="9"/>
<connect gate="A" pin="G7" pad="10"/>
<connect gate="A" pin="GND" pad="40"/>
<connect gate="A" pin="GND_2" pad="1"/>
<connect gate="A" pin="GND_3" pad="2"/>
<connect gate="A" pin="GND_4" pad="14"/>
<connect gate="A" pin="GND_5" pad="15"/>
<connect gate="A" pin="GND_6" pad="25"/>
<connect gate="A" pin="GND_7" pad="26"/>
<connect gate="A" pin="GND_8" pad="39"/>
<connect gate="A" pin="IOB" pad="28"/>
<connect gate="A" pin="IOG" pad="32"/>
<connect gate="A" pin="IOR" pad="34"/>
<connect gate="A" pin="R0" pad="41"/>
<connect gate="A" pin="R1" pad="42"/>
<connect gate="A" pin="R2" pad="43"/>
<connect gate="A" pin="R3" pad="44"/>
<connect gate="A" pin="R4" pad="45"/>
<connect gate="A" pin="R5" pad="46"/>
<connect gate="A" pin="R6" pad="47"/>
<connect gate="A" pin="R7" pad="48"/>
<connect gate="A" pin="RSET" pad="37"/>
<connect gate="A" pin="VAA" pad="30"/>
<connect gate="A" pin="VAA_2" pad="13"/>
<connect gate="A" pin="VAA_3" pad="29"/>
<connect gate="A" pin="VREF" pad="36"/>
<connect gate="A" pin="~BLANK" pad="11"/>
<connect gate="A" pin="~IOB" pad="27"/>
<connect gate="A" pin="~IOG" pad="31"/>
<connect gate="A" pin="~IOR" pad="33"/>
<connect gate="A" pin="~PSAVE" pad="38"/>
<connect gate="A" pin="~SYNC" pad="12"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-usb-3">
<description>&lt;b&gt;USB Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by Erik Christiansson (erik@alphafish.com)&lt;/author&gt;&lt;p&gt;
Based on the datasheets for the following products.&lt;br&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;Manufacturer&lt;/td&gt;&lt;td&gt;Part No.&lt;/td&gt;&lt;td&gt;Type&lt;/td&gt;&lt;tr&gt;
&lt;tr&gt;&lt;td&gt;Hsuan Mao&lt;/td&gt;&lt;td&gt;C8317-04AFDXX0&lt;/td&gt;&lt;td&gt;Series A Surface Mounted&lt;/td&gt;&lt;tr&gt;
&lt;tr&gt;&lt;td&gt;Hsuan Mao&lt;/td&gt;&lt;td&gt;C8317-04AFHSW0&lt;/td&gt;&lt;td&gt;Series A Hole Mounted&lt;/td&gt;&lt;tr&gt;
&lt;tr&gt;&lt;td&gt;Hsuan Mao&lt;/td&gt;&lt;td&gt;C3817-04AFVSW0&lt;/td&gt;&lt;td&gt;Series A Hole Mounted Up-Right&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Hsuan Mao&lt;/td&gt;&lt;td&gt;C8317-04BFHSW0&lt;/td&gt;&lt;td&gt;Series B Hole Mounted&lt;/td&gt;&lt;tr&gt;
&lt;tr&gt;&lt;td&gt;Hsuan Mao&lt;/td&gt;&lt;td&gt;C8320-05BFDSB0&lt;/td&gt;&lt;td&gt;Series Mini-B Surface Mounted&lt;/td&gt;&lt;tr&gt;
&lt;tr&gt;&lt;td&gt;Hsuan Mao&lt;/td&gt;&lt;td&gt;C8320-05BFRSB0&lt;/td&gt;&lt;td&gt;Series Mini-B Hole Mounted&lt;/td&gt;&lt;tr&gt;
&lt;tr&gt;&lt;td&gt;Amp&lt;/td&gt;&lt;td&gt;787616-*&lt;/td&gt;&lt;td&gt;Series A Hole Mounted&lt;/td&gt;&lt;tr&gt;
&lt;tr&gt;&lt;td&gt;Amp&lt;/td&gt;&lt;td&gt;787780-1&lt;/td&gt;&lt;td&gt;Series B Hole Mounted&lt;/td&gt;&lt;tr&gt;
&lt;tr&gt;&lt;td&gt;ACON&lt;/td&gt;&lt;td&gt;UAR80-4****0&lt;/td&gt;&lt;td&gt;Series A Hole Mounted Up-Right&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;ACON&lt;/td&gt;&lt;td&gt;MNC20-5K5*1*&lt;/td&gt;&lt;td&gt;Series Mini-B Surface Mounted&lt;/td&gt;&lt;tr&gt;
&lt;tr&gt;&lt;td&gt;Molex&lt;/td&gt;&lt;td&gt;67068-****&lt;/td&gt;&lt;td&gt;Series B Hole Mounted&lt;/td&gt;&lt;tr&gt;
&lt;/table&gt;</description>
<packages>
<package name="USB-MB-S">
<description>&lt;b&gt;USB Series Mini-B Surface Mounted&lt;/b&gt;</description>
<wire x1="-5.95" y1="3.85" x2="-5.95" y2="-3.85" width="0.127" layer="22"/>
<wire x1="-5.95" y1="3.85" x2="3.3" y2="3.85" width="0.127" layer="22"/>
<wire x1="3.3" y1="3.85" x2="3.3" y2="-3.85" width="0.127" layer="22"/>
<wire x1="3.3" y1="-3.85" x2="-5.95" y2="-3.85" width="0.127" layer="22"/>
<wire x1="-4.615" y1="1.27" x2="-1.44" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.44" y1="0.635" x2="-1.44" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.44" y1="-0.635" x2="-4.615" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.075" y1="-3.175" x2="-4.615" y2="-2.8575" width="0.127" layer="21"/>
<wire x1="-4.615" y1="-2.8575" x2="-4.615" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-4.615" y1="-2.2225" x2="-2.075" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-2.075" y1="3.175" x2="-4.615" y2="2.8575" width="0.127" layer="21"/>
<wire x1="-4.615" y1="2.8575" x2="-4.615" y2="2.2225" width="0.127" layer="21"/>
<wire x1="-4.615" y1="2.2225" x2="-2.075" y2="1.905" width="0.127" layer="21"/>
<pad name="P$5" x="0" y="2.2" drill="0.9"/>
<pad name="P$6" x="0" y="-2.2" drill="0.9"/>
<smd name="P$1" x="-3" y="-4.5" dx="2.5" dy="2" layer="1"/>
<smd name="P$2" x="-3" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="P$3" x="2.5" y="-4.4" dx="2.5" dy="2" layer="1"/>
<smd name="P$4" x="2.5" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="D+" x="2.5" y="0" dx="2.5" dy="0.5" layer="1"/>
<smd name="D-" x="2.5" y="0.8" dx="2.5" dy="0.5" layer="1"/>
<smd name="VBUS" x="2.5" y="1.6" dx="2.5" dy="0.5" layer="1"/>
<smd name="ID" x="2.5" y="-0.8" dx="2.5" dy="0.5" layer="1"/>
<smd name="GND" x="2.5" y="-1.6" dx="2.5" dy="0.5" layer="1"/>
<text x="5.2275" y="1.5875" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="USB-A-H">
<description>&lt;b&gt;USB Series A Hole Mounted&lt;/b&gt;</description>
<wire x1="3.6957" y1="6.5659" x2="-10.287" y2="6.5659" width="0.127" layer="21"/>
<wire x1="3.6957" y1="-6.5659" x2="-10.287" y2="-6.5659" width="0.127" layer="21"/>
<wire x1="-10.287" y1="6.477" x2="-10.287" y2="-6.477" width="0.127" layer="21"/>
<wire x1="3.7084" y1="6.5024" x2="3.7084" y2="-6.5024" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-5.08" x2="-8.89" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-4.445" x2="-8.89" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-1.27" x2="-2.54" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="-8.89" y2="4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="4.445" x2="-8.89" y2="1.27" width="0.127" layer="21"/>
<wire x1="-8.89" y1="1.27" x2="-2.54" y2="0.635" width="0.127" layer="21"/>
<pad name="VBUS" x="2.7178" y="3.4925" drill="0.9144" rot="R270"/>
<pad name="D-" x="2.7178" y="1.0033" drill="0.9144" rot="R270"/>
<pad name="D+" x="2.7178" y="-1.0033" drill="0.9144" rot="R270"/>
<pad name="GND" x="2.7178" y="-3.4925" drill="0.9144" rot="R270"/>
<pad name="P$5" x="0" y="-6.5659" drill="2.3114" rot="R270"/>
<pad name="P$6" x="0" y="6.5659" drill="2.3114" rot="R270"/>
<text x="5.715" y="3.81" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="USB-B-H">
<description>&lt;b&gt;USB Series B Hole Mounted&lt;/b&gt;</description>
<wire x1="-12.486" y1="6.0144" x2="-12.486" y2="-5.9998" width="0.127" layer="21"/>
<wire x1="3.2366" y1="6.0144" x2="3.2366" y2="-5.9998" width="0.127" layer="21"/>
<wire x1="3.2312" y1="6.0198" x2="-12.466" y2="6.0198" width="0.127" layer="21"/>
<wire x1="3.2058" y1="-6.0198" x2="-12.4406" y2="-6.0198" width="0.127" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="-11.43" y2="4.445" width="0.127" layer="21"/>
<wire x1="-11.43" y1="4.445" x2="-11.43" y2="1.27" width="0.127" layer="21"/>
<wire x1="-11.43" y1="1.27" x2="-5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="-11.43" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-4.445" x2="-11.43" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-1.27" x2="-5.08" y2="-0.635" width="0.127" layer="21"/>
<pad name="VBUS" x="1.9812" y="-1.25" drill="0.9144" rot="R90"/>
<pad name="D-" x="1.9812" y="1.25" drill="0.9144" rot="R90"/>
<pad name="D+" x="0" y="1.25" drill="0.9144" rot="R270"/>
<pad name="GND" x="0" y="-1.25" drill="0.9144" rot="R270"/>
<pad name="P$1" x="-2.7178" y="-6.0198" drill="2.286"/>
<pad name="P$2" x="-2.7178" y="6.0198" drill="2.286"/>
<text x="5.06" y="3.175" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="USB-A-S">
<description>&lt;b&gt;USB Series A Surface Mounted&lt;/b&gt;</description>
<wire x1="3.6957" y1="6.5659" x2="-10.287" y2="6.5659" width="0.127" layer="21"/>
<wire x1="3.6957" y1="-6.5659" x2="-10.287" y2="-6.5659" width="0.127" layer="21"/>
<wire x1="-10.287" y1="6.477" x2="-10.287" y2="-6.477" width="0.127" layer="21"/>
<wire x1="3.7084" y1="6.5024" x2="3.7084" y2="-6.5024" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-5.08" x2="-8.89" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-4.445" x2="-8.89" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-1.27" x2="-2.54" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="-8.89" y2="4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="4.445" x2="-8.89" y2="1.27" width="0.127" layer="21"/>
<wire x1="-8.89" y1="1.27" x2="-2.54" y2="0.635" width="0.127" layer="21"/>
<pad name="P$5" x="0" y="-6.5659" drill="2.3114" rot="R270"/>
<pad name="P$6" x="0" y="6.5659" drill="2.3114" rot="R270"/>
<smd name="D-" x="3.45" y="1" dx="3" dy="0.9" layer="1"/>
<smd name="VBUS" x="3.45" y="3" dx="3" dy="0.9" layer="1"/>
<smd name="D+" x="3.45" y="-1" dx="3" dy="0.9" layer="1"/>
<smd name="GND" x="3.45" y="-3" dx="3" dy="0.9" layer="1"/>
<text x="5.715" y="3.81" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="USB-MB-H">
<description>&lt;b&gt;USB Series Mini-B Hole Mounted&lt;/b&gt;</description>
<wire x1="-3.75" y1="3.9" x2="-3.75" y2="-3.9" width="0.127" layer="22"/>
<wire x1="5.25" y1="3.9" x2="5.25" y2="-3.9" width="0.127" layer="22"/>
<wire x1="-3.75" y1="3.9" x2="5.25" y2="3.9" width="0.127" layer="22"/>
<wire x1="-3.75" y1="-3.9" x2="5.25" y2="-3.9" width="0.127" layer="22"/>
<wire x1="0.75" y1="3.5" x2="-3.25" y2="3" width="0.127" layer="22"/>
<wire x1="-3.25" y1="3" x2="-3.25" y2="2" width="0.127" layer="22"/>
<wire x1="-3.25" y1="2" x2="0.75" y2="1.5" width="0.127" layer="22"/>
<wire x1="1.25" y1="-3.5" x2="-3.25" y2="-3" width="0.127" layer="22"/>
<wire x1="-3.25" y1="-3" x2="-3.25" y2="-2" width="0.127" layer="22"/>
<wire x1="-3.25" y1="-2" x2="1.25" y2="-1.5" width="0.127" layer="22"/>
<wire x1="-3.25" y1="1.25" x2="1.75" y2="0.75" width="0.127" layer="22"/>
<wire x1="1.75" y1="0.75" x2="1.75" y2="-0.75" width="0.127" layer="22"/>
<wire x1="1.75" y1="-0.75" x2="-3.25" y2="-1.25" width="0.127" layer="22"/>
<pad name="VBUS" x="5.1" y="1.6" drill="0.8"/>
<pad name="D+" x="5.1" y="0" drill="0.8"/>
<pad name="GND" x="5.1" y="-1.6" drill="0.8"/>
<pad name="D-" x="3.9" y="0.8" drill="0.8"/>
<pad name="ID" x="3.9" y="-0.8" drill="0.8"/>
<pad name="P$6" x="0" y="-3.65" drill="1.9"/>
<pad name="P$7" x="0" y="3.65" drill="1.9"/>
<text x="7.25" y="1.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="3.25" y1="3" x2="5.75" y2="4.4" layer="43"/>
<rectangle x1="3.25" y1="-4.4" x2="5.75" y2="-3" layer="43"/>
<rectangle x1="-3.75" y1="-3.1" x2="-1.425" y2="3.1" layer="43"/>
<rectangle x1="-1.425" y1="-2.325" x2="-0.65" y2="2.325" layer="43"/>
</package>
<package name="USB-A-HU">
<description>&lt;b&gt;USB Series A Hole Mounted Up-Right&lt;/b&gt;</description>
<wire x1="-11.6" y1="2.6" x2="-11.6" y2="-2.6" width="0.127" layer="21"/>
<wire x1="7.7" y1="2.6" x2="7.7" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-11.6" y1="2.6" x2="7.7" y2="2.6" width="0.127" layer="21"/>
<wire x1="-11.6" y1="-2.6" x2="7.7" y2="-2.6" width="0.127" layer="21"/>
<pad name="GND" x="0.73" y="0" drill="0.9"/>
<pad name="D+" x="2.73" y="0" drill="0.9"/>
<pad name="D-" x="4.73" y="0" drill="0.9"/>
<pad name="VBUS" x="6.73" y="0" drill="0.9"/>
<pad name="P$5" x="0" y="2.72" drill="1.5"/>
<pad name="P$6" x="0" y="-2.72" drill="1.5"/>
<pad name="P$7" x="7" y="2.72" drill="1.5"/>
<pad name="P$8" x="7" y="-2.72" drill="1.5"/>
<text x="10" y="1" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="USB-B-SMT">
<description>USB Series B Surface Mounted</description>
<wire x1="-9" y1="6" x2="-9" y2="-6" width="0.127" layer="21"/>
<wire x1="-9" y1="-6" x2="-1" y2="-6" width="0.127" layer="21"/>
<wire x1="-1" y1="-6" x2="2.4" y2="-6" width="0.127" layer="21"/>
<wire x1="2.4" y1="-6" x2="7" y2="-6" width="0.127" layer="21"/>
<wire x1="7" y1="-6" x2="7" y2="6" width="0.127" layer="21"/>
<wire x1="7" y1="6" x2="2.4" y2="6" width="0.127" layer="21"/>
<wire x1="2.4" y1="6" x2="-1" y2="6" width="0.127" layer="21"/>
<wire x1="-1" y1="6" x2="-9" y2="6" width="0.127" layer="21"/>
<wire x1="2.4" y1="6" x2="2.4" y2="7.3" width="0.127" layer="21"/>
<wire x1="2.4" y1="7.3" x2="2.2" y2="7.5" width="0.127" layer="21"/>
<wire x1="2.2" y1="7.5" x2="1.9" y2="7.5" width="0.127" layer="21"/>
<wire x1="1.9" y1="7.5" x2="1.4" y2="7" width="0.127" layer="21"/>
<wire x1="-1" y1="6" x2="-1" y2="7.3" width="0.127" layer="21"/>
<wire x1="-0.4" y1="7.4" x2="-0.3" y2="7.5" width="0.127" layer="22"/>
<wire x1="-0.8" y1="7.5" x2="-0.5" y2="7.5" width="0.127" layer="21"/>
<wire x1="-0.5" y1="7.5" x2="0" y2="7" width="0.127" layer="21"/>
<wire x1="0" y1="7" x2="1.4" y2="7" width="0.127" layer="21"/>
<wire x1="-1" y1="-6" x2="-1" y2="-7.3" width="0.127" layer="21"/>
<wire x1="-1" y1="-7.3" x2="-0.8" y2="-7.5" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-7.5" x2="-0.5" y2="-7.5" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-7.5" x2="0" y2="-7" width="0.127" layer="21"/>
<wire x1="3" y1="-7.4" x2="2.9" y2="-7.5" width="0.127" layer="22"/>
<wire x1="1.9" y1="-7.5" x2="1.4" y2="-7" width="0.127" layer="21"/>
<wire x1="1.4" y1="-7" x2="0" y2="-7" width="0.127" layer="21"/>
<wire x1="-1" y1="7.3" x2="-0.8" y2="7.5" width="0.127" layer="21"/>
<wire x1="2.2" y1="-7.5" x2="1.9" y2="-7.5" width="0.127" layer="21"/>
<wire x1="2.2" y1="-7.5" x2="2.4" y2="-7.3" width="0.127" layer="21"/>
<wire x1="2.4" y1="-6" x2="2.4" y2="-7.3" width="0.127" layer="21"/>
<wire x1="6" y1="0.5" x2="5" y2="0.5" width="0.0504" layer="21"/>
<wire x1="5" y1="0.5" x2="5" y2="-0.5" width="0.127" layer="21"/>
<wire x1="5" y1="-0.5" x2="6" y2="-0.5" width="0.0504" layer="21"/>
<smd name="5" x="0.58" y="6.8" dx="6.04" dy="3.4" layer="1"/>
<smd name="6" x="0.58" y="-6.8" dx="6.04" dy="3.4" layer="1"/>
<smd name="D+" x="7" y="1.875" dx="3" dy="0.7" layer="1"/>
<smd name="D-" x="7" y="0.625" dx="3" dy="0.7" layer="1"/>
<smd name="GND" x="7" y="-0.625" dx="3" dy="0.7" layer="1"/>
<smd name="VUSB" x="7" y="-1.875" dx="3" dy="0.7" layer="1"/>
<text x="9.525" y="6.35" size="1.27" layer="25" rot="R270">&gt;NAME</text>
<hole x="0" y="2.25" drill="1.4"/>
<hole x="0" y="-2.25" drill="1.4"/>
</package>
<package name="USB-BV2">
<wire x1="6" y1="5.4" x2="-6" y2="5.4" width="0.127" layer="21"/>
<wire x1="-6" y1="5.4" x2="-6" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-6" y1="-1.3" x2="-6" y2="-5.7" width="0.127" layer="21"/>
<wire x1="-6" y1="-5.7" x2="0" y2="-5.7" width="0.127" layer="21"/>
<wire x1="0" y1="-5.7" x2="6" y2="-5.7" width="0.127" layer="21"/>
<wire x1="6" y1="-5.7" x2="6" y2="5.4" width="0.127" layer="21"/>
<wire x1="-4.2" y1="-3.9" x2="4.2" y2="-3.9" width="0.127" layer="21"/>
<wire x1="4.2" y1="-3.9" x2="4.2" y2="2" width="0.127" layer="21"/>
<wire x1="4.2" y1="2" x2="2.3" y2="3.9" width="0.127" layer="21"/>
<wire x1="2.3" y1="3.9" x2="-2.3" y2="3.9" width="0.127" layer="21"/>
<wire x1="-2.3" y1="3.9" x2="-4.2" y2="2" width="0.127" layer="21"/>
<wire x1="-4.2" y1="2" x2="-4.2" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-2.8" y1="-1.4" x2="2.8" y2="-1.4" width="0.127" layer="21"/>
<wire x1="2.8" y1="-1.4" x2="2.8" y2="1.4" width="0.127" layer="21"/>
<wire x1="-2.8" y1="1.4" x2="-2.8" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.8" y1="1.4" x2="2.8" y2="1.4" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="-5.65" drill="2.4"/>
<pad name="P$2" x="-6.02" y="-1.34" drill="2.4"/>
<pad name="P$3" x="6.02" y="-1.34" drill="2.4"/>
<pad name="P$4" x="0" y="5.35" drill="2.4"/>
<pad name="GND" x="1.25" y="-1.8" drill="0.9144" rot="R90"/>
<pad name="VBUS" x="1.25" y="1.4" drill="0.9144" rot="R90"/>
<pad name="D-" x="-1.25" y="1.4" drill="0.9144" rot="R270"/>
<pad name="D+" x="-1.25" y="-1.8" drill="0.9144" rot="R270"/>
</package>
</packages>
<symbols>
<symbol name="USB">
<wire x1="5.08" y1="8.89" x2="0" y2="8.89" width="0.254" layer="94"/>
<wire x1="0" y1="8.89" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<text x="3.81" y="0" size="2.54" layer="94" rot="R90">USB</text>
<text x="0" y="-5.08" size="1.27" layer="95">&gt;NAME</text>
<pin name="D+" x="-2.54" y="7.62" visible="pad" length="short"/>
<pin name="D-" x="-2.54" y="5.08" visible="pad" length="short"/>
<pin name="VBUS" x="-2.54" y="2.54" visible="pad" length="short"/>
<pin name="GND" x="-2.54" y="0" visible="pad" length="short"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB" prefix="X" uservalue="yes">
<description>&lt;b&gt;USB Connectors&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="USB" x="0" y="0"/>
</gates>
<devices>
<device name="-MB-S" package="USB-MB-S">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-A-H" package="USB-A-H">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-B-H" package="USB-B-H">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-A-S" package="USB-A-S">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MB-H" package="USB-MB-H">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-A-HU" package="USB-A-HU">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-B-S" package="USB-B-SMT">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VUSB"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-B-V" package="USB-BV2">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="SCDA">
<wire x1="-14" y1="-26.3" x2="-14" y2="2.8" width="0.127" layer="21"/>
<wire x1="-14" y1="2.8" x2="14.1" y2="2.8" width="0.127" layer="21"/>
<wire x1="14.1" y1="2.8" x2="14.1" y2="-26.3" width="0.127" layer="21"/>
<wire x1="14.1" y1="-26.3" x2="12" y2="-26.3" width="0.127" layer="21"/>
<wire x1="12" y1="-26.3" x2="-12.4" y2="-26.3" width="0.127" layer="21"/>
<wire x1="-12.4" y1="-26.3" x2="-14" y2="-26.3" width="0.127" layer="21"/>
<wire x1="-12.4" y1="-25.5" x2="12" y2="-25.7" width="0.127" layer="21" curve="-27.095878"/>
<wire x1="-12.4" y1="-25.5" x2="-12.4" y2="-26.3" width="0.127" layer="21"/>
<wire x1="12" y1="-25.7" x2="12" y2="-26.3" width="0.127" layer="21"/>
<smd name="12" x="-14.2" y="-23.5" dx="1.7" dy="3.1" layer="1"/>
<smd name="13" x="14.2" y="-24.1" dx="1.9" dy="1.9" layer="1"/>
<smd name="11" x="-14.1" y="-1.7" dx="1.7" dy="1.8" layer="1"/>
<smd name="10" x="-12.8" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="8" x="-10.4" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="7" x="-8.4" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="6" x="-6.1" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="5" x="-3.6" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="3.9" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="4" x="-1.1" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="1" x="6.4" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="1.4" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="9" x="8.9" y="3.7" dx="4" dy="1.2" layer="1" rot="R90"/>
<rectangle x1="-14" y1="-26.3" x2="-11.7" y2="-20.2" layer="39"/>
<rectangle x1="11.3" y1="-26.3" x2="14.1" y2="-20.8" layer="39"/>
<hole x="-13" y="0" drill="1"/>
<hole x="13" y="0" drill="1"/>
<polygon width="0.127" layer="39">
<vertex x="14.1" y="-2.8"/>
<vertex x="11.3" y="-2.8"/>
<vertex x="11.3" y="-0.5"/>
<vertex x="-11.8" y="-0.4"/>
<vertex x="-11.8" y="-3.2"/>
<vertex x="-14" y="-3.2"/>
<vertex x="-14" y="4.7"/>
<vertex x="14.1" y="4.7"/>
</polygon>
</package>
<package name="SDCARD">
<wire x1="-14.2" y1="-28.81" x2="-14.2" y2="-0.01" width="0.127" layer="21"/>
<wire x1="-14.2" y1="-0.01" x2="14.3" y2="-0.01" width="0.127" layer="21"/>
<wire x1="14.3" y1="-0.01" x2="14.3" y2="-28.81" width="0.127" layer="21"/>
<wire x1="14.3" y1="-28.81" x2="11.8" y2="-28.81" width="0.127" layer="21"/>
<wire x1="11.8" y1="-28.81" x2="-12.6" y2="-28.81" width="0.127" layer="21"/>
<wire x1="-12.6" y1="-28.81" x2="-14.2" y2="-28.81" width="0.127" layer="21"/>
<wire x1="-12.6" y1="-28.01" x2="11.8" y2="-28.21" width="0.127" layer="21" curve="-27.095878"/>
<wire x1="-12.6" y1="-28.01" x2="-12.6" y2="-28.81" width="0.127" layer="21"/>
<wire x1="11.8" y1="-28.21" x2="11.8" y2="-28.81" width="0.127" layer="21"/>
<smd name="MT3" x="14.6" y="-19.01" dx="1.9" dy="1.9" layer="1"/>
<smd name="MT1" x="-14.5" y="-3.91" dx="1.7" dy="1.8" layer="1"/>
<smd name="CD" x="-12.5" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="8" x="-9.8" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="7" x="-8.1" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="6" x="-5.7" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="5" x="-3.1" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="4.3" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="4" x="-0.7" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="1" x="6.8" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="1.8" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="9" x="9.3" y="0.89" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="MT2" x="14.7" y="-3.91" dx="1.9" dy="1.9" layer="1"/>
<smd name="WP" x="-14.2" y="-19" dx="2.032" dy="2.032" layer="1"/>
<smd name="COM" x="-14.2" y="-9.3" dx="2.032" dy="2.032" layer="1"/>
<rectangle x1="-14" y1="-17.41" x2="-11.7" y2="-11.31" layer="39"/>
<rectangle x1="11.3" y1="-17.41" x2="14.1" y2="-11.91" layer="39"/>
<hole x="-12" y="-23.31" drill="1.5"/>
<hole x="12" y="-23.31" drill="1"/>
<polygon width="0.127" layer="39">
<vertex x="14.1" y="6.09"/>
<vertex x="11.3" y="6.09"/>
<vertex x="11.3" y="8.39"/>
<vertex x="-11.8" y="8.49"/>
<vertex x="-11.8" y="5.69"/>
<vertex x="-14" y="5.69"/>
<vertex x="-14" y="13.59"/>
<vertex x="14.1" y="13.59"/>
</polygon>
</package>
<package name="06132">
<wire x1="-13.2" y1="-25.1" x2="-13.2" y2="0" width="0.127" layer="21"/>
<wire x1="-13.2" y1="0" x2="10.6" y2="0" width="0.127" layer="21"/>
<wire x1="10.6" y1="0" x2="10.6" y2="-2.5" width="0.127" layer="21"/>
<wire x1="10.6" y1="-2.5" x2="13.2" y2="-2.5" width="0.127" layer="21"/>
<wire x1="13.2" y1="-2.5" x2="13.3" y2="-25.1" width="0.127" layer="21"/>
<wire x1="13.3" y1="-25.1" x2="12.2" y2="-25.1" width="0.127" layer="21"/>
<wire x1="12.2" y1="-25.1" x2="-12.3" y2="-25.1" width="0.127" layer="21"/>
<wire x1="-12.3" y1="-25.1" x2="-13.2" y2="-25.1" width="0.127" layer="21"/>
<wire x1="-12.4" y1="-24.8" x2="12" y2="-25" width="0.127" layer="21" curve="-27.095878"/>
<wire x1="-12.4" y1="-24.8" x2="-12.3" y2="-25.1" width="0.127" layer="21"/>
<wire x1="12" y1="-25" x2="12.2" y2="-25.1" width="0.127" layer="21"/>
<smd name="GP" x="-13.8" y="-21.8" dx="2.8" dy="2" layer="1" rot="R90"/>
<smd name="GP2" x="14.1" y="-21.8" dx="2.8" dy="2" layer="1" rot="R90"/>
<smd name="GP4" x="-13.9" y="-1" dx="2.8" dy="2" layer="1" rot="R90"/>
<smd name="WP" x="-12.2" y="0.9" dx="4" dy="0.7" layer="1" rot="R90"/>
<smd name="8" x="-9.7" y="0.9" dx="4" dy="0.7" layer="1" rot="R90"/>
<smd name="7" x="-8.1" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="6" x="-5.6" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="5" x="-3.1" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="4.4" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="4" x="-0.6" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="1" x="6.9" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="1.9" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="9" x="9.4" y="0.9" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="CD" x="-11" y="0.9" dx="4" dy="0.7" layer="1" rot="R90"/>
<smd name="GP3" x="11.7" y="-0.9" dx="2.8" dy="2" layer="1" rot="R90"/>
<hole x="-11.6" y="-2.1" drill="1.1"/>
<hole x="9.5" y="-2.1" drill="1.6"/>
</package>
<package name="SMADIODE">
<description>&lt;b&gt;SMA Surface Mount Diode&lt;/b&gt;</description>
<wire x1="-2.15" y1="1.3" x2="2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="1.3" x2="2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.3" x2="-2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.3" x2="-2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-3.789" y1="-1.394" x2="-3.789" y2="-1.146" width="0.127" layer="21"/>
<wire x1="-3.789" y1="-1.146" x2="-3.789" y2="1.6" width="0.127" layer="21"/>
<wire x1="-3.789" y1="1.6" x2="3.816" y2="1.6" width="0.127" layer="21"/>
<wire x1="3.816" y1="1.6" x2="3.816" y2="1.394" width="0.127" layer="21"/>
<wire x1="3.816" y1="1.394" x2="3.816" y2="1.3365" width="0.127" layer="21"/>
<wire x1="3.816" y1="1.394" x2="3.816" y2="-1.6" width="0.127" layer="21"/>
<wire x1="3.816" y1="-1.6" x2="-3.789" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-3.789" y1="-1.6" x2="-3.789" y2="-1.146" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="-0.4445" x2="-0.3175" y2="0.4445" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="0.4445" x2="-0.6985" y2="0" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="0" x2="-0.3175" y2="-0.4445" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="-0.4445" x2="-0.6985" y2="0.4445" width="0.127" layer="21"/>
<smd name="C" x="-2.3495" y="0" dx="2.54" dy="2.54" layer="1"/>
<smd name="A" x="2.3495" y="0" dx="2.54" dy="2.54" layer="1" rot="R180"/>
<text x="-2.54" y="1.905" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.286" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-2.825" y1="-1.1" x2="-2.175" y2="1.1" layer="51"/>
<rectangle x1="2.175" y1="-1.1" x2="2.825" y2="1.1" layer="51" rot="R180"/>
<rectangle x1="-1.75" y1="-1.225" x2="-1.075" y2="1.225" layer="51"/>
</package>
<package name="DO-1N4148">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.2032" layer="21"/>
<pad name="A" x="-3.81" y="0" drill="0.9"/>
<pad name="C" x="3.81" y="0" drill="0.9"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.032" y="-0.254" size="0.6096" layer="21">&gt;Value</text>
</package>
<package name="SOT23-R">
<description>&lt;b&gt;SOT23&lt;/b&gt; - Reflow soldering</description>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6524" x2="-1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.5724" y1="0.6604" x2="-0.5136" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6524" width="0.1524" layer="21"/>
<wire x1="0.5636" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="0.4224" y1="-0.6604" x2="-0.4364" y2="-0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1" dx="0.6" dy="0.7" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.6" dy="0.7" layer="1"/>
<smd name="1" x="-0.95" y="-1" dx="0.6" dy="0.7" layer="1"/>
<text x="1.778" y="0.254" size="0.4064" layer="25">&gt;NAME</text>
<text x="1.778" y="-0.508" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SOT23-W">
<description>&lt;b&gt;SOT23&lt;/b&gt; - Wave soldering</description>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.3984" x2="-1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.3984" width="0.1524" layer="21"/>
<wire x1="0.2954" y1="-0.6604" x2="-0.3094" y2="-0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.3" dx="2.8" dy="1.4" layer="1"/>
<smd name="2" x="1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<smd name="1" x="-1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<text x="2.032" y="0.254" size="0.4064" layer="25">&gt;NAME</text>
<text x="2.032" y="-0.508" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SOD-523">
<description>SOD-523 (0.8x1.2mm)

&lt;p&gt;Source: http://www.rohm.com/products/databook/di/pdf/rb751s-40.pdf&lt;/p&gt;</description>
<wire x1="-0.75" y1="1.5" x2="0.75" y2="1.5" width="0.127" layer="21"/>
<wire x1="0.75" y1="1.5" x2="0.75" y2="-1.5" width="0.127" layer="21"/>
<wire x1="0.75" y1="-1.5" x2="-0.75" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-0.75" y1="-1.5" x2="-0.75" y2="1.5" width="0.127" layer="21"/>
<wire x1="1" y1="0.25" x2="1.5" y2="0.25" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.25" x2="2" y2="0.25" width="0.127" layer="51"/>
<wire x1="1" y1="-0.25" x2="1.5" y2="-0.25" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.25" x2="2" y2="-0.25" width="0.127" layer="51"/>
<wire x1="2" y1="-0.25" x2="1.5" y2="0.25" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.25" x2="1" y2="-0.25" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.25" x2="1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.25" x2="1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="-0.4445" y1="-0.1905" x2="0.4445" y2="-0.1905" width="0.127" layer="21"/>
<wire x1="0.4445" y1="-0.1905" x2="0" y2="0.1905" width="0.127" layer="21"/>
<wire x1="0" y1="0.1905" x2="-0.4445" y2="-0.1905" width="0.127" layer="21"/>
<wire x1="-0.4445" y1="0.1905" x2="0.4445" y2="0.1905" width="0.127" layer="21"/>
<smd name="K" x="0" y="0.8" dx="0.8" dy="0.6" layer="1"/>
<smd name="A" x="0" y="-0.8" dx="0.8" dy="0.6" layer="1"/>
<text x="1.016" y="1.016" size="0.4064" layer="25">&gt;NAME</text>
<text x="1.016" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4" y1="-0.6" x2="0.4" y2="0.6" layer="51"/>
<rectangle x1="-0.15" y1="-0.8" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.8" layer="51"/>
</package>
<package name="SOD-123">
<wire x1="-1" y1="0.7" x2="1" y2="0.7" width="0.1524" layer="51"/>
<wire x1="1" y1="0.7" x2="1" y2="-0.7" width="0.1524" layer="51"/>
<wire x1="1" y1="-0.7" x2="-1" y2="-0.7" width="0.1524" layer="51"/>
<wire x1="-1" y1="-0.7" x2="-1" y2="0.7" width="0.1524" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.1" y2="0.4" width="0.1524" layer="51"/>
<wire x1="0.1" y1="0.4" x2="0.1" y2="-0.4" width="0.1524" layer="51"/>
<wire x1="0.1" y1="-0.4" x2="-0.5" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.762" x2="1.778" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.778" y1="0.762" x2="1.778" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.778" y1="-0.762" x2="-1.778" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-0.762" x2="-1.778" y2="0.762" width="0.127" layer="21"/>
<smd name="C" x="-1.85" y="0" dx="1.2" dy="0.7" layer="1"/>
<smd name="A" x="1.85" y="0" dx="1.2" dy="0.7" layer="1"/>
<text x="-1.1" y="1" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.1" y="-1.284" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.7" y1="-0.7" x2="-0.5" y2="0.7" layer="51"/>
</package>
<package name="SOD-323F">
<wire x1="-0.85" y1="0.65" x2="0.85" y2="0.65" width="0.127" layer="21"/>
<wire x1="0.85" y1="0.65" x2="0.85" y2="-0.65" width="0.127" layer="21"/>
<wire x1="0.85" y1="-0.65" x2="-0.85" y2="-0.65" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-0.65" x2="-0.85" y2="0.65" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.6" x2="0.4" y2="-0.6" width="0.127" layer="21"/>
<wire x1="0.4" y1="-0.6" x2="0.3" y2="-0.6" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.6" x2="0.3" y2="0.6" width="0.127" layer="21"/>
<wire x1="-0.9" y1="0.2" x2="-1.2" y2="0.2" width="0.127" layer="51"/>
<wire x1="-1.2" y1="0.2" x2="-1.2" y2="-0.2" width="0.127" layer="51"/>
<wire x1="-1.2" y1="-0.2" x2="-0.9" y2="-0.2" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.2" x2="1.2" y2="0.2" width="0.127" layer="51"/>
<wire x1="1.2" y1="0.2" x2="1.2" y2="-0.2" width="0.127" layer="51"/>
<wire x1="1.2" y1="-0.2" x2="0.9" y2="-0.2" width="0.127" layer="51"/>
<smd name="A" x="-1" y="0" dx="1" dy="0.8" layer="1"/>
<smd name="C" x="1" y="0" dx="1" dy="0.8" layer="1"/>
<text x="-1.8" y="0.9" size="0.8128" layer="25" font="vector">&gt;NAME</text>
<text x="-2.1" y="-1.7" size="0.8128" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="SOD-123FL">
<wire x1="-0.5" y1="0" x2="0.5" y2="0.4" width="0.1524" layer="21"/>
<wire x1="0.5" y1="0.4" x2="0.5" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="0.5" y1="-0.4" x2="-0.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.35" y1="0.825" x2="1.35" y2="0.825" width="0.127" layer="21"/>
<wire x1="1.35" y1="0.825" x2="1.35" y2="-0.825" width="0.127" layer="21"/>
<wire x1="1.35" y1="-0.825" x2="-1.35" y2="-0.825" width="0.127" layer="21"/>
<wire x1="-1.35" y1="-0.825" x2="-1.35" y2="0.825" width="0.127" layer="21"/>
<smd name="C" x="-1.6375" y="0" dx="0.91" dy="1.22" layer="1"/>
<smd name="A" x="1.6375" y="0" dx="0.91" dy="1.22" layer="1"/>
<text x="-1.1" y="1" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.1" y="-1.284" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.7" y1="-0.7" x2="-0.5" y2="0.7" layer="21"/>
</package>
<package name="HALFHEIGHTSD">
<wire x1="-13.2" y1="-8.64" x2="-13.2" y2="10.16" width="0.127" layer="21"/>
<wire x1="-13.2" y1="10.16" x2="10.6" y2="10.16" width="0.127" layer="21"/>
<wire x1="10.6" y1="10.16" x2="10.6" y2="7.66" width="0.127" layer="21"/>
<wire x1="10.6" y1="7.66" x2="13.2" y2="7.66" width="0.127" layer="21"/>
<wire x1="13.2" y1="7.66" x2="13.3" y2="-8.64" width="0.127" layer="21"/>
<wire x1="13.3" y1="-8.64" x2="12.2" y2="-8.64" width="0.127" layer="21"/>
<wire x1="12.2" y1="-8.64" x2="-12.4" y2="-8.64" width="0.127" layer="21"/>
<wire x1="-12.4" y1="-8.64" x2="-13.2" y2="-8.64" width="0.127" layer="21"/>
<wire x1="-12.4" y1="-8.64" x2="12.4" y2="-8.84" width="0.127" layer="21" curve="-27.095878"/>
<wire x1="12.4" y1="-8.84" x2="12.2" y2="-8.64" width="0.127" layer="21"/>
<smd name="GP" x="-13.8" y="-5.34" dx="2.8" dy="2" layer="1" rot="R90"/>
<smd name="GP2" x="14.1" y="-5.34" dx="2.8" dy="2" layer="1" rot="R90"/>
<smd name="GP4" x="-13.9" y="9.16" dx="2.8" dy="2" layer="1" rot="R90"/>
<smd name="WP" x="-12.2" y="11.06" dx="4" dy="0.7" layer="1" rot="R90"/>
<smd name="8" x="-9.7" y="11.06" dx="4" dy="0.7" layer="1" rot="R90"/>
<smd name="7" x="-8.1" y="11.06" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="6" x="-5.6" y="11.06" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="5" x="-3.1" y="11.06" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="4.4" y="11.06" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="4" x="-0.6" y="11.06" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="1" x="6.9" y="11.06" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="1.9" y="11.06" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="9" x="9.4" y="11.06" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="CD" x="-11" y="11.06" dx="4" dy="0.7" layer="1" rot="R90"/>
<smd name="GP3" x="11.7" y="9.26" dx="2.8" dy="2" layer="1" rot="R90"/>
<hole x="-11.6" y="8.06" drill="1.1"/>
<hole x="9.5" y="8.06" drill="1.6"/>
<circle x="0" y="-0.05" radius="0.820059375" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SD-MMC">
<wire x1="-10.16" y1="22.86" x2="-10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-20.32" x2="-7.62" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-22.86" x2="15.24" y2="-22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="-22.86" x2="15.24" y2="22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="22.86" x2="-10.16" y2="22.86" width="0.254" layer="94"/>
<text x="-2.54" y="-7.62" size="2.1844" layer="94">SD &amp; MMC</text>
<pin name="CS" x="-12.7" y="2.54" length="short" direction="in"/>
<pin name="DATA_IN" x="-12.7" y="7.62" length="short" direction="in"/>
<pin name="VSS1" x="-12.7" y="-10.16" length="short" direction="sup"/>
<pin name="VDD" x="-12.7" y="-7.62" length="short" direction="sup"/>
<pin name="SCLK" x="-12.7" y="5.08" length="short" direction="in"/>
<pin name="VSS2" x="-12.7" y="-12.7" length="short" direction="sup"/>
<pin name="DATA_OUT" x="-12.7" y="10.16" length="short" direction="out"/>
<pin name="DAT1" x="-12.7" y="0" length="short"/>
<pin name="DAT2" x="-12.7" y="-2.54" length="short"/>
<pin name="WRITE_PROTECT" x="-12.7" y="15.24" length="short" direction="pas"/>
<pin name="CARD_DETECT" x="-12.7" y="20.32" length="short" direction="pas"/>
<pin name="COMMON_SW" x="-12.7" y="17.78" length="short" direction="pas"/>
<pin name="GND" x="-12.7" y="-17.78" length="short" direction="sup"/>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SDMMC">
<description>&lt;b&gt;SD MMC Card holder&lt;/b&gt;
&lt;p&gt;
4UCON part #06132 - easy to solder</description>
<gates>
<gate name="G$1" symbol="SD-MMC" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SCDA" package="SCDA">
<connects>
<connect gate="G$1" pin="CARD_DETECT" pad="10"/>
<connect gate="G$1" pin="COMMON_SW" pad="11"/>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="DAT1" pad="8"/>
<connect gate="G$1" pin="DAT2" pad="9"/>
<connect gate="G$1" pin="DATA_IN" pad="2"/>
<connect gate="G$1" pin="DATA_OUT" pad="7"/>
<connect gate="G$1" pin="GND" pad="13"/>
<connect gate="G$1" pin="SCLK" pad="5"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS1" pad="3"/>
<connect gate="G$1" pin="VSS2" pad="6"/>
<connect gate="G$1" pin="WRITE_PROTECT" pad="12"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="18650" package="SDCARD">
<connects>
<connect gate="G$1" pin="CARD_DETECT" pad="CD"/>
<connect gate="G$1" pin="COMMON_SW" pad="COM"/>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="DAT1" pad="8"/>
<connect gate="G$1" pin="DAT2" pad="9"/>
<connect gate="G$1" pin="DATA_IN" pad="2"/>
<connect gate="G$1" pin="DATA_OUT" pad="7"/>
<connect gate="G$1" pin="GND" pad="MT1"/>
<connect gate="G$1" pin="SCLK" pad="5"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS1" pad="3"/>
<connect gate="G$1" pin="VSS2" pad="6"/>
<connect gate="G$1" pin="WRITE_PROTECT" pad="WP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="06132" package="06132">
<connects>
<connect gate="G$1" pin="CARD_DETECT" pad="CD"/>
<connect gate="G$1" pin="COMMON_SW" pad="GP3"/>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="DAT1" pad="8"/>
<connect gate="G$1" pin="DAT2" pad="9"/>
<connect gate="G$1" pin="DATA_IN" pad="2"/>
<connect gate="G$1" pin="DATA_OUT" pad="7"/>
<connect gate="G$1" pin="GND" pad="GP"/>
<connect gate="G$1" pin="SCLK" pad="5"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS1" pad="3"/>
<connect gate="G$1" pin="VSS2" pad="6"/>
<connect gate="G$1" pin="WRITE_PROTECT" pad="WP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HALFHEIGHT" package="HALFHEIGHTSD">
<connects>
<connect gate="G$1" pin="CARD_DETECT" pad="CD"/>
<connect gate="G$1" pin="COMMON_SW" pad="GP3"/>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="DAT1" pad="8"/>
<connect gate="G$1" pin="DAT2" pad="9"/>
<connect gate="G$1" pin="DATA_IN" pad="2"/>
<connect gate="G$1" pin="DATA_OUT" pad="7"/>
<connect gate="G$1" pin="GND" pad="GP"/>
<connect gate="G$1" pin="SCLK" pad="5"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS1" pad="3"/>
<connect gate="G$1" pin="VSS2" pad="6"/>
<connect gate="G$1" pin="WRITE_PROTECT" pad="WP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;b&gt;Diode&lt;/b&gt;
&lt;p&gt;
&lt;b&gt;SMADIODE&lt;/b&gt; - SMA Surface Mount Package
&lt;ul&gt;
&lt;li&gt;20V 1A Schottky Diode Digikey: 641-1014-6-ND&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;DO-1N4148&lt;/b&gt; - Through Hole Small Current Diode&lt;br&gt;
&lt;b&gt;SOD-123&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;20V 1 A (.5mV Vf) Schottky Diode - Digikey: MBRX120TPMSCT-ND&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;SOD-323&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;10V 570mA (.38mV Vf, 3ns) Schottky Diode - Digikey: ZLLS410CT-ND&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;SOD-523&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;30V 30mA Schottky Diode (RB751S-40TE61) - Digikey: RB751S-40TE61CT-ND&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;SOT23-R/W&lt;/b&gt; - SOT23 Package (R = Solder Paste/Reflow Ovens, W = Hand-Soldering)
&lt;ul&gt;
&lt;li&gt;BAT54Film 40V 300mA - Digikey: 497-7162-1-ND&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="SMADIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO-1N4148" package="DO-1N4148">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23_REFLOW" package="SOT23-R">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23_WAVE" package="SOT23-W">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-523" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-123" package="SOD-123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-323F" package="SOD-323F">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SOD-123FL" package="SOD-123FL">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-yamaichi">
<description>&lt;b&gt;Yamaichi Connectors&lt;/b&gt;&lt;p&gt;
Mini-DIN, USB, Fire Wire&lt;br&gt;
DIN female/male, etc.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MDIN06SS">
<description>&lt;b&gt;Mini Din&lt;/b&gt; YAMAICHI CONNECTOR</description>
<wire x1="-7.5" y1="0.1" x2="7.5" y2="0.1" width="0.2032" layer="21"/>
<wire x1="7.5" y1="0.1" x2="7.5" y2="12.9" width="0.2032" layer="21"/>
<wire x1="7.5" y1="12.9" x2="-7.5" y2="12.9" width="0.2032" layer="21"/>
<wire x1="-7.5" y1="12.9" x2="-7.5" y2="0.1" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="0.5" x2="-5.5" y2="1" width="0.2032" layer="27"/>
<wire x1="-5.5" y1="1.5" x2="-5.5" y2="2" width="0.2032" layer="27"/>
<wire x1="-5.5" y1="2.5" x2="-5" y2="2.5" width="0.2032" layer="27"/>
<wire x1="-4.5" y1="2.5" x2="-4" y2="2.5" width="0.2032" layer="27"/>
<wire x1="-3.5" y1="2.5" x2="-3" y2="2.5" width="0.2032" layer="27"/>
<wire x1="-2.5" y1="2.5" x2="-2" y2="2.5" width="0.2032" layer="27"/>
<wire x1="-1.5" y1="2.5" x2="-1" y2="2.5" width="0.2032" layer="27"/>
<wire x1="1" y1="2.5" x2="1.5" y2="2.5" width="0.2032" layer="27"/>
<wire x1="2" y1="2.5" x2="2.5" y2="2.5" width="0.2032" layer="27"/>
<wire x1="3" y1="2.5" x2="3.5" y2="2.5" width="0.2032" layer="27"/>
<wire x1="4" y1="2.5" x2="4.5" y2="2.5" width="0.2032" layer="27"/>
<wire x1="5.5" y1="0.5" x2="5.5" y2="1" width="0.2032" layer="27"/>
<wire x1="5.5" y1="1.5" x2="5.5" y2="2" width="0.2032" layer="27"/>
<wire x1="5" y1="2.5" x2="5.5" y2="2.5" width="0.2032" layer="27"/>
<wire x1="-0.75" y1="0.1" x2="-0.75" y2="1.5" width="0.2032" layer="27"/>
<wire x1="-0.75" y1="1.5" x2="0.7" y2="1.5" width="0.2032" layer="27"/>
<wire x1="0.7" y1="1.5" x2="0.7" y2="0.1" width="0.2032" layer="27"/>
<pad name="1" x="1.3" y="8.5" drill="0.9" shape="octagon"/>
<pad name="2" x="-1.3" y="8.5" drill="0.9" shape="octagon"/>
<pad name="3" x="3.4" y="8.5" drill="0.9" shape="octagon"/>
<pad name="4" x="-3.4" y="8.5" drill="0.9" shape="octagon"/>
<pad name="6" x="-3.4" y="11" drill="0.9" shape="octagon"/>
<pad name="5" x="3.4" y="11" drill="0.9" shape="octagon"/>
<text x="-5.08" y="6.35" size="1.27" layer="27">&gt;VALUE</text>
<text x="-5.08" y="13.1" size="1.27" layer="25">&gt;NAME</text>
<pad name="P$1" x="-6.75" y="5.51" drill="2.2"/>
<pad name="P$2" x="-0.01" y="4.7" drill="2.2"/>
<pad name="P$3" x="6.75" y="5.52" drill="2.2"/>
</package>
<package name="MDIN04SS">
<description>&lt;b&gt;Mini Din&lt;/b&gt; YAMAICHI CONNECTOR</description>
<wire x1="-7.5" y1="0.1" x2="7.5" y2="0.1" width="0.2032" layer="21"/>
<wire x1="7.5" y1="0.1" x2="7.5" y2="12.9" width="0.2032" layer="21"/>
<wire x1="7.5" y1="12.9" x2="-7.5" y2="12.9" width="0.2032" layer="21"/>
<wire x1="-7.5" y1="12.9" x2="-7.5" y2="0.1" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="0.5" x2="-5.5" y2="1" width="0.2032" layer="27"/>
<wire x1="-5.5" y1="1.5" x2="-5.5" y2="2" width="0.2032" layer="27"/>
<wire x1="-5.5" y1="2.5" x2="-5" y2="2.5" width="0.2032" layer="27"/>
<wire x1="-4.5" y1="2.5" x2="-4" y2="2.5" width="0.2032" layer="27"/>
<wire x1="-3.5" y1="2.5" x2="-3" y2="2.5" width="0.2032" layer="27"/>
<wire x1="-2.5" y1="2.5" x2="-2" y2="2.5" width="0.2032" layer="27"/>
<wire x1="-1.5" y1="2.5" x2="-1" y2="2.5" width="0.2032" layer="27"/>
<wire x1="1" y1="2.5" x2="1.5" y2="2.5" width="0.2032" layer="27"/>
<wire x1="2" y1="2.5" x2="2.5" y2="2.5" width="0.2032" layer="27"/>
<wire x1="3" y1="2.5" x2="3.5" y2="2.5" width="0.2032" layer="27"/>
<wire x1="4" y1="2.5" x2="4.5" y2="2.5" width="0.2032" layer="27"/>
<wire x1="5.5" y1="0.5" x2="5.5" y2="1" width="0.2032" layer="27"/>
<wire x1="5.5" y1="1.5" x2="5.5" y2="2" width="0.2032" layer="27"/>
<wire x1="5" y1="2.5" x2="5.5" y2="2.5" width="0.2032" layer="27"/>
<wire x1="-0.75" y1="0.1" x2="-0.75" y2="1.5" width="0.2032" layer="27"/>
<wire x1="-0.75" y1="1.5" x2="0.7" y2="1.5" width="0.2032" layer="27"/>
<wire x1="0.7" y1="1.5" x2="0.7" y2="0.1" width="0.2032" layer="27"/>
<pad name="1" x="3.4" y="8.5" drill="0.9" shape="octagon"/>
<pad name="2" x="-3.4" y="8.5" drill="0.9" shape="octagon"/>
<pad name="4" x="-3.4" y="11" drill="0.9" shape="octagon"/>
<pad name="3" x="3.4" y="11" drill="0.9" shape="octagon"/>
<text x="-5.08" y="6.35" size="1.27" layer="27">&gt;VALUE</text>
<text x="-5.08" y="13.1" size="1.27" layer="25">&gt;NAME</text>
<pad name="P$1" x="-6.74" y="5.5" drill="2.2"/>
<pad name="P$2" x="0" y="4.71" drill="2.2"/>
<pad name="P$3" x="6.74" y="5.5" drill="2.2"/>
</package>
</packages>
<symbols>
<symbol name="MDIN06">
<wire x1="6.35" y1="-1.778" x2="6.35" y2="1.778" width="0.127" layer="94"/>
<wire x1="6.35" y1="1.778" x2="8.636" y2="1.778" width="0.127" layer="94"/>
<wire x1="-3.302" y1="6.096" x2="-3.302" y2="4.572" width="0.127" layer="94"/>
<wire x1="-3.302" y1="4.572" x2="-4.826" y2="4.572" width="0.127" layer="94"/>
<wire x1="-4.826" y1="-4.572" x2="-3.302" y2="-4.572" width="0.127" layer="94"/>
<wire x1="-3.302" y1="-4.572" x2="-3.302" y2="-6.096" width="0.127" layer="94"/>
<wire x1="-4.826" y1="4.572" x2="-4.826" y2="-4.572" width="0.127" layer="94" curve="73.739795"/>
<wire x1="-3.302" y1="6.096" x2="8.6359" y2="1.778" width="0.127" layer="94" curve="-113.18391"/>
<wire x1="1.016" y1="-1.524" x2="1.016" y2="1.524" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.524" x2="5.08" y2="1.524" width="0.127" layer="94"/>
<wire x1="5.08" y1="1.524" x2="5.08" y2="-1.524" width="0.127" layer="94"/>
<wire x1="5.08" y1="-1.524" x2="1.016" y2="-1.524" width="0.127" layer="94"/>
<wire x1="1.524" y1="3.556" x2="-0.254" y2="3.556" width="0.254" layer="94"/>
<wire x1="-0.254" y1="3.556" x2="-0.254" y2="5.588" width="0.254" layer="94"/>
<wire x1="-0.254" y1="5.588" x2="1.524" y2="5.588" width="0.254" layer="94"/>
<wire x1="1.524" y1="5.588" x2="1.524" y2="3.556" width="0.254" layer="94"/>
<wire x1="1.524" y1="-5.588" x2="-0.254" y2="-5.588" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-5.588" x2="-0.254" y2="-3.556" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-3.556" x2="1.524" y2="-3.556" width="0.254" layer="94"/>
<wire x1="1.524" y1="-3.556" x2="1.524" y2="-5.588" width="0.254" layer="94"/>
<wire x1="5.334" y1="3.048" x2="3.302" y2="3.048" width="0.254" layer="94"/>
<wire x1="3.302" y1="3.048" x2="3.302" y2="4.826" width="0.254" layer="94"/>
<wire x1="3.302" y1="4.826" x2="5.334" y2="4.826" width="0.254" layer="94"/>
<wire x1="5.334" y1="4.826" x2="5.334" y2="3.048" width="0.254" layer="94"/>
<wire x1="5.334" y1="-4.572" x2="3.302" y2="-4.572" width="0.254" layer="94"/>
<wire x1="3.302" y1="-4.572" x2="3.302" y2="-2.794" width="0.254" layer="94"/>
<wire x1="3.302" y1="-2.794" x2="5.334" y2="-2.794" width="0.254" layer="94"/>
<wire x1="5.334" y1="-2.794" x2="5.334" y2="-4.572" width="0.254" layer="94"/>
<wire x1="-2.032" y1="1.016" x2="-3.81" y2="1.016" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.016" x2="-3.81" y2="3.048" width="0.254" layer="94"/>
<wire x1="-3.81" y1="3.048" x2="-2.032" y2="3.048" width="0.254" layer="94"/>
<wire x1="-2.032" y1="3.048" x2="-2.032" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-3.048" x2="-3.81" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-3.048" x2="-3.81" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.016" x2="-2.032" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-5.842" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-5.842" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-5.334" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-5.334" y1="5.08" x2="-4.826" y2="4.572" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-5.334" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-5.334" y1="-5.08" x2="-4.826" y2="-4.572" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="-4.826" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-4.826" y1="7.62" x2="-3.302" y2="6.096" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-1.778" x2="8.636" y2="-1.778" width="0.127" layer="94"/>
<wire x1="-3.302" y1="-6.096" x2="8.6359" y2="-1.778" width="0.127" layer="94" curve="113.18391"/>
<text x="-1.27" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.27" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-10.16" y="7.62" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-10.16" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="4" x="-10.16" y="0" visible="pad" length="short" direction="pas"/>
<pin name="5" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="6" x="-10.16" y="-5.08" visible="pad" length="short" direction="pas"/>
</symbol>
<symbol name="MDIN04">
<wire x1="6.35" y1="-0.508" x2="8.636" y2="-0.508" width="0.127" layer="94"/>
<wire x1="6.35" y1="-0.508" x2="6.35" y2="3.048" width="0.127" layer="94"/>
<wire x1="6.35" y1="3.048" x2="8.636" y2="3.048" width="0.127" layer="94"/>
<wire x1="-3.302" y1="7.366" x2="-3.302" y2="5.842" width="0.127" layer="94"/>
<wire x1="-3.302" y1="5.842" x2="-4.826" y2="5.842" width="0.127" layer="94"/>
<wire x1="-4.826" y1="-3.302" x2="-3.302" y2="-3.302" width="0.127" layer="94"/>
<wire x1="-3.302" y1="-3.302" x2="-3.302" y2="-4.826" width="0.127" layer="94"/>
<wire x1="-4.826" y1="5.842" x2="-4.826" y2="-3.302" width="0.127" layer="94" curve="73.739795"/>
<wire x1="-3.302" y1="7.366" x2="8.6359" y2="3.048" width="0.127" layer="94" curve="-113.18391"/>
<wire x1="-1.778" y1="-0.762" x2="-4.826" y2="-0.762" width="0.127" layer="94"/>
<wire x1="-4.826" y1="-0.762" x2="-4.826" y2="3.302" width="0.127" layer="94"/>
<wire x1="-4.826" y1="3.302" x2="-1.778" y2="3.302" width="0.127" layer="94"/>
<wire x1="-1.778" y1="3.302" x2="-1.778" y2="-0.762" width="0.127" layer="94"/>
<wire x1="1.524" y1="4.826" x2="-0.254" y2="4.826" width="0.254" layer="94"/>
<wire x1="-0.254" y1="4.826" x2="-0.254" y2="6.858" width="0.254" layer="94"/>
<wire x1="-0.254" y1="6.858" x2="1.524" y2="6.858" width="0.254" layer="94"/>
<wire x1="1.524" y1="6.858" x2="1.524" y2="4.826" width="0.254" layer="94"/>
<wire x1="1.524" y1="-4.318" x2="-0.254" y2="-4.318" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-4.318" x2="-0.254" y2="-2.286" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-2.286" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.524" y2="-4.318" width="0.254" layer="94"/>
<wire x1="5.334" y1="4.318" x2="3.302" y2="4.318" width="0.254" layer="94"/>
<wire x1="3.302" y1="4.318" x2="3.302" y2="6.096" width="0.254" layer="94"/>
<wire x1="3.302" y1="6.096" x2="5.334" y2="6.096" width="0.254" layer="94"/>
<wire x1="5.334" y1="6.096" x2="5.334" y2="4.318" width="0.254" layer="94"/>
<wire x1="5.334" y1="-3.302" x2="3.302" y2="-3.302" width="0.254" layer="94"/>
<wire x1="3.302" y1="-3.302" x2="3.302" y2="-1.524" width="0.254" layer="94"/>
<wire x1="3.302" y1="-1.524" x2="5.334" y2="-1.524" width="0.254" layer="94"/>
<wire x1="5.334" y1="-1.524" x2="5.334" y2="-3.302" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-6.35" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-5.334" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-5.334" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-5.334" y1="5.08" x2="-4.826" y2="5.842" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="-4.826" x2="8.6359" y2="-0.508" width="0.127" layer="94" curve="113.18391"/>
<text x="-1.27" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.27" y="-8.89" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-10.16" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-10.16" y="0" visible="pad" length="short" direction="pas"/>
<pin name="4" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MD06SS" prefix="X" uservalue="yes">
<description>&lt;b&gt;YAMAICHI CONNECTOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MDIN06" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MDIN06SS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MD04SS" prefix="X" uservalue="yes">
<description>&lt;b&gt;YAMAICHI CONNECTOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MDIN04" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MDIN04SS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="de1_parts">
<packages>
<package name="DIL-100-2X20">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 0.1"</description>
<wire x1="-25.4" y1="-2.54" x2="-25.4" y2="0" width="0.254" layer="21"/>
<wire x1="-25.4" y1="0" x2="-25.4" y2="2.54" width="0.254" layer="21"/>
<wire x1="-25.4" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="21"/>
<wire x1="25.4" y1="-2.54" x2="-22.86" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-22.86" y1="-2.54" x2="-25.4" y2="-2.54" width="0.254" layer="21"/>
<wire x1="25.4" y1="2.54" x2="25.4" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-22.86" y1="0" x2="-25.4" y2="0" width="0.254" layer="21"/>
<wire x1="-22.86" y1="0" x2="-22.86" y2="-2.54" width="0.254" layer="21"/>
<pad name="1" x="-24.13" y="-1.27" drill="1.016" shape="square"/>
<pad name="2" x="-24.13" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-21.59" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-21.59" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="-16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="-16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="-13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="-13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="-11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="-11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="-8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="14" x="-8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="-6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="16" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="17" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="18" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="19" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="20" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="21" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="22" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="23" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="24" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="25" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="26" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="27" x="8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="28" x="8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="29" x="11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="30" x="11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="31" x="13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="32" x="13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="33" x="16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="34" x="16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="35" x="19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="36" x="19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="37" x="21.59" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="38" x="21.59" y="1.27" drill="1.016" shape="octagon"/>
<pad name="39" x="24.13" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="40" x="24.13" y="1.27" drill="1.016" shape="octagon"/>
<text x="-29.872" y="-2.517" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="30.552" y="-1.982" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-24.384" y1="-1.524" x2="-23.876" y2="-1.016" layer="51"/>
<rectangle x1="-24.384" y1="1.016" x2="-23.876" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="1.016" x2="-21.336" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="-1.524" x2="-21.336" y2="-1.016" layer="51"/>
<rectangle x1="-19.304" y1="1.016" x2="-18.796" y2="1.524" layer="51"/>
<rectangle x1="-19.304" y1="-1.524" x2="-18.796" y2="-1.016" layer="51"/>
<rectangle x1="-16.764" y1="1.016" x2="-16.256" y2="1.524" layer="51"/>
<rectangle x1="-14.224" y1="1.016" x2="-13.716" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-16.764" y1="-1.524" x2="-16.256" y2="-1.016" layer="51"/>
<rectangle x1="-14.224" y1="-1.524" x2="-13.716" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="13.716" y1="1.016" x2="14.224" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="13.716" y1="-1.524" x2="14.224" y2="-1.016" layer="51"/>
<rectangle x1="16.256" y1="1.016" x2="16.764" y2="1.524" layer="51"/>
<rectangle x1="16.256" y1="-1.524" x2="16.764" y2="-1.016" layer="51"/>
<rectangle x1="18.796" y1="1.016" x2="19.304" y2="1.524" layer="51"/>
<rectangle x1="18.796" y1="-1.524" x2="19.304" y2="-1.016" layer="51"/>
<rectangle x1="21.336" y1="1.016" x2="21.844" y2="1.524" layer="51"/>
<rectangle x1="21.336" y1="-1.524" x2="21.844" y2="-1.016" layer="51"/>
<rectangle x1="23.876" y1="1.016" x2="24.384" y2="1.524" layer="51"/>
<rectangle x1="23.876" y1="-1.524" x2="24.384" y2="-1.016" layer="51"/>
<wire x1="-28.77" y1="4.295" x2="28.77" y2="4.295" width="0.127" layer="21" style="shortdash"/>
<wire x1="28.77" y1="-4.295" x2="-28.77" y2="-4.295" width="0.127" layer="21" style="shortdash"/>
<wire x1="-28.801" y1="4.332" x2="-28.823" y2="4.332" width="0.127" layer="21"/>
<wire x1="-28.823" y1="4.332" x2="-28.823" y2="-4.309" width="0.127" layer="21" style="shortdash"/>
<wire x1="28.711" y1="-4.265" x2="28.778" y2="-4.265" width="0.127" layer="21"/>
<wire x1="28.778" y1="-4.265" x2="28.778" y2="4.332" width="0.127" layer="21" style="shortdash"/>
</package>
</packages>
<symbols>
<symbol name="DE1_GPIO">
<wire x1="-7.62" y1="60.96" x2="5.08" y2="60.96" width="0.254" layer="94"/>
<wire x1="5.08" y1="60.96" x2="5.08" y2="-45.72" width="0.254" layer="94"/>
<wire x1="5.08" y1="-45.72" x2="-7.62" y2="-45.72" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-45.72" x2="-7.62" y2="60.96" width="0.254" layer="94"/>
<pin name="5V0" x="10.16" y="58.42" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="3V3" x="10.16" y="55.88" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@1" x="10.16" y="53.34" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@2" x="10.16" y="50.8" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="GPIO_0" x="10.16" y="45.72" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_1" x="10.16" y="43.18" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_2" x="10.16" y="40.64" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_3" x="10.16" y="38.1" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_4" x="10.16" y="35.56" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_5" x="10.16" y="33.02" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_6" x="10.16" y="30.48" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_7" x="10.16" y="27.94" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_8" x="10.16" y="25.4" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_9" x="10.16" y="22.86" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_10" x="10.16" y="20.32" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_11" x="10.16" y="17.78" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_12" x="10.16" y="15.24" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_13" x="10.16" y="12.7" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_14" x="10.16" y="10.16" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_15" x="10.16" y="7.62" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_16" x="10.16" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_17" x="10.16" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_18" x="10.16" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_19" x="10.16" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_20" x="10.16" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_21" x="10.16" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_22" x="10.16" y="-10.16" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_23" x="10.16" y="-12.7" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_24" x="10.16" y="-15.24" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_25" x="10.16" y="-17.78" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_26" x="10.16" y="-20.32" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_27" x="10.16" y="-22.86" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_28" x="10.16" y="-25.4" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_29" x="10.16" y="-27.94" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_30" x="10.16" y="-30.48" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_31" x="10.16" y="-33.02" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_32" x="10.16" y="-35.56" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_33" x="10.16" y="-38.1" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_34" x="10.16" y="-40.64" visible="pin" length="middle" rot="R180"/>
<pin name="GPIO_35" x="10.16" y="-43.18" visible="pin" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DE1_GPIO">
<gates>
<gate name="G$1" symbol="DE1_GPIO" x="2.54" y="-7.62" addlevel="must"/>
</gates>
<devices>
<device name="" package="DIL-100-2X20">
<connects>
<connect gate="G$1" pin="3V3" pad="29"/>
<connect gate="G$1" pin="5V0" pad="11"/>
<connect gate="G$1" pin="GND@1" pad="12"/>
<connect gate="G$1" pin="GND@2" pad="30"/>
<connect gate="G$1" pin="GPIO_0" pad="1"/>
<connect gate="G$1" pin="GPIO_1" pad="2"/>
<connect gate="G$1" pin="GPIO_10" pad="13"/>
<connect gate="G$1" pin="GPIO_11" pad="14"/>
<connect gate="G$1" pin="GPIO_12" pad="15"/>
<connect gate="G$1" pin="GPIO_13" pad="16"/>
<connect gate="G$1" pin="GPIO_14" pad="17"/>
<connect gate="G$1" pin="GPIO_15" pad="18"/>
<connect gate="G$1" pin="GPIO_16" pad="19"/>
<connect gate="G$1" pin="GPIO_17" pad="20"/>
<connect gate="G$1" pin="GPIO_18" pad="21"/>
<connect gate="G$1" pin="GPIO_19" pad="22"/>
<connect gate="G$1" pin="GPIO_2" pad="3"/>
<connect gate="G$1" pin="GPIO_20" pad="23"/>
<connect gate="G$1" pin="GPIO_21" pad="24"/>
<connect gate="G$1" pin="GPIO_22" pad="25"/>
<connect gate="G$1" pin="GPIO_23" pad="26"/>
<connect gate="G$1" pin="GPIO_24" pad="27"/>
<connect gate="G$1" pin="GPIO_25" pad="28"/>
<connect gate="G$1" pin="GPIO_26" pad="31"/>
<connect gate="G$1" pin="GPIO_27" pad="32"/>
<connect gate="G$1" pin="GPIO_28" pad="33"/>
<connect gate="G$1" pin="GPIO_29" pad="34"/>
<connect gate="G$1" pin="GPIO_3" pad="4"/>
<connect gate="G$1" pin="GPIO_30" pad="35"/>
<connect gate="G$1" pin="GPIO_31" pad="36"/>
<connect gate="G$1" pin="GPIO_32" pad="37"/>
<connect gate="G$1" pin="GPIO_33" pad="38"/>
<connect gate="G$1" pin="GPIO_34" pad="39"/>
<connect gate="G$1" pin="GPIO_35" pad="40"/>
<connect gate="G$1" pin="GPIO_4" pad="5"/>
<connect gate="G$1" pin="GPIO_5" pad="6"/>
<connect gate="G$1" pin="GPIO_6" pad="7"/>
<connect gate="G$1" pin="GPIO_7" pad="8"/>
<connect gate="G$1" pin="GPIO_8" pad="9"/>
<connect gate="G$1" pin="GPIO_9" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor-array">
<description>&lt;b&gt;SMD Resistor Arrays&lt;/b&gt;&lt;p&gt;
CTS, Rohm, etc&lt;p&gt;
&lt;p&gt;THIS LIBRARY IS PROVIDED AS IS AND WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED.&lt;br&gt;
USE AT YOUR OWN RISK!&lt;p&gt;
&lt;author&gt;Copyright (C) 2008, Bob Starr&lt;br&gt; http://www.bobstarr.net&lt;br&gt;&lt;/author&gt;</description>
<packages>
<package name="742-8">
<description>CTS 742 Series&lt;p&gt;
8 Terminations, 4 Resistors</description>
<wire x1="-1.7463" y1="0.9525" x2="-1.7463" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="1.7463" y1="0.9525" x2="1.7463" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-1.7463" y1="0.9525" x2="1.7463" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="-1.7463" y1="-0.9525" x2="1.7463" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-1.7463" y1="1.6375" x2="1.7463" y2="1.6375" width="0.2032" layer="21"/>
<wire x1="1.7463" y1="1.6375" x2="1.7463" y2="-1.6375" width="0.2032" layer="21"/>
<wire x1="1.7463" y1="-1.6375" x2="-1.7463" y2="-1.6375" width="0.2032" layer="21"/>
<wire x1="-1.7463" y1="-1.6375" x2="-1.7463" y2="1.6375" width="0.2032" layer="21"/>
<circle x="-1.1875" y="-0.1263" radius="0.1797" width="0" layer="21"/>
<smd name="1" x="-1.2" y="-0.9" dx="0.5" dy="0.9" layer="1"/>
<smd name="2" x="-0.4" y="-0.9" dx="0.5" dy="0.9" layer="1"/>
<smd name="3" x="0.4" y="-0.9" dx="0.5" dy="0.9" layer="1"/>
<smd name="4" x="1.2" y="-0.9" dx="0.5" dy="0.9" layer="1"/>
<smd name="5" x="1.2" y="0.9" dx="0.5" dy="0.9" layer="1" rot="R180"/>
<smd name="6" x="0.4" y="0.9" dx="0.5" dy="0.9" layer="1" rot="R180"/>
<smd name="7" x="-0.4" y="0.9" dx="0.5" dy="0.9" layer="1" rot="R180"/>
<smd name="8" x="-1.2" y="0.9" dx="0.5" dy="0.9" layer="1" rot="R180"/>
<text x="-2.2225" y="-1.5875" size="0.8128" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="2.8575" y="-1.5875" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
<package name="742-8NS">
<description>CTS 742 Series&lt;p&gt;
8 Terminations, 4 Resistors</description>
<wire x1="-1.7463" y1="0.9525" x2="-1.7463" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="1.7463" y1="0.9525" x2="1.7463" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-1.7463" y1="0.9525" x2="1.7463" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="-1.7463" y1="-0.9525" x2="1.7463" y2="-0.9525" width="0.2032" layer="51"/>
<circle x="-1.1875" y="-0.1263" radius="0.1797" width="0" layer="21"/>
<smd name="1" x="-1.2" y="-0.9" dx="0.5" dy="0.9" layer="1"/>
<smd name="2" x="-0.4" y="-0.9" dx="0.5" dy="0.9" layer="1"/>
<smd name="3" x="0.4" y="-0.9" dx="0.5" dy="0.9" layer="1"/>
<smd name="4" x="1.2" y="-0.9" dx="0.5" dy="0.9" layer="1"/>
<smd name="5" x="1.2" y="0.9" dx="0.5" dy="0.9" layer="1" rot="R180"/>
<smd name="6" x="0.4" y="0.9" dx="0.5" dy="0.9" layer="1" rot="R180"/>
<smd name="7" x="-0.4" y="0.9" dx="0.5" dy="0.9" layer="1" rot="R180"/>
<smd name="8" x="-1.2" y="0.9" dx="0.5" dy="0.9" layer="1" rot="R180"/>
<text x="-2.2225" y="-1.5875" size="0.8128" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="2.8575" y="-1.5875" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
<package name="743-8">
<description>CTS 744 Series&lt;p&gt;
8 Terminations, 4 Resistors</description>
<wire x1="-2.54" y1="0.9525" x2="-2.54" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.9525" x2="2.54" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.9525" x2="2.54" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-2.59" y1="1.8256" x2="2.59" y2="1.8256" width="0.2032" layer="21"/>
<wire x1="2.59" y1="1.8256" x2="2.59" y2="-1.8256" width="0.2032" layer="21"/>
<wire x1="2.59" y1="-1.8256" x2="-2.59" y2="-1.8256" width="0.2032" layer="21"/>
<wire x1="-2.59" y1="-1.8256" x2="-2.59" y2="1.8256" width="0.2032" layer="21"/>
<circle x="-1.905" y="-0.0263" radius="0.1797" width="0" layer="21"/>
<smd name="8" x="-1.905" y="0.9525" dx="0.8" dy="1.2" layer="1"/>
<smd name="7" x="-0.635" y="0.9525" dx="0.8" dy="1.2" layer="1"/>
<smd name="1" x="-1.905" y="-0.9525" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="-0.635" y="-0.9525" dx="0.8" dy="1.2" layer="1"/>
<smd name="3" x="0.635" y="-0.9525" dx="0.8" dy="1.2" layer="1"/>
<smd name="4" x="1.905" y="-0.9525" dx="0.8" dy="1.2" layer="1"/>
<smd name="6" x="0.635" y="0.9525" dx="0.8" dy="1.2" layer="1"/>
<smd name="5" x="1.905" y="0.9525" dx="0.8" dy="1.2" layer="1"/>
<text x="-2.8575" y="-1.5875" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.5875" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="RN-8T4R">
<wire x1="2.54" y1="2.54" x2="2.2225" y2="1.5875" width="0.2032" layer="94"/>
<wire x1="2.2225" y1="1.5875" x2="1.5875" y2="3.4925" width="0.2032" layer="94"/>
<wire x1="1.5875" y1="3.4925" x2="0.9525" y2="1.5875" width="0.2032" layer="94"/>
<wire x1="0.9525" y1="1.5875" x2="0.3175" y2="3.4925" width="0.2032" layer="94"/>
<wire x1="0.3175" y1="3.4925" x2="-0.3175" y2="1.5875" width="0.2032" layer="94"/>
<wire x1="-0.3175" y1="1.5875" x2="-0.9525" y2="3.4925" width="0.2032" layer="94"/>
<wire x1="-0.9525" y1="3.4925" x2="-1.5875" y2="1.5875" width="0.2032" layer="94"/>
<wire x1="-1.5875" y1="1.5875" x2="-2.2225" y2="3.4925" width="0.2032" layer="94"/>
<wire x1="-2.2225" y1="3.4925" x2="-2.54" y2="2.54" width="0.2032" layer="94"/>
<wire x1="2.54" y1="0" x2="2.2225" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="2.2225" y1="-0.9525" x2="1.5875" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="1.5875" y1="0.9525" x2="0.9525" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="0.9525" y1="-0.9525" x2="0.3175" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="0.3175" y1="0.9525" x2="-0.3175" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="-0.3175" y1="-0.9525" x2="-0.9525" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="-0.9525" y1="0.9525" x2="-1.5875" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="-1.5875" y1="-0.9525" x2="-2.2225" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="-2.2225" y1="0.9525" x2="-2.54" y2="0" width="0.2032" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.2225" y2="-3.4925" width="0.2032" layer="94"/>
<wire x1="2.2225" y1="-3.4925" x2="1.5875" y2="-1.5875" width="0.2032" layer="94"/>
<wire x1="1.5875" y1="-1.5875" x2="0.9525" y2="-3.4925" width="0.2032" layer="94"/>
<wire x1="0.9525" y1="-3.4925" x2="0.3175" y2="-1.5875" width="0.2032" layer="94"/>
<wire x1="0.3175" y1="-1.5875" x2="-0.3175" y2="-3.4925" width="0.2032" layer="94"/>
<wire x1="-0.3175" y1="-3.4925" x2="-0.9525" y2="-1.5875" width="0.2032" layer="94"/>
<wire x1="-0.9525" y1="-1.5875" x2="-1.5875" y2="-3.4925" width="0.2032" layer="94"/>
<wire x1="-1.5875" y1="-3.4925" x2="-2.2225" y2="-1.5875" width="0.2032" layer="94"/>
<wire x1="-2.2225" y1="-1.5875" x2="-2.54" y2="-2.54" width="0.2032" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.2225" y2="-6.0325" width="0.2032" layer="94"/>
<wire x1="2.2225" y1="-6.0325" x2="1.5875" y2="-4.1275" width="0.2032" layer="94"/>
<wire x1="1.5875" y1="-4.1275" x2="0.9525" y2="-6.0325" width="0.2032" layer="94"/>
<wire x1="0.9525" y1="-6.0325" x2="0.3175" y2="-4.1275" width="0.2032" layer="94"/>
<wire x1="0.3175" y1="-4.1275" x2="-0.3175" y2="-6.0325" width="0.2032" layer="94"/>
<wire x1="-0.3175" y1="-6.0325" x2="-0.9525" y2="-4.1275" width="0.2032" layer="94"/>
<wire x1="-0.9525" y1="-4.1275" x2="-1.5875" y2="-6.0325" width="0.2032" layer="94"/>
<wire x1="-1.5875" y1="-6.0325" x2="-2.2225" y2="-4.1275" width="0.2032" layer="94"/>
<wire x1="-2.2225" y1="-4.1275" x2="-2.54" y2="-5.08" width="0.2032" layer="94"/>
<wire x1="-3.81" y1="4.445" x2="3.81" y2="4.445" width="0.254" layer="94"/>
<wire x1="3.81" y1="4.445" x2="3.81" y2="-6.985" width="0.254" layer="94"/>
<wire x1="3.81" y1="-6.985" x2="-3.81" y2="-6.985" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-6.985" x2="-3.81" y2="4.445" width="0.254" layer="94"/>
<text x="-3.81" y="-9.525" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.207" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="742-8" prefix="RN" uservalue="yes">
<description>&lt;b&gt;CTS 742 Series&lt;/b&gt;&lt;p&gt;
8 Terminations, 4 Resistors</description>
<gates>
<gate name="G$1" symbol="RN-8T4R" x="0" y="0"/>
</gates>
<devices>
<device name="" package="742-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NS" package="742-8NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="742_3" package="743-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="sdram">
<description>&lt;b&gt;SDRAM  memories&lt;/b&gt;

by Martin Hinner&lt;br&gt;
&lt;a href="http://martin.hinner.info/"&gt;http://martin.hinner.info/&lt;/a&gt;</description>
<packages>
<package name="TSOP54">
<description>TSOP 54 Pin, pitch 0.8 mm</description>
<wire x1="11.2" y1="5.2" x2="11.2" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="11.2" y1="-5.2" x2="-11.2" y2="-5.2" width="0.2032" layer="51"/>
<wire x1="-11.2" y1="-5.2" x2="-11.2" y2="5.2" width="0.2032" layer="21"/>
<wire x1="-11.2" y1="5.2" x2="11.2" y2="5.2" width="0.2032" layer="51"/>
<circle x="-10" y="-3.6" radius="0.3175" width="0" layer="21"/>
<smd name="1" x="-10.4" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="2" x="-9.6" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="3" x="-8.8" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="4" x="-8" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="5" x="-7.2" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="6" x="-6.4" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="7" x="-5.6" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="8" x="-4.8" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="9" x="-4" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="10" x="-3.2" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="11" x="-2.4" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="12" x="-1.6" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="13" x="-0.8" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="14" x="0" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="15" x="0.8" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="16" x="1.6" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="17" x="2.4" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="18" x="3.2" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="19" x="4" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="20" x="4.8" y="-5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="21" x="5.6" y="-5.45" dx="0.4" dy="1.6" layer="1" rot="R180"/>
<smd name="22" x="6.4" y="-5.45" dx="0.4" dy="1.6" layer="1" rot="R180"/>
<smd name="23" x="7.2" y="-5.45" dx="0.4" dy="1.6" layer="1" rot="R180"/>
<smd name="24" x="8" y="-5.45" dx="0.4" dy="1.6" layer="1" rot="R180"/>
<smd name="25" x="8.8" y="-5.45" dx="0.4" dy="1.6" layer="1" rot="R180"/>
<smd name="26" x="9.6" y="-5.45" dx="0.4" dy="1.6" layer="1" rot="R180"/>
<smd name="27" x="10.4" y="-5.45" dx="0.4" dy="1.6" layer="1" rot="R180"/>
<smd name="28" x="10.4" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="29" x="9.6" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="30" x="8.8" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="31" x="8" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="32" x="7.2" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="33" x="6.4" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="34" x="5.6" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="35" x="4.8" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="36" x="4" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="37" x="3.2" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="38" x="2.4" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="39" x="1.6" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="40" x="0.8" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="41" x="0" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="42" x="-0.8" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="43" x="-1.6" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="44" x="-2.4" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="45" x="-3.2" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="46" x="-4" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="47" x="-4.8" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="48" x="-5.6" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="49" x="-6.4" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="50" x="-7.2" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="51" x="-8" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="52" x="-8.8" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="53" x="-9.6" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<smd name="54" x="-10.4" y="5.45" dx="0.4" dy="1.6" layer="1"/>
<text x="-11.7475" y="-5.08" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="-2.2225" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="SDRAM16">
<wire x1="-10.16" y1="-43.18" x2="10.16" y2="-43.18" width="0.4064" layer="94"/>
<wire x1="10.16" y1="40.64" x2="10.16" y2="-43.18" width="0.4064" layer="94"/>
<wire x1="10.16" y1="40.64" x2="-10.16" y2="40.64" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-43.18" x2="-10.16" y2="40.64" width="0.4064" layer="94"/>
<text x="-10.16" y="41.275" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-45.72" size="1.778" layer="96">&gt;VALUE</text>
<text x="3.6513" y="-8.4138" size="1.524" layer="95">VDD</text>
<text x="2.2225" y="-16.0338" size="1.524" layer="95">VDDQ</text>
<text x="3.6513" y="-28.7338" size="1.524" layer="95">VSS</text>
<text x="2.2225" y="-36.3538" size="1.524" layer="95">VSSQ</text>
<text x="3.6513" y="-10.9538" size="1.524" layer="95">VDD</text>
<text x="2.2225" y="-18.5738" size="1.524" layer="95">VDDQ</text>
<text x="2.2225" y="-21.1138" size="1.524" layer="95">VDDQ</text>
<text x="3.6513" y="-31.2738" size="1.524" layer="95">VSS</text>
<text x="2.2225" y="-38.8938" size="1.524" layer="95">VSSQ</text>
<text x="2.2225" y="-41.4338" size="1.524" layer="95">VSSQ</text>
<pin name="A0" x="-15.24" y="38.1" length="middle" direction="in"/>
<pin name="A1" x="-15.24" y="35.56" length="middle" direction="in"/>
<pin name="A2" x="-15.24" y="33.02" length="middle" direction="in"/>
<pin name="A3" x="-15.24" y="30.48" length="middle" direction="in"/>
<pin name="A4" x="-15.24" y="27.94" length="middle" direction="in"/>
<pin name="A5" x="-15.24" y="25.4" length="middle" direction="in"/>
<pin name="A6" x="-15.24" y="22.86" length="middle" direction="in"/>
<pin name="A7" x="-15.24" y="20.32" length="middle" direction="in"/>
<pin name="A8" x="-15.24" y="17.78" length="middle" direction="in"/>
<pin name="A9" x="-15.24" y="15.24" length="middle" direction="in"/>
<pin name="A10/AP" x="-15.24" y="12.7" length="middle" direction="in"/>
<pin name="A11" x="-15.24" y="10.16" length="middle" direction="in"/>
<pin name="A12" x="-15.24" y="7.62" length="middle" direction="in"/>
<pin name="BA0" x="-15.24" y="2.54" length="middle" direction="in"/>
<pin name="BA1" x="-15.24" y="0" length="middle" direction="in"/>
<pin name="WE#" x="-15.24" y="-27.94" length="middle" direction="in"/>
<pin name="RAS#" x="-15.24" y="-22.86" length="middle" direction="in"/>
<pin name="CAS#" x="-15.24" y="-25.4" length="middle" direction="in"/>
<pin name="DQ0" x="15.24" y="38.1" length="middle" rot="R180"/>
<pin name="DQ1" x="15.24" y="35.56" length="middle" rot="R180"/>
<pin name="DQ2" x="15.24" y="33.02" length="middle" rot="R180"/>
<pin name="DQ3" x="15.24" y="30.48" length="middle" rot="R180"/>
<pin name="DQ4" x="15.24" y="27.94" length="middle" rot="R180"/>
<pin name="DQ5" x="15.24" y="25.4" length="middle" rot="R180"/>
<pin name="DQ6" x="15.24" y="22.86" length="middle" rot="R180"/>
<pin name="DQ7" x="15.24" y="20.32" length="middle" rot="R180"/>
<pin name="CS#" x="-15.24" y="-20.32" length="middle" direction="in"/>
<pin name="DQ8" x="15.24" y="17.78" length="middle" rot="R180"/>
<pin name="DQ9" x="15.24" y="15.24" length="middle" rot="R180"/>
<pin name="DQ10" x="15.24" y="12.7" length="middle" rot="R180"/>
<pin name="DQ11" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="DQ12" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="DQ13" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="DQ14" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="DQ15" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="CKE" x="-15.24" y="-15.24" length="middle" direction="in"/>
<pin name="CLK" x="-15.24" y="-12.7" length="middle" direction="in"/>
<pin name="DQM" x="-15.24" y="-5.08" length="middle" direction="in"/>
<pin name="LDQM" x="-15.24" y="-7.62" length="middle" direction="in"/>
<pin name="VDD" x="15.24" y="-5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="VSSQ" x="15.24" y="-33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="VSSQ@12" x="15.24" y="-35.56" visible="pad" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDQ" x="15.24" y="-12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDQ@9" x="15.24" y="-15.24" visible="pad" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD@14" x="15.24" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD@27" x="15.24" y="-10.16" visible="pad" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS" x="15.24" y="-25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS@41" x="15.24" y="-30.48" visible="pad" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDQ@43" x="15.24" y="-17.78" visible="pad" length="middle" direction="pwr" rot="R180"/>
<pin name="VSSQ@46" x="15.24" y="-38.1" visible="pad" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDQ@49" x="15.24" y="-20.32" visible="pad" length="middle" direction="pwr" rot="R180"/>
<pin name="VSSQ@52" x="15.24" y="-40.64" visible="pad" length="middle" direction="pwr" rot="R180"/>
<pin name="NC" x="-15.24" y="-33.02" length="middle" direction="nc"/>
<pin name="VSS@28" x="15.24" y="-27.94" visible="pad" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="K4S561632E-TC" prefix="U">
<description>&lt;b&gt;SDRAM&lt;/b&gt;&lt;p&gt;
4M x 16bit x 4 banks synchronous DRAM. 256 Mbit SDRAM.</description>
<gates>
<gate name="G$1" symbol="SDRAM16" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSOP54">
<connects>
<connect gate="G$1" pin="A0" pad="23"/>
<connect gate="G$1" pin="A1" pad="24"/>
<connect gate="G$1" pin="A10/AP" pad="22"/>
<connect gate="G$1" pin="A11" pad="35"/>
<connect gate="G$1" pin="A12" pad="36"/>
<connect gate="G$1" pin="A2" pad="25"/>
<connect gate="G$1" pin="A3" pad="26"/>
<connect gate="G$1" pin="A4" pad="29"/>
<connect gate="G$1" pin="A5" pad="30"/>
<connect gate="G$1" pin="A6" pad="31"/>
<connect gate="G$1" pin="A7" pad="32"/>
<connect gate="G$1" pin="A8" pad="33"/>
<connect gate="G$1" pin="A9" pad="34"/>
<connect gate="G$1" pin="BA0" pad="20"/>
<connect gate="G$1" pin="BA1" pad="21"/>
<connect gate="G$1" pin="CAS#" pad="17"/>
<connect gate="G$1" pin="CKE" pad="37"/>
<connect gate="G$1" pin="CLK" pad="38"/>
<connect gate="G$1" pin="CS#" pad="19"/>
<connect gate="G$1" pin="DQ0" pad="2"/>
<connect gate="G$1" pin="DQ1" pad="4"/>
<connect gate="G$1" pin="DQ10" pad="45"/>
<connect gate="G$1" pin="DQ11" pad="47"/>
<connect gate="G$1" pin="DQ12" pad="48"/>
<connect gate="G$1" pin="DQ13" pad="50"/>
<connect gate="G$1" pin="DQ14" pad="51"/>
<connect gate="G$1" pin="DQ15" pad="53"/>
<connect gate="G$1" pin="DQ2" pad="5"/>
<connect gate="G$1" pin="DQ3" pad="7"/>
<connect gate="G$1" pin="DQ4" pad="8"/>
<connect gate="G$1" pin="DQ5" pad="10"/>
<connect gate="G$1" pin="DQ6" pad="11"/>
<connect gate="G$1" pin="DQ7" pad="13"/>
<connect gate="G$1" pin="DQ8" pad="42"/>
<connect gate="G$1" pin="DQ9" pad="44"/>
<connect gate="G$1" pin="DQM" pad="39"/>
<connect gate="G$1" pin="LDQM" pad="15"/>
<connect gate="G$1" pin="NC" pad="40"/>
<connect gate="G$1" pin="RAS#" pad="18"/>
<connect gate="G$1" pin="VDD" pad="1"/>
<connect gate="G$1" pin="VDD@14" pad="14"/>
<connect gate="G$1" pin="VDD@27" pad="27"/>
<connect gate="G$1" pin="VDDQ" pad="3"/>
<connect gate="G$1" pin="VDDQ@43" pad="43"/>
<connect gate="G$1" pin="VDDQ@49" pad="49"/>
<connect gate="G$1" pin="VDDQ@9" pad="9"/>
<connect gate="G$1" pin="VSS" pad="54"/>
<connect gate="G$1" pin="VSS@28" pad="28"/>
<connect gate="G$1" pin="VSS@41" pad="41"/>
<connect gate="G$1" pin="VSSQ" pad="6"/>
<connect gate="G$1" pin="VSSQ@12" pad="12"/>
<connect gate="G$1" pin="VSSQ@46" pad="46"/>
<connect gate="G$1" pin="VSSQ@52" pad="52"/>
<connect gate="G$1" pin="WE#" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Exar_By_element14_Batch_1">
<description>Developed by element14 :&lt;br&gt;
element14 CAD Library consolidation.ulp
at 30/07/2012 17:16:13</description>
<packages>
<package name="SOT95P280X145-5N">
<smd name="1" x="-1.1684" y="0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="2" x="-1.1684" y="0" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="3" x="-1.1684" y="-0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="4" x="1.1684" y="-0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="5" x="1.1684" y="0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<wire x1="-0.2794" y1="-1.4478" x2="0.2794" y2="-1.4478" width="0.1524" layer="21"/>
<wire x1="0.7874" y1="-0.3302" x2="0.7874" y2="0.3302" width="0.1524" layer="21"/>
<wire x1="0.2794" y1="1.4478" x2="-0.2794" y2="1.4478" width="0.1524" layer="21"/>
<wire x1="0.1778" y1="1.2192" x2="-0.1778" y2="1.2192" width="0.1524" layer="21" curve="-76"/>
<wire x1="0.1778" y1="1.2192" x2="-0.1778" y2="1.2192" width="0.1524" layer="21" curve="-76"/>
<text x="-2.0066" y="1.3716" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-2.0066" y="1.3716" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="0.7874" y1="-1.4478" x2="0.7874" y2="-1.1938" width="0" layer="51"/>
<wire x1="-0.7874" y1="1.4478" x2="-0.7874" y2="1.1938" width="0" layer="51"/>
<wire x1="-0.7874" y1="0.7112" x2="-0.7874" y2="0.254" width="0" layer="51"/>
<wire x1="-0.7874" y1="-0.254" x2="-0.7874" y2="-0.7112" width="0" layer="51"/>
<wire x1="-0.7874" y1="-1.4478" x2="-0.7874" y2="-1.1938" width="0" layer="51"/>
<wire x1="0.7874" y1="1.4478" x2="0.7874" y2="1.1938" width="0" layer="51"/>
<wire x1="-0.7874" y1="-1.4478" x2="0.7874" y2="-1.4478" width="0" layer="51"/>
<wire x1="0.7874" y1="-0.7112" x2="0.7874" y2="0.7112" width="0" layer="51"/>
<wire x1="0.7874" y1="1.4478" x2="0.3048" y2="1.4478" width="0" layer="51"/>
<wire x1="0.3048" y1="1.4478" x2="-0.3048" y2="1.4478" width="0" layer="51"/>
<wire x1="-0.3048" y1="1.4478" x2="-0.7874" y2="1.4478" width="0" layer="51"/>
<wire x1="-0.7874" y1="0.7112" x2="-0.7874" y2="1.1938" width="0" layer="51"/>
<wire x1="-0.7874" y1="1.1938" x2="-1.397" y2="1.1938" width="0" layer="51"/>
<wire x1="-1.397" y1="1.1938" x2="-1.397" y2="0.7112" width="0" layer="51"/>
<wire x1="-1.397" y1="0.7112" x2="-0.7874" y2="0.7112" width="0" layer="51"/>
<wire x1="-0.7874" y1="-0.254" x2="-0.7874" y2="0.254" width="0" layer="51"/>
<wire x1="-0.7874" y1="0.254" x2="-1.397" y2="0.254" width="0" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-0.7874" y2="-0.254" width="0" layer="51"/>
<wire x1="-0.7874" y1="-1.1938" x2="-0.7874" y2="-0.7112" width="0" layer="51"/>
<wire x1="-0.7874" y1="-0.7112" x2="-1.397" y2="-0.7112" width="0" layer="51"/>
<wire x1="-1.397" y1="-0.7112" x2="-1.397" y2="-1.1938" width="0" layer="51"/>
<wire x1="-1.397" y1="-1.1938" x2="-0.7874" y2="-1.1938" width="0" layer="51"/>
<wire x1="0.7874" y1="-0.7112" x2="0.7874" y2="-1.1938" width="0" layer="51"/>
<wire x1="0.7874" y1="-1.1938" x2="1.397" y2="-1.1938" width="0" layer="51"/>
<wire x1="1.397" y1="-1.1938" x2="1.397" y2="-0.7112" width="0" layer="51"/>
<wire x1="1.397" y1="-0.7112" x2="0.7874" y2="-0.7112" width="0" layer="51"/>
<wire x1="0.7874" y1="1.1938" x2="0.7874" y2="0.7112" width="0" layer="51"/>
<wire x1="0.7874" y1="0.7112" x2="1.397" y2="0.7112" width="0" layer="51"/>
<wire x1="1.397" y1="0.7112" x2="1.397" y2="1.1938" width="0" layer="51"/>
<wire x1="1.397" y1="1.1938" x2="0.7874" y2="1.1938" width="0" layer="51"/>
<wire x1="0.3048" y1="1.4478" x2="-0.3048" y2="1.4478" width="0" layer="51" curve="-180"/>
<wire x1="0.3048" y1="1.4478" x2="-0.3048" y2="1.4478" width="0" layer="51" curve="-180"/>
<text x="-2.0066" y="1.3716" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-2.0066" y="1.3716" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="2.54" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-4.445" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="SPX3819M5-L-5-0">
<pin name="VIN" x="-17.78" y="2.54" length="middle" direction="pwr"/>
<pin name="EN" x="-17.78" y="-2.54" length="middle" direction="in"/>
<pin name="ADJ/BYP" x="-17.78" y="-5.08" length="middle" direction="pas"/>
<pin name="GND" x="-17.78" y="-10.16" length="middle" direction="pas"/>
<pin name="VOUT" x="17.78" y="2.54" length="middle" direction="out" rot="R180"/>
<wire x1="-12.7" y1="7.62" x2="-12.7" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="12.7" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="12.7" y2="7.62" width="0.4064" layer="94"/>
<wire x1="12.7" y1="7.62" x2="-12.7" y2="7.62" width="0.4064" layer="94"/>
<text x="-5.588" y="9.779" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-4.2164" y="-19.177" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SPX3819M5-L-5-0" prefix="U">
<description>500mA, LOW-NOISE LDO VOLTAGE REGULATOR</description>
<gates>
<gate name="A" symbol="SPX3819M5-L-5-0" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P280X145-5N">
<connects>
<connect gate="A" pin="ADJ/BYP" pad="4"/>
<connect gate="A" pin="EN" pad="3"/>
<connect gate="A" pin="GND" pad="2"/>
<connect gate="A" pin="VIN" pad="1"/>
<connect gate="A" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="SPX3819M5-L-5-0" constant="no"/>
<attribute name="OC_FARNELL" value="1829408" constant="no"/>
<attribute name="OC_NEWARK" value="84R4417" constant="no"/>
<attribute name="PACKAGE" value="SOT-23-5" constant="no"/>
<attribute name="SUPPLIER" value="EXAR" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="cyclonev_f484">
<packages>
<package name="CYCLONEVF484V2">
<wire x1="-11.5" y1="11.5" x2="-10.69" y2="11.5" width="0.2" layer="21"/>
<wire x1="-10.69" y1="11.5" x2="11.5" y2="11.5" width="0.2" layer="21"/>
<wire x1="-11.5" y1="11.5" x2="-11.5" y2="10.69" width="0.2" layer="21"/>
<wire x1="-11.5" y1="10.69" x2="-11.5" y2="-11.5" width="0.2" layer="21"/>
<wire x1="-11.5" y1="-11.5" x2="11.5" y2="-11.5" width="0.2" layer="21"/>
<wire x1="11.5" y1="11.5" x2="11.5" y2="-11.5" width="0.2" layer="21"/>
<circle x="-11.8" y="10.5" radius="0.1" width="0.2" layer="21"/>
<wire x1="-12.4" y1="12.4" x2="12.4" y2="12.4" width="0.2" layer="39"/>
<wire x1="-12.4" y1="12.4" x2="-12.4" y2="-12.4" width="0.2" layer="39"/>
<wire x1="-12.4" y1="-12.4" x2="12.4" y2="-12.4" width="0.2" layer="39"/>
<wire x1="12.4" y1="12.4" x2="12.4" y2="-12.4" width="0.2" layer="39"/>
<smd name="A1" x="-10.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A2" x="-9.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A3" x="-8.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A4" x="-7.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A5" x="-6.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A6" x="-5.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A7" x="-4.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A8" x="-3.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A9" x="-2.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A10" x="-1.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A11" x="-0.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A12" x="0.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A13" x="1.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A14" x="2.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A15" x="3.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A16" x="4.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A17" x="5.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A18" x="6.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A19" x="7.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A20" x="8.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A21" x="9.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="A22" x="10.5" y="10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B1" x="-10.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B2" x="-9.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B3" x="-8.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B4" x="-7.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B5" x="-6.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B6" x="-5.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B7" x="-4.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B8" x="-3.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B9" x="-2.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B10" x="-1.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B11" x="-0.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B12" x="0.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B13" x="1.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B14" x="2.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B15" x="3.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B16" x="4.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B17" x="5.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B18" x="6.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B19" x="7.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B20" x="8.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B21" x="9.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="B22" x="10.5" y="9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C1" x="-10.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C2" x="-9.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C3" x="-8.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C4" x="-7.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C5" x="-6.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C6" x="-5.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C7" x="-4.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C8" x="-3.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C9" x="-2.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C10" x="-1.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C11" x="-0.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C12" x="0.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C13" x="1.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C14" x="2.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C15" x="3.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C16" x="4.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C17" x="5.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C18" x="6.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C19" x="7.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C20" x="8.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C21" x="9.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="C22" x="10.5" y="8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D1" x="-10.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D2" x="-9.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D3" x="-8.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D4" x="-7.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D5" x="-6.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D6" x="-5.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D7" x="-4.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D8" x="-3.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D9" x="-2.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D10" x="-1.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D11" x="-0.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D12" x="0.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D13" x="1.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D14" x="2.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D15" x="3.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D16" x="4.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D17" x="5.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D18" x="6.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D19" x="7.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D20" x="8.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D21" x="9.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="D22" x="10.5" y="7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E1" x="-10.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E2" x="-9.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E3" x="-8.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E4" x="-7.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E5" x="-6.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E6" x="-5.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E7" x="-4.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E8" x="-3.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E9" x="-2.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E10" x="-1.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E11" x="-0.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E12" x="0.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E13" x="1.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E14" x="2.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E15" x="3.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E16" x="4.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E17" x="5.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E18" x="6.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E19" x="7.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E20" x="8.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E21" x="9.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="E22" x="10.5" y="6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F1" x="-10.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F2" x="-9.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F3" x="-8.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F4" x="-7.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F5" x="-6.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F6" x="-5.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F7" x="-4.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F8" x="-3.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F9" x="-2.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F10" x="-1.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F11" x="-0.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F12" x="0.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F13" x="1.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F14" x="2.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F15" x="3.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F16" x="4.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F17" x="5.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F18" x="6.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F19" x="7.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F20" x="8.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F21" x="9.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="F22" x="10.5" y="5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G1" x="-10.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G2" x="-9.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G3" x="-8.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G4" x="-7.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G5" x="-6.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G6" x="-5.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G7" x="-4.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G8" x="-3.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G9" x="-2.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G10" x="-1.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G11" x="-0.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G12" x="0.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G13" x="1.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G14" x="2.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G15" x="3.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G16" x="4.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G17" x="5.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G18" x="6.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G19" x="7.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G20" x="8.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G21" x="9.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="G22" x="10.5" y="4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H1" x="-10.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H2" x="-9.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H3" x="-8.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H4" x="-7.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H5" x="-6.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H6" x="-5.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H7" x="-4.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H8" x="-3.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H9" x="-2.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H10" x="-1.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H11" x="-0.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H12" x="0.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H13" x="1.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H14" x="2.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H15" x="3.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H16" x="4.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H17" x="5.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H18" x="6.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H19" x="7.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H20" x="8.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H21" x="9.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="H22" x="10.5" y="3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J1" x="-10.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J2" x="-9.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J3" x="-8.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J4" x="-7.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J5" x="-6.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J6" x="-5.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J7" x="-4.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J8" x="-3.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J9" x="-2.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J10" x="-1.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J11" x="-0.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J12" x="0.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J13" x="1.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J14" x="2.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J15" x="3.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J16" x="4.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J17" x="5.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J18" x="6.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J19" x="7.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J20" x="8.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J21" x="9.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="J22" x="10.5" y="2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K1" x="-10.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K2" x="-9.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K3" x="-8.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K4" x="-7.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K5" x="-6.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K6" x="-5.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K7" x="-4.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K8" x="-3.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K9" x="-2.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K10" x="-1.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K11" x="-0.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K12" x="0.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K13" x="1.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K14" x="2.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K15" x="3.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K16" x="4.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K17" x="5.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K18" x="6.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K19" x="7.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K20" x="8.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K21" x="9.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="K22" x="10.5" y="1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L1" x="-10.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L2" x="-9.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L3" x="-8.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L4" x="-7.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L5" x="-6.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L6" x="-5.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L7" x="-4.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L8" x="-3.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L9" x="-2.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L10" x="-1.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L11" x="-0.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L12" x="0.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L13" x="1.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L14" x="2.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L15" x="3.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L16" x="4.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L17" x="5.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L18" x="6.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L19" x="7.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L20" x="8.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L21" x="9.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="L22" x="10.5" y="0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M1" x="-10.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M2" x="-9.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M3" x="-8.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M4" x="-7.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M5" x="-6.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M6" x="-5.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M7" x="-4.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M8" x="-3.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M9" x="-2.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M10" x="-1.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M11" x="-0.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M12" x="0.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M13" x="1.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M14" x="2.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M15" x="3.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M16" x="4.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M17" x="5.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M18" x="6.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M19" x="7.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M20" x="8.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M21" x="9.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="M22" x="10.5" y="-0.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N1" x="-10.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N2" x="-9.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N3" x="-8.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N4" x="-7.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N5" x="-6.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N6" x="-5.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N7" x="-4.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N8" x="-3.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N9" x="-2.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N10" x="-1.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N11" x="-0.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N12" x="0.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N13" x="1.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N14" x="2.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N15" x="3.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N16" x="4.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N17" x="5.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N18" x="6.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N19" x="7.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N20" x="8.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N21" x="9.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="N22" x="10.5" y="-1.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P1" x="-10.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P2" x="-9.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P3" x="-8.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P4" x="-7.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P5" x="-6.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P6" x="-5.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P7" x="-4.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P8" x="-3.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P9" x="-2.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P10" x="-1.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P11" x="-0.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P12" x="0.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P13" x="1.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P14" x="2.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P15" x="3.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P16" x="4.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P17" x="5.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P18" x="6.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P19" x="7.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P20" x="8.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P21" x="9.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="P22" x="10.5" y="-2.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R1" x="-10.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R2" x="-9.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R3" x="-8.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R4" x="-7.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R5" x="-6.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R6" x="-5.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R7" x="-4.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R8" x="-3.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R9" x="-2.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R10" x="-1.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R11" x="-0.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R12" x="0.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R13" x="1.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R14" x="2.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R15" x="3.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R16" x="4.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R17" x="5.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R18" x="6.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R19" x="7.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R20" x="8.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R21" x="9.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="R22" x="10.5" y="-3.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T1" x="-10.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T2" x="-9.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T3" x="-8.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T4" x="-7.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T5" x="-6.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T6" x="-5.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T7" x="-4.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T8" x="-3.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T9" x="-2.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T10" x="-1.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T11" x="-0.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T12" x="0.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T13" x="1.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T14" x="2.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T15" x="3.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T16" x="4.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T17" x="5.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T18" x="6.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T19" x="7.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T20" x="8.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T21" x="9.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="T22" x="10.5" y="-4.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U1" x="-10.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U2" x="-9.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U3" x="-8.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U4" x="-7.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U5" x="-6.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U6" x="-5.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U7" x="-4.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U8" x="-3.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U9" x="-2.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U10" x="-1.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U11" x="-0.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U12" x="0.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U13" x="1.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U14" x="2.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U15" x="3.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U16" x="4.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U17" x="5.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U18" x="6.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U19" x="7.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U20" x="8.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U21" x="9.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="U22" x="10.5" y="-5.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V1" x="-10.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V2" x="-9.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V3" x="-8.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V4" x="-7.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V5" x="-6.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V6" x="-5.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V7" x="-4.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V8" x="-3.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V9" x="-2.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V10" x="-1.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V11" x="-0.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V12" x="0.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V13" x="1.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V14" x="2.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V15" x="3.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V16" x="4.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V17" x="5.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V18" x="6.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V19" x="7.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V20" x="8.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V21" x="9.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="V22" x="10.5" y="-6.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W1" x="-10.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W2" x="-9.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W3" x="-8.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W4" x="-7.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W5" x="-6.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W6" x="-5.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W7" x="-4.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W8" x="-3.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W9" x="-2.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W10" x="-1.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W11" x="-0.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W12" x="0.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W13" x="1.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W14" x="2.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W15" x="3.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W16" x="4.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W17" x="5.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W18" x="6.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W19" x="7.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W20" x="8.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W21" x="9.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="W22" x="10.5" y="-7.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y1" x="-10.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y2" x="-9.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y3" x="-8.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y4" x="-7.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y5" x="-6.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y6" x="-5.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y7" x="-4.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y8" x="-3.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y9" x="-2.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y10" x="-1.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y11" x="-0.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y12" x="0.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y13" x="1.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y14" x="2.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y15" x="3.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y16" x="4.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y17" x="5.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y18" x="6.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y19" x="7.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y20" x="8.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y21" x="9.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="Y22" x="10.5" y="-8.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA1" x="-10.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA2" x="-9.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA3" x="-8.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA4" x="-7.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA5" x="-6.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA6" x="-5.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA7" x="-4.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA8" x="-3.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA9" x="-2.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA10" x="-1.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA11" x="-0.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA12" x="0.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA13" x="1.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA14" x="2.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA15" x="3.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA16" x="4.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA17" x="5.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA18" x="6.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA19" x="7.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA20" x="8.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA21" x="9.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AA22" x="10.5" y="-9.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB1" x="-10.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB2" x="-9.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB3" x="-8.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB4" x="-7.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB5" x="-6.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB6" x="-5.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB7" x="-4.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB8" x="-3.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB9" x="-2.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB10" x="-1.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB11" x="-0.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB12" x="0.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB13" x="1.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB14" x="2.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB15" x="3.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB16" x="4.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB17" x="5.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB18" x="6.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB19" x="7.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB20" x="8.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB21" x="9.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<smd name="AB22" x="10.5" y="-10.5" dx="0.38" dy="0.38" layer="1" roundness="100"/>
<wire x1="-11.5" y1="10.69" x2="-10.69" y2="11.5" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CYCLONEVSYM">
<description>Cyclone V</description>
<pin name="JTAG_TCK" x="-73.66" y="104.14" length="middle" direction="in"/>
<pin name="JTAG_TMS" x="-73.66" y="101.6" length="middle" direction="in"/>
<pin name="JTAG_TDI" x="-73.66" y="99.06" length="middle" direction="in"/>
<pin name="JTAG_TDO" x="-73.66" y="96.52" length="middle" direction="out"/>
<pin name="CLK" x="-73.66" y="91.44" length="middle" direction="in"/>
<pin name="AS_DATA0" x="-73.66" y="15.24" length="middle"/>
<pin name="AS_DATA1" x="-73.66" y="17.78" length="middle"/>
<pin name="AS_DATA2" x="-73.66" y="20.32" length="middle"/>
<pin name="AS_DATA3" x="-73.66" y="22.86" length="middle"/>
<pin name="AS_CSO_N" x="-73.66" y="25.4" length="middle" direction="out"/>
<pin name="CE_N" x="-73.66" y="33.02" length="middle" direction="in"/>
<pin name="CONFIG_N" x="-73.66" y="35.56" length="middle" direction="in"/>
<pin name="CONF_DONE" x="-73.66" y="38.1" length="middle"/>
<pin name="STATUS_N" x="-73.66" y="40.64" length="middle"/>
<pin name="MSEL_0" x="-73.66" y="83.82" length="middle" direction="in"/>
<pin name="MSEL_1" x="-73.66" y="81.28" length="middle" direction="in"/>
<pin name="MSEL_2" x="-73.66" y="78.74" length="middle" direction="in"/>
<pin name="MSEL_3" x="-73.66" y="76.2" length="middle" direction="in"/>
<pin name="MSEL_4" x="-73.66" y="73.66" length="middle" direction="in"/>
<wire x1="-68.58" y1="106.68" x2="-68.58" y2="48.26" width="0.254" layer="94"/>
<pin name="AS_DCLK" x="-73.66" y="27.94" length="middle" direction="out"/>
<pin name="INIT_DONE" x="-73.66" y="43.18" length="middle" direction="out"/>
<pin name="RREF_TL" x="-73.66" y="45.72" length="middle" direction="in"/>
<pin name="VCC" x="-73.66" y="68.58" length="middle" direction="pwr"/>
<pin name="VCCA_FPLL" x="-73.66" y="66.04" length="middle" direction="pwr"/>
<pin name="VCC_AUX" x="-73.66" y="63.5" length="middle" direction="pwr"/>
<pin name="VCCIO" x="-73.66" y="60.96" length="middle" direction="pwr"/>
<pin name="VCCPGM" x="-73.66" y="58.42" length="middle" direction="pwr"/>
<pin name="VCCBAT" x="-73.66" y="55.88" length="middle" direction="pwr"/>
<pin name="VREF" x="-73.66" y="53.34" length="middle" direction="pwr"/>
<pin name="GND" x="-73.66" y="50.8" length="middle" direction="pwr"/>
<text x="-66.04" y="109.22" size="1.27" layer="95">Cyclone V</text>
<text x="-66.04" y="111.76" size="1.27" layer="95">&gt;NAME</text>
<wire x1="124.46" y1="124.46" x2="-68.58" y2="124.46" width="0.254" layer="95"/>
<wire x1="-68.58" y1="124.46" x2="-68.58" y2="106.68" width="0.254" layer="95"/>
<pin name="P$1" x="-66.04" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$2" x="-63.5" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$3" x="-60.96" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$4" x="-58.42" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$5" x="-55.88" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$6" x="-53.34" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$7" x="-50.8" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$8" x="-48.26" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$9" x="-45.72" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$10" x="-43.18" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$11" x="-40.64" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$12" x="-38.1" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$13" x="-35.56" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$14" x="-33.02" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$15" x="-30.48" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$16" x="-27.94" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$17" x="-25.4" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$18" x="-22.86" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$19" x="-20.32" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$20" x="-17.78" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$21" x="-15.24" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$22" x="-12.7" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$23" x="-10.16" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$24" x="-7.62" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$25" x="-5.08" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$26" x="-2.54" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$27" x="0" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$28" x="2.54" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$29" x="5.08" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$30" x="7.62" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$31" x="10.16" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$32" x="12.7" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$33" x="15.24" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$34" x="17.78" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$35" x="20.32" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$36" x="22.86" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$37" x="25.4" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$38" x="27.94" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$39" x="30.48" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$40" x="33.02" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$41" x="35.56" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$42" x="38.1" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$43" x="40.64" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$44" x="43.18" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$45" x="45.72" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$46" x="48.26" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$47" x="50.8" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$48" x="53.34" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$49" x="55.88" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$50" x="58.42" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$51" x="60.96" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$52" x="63.5" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$53" x="66.04" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$54" x="68.58" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$55" x="71.12" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$56" x="73.66" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$57" x="76.2" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$58" x="78.74" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$59" x="81.28" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$60" x="83.82" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$61" x="86.36" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$62" x="88.9" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$63" x="91.44" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$64" x="93.98" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$65" x="96.52" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$66" x="99.06" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$67" x="101.6" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$68" x="104.14" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$69" x="106.68" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$70" x="109.22" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$71" x="111.76" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$72" x="114.3" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$73" x="116.84" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$74" x="119.38" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<pin name="P$75" x="121.92" y="129.54" length="middle" swaplevel="1" rot="R270"/>
<wire x1="124.46" y1="124.46" x2="124.46" y2="-68.58" width="0.254" layer="95"/>
<pin name="P$76" x="129.54" y="121.92" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$77" x="129.54" y="119.38" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$78" x="129.54" y="116.84" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$79" x="129.54" y="114.3" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$80" x="129.54" y="111.76" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$81" x="129.54" y="109.22" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$82" x="129.54" y="106.68" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$83" x="129.54" y="104.14" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$84" x="129.54" y="101.6" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$85" x="129.54" y="99.06" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$86" x="129.54" y="96.52" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$87" x="129.54" y="93.98" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$88" x="129.54" y="91.44" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$89" x="129.54" y="88.9" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$90" x="129.54" y="86.36" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$91" x="129.54" y="83.82" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$92" x="129.54" y="81.28" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$93" x="129.54" y="78.74" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$94" x="129.54" y="76.2" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$95" x="129.54" y="73.66" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$96" x="129.54" y="71.12" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$97" x="129.54" y="68.58" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$98" x="129.54" y="66.04" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$99" x="129.54" y="63.5" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$100" x="129.54" y="60.96" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$101" x="129.54" y="58.42" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$102" x="129.54" y="55.88" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$103" x="129.54" y="53.34" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$104" x="129.54" y="50.8" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$105" x="129.54" y="48.26" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$106" x="129.54" y="45.72" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$107" x="129.54" y="43.18" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$108" x="129.54" y="40.64" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$109" x="129.54" y="38.1" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$110" x="129.54" y="35.56" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$111" x="129.54" y="33.02" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$112" x="129.54" y="30.48" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$113" x="129.54" y="27.94" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$114" x="129.54" y="25.4" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$115" x="129.54" y="22.86" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$116" x="129.54" y="20.32" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$117" x="129.54" y="17.78" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$118" x="129.54" y="15.24" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$119" x="129.54" y="12.7" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$120" x="129.54" y="10.16" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$121" x="129.54" y="7.62" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$122" x="129.54" y="5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$123" x="129.54" y="2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$124" x="129.54" y="0" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$125" x="129.54" y="-2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$126" x="129.54" y="-5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$127" x="129.54" y="-7.62" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$128" x="129.54" y="-10.16" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$129" x="129.54" y="-12.7" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$130" x="129.54" y="-15.24" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$131" x="129.54" y="-17.78" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$132" x="129.54" y="-20.32" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$133" x="129.54" y="-22.86" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$134" x="129.54" y="-25.4" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$135" x="129.54" y="-27.94" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$136" x="129.54" y="-30.48" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$137" x="129.54" y="-33.02" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$138" x="129.54" y="-35.56" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$139" x="129.54" y="-38.1" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$140" x="129.54" y="-40.64" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$141" x="129.54" y="-43.18" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$142" x="129.54" y="-45.72" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$143" x="129.54" y="-48.26" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$144" x="129.54" y="-50.8" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$145" x="129.54" y="-53.34" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$146" x="129.54" y="-55.88" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$147" x="129.54" y="-58.42" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$148" x="129.54" y="-60.96" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$149" x="129.54" y="-63.5" length="middle" swaplevel="1" rot="R180"/>
<pin name="P$150" x="129.54" y="-66.04" length="middle" swaplevel="1" rot="R180"/>
<wire x1="124.46" y1="-68.58" x2="-68.58" y2="-68.58" width="0.254" layer="95"/>
<wire x1="-68.58" y1="-68.58" x2="-68.58" y2="48.26" width="0.254" layer="95"/>
<pin name="P$151" x="121.92" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$152" x="119.38" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$153" x="116.84" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$154" x="114.3" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$155" x="111.76" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$156" x="109.22" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$157" x="106.68" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$158" x="104.14" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$159" x="101.6" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$160" x="99.06" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$161" x="96.52" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$162" x="93.98" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$163" x="91.44" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$164" x="88.9" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$165" x="86.36" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$166" x="83.82" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$167" x="81.28" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$168" x="78.74" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$169" x="76.2" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$170" x="73.66" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$171" x="71.12" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$172" x="68.58" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$173" x="66.04" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$174" x="63.5" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$175" x="60.96" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$176" x="58.42" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$177" x="55.88" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$178" x="53.34" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$179" x="50.8" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$180" x="48.26" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$181" x="45.72" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$182" x="43.18" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$183" x="40.64" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$184" x="38.1" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$185" x="35.56" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$186" x="33.02" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$187" x="30.48" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$188" x="27.94" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$189" x="25.4" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$190" x="22.86" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$191" x="20.32" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$192" x="17.78" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
<pin name="P$193" x="15.24" y="-73.66" length="middle" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CYCLONEVFPGA484">
<gates>
<gate name="G$1" symbol="CYCLONEVSYM" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="SMALLPINS" package="CYCLONEVF484V2">
<connects>
<connect gate="G$1" pin="AS_CSO_N" pad="R4"/>
<connect gate="G$1" pin="AS_DATA0" pad="AB4"/>
<connect gate="G$1" pin="AS_DATA1" pad="AB3"/>
<connect gate="G$1" pin="AS_DATA2" pad="AA5"/>
<connect gate="G$1" pin="AS_DATA3" pad="T4"/>
<connect gate="G$1" pin="AS_DCLK" pad="V3"/>
<connect gate="G$1" pin="CE_N" pad="G5"/>
<connect gate="G$1" pin="CLK" pad="H16"/>
<connect gate="G$1" pin="CONFIG_N" pad="A4"/>
<connect gate="G$1" pin="CONF_DONE" pad="K6"/>
<connect gate="G$1" pin="GND" pad="A11 A21 AA3 AA4 AA6 AA11 AB1 AB2 AB9 AB14 AB19 B1 B2 B9 B14 C3 C4 C5 C17 D1 D2 D5 D10 D20 E3 E4 E13 F1 F2 F5 F6 F16 F17 G3 G4 G9 G19 H1 H2 H3 H4 H7 H12 H15 H22 J3 J5 J15 J20 K1 K2 K4 K8 K10 K12 K14 L3 L5 L11 L13 L15 L21 M1 M2 M4 M10 M12 M14 N3 N5 N7 N11 N13 N15 N17 N22 P1 P2 P4 P10 R3 R13 T1 T2 T16 T21 U3 U4 U5 U9 V1 V2 V4 V7 V12 V17 V22 W3 W4 Y1 Y2 Y5 Y18"/>
<connect gate="G$1" pin="INIT_DONE" pad="T18"/>
<connect gate="G$1" pin="JTAG_TCK" pad="V5"/>
<connect gate="G$1" pin="JTAG_TDI" pad="W5"/>
<connect gate="G$1" pin="JTAG_TDO" pad="M5"/>
<connect gate="G$1" pin="JTAG_TMS" pad="P5"/>
<connect gate="G$1" pin="MSEL_0" pad="L6"/>
<connect gate="G$1" pin="MSEL_1" pad="J6"/>
<connect gate="G$1" pin="MSEL_2" pad="A2"/>
<connect gate="G$1" pin="MSEL_3" pad="E5"/>
<connect gate="G$1" pin="MSEL_4" pad="F3"/>
<connect gate="G$1" pin="P$1" pad="A5"/>
<connect gate="G$1" pin="P$10" pad="F7"/>
<connect gate="G$1" pin="P$100" pad="V20"/>
<connect gate="G$1" pin="P$101" pad="W22"/>
<connect gate="G$1" pin="P$102" pad="W21"/>
<connect gate="G$1" pin="P$103" pad="Y22"/>
<connect gate="G$1" pin="P$104" pad="Y21"/>
<connect gate="G$1" pin="P$105" pad="AA22"/>
<connect gate="G$1" pin="P$106" pad="AB22"/>
<connect gate="G$1" pin="P$107" pad="AB21"/>
<connect gate="G$1" pin="P$108" pad="AB20"/>
<connect gate="G$1" pin="P$109" pad="AA20"/>
<connect gate="G$1" pin="P$11" pad="A8"/>
<connect gate="G$1" pin="P$110" pad="Y20"/>
<connect gate="G$1" pin="P$111" pad="AA19"/>
<connect gate="G$1" pin="P$112" pad="W19"/>
<connect gate="G$1" pin="P$113" pad="Y19"/>
<connect gate="G$1" pin="P$114" pad="AB18"/>
<connect gate="G$1" pin="P$115" pad="AA18"/>
<connect gate="G$1" pin="P$116" pad="V18"/>
<connect gate="G$1" pin="P$117" pad="AB17"/>
<connect gate="G$1" pin="P$118" pad="AA17"/>
<connect gate="G$1" pin="P$119" pad="U17"/>
<connect gate="G$1" pin="P$12" pad="G8"/>
<connect gate="G$1" pin="P$120" pad="Y17"/>
<connect gate="G$1" pin="P$121" pad="Y16"/>
<connect gate="G$1" pin="P$122" pad="V16"/>
<connect gate="G$1" pin="P$123" pad="W16"/>
<connect gate="G$1" pin="P$124" pad="U16"/>
<connect gate="G$1" pin="P$125" pad="AB15"/>
<connect gate="G$1" pin="P$126" pad="AA15"/>
<connect gate="G$1" pin="P$127" pad="V15"/>
<connect gate="G$1" pin="P$128" pad="Y15"/>
<connect gate="G$1" pin="P$129" pad="U15"/>
<connect gate="G$1" pin="P$13" pad="C8"/>
<connect gate="G$1" pin="P$130" pad="T15"/>
<connect gate="G$1" pin="P$131" pad="AA14"/>
<connect gate="G$1" pin="P$132" pad="V14"/>
<connect gate="G$1" pin="P$133" pad="Y14"/>
<connect gate="G$1" pin="P$134" pad="T14"/>
<connect gate="G$1" pin="P$135" pad="R14"/>
<connect gate="G$1" pin="P$136" pad="AB13"/>
<connect gate="G$1" pin="P$137" pad="AA13"/>
<connect gate="G$1" pin="P$138" pad="V13"/>
<connect gate="G$1" pin="P$139" pad="U13"/>
<connect gate="G$1" pin="P$14" pad="H8"/>
<connect gate="G$1" pin="P$140" pad="T13"/>
<connect gate="G$1" pin="P$141" pad="AB12"/>
<connect gate="G$1" pin="P$142" pad="AA12"/>
<connect gate="G$1" pin="P$143" pad="U12"/>
<connect gate="G$1" pin="P$144" pad="T12"/>
<connect gate="G$1" pin="P$145" pad="R12"/>
<connect gate="G$1" pin="P$146" pad="AB11"/>
<connect gate="G$1" pin="P$147" pad="U11"/>
<connect gate="G$1" pin="P$148" pad="Y11"/>
<connect gate="G$1" pin="P$149" pad="R11"/>
<connect gate="G$1" pin="P$15" pad="A9"/>
<connect gate="G$1" pin="P$150" pad="AB10"/>
<connect gate="G$1" pin="P$151" pad="AA10"/>
<connect gate="G$1" pin="P$152" pad="V10"/>
<connect gate="G$1" pin="P$153" pad="Y10"/>
<connect gate="G$1" pin="P$154" pad="U10"/>
<connect gate="G$1" pin="P$155" pad="T10"/>
<connect gate="G$1" pin="P$156" pad="AA9"/>
<connect gate="G$1" pin="P$157" pad="W9"/>
<connect gate="G$1" pin="P$158" pad="Y9"/>
<connect gate="G$1" pin="P$159" pad="V9"/>
<connect gate="G$1" pin="P$16" pad="D9"/>
<connect gate="G$1" pin="P$160" pad="T9"/>
<connect gate="G$1" pin="P$161" pad="AB8"/>
<connect gate="G$1" pin="P$162" pad="AA8"/>
<connect gate="G$1" pin="P$163" pad="U8"/>
<connect gate="G$1" pin="P$164" pad="W8"/>
<connect gate="G$1" pin="P$165" pad="T8"/>
<connect gate="G$1" pin="P$166" pad="AB7"/>
<connect gate="G$1" pin="P$167" pad="AA7"/>
<connect gate="G$1" pin="P$168" pad="U7"/>
<connect gate="G$1" pin="P$169" pad="AB6"/>
<connect gate="G$1" pin="P$17" pad="C9"/>
<connect gate="G$1" pin="P$170" pad="V6"/>
<connect gate="G$1" pin="P$171" pad="AB5"/>
<connect gate="G$1" pin="P$172" pad="U6"/>
<connect gate="G$1" pin="P$173" pad="T7"/>
<connect gate="G$1" pin="P$174" pad="R5"/>
<connect gate="G$1" pin="P$175" pad="R6"/>
<connect gate="G$1" pin="P$176" pad="R7"/>
<connect gate="G$1" pin="P$177" pad="P6"/>
<connect gate="G$1" pin="P$178" pad="P7"/>
<connect gate="G$1" pin="P$179" pad="P8"/>
<connect gate="G$1" pin="P$18" pad="E9"/>
<connect gate="G$1" pin="P$180" pad="N6"/>
<connect gate="G$1" pin="P$181" pad="N8"/>
<connect gate="G$1" pin="P$182" pad="N9"/>
<connect gate="G$1" pin="P$183" pad="M6"/>
<connect gate="G$1" pin="P$184" pad="M7"/>
<connect gate="G$1" pin="P$185" pad="M8"/>
<connect gate="G$1" pin="P$186" pad="L7"/>
<connect gate="G$1" pin="P$187" pad="L8"/>
<connect gate="G$1" pin="P$188" pad="K7"/>
<connect gate="G$1" pin="P$189" pad="K9"/>
<connect gate="G$1" pin="P$19" pad="F9"/>
<connect gate="G$1" pin="P$190" pad="J7"/>
<connect gate="G$1" pin="P$191" pad="J8"/>
<connect gate="G$1" pin="P$192" pad="H6"/>
<connect gate="G$1" pin="P$193" pad="G6"/>
<connect gate="G$1" pin="P$2" pad="B5"/>
<connect gate="G$1" pin="P$20" pad="A10"/>
<connect gate="G$1" pin="P$21" pad="B10"/>
<connect gate="G$1" pin="P$22" pad="E10"/>
<connect gate="G$1" pin="P$23" pad="F10"/>
<connect gate="G$1" pin="P$24" pad="G10"/>
<connect gate="G$1" pin="P$25" pad="B11"/>
<connect gate="G$1" pin="P$26" pad="G11"/>
<connect gate="G$1" pin="P$27" pad="C11"/>
<connect gate="G$1" pin="P$28" pad="H11"/>
<connect gate="G$1" pin="P$29" pad="J11"/>
<connect gate="G$1" pin="P$3" pad="B6"/>
<connect gate="G$1" pin="P$30" pad="A12"/>
<connect gate="G$1" pin="P$31" pad="B12"/>
<connect gate="G$1" pin="P$32" pad="D12"/>
<connect gate="G$1" pin="P$33" pad="E12"/>
<connect gate="G$1" pin="P$34" pad="F12"/>
<connect gate="G$1" pin="P$35" pad="A13"/>
<connect gate="G$1" pin="P$36" pad="B13"/>
<connect gate="G$1" pin="P$37" pad="D13"/>
<connect gate="G$1" pin="P$38" pad="C13"/>
<connect gate="G$1" pin="P$39" pad="F13"/>
<connect gate="G$1" pin="P$4" pad="D6"/>
<connect gate="G$1" pin="P$40" pad="G13"/>
<connect gate="G$1" pin="P$41" pad="A14"/>
<connect gate="G$1" pin="P$42" pad="E14"/>
<connect gate="G$1" pin="P$43" pad="F14"/>
<connect gate="G$1" pin="P$44" pad="H14"/>
<connect gate="G$1" pin="P$45" pad="A15"/>
<connect gate="G$1" pin="P$46" pad="B15"/>
<connect gate="G$1" pin="P$47" pad="E15"/>
<connect gate="G$1" pin="P$48" pad="C15"/>
<connect gate="G$1" pin="P$49" pad="F15"/>
<connect gate="G$1" pin="P$5" pad="C6"/>
<connect gate="G$1" pin="P$50" pad="G15"/>
<connect gate="G$1" pin="P$51" pad="B16"/>
<connect gate="G$1" pin="P$52" pad="E16"/>
<connect gate="G$1" pin="P$53" pad="C16"/>
<connect gate="G$1" pin="P$54" pad="D17"/>
<connect gate="G$1" pin="P$55" pad="G17"/>
<connect gate="G$1" pin="P$56" pad="G18"/>
<connect gate="G$1" pin="P$57" pad="G16"/>
<connect gate="G$1" pin="P$58" pad="H18"/>
<connect gate="G$1" pin="P$59" pad="J18"/>
<connect gate="G$1" pin="P$6" pad="A7"/>
<connect gate="G$1" pin="P$60" pad="J19"/>
<connect gate="G$1" pin="P$61" pad="J17"/>
<connect gate="G$1" pin="P$62" pad="K22"/>
<connect gate="G$1" pin="P$63" pad="K21"/>
<connect gate="G$1" pin="P$64" pad="K19"/>
<connect gate="G$1" pin="P$65" pad="K20"/>
<connect gate="G$1" pin="P$66" pad="K17"/>
<connect gate="G$1" pin="P$67" pad="K16"/>
<connect gate="G$1" pin="P$68" pad="L22"/>
<connect gate="G$1" pin="P$69" pad="L19"/>
<connect gate="G$1" pin="P$7" pad="B7"/>
<connect gate="G$1" pin="P$70" pad="L18"/>
<connect gate="G$1" pin="P$71" pad="L17"/>
<connect gate="G$1" pin="P$72" pad="M22"/>
<connect gate="G$1" pin="P$73" pad="M21"/>
<connect gate="G$1" pin="P$74" pad="M18"/>
<connect gate="G$1" pin="P$75" pad="M20"/>
<connect gate="G$1" pin="P$76" pad="M16"/>
<connect gate="G$1" pin="P$77" pad="N21"/>
<connect gate="G$1" pin="P$78" pad="N19"/>
<connect gate="G$1" pin="P$79" pad="N20"/>
<connect gate="G$1" pin="P$8" pad="E7"/>
<connect gate="G$1" pin="P$80" pad="N16"/>
<connect gate="G$1" pin="P$81" pad="P22"/>
<connect gate="G$1" pin="P$82" pad="P18"/>
<connect gate="G$1" pin="P$83" pad="P19"/>
<connect gate="G$1" pin="P$84" pad="P17"/>
<connect gate="G$1" pin="P$85" pad="P16"/>
<connect gate="G$1" pin="P$86" pad="R22"/>
<connect gate="G$1" pin="P$87" pad="R21"/>
<connect gate="G$1" pin="P$88" pad="R17"/>
<connect gate="G$1" pin="P$89" pad="R16"/>
<connect gate="G$1" pin="P$9" pad="D7"/>
<connect gate="G$1" pin="P$90" pad="T22"/>
<connect gate="G$1" pin="P$91" pad="T19"/>
<connect gate="G$1" pin="P$92" pad="T20"/>
<connect gate="G$1" pin="P$93" pad="T18"/>
<connect gate="G$1" pin="P$94" pad="T17"/>
<connect gate="G$1" pin="P$95" pad="U22"/>
<connect gate="G$1" pin="P$96" pad="U21"/>
<connect gate="G$1" pin="P$97" pad="U20"/>
<connect gate="G$1" pin="P$98" pad="V21"/>
<connect gate="G$1" pin="P$99" pad="V19"/>
<connect gate="G$1" pin="RREF_TL" pad="A1"/>
<connect gate="G$1" pin="STATUS_N" pad="H5"/>
<connect gate="G$1" pin="VCC" pad="J4 J10 J12 J14 J16 K3 K5 K11 K13 K15 L4 L10 L12 L14 L16 M11 M13 M15 N4 N10 N12 N14 P3 P11 P13 P15"/>
<connect gate="G$1" pin="VCCA_FPLL" pad="F4 H19 M3 T3 T5 U18"/>
<connect gate="G$1" pin="VCCBAT" pad="A3"/>
<connect gate="G$1" pin="VCCIO" pad="A6 A16 AA16 AA21 B19 C7 C12 C22 D4 D15 E8 E18 F11 F21 G7 G14 H17 J1 K18 M19 P20 R1 R8 R18 T6 T11 U14 U19 W10 W15 W20 Y4 Y8 Y13"/>
<connect gate="G$1" pin="VCCPGM" pad="C10 D8 D14 D16 E1 E11 F8 J2 M17 N18 P21 R2 R19 V8 W6 W11 W12 W14 W17"/>
<connect gate="G$1" pin="VCC_AUX" pad="D11 D18 E6 W7 W13 W18"/>
<connect gate="G$1" pin="VREF" pad="AB16 B8 C14 L20 R20 W1 Y7 Y12"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Eagle_Wuerth_Elektronik_Switch_rev15b">
<description>&lt;BR&gt;Wurth Elektronik - Switches&lt;br&gt;&lt;Hr&gt;
&lt;BR&gt;&lt;BR&gt; 
&lt;TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0&gt;
&lt;TR&gt;   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;&lt;br&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt; &lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------------------------&lt;BR&gt;
&lt;B&gt;&lt;I&gt;&lt;span style='font-size:26pt;
  color:#FF6600;'&gt;WE &lt;/span&gt;&lt;/i&gt;&lt;/b&gt;
&lt;BR&gt;
      ---------------------------&lt;BR&gt;&lt;b&gt;WÃ¼rth Elektronik&lt;/b&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;

  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  
&lt;/TABLE&gt;
&lt;B&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;More than you expect&lt;BR&gt;&lt;BR&gt;&lt;BR&gt;&lt;/B&gt;

&lt;HR&gt;&lt;BR&gt;
&lt;b&gt;WÃ¼rth Elektronik eiSos GmbH &amp; Co. KG&lt;/b&gt;&lt;br&gt;
EMC &amp; Inductive Solutions&lt;br&gt;

Max-Eyth-Str.1&lt;br&gt;
D-74638 Waldenburg&lt;br&gt;
&lt;br&gt;
Tel: +49 (0)7942-945-0&lt;br&gt;
Fax:+49 (0)7942-945-405&lt;br&gt;
&lt;br&gt;
&lt;a href="http://www.we-online.com/eagle"&gt;http://www.we-online.com/eagle&lt;/a&gt;&lt;br&gt;
&lt;a href="mailto:libraries@we-online.com"&gt;libraries@we-online.com&lt;/a&gt; &lt;BR&gt;&lt;BR&gt;
&lt;br&gt;&lt;HR&gt;&lt;BR&gt;
Neither CadSoft nor WE-eiSos does warrant that this library is error-free or &lt;br&gt;
that it meets your specific requirements.&lt;br&gt;&lt;BR&gt;
Please contact us for more information.&lt;br&gt;&lt;BR&gt;&lt;br&gt;
&lt;hr&gt;
Eagle Version 6, Library Revision 2015b, 08.04.2015&lt;br&gt;
&lt;HR&gt;
Copyright: WÃ¼rth Elektronik</description>
<packages>
<package name="450302014072">
<description>WS-SUT, 10mm*2.5mm, SPDT, ON-ON, 2.54mm, right angle type, THT, Mini Slide Switch, 3 pins</description>
<wire x1="5" y1="6.8" x2="-5" y2="6.8" width="0.127" layer="21"/>
<wire x1="-5" y1="6.8" x2="-5" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-5" y1="-1.25" x2="5" y2="-1.25" width="0.127" layer="21"/>
<wire x1="5" y1="-1.25" x2="5" y2="6.8" width="0.127" layer="21"/>
<wire x1="5" y1="6.8" x2="-5" y2="6.8" width="0.127" layer="51"/>
<wire x1="-5" y1="6.8" x2="-5" y2="0.02" width="0.127" layer="51"/>
<wire x1="-5" y1="0.02" x2="5" y2="0.02" width="0.127" layer="51"/>
<wire x1="5" y1="0.02" x2="5" y2="6.8" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.8"/>
<pad name="3" x="2.54" y="0" drill="0.8"/>
<pad name="2" x="-2.54" y="0" drill="0.8"/>
<text x="0" y="0" size="1.27" layer="51">1</text>
<text x="-8.4224" y="7.0133" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.4224" y="-7.793" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="ROCKER_VERT_SPDT">
<wire x1="-3.81" y1="6" x2="3.81" y2="6" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-7" x2="3.81" y2="-7" width="0.127" layer="21"/>
<wire x1="-3.86" y1="6.06" x2="-3.81" y2="6" width="0.127" layer="21"/>
<wire x1="-3.81" y1="6" x2="-3.81" y2="-7" width="0.127" layer="21"/>
<wire x1="3.82" y1="-5.88" x2="3.81" y2="-7" width="0.127" layer="21"/>
<wire x1="3.81" y1="-7" x2="3.81" y2="6" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="-0.5" drill="1.09"/>
<pad name="P$2" x="0" y="-3.04" drill="1.09"/>
<pad name="P$3" x="0" y="-5.58" drill="1.09"/>
<hole x="0" y="4.58" drill="1.09"/>
</package>
<package name="ROCKER_VERT_SPDT2">
<wire x1="-3.43" y1="14" x2="3.43" y2="14" width="0.127" layer="21"/>
<wire x1="-3.43" y1="1" x2="3.43" y2="1" width="0.127" layer="21"/>
<wire x1="-3.43" y1="14" x2="-3.43" y2="1" width="0.127" layer="21"/>
<wire x1="3.43" y1="1" x2="3.43" y2="14" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.85"/>
<pad name="P$2" x="0" y="-3.81" drill="1.85"/>
<pad name="P$3" x="0" y="-7.62" drill="1.85"/>
<hole x="2.54" y="12.7" drill="1.85"/>
<hole x="-2.54" y="12.7" drill="1.85"/>
</package>
</packages>
<symbols>
<symbol name="SPDT">
<text x="-5.08" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="2" rot="R180"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="3.048" y2="1.778" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="450302014072" prefix="SW">
<description>WS-SUT, 10mm*2.5mm, SPDT, ON-ON, 2.54mm, right angle type, THT, Mini Slide Switch, 3 pins</description>
<gates>
<gate name="G$1" symbol="SPDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="450302014072">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROCKER" package="ROCKER_VERT_SPDT">
<connects>
<connect gate="G$1" pin="1" pad="P$2"/>
<connect gate="G$1" pin="2" pad="P$1"/>
<connect gate="G$1" pin="3" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROCKER2" package="ROCKER_VERT_SPDT2">
<connects>
<connect gate="G$1" pin="1" pad="P$2"/>
<connect gate="G$1" pin="2" pad="P$1"/>
<connect gate="G$1" pin="3" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adc">
<packages>
<package name="SOT95P280X145-5N">
<smd name="1" x="-1.1684" y="0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="2" x="-1.1684" y="0" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="3" x="-1.1684" y="-0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="4" x="1.1684" y="-0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="5" x="1.1684" y="0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<wire x1="-0.2794" y1="-1.4478" x2="0.2794" y2="-1.4478" width="0.1524" layer="21"/>
<wire x1="0.7874" y1="-0.3302" x2="0.7874" y2="0.3302" width="0.1524" layer="21"/>
<wire x1="0.2794" y1="1.4478" x2="-0.2794" y2="1.4478" width="0.1524" layer="21"/>
<wire x1="0.1778" y1="1.2192" x2="-0.1778" y2="1.2192" width="0.1524" layer="21" curve="-76"/>
<wire x1="0.1778" y1="1.2192" x2="-0.1778" y2="1.2192" width="0.1524" layer="21" curve="-76"/>
<text x="-2.0066" y="1.3716" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-2.0066" y="1.3716" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="0.7874" y1="-1.4478" x2="0.7874" y2="-1.1938" width="0" layer="51"/>
<wire x1="-0.7874" y1="1.4478" x2="-0.7874" y2="1.1938" width="0" layer="51"/>
<wire x1="-0.7874" y1="0.7112" x2="-0.7874" y2="0.254" width="0" layer="51"/>
<wire x1="-0.7874" y1="-0.254" x2="-0.7874" y2="-0.7112" width="0" layer="51"/>
<wire x1="-0.7874" y1="-1.4478" x2="-0.7874" y2="-1.1938" width="0" layer="51"/>
<wire x1="0.7874" y1="1.4478" x2="0.7874" y2="1.1938" width="0" layer="51"/>
<wire x1="-0.7874" y1="-1.4478" x2="0.7874" y2="-1.4478" width="0" layer="51"/>
<wire x1="0.7874" y1="-0.7112" x2="0.7874" y2="0.7112" width="0" layer="51"/>
<wire x1="0.7874" y1="1.4478" x2="0.3048" y2="1.4478" width="0" layer="51"/>
<wire x1="0.3048" y1="1.4478" x2="-0.3048" y2="1.4478" width="0" layer="51"/>
<wire x1="-0.3048" y1="1.4478" x2="-0.7874" y2="1.4478" width="0" layer="51"/>
<wire x1="-0.7874" y1="0.7112" x2="-0.7874" y2="1.1938" width="0" layer="51"/>
<wire x1="-0.7874" y1="1.1938" x2="-1.397" y2="1.1938" width="0" layer="51"/>
<wire x1="-1.397" y1="1.1938" x2="-1.397" y2="0.7112" width="0" layer="51"/>
<wire x1="-1.397" y1="0.7112" x2="-0.7874" y2="0.7112" width="0" layer="51"/>
<wire x1="-0.7874" y1="-0.254" x2="-0.7874" y2="0.254" width="0" layer="51"/>
<wire x1="-0.7874" y1="0.254" x2="-1.397" y2="0.254" width="0" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-0.7874" y2="-0.254" width="0" layer="51"/>
<wire x1="-0.7874" y1="-1.1938" x2="-0.7874" y2="-0.7112" width="0" layer="51"/>
<wire x1="-0.7874" y1="-0.7112" x2="-1.397" y2="-0.7112" width="0" layer="51"/>
<wire x1="-1.397" y1="-0.7112" x2="-1.397" y2="-1.1938" width="0" layer="51"/>
<wire x1="-1.397" y1="-1.1938" x2="-0.7874" y2="-1.1938" width="0" layer="51"/>
<wire x1="0.7874" y1="-0.7112" x2="0.7874" y2="-1.1938" width="0" layer="51"/>
<wire x1="0.7874" y1="-1.1938" x2="1.397" y2="-1.1938" width="0" layer="51"/>
<wire x1="1.397" y1="-1.1938" x2="1.397" y2="-0.7112" width="0" layer="51"/>
<wire x1="1.397" y1="-0.7112" x2="0.7874" y2="-0.7112" width="0" layer="51"/>
<wire x1="0.7874" y1="1.1938" x2="0.7874" y2="0.7112" width="0" layer="51"/>
<wire x1="0.7874" y1="0.7112" x2="1.397" y2="0.7112" width="0" layer="51"/>
<wire x1="1.397" y1="0.7112" x2="1.397" y2="1.1938" width="0" layer="51"/>
<wire x1="1.397" y1="1.1938" x2="0.7874" y2="1.1938" width="0" layer="51"/>
<wire x1="0.3048" y1="1.4478" x2="-0.3048" y2="1.4478" width="0" layer="51" curve="-180"/>
<wire x1="0.3048" y1="1.4478" x2="-0.3048" y2="1.4478" width="0" layer="51" curve="-180"/>
<text x="-2.0066" y="1.3716" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-2.0066" y="1.3716" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="2.54" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-4.445" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ADC">
<wire x1="-7.62" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="VCC" x="-12.7" y="5.08" length="middle" direction="pwr"/>
<pin name="VSS" x="-12.7" y="2.54" length="middle" direction="pwr"/>
<pin name="AIN" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="SCL" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="SDA" x="-12.7" y="-5.08" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCP3021">
<gates>
<gate name="G$1" symbol="ADC" x="-7.62" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P280X145-5N">
<connects>
<connect gate="G$1" pin="AIN" pad="3"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="VCC" pad="1"/>
<connect gate="G$1" pin="VSS" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="j_usb_hirose_zx62d-b-5p8">
<packages>
<package name="ZX62D-B-5P8">
<description>Mounting pattern for Hirose ZX62D-B-5P8 Micro-B USB connector with SMD leads and through-hole shell connection. Modified from the manufacturer's recommendations by using round holes and leaving out shell SMD pads.</description>
<wire x1="-3.8" y1="-2.75" x2="3.8" y2="-2.75" width="0.15" layer="51"/>
<wire x1="-3.8" y1="3.35" x2="3.8" y2="3.35" width="0.15" layer="51"/>
<wire x1="-3.8" y1="3.35" x2="-3.8" y2="-1.45" width="0.15" layer="51"/>
<wire x1="-3.8" y1="-1.45" x2="-3.8" y2="-2.75" width="0.15" layer="51"/>
<wire x1="3.8" y1="3.35" x2="3.8" y2="-1.45" width="0.15" layer="51"/>
<wire x1="3.8" y1="-1.45" x2="3.8" y2="-2.75" width="0.15" layer="51"/>
<wire x1="-3.8" y1="-1.45" x2="3.8" y2="-1.45" width="0" layer="51"/>
<pad name="SH3" x="-3.6" y="0" drill="1" diameter="1.4"/>
<pad name="SH4" x="3.6" y="0" drill="1" diameter="1.4"/>
<pad name="SH2" x="2.425" y="3" drill="0.6" diameter="1"/>
<pad name="SH1" x="-2.425" y="3" drill="0.6" diameter="1" shape="square"/>
<smd name="P$1" x="-1.3" y="2.675" dx="1.35" dy="0.4" layer="1" rot="R90"/>
<smd name="P$2" x="-0.65" y="2.675" dx="1.35" dy="0.4" layer="1" rot="R90"/>
<smd name="P$3" x="0" y="2.675" dx="1.35" dy="0.4" layer="1" rot="R90"/>
<smd name="P$4" x="0.65" y="2.675" dx="1.35" dy="0.4" layer="1" roundness="1" rot="R90"/>
<smd name="P$5" x="1.3" y="2.675" dx="1.35" dy="0.4" layer="1" rot="R90"/>
<text x="-3.16" y="3.96" size="1.016" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-3.32" y="-4.52" size="1.016" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<rectangle x1="-3.8" y1="0.85" x2="3.8" y2="1.85" layer="41"/>
<rectangle x1="-3.8" y1="-1.45" x2="3.8" y2="-0.85" layer="41"/>
<rectangle x1="-2.8" y1="-0.85" x2="2.8" y2="0.85" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="ZX62D-B-5P8">
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-1.27" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="1.016" y2="-8.128" width="0.254" layer="94" curve="-53.130102"/>
<wire x1="1.016" y1="-8.128" x2="2.54" y2="-8.89" width="0.254" layer="94" curve="53.130102"/>
<wire x1="2.54" y1="-8.89" x2="5.08" y2="-8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="-8.89" x2="6.35" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="-1.27" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="1.016" y2="8.128" width="0.254" layer="94" curve="53.130102"/>
<wire x1="1.016" y1="8.128" x2="2.54" y2="8.89" width="0.254" layer="94" curve="-53.130102"/>
<wire x1="2.54" y1="8.89" x2="5.08" y2="8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="8.89" x2="6.35" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="3.81" y2="-6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="-6.35" x2="3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="6.35" x2="1.27" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="0" y2="-10.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="0" y1="-10.16" x2="2.54" y2="-10.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="2.54" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="-2.54" y1="-11.43" x2="-2.54" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="0" y1="-11.43" x2="0" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-11.43" x2="2.54" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="7.62" y2="-7.62" width="0.254" layer="94" style="shortdash" curve="90"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="7.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="7.62" y1="7.62" x2="5.08" y2="10.16" width="0.254" layer="94" style="shortdash" curve="90"/>
<wire x1="5.08" y1="10.16" x2="0" y2="10.16" width="0.254" layer="94" style="shortdash"/>
<text x="-5.588" y="10.16" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="7.62" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="5.08" visible="pin" direction="pas"/>
<pin name="2" x="-5.08" y="2.54" visible="pin" direction="pas"/>
<pin name="3" x="-5.08" y="0" visible="pin" direction="pas"/>
<pin name="4" x="-5.08" y="-2.54" visible="pin" direction="pas"/>
<pin name="5" x="-5.08" y="-5.08" visible="pin" direction="pas"/>
<pin name="SH1" x="-2.54" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="SH2" x="0" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="SH3" x="2.54" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="SH4" x="5.08" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ZX62D-B-5P8">
<description>Hirose ZX62D-B-5P8 Micro-B USB connector, standard mount location, with through-hold shell tabs and SMD pins.</description>
<gates>
<gate name="G$1" symbol="ZX62D-B-5P8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ZX62D-B-5P8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="SH1" pad="SH1"/>
<connect gate="G$1" pin="SH2" pad="SH2"/>
<connect gate="G$1" pin="SH3" pad="SH3"/>
<connect gate="G$1" pin="SH4" pad="SH4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-hirschmann">
<description>&lt;b&gt;Hirschmann Connectors&lt;/b&gt;&lt;p&gt;
Audio, scart, microphone, headphone&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="TOBU3">
<description>Female Cinch &lt;B&gt;CONNECTOR&lt;/B&gt; ( RCA Jack )</description>
<wire x1="2.286" y1="5.08" x2="4.826" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-5.588" y1="-0.127" x2="-5.588" y2="4.318" width="0.6096" layer="21"/>
<wire x1="5.588" y1="-0.127" x2="5.588" y2="-3.302" width="0.6096" layer="51"/>
<wire x1="-5.588" y1="-0.127" x2="-5.588" y2="-3.302" width="0.6096" layer="51"/>
<wire x1="1.397" y1="-2.54" x2="-1.397" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-2.54" x2="-1.397" y2="3.429" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-2.54" x2="1.397" y2="3.429" width="0.1524" layer="21"/>
<wire x1="1.397" y1="3.429" x2="1.397" y2="5.08" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="3.429" x2="-1.397" y2="5.08" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="5.08" x2="-2.286" y2="5.08" width="0.6096" layer="51"/>
<wire x1="-1.397" y1="5.08" x2="1.397" y2="5.08" width="0.6096" layer="51"/>
<wire x1="1.397" y1="5.08" x2="2.286" y2="5.08" width="0.6096" layer="51"/>
<wire x1="4.826" y1="5.08" x2="5.588" y2="4.318" width="0.6096" layer="21" curve="-90"/>
<wire x1="5.588" y1="4.318" x2="5.588" y2="-0.127" width="0.6096" layer="21"/>
<wire x1="-5.588" y1="4.318" x2="-4.826" y2="5.08" width="0.6096" layer="21" curve="-90"/>
<wire x1="-4.826" y1="5.08" x2="-2.286" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="5.334" x2="-4.191" y2="5.842" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.191" y1="11.303" x2="-3.683" y2="11.811" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.683" y1="11.811" x2="4.191" y2="11.303" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.191" y1="5.842" x2="4.699" y2="5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.191" y1="5.842" x2="-4.191" y2="11.303" width="0.1524" layer="21"/>
<wire x1="4.191" y1="11.303" x2="4.191" y2="5.842" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="11.811" x2="-3.048" y2="11.811" width="0.1524" layer="21"/>
<wire x1="4.191" y1="5.842" x2="2.159" y2="5.842" width="0.1524" layer="21"/>
<wire x1="2.159" y1="5.842" x2="-2.159" y2="5.842" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="5.842" x2="-4.191" y2="5.842" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="11.303" x2="3.81" y2="11.303" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-2.286" x2="-1.397" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="6.477" x2="-3.556" y2="10.668" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="6.477" x2="-3.048" y2="9.017" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.477" x2="-2.54" y2="7.874" width="0.1524" layer="21"/>
<wire x1="3.556" y1="6.477" x2="3.556" y2="10.668" width="0.1524" layer="21"/>
<wire x1="3.048" y1="6.477" x2="3.048" y2="9.017" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.477" x2="2.54" y2="7.874" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="11.811" x2="-3.048" y2="11.938" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="11.811" x2="3.048" y2="11.811" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="11.938" x2="3.048" y2="11.938" width="0.1524" layer="21"/>
<wire x1="3.048" y1="11.938" x2="3.048" y2="11.811" width="0.1524" layer="21"/>
<wire x1="3.048" y1="11.811" x2="3.683" y2="11.811" width="0.1524" layer="21"/>
<pad name="1" x="0" y="-2.54" drill="1.397" shape="octagon"/>
<pad name="2" x="5.588" y="-2.54" drill="2.2098" shape="octagon"/>
<pad name="2@1" x="-5.588" y="-2.54" drill="2.2098" shape="octagon"/>
<pad name="2@2" x="0" y="5.08" drill="2.2098" shape="octagon"/>
<text x="-4.826" y="5.842" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.096" y="5.842" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="1.905" y="-1.905" size="1.27" layer="21" ratio="10">1</text>
<text x="6.35" y="0" size="1.27" layer="21" ratio="10">2</text>
<text x="-4.826" y="0" size="1.27" layer="21" ratio="10">2 </text>
</package>
<package name="TOBU3_EXT">
<wire x1="2.286" y1="5.16" x2="4.826" y2="5.16" width="0.1524" layer="21"/>
<wire x1="-5.588" y1="-0.047" x2="-5.588" y2="4.398" width="0.6096" layer="21"/>
<wire x1="5.588" y1="-0.047" x2="5.588" y2="-3.222" width="0.6096" layer="51"/>
<wire x1="-5.588" y1="-0.047" x2="-5.588" y2="-3.222" width="0.6096" layer="51"/>
<wire x1="1.397" y1="-2.46" x2="-1.397" y2="-2.46" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-2.46" x2="-1.397" y2="3.509" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-2.46" x2="1.397" y2="3.509" width="0.1524" layer="21"/>
<wire x1="1.397" y1="3.509" x2="1.397" y2="5.16" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="3.509" x2="-1.397" y2="5.16" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="5.16" x2="-2.286" y2="5.16" width="0.6096" layer="51"/>
<wire x1="-1.397" y1="5.16" x2="1.397" y2="5.16" width="0.6096" layer="51"/>
<wire x1="1.397" y1="5.16" x2="2.286" y2="5.16" width="0.6096" layer="51"/>
<wire x1="4.826" y1="5.16" x2="5.588" y2="4.398" width="0.6096" layer="21" curve="-90"/>
<wire x1="5.588" y1="4.398" x2="5.588" y2="-0.047" width="0.6096" layer="21"/>
<wire x1="-5.588" y1="4.398" x2="-4.826" y2="5.16" width="0.6096" layer="21" curve="-90"/>
<wire x1="-4.826" y1="5.16" x2="-2.286" y2="5.16" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="5.414" x2="-4.191" y2="5.922" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.191" y1="11.383" x2="-3.683" y2="11.891" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.683" y1="11.891" x2="4.191" y2="11.383" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.191" y1="5.922" x2="4.699" y2="5.414" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.191" y1="5.922" x2="-4.191" y2="11.383" width="0.1524" layer="21"/>
<wire x1="4.191" y1="11.383" x2="4.191" y2="5.922" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="11.891" x2="-3.048" y2="11.891" width="0.1524" layer="21"/>
<wire x1="4.191" y1="5.922" x2="2.159" y2="5.922" width="0.1524" layer="21"/>
<wire x1="2.159" y1="5.922" x2="-2.159" y2="5.922" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="5.922" x2="-4.191" y2="5.922" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="11.383" x2="3.81" y2="11.383" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-2.206" x2="-1.397" y2="-2.206" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="6.557" x2="-3.556" y2="10.748" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="6.557" x2="-3.048" y2="9.097" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.557" x2="-2.54" y2="7.954" width="0.1524" layer="21"/>
<wire x1="3.556" y1="6.557" x2="3.556" y2="10.748" width="0.1524" layer="21"/>
<wire x1="3.048" y1="6.557" x2="3.048" y2="9.097" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.557" x2="2.54" y2="7.954" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="11.891" x2="-3.048" y2="12.018" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="11.891" x2="3.048" y2="11.891" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="12.018" x2="3.048" y2="12.018" width="0.1524" layer="21"/>
<wire x1="3.048" y1="12.018" x2="3.048" y2="11.891" width="0.1524" layer="21"/>
<wire x1="3.048" y1="11.891" x2="3.683" y2="11.891" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.7018" shape="octagon"/>
<pad name="2" x="5" y="0" drill="2.5908" shape="octagon"/>
<pad name="2@1" x="-5" y="0" drill="2.5908" shape="octagon"/>
<pad name="2@2" x="0" y="4.5" drill="2.5908" shape="octagon"/>
<text x="-4.826" y="5.922" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.096" y="5.922" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="1.905" y="-1.825" size="1.27" layer="21" ratio="10">1</text>
<text x="6.35" y="0.08" size="1.27" layer="21" ratio="10">2</text>
<text x="-4.826" y="0.08" size="1.27" layer="21" ratio="10">2 </text>
</package>
</packages>
<symbols>
<symbol name="TBU">
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.635" width="0.1524" layer="94"/>
<text x="-1.524" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TOBU3" prefix="X">
<description>Female Cinch &lt;B&gt;CONNECTOR&lt;/B&gt; ( RCA Jack )</description>
<gates>
<gate name="B" symbol="TBU" x="2.54" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="TOBU3">
<connects>
<connect gate="B" pin="1" pad="1"/>
<connect gate="B" pin="2" pad="2 2@1 2@2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="FARNELL" constant="no"/>
<attribute name="MPN" value="TOBU 3" constant="no"/>
<attribute name="OC_FARNELL" value="592791" constant="no"/>
<attribute name="OC_NEWARK" value="06WX5914" constant="no"/>
</technology>
</technologies>
</device>
<device name="MINE" package="TOBU3_EXT">
<connects>
<connect gate="B" pin="1" pad="1"/>
<connect gate="B" pin="2" pad="2 2@1 2@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-OPL-Switch">
<packages>
<package name="SW4-SMD-4.0X4.0X1.7MM">
<smd name="1" x="-3.1" y="1.85" dx="1.8" dy="1.1" layer="1"/>
<smd name="2" x="3.1" y="1.85" dx="1.8" dy="1.1" layer="1"/>
<smd name="3" x="-3.1" y="-1.85" dx="1.8" dy="1.1" layer="1"/>
<smd name="4" x="3.1" y="-1.85" dx="1.8" dy="1.1" layer="1"/>
<wire x1="-2.5" y1="1.206409375" x2="-2.5" y2="-1.251525" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1.251525" x2="-1.251525" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-1.251525" y1="-2.5" x2="1.206409375" y2="-2.5" width="0.127" layer="21"/>
<wire x1="1.206409375" y1="-2.5" x2="2.5" y2="-1.206409375" width="0.127" layer="21"/>
<wire x1="2.5" y1="-1.206409375" x2="2.5" y2="1.251525" width="0.127" layer="21"/>
<wire x1="2.5" y1="1.251525" x2="1.251525" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.251525" y1="2.5" x2="-1.206409375" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.206409375" y1="2.5" x2="-2.5" y2="1.206409375" width="0.127" layer="21"/>
<rectangle x1="-2.54" y1="-2.54" x2="2.54" y2="2.54" layer="39"/>
<text x="-2.413" y="2.794" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="0" size="0.635" layer="27" ratio="11">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="BOTTON-4P">
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="-5.08" y="5.08" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-6.35" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="A0" x="-8.89" y="2.54" length="short"/>
<pin name="A1" x="8.89" y="2.54" length="short" rot="R180"/>
<pin name="B0" x="-8.89" y="-2.54" length="short"/>
<pin name="B1" x="8.89" y="-2.54" length="short" rot="R180"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="6.35" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<circle x="-1.27" y="-0.762" radius="0.1524" width="0" layer="94"/>
<circle x="0" y="-1.27" radius="0.1524" width="0" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-BUTTON(4P-4.0X4.0X1.7MM)" prefix="SW" uservalue="yes">
<description>311020027</description>
<gates>
<gate name="G$1" symbol="BOTTON-4P" x="0" y="0"/>
</gates>
<devices>
<device name="-DHT-1152" package="SW4-SMD-4.0X4.0X1.7MM">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="B0" pad="3"/>
<connect gate="G$1" pin="B1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="header127">
<packages>
<package name="HDR_SML_36">
<pad name="P$1" x="-21.59" y="0" drill="0.65" shape="octagon"/>
<pad name="P$2" x="-20.32" y="0" drill="0.65" shape="octagon"/>
<pad name="P$3" x="-19.05" y="0" drill="0.65" shape="octagon"/>
<pad name="P$4" x="-17.78" y="0" drill="0.65" shape="octagon"/>
<pad name="P$5" x="-16.51" y="0" drill="0.65" shape="octagon"/>
<pad name="P$6" x="-15.24" y="0" drill="0.65" shape="octagon"/>
<pad name="P$7" x="-13.97" y="0" drill="0.65" shape="octagon"/>
<pad name="P$8" x="-12.7" y="0" drill="0.65" shape="octagon"/>
<pad name="P$9" x="-11.43" y="0" drill="0.65" shape="octagon"/>
<pad name="P$10" x="-10.16" y="0" drill="0.65" shape="octagon"/>
<pad name="P$11" x="-8.89" y="0" drill="0.65" shape="octagon"/>
<pad name="P$12" x="-7.62" y="0" drill="0.65" shape="octagon"/>
<pad name="P$13" x="-6.35" y="0" drill="0.65" shape="octagon"/>
<pad name="P$14" x="-5.08" y="0" drill="0.65" shape="octagon"/>
<pad name="P$15" x="-3.81" y="0" drill="0.65" shape="octagon"/>
<pad name="P$16" x="-2.54" y="0" drill="0.65" shape="octagon"/>
<pad name="P$17" x="-1.27" y="0" drill="0.65" shape="octagon"/>
<pad name="P$18" x="0" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$19" x="1.27" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$20" x="2.54" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$21" x="3.81" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$22" x="5.08" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$23" x="6.35" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$24" x="7.62" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$25" x="8.89" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$26" x="10.16" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$27" x="11.43" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$28" x="12.7" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$29" x="13.97" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$30" x="15.24" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$31" x="16.51" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$32" x="17.78" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$33" x="19.05" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$34" x="20.32" y="0" drill="0.65" shape="octagon" rot="R180"/>
<pad name="P$35" x="21.59" y="0" drill="0.65" shape="octagon"/>
<pad name="P$36" x="22.86" y="0" drill="0.65" shape="octagon"/>
</package>
<package name="HDR_141_36">
<pad name="P$1" x="-25.38" y="0" drill="0.65" shape="octagon"/>
<pad name="P$2" x="-23.97" y="0" drill="0.65" shape="octagon"/>
<pad name="P$3" x="-22.56" y="0" drill="0.65" shape="octagon"/>
<pad name="P$4" x="-21.15" y="0" drill="0.65" shape="octagon"/>
<pad name="P$5" x="-19.74" y="0" drill="0.65" shape="octagon"/>
<pad name="P$6" x="-18.33" y="0" drill="0.65" shape="octagon"/>
<pad name="P$7" x="-16.92" y="0" drill="0.65" shape="octagon"/>
<pad name="P$8" x="-15.51" y="0" drill="0.65" shape="octagon"/>
<pad name="P$9" x="-14.1" y="0" drill="0.65" shape="octagon"/>
<pad name="P$10" x="-12.69" y="0" drill="0.65" shape="octagon"/>
<pad name="P$11" x="-11.28" y="0" drill="0.65" shape="octagon"/>
<pad name="P$12" x="-9.87" y="0" drill="0.65" shape="octagon"/>
<pad name="P$13" x="-8.46" y="0" drill="0.65" shape="octagon"/>
<pad name="P$14" x="-7.05" y="0" drill="0.65" shape="octagon"/>
<pad name="P$15" x="-5.64" y="0" drill="0.65" shape="octagon"/>
<pad name="P$16" x="-4.23" y="0" drill="0.65" shape="octagon"/>
<pad name="P$17" x="-2.82" y="0" drill="0.65" shape="octagon"/>
<pad name="P$18" x="-1.41" y="0" drill="0.65" shape="octagon"/>
<pad name="P$19" x="0" y="0" drill="0.65" shape="octagon"/>
<pad name="P$20" x="1.41" y="0" drill="0.65" shape="octagon"/>
<pad name="P$21" x="2.82" y="0" drill="0.65" shape="octagon"/>
<pad name="P$22" x="4.23" y="0" drill="0.65" shape="octagon"/>
<pad name="P$23" x="5.64" y="0" drill="0.65" shape="octagon"/>
<pad name="P$24" x="7.05" y="0" drill="0.65" shape="octagon"/>
<pad name="P$25" x="8.46" y="0" drill="0.65" shape="octagon"/>
<pad name="P$26" x="9.87" y="0" drill="0.65" shape="octagon"/>
<pad name="P$27" x="11.28" y="0" drill="0.65" shape="octagon"/>
<pad name="P$28" x="12.69" y="0" drill="0.65" shape="octagon"/>
<pad name="P$29" x="14.1" y="0" drill="0.65" shape="octagon"/>
<pad name="P$30" x="15.51" y="0" drill="0.65" shape="octagon"/>
<pad name="P$31" x="16.92" y="0" drill="0.65" shape="octagon"/>
<pad name="P$32" x="18.33" y="0" drill="0.65" shape="octagon"/>
<pad name="P$33" x="19.74" y="0" drill="0.65" shape="octagon"/>
<pad name="P$34" x="21.15" y="0" drill="0.65" shape="octagon"/>
<pad name="P$35" x="22.56" y="0" drill="0.65" shape="octagon"/>
<pad name="P$36" x="23.97" y="0" drill="0.65" shape="octagon"/>
</package>
</packages>
<symbols>
<symbol name="DE1_GPIO_L">
<wire x1="10.16" y1="40.64" x2="-2.54" y2="40.64" width="0.254" layer="94"/>
<wire x1="-2.54" y1="40.64" x2="-2.54" y2="-53.34" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-53.34" x2="10.16" y2="-53.34" width="0.254" layer="94"/>
<wire x1="10.16" y1="-53.34" x2="10.16" y2="40.64" width="0.254" layer="94"/>
<pin name="GPIO_0" x="-7.62" y="38.1" visible="pin" length="middle"/>
<pin name="GPIO_1" x="-7.62" y="35.56" visible="pin" length="middle"/>
<pin name="GPIO_2" x="-7.62" y="33.02" visible="pin" length="middle"/>
<pin name="GPIO_3" x="-7.62" y="30.48" visible="pin" length="middle"/>
<pin name="GPIO_4" x="-7.62" y="27.94" visible="pin" length="middle"/>
<pin name="GPIO_5" x="-7.62" y="25.4" visible="pin" length="middle"/>
<pin name="GPIO_6" x="-7.62" y="22.86" visible="pin" length="middle"/>
<pin name="GPIO_7" x="-7.62" y="20.32" visible="pin" length="middle"/>
<pin name="GPIO_8" x="-7.62" y="17.78" visible="pin" length="middle"/>
<pin name="GPIO_9" x="-7.62" y="15.24" visible="pin" length="middle"/>
<pin name="GPIO_10" x="-7.62" y="12.7" visible="pin" length="middle"/>
<pin name="GPIO_11" x="-7.62" y="10.16" visible="pin" length="middle"/>
<pin name="GPIO_12" x="-7.62" y="7.62" visible="pin" length="middle"/>
<pin name="GPIO_13" x="-7.62" y="5.08" visible="pin" length="middle"/>
<pin name="GPIO_14" x="-7.62" y="2.54" visible="pin" length="middle"/>
<pin name="GPIO_15" x="-7.62" y="0" visible="pin" length="middle"/>
<pin name="GPIO_16" x="-7.62" y="-2.54" visible="pin" length="middle"/>
<pin name="GPIO_17" x="-7.62" y="-5.08" visible="pin" length="middle"/>
<pin name="GPIO_18" x="-7.62" y="-7.62" visible="pin" length="middle"/>
<pin name="GPIO_19" x="-7.62" y="-10.16" visible="pin" length="middle"/>
<pin name="GPIO_20" x="-7.62" y="-12.7" visible="pin" length="middle"/>
<pin name="GPIO_21" x="-7.62" y="-15.24" visible="pin" length="middle"/>
<pin name="GPIO_22" x="-7.62" y="-17.78" visible="pin" length="middle"/>
<pin name="GPIO_23" x="-7.62" y="-20.32" visible="pin" length="middle"/>
<pin name="GPIO_24" x="-7.62" y="-22.86" visible="pin" length="middle"/>
<pin name="GPIO_25" x="-7.62" y="-25.4" visible="pin" length="middle"/>
<pin name="GPIO_26" x="-7.62" y="-27.94" visible="pin" length="middle"/>
<pin name="GPIO_27" x="-7.62" y="-30.48" visible="pin" length="middle"/>
<pin name="GPIO_28" x="-7.62" y="-33.02" visible="pin" length="middle"/>
<pin name="GPIO_29" x="-7.62" y="-35.56" visible="pin" length="middle"/>
<pin name="GPIO_30" x="-7.62" y="-38.1" visible="pin" length="middle"/>
<pin name="GPIO_31" x="-7.62" y="-40.64" visible="pin" length="middle"/>
<pin name="GPIO_32" x="-7.62" y="-43.18" visible="pin" length="middle"/>
<pin name="GPIO_33" x="-7.62" y="-45.72" visible="pin" length="middle"/>
<pin name="GPIO_34" x="-7.62" y="-48.26" visible="pin" length="middle"/>
<pin name="GPIO_35" x="-7.62" y="-50.8" visible="pin" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HDR_SMALL_36_DEV_L">
<gates>
<gate name="G$1" symbol="DE1_GPIO_L" x="-2.54" y="5.08"/>
</gates>
<devices>
<device name="" package="HDR_SML_36">
<connects>
<connect gate="G$1" pin="GPIO_0" pad="P$1"/>
<connect gate="G$1" pin="GPIO_1" pad="P$2"/>
<connect gate="G$1" pin="GPIO_10" pad="P$11"/>
<connect gate="G$1" pin="GPIO_11" pad="P$12"/>
<connect gate="G$1" pin="GPIO_12" pad="P$13"/>
<connect gate="G$1" pin="GPIO_13" pad="P$14"/>
<connect gate="G$1" pin="GPIO_14" pad="P$15"/>
<connect gate="G$1" pin="GPIO_15" pad="P$16"/>
<connect gate="G$1" pin="GPIO_16" pad="P$17"/>
<connect gate="G$1" pin="GPIO_17" pad="P$18"/>
<connect gate="G$1" pin="GPIO_18" pad="P$19"/>
<connect gate="G$1" pin="GPIO_19" pad="P$20"/>
<connect gate="G$1" pin="GPIO_2" pad="P$3"/>
<connect gate="G$1" pin="GPIO_20" pad="P$21"/>
<connect gate="G$1" pin="GPIO_21" pad="P$22"/>
<connect gate="G$1" pin="GPIO_22" pad="P$23"/>
<connect gate="G$1" pin="GPIO_23" pad="P$24"/>
<connect gate="G$1" pin="GPIO_24" pad="P$25"/>
<connect gate="G$1" pin="GPIO_25" pad="P$26"/>
<connect gate="G$1" pin="GPIO_26" pad="P$27"/>
<connect gate="G$1" pin="GPIO_27" pad="P$28"/>
<connect gate="G$1" pin="GPIO_28" pad="P$29"/>
<connect gate="G$1" pin="GPIO_29" pad="P$30"/>
<connect gate="G$1" pin="GPIO_3" pad="P$4"/>
<connect gate="G$1" pin="GPIO_30" pad="P$31"/>
<connect gate="G$1" pin="GPIO_31" pad="P$32"/>
<connect gate="G$1" pin="GPIO_32" pad="P$33"/>
<connect gate="G$1" pin="GPIO_33" pad="P$34"/>
<connect gate="G$1" pin="GPIO_34" pad="P$35"/>
<connect gate="G$1" pin="GPIO_35" pad="P$36"/>
<connect gate="G$1" pin="GPIO_4" pad="P$5"/>
<connect gate="G$1" pin="GPIO_5" pad="P$6"/>
<connect gate="G$1" pin="GPIO_6" pad="P$7"/>
<connect gate="G$1" pin="GPIO_7" pad="P$8"/>
<connect gate="G$1" pin="GPIO_8" pad="P$9"/>
<connect gate="G$1" pin="GPIO_9" pad="P$10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ALIGNED" package="HDR_141_36">
<connects>
<connect gate="G$1" pin="GPIO_0" pad="P$1"/>
<connect gate="G$1" pin="GPIO_1" pad="P$2"/>
<connect gate="G$1" pin="GPIO_10" pad="P$11"/>
<connect gate="G$1" pin="GPIO_11" pad="P$12"/>
<connect gate="G$1" pin="GPIO_12" pad="P$13"/>
<connect gate="G$1" pin="GPIO_13" pad="P$14"/>
<connect gate="G$1" pin="GPIO_14" pad="P$15"/>
<connect gate="G$1" pin="GPIO_15" pad="P$16"/>
<connect gate="G$1" pin="GPIO_16" pad="P$17"/>
<connect gate="G$1" pin="GPIO_17" pad="P$18"/>
<connect gate="G$1" pin="GPIO_18" pad="P$19"/>
<connect gate="G$1" pin="GPIO_19" pad="P$20"/>
<connect gate="G$1" pin="GPIO_2" pad="P$3"/>
<connect gate="G$1" pin="GPIO_20" pad="P$21"/>
<connect gate="G$1" pin="GPIO_21" pad="P$22"/>
<connect gate="G$1" pin="GPIO_22" pad="P$23"/>
<connect gate="G$1" pin="GPIO_23" pad="P$24"/>
<connect gate="G$1" pin="GPIO_24" pad="P$25"/>
<connect gate="G$1" pin="GPIO_25" pad="P$26"/>
<connect gate="G$1" pin="GPIO_26" pad="P$27"/>
<connect gate="G$1" pin="GPIO_27" pad="P$28"/>
<connect gate="G$1" pin="GPIO_28" pad="P$29"/>
<connect gate="G$1" pin="GPIO_29" pad="P$30"/>
<connect gate="G$1" pin="GPIO_3" pad="P$4"/>
<connect gate="G$1" pin="GPIO_30" pad="P$31"/>
<connect gate="G$1" pin="GPIO_31" pad="P$32"/>
<connect gate="G$1" pin="GPIO_32" pad="P$33"/>
<connect gate="G$1" pin="GPIO_33" pad="P$34"/>
<connect gate="G$1" pin="GPIO_34" pad="P$35"/>
<connect gate="G$1" pin="GPIO_35" pad="P$36"/>
<connect gate="G$1" pin="GPIO_4" pad="P$5"/>
<connect gate="G$1" pin="GPIO_5" pad="P$6"/>
<connect gate="G$1" pin="GPIO_6" pad="P$7"/>
<connect gate="G$1" pin="GPIO_7" pad="P$8"/>
<connect gate="G$1" pin="GPIO_8" pad="P$9"/>
<connect gate="G$1" pin="GPIO_9" pad="P$10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="epcs">
<packages>
<package name="EPCS_BOTH">
<smd name="1" x="-2.4638" y="1.905" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="2" x="-2.4638" y="0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="3" x="-2.4638" y="-0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="4" x="-2.4638" y="-1.905" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="5" x="2.4638" y="-1.905" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="6" x="2.4638" y="-0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="7" x="2.4638" y="0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="8" x="2.4638" y="1.905" dx="1.9812" dy="0.5588" layer="1"/>
<wire x1="-2.0066" y1="1.651" x2="-2.0066" y2="2.159" width="0" layer="51"/>
<wire x1="-2.0066" y1="2.159" x2="-3.0988" y2="2.159" width="0" layer="51"/>
<wire x1="-3.0988" y1="2.159" x2="-3.0988" y2="1.651" width="0" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-2.0066" y2="1.651" width="0" layer="51"/>
<wire x1="-2.0066" y1="0.381" x2="-2.0066" y2="0.889" width="0" layer="51"/>
<wire x1="-2.0066" y1="0.889" x2="-3.0988" y2="0.889" width="0" layer="51"/>
<wire x1="-3.0988" y1="0.889" x2="-3.0988" y2="0.381" width="0" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-2.0066" y2="0.381" width="0" layer="51"/>
<wire x1="-2.0066" y1="-0.889" x2="-2.0066" y2="-0.381" width="0" layer="51"/>
<wire x1="-2.0066" y1="-0.381" x2="-3.0988" y2="-0.381" width="0" layer="51"/>
<wire x1="-3.0988" y1="-0.381" x2="-3.0988" y2="-0.889" width="0" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-2.0066" y2="-0.889" width="0" layer="51"/>
<wire x1="-2.0066" y1="-2.159" x2="-2.0066" y2="-1.651" width="0" layer="51"/>
<wire x1="-2.0066" y1="-1.651" x2="-3.0988" y2="-1.651" width="0" layer="51"/>
<wire x1="-3.0988" y1="-1.651" x2="-3.0988" y2="-2.159" width="0" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-2.0066" y2="-2.159" width="0" layer="51"/>
<wire x1="2.0066" y1="-1.651" x2="2.0066" y2="-2.159" width="0" layer="51"/>
<wire x1="2.0066" y1="-2.159" x2="3.0988" y2="-2.159" width="0" layer="51"/>
<wire x1="3.0988" y1="-2.159" x2="3.0988" y2="-1.651" width="0" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="2.0066" y2="-1.651" width="0" layer="51"/>
<wire x1="2.0066" y1="-0.381" x2="2.0066" y2="-0.889" width="0" layer="51"/>
<wire x1="2.0066" y1="-0.889" x2="3.0988" y2="-0.889" width="0" layer="51"/>
<wire x1="3.0988" y1="-0.889" x2="3.0988" y2="-0.381" width="0" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="2.0066" y2="-0.381" width="0" layer="51"/>
<wire x1="2.0066" y1="0.889" x2="2.0066" y2="0.381" width="0" layer="51"/>
<wire x1="2.0066" y1="0.381" x2="3.0988" y2="0.381" width="0" layer="51"/>
<wire x1="3.0988" y1="0.381" x2="3.0988" y2="0.889" width="0" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="2.0066" y2="0.889" width="0" layer="51"/>
<wire x1="2.0066" y1="2.159" x2="2.0066" y2="1.651" width="0" layer="51"/>
<wire x1="2.0066" y1="1.651" x2="3.0988" y2="1.651" width="0" layer="51"/>
<wire x1="3.0988" y1="1.651" x2="3.0988" y2="2.159" width="0" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="2.0066" y2="2.159" width="0" layer="51"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0" layer="51"/>
<wire x1="2.0066" y1="-2.4892" x2="2.0066" y2="2.4892" width="0" layer="51"/>
<wire x1="2.0066" y1="2.4892" x2="0.3048" y2="2.4892" width="0" layer="51"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0" layer="51"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0" layer="51"/>
<wire x1="-2.0066" y1="2.4892" x2="-2.0066" y2="-2.4892" width="0" layer="51"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0" layer="51" curve="-180"/>
<wire x1="-1.3716" y1="-2.4892" x2="1.3716" y2="-2.4892" width="0.1524" layer="21"/>
<wire x1="1.3716" y1="2.4892" x2="0.3048" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.4892" x2="-1.3716" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="21" curve="-180"/>
<text x="-2.794" y="1.5748" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-3.4798" y="-8.0518" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="5.5118" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<wire x1="-5.25" y1="3.75" x2="5.25" y2="3.75" width="0.127" layer="21"/>
<wire x1="5.25" y1="3.75" x2="5.25" y2="-3.75" width="0.127" layer="21"/>
<wire x1="5.25" y1="-3.75" x2="-5.25" y2="-3.75" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-3.75" x2="-5.25" y2="3.75" width="0.127" layer="21"/>
<smd name="L$9" x="-4.48" y="-4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$10" x="-3.21" y="-4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$11" x="-1.94" y="-4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$12" x="-0.67" y="-4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$13" x="0.6" y="-4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$14" x="1.87" y="-4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$15" x="3.14" y="-4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$16" x="4.41" y="-4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$1" x="4.41" y="4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$2" x="3.14" y="4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$3" x="1.87" y="4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$4" x="0.6" y="4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$5" x="-0.67" y="4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$6" x="-1.94" y="4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$7" x="-3.21" y="4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<smd name="L$8" x="-4.48" y="4.21" dx="1.9812" dy="0.5588" layer="1" rot="R90"/>
<text x="6.2992" y="3.81" size="1.27" layer="21" ratio="6" rot="SR90">*</text>
<wire x1="5.334" y1="0.762" x2="5.334" y2="-0.762" width="0.127" layer="21" curve="180"/>
</package>
</packages>
<symbols>
<symbol name="AT25320B-SSHL-B">
<pin name="VCC" x="-17.78" y="7.62" length="middle" direction="pwr"/>
<pin name="SCK" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="~CS" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="~WP" x="-17.78" y="-2.54" length="middle" direction="in"/>
<pin name="SI" x="-17.78" y="-5.08" length="middle" direction="in"/>
<pin name="~HOLD" x="-17.78" y="-7.62" length="middle" direction="in"/>
<pin name="GND" x="-17.78" y="-12.7" length="middle" direction="pas"/>
<pin name="SO" x="17.78" y="7.62" length="middle" direction="out" rot="R180"/>
<wire x1="-12.7" y1="12.7" x2="-12.7" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-17.78" x2="12.7" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="12.7" y2="12.7" width="0.4064" layer="94"/>
<wire x1="12.7" y1="12.7" x2="-12.7" y2="12.7" width="0.4064" layer="94"/>
<text x="-5.6642" y="14.9352" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.8862" y="-22.8092" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="EPCS" prefix="U">
<description>SPI Serial EEPROM</description>
<gates>
<gate name="A" symbol="AT25320B-SSHL-B" x="0" y="0"/>
</gates>
<devices>
<device name="BOTH" package="EPCS_BOTH">
<connects>
<connect gate="A" pin="GND" pad="4 L$10"/>
<connect gate="A" pin="SCK" pad="6 L$16"/>
<connect gate="A" pin="SI" pad="5 L$15"/>
<connect gate="A" pin="SO" pad="2 L$8"/>
<connect gate="A" pin="VCC" pad="8 L$2"/>
<connect gate="A" pin="~CS" pad="1 L$7"/>
<connect gate="A" pin="~HOLD" pad="7 L$1"/>
<connect gate="A" pin="~WP" pad="3 L$9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="AT25320B-SSHL-B" constant="no"/>
<attribute name="OC_FARNELL" value="1841578" constant="no"/>
<attribute name="OC_NEWARK" value="12T1359" constant="no"/>
<attribute name="PACKAGE" value="SOIC-8" constant="no"/>
<attribute name="SUPPLIER" value="Atmel" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X04">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.1562" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
</package>
<package name="1X04/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-5.715" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.985" y="-4.445" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PINHD4">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X4" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X04">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X04/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="holes">
<description>&lt;b&gt;Mounting Holes and Pads&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2,8-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 2.8 mm, round</description>
<wire x1="0" y1="2.921" x2="0" y2="2.667" width="0.0508" layer="21"/>
<wire x1="0" y1="-2.667" x2="0" y2="-2.921" width="0.0508" layer="21"/>
<wire x1="-1.778" y1="0" x2="0" y2="-1.778" width="2.286" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="1.778" x2="1.778" y2="0" width="2.286" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="0.635" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="2.921" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="39"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="40"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="43"/>
<circle x="0" y="0" radius="1.5" width="0.2032" layer="21"/>
<pad name="B2,8" x="0" y="0" drill="2.8" diameter="5.334"/>
</package>
<package name="3,0-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.0 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="39"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.6" width="0.2032" layer="21"/>
<pad name="B3,0" x="0" y="0" drill="3" diameter="5.842"/>
<text x="-1.27" y="-3.81" size="1.27" layer="48">3,0</text>
</package>
<package name="3,2-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.2 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.7" width="0.1524" layer="21"/>
<pad name="B3,2" x="0" y="0" drill="3.2" diameter="5.842"/>
<text x="-1.27" y="-3.81" size="1.27" layer="48">3,2</text>
</package>
<package name="3,3-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.3 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.7" width="0.2032" layer="21"/>
<pad name="B3,3" x="0" y="0" drill="3.3" diameter="5.842"/>
</package>
<package name="3,6-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.6 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.397" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.397" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.9" width="0.2032" layer="21"/>
<pad name="B3,6" x="0" y="0" drill="3.6" diameter="5.842"/>
</package>
<package name="4,1-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.1 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="5.08" width="2" layer="43"/>
<circle x="0" y="0" radius="2.15" width="0.2032" layer="21"/>
<pad name="B4,1" x="0" y="0" drill="4.1" diameter="8"/>
</package>
<package name="4,3-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.3 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.25" width="0.1524" layer="21"/>
<pad name="B4,3" x="0" y="0" drill="4.3" diameter="9"/>
</package>
<package name="4,5-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.5 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.35" width="0.1524" layer="21"/>
<pad name="B4,5" x="0" y="0" drill="4.5" diameter="9"/>
</package>
<package name="5,0-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 5.0 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.6" width="0.1524" layer="21"/>
<pad name="B5" x="0" y="0" drill="5" diameter="9"/>
</package>
<package name="5,5-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 5.5 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.85" width="0.1524" layer="21"/>
<pad name="B5,5" x="0" y="0" drill="5.5" diameter="9"/>
</package>
<package name="2,5-PAD">
<pad name="B2,8" x="0" y="0" drill="2.5273" diameter="5"/>
<circle x="0" y="0" radius="1.26365" width="0" layer="31"/>
</package>
</packages>
<symbols>
<symbol name="MOUNT-PAD">
<wire x1="0.254" y1="2.032" x2="2.032" y2="0.254" width="1.016" layer="94" curve="-75.749967" cap="flat"/>
<wire x1="-2.032" y1="0.254" x2="-0.254" y2="2.032" width="1.016" layer="94" curve="-75.749967" cap="flat"/>
<wire x1="-2.032" y1="-0.254" x2="-0.254" y2="-2.032" width="1.016" layer="94" curve="75.749967" cap="flat"/>
<wire x1="0.254" y1="-2.032" x2="2.032" y2="-0.254" width="1.016" layer="94" curve="75.749967" cap="flat"/>
<circle x="0" y="0" radius="1.524" width="0.0508" layer="94"/>
<text x="2.794" y="0.5842" size="1.778" layer="95">&gt;NAME</text>
<text x="2.794" y="-2.4638" size="1.778" layer="96">&gt;VALUE</text>
<pin name="MOUNT" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOUNT-PAD-ROUND" prefix="H">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt;, round</description>
<gates>
<gate name="G$1" symbol="MOUNT-PAD" x="0" y="0"/>
</gates>
<devices>
<device name="2.8" package="2,8-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B2,8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.0" package="3,0-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,0"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.2" package="3,2-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.3" package="3,3-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.6" package="3,6-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.1" package="4,1-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.3" package="4,3-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.5" package="4,5-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.0" package="5,0-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.5" package="5,5-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B5,5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.5" package="2,5-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B2,8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="raspberrypi_cm">
<packages>
<package name="HDMI">
<pad name="P$1" x="7.25" y="3" drill="1.3" shape="long" rot="R90"/>
<pad name="P$2" x="-7.25" y="3" drill="1.3" shape="long" rot="R90"/>
<pad name="P$3" x="-7.85" y="-1.9" drill="1.3" shape="long" rot="R90"/>
<pad name="P$4" x="7.85" y="-1.9" drill="1.3" shape="long" rot="R90"/>
<smd name="1" x="4.75" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="2" x="4.25" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="3" x="3.75" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="4" x="3.25" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="5" x="2.75" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="6" x="2.25" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="7" x="1.75" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="8" x="1.25" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="9" x="0.75" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="10" x="0.25" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="11" x="-0.25" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="12" x="-0.75" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="13" x="-1.25" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="14" x="-1.75" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="15" x="-2.25" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="16" x="-2.75" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="17" x="-3.25" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="18" x="-3.75" y="4" dx="0.3" dy="2.6" layer="1"/>
<smd name="19" x="-4.25" y="4" dx="0.3" dy="2.6" layer="1"/>
<wire x1="-7.5" y1="-5.5" x2="7.5" y2="-5.5" width="0.127" layer="21"/>
<wire x1="7.5" y1="-5.5" x2="7.5" y2="-4" width="0.127" layer="21"/>
<wire x1="7.5" y1="5.5" x2="7.5" y2="6" width="0.127" layer="21"/>
<wire x1="7.5" y1="6" x2="-7.5" y2="6" width="0.127" layer="21"/>
<wire x1="-7.5" y1="6" x2="-7.5" y2="5.5" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-5.5" x2="-7.5" y2="-4" width="0.127" layer="21"/>
<text x="-2.96" y="-2.96" size="1.27" layer="25">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="HDMI">
<pin name="TX2_P" x="-17.78" y="15.24" length="middle"/>
<pin name="TX2_N" x="-17.78" y="12.7" length="middle"/>
<pin name="TX1_P" x="-17.78" y="10.16" length="middle"/>
<pin name="TX1_N" x="-17.78" y="7.62" length="middle"/>
<pin name="TX0_P" x="-17.78" y="5.08" length="middle"/>
<pin name="TX0_N" x="-17.78" y="2.54" length="middle"/>
<pin name="CLK_P" x="-17.78" y="0" length="middle"/>
<pin name="CLK_N" x="-17.78" y="-2.54" length="middle"/>
<pin name="CEC_DAT" x="-17.78" y="-5.08" length="middle"/>
<pin name="SCL" x="-17.78" y="-7.62" length="middle"/>
<pin name="SDA" x="-17.78" y="-10.16" length="middle"/>
<pin name="HPD" x="-17.78" y="-12.7" length="middle"/>
<pin name="GND@1" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="GND@2" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="GND@3" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="GND@4" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="GND@5" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="GND@6" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="GND@7" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="GND@8" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="GND@9" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="+5V" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="NC" x="17.78" y="-10.16" length="middle" rot="R180"/>
<wire x1="-12.7" y1="17.78" x2="12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="17.78" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="-12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="-12.7" y2="17.78" width="0.254" layer="94"/>
<text x="-12.7" y="20.32" size="1.778" layer="95">&gt;Name</text>
<text x="-12.7" y="-17.78" size="1.778" layer="96">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="HDMI" prefix="X">
<gates>
<gate name="G$1" symbol="HDMI" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDMI">
<connects>
<connect gate="G$1" pin="+5V" pad="18"/>
<connect gate="G$1" pin="CEC_DAT" pad="13"/>
<connect gate="G$1" pin="CLK_N" pad="12"/>
<connect gate="G$1" pin="CLK_P" pad="10"/>
<connect gate="G$1" pin="GND@1" pad="2"/>
<connect gate="G$1" pin="GND@2" pad="5"/>
<connect gate="G$1" pin="GND@3" pad="8"/>
<connect gate="G$1" pin="GND@4" pad="11"/>
<connect gate="G$1" pin="GND@5" pad="17"/>
<connect gate="G$1" pin="GND@6" pad="P$1"/>
<connect gate="G$1" pin="GND@7" pad="P$2"/>
<connect gate="G$1" pin="GND@8" pad="P$3"/>
<connect gate="G$1" pin="GND@9" pad="P$4"/>
<connect gate="G$1" pin="HPD" pad="19"/>
<connect gate="G$1" pin="NC" pad="14"/>
<connect gate="G$1" pin="SCL" pad="15"/>
<connect gate="G$1" pin="SDA" pad="16"/>
<connect gate="G$1" pin="TX0_N" pad="9"/>
<connect gate="G$1" pin="TX0_P" pad="7"/>
<connect gate="G$1" pin="TX1_N" pad="6"/>
<connect gate="G$1" pin="TX1_P" pad="4"/>
<connect gate="G$1" pin="TX2_N" pad="3"/>
<connect gate="G$1" pin="TX2_P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LIBstereoaudiojack">
<packages>
<package name="AUDIO-JACK-CUI-353X">
<wire x1="-5.9" y1="-8.2" x2="2.3" y2="-8.2" width="0.127" layer="21"/>
<wire x1="2.3" y1="-8.2" x2="2.3" y2="5.8" width="0.127" layer="21"/>
<wire x1="2.3" y1="5.8" x2="1.8" y2="5.8" width="0.127" layer="21"/>
<wire x1="1.8" y1="5.8" x2="-3.9" y2="5.8" width="0.127" layer="21"/>
<wire x1="-3.9" y1="5.8" x2="-5.9" y2="5.8" width="0.127" layer="21"/>
<wire x1="-5.9" y1="5.8" x2="-5.9" y2="-8.2" width="0.127" layer="21"/>
<wire x1="-3.9" y1="5.8" x2="-3.9" y2="9.5" width="0.127" layer="21"/>
<wire x1="-3.9" y1="9.5" x2="1.8" y2="9.5" width="0.127" layer="21"/>
<wire x1="1.8" y1="9.5" x2="1.8" y2="5.8" width="0.127" layer="21"/>
<pad name="P$1" x="-1.9" y="4.6" drill="1.6" diameter="2.3" shape="octagon"/>
<pad name="P$2" x="0.1" y="2.2" drill="1.6" diameter="2.3" shape="octagon"/>
<pad name="P$3" x="0.1" y="-3.3" drill="1.6" diameter="2.3" shape="octagon"/>
<pad name="P$4" x="-1.9" y="-7" drill="1.6" diameter="2.3" shape="octagon"/>
<pad name="P$5" x="-1.9" y="-1.5" drill="1.6" diameter="2.4" shape="octagon"/>
</package>
<package name="AUDIO-JACK-KIT">
<description>Kit footprint for 1/8" audio jack.

Reduced openings in tStop, pins spread a tiny bit out to hold part on PCB during assembly</description>
<circle x="0" y="0" radius="0.6604" width="0" layer="29"/>
<circle x="5" y="5.1" radius="0.6604" width="0" layer="29"/>
<circle x="5" y="2.5" radius="0.6604" width="0" layer="29"/>
<circle x="5" y="-2.5" radius="0.6604" width="0" layer="29"/>
<circle x="5" y="-5.1" radius="0.6604" width="0" layer="29"/>
<circle x="0" y="0" radius="1.2192" width="0" layer="30"/>
<circle x="5" y="5.1" radius="1.2192" width="0" layer="30"/>
<circle x="5" y="-5.1" radius="1.2192" width="0" layer="30"/>
<circle x="5" y="-2.5" radius="1.2192" width="0" layer="30"/>
<circle x="5" y="2.5" radius="1.2192" width="0" layer="30"/>
<wire x1="7.5" y1="6" x2="7.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="6" x2="-3.5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-4.5" x2="-3.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="4.5" x2="-4.5" y2="3" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-3" x2="-4.5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-4.5" x2="-3.5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="4.5" x2="-3.5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="3" x2="-6.5" y2="-3" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="-3" x2="-4.5" y2="-3" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="3" x2="-4.5" y2="3" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="6" x2="3.5" y2="6" width="0.2032" layer="21"/>
<wire x1="7.5" y1="6" x2="6.5" y2="6" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="6" x2="-3.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-6" x2="3.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="7.5" y1="-6" x2="6.5" y2="-6" width="0.2032" layer="21"/>
<pad name="RING" x="5" y="5.1" drill="1.3" diameter="2.1844" stop="no"/>
<pad name="RSH" x="5" y="2.5" drill="1.3" diameter="2.1844" stop="no"/>
<pad name="SLEEVE" x="0" y="0" drill="1.3" diameter="2.1844" stop="no"/>
<pad name="TIP" x="5" y="-5.1" drill="1.3" diameter="2.1844" stop="no"/>
<pad name="TSH" x="5" y="-2.5" drill="1.3" diameter="2.1844" stop="no"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
<hole x="0" y="5" drill="1.2"/>
<hole x="-2.5" y="5" drill="1.2"/>
<hole x="-2.5" y="-5" drill="1.2"/>
<hole x="0" y="-5" drill="1.2"/>
<hole x="5" y="0" drill="1.2"/>
</package>
<package name="AUDIO-JACK">
<wire x1="7.5" y1="6" x2="7.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="6" x2="-3.5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-4.5" x2="-3.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="4.5" x2="-4.5" y2="3" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-3" x2="-4.5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-4.5" x2="-3.5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="4.5" x2="-3.5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="3" x2="-6.5" y2="-3" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="-3" x2="-4.5" y2="-3" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="3" x2="-4.5" y2="3" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="6" x2="3.5" y2="6" width="0.2032" layer="21"/>
<wire x1="7.5" y1="6" x2="6.5" y2="6" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="6" x2="-3.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-6" x2="3.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="7.5" y1="-6" x2="6.5" y2="-6" width="0.2032" layer="21"/>
<pad name="RING" x="5" y="5" drill="1.3" diameter="2.1844"/>
<pad name="RSH" x="5" y="2.5" drill="1.3" diameter="2.1844"/>
<pad name="SLEEVE" x="0" y="0" drill="1.3" diameter="2.1844"/>
<pad name="TIP" x="5" y="-5" drill="1.3" diameter="2.1844"/>
<pad name="TSH" x="5" y="-2.5" drill="1.3" diameter="2.1844"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
<hole x="0" y="5" drill="1.2"/>
<hole x="-2.5" y="5" drill="1.2"/>
<hole x="-2.5" y="-5" drill="1.2"/>
<hole x="0" y="-5" drill="1.2"/>
<hole x="5" y="0" drill="1.2"/>
</package>
<package name="AUDIO-JACK-3.5MM-SMD">
<wire x1="3.9" y1="-3" x2="10.2" y2="-3" width="0.254" layer="21"/>
<wire x1="14.5" y1="-0.635" x2="14.5" y2="-3" width="0.254" layer="21"/>
<wire x1="14.5" y1="-3" x2="13.2" y2="-3" width="0.254" layer="21"/>
<wire x1="14.5" y1="2.2098" x2="14.5" y2="3" width="0.254" layer="21"/>
<wire x1="14.5" y1="3" x2="5.7" y2="3" width="0.254" layer="21"/>
<wire x1="0.5" y1="3" x2="0" y2="3" width="0.254" layer="51"/>
<wire x1="0" y1="3" x2="0" y2="-2.5" width="0.254" layer="51"/>
<wire x1="0" y1="-2.5" x2="0" y2="-3" width="0.254" layer="51"/>
<wire x1="0" y1="-3" x2="1" y2="-3" width="0.254" layer="51"/>
<wire x1="-0.1524" y1="2.5" x2="-2.5" y2="2.5" width="0.254" layer="51"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="-2.5" width="0.254" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="0" y2="-2.5" width="0.254" layer="51"/>
<smd name="RING" x="4.3" y="3.45" dx="2" dy="2.5" layer="1"/>
<smd name="RSH" x="1.5" y="3.45" dx="1.2" dy="2.5" layer="1" rot="R180"/>
<smd name="SLEEVE" x="2.4" y="-3.45" dx="2" dy="2.5" layer="1"/>
<smd name="TIP" x="11.7" y="-3.45" dx="2" dy="2.5" layer="1"/>
<smd name="TSH" x="15.75" y="0.75" dx="2" dy="2.5" layer="1" rot="R90"/>
<hole x="3.5" y="0" drill="2"/>
<hole x="10.5" y="0" drill="2"/>
</package>
</packages>
<symbols>
<symbol name="AUDIO-JACK2">
<rectangle x1="-6.35" y1="-5.08" x2="-5.08" y2="5.08" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.81" x2="1.27" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="-3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="-5.08" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<pin name="RING" x="5.08" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="RSH" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="SLEEVE" x="5.08" y="5.08" visible="off" length="short" rot="R180"/>
<pin name="TIP" x="5.08" y="-5.08" visible="off" length="short" rot="R180"/>
<pin name="TSH" x="5.08" y="-2.54" visible="off" length="short" rot="R180"/>
<text x="-5.08" y="5.588" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AUDIO-JACK2" prefix="J">
<description>Stereo audio jack with bypass switches.&lt;br&gt;
SMT version used on LilyPad MP3 board.&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="AUDIO-JACK2" x="0" y="0"/>
</gates>
<devices>
<device name="CUI" package="AUDIO-JACK-CUI-353X">
<connects>
<connect gate="G$1" pin="RING" pad="P$3"/>
<connect gate="G$1" pin="RSH" pad="P$5"/>
<connect gate="G$1" pin="SLEEVE" pad="P$1"/>
<connect gate="G$1" pin="TIP" pad="P$2"/>
<connect gate="G$1" pin="TSH" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AUDIO-JACK-KIT">
<connects>
<connect gate="G$1" pin="RING" pad="RING"/>
<connect gate="G$1" pin="RSH" pad="RSH"/>
<connect gate="G$1" pin="SLEEVE" pad="SLEEVE"/>
<connect gate="G$1" pin="TIP" pad="TIP"/>
<connect gate="G$1" pin="TSH" pad="TSH"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08774" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH" package="AUDIO-JACK">
<connects>
<connect gate="G$1" pin="RING" pad="RING"/>
<connect gate="G$1" pin="RSH" pad="RSH"/>
<connect gate="G$1" pin="SLEEVE" pad="SLEEVE"/>
<connect gate="G$1" pin="TIP" pad="TIP"/>
<connect gate="G$1" pin="TSH" pad="TSH"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08774" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="AUDIO-JACK-3.5MM-SMD">
<connects>
<connect gate="G$1" pin="RING" pad="RING"/>
<connect gate="G$1" pin="RSH" pad="RSH"/>
<connect gate="G$1" pin="SLEEVE" pad="SLEEVE"/>
<connect gate="G$1" pin="TIP" pad="TIP"/>
<connect gate="G$1" pin="TSH" pad="TSH"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10353"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="R1" library="rcl" deviceset="R-EU_" device="M0805" value="10k"/>
<part name="R2" library="rcl" deviceset="R-EU_" device="M0805" value="10k"/>
<part name="R3" library="rcl" deviceset="R-EU_" device="M0805" value="10k"/>
<part name="JTAG" library="con-headers-jp" deviceset="M-2X05-" device="BORDER" value="M-2X05-BORDER"/>
<part name="R4" library="rcl" deviceset="R-EU_" device="M0805" value="1k"/>
<part name="R6" library="rcl" deviceset="R-EU_" device="M0805" value="1k-10k"/>
<part name="R5" library="rcl" deviceset="R-EU_" device="M0805" value="1k-10k"/>
<part name="R8" library="rcl" deviceset="R-EU_" device="M0805" value="2K 1%"/>
<part name="R9" library="rcl" deviceset="R-EU_" device="M0805" value="4.7k"/>
<part name="R10" library="rcl" deviceset="R-EU_" device="M0805" value="4.7k"/>
<part name="XTL" library="crystal" deviceset="CRYSTAL_RES-" device="SG636" value="5MHz"/>
<part name="C1" library="rcl" deviceset="C-EU" device="C0805" value="10uF"/>
<part name="C2" library="rcl" deviceset="C-EU" device="C0805" value="0.047uF"/>
<part name="C3" library="rcl" deviceset="C-EU" device="C0805" value="0.047uF"/>
<part name="C5" library="rcl" deviceset="C-EU" device="C0805" value="2.2uF"/>
<part name="C6" library="rcl" deviceset="C-EU" device="C0805" value="0.022uF"/>
<part name="C7" library="rcl" deviceset="C-EU" device="C0805" value="0.0047uF"/>
<part name="C9" library="rcl" deviceset="C-EU" device="C0805" value="10uF"/>
<part name="C10" library="rcl" deviceset="C-EU" device="C0805" value="0.022uF"/>
<part name="C11" library="rcl" deviceset="C-EU" device="C0805" value="0.022uF"/>
<part name="C12" library="rcl" deviceset="C-EU" device="C0805" value="0.01uF"/>
<part name="VGA" library="SparkFun-Connectors" deviceset="DB15" device=""/>
<part name="VIDEODACSA" library="Analog Devices_By_element14_Batch_1" deviceset="ADV7125BCPZ170" device="TQFP-48" value="CDK3405"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="R11" library="rcl" deviceset="R-EU_" device="M0805" value="75"/>
<part name="R12" library="rcl" deviceset="R-EU_" device="M0805" value="75"/>
<part name="R13" library="rcl" deviceset="R-EU_" device="M0805" value="75"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="C13" library="rcl" deviceset="C-EU" device="C0805" value="0.1uF"/>
<part name="R14" library="rcl" deviceset="R-EU_" device="M0805" value="348"/>
<part name="C14" library="rcl" deviceset="C-EU" device="C0805" value="0.1uF"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="C15" library="rcl" deviceset="C-EU" device="C0805" value="10uF"/>
<part name="C16" library="rcl" deviceset="C-EU" device="C0805" value="0.1uF"/>
<part name="C17" library="rcl" deviceset="C-EU" device="C0805" value="0.1uF"/>
<part name="C18" library="rcl" deviceset="C-EU" device="C0805" value="0.01uF"/>
<part name="USB.A" library="con-usb-3" deviceset="USB" device="-A-H"/>
<part name="USB.B" library="con-usb-3" deviceset="USB" device="-A-H"/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="R15" library="rcl" deviceset="R-EU_" device="R0805" value="22"/>
<part name="R16" library="rcl" deviceset="R-EU_" device="R0805" value="22"/>
<part name="R17" library="rcl" deviceset="R-EU_" device="R0805" value="22"/>
<part name="R18" library="rcl" deviceset="R-EU_" device="R0805" value="22"/>
<part name="R19" library="rcl" deviceset="R-EU_" device="R0805" value="15k"/>
<part name="R20" library="rcl" deviceset="R-EU_" device="R0805" value="15k"/>
<part name="R21" library="rcl" deviceset="R-EU_" device="R0805" value="15k"/>
<part name="R22" library="rcl" deviceset="R-EU_" device="R0805" value="15k"/>
<part name="U$2" library="adafruit" deviceset="SDMMC" device="HALFHEIGHT" value="SDMMCHALFHEIGHT"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="R23" library="rcl" deviceset="R-EU_" device="M0805" value="47K"/>
<part name="R24" library="rcl" deviceset="R-EU_" device="M0805" value="47K"/>
<part name="R25" library="rcl" deviceset="R-EU_" device="M0805" value="47K"/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="PS/2" library="con-yamaichi" deviceset="MD06SS" device=""/>
<part name="ZD1" library="adafruit" deviceset="DIODE" device="SOT23_REFLOW"/>
<part name="ZD2" library="adafruit" deviceset="DIODE" device="SOT23_REFLOW"/>
<part name="R26" library="rcl" deviceset="R-EU_" device="R0805" value="1k"/>
<part name="R27" library="rcl" deviceset="R-EU_" device="R0805" value="1k"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="C19" library="rcl" deviceset="C-EU" device="C0805" value="10nF"/>
<part name="C20" library="rcl" deviceset="C-EU" device="C0805" value="10nF"/>
<part name="R28" library="rcl" deviceset="R-EU_" device="M0805" value="560"/>
<part name="R29" library="rcl" deviceset="R-EU_" device="M0805" value="560"/>
<part name="GPIO_A" library="de1_parts" deviceset="DE1_GPIO" device=""/>
<part name="GPIO_B" library="de1_parts" deviceset="DE1_GPIO" device=""/>
<part name="GPIO_C" library="de1_parts" deviceset="DE1_GPIO" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="RN15" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="SDRAM" library="sdram" deviceset="K4S561632E-TC" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="3.3V" library="Exar_By_element14_Batch_1" deviceset="SPX3819M5-L-5-0" device="" value="3.3V"/>
<part name="2.5V" library="Exar_By_element14_Batch_1" deviceset="SPX3819M5-L-5-0" device="" value="2.5V"/>
<part name="1.1V" library="Exar_By_element14_Batch_1" deviceset="SPX3819M5-L-5-0" device="" value="1.1V"/>
<part name="L1" library="rcl" deviceset="L-EU" device="L3225M" value="1uH"/>
<part name="L2" library="rcl" deviceset="L-EU" device="L3225M" value="1uH"/>
<part name="L3" library="rcl" deviceset="L-EU" device="L3225M" value="1uH"/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="C21" library="rcl" deviceset="C-EU" device="C0805" value="10uF"/>
<part name="C22" library="rcl" deviceset="C-EU" device="C0805" value="10uF"/>
<part name="C23" library="rcl" deviceset="C-EU" device="C0805" value="10uF"/>
<part name="C24" library="rcl" deviceset="C-EU" device="C0805" value="22uF"/>
<part name="C25" library="rcl" deviceset="C-EU" device="C0805" value="22uF"/>
<part name="C26" library="rcl" deviceset="C-EU" device="C0805" value="22uF"/>
<part name="C27" library="rcl" deviceset="C-EU" device="C0805" value="0.1uF"/>
<part name="5V_IN_B" library="con-usb-3" deviceset="USB" device="-B-H"/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="U$1" library="cyclonev_f484" deviceset="CYCLONEVFPGA484" device="SMALLPINS"/>
<part name="C28" library="rcl" deviceset="C-EU" device="C0805" value="0.01uF"/>
<part name="C4" library="rcl" deviceset="C-EU" device="C0805" value="SP"/>
<part name="C8" library="rcl" deviceset="C-EU" device="C0805" value="SP"/>
<part name="C29" library="rcl" deviceset="C-EU" device="C0805" value="SP"/>
<part name="C30" library="rcl" deviceset="C-EU" device="C0805" value="0.1uF"/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="POWER_SW" library="Eagle_Wuerth_Elektronik_Switch_rev15b" deviceset="450302014072" device="ROCKER2" value="450302014072ROCKER2"/>
<part name="R7" library="rcl" deviceset="R-EU_" device="R0805" value="4k7"/>
<part name="C31" library="rcl" deviceset="C-EU" device="C0805" value="10uF"/>
<part name="C32" library="rcl" deviceset="C-EU" device="C0805" value="10uF"/>
<part name="R30" library="rcl" deviceset="R-EU_" device="R0805" value="1k"/>
<part name="R31" library="rcl" deviceset="R-EU_" device="R0805" value="100"/>
<part name="R32" library="rcl" deviceset="R-EU_" device="R0805" value="100"/>
<part name="PF1" library="rcl" deviceset="R-EU_" device="R1206" value="1A"/>
<part name="ADC" library="adc" deviceset="MCP3021" device=""/>
<part name="R33" library="rcl" deviceset="R-EU_" device="M0805" value="560"/>
<part name="R34" library="rcl" deviceset="R-EU_" device="M0805" value="560"/>
<part name="C33" library="rcl" deviceset="C-EU" device="C0805" value="10nF"/>
<part name="C34" library="rcl" deviceset="C-EU" device="C0805" value="10nF"/>
<part name="R35" library="rcl" deviceset="R-EU_" device="M0805" value="1k"/>
<part name="C35" library="rcl" deviceset="C-EU" device="C1206" value="100uF"/>
<part name="C36" library="rcl" deviceset="C-EU" device="C1206" value="100uF"/>
<part name="S-VIDEO" library="con-yamaichi" deviceset="MD04SS" device=""/>
<part name="5V_IN" library="j_usb_hirose_zx62d-b-5p8" deviceset="ZX62D-B-5P8" device=""/>
<part name="C-VIDEO" library="con-hirschmann" deviceset="TOBU3" device="MINE" value="TOBU3MINE"/>
<part name="RESET" library="Seeed-OPL-Switch" deviceset="SMD-BUTTON(4P-4.0X4.0X1.7MM)" device="-DHT-1152"/>
<part name="AUDIO" library="LIBstereoaudiojack" deviceset="AUDIO-JACK2" device="CUI"/>
<part name="C37" library="rcl" deviceset="C-EU" device="C0805" value="15pF"/>
<part name="R36" library="rcl" deviceset="R-EU_" device="R0805" value="240"/>
<part name="R37" library="rcl" deviceset="R-EU_" device="R0805" value="200"/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="JP1" library="SparkFun-Connectors" deviceset="M02" device="PTH"/>
<part name="RN9" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN10" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN11" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN12" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN16" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN17" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN18" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN19" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="U$3" library="header127" deviceset="HDR_SMALL_36_DEV_L" device="ALIGNED" value="HDR_SMALL_36_DEV_LALIGNED"/>
<part name="RN5" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN6" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN7" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN8" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN14" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN20" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN21" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN22" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN23" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="U$4" library="header127" deviceset="HDR_SMALL_36_DEV_L" device="ALIGNED" value="HDR_SMALL_36_DEV_LALIGNED"/>
<part name="RN1" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN2" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN3" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN4" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN13" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN24" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN25" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN26" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="RN27" library="resistor-array" deviceset="742-8" device="742_3" value="47"/>
<part name="U$5" library="header127" deviceset="HDR_SMALL_36_DEV_L" device="ALIGNED" value="HDR_SMALL_36_DEV_LALIGNED"/>
<part name="C38" library="rcl" deviceset="C-EU" device="C0805" value="0.1uF"/>
<part name="SPI" library="epcs" deviceset="EPCS" device="BOTH" value="EPCS"/>
<part name="3.3/2.5/1.1/GND" library="pinhead" deviceset="PINHD-1X4" device=""/>
<part name="R38" library="rcl" deviceset="R-EU_" device="M0805" value="2k"/>
<part name="R39" library="rcl" deviceset="R-EU_" device="M0805" value="2k"/>
<part name="C39" library="rcl" deviceset="C-EU" device="C0805" value="0.1uF"/>
<part name="H1" library="holes" deviceset="MOUNT-PAD-ROUND" device="2.5"/>
<part name="H2" library="holes" deviceset="MOUNT-PAD-ROUND" device="2.5"/>
<part name="H3" library="holes" deviceset="MOUNT-PAD-ROUND" device="2.5"/>
<part name="H4" library="holes" deviceset="MOUNT-PAD-ROUND" device="2.5"/>
<part name="H5" library="holes" deviceset="MOUNT-PAD-ROUND" device="2.5"/>
<part name="H6" library="holes" deviceset="MOUNT-PAD-ROUND" device="2.5"/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="H7" library="holes" deviceset="MOUNT-PAD-ROUND" device="2.5"/>
<part name="HDMI" library="raspberrypi_cm" deviceset="HDMI" device=""/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="RN28" library="resistor-array" deviceset="742-8" device="742_3" value="270"/>
<part name="RN29" library="resistor-array" deviceset="742-8" device="742_3" value="270"/>
</parts>
<sheets>
<sheet>
<description>FPGACore</description>
<plain>
<text x="-2.54" y="104.14" size="1.778" layer="95">10011 AS 1x/4x</text>
<text x="-2.54" y="124.46" size="1.778" layer="95">JTAG also used to program AS via special core</text>
<text x="-2.54" y="66.04" size="1.778" layer="91">Ooops, INIT_DONE shared
with GPIOB26. Will disable!</text>
<text x="-111.76" y="66.04" size="1.778" layer="91">450-2133-1-ND</text>
</plain>
<instances>
<instance part="R1" gate="G$1" x="-71.12" y="81.28" rot="R90"/>
<instance part="R2" gate="G$1" x="-78.74" y="81.28" rot="R90"/>
<instance part="R3" gate="G$1" x="-86.36" y="81.28" rot="R90"/>
<instance part="JTAG" gate="G$1" x="-78.74" y="127"/>
<instance part="R4" gate="G$1" x="-58.42" y="132.08" rot="R270"/>
<instance part="R6" gate="G$1" x="-33.02" y="147.32" rot="R90"/>
<instance part="R5" gate="G$1" x="-40.64" y="147.32" rot="R90"/>
<instance part="R8" gate="G$1" x="-35.56" y="71.12" rot="R180"/>
<instance part="R9" gate="G$1" x="-50.8" y="22.86" rot="R90"/>
<instance part="R10" gate="G$1" x="-43.18" y="22.86" rot="R90"/>
<instance part="XTL" gate="A" x="-119.38" y="99.06"/>
<instance part="U$1" gate="G$1" x="48.26" y="25.4"/>
<instance part="C30" gate="G$1" x="-63.5" y="-2.54"/>
<instance part="GND17" gate="1" x="-63.5" y="-15.24"/>
<instance part="RESET" gate="G$1" x="-105.41" y="73.66"/>
<instance part="C38" gate="G$1" x="-149.86" y="101.6"/>
<instance part="SPI" gate="A" x="-81.28" y="33.02" rot="R180"/>
</instances>
<busses>
<bus name="SDRAM:A[0..12],DQ[0..15],CS_N,RAS_N,CAS_N,WE_N,SDRAM_CLK,CKE,SDRAM_UDQM,SDRAM_LDQM,BA[0..1]">
<segment>
<wire x1="165.1" y1="-60.96" x2="68.58" y2="-60.96" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="USB1DP,USB1DM,USB2DP,USB2DM">
<segment>
<wire x1="187.96" y1="-38.1" x2="187.96" y2="-60.96" width="0.762" layer="92"/>
<wire x1="187.96" y1="-60.96" x2="167.64" y2="-60.96" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="VGADAC:VGA_R[0..7],VGA_G[0..7],VGA_B[0..7],VGA_CLK,VGA_BLANK_N,HSYNC,VSYNC">
<segment>
<wire x1="-20.32" y1="160.02" x2="50.8" y2="160.02" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="SD_DETECT,SD_WRITEPROTECT,SD_CS,SD_MISO,SD_MOSI,SD_SCK,SD_DAT1,SD_DAT2">
<segment>
<wire x1="200.66" y1="-30.48" x2="200.66" y2="-12.7" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="GPIOC[0..35]">
<segment>
<wire x1="205.74" y1="-10.16" x2="205.74" y2="33.02" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="101.6" y1="167.64" x2="58.42" y2="167.64" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="GPIOA[0..35]">
<segment>
<wire x1="104.14" y1="165.1" x2="193.04" y2="165.1" width="0.762" layer="92"/>
<wire x1="193.04" y1="165.1" x2="193.04" y2="35.56" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="GPIOB[0..35]">
<segment>
<wire x1="205.74" y1="35.56" x2="205.74" y2="175.26" width="0.762" layer="92"/>
<wire x1="205.74" y1="175.26" x2="104.14" y2="175.26" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="ADC:SCL,SDA">
<segment>
<wire x1="60.96" y1="-60.96" x2="66.04" y2="-60.96" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="3V3" class="0">
<segment>
<wire x1="-30.48" y1="99.06" x2="-25.4" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="106.68" x2="-30.48" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="106.68" x2="-30.48" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="109.22" x2="-30.48" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="109.22" x2="-30.48" y2="106.68" width="0.1524" layer="91"/>
<junction x="-30.48" y="106.68"/>
<wire x1="-30.48" y1="106.68" x2="-38.1" y2="106.68" width="0.1524" layer="91"/>
<label x="-38.1" y="106.68" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="MSEL_0"/>
<pinref part="U$1" gate="G$1" pin="MSEL_1"/>
<pinref part="U$1" gate="G$1" pin="MSEL_4"/>
</segment>
<segment>
<wire x1="-25.4" y1="83.82" x2="-30.48" y2="83.82" width="0.1524" layer="91"/>
<label x="-38.1" y="83.82" size="1.778" layer="95"/>
<wire x1="-30.48" y1="83.82" x2="-38.1" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="86.36" x2="-30.48" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="86.36" x2="-30.48" y2="83.82" width="0.1524" layer="91"/>
<junction x="-30.48" y="83.82"/>
<wire x1="-25.4" y1="78.74" x2="-30.48" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="78.74" x2="-30.48" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="VCCIO"/>
<pinref part="U$1" gate="G$1" pin="VCCPGM"/>
<pinref part="U$1" gate="G$1" pin="VREF"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-86.36" y1="86.36" x2="-86.36" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="88.9" x2="-78.74" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-78.74" y1="88.9" x2="-71.12" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="88.9" x2="-71.12" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-78.74" y1="86.36" x2="-78.74" y2="88.9" width="0.1524" layer="91"/>
<junction x="-78.74" y="88.9"/>
<label x="-83.82" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JTAG" gate="G$1" pin="4"/>
<wire x1="-71.12" y1="129.54" x2="-66.04" y2="129.54" width="0.1524" layer="91"/>
<label x="-68.58" y="129.54" size="1.778" layer="95"/>
<wire x1="-66.04" y1="129.54" x2="-66.04" y2="154.94" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="154.94" x2="-40.64" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-40.64" y1="154.94" x2="-33.02" y2="154.94" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="154.94" x2="-33.02" y2="152.4" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-40.64" y1="152.4" x2="-40.64" y2="154.94" width="0.1524" layer="91"/>
<junction x="-40.64" y="154.94"/>
</segment>
<segment>
<wire x1="-58.42" y1="25.4" x2="-63.5" y2="25.4" width="0.1524" layer="91"/>
<label x="-60.96" y="25.4" size="1.778" layer="95"/>
<wire x1="-58.42" y1="25.4" x2="-58.42" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="15.24" x2="-50.8" y2="15.24" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="15.24" x2="-43.18" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="15.24" x2="-43.18" y2="17.78" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="17.78" x2="-50.8" y2="15.24" width="0.1524" layer="91"/>
<junction x="-50.8" y="15.24"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="-63.5" y1="0" x2="-63.5" y2="25.4" width="0.1524" layer="91"/>
<pinref part="SPI" gate="A" pin="VCC"/>
<junction x="-63.5" y="25.4"/>
</segment>
<segment>
<pinref part="XTL" gate="A" pin="CON"/>
<wire x1="-106.68" y1="104.14" x2="-104.14" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="104.14" x2="-104.14" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="111.76" x2="-137.16" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="111.76" x2="-137.16" y2="104.14" width="0.1524" layer="91"/>
<pinref part="XTL" gate="A" pin="VDD"/>
<wire x1="-137.16" y1="104.14" x2="-132.08" y2="104.14" width="0.1524" layer="91"/>
<label x="-137.16" y="111.76" size="1.778" layer="95"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="-149.86" y1="104.14" x2="-149.86" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-149.86" y1="111.76" x2="-137.16" y2="111.76" width="0.1524" layer="91"/>
<junction x="-137.16" y="111.76"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="-25.4" y1="101.6" x2="-25.4" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="101.6" x2="-38.1" y2="101.6" width="0.1524" layer="91"/>
<label x="-38.1" y="101.6" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="MSEL_2"/>
<pinref part="U$1" gate="G$1" pin="MSEL_3"/>
<junction x="-25.4" y="101.6"/>
</segment>
<segment>
<wire x1="-25.4" y1="76.2" x2="-40.64" y2="76.2" width="0.1524" layer="91"/>
<label x="-38.1" y="76.2" size="1.778" layer="95"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="-40.64" y1="71.12" x2="-40.64" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND"/>
</segment>
<segment>
<label x="-38.1" y="58.42" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="CE_N"/>
<wire x1="-25.4" y1="58.42" x2="-92.71" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-92.71" y1="58.42" x2="-115.57" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-115.57" y1="58.42" x2="-115.57" y2="71.12" width="0.1524" layer="91"/>
<pinref part="RESET" gate="G$1" pin="B0"/>
<wire x1="-115.57" y1="71.12" x2="-114.3" y2="71.12" width="0.1524" layer="91"/>
<pinref part="RESET" gate="G$1" pin="B1"/>
<wire x1="-96.52" y1="71.12" x2="-92.71" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-92.71" y1="71.12" x2="-92.71" y2="58.42" width="0.1524" layer="91"/>
<junction x="-92.71" y="58.42"/>
</segment>
<segment>
<label x="-68.58" y="132.08" size="1.778" layer="95"/>
<pinref part="JTAG" gate="G$1" pin="10"/>
<wire x1="-71.12" y1="121.92" x2="-60.96" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="121.92" x2="-60.96" y2="127" width="0.1524" layer="91"/>
<pinref part="JTAG" gate="G$1" pin="2"/>
<wire x1="-60.96" y1="127" x2="-60.96" y2="132.08" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="132.08" x2="-71.12" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-60.96" y1="127" x2="-58.42" y2="127" width="0.1524" layer="91"/>
<junction x="-60.96" y="127"/>
</segment>
<segment>
<wire x1="-58.42" y1="45.72" x2="-63.5" y2="45.72" width="0.1524" layer="91"/>
<label x="-63.5" y="45.72" size="1.778" layer="95"/>
<pinref part="SPI" gate="A" pin="GND"/>
</segment>
<segment>
<pinref part="XTL" gate="A" pin="VSS"/>
<label x="-137.16" y="93.98" size="1.778" layer="95"/>
<wire x1="-132.08" y1="93.98" x2="-149.86" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="-149.86" y1="93.98" x2="-149.86" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="-63.5" y1="-12.7" x2="-63.5" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="1V1" class="0">
<segment>
<wire x1="-25.4" y1="93.98" x2="-38.1" y2="93.98" width="0.1524" layer="91"/>
<label x="-38.1" y="93.98" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="VCC"/>
</segment>
</net>
<net name="2V5" class="0">
<segment>
<wire x1="-25.4" y1="91.44" x2="-33.02" y2="91.44" width="0.1524" layer="91"/>
<label x="-38.1" y="91.44" size="1.778" layer="95"/>
<wire x1="-33.02" y1="91.44" x2="-38.1" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="88.9" x2="-33.02" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="88.9" x2="-33.02" y2="91.44" width="0.1524" layer="91"/>
<junction x="-33.02" y="91.44"/>
<wire x1="-33.02" y1="88.9" x2="-33.02" y2="81.28" width="0.1524" layer="91"/>
<junction x="-33.02" y="88.9"/>
<wire x1="-33.02" y1="81.28" x2="-25.4" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="VCCA_FPLL"/>
<pinref part="U$1" gate="G$1" pin="VCC_AUX"/>
<pinref part="U$1" gate="G$1" pin="VCCBAT"/>
</segment>
</net>
<net name="CONF_DONE" class="0">
<segment>
<wire x1="-25.4" y1="63.5" x2="-78.74" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-78.74" y1="63.5" x2="-78.74" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="CONF_DONE"/>
</segment>
</net>
<net name="CONFIG_N" class="0">
<segment>
<wire x1="-25.4" y1="60.96" x2="-86.36" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="CONFIG_N"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-86.36" y1="76.2" x2="-86.36" y2="73.66" width="0.1524" layer="91"/>
<pinref part="RESET" gate="G$1" pin="A1"/>
<wire x1="-86.36" y1="73.66" x2="-86.36" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="76.2" x2="-93.98" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="76.2" x2="-91.44" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="76.2" x2="-91.44" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="73.66" x2="-86.36" y2="73.66" width="0.1524" layer="91"/>
<junction x="-86.36" y="73.66"/>
<pinref part="RESET" gate="G$1" pin="A0"/>
<wire x1="-114.3" y1="76.2" x2="-115.57" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-115.57" y1="76.2" x2="-115.57" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-115.57" y1="81.28" x2="-93.98" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="81.28" x2="-93.98" y2="76.2" width="0.1524" layer="91"/>
<junction x="-93.98" y="76.2"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<pinref part="JTAG" gate="G$1" pin="1"/>
<wire x1="-86.36" y1="132.08" x2="-93.98" y2="132.08" width="0.1524" layer="91"/>
<label x="-93.98" y="132.08" size="1.778" layer="95"/>
<wire x1="-25.4" y1="129.54" x2="-43.18" y2="129.54" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="129.54" x2="-43.18" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="139.7" x2="-58.42" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="139.7" x2="-93.98" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="139.7" x2="-93.98" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-58.42" y1="139.7" x2="-58.42" y2="137.16" width="0.1524" layer="91"/>
<junction x="-58.42" y="139.7"/>
<label x="-30.48" y="129.54" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="JTAG_TCK"/>
</segment>
</net>
<net name="TDO" class="0">
<segment>
<wire x1="-25.4" y1="121.92" x2="-50.8" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="121.92" x2="-50.8" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="111.76" x2="-99.06" y2="111.76" width="0.1524" layer="91"/>
<pinref part="JTAG" gate="G$1" pin="3"/>
<wire x1="-99.06" y1="129.54" x2="-86.36" y2="129.54" width="0.1524" layer="91"/>
<label x="-93.98" y="129.54" size="1.778" layer="95"/>
<wire x1="-99.06" y1="111.76" x2="-99.06" y2="129.54" width="0.1524" layer="91"/>
<label x="-30.48" y="121.92" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="JTAG_TDO"/>
</segment>
</net>
<net name="TMS" class="0">
<segment>
<wire x1="-25.4" y1="127" x2="-33.02" y2="127" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="127" x2="-55.88" y2="127" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="127" x2="-55.88" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="116.84" x2="-93.98" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-33.02" y1="127" x2="-33.02" y2="142.24" width="0.1524" layer="91"/>
<junction x="-33.02" y="127"/>
<pinref part="JTAG" gate="G$1" pin="5"/>
<wire x1="-93.98" y1="127" x2="-86.36" y2="127" width="0.1524" layer="91"/>
<label x="-93.98" y="127" size="1.778" layer="95"/>
<wire x1="-93.98" y1="127" x2="-93.98" y2="116.84" width="0.1524" layer="91"/>
<label x="-30.48" y="127" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="JTAG_TMS"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-40.64" y1="142.24" x2="-40.64" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="124.46" x2="-25.4" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="124.46" x2="-53.34" y2="124.46" width="0.1524" layer="91"/>
<junction x="-40.64" y="124.46"/>
<wire x1="-53.34" y1="124.46" x2="-53.34" y2="114.3" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="114.3" x2="-96.52" y2="114.3" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="114.3" x2="-96.52" y2="121.92" width="0.1524" layer="91"/>
<pinref part="JTAG" gate="G$1" pin="9"/>
<wire x1="-96.52" y1="121.92" x2="-86.36" y2="121.92" width="0.1524" layer="91"/>
<label x="-93.98" y="121.92" size="1.778" layer="95"/>
<label x="-30.48" y="124.46" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="JTAG_TDI"/>
</segment>
</net>
<net name="STATUS_N" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-71.12" y1="76.2" x2="-71.12" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="66.04" x2="-25.4" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="STATUS_N"/>
</segment>
</net>
<net name="RREF_TL" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="71.12" x2="-30.48" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="RREF_TL"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="-25.4" y1="53.34" x2="-40.64" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="53.34" x2="-40.64" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="30.48" x2="-63.5" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="AS_DCLK"/>
<pinref part="SPI" gate="A" pin="SCK"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="-25.4" y1="50.8" x2="-38.1" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="50.8" x2="-38.1" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="33.02" x2="-63.5" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="AS_CSO_N"/>
<pinref part="SPI" gate="A" pin="~CS"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="-35.56" y1="43.18" x2="-35.56" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="12.7" x2="-99.06" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="12.7" x2="-99.06" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="43.18" x2="-35.56" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="AS_DATA1"/>
<pinref part="SPI" gate="A" pin="SO"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="-63.5" y1="38.1" x2="-33.02" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="38.1" x2="-33.02" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="40.64" x2="-25.4" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="AS_DATA0"/>
<pinref part="SPI" gate="A" pin="SI"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="-50.8" y1="27.94" x2="-50.8" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="35.56" x2="-63.5" y2="35.56" width="0.1524" layer="91"/>
<pinref part="SPI" gate="A" pin="~WP"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="-63.5" y1="40.64" x2="-43.18" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="-43.18" y1="40.64" x2="-43.18" y2="27.94" width="0.1524" layer="91"/>
<pinref part="SPI" gate="A" pin="~HOLD"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="XTL" gate="A" pin="OUT"/>
<wire x1="-106.68" y1="99.06" x2="-43.18" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="99.06" x2="-43.18" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="116.84" x2="-25.4" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="CLK"/>
</segment>
</net>
<net name="VGA_B0" class="0">
<segment>
<wire x1="25.4" y1="160.02" x2="25.4" y2="154.94" width="0.1524" layer="91"/>
<label x="25.4" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$18"/>
</segment>
</net>
<net name="VGA_B1" class="0">
<segment>
<wire x1="27.94" y1="160.02" x2="27.94" y2="154.94" width="0.1524" layer="91"/>
<label x="27.94" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$19"/>
</segment>
</net>
<net name="VGA_B2" class="0">
<segment>
<wire x1="30.48" y1="160.02" x2="30.48" y2="154.94" width="0.1524" layer="91"/>
<label x="30.48" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$20"/>
</segment>
</net>
<net name="VGA_B3" class="0">
<segment>
<wire x1="33.02" y1="160.02" x2="33.02" y2="154.94" width="0.1524" layer="91"/>
<label x="33.02" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$21"/>
</segment>
</net>
<net name="VGA_B4" class="0">
<segment>
<wire x1="35.56" y1="160.02" x2="35.56" y2="154.94" width="0.1524" layer="91"/>
<label x="35.56" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$22"/>
</segment>
</net>
<net name="VGA_B5" class="0">
<segment>
<wire x1="38.1" y1="160.02" x2="38.1" y2="154.94" width="0.1524" layer="91"/>
<label x="38.1" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$23"/>
</segment>
</net>
<net name="VGA_B6" class="0">
<segment>
<wire x1="40.64" y1="160.02" x2="40.64" y2="154.94" width="0.1524" layer="91"/>
<label x="40.64" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$24"/>
</segment>
</net>
<net name="VGA_B7" class="0">
<segment>
<wire x1="43.18" y1="160.02" x2="43.18" y2="154.94" width="0.1524" layer="91"/>
<label x="43.18" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$25"/>
</segment>
</net>
<net name="VGA_CLK" class="0">
<segment>
<wire x1="45.72" y1="160.02" x2="45.72" y2="154.94" width="0.1524" layer="91"/>
<label x="45.72" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$26"/>
</segment>
</net>
<net name="USB1DP" class="0">
<segment>
<wire x1="167.64" y1="-60.96" x2="167.64" y2="-48.26" width="0.1524" layer="91"/>
<label x="170.18" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$152"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<wire x1="165.1" y1="-60.96" x2="165.1" y2="-48.26" width="0.1524" layer="91"/>
<label x="165.1" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$153"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<wire x1="162.56" y1="-60.96" x2="162.56" y2="-48.26" width="0.1524" layer="91"/>
<label x="162.56" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$154"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<wire x1="160.02" y1="-60.96" x2="160.02" y2="-48.26" width="0.1524" layer="91"/>
<label x="160.02" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$155"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<wire x1="157.48" y1="-60.96" x2="157.48" y2="-48.26" width="0.1524" layer="91"/>
<label x="157.48" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$156"/>
</segment>
</net>
<net name="A8" class="0">
<segment>
<wire x1="154.94" y1="-60.96" x2="154.94" y2="-48.26" width="0.1524" layer="91"/>
<label x="154.94" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$157"/>
</segment>
</net>
<net name="A9" class="0">
<segment>
<wire x1="152.4" y1="-60.96" x2="152.4" y2="-48.26" width="0.1524" layer="91"/>
<label x="152.4" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$158"/>
</segment>
</net>
<net name="A11" class="0">
<segment>
<wire x1="149.86" y1="-60.96" x2="149.86" y2="-48.26" width="0.1524" layer="91"/>
<label x="149.86" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$159"/>
</segment>
</net>
<net name="A12" class="0">
<segment>
<wire x1="147.32" y1="-60.96" x2="147.32" y2="-48.26" width="0.1524" layer="91"/>
<label x="147.32" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$160"/>
</segment>
</net>
<net name="CKE" class="0">
<segment>
<wire x1="144.78" y1="-60.96" x2="144.78" y2="-48.26" width="0.1524" layer="91"/>
<label x="144.78" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$161"/>
</segment>
</net>
<net name="SDRAM_CLK" class="0">
<segment>
<wire x1="142.24" y1="-60.96" x2="142.24" y2="-48.26" width="0.1524" layer="91"/>
<label x="142.24" y="-68.58" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$162"/>
</segment>
</net>
<net name="SDRAM_UDQM" class="0">
<segment>
<wire x1="139.7" y1="-60.96" x2="139.7" y2="-48.26" width="0.1524" layer="91"/>
<label x="139.7" y="-68.58" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$163"/>
</segment>
</net>
<net name="DQ8" class="0">
<segment>
<wire x1="137.16" y1="-60.96" x2="137.16" y2="-48.26" width="0.1524" layer="91"/>
<label x="137.16" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$164"/>
</segment>
</net>
<net name="DQ9" class="0">
<segment>
<wire x1="134.62" y1="-60.96" x2="134.62" y2="-48.26" width="0.1524" layer="91"/>
<label x="134.62" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$165"/>
</segment>
</net>
<net name="DQ10" class="0">
<segment>
<wire x1="132.08" y1="-60.96" x2="132.08" y2="-48.26" width="0.1524" layer="91"/>
<label x="132.08" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$166"/>
</segment>
</net>
<net name="DQ11" class="0">
<segment>
<wire x1="129.54" y1="-60.96" x2="129.54" y2="-48.26" width="0.1524" layer="91"/>
<label x="129.54" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$167"/>
</segment>
</net>
<net name="DQ12" class="0">
<segment>
<wire x1="127" y1="-60.96" x2="127" y2="-48.26" width="0.1524" layer="91"/>
<label x="127" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$168"/>
</segment>
</net>
<net name="DQ13" class="0">
<segment>
<wire x1="124.46" y1="-60.96" x2="124.46" y2="-48.26" width="0.1524" layer="91"/>
<label x="124.46" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$169"/>
</segment>
</net>
<net name="DQ14" class="0">
<segment>
<wire x1="121.92" y1="-60.96" x2="121.92" y2="-48.26" width="0.1524" layer="91"/>
<label x="121.92" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$170"/>
</segment>
</net>
<net name="DQ15" class="0">
<segment>
<wire x1="119.38" y1="-60.96" x2="119.38" y2="-48.26" width="0.1524" layer="91"/>
<label x="119.38" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$171"/>
</segment>
</net>
<net name="PS2CLK" class="0">
<segment>
<wire x1="187.96" y1="-33.02" x2="177.8" y2="-33.02" width="0.1524" layer="91"/>
<label x="182.88" y="-33.02" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="P$147"/>
</segment>
</net>
<net name="PS2DAT" class="0">
<segment>
<wire x1="187.96" y1="-35.56" x2="177.8" y2="-35.56" width="0.1524" layer="91"/>
<label x="182.88" y="-35.56" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="P$148"/>
</segment>
</net>
<net name="USB2DM" class="0">
<segment>
<wire x1="187.96" y1="-38.1" x2="177.8" y2="-38.1" width="0.1524" layer="91"/>
<label x="182.88" y="-38.1" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="P$149"/>
</segment>
</net>
<net name="USB2DP" class="0">
<segment>
<wire x1="187.96" y1="-40.64" x2="177.8" y2="-40.64" width="0.1524" layer="91"/>
<label x="182.88" y="-40.64" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="P$150"/>
</segment>
</net>
<net name="AUDIO_LEFT" class="0">
<segment>
<wire x1="53.34" y1="160.02" x2="53.34" y2="154.94" width="0.1524" layer="91"/>
<label x="53.34" y="154.94" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$29"/>
</segment>
</net>
<net name="AUDIO_RIGHT" class="0">
<segment>
<wire x1="55.88" y1="160.02" x2="55.88" y2="154.94" width="0.1524" layer="91"/>
<label x="55.88" y="154.94" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$30"/>
</segment>
</net>
<net name="VGA_R1" class="0">
<segment>
<wire x1="-15.24" y1="160.02" x2="-15.24" y2="154.94" width="0.1524" layer="91"/>
<label x="-15.24" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="VGA_R2" class="0">
<segment>
<wire x1="-12.7" y1="160.02" x2="-12.7" y2="154.94" width="0.1524" layer="91"/>
<label x="-12.7" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="VGA_R3" class="0">
<segment>
<wire x1="-10.16" y1="160.02" x2="-10.16" y2="154.94" width="0.1524" layer="91"/>
<label x="-10.16" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$4"/>
</segment>
</net>
<net name="VGA_R4" class="0">
<segment>
<wire x1="-7.62" y1="160.02" x2="-7.62" y2="154.94" width="0.1524" layer="91"/>
<label x="-7.62" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$5"/>
</segment>
</net>
<net name="VGA_R5" class="0">
<segment>
<wire x1="-5.08" y1="160.02" x2="-5.08" y2="154.94" width="0.1524" layer="91"/>
<label x="-5.08" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$6"/>
</segment>
</net>
<net name="VGA_R6" class="0">
<segment>
<wire x1="-2.54" y1="160.02" x2="-2.54" y2="154.94" width="0.1524" layer="91"/>
<label x="-2.54" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$7"/>
</segment>
</net>
<net name="VGA_R7" class="0">
<segment>
<wire x1="0" y1="160.02" x2="0" y2="154.94" width="0.1524" layer="91"/>
<label x="0" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$8"/>
</segment>
</net>
<net name="VGA_G0" class="0">
<segment>
<wire x1="2.54" y1="160.02" x2="2.54" y2="154.94" width="0.1524" layer="91"/>
<label x="2.54" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$9"/>
</segment>
</net>
<net name="VGA_G1" class="0">
<segment>
<wire x1="5.08" y1="160.02" x2="5.08" y2="154.94" width="0.1524" layer="91"/>
<label x="5.08" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$10"/>
</segment>
</net>
<net name="VGA_G2" class="0">
<segment>
<wire x1="7.62" y1="160.02" x2="7.62" y2="154.94" width="0.1524" layer="91"/>
<label x="7.62" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$11"/>
</segment>
</net>
<net name="VGA_G3" class="0">
<segment>
<wire x1="10.16" y1="160.02" x2="10.16" y2="154.94" width="0.1524" layer="91"/>
<label x="10.16" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$12"/>
</segment>
</net>
<net name="VGA_G4" class="0">
<segment>
<wire x1="12.7" y1="160.02" x2="12.7" y2="154.94" width="0.1524" layer="91"/>
<label x="12.7" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$13"/>
</segment>
</net>
<net name="VGA_G5" class="0">
<segment>
<wire x1="15.24" y1="160.02" x2="15.24" y2="154.94" width="0.1524" layer="91"/>
<label x="15.24" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$14"/>
</segment>
</net>
<net name="VGA_G6" class="0">
<segment>
<wire x1="17.78" y1="160.02" x2="17.78" y2="154.94" width="0.1524" layer="91"/>
<label x="17.78" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$15"/>
</segment>
</net>
<net name="VGA_G7" class="0">
<segment>
<wire x1="20.32" y1="160.02" x2="20.32" y2="154.94" width="0.1524" layer="91"/>
<label x="20.32" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$16"/>
</segment>
</net>
<net name="VGA_BLANK_N" class="0">
<segment>
<wire x1="22.86" y1="160.02" x2="22.86" y2="154.94" width="0.1524" layer="91"/>
<label x="22.86" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$17"/>
</segment>
</net>
<net name="VGA_R0" class="0">
<segment>
<wire x1="-17.78" y1="160.02" x2="-17.78" y2="154.94" width="0.1524" layer="91"/>
<label x="-17.78" y="157.48" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="HSYNC" class="0">
<segment>
<wire x1="48.26" y1="160.02" x2="48.26" y2="154.94" width="0.1524" layer="91"/>
<label x="48.26" y="160.02" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$27"/>
</segment>
</net>
<net name="VSYNC" class="0">
<segment>
<wire x1="50.8" y1="160.02" x2="50.8" y2="154.94" width="0.1524" layer="91"/>
<label x="50.8" y="160.02" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$28"/>
</segment>
</net>
<net name="USB1DM" class="0">
<segment>
<wire x1="170.18" y1="-60.96" x2="170.18" y2="-48.26" width="0.1524" layer="91"/>
<label x="172.72" y="-58.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$1" gate="G$1" pin="P$151"/>
</segment>
</net>
<net name="SD_DAT2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$146"/>
<wire x1="200.66" y1="-30.48" x2="177.8" y2="-30.48" width="0.1524" layer="91"/>
<label x="182.88" y="-30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_CS" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$145"/>
<wire x1="200.66" y1="-27.94" x2="177.8" y2="-27.94" width="0.1524" layer="91"/>
<label x="182.88" y="-27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_MOSI" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$144"/>
<wire x1="200.66" y1="-25.4" x2="177.8" y2="-25.4" width="0.1524" layer="91"/>
<label x="182.88" y="-25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_SCK" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$143"/>
<wire x1="200.66" y1="-22.86" x2="177.8" y2="-22.86" width="0.1524" layer="91"/>
<label x="182.88" y="-22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_MISO" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$142"/>
<wire x1="200.66" y1="-20.32" x2="177.8" y2="-20.32" width="0.1524" layer="91"/>
<label x="182.88" y="-20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_DAT1" class="0">
<segment>
<wire x1="200.66" y1="-17.78" x2="177.8" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="P$141"/>
<label x="182.88" y="-17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA0" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$49"/>
<wire x1="104.14" y1="165.1" x2="104.14" y2="154.94" width="0.1524" layer="91"/>
<label x="104.14" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$51"/>
<wire x1="109.22" y1="165.1" x2="109.22" y2="154.94" width="0.1524" layer="91"/>
<label x="109.22" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$53"/>
<wire x1="114.3" y1="165.1" x2="114.3" y2="154.94" width="0.1524" layer="91"/>
<label x="114.3" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$55"/>
<wire x1="119.38" y1="165.1" x2="119.38" y2="154.94" width="0.1524" layer="91"/>
<label x="119.38" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA4" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$57"/>
<wire x1="124.46" y1="165.1" x2="124.46" y2="154.94" width="0.1524" layer="91"/>
<label x="124.46" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA5" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$59"/>
<wire x1="129.54" y1="165.1" x2="129.54" y2="154.94" width="0.1524" layer="91"/>
<label x="129.54" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA6" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$61"/>
<wire x1="134.62" y1="165.1" x2="134.62" y2="154.94" width="0.1524" layer="91"/>
<label x="134.62" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA7" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$63"/>
<wire x1="139.7" y1="165.1" x2="139.7" y2="154.94" width="0.1524" layer="91"/>
<label x="139.7" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB0" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$50"/>
<wire x1="106.68" y1="175.26" x2="106.68" y2="154.94" width="0.1524" layer="91"/>
<label x="106.68" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$52"/>
<wire x1="111.76" y1="175.26" x2="111.76" y2="154.94" width="0.1524" layer="91"/>
<label x="111.76" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$54"/>
<wire x1="116.84" y1="175.26" x2="116.84" y2="154.94" width="0.1524" layer="91"/>
<label x="116.84" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$56"/>
<wire x1="119.38" y1="175.26" x2="121.92" y2="175.26" width="0.1524" layer="91"/>
<wire x1="121.92" y1="175.26" x2="121.92" y2="154.94" width="0.1524" layer="91"/>
<label x="121.92" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB4" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$58"/>
<wire x1="127" y1="175.26" x2="127" y2="154.94" width="0.1524" layer="91"/>
<label x="127" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB5" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$60"/>
<wire x1="132.08" y1="175.26" x2="132.08" y2="154.94" width="0.1524" layer="91"/>
<label x="132.08" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB6" class="0">
<segment>
<wire x1="137.16" y1="175.26" x2="137.16" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="P$62"/>
<label x="137.16" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB7" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$64"/>
<wire x1="142.24" y1="175.26" x2="142.24" y2="154.94" width="0.1524" layer="91"/>
<label x="142.24" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB8" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$66"/>
<wire x1="147.32" y1="175.26" x2="147.32" y2="154.94" width="0.1524" layer="91"/>
<label x="147.32" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB9" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$68"/>
<wire x1="152.4" y1="175.26" x2="152.4" y2="154.94" width="0.1524" layer="91"/>
<label x="152.4" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB10" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$70"/>
<wire x1="157.48" y1="175.26" x2="157.48" y2="154.94" width="0.1524" layer="91"/>
<label x="157.48" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB11" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$72"/>
<wire x1="162.56" y1="175.26" x2="162.56" y2="154.94" width="0.1524" layer="91"/>
<label x="162.56" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB12" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$74"/>
<wire x1="167.64" y1="175.26" x2="167.64" y2="154.94" width="0.1524" layer="91"/>
<label x="167.64" y="170.18" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOB13" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$76"/>
<wire x1="205.74" y1="147.32" x2="177.8" y2="147.32" width="0.1524" layer="91"/>
<label x="200.66" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB14" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$78"/>
<wire x1="205.74" y1="142.24" x2="177.8" y2="142.24" width="0.1524" layer="91"/>
<label x="200.66" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB15" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$80"/>
<wire x1="205.74" y1="137.16" x2="177.8" y2="137.16" width="0.1524" layer="91"/>
<label x="200.66" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB16" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$82"/>
<wire x1="205.74" y1="132.08" x2="177.8" y2="132.08" width="0.1524" layer="91"/>
<label x="200.66" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB17" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$84"/>
<wire x1="205.74" y1="127" x2="177.8" y2="127" width="0.1524" layer="91"/>
<label x="200.66" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA8" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$65"/>
<wire x1="144.78" y1="165.1" x2="144.78" y2="154.94" width="0.1524" layer="91"/>
<label x="144.78" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA9" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$67"/>
<wire x1="149.86" y1="165.1" x2="149.86" y2="154.94" width="0.1524" layer="91"/>
<label x="149.86" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA10" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$69"/>
<wire x1="154.94" y1="165.1" x2="154.94" y2="154.94" width="0.1524" layer="91"/>
<label x="154.94" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA11" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$71"/>
<wire x1="160.02" y1="165.1" x2="160.02" y2="154.94" width="0.1524" layer="91"/>
<label x="160.02" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA12" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$73"/>
<wire x1="165.1" y1="165.1" x2="165.1" y2="154.94" width="0.1524" layer="91"/>
<label x="165.1" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA13" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$75"/>
<wire x1="170.18" y1="165.1" x2="170.18" y2="154.94" width="0.1524" layer="91"/>
<label x="170.18" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOA14" class="0">
<segment>
<wire x1="193.04" y1="144.78" x2="177.8" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="P$77"/>
<label x="187.96" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA15" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$79"/>
<wire x1="193.04" y1="139.7" x2="177.8" y2="139.7" width="0.1524" layer="91"/>
<label x="187.96" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA16" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$81"/>
<wire x1="193.04" y1="134.62" x2="177.8" y2="134.62" width="0.1524" layer="91"/>
<label x="187.96" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA17" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$83"/>
<wire x1="193.04" y1="129.54" x2="177.8" y2="129.54" width="0.1524" layer="91"/>
<label x="187.96" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA18" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$85"/>
<wire x1="193.04" y1="124.46" x2="177.8" y2="124.46" width="0.1524" layer="91"/>
<label x="187.96" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA19" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$87"/>
<wire x1="193.04" y1="119.38" x2="177.8" y2="119.38" width="0.1524" layer="91"/>
<label x="187.96" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA20" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$89"/>
<wire x1="193.04" y1="114.3" x2="177.8" y2="114.3" width="0.1524" layer="91"/>
<label x="187.96" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA21" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$91"/>
<wire x1="193.04" y1="109.22" x2="177.8" y2="109.22" width="0.1524" layer="91"/>
<label x="187.96" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA22" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$93"/>
<wire x1="193.04" y1="104.14" x2="177.8" y2="104.14" width="0.1524" layer="91"/>
<label x="187.96" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA23" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$95"/>
<wire x1="193.04" y1="99.06" x2="177.8" y2="99.06" width="0.1524" layer="91"/>
<label x="187.96" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA24" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$97"/>
<wire x1="193.04" y1="93.98" x2="177.8" y2="93.98" width="0.1524" layer="91"/>
<label x="187.96" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA25" class="0">
<segment>
<wire x1="193.04" y1="88.9" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="P$99"/>
<label x="187.96" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA26" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$101"/>
<wire x1="193.04" y1="83.82" x2="177.8" y2="83.82" width="0.1524" layer="91"/>
<label x="187.96" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA27" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$103"/>
<wire x1="193.04" y1="78.74" x2="177.8" y2="78.74" width="0.1524" layer="91"/>
<label x="187.96" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA28" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$105"/>
<wire x1="193.04" y1="73.66" x2="177.8" y2="73.66" width="0.1524" layer="91"/>
<label x="187.96" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA29" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$107"/>
<wire x1="193.04" y1="68.58" x2="177.8" y2="68.58" width="0.1524" layer="91"/>
<label x="187.96" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA30" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$109"/>
<wire x1="193.04" y1="63.5" x2="177.8" y2="63.5" width="0.1524" layer="91"/>
<label x="187.96" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA31" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$111"/>
<wire x1="193.04" y1="58.42" x2="177.8" y2="58.42" width="0.1524" layer="91"/>
<label x="187.96" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA32" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$113"/>
<wire x1="193.04" y1="53.34" x2="177.8" y2="53.34" width="0.1524" layer="91"/>
<label x="187.96" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA33" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$115"/>
<wire x1="193.04" y1="48.26" x2="177.8" y2="48.26" width="0.1524" layer="91"/>
<label x="187.96" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA34" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$117"/>
<wire x1="193.04" y1="43.18" x2="177.8" y2="43.18" width="0.1524" layer="91"/>
<label x="187.96" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOA35" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$119"/>
<wire x1="193.04" y1="38.1" x2="177.8" y2="38.1" width="0.1524" layer="91"/>
<label x="187.96" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB18" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$86"/>
<wire x1="205.74" y1="121.92" x2="177.8" y2="121.92" width="0.1524" layer="91"/>
<label x="200.66" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB19" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$88"/>
<wire x1="205.74" y1="116.84" x2="177.8" y2="116.84" width="0.1524" layer="91"/>
<label x="200.66" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB20" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$90"/>
<wire x1="205.74" y1="111.76" x2="177.8" y2="111.76" width="0.1524" layer="91"/>
<label x="200.66" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB21" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$92"/>
<wire x1="205.74" y1="106.68" x2="177.8" y2="106.68" width="0.1524" layer="91"/>
<label x="200.66" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB22" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$94"/>
<wire x1="205.74" y1="101.6" x2="177.8" y2="101.6" width="0.1524" layer="91"/>
<label x="200.66" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB23" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$96"/>
<wire x1="205.74" y1="96.52" x2="177.8" y2="96.52" width="0.1524" layer="91"/>
<label x="200.66" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB24" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$98"/>
<wire x1="205.74" y1="91.44" x2="177.8" y2="91.44" width="0.1524" layer="91"/>
<label x="200.66" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB25" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$100"/>
<wire x1="205.74" y1="86.36" x2="177.8" y2="86.36" width="0.1524" layer="91"/>
<label x="200.66" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB26" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$102"/>
<wire x1="205.74" y1="81.28" x2="177.8" y2="81.28" width="0.1524" layer="91"/>
<label x="200.66" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB27" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$104"/>
<wire x1="205.74" y1="76.2" x2="177.8" y2="76.2" width="0.1524" layer="91"/>
<label x="200.66" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB28" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$106"/>
<wire x1="205.74" y1="71.12" x2="177.8" y2="71.12" width="0.1524" layer="91"/>
<label x="200.66" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB29" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$108"/>
<wire x1="205.74" y1="66.04" x2="177.8" y2="66.04" width="0.1524" layer="91"/>
<label x="200.66" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB30" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$110"/>
<wire x1="205.74" y1="60.96" x2="177.8" y2="60.96" width="0.1524" layer="91"/>
<label x="200.66" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB31" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$112"/>
<wire x1="205.74" y1="55.88" x2="177.8" y2="55.88" width="0.1524" layer="91"/>
<label x="200.66" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB32" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$114"/>
<wire x1="205.74" y1="50.8" x2="177.8" y2="50.8" width="0.1524" layer="91"/>
<label x="200.66" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB33" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$116"/>
<wire x1="205.74" y1="45.72" x2="177.8" y2="45.72" width="0.1524" layer="91"/>
<label x="200.66" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB34" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$118"/>
<wire x1="205.74" y1="40.64" x2="177.8" y2="40.64" width="0.1524" layer="91"/>
<label x="200.66" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOB35" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$120"/>
<wire x1="205.74" y1="35.56" x2="177.8" y2="35.56" width="0.1524" layer="91"/>
<label x="200.66" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_DETECT" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$140"/>
<wire x1="200.66" y1="-15.24" x2="177.8" y2="-15.24" width="0.1524" layer="91"/>
<label x="182.88" y="-15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_WRITEPROTECT" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$139"/>
<wire x1="200.66" y1="-12.7" x2="177.8" y2="-12.7" width="0.1524" layer="91"/>
<label x="182.88" y="-12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$192"/>
<wire x1="66.04" y1="-60.96" x2="66.04" y2="-48.26" width="0.1524" layer="91"/>
<label x="66.04" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$193"/>
<wire x1="63.5" y1="-60.96" x2="63.5" y2="-48.26" width="0.1524" layer="91"/>
<label x="63.5" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$172"/>
<wire x1="116.84" y1="-60.96" x2="116.84" y2="-48.26" width="0.1524" layer="91"/>
<label x="116.84" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$173"/>
<wire x1="114.3" y1="-60.96" x2="114.3" y2="-48.26" width="0.1524" layer="91"/>
<label x="114.3" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$174"/>
<wire x1="111.76" y1="-60.96" x2="111.76" y2="-48.26" width="0.1524" layer="91"/>
<label x="111.76" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$175"/>
<wire x1="109.22" y1="-60.96" x2="109.22" y2="-48.26" width="0.1524" layer="91"/>
<label x="109.22" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="A10" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$176"/>
<wire x1="106.68" y1="-60.96" x2="106.68" y2="-48.26" width="0.1524" layer="91"/>
<label x="106.68" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="BA1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$177"/>
<wire x1="104.14" y1="-60.96" x2="104.14" y2="-48.26" width="0.1524" layer="91"/>
<label x="104.14" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="BA0" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$178"/>
<wire x1="101.6" y1="-60.96" x2="101.6" y2="-48.26" width="0.1524" layer="91"/>
<label x="101.6" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="CS_N" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$179"/>
<wire x1="99.06" y1="-60.96" x2="99.06" y2="-48.26" width="0.1524" layer="91"/>
<label x="99.06" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="RAS_N" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$180"/>
<wire x1="96.52" y1="-60.96" x2="96.52" y2="-48.26" width="0.1524" layer="91"/>
<label x="96.52" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="CAS_N" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$181"/>
<wire x1="93.98" y1="-60.96" x2="93.98" y2="-48.26" width="0.1524" layer="91"/>
<label x="93.98" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="DQ4" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$187"/>
<wire x1="78.74" y1="-60.96" x2="78.74" y2="-48.26" width="0.1524" layer="91"/>
<label x="78.74" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="DQ5" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$186"/>
<wire x1="81.28" y1="-60.96" x2="81.28" y2="-48.26" width="0.1524" layer="91"/>
<label x="81.28" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="DQ6" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$185"/>
<wire x1="83.82" y1="-60.96" x2="83.82" y2="-48.26" width="0.1524" layer="91"/>
<label x="83.82" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="DQ7" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$184"/>
<wire x1="86.36" y1="-60.96" x2="86.36" y2="-48.26" width="0.1524" layer="91"/>
<label x="86.36" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SDRAM_LDQM" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$183"/>
<wire x1="88.9" y1="-60.96" x2="88.9" y2="-48.26" width="0.1524" layer="91"/>
<label x="88.9" y="-68.58" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="WE_N" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$182"/>
<wire x1="91.44" y1="-60.96" x2="91.44" y2="-48.26" width="0.1524" layer="91"/>
<label x="91.44" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="DQ0" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$188"/>
<wire x1="76.2" y1="-60.96" x2="76.2" y2="-48.26" width="0.1524" layer="91"/>
<label x="76.2" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="DQ1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$189"/>
<wire x1="73.66" y1="-60.96" x2="73.66" y2="-48.26" width="0.1524" layer="91"/>
<label x="73.66" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="DQ2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$190"/>
<wire x1="71.12" y1="-60.96" x2="71.12" y2="-48.26" width="0.1524" layer="91"/>
<label x="71.12" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="DQ3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$191"/>
<wire x1="68.58" y1="-60.96" x2="68.58" y2="-48.26" width="0.1524" layer="91"/>
<label x="68.58" y="-58.42" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC35" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$138"/>
<wire x1="205.74" y1="-10.16" x2="177.8" y2="-10.16" width="0.1524" layer="91"/>
<label x="182.88" y="-10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC34" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$137"/>
<wire x1="205.74" y1="-7.62" x2="177.8" y2="-7.62" width="0.1524" layer="91"/>
<label x="182.88" y="-7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC33" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$136"/>
<wire x1="205.74" y1="-5.08" x2="177.8" y2="-5.08" width="0.1524" layer="91"/>
<label x="182.88" y="-5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC32" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$135"/>
<wire x1="205.74" y1="-2.54" x2="177.8" y2="-2.54" width="0.1524" layer="91"/>
<label x="182.88" y="-2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC31" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$134"/>
<wire x1="205.74" y1="0" x2="177.8" y2="0" width="0.1524" layer="91"/>
<label x="182.88" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC30" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$133"/>
<wire x1="205.74" y1="2.54" x2="177.8" y2="2.54" width="0.1524" layer="91"/>
<label x="182.88" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC29" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$132"/>
<wire x1="205.74" y1="5.08" x2="177.8" y2="5.08" width="0.1524" layer="91"/>
<label x="182.88" y="5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC28" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$131"/>
<wire x1="205.74" y1="7.62" x2="177.8" y2="7.62" width="0.1524" layer="91"/>
<label x="182.88" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC27" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$130"/>
<wire x1="205.74" y1="10.16" x2="177.8" y2="10.16" width="0.1524" layer="91"/>
<label x="182.88" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC26" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$129"/>
<wire x1="205.74" y1="12.7" x2="177.8" y2="12.7" width="0.1524" layer="91"/>
<label x="182.88" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC25" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$128"/>
<wire x1="205.74" y1="15.24" x2="177.8" y2="15.24" width="0.1524" layer="91"/>
<label x="182.88" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC24" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$127"/>
<wire x1="205.74" y1="17.78" x2="177.8" y2="17.78" width="0.1524" layer="91"/>
<label x="182.88" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC23" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$126"/>
<wire x1="205.74" y1="20.32" x2="177.8" y2="20.32" width="0.1524" layer="91"/>
<label x="182.88" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC22" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$125"/>
<wire x1="205.74" y1="22.86" x2="177.8" y2="22.86" width="0.1524" layer="91"/>
<label x="182.88" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC21" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$124"/>
<wire x1="205.74" y1="25.4" x2="177.8" y2="25.4" width="0.1524" layer="91"/>
<label x="182.88" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC20" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$123"/>
<wire x1="205.74" y1="27.94" x2="177.8" y2="27.94" width="0.1524" layer="91"/>
<label x="182.88" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC19" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$122"/>
<wire x1="205.74" y1="30.48" x2="177.8" y2="30.48" width="0.1524" layer="91"/>
<label x="182.88" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC18" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$121"/>
<wire x1="205.74" y1="33.02" x2="177.8" y2="33.02" width="0.1524" layer="91"/>
<label x="182.88" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIOC0" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$31"/>
<wire x1="58.42" y1="167.64" x2="58.42" y2="154.94" width="0.1524" layer="91"/>
<label x="58.42" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$32"/>
<wire x1="60.96" y1="167.64" x2="60.96" y2="154.94" width="0.1524" layer="91"/>
<label x="60.96" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$33"/>
<wire x1="63.5" y1="167.64" x2="63.5" y2="154.94" width="0.1524" layer="91"/>
<label x="63.5" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$34"/>
<wire x1="66.04" y1="167.64" x2="66.04" y2="154.94" width="0.1524" layer="91"/>
<label x="66.04" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC4" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$35"/>
<wire x1="68.58" y1="167.64" x2="68.58" y2="154.94" width="0.1524" layer="91"/>
<label x="68.58" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC5" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$36"/>
<wire x1="71.12" y1="167.64" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<label x="71.12" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC6" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$37"/>
<wire x1="73.66" y1="167.64" x2="73.66" y2="154.94" width="0.1524" layer="91"/>
<label x="73.66" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC7" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$38"/>
<wire x1="76.2" y1="167.64" x2="76.2" y2="154.94" width="0.1524" layer="91"/>
<label x="76.2" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC8" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$39"/>
<wire x1="78.74" y1="167.64" x2="78.74" y2="154.94" width="0.1524" layer="91"/>
<label x="78.74" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC9" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$40"/>
<wire x1="81.28" y1="167.64" x2="81.28" y2="154.94" width="0.1524" layer="91"/>
<label x="81.28" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC10" class="0">
<segment>
<wire x1="83.82" y1="167.64" x2="83.82" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="P$41"/>
<label x="83.82" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC11" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$42"/>
<wire x1="86.36" y1="167.64" x2="86.36" y2="154.94" width="0.1524" layer="91"/>
<label x="86.36" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC12" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$43"/>
<wire x1="88.9" y1="167.64" x2="88.9" y2="154.94" width="0.1524" layer="91"/>
<label x="88.9" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC13" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$44"/>
<wire x1="91.44" y1="167.64" x2="91.44" y2="154.94" width="0.1524" layer="91"/>
<label x="91.44" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC14" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$45"/>
<wire x1="93.98" y1="167.64" x2="93.98" y2="154.94" width="0.1524" layer="91"/>
<label x="93.98" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC15" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$46"/>
<wire x1="96.52" y1="167.64" x2="96.52" y2="154.94" width="0.1524" layer="91"/>
<label x="96.52" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC16" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$47"/>
<wire x1="99.06" y1="167.64" x2="99.06" y2="154.94" width="0.1524" layer="91"/>
<label x="99.06" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GPIOC17" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$48"/>
<wire x1="101.6" y1="167.64" x2="101.6" y2="154.94" width="0.1524" layer="91"/>
<label x="101.6" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>FPGA decoupling</description>
<plain>
<text x="-17.78" y="109.22" size="1.778" layer="91">Values from the Altera PDN spreadsheet. 400mA 1.1V, 200mA 2.5V and 700mA 3.3V. </text>
</plain>
<instances>
<instance part="C1" gate="G$1" x="63.5" y="68.58"/>
<instance part="C2" gate="G$1" x="73.66" y="68.58"/>
<instance part="C3" gate="G$1" x="83.82" y="68.58"/>
<instance part="C5" gate="G$1" x="-15.24" y="68.58"/>
<instance part="C6" gate="G$1" x="-5.08" y="68.58"/>
<instance part="C7" gate="G$1" x="5.08" y="68.58"/>
<instance part="C9" gate="G$1" x="63.5" y="86.36"/>
<instance part="C10" gate="G$1" x="73.66" y="86.36"/>
<instance part="C11" gate="G$1" x="83.82" y="86.36"/>
<instance part="C12" gate="G$1" x="93.98" y="86.36"/>
<instance part="GND7" gate="1" x="45.72" y="73.66"/>
<instance part="C28" gate="G$1" x="104.14" y="86.36"/>
<instance part="C4" gate="G$1" x="20.32" y="68.58"/>
<instance part="C8" gate="G$1" x="99.06" y="68.58"/>
<instance part="C29" gate="G$1" x="116.84" y="86.36"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="-25.4" y1="76.2" x2="-15.24" y2="76.2" width="0.1524" layer="91"/>
<label x="-25.4" y="76.2" size="1.778" layer="95"/>
<wire x1="-15.24" y1="76.2" x2="-5.08" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="76.2" x2="5.08" y2="76.2" width="0.1524" layer="91"/>
<wire x1="5.08" y1="76.2" x2="20.32" y2="76.2" width="0.1524" layer="91"/>
<wire x1="20.32" y1="76.2" x2="45.72" y2="76.2" width="0.1524" layer="91"/>
<wire x1="45.72" y1="76.2" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<wire x1="63.5" y1="76.2" x2="73.66" y2="76.2" width="0.1524" layer="91"/>
<wire x1="73.66" y1="76.2" x2="83.82" y2="76.2" width="0.1524" layer="91"/>
<wire x1="83.82" y1="76.2" x2="93.98" y2="76.2" width="0.1524" layer="91"/>
<wire x1="93.98" y1="76.2" x2="99.06" y2="76.2" width="0.1524" layer="91"/>
<junction x="93.98" y="76.2"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="99.06" y1="76.2" x2="104.14" y2="76.2" width="0.1524" layer="91"/>
<wire x1="104.14" y1="76.2" x2="116.84" y2="76.2" width="0.1524" layer="91"/>
<wire x1="116.84" y1="76.2" x2="152.4" y2="76.2" width="0.1524" layer="91"/>
<wire x1="83.82" y1="71.12" x2="83.82" y2="76.2" width="0.1524" layer="91"/>
<junction x="83.82" y="76.2"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="73.66" y1="71.12" x2="73.66" y2="76.2" width="0.1524" layer="91"/>
<junction x="73.66" y="76.2"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="63.5" y1="71.12" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<junction x="63.5" y="76.2"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="63.5" y1="81.28" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="73.66" y1="81.28" x2="73.66" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="83.82" y1="76.2" x2="83.82" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="93.98" y1="76.2" x2="93.98" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="5.08" y1="71.12" x2="5.08" y2="76.2" width="0.1524" layer="91"/>
<junction x="5.08" y="76.2"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="71.12" x2="-5.08" y2="76.2" width="0.1524" layer="91"/>
<junction x="-5.08" y="76.2"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="71.12" x2="-15.24" y2="76.2" width="0.1524" layer="91"/>
<junction x="-15.24" y="76.2"/>
<pinref part="GND7" gate="1" pin="GND"/>
<junction x="45.72" y="76.2"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="104.14" y1="76.2" x2="104.14" y2="81.28" width="0.1524" layer="91"/>
<junction x="104.14" y="76.2"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="20.32" y1="71.12" x2="20.32" y2="76.2" width="0.1524" layer="91"/>
<junction x="20.32" y="76.2"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="99.06" y1="71.12" x2="99.06" y2="76.2" width="0.1524" layer="91"/>
<junction x="99.06" y="76.2"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="116.84" y1="81.28" x2="116.84" y2="76.2" width="0.1524" layer="91"/>
<junction x="116.84" y="76.2"/>
</segment>
</net>
<net name="2V5" class="0">
<segment>
<wire x1="-25.4" y1="60.96" x2="-15.24" y2="60.96" width="0.1524" layer="91"/>
<label x="-25.4" y="60.96" size="1.778" layer="95"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="-15.24" y1="60.96" x2="-5.08" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="60.96" x2="5.08" y2="60.96" width="0.1524" layer="91"/>
<wire x1="5.08" y1="60.96" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<wire x1="20.32" y1="60.96" x2="43.18" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="63.5" x2="-15.24" y2="60.96" width="0.1524" layer="91"/>
<junction x="-15.24" y="60.96"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="63.5" x2="-5.08" y2="60.96" width="0.1524" layer="91"/>
<junction x="-5.08" y="60.96"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="5.08" y1="63.5" x2="5.08" y2="60.96" width="0.1524" layer="91"/>
<junction x="5.08" y="60.96"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="20.32" y1="63.5" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<junction x="20.32" y="60.96"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<wire x1="53.34" y1="93.98" x2="63.5" y2="93.98" width="0.1524" layer="91"/>
<label x="53.34" y="93.98" size="1.778" layer="95"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="63.5" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<wire x1="73.66" y1="93.98" x2="83.82" y2="93.98" width="0.1524" layer="91"/>
<wire x1="83.82" y1="93.98" x2="93.98" y2="93.98" width="0.1524" layer="91"/>
<wire x1="93.98" y1="93.98" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<wire x1="104.14" y1="93.98" x2="111.76" y2="93.98" width="0.1524" layer="91"/>
<wire x1="93.98" y1="88.9" x2="93.98" y2="93.98" width="0.1524" layer="91"/>
<junction x="93.98" y="93.98"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="83.82" y1="88.9" x2="83.82" y2="93.98" width="0.1524" layer="91"/>
<junction x="83.82" y="93.98"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="63.5" y1="93.98" x2="63.5" y2="88.9" width="0.1524" layer="91"/>
<junction x="63.5" y="93.98"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="73.66" y1="93.98" x2="73.66" y2="88.9" width="0.1524" layer="91"/>
<junction x="73.66" y="93.98"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="104.14" y1="93.98" x2="104.14" y2="88.9" width="0.1524" layer="91"/>
<junction x="104.14" y="93.98"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="116.84" y1="88.9" x2="111.76" y2="88.9" width="0.1524" layer="91"/>
<wire x1="111.76" y1="88.9" x2="111.76" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="1V1" class="0">
<segment>
<wire x1="53.34" y1="60.96" x2="63.5" y2="60.96" width="0.1524" layer="91"/>
<label x="53.34" y="60.96" size="1.778" layer="95"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="63.5" y1="60.96" x2="73.66" y2="60.96" width="0.1524" layer="91"/>
<wire x1="73.66" y1="60.96" x2="83.82" y2="60.96" width="0.1524" layer="91"/>
<wire x1="83.82" y1="60.96" x2="99.06" y2="60.96" width="0.1524" layer="91"/>
<wire x1="99.06" y1="60.96" x2="109.22" y2="60.96" width="0.1524" layer="91"/>
<wire x1="63.5" y1="60.96" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<junction x="63.5" y="60.96"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="73.66" y1="63.5" x2="73.66" y2="60.96" width="0.1524" layer="91"/>
<junction x="73.66" y="60.96"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="83.82" y1="63.5" x2="83.82" y2="60.96" width="0.1524" layer="91"/>
<junction x="83.82" y="60.96"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="99.06" y1="63.5" x2="99.06" y2="60.96" width="0.1524" layer="91"/>
<junction x="99.06" y="60.96"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Video</description>
<plain>
</plain>
<instances>
<instance part="VGA" gate="G$1" x="114.3" y="124.46"/>
<instance part="VIDEODACSA" gate="A" x="-5.08" y="81.28"/>
<instance part="GND1" gate="1" x="20.32" y="109.22"/>
<instance part="GND2" gate="1" x="-43.18" y="25.4"/>
<instance part="GND3" gate="1" x="114.3" y="88.9"/>
<instance part="R11" gate="G$1" x="38.1" y="121.92" rot="R90"/>
<instance part="R12" gate="G$1" x="45.72" y="121.92" rot="R90"/>
<instance part="R13" gate="G$1" x="53.34" y="121.92" rot="R90"/>
<instance part="GND4" gate="1" x="38.1" y="109.22"/>
<instance part="GND5" gate="1" x="45.72" y="109.22"/>
<instance part="GND6" gate="1" x="53.34" y="109.22"/>
<instance part="C13" gate="G$1" x="-43.18" y="45.72"/>
<instance part="R14" gate="G$1" x="-33.02" y="40.64" rot="R90"/>
<instance part="C14" gate="G$1" x="-40.64" y="139.7"/>
<instance part="R31" gate="G$1" x="154.94" y="78.74" rot="R90"/>
<instance part="R32" gate="G$1" x="170.18" y="78.74" rot="R90"/>
<instance part="S-VIDEO" gate="G$1" x="109.22" y="58.42"/>
<instance part="C-VIDEO" gate="B" x="63.5" y="58.42"/>
<instance part="HDMI" gate="G$1" x="-88.9" y="104.14"/>
<instance part="GND20" gate="1" x="-68.58" y="66.04"/>
<instance part="RN28" gate="G$1" x="-116.84" y="116.84"/>
<instance part="RN29" gate="G$1" x="-116.84" y="106.68"/>
</instances>
<busses>
<bus name="VGADAC:VGA_R[0..7],VGA_G[0..7],VGA_B[0..7],VGA_CLK,VGA_BLANK_N,HSYNC,VSYNC">
<segment>
<wire x1="-50.8" y1="129.54" x2="-50.8" y2="43.18" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="20.32" y1="116.84" x2="20.32" y2="111.76" width="0.1524" layer="91"/>
<pinref part="VIDEODACSA" gate="A" pin="~IOB"/>
<pinref part="VIDEODACSA" gate="A" pin="~IOG"/>
<pinref part="VIDEODACSA" gate="A" pin="~IOR"/>
<wire x1="12.7" y1="127" x2="20.32" y2="127" width="0.1524" layer="91"/>
<wire x1="20.32" y1="127" x2="20.32" y2="124.46" width="0.1524" layer="91"/>
<wire x1="20.32" y1="124.46" x2="12.7" y2="124.46" width="0.1524" layer="91"/>
<junction x="20.32" y="124.46"/>
<wire x1="20.32" y1="124.46" x2="20.32" y2="121.92" width="0.1524" layer="91"/>
<wire x1="20.32" y1="121.92" x2="12.7" y2="121.92" width="0.1524" layer="91"/>
<junction x="20.32" y="121.92"/>
<wire x1="20.32" y1="121.92" x2="20.32" y2="119.38" width="0.1524" layer="91"/>
<wire x1="20.32" y1="119.38" x2="20.32" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="VIDEODACSA" gate="A" pin="GND_2"/>
<wire x1="-22.86" y1="38.1" x2="-27.94" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="38.1" x2="-27.94" y2="35.56" width="0.1524" layer="91"/>
<pinref part="VIDEODACSA" gate="A" pin="GND"/>
<wire x1="-27.94" y1="35.56" x2="-27.94" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="33.02" x2="-27.94" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="30.48" x2="-27.94" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="27.94" x2="-27.94" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="25.4" x2="-27.94" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="22.86" x2="-27.94" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="20.32" x2="-27.94" y2="20.32" width="0.1524" layer="91"/>
<pinref part="VIDEODACSA" gate="A" pin="GND_8"/>
<wire x1="-27.94" y1="22.86" x2="-22.86" y2="22.86" width="0.1524" layer="91"/>
<junction x="-27.94" y="22.86"/>
<pinref part="VIDEODACSA" gate="A" pin="GND_7"/>
<wire x1="-22.86" y1="25.4" x2="-27.94" y2="25.4" width="0.1524" layer="91"/>
<junction x="-27.94" y="25.4"/>
<pinref part="VIDEODACSA" gate="A" pin="GND_6"/>
<wire x1="-22.86" y1="27.94" x2="-27.94" y2="27.94" width="0.1524" layer="91"/>
<junction x="-27.94" y="27.94"/>
<pinref part="VIDEODACSA" gate="A" pin="GND_5"/>
<wire x1="-22.86" y1="30.48" x2="-27.94" y2="30.48" width="0.1524" layer="91"/>
<junction x="-27.94" y="30.48"/>
<pinref part="VIDEODACSA" gate="A" pin="GND_4"/>
<wire x1="-22.86" y1="33.02" x2="-27.94" y2="33.02" width="0.1524" layer="91"/>
<junction x="-27.94" y="33.02"/>
<pinref part="VIDEODACSA" gate="A" pin="GND_3"/>
<wire x1="-22.86" y1="35.56" x2="-27.94" y2="35.56" width="0.1524" layer="91"/>
<junction x="-27.94" y="35.56"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="-27.94" y1="27.94" x2="-33.02" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="-33.02" y1="27.94" x2="-43.18" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="40.64" x2="-43.18" y2="27.94" width="0.1524" layer="91"/>
<junction x="-43.18" y="27.94"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="-33.02" y1="35.56" x2="-33.02" y2="27.94" width="0.1524" layer="91"/>
<junction x="-33.02" y="27.94"/>
<pinref part="VIDEODACSA" gate="A" pin="~SYNC"/>
<wire x1="-22.86" y1="55.88" x2="-48.26" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="55.88" x2="-48.26" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="27.94" x2="-43.18" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="142.24" y1="134.62" x2="142.24" y2="129.54" width="0.1524" layer="91"/>
<wire x1="142.24" y1="129.54" x2="142.24" y2="124.46" width="0.1524" layer="91"/>
<wire x1="142.24" y1="124.46" x2="142.24" y2="114.3" width="0.1524" layer="91"/>
<wire x1="142.24" y1="114.3" x2="142.24" y2="99.06" width="0.1524" layer="91"/>
<pinref part="VGA" gate="G$1" pin="5"/>
<wire x1="106.68" y1="111.76" x2="99.06" y2="111.76" width="0.1524" layer="91"/>
<wire x1="99.06" y1="111.76" x2="99.06" y2="99.06" width="0.1524" layer="91"/>
<wire x1="99.06" y1="99.06" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="114.3" y1="99.06" x2="142.24" y2="99.06" width="0.1524" layer="91"/>
<wire x1="114.3" y1="91.44" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
<junction x="114.3" y="99.06"/>
<pinref part="VGA" gate="G$1" pin="8"/>
<wire x1="122.174" y1="124.46" x2="142.24" y2="124.46" width="0.1524" layer="91"/>
<junction x="142.24" y="124.46"/>
<pinref part="VGA" gate="G$1" pin="10"/>
<wire x1="122.174" y1="114.3" x2="142.24" y2="114.3" width="0.1524" layer="91"/>
<junction x="142.24" y="114.3"/>
<pinref part="VGA" gate="G$1" pin="6"/>
<wire x1="122.174" y1="134.62" x2="142.24" y2="134.62" width="0.1524" layer="91"/>
<pinref part="VGA" gate="G$1" pin="7"/>
<wire x1="122.174" y1="129.54" x2="142.24" y2="129.54" width="0.1524" layer="91"/>
<junction x="142.24" y="129.54"/>
<pinref part="S-VIDEO" gate="G$1" pin="1"/>
<wire x1="99.06" y1="63.5" x2="96.52" y2="63.5" width="0.1524" layer="91"/>
<wire x1="96.52" y1="63.5" x2="96.52" y2="99.06" width="0.1524" layer="91"/>
<wire x1="96.52" y1="99.06" x2="99.06" y2="99.06" width="0.1524" layer="91"/>
<junction x="99.06" y="99.06"/>
<pinref part="S-VIDEO" gate="G$1" pin="2"/>
<wire x1="99.06" y1="60.96" x2="96.52" y2="60.96" width="0.1524" layer="91"/>
<wire x1="96.52" y1="60.96" x2="96.52" y2="63.5" width="0.1524" layer="91"/>
<junction x="96.52" y="63.5"/>
<wire x1="71.12" y1="40.64" x2="96.52" y2="40.64" width="0.1524" layer="91"/>
<wire x1="96.52" y1="40.64" x2="96.52" y2="60.96" width="0.1524" layer="91"/>
<junction x="96.52" y="60.96"/>
<wire x1="71.12" y1="40.64" x2="71.12" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C-VIDEO" gate="B" pin="2"/>
<wire x1="71.12" y1="55.88" x2="66.04" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="38.1" y1="116.84" x2="38.1" y2="111.76" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="45.72" y1="116.84" x2="45.72" y2="111.76" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="53.34" y1="111.76" x2="53.34" y2="116.84" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-60.96" y1="119.38" x2="-60.96" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="116.84" x2="-60.96" y2="114.3" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="114.3" x2="-60.96" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="111.76" x2="-60.96" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="109.22" x2="-60.96" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="106.68" x2="-60.96" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="104.14" x2="-60.96" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="101.6" x2="-60.96" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="99.06" x2="-60.96" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="71.12" x2="-68.58" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="-68.58" y1="71.12" x2="-68.58" y2="68.58" width="0.1524" layer="91"/>
<pinref part="HDMI" gate="G$1" pin="GND@1"/>
<wire x1="-60.96" y1="119.38" x2="-71.12" y2="119.38" width="0.1524" layer="91"/>
<pinref part="HDMI" gate="G$1" pin="GND@2"/>
<wire x1="-71.12" y1="116.84" x2="-60.96" y2="116.84" width="0.1524" layer="91"/>
<junction x="-60.96" y="116.84"/>
<pinref part="HDMI" gate="G$1" pin="GND@3"/>
<wire x1="-71.12" y1="114.3" x2="-60.96" y2="114.3" width="0.1524" layer="91"/>
<junction x="-60.96" y="114.3"/>
<pinref part="HDMI" gate="G$1" pin="GND@4"/>
<wire x1="-71.12" y1="111.76" x2="-60.96" y2="111.76" width="0.1524" layer="91"/>
<junction x="-60.96" y="111.76"/>
<pinref part="HDMI" gate="G$1" pin="GND@5"/>
<wire x1="-71.12" y1="109.22" x2="-60.96" y2="109.22" width="0.1524" layer="91"/>
<junction x="-60.96" y="109.22"/>
<pinref part="HDMI" gate="G$1" pin="GND@6"/>
<wire x1="-71.12" y1="106.68" x2="-60.96" y2="106.68" width="0.1524" layer="91"/>
<junction x="-60.96" y="106.68"/>
<pinref part="HDMI" gate="G$1" pin="GND@7"/>
<wire x1="-71.12" y1="104.14" x2="-60.96" y2="104.14" width="0.1524" layer="91"/>
<junction x="-60.96" y="104.14"/>
<pinref part="HDMI" gate="G$1" pin="GND@8"/>
<wire x1="-71.12" y1="101.6" x2="-60.96" y2="101.6" width="0.1524" layer="91"/>
<junction x="-60.96" y="101.6"/>
<pinref part="HDMI" gate="G$1" pin="GND@9"/>
<wire x1="-71.12" y1="99.06" x2="-60.96" y2="99.06" width="0.1524" layer="91"/>
<junction x="-60.96" y="99.06"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="IOR"/>
<wire x1="12.7" y1="137.16" x2="53.34" y2="137.16" width="0.1524" layer="91"/>
<wire x1="53.34" y1="137.16" x2="83.82" y2="137.16" width="0.1524" layer="91"/>
<wire x1="83.82" y1="137.16" x2="99.06" y2="137.16" width="0.1524" layer="91"/>
<wire x1="99.06" y1="137.16" x2="99.06" y2="132.08" width="0.1524" layer="91"/>
<pinref part="VGA" gate="G$1" pin="1"/>
<wire x1="99.06" y1="132.08" x2="106.68" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="53.34" y1="127" x2="53.34" y2="137.16" width="0.1524" layer="91"/>
<junction x="53.34" y="137.16"/>
<pinref part="S-VIDEO" gate="G$1" pin="3"/>
<wire x1="99.06" y1="58.42" x2="83.82" y2="58.42" width="0.1524" layer="91"/>
<wire x1="83.82" y1="58.42" x2="83.82" y2="137.16" width="0.1524" layer="91"/>
<junction x="83.82" y="137.16"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="IOG"/>
<wire x1="12.7" y1="134.62" x2="45.72" y2="134.62" width="0.1524" layer="91"/>
<wire x1="45.72" y1="134.62" x2="78.74" y2="134.62" width="0.1524" layer="91"/>
<wire x1="78.74" y1="134.62" x2="96.52" y2="134.62" width="0.1524" layer="91"/>
<wire x1="96.52" y1="134.62" x2="96.52" y2="127" width="0.1524" layer="91"/>
<pinref part="VGA" gate="G$1" pin="2"/>
<wire x1="96.52" y1="127" x2="106.68" y2="127" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="45.72" y1="127" x2="45.72" y2="134.62" width="0.1524" layer="91"/>
<junction x="45.72" y="134.62"/>
<pinref part="S-VIDEO" gate="G$1" pin="4"/>
<wire x1="99.06" y1="55.88" x2="78.74" y2="55.88" width="0.1524" layer="91"/>
<wire x1="78.74" y1="55.88" x2="78.74" y2="134.62" width="0.1524" layer="91"/>
<junction x="78.74" y="134.62"/>
</segment>
</net>
<net name="VGA_R0" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="R0"/>
<wire x1="-50.8" y1="129.54" x2="-22.86" y2="129.54" width="0.1524" layer="91"/>
<label x="-48.26" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RN28" gate="G$1" pin="1"/>
<wire x1="-124.46" y1="119.38" x2="-132.08" y2="119.38" width="0.1524" layer="91"/>
<label x="-134.62" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_R1" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="R1"/>
<wire x1="-50.8" y1="127" x2="-22.86" y2="127" width="0.1524" layer="91"/>
<label x="-48.26" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RN28" gate="G$1" pin="2"/>
<wire x1="-124.46" y1="116.84" x2="-132.08" y2="116.84" width="0.1524" layer="91"/>
<label x="-134.62" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_R2" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="R2"/>
<wire x1="-50.8" y1="124.46" x2="-22.86" y2="124.46" width="0.1524" layer="91"/>
<label x="-48.26" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_R3" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="R3"/>
<wire x1="-50.8" y1="121.92" x2="-22.86" y2="121.92" width="0.1524" layer="91"/>
<label x="-48.26" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RN28" gate="G$1" pin="4"/>
<wire x1="-132.08" y1="111.76" x2="-124.46" y2="111.76" width="0.1524" layer="91"/>
<label x="-134.62" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_R4" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="R4"/>
<wire x1="-50.8" y1="119.38" x2="-22.86" y2="119.38" width="0.1524" layer="91"/>
<label x="-48.26" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RN28" gate="G$1" pin="3"/>
<wire x1="-124.46" y1="114.3" x2="-132.08" y2="114.3" width="0.1524" layer="91"/>
<label x="-134.62" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_R5" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="R5"/>
<wire x1="-50.8" y1="116.84" x2="-22.86" y2="116.84" width="0.1524" layer="91"/>
<label x="-48.26" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_R6" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="R6"/>
<wire x1="-50.8" y1="114.3" x2="-22.86" y2="114.3" width="0.1524" layer="91"/>
<label x="-48.26" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_R7" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="R7"/>
<wire x1="-50.8" y1="111.76" x2="-22.86" y2="111.76" width="0.1524" layer="91"/>
<label x="-48.26" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_G0" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="G0"/>
<wire x1="-50.8" y1="106.68" x2="-22.86" y2="106.68" width="0.1524" layer="91"/>
<label x="-48.26" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_G1" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="G1"/>
<wire x1="-50.8" y1="104.14" x2="-22.86" y2="104.14" width="0.1524" layer="91"/>
<label x="-48.26" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_G2" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="G2"/>
<wire x1="-50.8" y1="101.6" x2="-22.86" y2="101.6" width="0.1524" layer="91"/>
<label x="-48.26" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_G3" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="G3"/>
<wire x1="-50.8" y1="99.06" x2="-22.86" y2="99.06" width="0.1524" layer="91"/>
<label x="-48.26" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_G4" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="G4"/>
<wire x1="-50.8" y1="96.52" x2="-22.86" y2="96.52" width="0.1524" layer="91"/>
<label x="-48.26" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_G5" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="G5"/>
<wire x1="-50.8" y1="93.98" x2="-22.86" y2="93.98" width="0.1524" layer="91"/>
<label x="-48.26" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_G6" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="G6"/>
<wire x1="-50.8" y1="91.44" x2="-22.86" y2="91.44" width="0.1524" layer="91"/>
<label x="-48.26" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_G7" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="G7"/>
<wire x1="-50.8" y1="88.9" x2="-22.86" y2="88.9" width="0.1524" layer="91"/>
<label x="-48.26" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_B0" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="B0"/>
<wire x1="-50.8" y1="83.82" x2="-22.86" y2="83.82" width="0.1524" layer="91"/>
<label x="-48.26" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_B1" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="B1"/>
<wire x1="-50.8" y1="81.28" x2="-22.86" y2="81.28" width="0.1524" layer="91"/>
<label x="-48.26" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RN29" gate="G$1" pin="2"/>
<wire x1="-132.08" y1="106.68" x2="-124.46" y2="106.68" width="0.1524" layer="91"/>
<label x="-134.62" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_B2" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="B2"/>
<wire x1="-50.8" y1="78.74" x2="-22.86" y2="78.74" width="0.1524" layer="91"/>
<label x="-48.26" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_B3" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="B3"/>
<wire x1="-50.8" y1="76.2" x2="-22.86" y2="76.2" width="0.1524" layer="91"/>
<label x="-48.26" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_B4" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="B4"/>
<wire x1="-50.8" y1="73.66" x2="-22.86" y2="73.66" width="0.1524" layer="91"/>
<label x="-48.26" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RN29" gate="G$1" pin="1"/>
<wire x1="-132.08" y1="109.22" x2="-124.46" y2="109.22" width="0.1524" layer="91"/>
<label x="-134.62" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_B5" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="B5"/>
<wire x1="-50.8" y1="71.12" x2="-22.86" y2="71.12" width="0.1524" layer="91"/>
<label x="-48.26" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RN29" gate="G$1" pin="4"/>
<wire x1="-132.08" y1="101.6" x2="-124.46" y2="101.6" width="0.1524" layer="91"/>
<label x="-134.62" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_B6" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="B6"/>
<wire x1="-50.8" y1="68.58" x2="-22.86" y2="68.58" width="0.1524" layer="91"/>
<label x="-48.26" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RN29" gate="G$1" pin="3"/>
<wire x1="-132.08" y1="104.14" x2="-124.46" y2="104.14" width="0.1524" layer="91"/>
<label x="-134.62" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_B7" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="B7"/>
<wire x1="-50.8" y1="66.04" x2="-22.86" y2="66.04" width="0.1524" layer="91"/>
<label x="-48.26" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_BLANK_N" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="~BLANK"/>
<wire x1="-50.8" y1="60.96" x2="-22.86" y2="60.96" width="0.1524" layer="91"/>
<label x="-48.26" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="VGA_CLK" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="CLOCK"/>
<wire x1="-50.8" y1="53.34" x2="-22.86" y2="53.34" width="0.1524" layer="91"/>
<label x="-48.26" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="VREF"/>
<wire x1="-22.86" y1="50.8" x2="-43.18" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="50.8" x2="-43.18" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="RSET"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="-22.86" y1="45.72" x2="-33.02" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="COMP"/>
<wire x1="-22.86" y1="43.18" x2="-27.94" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="43.18" x2="-27.94" y2="132.08" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="132.08" x2="-40.64" y2="132.08" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="-40.64" y1="132.08" x2="-40.64" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="VIDEODACSA" gate="A" pin="VAA"/>
<wire x1="-22.86" y1="134.62" x2="-25.4" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="134.62" x2="-25.4" y2="137.16" width="0.1524" layer="91"/>
<pinref part="VIDEODACSA" gate="A" pin="VAA_3"/>
<wire x1="-25.4" y1="137.16" x2="-22.86" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="137.16" x2="-25.4" y2="139.7" width="0.1524" layer="91"/>
<junction x="-25.4" y="137.16"/>
<pinref part="VIDEODACSA" gate="A" pin="VAA_2"/>
<wire x1="-25.4" y1="139.7" x2="-22.86" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="139.7" x2="-25.4" y2="144.78" width="0.1524" layer="91"/>
<junction x="-25.4" y="139.7"/>
<wire x1="-25.4" y1="144.78" x2="-40.64" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="-40.64" y1="144.78" x2="-40.64" y2="142.24" width="0.1524" layer="91"/>
<label x="-30.48" y="144.78" size="1.778" layer="95"/>
<pinref part="VIDEODACSA" gate="A" pin="~PSAVE"/>
<wire x1="-22.86" y1="58.42" x2="-55.88" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="58.42" x2="-55.88" y2="144.78" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="144.78" x2="-40.64" y2="144.78" width="0.1524" layer="91"/>
<junction x="-40.64" y="144.78"/>
</segment>
</net>
<net name="VSYNC" class="0">
<segment>
<wire x1="154.94" y1="7.62" x2="-73.66" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="7.62" x2="-73.66" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="45.72" x2="-50.8" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="154.94" y1="73.66" x2="154.94" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HSYNC" class="0">
<segment>
<wire x1="170.18" y1="5.08" x2="-81.28" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="5.08" x2="-81.28" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="48.26" x2="-50.8" y2="48.26" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="170.18" y1="5.08" x2="170.18" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="VGA" gate="G$1" pin="14"/>
<wire x1="122.174" y1="116.84" x2="154.94" y2="116.84" width="0.1524" layer="91"/>
<label x="152.4" y="116.84" size="1.778" layer="95"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="154.94" y1="83.82" x2="154.94" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="VGA" gate="G$1" pin="13"/>
<wire x1="122.174" y1="121.92" x2="170.18" y2="121.92" width="0.1524" layer="91"/>
<label x="152.4" y="121.92" size="1.778" layer="95"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="170.18" y1="83.82" x2="170.18" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$143" class="0">
<segment>
<pinref part="C-VIDEO" gate="B" pin="1"/>
<pinref part="VIDEODACSA" gate="A" pin="IOB"/>
<wire x1="12.7" y1="132.08" x2="38.1" y2="132.08" width="0.1524" layer="91"/>
<wire x1="38.1" y1="132.08" x2="76.2" y2="132.08" width="0.1524" layer="91"/>
<wire x1="76.2" y1="132.08" x2="93.98" y2="132.08" width="0.1524" layer="91"/>
<wire x1="93.98" y1="132.08" x2="93.98" y2="121.92" width="0.1524" layer="91"/>
<pinref part="VGA" gate="G$1" pin="3"/>
<wire x1="93.98" y1="121.92" x2="106.68" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="38.1" y1="127" x2="38.1" y2="132.08" width="0.1524" layer="91"/>
<junction x="38.1" y="132.08"/>
<wire x1="66.04" y1="58.42" x2="76.2" y2="58.42" width="0.1524" layer="91"/>
<wire x1="76.2" y1="58.42" x2="76.2" y2="132.08" width="0.1524" layer="91"/>
<junction x="76.2" y="132.08"/>
</segment>
</net>
<net name="5V0" class="0">
<segment>
<pinref part="HDMI" gate="G$1" pin="+5V"/>
<wire x1="-71.12" y1="91.44" x2="-66.04" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="91.44" x2="-66.04" y2="83.82" width="0.1524" layer="91"/>
<label x="-73.66" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$134" class="0">
<segment>
<pinref part="RN28" gate="G$1" pin="8"/>
<pinref part="HDMI" gate="G$1" pin="TX2_P"/>
<wire x1="-109.22" y1="119.38" x2="-106.68" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$144" class="0">
<segment>
<pinref part="HDMI" gate="G$1" pin="TX2_N"/>
<pinref part="RN28" gate="G$1" pin="7"/>
<wire x1="-106.68" y1="116.84" x2="-109.22" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$146" class="0">
<segment>
<pinref part="RN28" gate="G$1" pin="6"/>
<pinref part="HDMI" gate="G$1" pin="TX1_P"/>
<wire x1="-109.22" y1="114.3" x2="-106.68" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$147" class="0">
<segment>
<pinref part="HDMI" gate="G$1" pin="TX1_N"/>
<pinref part="RN28" gate="G$1" pin="5"/>
<wire x1="-106.68" y1="111.76" x2="-109.22" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$148" class="0">
<segment>
<pinref part="RN29" gate="G$1" pin="8"/>
<pinref part="HDMI" gate="G$1" pin="TX0_P"/>
<wire x1="-109.22" y1="109.22" x2="-106.68" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$149" class="0">
<segment>
<pinref part="HDMI" gate="G$1" pin="TX0_N"/>
<pinref part="RN29" gate="G$1" pin="7"/>
<wire x1="-106.68" y1="106.68" x2="-109.22" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$150" class="0">
<segment>
<pinref part="RN29" gate="G$1" pin="6"/>
<pinref part="HDMI" gate="G$1" pin="CLK_P"/>
<wire x1="-109.22" y1="104.14" x2="-106.68" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$151" class="0">
<segment>
<pinref part="HDMI" gate="G$1" pin="CLK_N"/>
<pinref part="RN29" gate="G$1" pin="5"/>
<wire x1="-106.68" y1="101.6" x2="-109.22" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Audio</description>
<plain>
<text x="-27.94" y="68.58" size="1.778" layer="91">Filter for delta sigma audio dac</text>
</plain>
<instances>
<instance part="GND12" gate="1" x="-20.32" y="88.9" rot="R270"/>
<instance part="C19" gate="G$1" x="7.62" y="93.98"/>
<instance part="C20" gate="G$1" x="7.62" y="83.82"/>
<instance part="R28" gate="G$1" x="17.78" y="76.2"/>
<instance part="R29" gate="G$1" x="17.78" y="99.06"/>
<instance part="R33" gate="G$1" x="0" y="99.06"/>
<instance part="R34" gate="G$1" x="0" y="76.2"/>
<instance part="C33" gate="G$1" x="-10.16" y="93.98"/>
<instance part="C34" gate="G$1" x="-10.16" y="83.82"/>
<instance part="R35" gate="G$1" x="12.7" y="88.9" rot="R90"/>
<instance part="C35" gate="G$1" x="-20.32" y="99.06" rot="R90"/>
<instance part="C36" gate="G$1" x="-20.32" y="76.2" rot="R90"/>
<instance part="AUDIO" gate="G$1" x="-33.02" y="88.9"/>
</instances>
<busses>
</busses>
<nets>
<net name="AUDIO_RIGHT" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="22.86" y1="99.06" x2="33.02" y2="99.06" width="0.1524" layer="91"/>
<label x="27.94" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="AUDIO_LEFT" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="22.86" y1="76.2" x2="35.56" y2="76.2" width="0.1524" layer="91"/>
<label x="27.94" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C20" gate="G$1" pin="1"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="7.62" y1="86.36" x2="7.62" y2="88.9" width="0.1524" layer="91"/>
<pinref part="GND12" gate="1" pin="GND"/>
<junction x="7.62" y="88.9"/>
<wire x1="7.62" y1="88.9" x2="-10.16" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="88.9" x2="-15.24" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="88.9" x2="-17.78" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="91.44" x2="-15.24" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="91.44" x2="-15.24" y2="88.9" width="0.1524" layer="91"/>
<junction x="-15.24" y="88.9"/>
<pinref part="C33" gate="G$1" pin="2"/>
<junction x="-10.16" y="88.9"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="88.9" x2="-10.16" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="91.44" x2="-20.32" y2="93.98" width="0.1524" layer="91"/>
<pinref part="AUDIO" gate="G$1" pin="SLEEVE"/>
<wire x1="-20.32" y1="93.98" x2="-27.94" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="12.7" y1="99.06" x2="7.62" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="7.62" y1="99.06" x2="7.62" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="5.08" y1="99.06" x2="7.62" y2="99.06" width="0.1524" layer="91"/>
<junction x="7.62" y="99.06"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="12.7" y1="93.98" x2="12.7" y2="99.06" width="0.1524" layer="91"/>
<junction x="12.7" y="99.06"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="12.7" y1="76.2" x2="7.62" y2="76.2" width="0.1524" layer="91"/>
<wire x1="7.62" y1="76.2" x2="7.62" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="5.08" y1="76.2" x2="7.62" y2="76.2" width="0.1524" layer="91"/>
<junction x="7.62" y="76.2"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="12.7" y1="83.82" x2="12.7" y2="76.2" width="0.1524" layer="91"/>
<junction x="12.7" y="76.2"/>
</segment>
</net>
<net name="N$138" class="0">
<segment>
<wire x1="-25.4" y1="91.44" x2="-25.4" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="99.06" x2="-22.86" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="1"/>
<pinref part="AUDIO" gate="G$1" pin="RING"/>
<wire x1="-25.4" y1="91.44" x2="-27.94" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$141" class="0">
<segment>
<wire x1="-25.4" y1="83.82" x2="-25.4" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="76.2" x2="-22.86" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="1"/>
<pinref part="AUDIO" gate="G$1" pin="TIP"/>
<wire x1="-25.4" y1="83.82" x2="-27.94" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="99.06" x2="-10.16" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="99.06" x2="-10.16" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="99.06" x2="-10.16" y2="99.06" width="0.1524" layer="91"/>
<junction x="-10.16" y="99.06"/>
<pinref part="C35" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$142" class="0">
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="78.74" x2="-10.16" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="76.2" x2="-5.08" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="76.2" x2="-10.16" y2="76.2" width="0.1524" layer="91"/>
<junction x="-10.16" y="76.2"/>
<pinref part="C36" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>GPIO</description>
<plain>
<text x="-40.64" y="73.66" size="1.778" layer="91">Resistors!!</text>
</plain>
<instances>
<instance part="GPIO_A" gate="G$1" x="-30.48" y="7.62"/>
<instance part="GPIO_B" gate="G$1" x="45.72" y="7.62"/>
<instance part="GPIO_C" gate="G$1" x="121.92" y="7.62"/>
<instance part="GND13" gate="1" x="106.68" y="68.58"/>
<instance part="RN15" gate="G$1" x="144.78" y="-30.48" rot="MR0"/>
<instance part="RN9" gate="G$1" x="144.78" y="50.8" rot="MR0"/>
<instance part="RN10" gate="G$1" x="144.78" y="40.64" rot="MR0"/>
<instance part="RN11" gate="G$1" x="162.56" y="30.48" rot="MR0"/>
<instance part="RN12" gate="G$1" x="162.56" y="20.32" rot="MR0"/>
<instance part="RN16" gate="G$1" x="144.78" y="10.16" rot="MR0"/>
<instance part="RN17" gate="G$1" x="144.78" y="0" rot="MR0"/>
<instance part="RN18" gate="G$1" x="162.56" y="-10.16" rot="MR0"/>
<instance part="RN19" gate="G$1" x="162.56" y="-20.32" rot="MR0"/>
<instance part="U$3" gate="G$1" x="180.34" y="15.24"/>
<instance part="RN5" gate="G$1" x="71.12" y="50.8" rot="MR0"/>
<instance part="RN6" gate="G$1" x="71.12" y="40.64" rot="MR0"/>
<instance part="RN7" gate="G$1" x="86.36" y="30.48" rot="MR0"/>
<instance part="RN8" gate="G$1" x="86.36" y="20.32" rot="MR0"/>
<instance part="RN14" gate="G$1" x="71.12" y="10.16" rot="MR0"/>
<instance part="RN20" gate="G$1" x="71.12" y="0" rot="MR0"/>
<instance part="RN21" gate="G$1" x="86.36" y="-10.16" rot="MR0"/>
<instance part="RN22" gate="G$1" x="86.36" y="-20.32" rot="MR0"/>
<instance part="RN23" gate="G$1" x="71.12" y="-30.48" rot="MR0"/>
<instance part="U$4" gate="G$1" x="104.14" y="15.24"/>
<instance part="RN1" gate="G$1" x="-5.08" y="50.8" rot="MR0"/>
<instance part="RN2" gate="G$1" x="-5.08" y="40.64" rot="MR0"/>
<instance part="RN3" gate="G$1" x="12.7" y="30.48" rot="MR0"/>
<instance part="RN4" gate="G$1" x="12.7" y="20.32" rot="MR0"/>
<instance part="RN13" gate="G$1" x="-5.08" y="10.16" rot="MR0"/>
<instance part="RN24" gate="G$1" x="-5.08" y="0" rot="MR0"/>
<instance part="RN25" gate="G$1" x="12.7" y="-10.16" rot="MR0"/>
<instance part="RN26" gate="G$1" x="12.7" y="-20.32" rot="MR0"/>
<instance part="RN27" gate="G$1" x="-5.08" y="-30.48" rot="MR0"/>
<instance part="U$5" gate="G$1" x="27.94" y="15.24"/>
</instances>
<busses>
<bus name="GPIOA[0..35]">
<segment>
<wire x1="30.48" y1="53.34" x2="30.48" y2="-38.1" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="GPIOB[0..35]">
<segment>
<wire x1="109.22" y1="-38.1" x2="109.22" y2="55.88" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="GPIOC[0..35]">
<segment>
<wire x1="175.26" y1="58.42" x2="175.26" y2="-38.1" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="5V0" class="0">
<segment>
<wire x1="-20.32" y1="76.2" x2="-10.16" y2="76.2" width="0.1524" layer="91"/>
<label x="-17.78" y="76.2" size="1.778" layer="95"/>
<pinref part="GPIO_A" gate="G$1" pin="5V0"/>
<wire x1="-10.16" y1="76.2" x2="66.04" y2="76.2" width="0.1524" layer="91"/>
<wire x1="66.04" y1="76.2" x2="137.16" y2="76.2" width="0.1524" layer="91"/>
<wire x1="137.16" y1="76.2" x2="149.86" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="66.04" x2="-10.16" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="66.04" x2="-10.16" y2="76.2" width="0.1524" layer="91"/>
<junction x="-10.16" y="76.2"/>
<pinref part="GPIO_B" gate="G$1" pin="5V0"/>
<wire x1="55.88" y1="66.04" x2="66.04" y2="66.04" width="0.1524" layer="91"/>
<wire x1="66.04" y1="66.04" x2="66.04" y2="76.2" width="0.1524" layer="91"/>
<junction x="66.04" y="76.2"/>
<pinref part="GPIO_C" gate="G$1" pin="5V0"/>
<wire x1="132.08" y1="66.04" x2="137.16" y2="66.04" width="0.1524" layer="91"/>
<wire x1="137.16" y1="66.04" x2="137.16" y2="76.2" width="0.1524" layer="91"/>
<junction x="137.16" y="76.2"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="-20.32" y1="71.12" x2="-5.08" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="-5.08" y1="71.12" x2="71.12" y2="71.12" width="0.1524" layer="91"/>
<wire x1="71.12" y1="71.12" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<wire x1="106.68" y1="71.12" x2="142.24" y2="71.12" width="0.1524" layer="91"/>
<junction x="106.68" y="71.12"/>
<label x="-17.78" y="71.12" size="1.778" layer="95"/>
<pinref part="GPIO_A" gate="G$1" pin="GND@1"/>
<wire x1="142.24" y1="71.12" x2="149.86" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="60.96" x2="-17.78" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="60.96" x2="-5.08" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="60.96" x2="-5.08" y2="71.12" width="0.1524" layer="91"/>
<junction x="-5.08" y="71.12"/>
<pinref part="GPIO_A" gate="G$1" pin="GND@2"/>
<wire x1="-20.32" y1="58.42" x2="-17.78" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="58.42" x2="-17.78" y2="60.96" width="0.1524" layer="91"/>
<junction x="-17.78" y="60.96"/>
<pinref part="GPIO_B" gate="G$1" pin="GND@1"/>
<wire x1="55.88" y1="60.96" x2="58.42" y2="60.96" width="0.1524" layer="91"/>
<wire x1="58.42" y1="60.96" x2="71.12" y2="60.96" width="0.1524" layer="91"/>
<wire x1="71.12" y1="60.96" x2="71.12" y2="71.12" width="0.1524" layer="91"/>
<junction x="71.12" y="71.12"/>
<pinref part="GPIO_B" gate="G$1" pin="GND@2"/>
<wire x1="55.88" y1="58.42" x2="58.42" y2="58.42" width="0.1524" layer="91"/>
<wire x1="58.42" y1="58.42" x2="58.42" y2="60.96" width="0.1524" layer="91"/>
<junction x="58.42" y="60.96"/>
<pinref part="GPIO_C" gate="G$1" pin="GND@1"/>
<wire x1="132.08" y1="60.96" x2="134.62" y2="60.96" width="0.1524" layer="91"/>
<wire x1="134.62" y1="60.96" x2="142.24" y2="60.96" width="0.1524" layer="91"/>
<wire x1="142.24" y1="60.96" x2="142.24" y2="71.12" width="0.1524" layer="91"/>
<junction x="142.24" y="71.12"/>
<pinref part="GPIO_C" gate="G$1" pin="GND@2"/>
<wire x1="132.08" y1="58.42" x2="134.62" y2="58.42" width="0.1524" layer="91"/>
<wire x1="134.62" y1="58.42" x2="134.62" y2="60.96" width="0.1524" layer="91"/>
<junction x="134.62" y="60.96"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<wire x1="-20.32" y1="73.66" x2="-7.62" y2="73.66" width="0.1524" layer="91"/>
<label x="-17.78" y="73.66" size="1.778" layer="95"/>
<pinref part="GPIO_A" gate="G$1" pin="3V3"/>
<wire x1="-7.62" y1="73.66" x2="68.58" y2="73.66" width="0.1524" layer="91"/>
<wire x1="68.58" y1="73.66" x2="139.7" y2="73.66" width="0.1524" layer="91"/>
<wire x1="139.7" y1="73.66" x2="149.86" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="63.5" x2="-7.62" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="63.5" x2="-7.62" y2="73.66" width="0.1524" layer="91"/>
<junction x="-7.62" y="73.66"/>
<pinref part="GPIO_B" gate="G$1" pin="3V3"/>
<wire x1="55.88" y1="63.5" x2="68.58" y2="63.5" width="0.1524" layer="91"/>
<wire x1="68.58" y1="63.5" x2="68.58" y2="73.66" width="0.1524" layer="91"/>
<junction x="68.58" y="73.66"/>
<pinref part="GPIO_C" gate="G$1" pin="3V3"/>
<wire x1="132.08" y1="63.5" x2="139.7" y2="63.5" width="0.1524" layer="91"/>
<wire x1="139.7" y1="63.5" x2="139.7" y2="73.66" width="0.1524" layer="91"/>
<junction x="139.7" y="73.66"/>
</segment>
</net>
<net name="GPIOC0" class="0">
<segment>
<wire x1="175.26" y1="53.34" x2="172.72" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_0"/>
<pinref part="RN9" gate="G$1" pin="1"/>
<wire x1="152.4" y1="53.34" x2="172.72" y2="53.34" width="0.1524" layer="91"/>
<junction x="172.72" y="53.34"/>
</segment>
</net>
<net name="GPIOC1" class="0">
<segment>
<wire x1="175.26" y1="50.8" x2="172.72" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_1"/>
<pinref part="RN9" gate="G$1" pin="2"/>
<wire x1="152.4" y1="50.8" x2="172.72" y2="50.8" width="0.1524" layer="91"/>
<junction x="172.72" y="50.8"/>
</segment>
</net>
<net name="GPIOC2" class="0">
<segment>
<wire x1="175.26" y1="48.26" x2="172.72" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_2"/>
<pinref part="RN9" gate="G$1" pin="3"/>
<wire x1="172.72" y1="48.26" x2="152.4" y2="48.26" width="0.1524" layer="91"/>
<junction x="172.72" y="48.26"/>
</segment>
</net>
<net name="GPIOC3" class="0">
<segment>
<wire x1="175.26" y1="45.72" x2="172.72" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_3"/>
<pinref part="RN9" gate="G$1" pin="4"/>
<wire x1="152.4" y1="45.72" x2="172.72" y2="45.72" width="0.1524" layer="91"/>
<junction x="172.72" y="45.72"/>
</segment>
</net>
<net name="GPIOC4" class="0">
<segment>
<wire x1="175.26" y1="43.18" x2="172.72" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_4"/>
<pinref part="RN10" gate="G$1" pin="1"/>
<wire x1="172.72" y1="43.18" x2="152.4" y2="43.18" width="0.1524" layer="91"/>
<junction x="172.72" y="43.18"/>
</segment>
</net>
<net name="GPIOC5" class="0">
<segment>
<wire x1="175.26" y1="40.64" x2="172.72" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_5"/>
<pinref part="RN10" gate="G$1" pin="2"/>
<wire x1="152.4" y1="40.64" x2="172.72" y2="40.64" width="0.1524" layer="91"/>
<junction x="172.72" y="40.64"/>
</segment>
</net>
<net name="GPIOC6" class="0">
<segment>
<wire x1="175.26" y1="38.1" x2="172.72" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_6"/>
<pinref part="RN10" gate="G$1" pin="3"/>
<wire x1="172.72" y1="38.1" x2="152.4" y2="38.1" width="0.1524" layer="91"/>
<junction x="172.72" y="38.1"/>
</segment>
</net>
<net name="GPIOC7" class="0">
<segment>
<wire x1="175.26" y1="35.56" x2="172.72" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_7"/>
<pinref part="RN10" gate="G$1" pin="4"/>
<wire x1="152.4" y1="35.56" x2="172.72" y2="35.56" width="0.1524" layer="91"/>
<junction x="172.72" y="35.56"/>
</segment>
</net>
<net name="GPIOC8" class="0">
<segment>
<wire x1="175.26" y1="33.02" x2="172.72" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_8"/>
<pinref part="RN11" gate="G$1" pin="1"/>
<wire x1="170.18" y1="33.02" x2="172.72" y2="33.02" width="0.1524" layer="91"/>
<junction x="172.72" y="33.02"/>
</segment>
</net>
<net name="GPIOC9" class="0">
<segment>
<wire x1="175.26" y1="30.48" x2="172.72" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_9"/>
<pinref part="RN11" gate="G$1" pin="2"/>
<wire x1="170.18" y1="30.48" x2="172.72" y2="30.48" width="0.1524" layer="91"/>
<junction x="172.72" y="30.48"/>
</segment>
</net>
<net name="GPIOC10" class="0">
<segment>
<wire x1="175.26" y1="27.94" x2="172.72" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_10"/>
<pinref part="RN11" gate="G$1" pin="3"/>
<wire x1="170.18" y1="27.94" x2="172.72" y2="27.94" width="0.1524" layer="91"/>
<junction x="172.72" y="27.94"/>
</segment>
</net>
<net name="GPIOC11" class="0">
<segment>
<wire x1="175.26" y1="25.4" x2="172.72" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_11"/>
<pinref part="RN11" gate="G$1" pin="4"/>
<wire x1="170.18" y1="25.4" x2="172.72" y2="25.4" width="0.1524" layer="91"/>
<junction x="172.72" y="25.4"/>
</segment>
</net>
<net name="GPIOC12" class="0">
<segment>
<wire x1="175.26" y1="22.86" x2="172.72" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_12"/>
<pinref part="RN12" gate="G$1" pin="1"/>
<wire x1="170.18" y1="22.86" x2="172.72" y2="22.86" width="0.1524" layer="91"/>
<junction x="172.72" y="22.86"/>
</segment>
</net>
<net name="GPIOC13" class="0">
<segment>
<wire x1="175.26" y1="20.32" x2="172.72" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_13"/>
<pinref part="RN12" gate="G$1" pin="2"/>
<wire x1="170.18" y1="20.32" x2="172.72" y2="20.32" width="0.1524" layer="91"/>
<junction x="172.72" y="20.32"/>
</segment>
</net>
<net name="GPIOC14" class="0">
<segment>
<wire x1="175.26" y1="17.78" x2="172.72" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_14"/>
<pinref part="RN12" gate="G$1" pin="3"/>
<wire x1="170.18" y1="17.78" x2="172.72" y2="17.78" width="0.1524" layer="91"/>
<junction x="172.72" y="17.78"/>
</segment>
</net>
<net name="GPIOC15" class="0">
<segment>
<wire x1="175.26" y1="15.24" x2="172.72" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_15"/>
<pinref part="RN12" gate="G$1" pin="4"/>
<wire x1="170.18" y1="15.24" x2="172.72" y2="15.24" width="0.1524" layer="91"/>
<junction x="172.72" y="15.24"/>
</segment>
</net>
<net name="GPIOC16" class="0">
<segment>
<wire x1="175.26" y1="12.7" x2="172.72" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_16"/>
<pinref part="RN16" gate="G$1" pin="1"/>
<wire x1="152.4" y1="12.7" x2="172.72" y2="12.7" width="0.1524" layer="91"/>
<junction x="172.72" y="12.7"/>
</segment>
</net>
<net name="GPIOC17" class="0">
<segment>
<wire x1="175.26" y1="10.16" x2="172.72" y2="10.16" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_17"/>
<pinref part="RN16" gate="G$1" pin="2"/>
<wire x1="152.4" y1="10.16" x2="172.72" y2="10.16" width="0.1524" layer="91"/>
<junction x="172.72" y="10.16"/>
</segment>
</net>
<net name="GPIOC18" class="0">
<segment>
<wire x1="175.26" y1="7.62" x2="172.72" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_18"/>
<pinref part="RN16" gate="G$1" pin="3"/>
<wire x1="152.4" y1="7.62" x2="172.72" y2="7.62" width="0.1524" layer="91"/>
<junction x="172.72" y="7.62"/>
</segment>
</net>
<net name="GPIOC19" class="0">
<segment>
<wire x1="175.26" y1="5.08" x2="172.72" y2="5.08" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_19"/>
<pinref part="RN16" gate="G$1" pin="4"/>
<wire x1="152.4" y1="5.08" x2="172.72" y2="5.08" width="0.1524" layer="91"/>
<junction x="172.72" y="5.08"/>
</segment>
</net>
<net name="GPIOC20" class="0">
<segment>
<wire x1="175.26" y1="2.54" x2="172.72" y2="2.54" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_20"/>
<pinref part="RN17" gate="G$1" pin="1"/>
<wire x1="152.4" y1="2.54" x2="172.72" y2="2.54" width="0.1524" layer="91"/>
<junction x="172.72" y="2.54"/>
</segment>
</net>
<net name="GPIOC21" class="0">
<segment>
<wire x1="175.26" y1="0" x2="172.72" y2="0" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_21"/>
<pinref part="RN17" gate="G$1" pin="2"/>
<wire x1="152.4" y1="0" x2="172.72" y2="0" width="0.1524" layer="91"/>
<junction x="172.72" y="0"/>
</segment>
</net>
<net name="GPIOC22" class="0">
<segment>
<wire x1="175.26" y1="-2.54" x2="172.72" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_22"/>
<pinref part="RN17" gate="G$1" pin="3"/>
<wire x1="152.4" y1="-2.54" x2="172.72" y2="-2.54" width="0.1524" layer="91"/>
<junction x="172.72" y="-2.54"/>
</segment>
</net>
<net name="GPIOC23" class="0">
<segment>
<wire x1="175.26" y1="-5.08" x2="172.72" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_23"/>
<pinref part="RN17" gate="G$1" pin="4"/>
<wire x1="152.4" y1="-5.08" x2="172.72" y2="-5.08" width="0.1524" layer="91"/>
<junction x="172.72" y="-5.08"/>
</segment>
</net>
<net name="GPIOC24" class="0">
<segment>
<wire x1="175.26" y1="-7.62" x2="172.72" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_24"/>
<pinref part="RN18" gate="G$1" pin="1"/>
<wire x1="170.18" y1="-7.62" x2="172.72" y2="-7.62" width="0.1524" layer="91"/>
<junction x="172.72" y="-7.62"/>
</segment>
</net>
<net name="GPIOC25" class="0">
<segment>
<wire x1="175.26" y1="-10.16" x2="172.72" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_25"/>
<pinref part="RN18" gate="G$1" pin="2"/>
<wire x1="170.18" y1="-10.16" x2="172.72" y2="-10.16" width="0.1524" layer="91"/>
<junction x="172.72" y="-10.16"/>
</segment>
</net>
<net name="GPIOC26" class="0">
<segment>
<wire x1="175.26" y1="-12.7" x2="172.72" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_26"/>
<pinref part="RN18" gate="G$1" pin="3"/>
<wire x1="170.18" y1="-12.7" x2="172.72" y2="-12.7" width="0.1524" layer="91"/>
<junction x="172.72" y="-12.7"/>
</segment>
</net>
<net name="GPIOC27" class="0">
<segment>
<wire x1="175.26" y1="-15.24" x2="172.72" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_27"/>
<pinref part="RN18" gate="G$1" pin="4"/>
<wire x1="170.18" y1="-15.24" x2="172.72" y2="-15.24" width="0.1524" layer="91"/>
<junction x="172.72" y="-15.24"/>
</segment>
</net>
<net name="GPIOC30" class="0">
<segment>
<wire x1="175.26" y1="-22.86" x2="172.72" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_30"/>
<pinref part="RN19" gate="G$1" pin="3"/>
<wire x1="170.18" y1="-22.86" x2="172.72" y2="-22.86" width="0.1524" layer="91"/>
<junction x="172.72" y="-22.86"/>
</segment>
</net>
<net name="GPIOC31" class="0">
<segment>
<wire x1="175.26" y1="-25.4" x2="172.72" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_31"/>
<pinref part="RN19" gate="G$1" pin="4"/>
<wire x1="172.72" y1="-25.4" x2="170.18" y2="-25.4" width="0.1524" layer="91"/>
<junction x="172.72" y="-25.4"/>
</segment>
</net>
<net name="GPIOC32" class="0">
<segment>
<wire x1="175.26" y1="-27.94" x2="172.72" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_32"/>
<pinref part="RN15" gate="G$1" pin="1"/>
<wire x1="152.4" y1="-27.94" x2="172.72" y2="-27.94" width="0.1524" layer="91"/>
<junction x="172.72" y="-27.94"/>
</segment>
</net>
<net name="GPIOC34" class="0">
<segment>
<wire x1="175.26" y1="-33.02" x2="172.72" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_34"/>
<pinref part="RN15" gate="G$1" pin="3"/>
<wire x1="152.4" y1="-33.02" x2="172.72" y2="-33.02" width="0.1524" layer="91"/>
<junction x="172.72" y="-33.02"/>
</segment>
</net>
<net name="GPIOC35" class="0">
<segment>
<wire x1="175.26" y1="-35.56" x2="172.72" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GPIO_35"/>
<pinref part="RN15" gate="G$1" pin="4"/>
<wire x1="152.4" y1="-35.56" x2="172.72" y2="-35.56" width="0.1524" layer="91"/>
<junction x="172.72" y="-35.56"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_0"/>
<wire x1="-20.32" y1="53.34" x2="-12.7" y2="53.34" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="8"/>
</segment>
</net>
<net name="GPIOA0" class="0">
<segment>
<wire x1="30.48" y1="53.34" x2="20.32" y2="53.34" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="GPIO_0"/>
<wire x1="20.32" y1="53.34" x2="2.54" y2="53.34" width="0.1524" layer="91"/>
<junction x="20.32" y="53.34"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_1"/>
<wire x1="-20.32" y1="50.8" x2="-12.7" y2="50.8" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_2"/>
<wire x1="-12.7" y1="48.26" x2="-20.32" y2="48.26" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_3"/>
<wire x1="-20.32" y1="45.72" x2="-12.7" y2="45.72" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_4"/>
<wire x1="-12.7" y1="43.18" x2="-20.32" y2="43.18" width="0.1524" layer="91"/>
<pinref part="RN2" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_5"/>
<wire x1="-20.32" y1="40.64" x2="-12.7" y2="40.64" width="0.1524" layer="91"/>
<pinref part="RN2" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_6"/>
<wire x1="-12.7" y1="38.1" x2="-20.32" y2="38.1" width="0.1524" layer="91"/>
<pinref part="RN2" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_7"/>
<wire x1="-20.32" y1="35.56" x2="-12.7" y2="35.56" width="0.1524" layer="91"/>
<pinref part="RN2" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_8"/>
<wire x1="-20.32" y1="33.02" x2="5.08" y2="33.02" width="0.1524" layer="91"/>
<pinref part="RN3" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_9"/>
<wire x1="5.08" y1="30.48" x2="-20.32" y2="30.48" width="0.1524" layer="91"/>
<pinref part="RN3" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_10"/>
<wire x1="-20.32" y1="27.94" x2="5.08" y2="27.94" width="0.1524" layer="91"/>
<pinref part="RN3" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_11"/>
<wire x1="5.08" y1="25.4" x2="-20.32" y2="25.4" width="0.1524" layer="91"/>
<pinref part="RN3" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_13"/>
<wire x1="5.08" y1="20.32" x2="-20.32" y2="20.32" width="0.1524" layer="91"/>
<pinref part="RN4" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_14"/>
<wire x1="5.08" y1="17.78" x2="-20.32" y2="17.78" width="0.1524" layer="91"/>
<pinref part="RN4" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_15"/>
<wire x1="-20.32" y1="15.24" x2="5.08" y2="15.24" width="0.1524" layer="91"/>
<pinref part="RN4" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_16"/>
<wire x1="-12.7" y1="12.7" x2="-20.32" y2="12.7" width="0.1524" layer="91"/>
<pinref part="RN13" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_17"/>
<wire x1="-20.32" y1="10.16" x2="-12.7" y2="10.16" width="0.1524" layer="91"/>
<pinref part="RN13" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_18"/>
<wire x1="-12.7" y1="7.62" x2="-20.32" y2="7.62" width="0.1524" layer="91"/>
<pinref part="RN13" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_19"/>
<wire x1="-20.32" y1="5.08" x2="-12.7" y2="5.08" width="0.1524" layer="91"/>
<pinref part="RN13" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_20"/>
<wire x1="-12.7" y1="2.54" x2="-20.32" y2="2.54" width="0.1524" layer="91"/>
<pinref part="RN24" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_22"/>
<wire x1="-20.32" y1="-2.54" x2="-12.7" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="RN24" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_21"/>
<wire x1="-12.7" y1="0" x2="-20.32" y2="0" width="0.1524" layer="91"/>
<pinref part="RN24" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_23"/>
<wire x1="-20.32" y1="-5.08" x2="-12.7" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="RN24" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_24"/>
<wire x1="-20.32" y1="-7.62" x2="5.08" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="RN25" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_25"/>
<wire x1="5.08" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="RN25" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_26"/>
<wire x1="-20.32" y1="-12.7" x2="5.08" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="RN25" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_27"/>
<wire x1="5.08" y1="-15.24" x2="-20.32" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="RN25" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_28"/>
<wire x1="-20.32" y1="-17.78" x2="5.08" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="RN26" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_29"/>
<wire x1="5.08" y1="-20.32" x2="-20.32" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="RN26" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_30"/>
<wire x1="-20.32" y1="-22.86" x2="5.08" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="RN26" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_31"/>
<wire x1="5.08" y1="-25.4" x2="-20.32" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="RN26" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_32"/>
<wire x1="-20.32" y1="-27.94" x2="-12.7" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="RN27" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_33"/>
<wire x1="-12.7" y1="-30.48" x2="-20.32" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="RN27" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_34"/>
<wire x1="-20.32" y1="-33.02" x2="-12.7" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="RN27" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_35"/>
<wire x1="-12.7" y1="-35.56" x2="-20.32" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="RN27" gate="G$1" pin="5"/>
</segment>
</net>
<net name="GPIOA1" class="0">
<segment>
<wire x1="30.48" y1="50.8" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="GPIO_1"/>
<wire x1="20.32" y1="50.8" x2="2.54" y2="50.8" width="0.1524" layer="91"/>
<junction x="20.32" y="50.8"/>
</segment>
</net>
<net name="GPIOA2" class="0">
<segment>
<wire x1="30.48" y1="48.26" x2="20.32" y2="48.26" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="3"/>
<pinref part="U$5" gate="G$1" pin="GPIO_2"/>
<wire x1="20.32" y1="48.26" x2="2.54" y2="48.26" width="0.1524" layer="91"/>
<junction x="20.32" y="48.26"/>
</segment>
</net>
<net name="GPIOA3" class="0">
<segment>
<wire x1="30.48" y1="45.72" x2="20.32" y2="45.72" width="0.1524" layer="91"/>
<pinref part="RN1" gate="G$1" pin="4"/>
<pinref part="U$5" gate="G$1" pin="GPIO_3"/>
<wire x1="20.32" y1="45.72" x2="2.54" y2="45.72" width="0.1524" layer="91"/>
<junction x="20.32" y="45.72"/>
</segment>
</net>
<net name="GPIOA4" class="0">
<segment>
<wire x1="30.48" y1="43.18" x2="20.32" y2="43.18" width="0.1524" layer="91"/>
<pinref part="RN2" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="GPIO_4"/>
<wire x1="20.32" y1="43.18" x2="2.54" y2="43.18" width="0.1524" layer="91"/>
<junction x="20.32" y="43.18"/>
</segment>
</net>
<net name="GPIOA5" class="0">
<segment>
<wire x1="30.48" y1="40.64" x2="20.32" y2="40.64" width="0.1524" layer="91"/>
<pinref part="RN2" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="GPIO_5"/>
<wire x1="20.32" y1="40.64" x2="2.54" y2="40.64" width="0.1524" layer="91"/>
<junction x="20.32" y="40.64"/>
</segment>
</net>
<net name="GPIOA6" class="0">
<segment>
<wire x1="30.48" y1="38.1" x2="20.32" y2="38.1" width="0.1524" layer="91"/>
<pinref part="RN2" gate="G$1" pin="3"/>
<pinref part="U$5" gate="G$1" pin="GPIO_6"/>
<wire x1="20.32" y1="38.1" x2="2.54" y2="38.1" width="0.1524" layer="91"/>
<junction x="20.32" y="38.1"/>
</segment>
</net>
<net name="GPIOA7" class="0">
<segment>
<wire x1="30.48" y1="35.56" x2="20.32" y2="35.56" width="0.1524" layer="91"/>
<pinref part="RN2" gate="G$1" pin="4"/>
<pinref part="U$5" gate="G$1" pin="GPIO_7"/>
<wire x1="20.32" y1="35.56" x2="2.54" y2="35.56" width="0.1524" layer="91"/>
<junction x="20.32" y="35.56"/>
</segment>
</net>
<net name="GPIOA8" class="0">
<segment>
<wire x1="30.48" y1="33.02" x2="20.32" y2="33.02" width="0.1524" layer="91"/>
<pinref part="RN3" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="GPIO_8"/>
<junction x="20.32" y="33.02"/>
</segment>
</net>
<net name="GPIOA9" class="0">
<segment>
<wire x1="30.48" y1="30.48" x2="20.32" y2="30.48" width="0.1524" layer="91"/>
<pinref part="RN3" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="GPIO_9"/>
<junction x="20.32" y="30.48"/>
</segment>
</net>
<net name="GPIOA10" class="0">
<segment>
<wire x1="30.48" y1="27.94" x2="20.32" y2="27.94" width="0.1524" layer="91"/>
<pinref part="RN3" gate="G$1" pin="3"/>
<pinref part="U$5" gate="G$1" pin="GPIO_10"/>
<junction x="20.32" y="27.94"/>
</segment>
</net>
<net name="GPIOA11" class="0">
<segment>
<wire x1="30.48" y1="25.4" x2="20.32" y2="25.4" width="0.1524" layer="91"/>
<pinref part="RN3" gate="G$1" pin="4"/>
<pinref part="U$5" gate="G$1" pin="GPIO_11"/>
<junction x="20.32" y="25.4"/>
</segment>
</net>
<net name="GPIOA12" class="0">
<segment>
<wire x1="30.48" y1="22.86" x2="20.32" y2="22.86" width="0.1524" layer="91"/>
<pinref part="RN4" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="GPIO_12"/>
<junction x="20.32" y="22.86"/>
</segment>
</net>
<net name="GPIOA13" class="0">
<segment>
<wire x1="30.48" y1="20.32" x2="20.32" y2="20.32" width="0.1524" layer="91"/>
<pinref part="RN4" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="GPIO_13"/>
<junction x="20.32" y="20.32"/>
</segment>
</net>
<net name="GPIOA14" class="0">
<segment>
<wire x1="30.48" y1="17.78" x2="20.32" y2="17.78" width="0.1524" layer="91"/>
<pinref part="RN4" gate="G$1" pin="3"/>
<pinref part="U$5" gate="G$1" pin="GPIO_14"/>
<junction x="20.32" y="17.78"/>
</segment>
</net>
<net name="GPIOA15" class="0">
<segment>
<wire x1="30.48" y1="15.24" x2="20.32" y2="15.24" width="0.1524" layer="91"/>
<pinref part="RN4" gate="G$1" pin="4"/>
<pinref part="U$5" gate="G$1" pin="GPIO_15"/>
<junction x="20.32" y="15.24"/>
</segment>
</net>
<net name="GPIOA16" class="0">
<segment>
<wire x1="30.48" y1="12.7" x2="20.32" y2="12.7" width="0.1524" layer="91"/>
<pinref part="RN13" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="GPIO_16"/>
<wire x1="20.32" y1="12.7" x2="2.54" y2="12.7" width="0.1524" layer="91"/>
<junction x="20.32" y="12.7"/>
</segment>
</net>
<net name="GPIOA17" class="0">
<segment>
<wire x1="30.48" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="91"/>
<pinref part="RN13" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="GPIO_17"/>
<wire x1="20.32" y1="10.16" x2="2.54" y2="10.16" width="0.1524" layer="91"/>
<junction x="20.32" y="10.16"/>
</segment>
</net>
<net name="GPIOA18" class="0">
<segment>
<wire x1="30.48" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="91"/>
<pinref part="RN13" gate="G$1" pin="3"/>
<pinref part="U$5" gate="G$1" pin="GPIO_18"/>
<wire x1="20.32" y1="7.62" x2="2.54" y2="7.62" width="0.1524" layer="91"/>
<junction x="20.32" y="7.62"/>
</segment>
</net>
<net name="GPIOA19" class="0">
<segment>
<wire x1="30.48" y1="5.08" x2="20.32" y2="5.08" width="0.1524" layer="91"/>
<pinref part="RN13" gate="G$1" pin="4"/>
<pinref part="U$5" gate="G$1" pin="GPIO_19"/>
<wire x1="20.32" y1="5.08" x2="2.54" y2="5.08" width="0.1524" layer="91"/>
<junction x="20.32" y="5.08"/>
</segment>
</net>
<net name="GPIOA20" class="0">
<segment>
<wire x1="30.48" y1="2.54" x2="20.32" y2="2.54" width="0.1524" layer="91"/>
<pinref part="RN24" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="GPIO_20"/>
<wire x1="20.32" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="91"/>
<junction x="20.32" y="2.54"/>
</segment>
</net>
<net name="GPIOA21" class="0">
<segment>
<wire x1="30.48" y1="0" x2="20.32" y2="0" width="0.1524" layer="91"/>
<pinref part="RN24" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="GPIO_21"/>
<wire x1="20.32" y1="0" x2="2.54" y2="0" width="0.1524" layer="91"/>
<junction x="20.32" y="0"/>
</segment>
</net>
<net name="GPIOA22" class="0">
<segment>
<wire x1="30.48" y1="-2.54" x2="20.32" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="RN24" gate="G$1" pin="3"/>
<pinref part="U$5" gate="G$1" pin="GPIO_22"/>
<wire x1="20.32" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="91"/>
<junction x="20.32" y="-2.54"/>
</segment>
</net>
<net name="GPIOA23" class="0">
<segment>
<wire x1="30.48" y1="-5.08" x2="20.32" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="RN24" gate="G$1" pin="4"/>
<pinref part="U$5" gate="G$1" pin="GPIO_23"/>
<wire x1="20.32" y1="-5.08" x2="2.54" y2="-5.08" width="0.1524" layer="91"/>
<junction x="20.32" y="-5.08"/>
</segment>
</net>
<net name="GPIOA24" class="0">
<segment>
<wire x1="30.48" y1="-7.62" x2="20.32" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="RN25" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="GPIO_24"/>
<junction x="20.32" y="-7.62"/>
</segment>
</net>
<net name="GPIOA25" class="0">
<segment>
<wire x1="30.48" y1="-10.16" x2="20.32" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="RN25" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="GPIO_25"/>
<junction x="20.32" y="-10.16"/>
</segment>
</net>
<net name="GPIOA26" class="0">
<segment>
<wire x1="30.48" y1="-12.7" x2="20.32" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="RN25" gate="G$1" pin="3"/>
<pinref part="U$5" gate="G$1" pin="GPIO_26"/>
<junction x="20.32" y="-12.7"/>
</segment>
</net>
<net name="GPIOA27" class="0">
<segment>
<wire x1="30.48" y1="-15.24" x2="20.32" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="RN25" gate="G$1" pin="4"/>
<pinref part="U$5" gate="G$1" pin="GPIO_27"/>
<junction x="20.32" y="-15.24"/>
</segment>
</net>
<net name="GPIOA28" class="0">
<segment>
<wire x1="30.48" y1="-17.78" x2="20.32" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="RN26" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="GPIO_28"/>
<junction x="20.32" y="-17.78"/>
</segment>
</net>
<net name="GPIOA29" class="0">
<segment>
<wire x1="30.48" y1="-20.32" x2="20.32" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="RN26" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="GPIO_29"/>
<junction x="20.32" y="-20.32"/>
</segment>
</net>
<net name="GPIOA30" class="0">
<segment>
<wire x1="30.48" y1="-22.86" x2="20.32" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="RN26" gate="G$1" pin="3"/>
<pinref part="U$5" gate="G$1" pin="GPIO_30"/>
<junction x="20.32" y="-22.86"/>
</segment>
</net>
<net name="GPIOA31" class="0">
<segment>
<wire x1="30.48" y1="-25.4" x2="20.32" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="RN26" gate="G$1" pin="4"/>
<pinref part="U$5" gate="G$1" pin="GPIO_31"/>
<junction x="20.32" y="-25.4"/>
</segment>
</net>
<net name="GPIOA32" class="0">
<segment>
<wire x1="30.48" y1="-27.94" x2="20.32" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="RN27" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="GPIO_32"/>
<wire x1="20.32" y1="-27.94" x2="2.54" y2="-27.94" width="0.1524" layer="91"/>
<junction x="20.32" y="-27.94"/>
</segment>
</net>
<net name="GPIOA33" class="0">
<segment>
<wire x1="30.48" y1="-30.48" x2="20.32" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="RN27" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="GPIO_33"/>
<wire x1="20.32" y1="-30.48" x2="2.54" y2="-30.48" width="0.1524" layer="91"/>
<junction x="20.32" y="-30.48"/>
</segment>
</net>
<net name="GPIOA34" class="0">
<segment>
<wire x1="30.48" y1="-33.02" x2="20.32" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="RN27" gate="G$1" pin="3"/>
<pinref part="U$5" gate="G$1" pin="GPIO_34"/>
<wire x1="20.32" y1="-33.02" x2="2.54" y2="-33.02" width="0.1524" layer="91"/>
<junction x="20.32" y="-33.02"/>
</segment>
</net>
<net name="GPIOA35" class="0">
<segment>
<wire x1="30.48" y1="-35.56" x2="20.32" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="RN27" gate="G$1" pin="4"/>
<pinref part="U$5" gate="G$1" pin="GPIO_35"/>
<wire x1="20.32" y1="-35.56" x2="2.54" y2="-35.56" width="0.1524" layer="91"/>
<junction x="20.32" y="-35.56"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_0"/>
<wire x1="55.88" y1="53.34" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
<pinref part="RN5" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_1"/>
<wire x1="55.88" y1="50.8" x2="63.5" y2="50.8" width="0.1524" layer="91"/>
<pinref part="RN5" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_2"/>
<wire x1="63.5" y1="48.26" x2="55.88" y2="48.26" width="0.1524" layer="91"/>
<pinref part="RN5" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_3"/>
<wire x1="55.88" y1="45.72" x2="63.5" y2="45.72" width="0.1524" layer="91"/>
<pinref part="RN5" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_4"/>
<wire x1="63.5" y1="43.18" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
<pinref part="RN6" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_5"/>
<wire x1="55.88" y1="40.64" x2="63.5" y2="40.64" width="0.1524" layer="91"/>
<pinref part="RN6" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_6"/>
<wire x1="63.5" y1="38.1" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
<pinref part="RN6" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_7"/>
<wire x1="55.88" y1="35.56" x2="63.5" y2="35.56" width="0.1524" layer="91"/>
<pinref part="RN6" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_8"/>
<wire x1="55.88" y1="33.02" x2="78.74" y2="33.02" width="0.1524" layer="91"/>
<pinref part="RN7" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_9"/>
<wire x1="78.74" y1="30.48" x2="55.88" y2="30.48" width="0.1524" layer="91"/>
<pinref part="RN7" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_10"/>
<wire x1="55.88" y1="27.94" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
<pinref part="RN7" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_11"/>
<wire x1="78.74" y1="25.4" x2="55.88" y2="25.4" width="0.1524" layer="91"/>
<pinref part="RN7" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_12"/>
<wire x1="55.88" y1="22.86" x2="78.74" y2="22.86" width="0.1524" layer="91"/>
<pinref part="RN8" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_13"/>
<wire x1="78.74" y1="20.32" x2="55.88" y2="20.32" width="0.1524" layer="91"/>
<pinref part="RN8" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_14"/>
<wire x1="55.88" y1="17.78" x2="78.74" y2="17.78" width="0.1524" layer="91"/>
<pinref part="RN8" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_15"/>
<wire x1="78.74" y1="15.24" x2="55.88" y2="15.24" width="0.1524" layer="91"/>
<pinref part="RN8" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_16"/>
<wire x1="55.88" y1="12.7" x2="63.5" y2="12.7" width="0.1524" layer="91"/>
<pinref part="RN14" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_17"/>
<wire x1="63.5" y1="10.16" x2="55.88" y2="10.16" width="0.1524" layer="91"/>
<pinref part="RN14" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_18"/>
<wire x1="55.88" y1="7.62" x2="63.5" y2="7.62" width="0.1524" layer="91"/>
<pinref part="RN14" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_19"/>
<wire x1="63.5" y1="5.08" x2="55.88" y2="5.08" width="0.1524" layer="91"/>
<pinref part="RN14" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_20"/>
<wire x1="55.88" y1="2.54" x2="63.5" y2="2.54" width="0.1524" layer="91"/>
<pinref part="RN20" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_21"/>
<wire x1="63.5" y1="0" x2="55.88" y2="0" width="0.1524" layer="91"/>
<pinref part="RN20" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_22"/>
<wire x1="55.88" y1="-2.54" x2="63.5" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="RN20" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_23"/>
<wire x1="63.5" y1="-5.08" x2="55.88" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="RN20" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_24"/>
<wire x1="55.88" y1="-7.62" x2="78.74" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="RN21" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_25"/>
<wire x1="78.74" y1="-10.16" x2="55.88" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="RN21" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_26"/>
<wire x1="55.88" y1="-12.7" x2="78.74" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="RN21" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_27"/>
<wire x1="78.74" y1="-15.24" x2="55.88" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="RN21" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_28"/>
<wire x1="55.88" y1="-17.78" x2="78.74" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="RN22" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_29"/>
<wire x1="78.74" y1="-20.32" x2="55.88" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="RN22" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_30"/>
<wire x1="55.88" y1="-22.86" x2="78.74" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="RN22" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_31"/>
<wire x1="78.74" y1="-25.4" x2="55.88" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="RN22" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_32"/>
<wire x1="55.88" y1="-27.94" x2="63.5" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="RN23" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_33"/>
<wire x1="63.5" y1="-30.48" x2="55.88" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="RN23" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_34"/>
<wire x1="55.88" y1="-33.02" x2="63.5" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="RN23" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="GPIO_B" gate="G$1" pin="GPIO_35"/>
<wire x1="63.5" y1="-35.56" x2="55.88" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="RN23" gate="G$1" pin="5"/>
</segment>
</net>
<net name="GPIOB0" class="0">
<segment>
<wire x1="109.22" y1="53.34" x2="96.52" y2="53.34" width="0.1524" layer="91"/>
<pinref part="RN5" gate="G$1" pin="1"/>
<pinref part="U$4" gate="G$1" pin="GPIO_0"/>
<wire x1="96.52" y1="53.34" x2="78.74" y2="53.34" width="0.1524" layer="91"/>
<junction x="96.52" y="53.34"/>
</segment>
</net>
<net name="GPIOB1" class="0">
<segment>
<wire x1="109.22" y1="50.8" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<pinref part="RN5" gate="G$1" pin="2"/>
<pinref part="U$4" gate="G$1" pin="GPIO_1"/>
<wire x1="96.52" y1="50.8" x2="78.74" y2="50.8" width="0.1524" layer="91"/>
<junction x="96.52" y="50.8"/>
</segment>
</net>
<net name="GPIOB2" class="0">
<segment>
<wire x1="109.22" y1="48.26" x2="96.52" y2="48.26" width="0.1524" layer="91"/>
<pinref part="RN5" gate="G$1" pin="3"/>
<pinref part="U$4" gate="G$1" pin="GPIO_2"/>
<wire x1="96.52" y1="48.26" x2="78.74" y2="48.26" width="0.1524" layer="91"/>
<junction x="96.52" y="48.26"/>
</segment>
</net>
<net name="GPIOB3" class="0">
<segment>
<wire x1="109.22" y1="45.72" x2="96.52" y2="45.72" width="0.1524" layer="91"/>
<pinref part="RN5" gate="G$1" pin="4"/>
<pinref part="U$4" gate="G$1" pin="GPIO_3"/>
<wire x1="96.52" y1="45.72" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
<junction x="96.52" y="45.72"/>
</segment>
</net>
<net name="GPIOB4" class="0">
<segment>
<wire x1="109.22" y1="43.18" x2="96.52" y2="43.18" width="0.1524" layer="91"/>
<pinref part="RN6" gate="G$1" pin="1"/>
<pinref part="U$4" gate="G$1" pin="GPIO_4"/>
<wire x1="96.52" y1="43.18" x2="78.74" y2="43.18" width="0.1524" layer="91"/>
<junction x="96.52" y="43.18"/>
</segment>
</net>
<net name="GPIOB5" class="0">
<segment>
<wire x1="109.22" y1="40.64" x2="96.52" y2="40.64" width="0.1524" layer="91"/>
<pinref part="RN6" gate="G$1" pin="2"/>
<pinref part="U$4" gate="G$1" pin="GPIO_5"/>
<wire x1="96.52" y1="40.64" x2="78.74" y2="40.64" width="0.1524" layer="91"/>
<junction x="96.52" y="40.64"/>
</segment>
</net>
<net name="GPIOB6" class="0">
<segment>
<wire x1="109.22" y1="38.1" x2="96.52" y2="38.1" width="0.1524" layer="91"/>
<pinref part="RN6" gate="G$1" pin="3"/>
<pinref part="U$4" gate="G$1" pin="GPIO_6"/>
<wire x1="96.52" y1="38.1" x2="78.74" y2="38.1" width="0.1524" layer="91"/>
<junction x="96.52" y="38.1"/>
</segment>
</net>
<net name="GPIOB7" class="0">
<segment>
<wire x1="109.22" y1="35.56" x2="96.52" y2="35.56" width="0.1524" layer="91"/>
<pinref part="RN6" gate="G$1" pin="4"/>
<pinref part="U$4" gate="G$1" pin="GPIO_7"/>
<wire x1="96.52" y1="35.56" x2="78.74" y2="35.56" width="0.1524" layer="91"/>
<junction x="96.52" y="35.56"/>
</segment>
</net>
<net name="GPIOB8" class="0">
<segment>
<wire x1="109.22" y1="33.02" x2="96.52" y2="33.02" width="0.1524" layer="91"/>
<pinref part="RN7" gate="G$1" pin="1"/>
<pinref part="U$4" gate="G$1" pin="GPIO_8"/>
<wire x1="96.52" y1="33.02" x2="93.98" y2="33.02" width="0.1524" layer="91"/>
<junction x="96.52" y="33.02"/>
</segment>
</net>
<net name="GPIOB9" class="0">
<segment>
<wire x1="109.22" y1="30.48" x2="96.52" y2="30.48" width="0.1524" layer="91"/>
<pinref part="RN7" gate="G$1" pin="2"/>
<pinref part="U$4" gate="G$1" pin="GPIO_9"/>
<wire x1="96.52" y1="30.48" x2="93.98" y2="30.48" width="0.1524" layer="91"/>
<junction x="96.52" y="30.48"/>
</segment>
</net>
<net name="GPIOB10" class="0">
<segment>
<wire x1="109.22" y1="27.94" x2="96.52" y2="27.94" width="0.1524" layer="91"/>
<pinref part="RN7" gate="G$1" pin="3"/>
<pinref part="U$4" gate="G$1" pin="GPIO_10"/>
<wire x1="96.52" y1="27.94" x2="93.98" y2="27.94" width="0.1524" layer="91"/>
<junction x="96.52" y="27.94"/>
</segment>
</net>
<net name="GPIOB11" class="0">
<segment>
<wire x1="109.22" y1="25.4" x2="96.52" y2="25.4" width="0.1524" layer="91"/>
<pinref part="RN7" gate="G$1" pin="4"/>
<pinref part="U$4" gate="G$1" pin="GPIO_11"/>
<wire x1="96.52" y1="25.4" x2="93.98" y2="25.4" width="0.1524" layer="91"/>
<junction x="96.52" y="25.4"/>
</segment>
</net>
<net name="GPIOB12" class="0">
<segment>
<wire x1="109.22" y1="22.86" x2="96.52" y2="22.86" width="0.1524" layer="91"/>
<pinref part="RN8" gate="G$1" pin="1"/>
<pinref part="U$4" gate="G$1" pin="GPIO_12"/>
<wire x1="96.52" y1="22.86" x2="93.98" y2="22.86" width="0.1524" layer="91"/>
<junction x="96.52" y="22.86"/>
</segment>
</net>
<net name="GPIOB13" class="0">
<segment>
<wire x1="109.22" y1="20.32" x2="96.52" y2="20.32" width="0.1524" layer="91"/>
<pinref part="RN8" gate="G$1" pin="2"/>
<pinref part="U$4" gate="G$1" pin="GPIO_13"/>
<wire x1="96.52" y1="20.32" x2="93.98" y2="20.32" width="0.1524" layer="91"/>
<junction x="96.52" y="20.32"/>
</segment>
</net>
<net name="GPIOB14" class="0">
<segment>
<wire x1="109.22" y1="17.78" x2="96.52" y2="17.78" width="0.1524" layer="91"/>
<pinref part="RN8" gate="G$1" pin="3"/>
<pinref part="U$4" gate="G$1" pin="GPIO_14"/>
<wire x1="96.52" y1="17.78" x2="93.98" y2="17.78" width="0.1524" layer="91"/>
<junction x="96.52" y="17.78"/>
</segment>
</net>
<net name="GPIOB15" class="0">
<segment>
<wire x1="109.22" y1="15.24" x2="96.52" y2="15.24" width="0.1524" layer="91"/>
<pinref part="RN8" gate="G$1" pin="4"/>
<pinref part="U$4" gate="G$1" pin="GPIO_15"/>
<wire x1="96.52" y1="15.24" x2="93.98" y2="15.24" width="0.1524" layer="91"/>
<junction x="96.52" y="15.24"/>
</segment>
</net>
<net name="GPIOB16" class="0">
<segment>
<wire x1="109.22" y1="12.7" x2="96.52" y2="12.7" width="0.1524" layer="91"/>
<pinref part="RN14" gate="G$1" pin="1"/>
<pinref part="U$4" gate="G$1" pin="GPIO_16"/>
<wire x1="96.52" y1="12.7" x2="78.74" y2="12.7" width="0.1524" layer="91"/>
<junction x="96.52" y="12.7"/>
</segment>
</net>
<net name="GPIOB17" class="0">
<segment>
<wire x1="109.22" y1="10.16" x2="96.52" y2="10.16" width="0.1524" layer="91"/>
<pinref part="RN14" gate="G$1" pin="2"/>
<pinref part="U$4" gate="G$1" pin="GPIO_17"/>
<wire x1="96.52" y1="10.16" x2="78.74" y2="10.16" width="0.1524" layer="91"/>
<junction x="96.52" y="10.16"/>
</segment>
</net>
<net name="GPIOB18" class="0">
<segment>
<wire x1="109.22" y1="7.62" x2="96.52" y2="7.62" width="0.1524" layer="91"/>
<pinref part="RN14" gate="G$1" pin="3"/>
<pinref part="U$4" gate="G$1" pin="GPIO_18"/>
<wire x1="96.52" y1="7.62" x2="78.74" y2="7.62" width="0.1524" layer="91"/>
<junction x="96.52" y="7.62"/>
</segment>
</net>
<net name="GPIOB19" class="0">
<segment>
<wire x1="109.22" y1="5.08" x2="96.52" y2="5.08" width="0.1524" layer="91"/>
<pinref part="RN14" gate="G$1" pin="4"/>
<pinref part="U$4" gate="G$1" pin="GPIO_19"/>
<wire x1="96.52" y1="5.08" x2="78.74" y2="5.08" width="0.1524" layer="91"/>
<junction x="96.52" y="5.08"/>
</segment>
</net>
<net name="GPIOB20" class="0">
<segment>
<wire x1="109.22" y1="2.54" x2="96.52" y2="2.54" width="0.1524" layer="91"/>
<pinref part="RN20" gate="G$1" pin="1"/>
<pinref part="U$4" gate="G$1" pin="GPIO_20"/>
<wire x1="96.52" y1="2.54" x2="78.74" y2="2.54" width="0.1524" layer="91"/>
<junction x="96.52" y="2.54"/>
</segment>
</net>
<net name="GPIOB21" class="0">
<segment>
<wire x1="109.22" y1="0" x2="96.52" y2="0" width="0.1524" layer="91"/>
<pinref part="RN20" gate="G$1" pin="2"/>
<pinref part="U$4" gate="G$1" pin="GPIO_21"/>
<wire x1="96.52" y1="0" x2="78.74" y2="0" width="0.1524" layer="91"/>
<junction x="96.52" y="0"/>
</segment>
</net>
<net name="GPIOB22" class="0">
<segment>
<wire x1="109.22" y1="-2.54" x2="96.52" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="RN20" gate="G$1" pin="3"/>
<pinref part="U$4" gate="G$1" pin="GPIO_22"/>
<wire x1="96.52" y1="-2.54" x2="78.74" y2="-2.54" width="0.1524" layer="91"/>
<junction x="96.52" y="-2.54"/>
</segment>
</net>
<net name="GPIOB23" class="0">
<segment>
<wire x1="109.22" y1="-5.08" x2="96.52" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="RN20" gate="G$1" pin="4"/>
<pinref part="U$4" gate="G$1" pin="GPIO_23"/>
<wire x1="96.52" y1="-5.08" x2="78.74" y2="-5.08" width="0.1524" layer="91"/>
<junction x="96.52" y="-5.08"/>
</segment>
</net>
<net name="GPIOB24" class="0">
<segment>
<wire x1="109.22" y1="-7.62" x2="96.52" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="RN21" gate="G$1" pin="1"/>
<pinref part="U$4" gate="G$1" pin="GPIO_24"/>
<wire x1="96.52" y1="-7.62" x2="93.98" y2="-7.62" width="0.1524" layer="91"/>
<junction x="96.52" y="-7.62"/>
</segment>
</net>
<net name="GPIOB25" class="0">
<segment>
<wire x1="109.22" y1="-10.16" x2="96.52" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="RN21" gate="G$1" pin="2"/>
<pinref part="U$4" gate="G$1" pin="GPIO_25"/>
<wire x1="96.52" y1="-10.16" x2="93.98" y2="-10.16" width="0.1524" layer="91"/>
<junction x="96.52" y="-10.16"/>
</segment>
</net>
<net name="GPIOB26" class="0">
<segment>
<wire x1="109.22" y1="-12.7" x2="96.52" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="RN21" gate="G$1" pin="3"/>
<pinref part="U$4" gate="G$1" pin="GPIO_26"/>
<wire x1="96.52" y1="-12.7" x2="93.98" y2="-12.7" width="0.1524" layer="91"/>
<junction x="96.52" y="-12.7"/>
</segment>
</net>
<net name="GPIOB27" class="0">
<segment>
<wire x1="109.22" y1="-15.24" x2="96.52" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="RN21" gate="G$1" pin="4"/>
<pinref part="U$4" gate="G$1" pin="GPIO_27"/>
<wire x1="96.52" y1="-15.24" x2="93.98" y2="-15.24" width="0.1524" layer="91"/>
<junction x="96.52" y="-15.24"/>
</segment>
</net>
<net name="GPIOB28" class="0">
<segment>
<wire x1="109.22" y1="-17.78" x2="96.52" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="RN22" gate="G$1" pin="1"/>
<pinref part="U$4" gate="G$1" pin="GPIO_28"/>
<wire x1="96.52" y1="-17.78" x2="93.98" y2="-17.78" width="0.1524" layer="91"/>
<junction x="96.52" y="-17.78"/>
</segment>
</net>
<net name="GPIOB29" class="0">
<segment>
<wire x1="109.22" y1="-20.32" x2="96.52" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="RN22" gate="G$1" pin="2"/>
<pinref part="U$4" gate="G$1" pin="GPIO_29"/>
<wire x1="96.52" y1="-20.32" x2="93.98" y2="-20.32" width="0.1524" layer="91"/>
<junction x="96.52" y="-20.32"/>
</segment>
</net>
<net name="GPIOB30" class="0">
<segment>
<wire x1="109.22" y1="-22.86" x2="96.52" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="RN22" gate="G$1" pin="3"/>
<pinref part="U$4" gate="G$1" pin="GPIO_30"/>
<wire x1="96.52" y1="-22.86" x2="93.98" y2="-22.86" width="0.1524" layer="91"/>
<junction x="96.52" y="-22.86"/>
</segment>
</net>
<net name="GPIOB31" class="0">
<segment>
<wire x1="109.22" y1="-25.4" x2="96.52" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="RN22" gate="G$1" pin="4"/>
<pinref part="U$4" gate="G$1" pin="GPIO_31"/>
<wire x1="96.52" y1="-25.4" x2="93.98" y2="-25.4" width="0.1524" layer="91"/>
<junction x="96.52" y="-25.4"/>
</segment>
</net>
<net name="GPIOB32" class="0">
<segment>
<wire x1="109.22" y1="-27.94" x2="96.52" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="RN23" gate="G$1" pin="1"/>
<pinref part="U$4" gate="G$1" pin="GPIO_32"/>
<wire x1="96.52" y1="-27.94" x2="78.74" y2="-27.94" width="0.1524" layer="91"/>
<junction x="96.52" y="-27.94"/>
</segment>
</net>
<net name="GPIOB33" class="0">
<segment>
<wire x1="109.22" y1="-30.48" x2="96.52" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="RN23" gate="G$1" pin="2"/>
<pinref part="U$4" gate="G$1" pin="GPIO_33"/>
<wire x1="96.52" y1="-30.48" x2="78.74" y2="-30.48" width="0.1524" layer="91"/>
<junction x="96.52" y="-30.48"/>
</segment>
</net>
<net name="GPIOB34" class="0">
<segment>
<wire x1="109.22" y1="-33.02" x2="96.52" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="RN23" gate="G$1" pin="3"/>
<pinref part="U$4" gate="G$1" pin="GPIO_34"/>
<wire x1="96.52" y1="-33.02" x2="78.74" y2="-33.02" width="0.1524" layer="91"/>
<junction x="96.52" y="-33.02"/>
</segment>
</net>
<net name="GPIOB35" class="0">
<segment>
<wire x1="109.22" y1="-35.56" x2="96.52" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="RN23" gate="G$1" pin="4"/>
<pinref part="U$4" gate="G$1" pin="GPIO_35"/>
<wire x1="96.52" y1="-35.56" x2="78.74" y2="-35.56" width="0.1524" layer="91"/>
<junction x="96.52" y="-35.56"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="GPIO_A" gate="G$1" pin="GPIO_12"/>
<wire x1="-20.32" y1="22.86" x2="5.08" y2="22.86" width="0.1524" layer="91"/>
<pinref part="RN4" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_0"/>
<pinref part="RN9" gate="G$1" pin="8"/>
<wire x1="132.08" y1="53.34" x2="137.16" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_1"/>
<pinref part="RN9" gate="G$1" pin="7"/>
<wire x1="132.08" y1="50.8" x2="137.16" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="RN9" gate="G$1" pin="6"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_2"/>
<wire x1="137.16" y1="48.26" x2="132.08" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_3"/>
<pinref part="RN9" gate="G$1" pin="5"/>
<wire x1="132.08" y1="45.72" x2="137.16" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="RN10" gate="G$1" pin="8"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_4"/>
<wire x1="137.16" y1="43.18" x2="132.08" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_5"/>
<pinref part="RN10" gate="G$1" pin="7"/>
<wire x1="132.08" y1="40.64" x2="137.16" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_6"/>
<pinref part="RN10" gate="G$1" pin="6"/>
<wire x1="132.08" y1="38.1" x2="137.16" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="RN10" gate="G$1" pin="5"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_7"/>
<wire x1="137.16" y1="35.56" x2="132.08" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_8"/>
<pinref part="RN11" gate="G$1" pin="8"/>
<wire x1="132.08" y1="33.02" x2="154.94" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="RN11" gate="G$1" pin="7"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_9"/>
<wire x1="154.94" y1="30.48" x2="132.08" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_10"/>
<pinref part="RN11" gate="G$1" pin="6"/>
<wire x1="132.08" y1="27.94" x2="154.94" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<pinref part="RN11" gate="G$1" pin="5"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_11"/>
<wire x1="154.94" y1="25.4" x2="132.08" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_12"/>
<pinref part="RN12" gate="G$1" pin="8"/>
<wire x1="132.08" y1="22.86" x2="154.94" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<pinref part="RN12" gate="G$1" pin="7"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_13"/>
<wire x1="154.94" y1="20.32" x2="132.08" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_14"/>
<pinref part="RN12" gate="G$1" pin="6"/>
<wire x1="132.08" y1="17.78" x2="154.94" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$113" class="0">
<segment>
<pinref part="RN12" gate="G$1" pin="5"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_15"/>
<wire x1="154.94" y1="15.24" x2="132.08" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$114" class="0">
<segment>
<pinref part="RN16" gate="G$1" pin="8"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_16"/>
<wire x1="137.16" y1="12.7" x2="132.08" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$115" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_17"/>
<pinref part="RN16" gate="G$1" pin="7"/>
<wire x1="132.08" y1="10.16" x2="137.16" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$116" class="0">
<segment>
<pinref part="RN16" gate="G$1" pin="6"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_18"/>
<wire x1="137.16" y1="7.62" x2="132.08" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_19"/>
<pinref part="RN16" gate="G$1" pin="5"/>
<wire x1="132.08" y1="5.08" x2="137.16" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$118" class="0">
<segment>
<pinref part="RN17" gate="G$1" pin="8"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_20"/>
<wire x1="137.16" y1="2.54" x2="132.08" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$119" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_21"/>
<pinref part="RN17" gate="G$1" pin="7"/>
<wire x1="132.08" y1="0" x2="137.16" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<pinref part="RN17" gate="G$1" pin="6"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_22"/>
<wire x1="137.16" y1="-2.54" x2="132.08" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$121" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_23"/>
<pinref part="RN17" gate="G$1" pin="5"/>
<wire x1="132.08" y1="-5.08" x2="137.16" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$122" class="0">
<segment>
<pinref part="RN18" gate="G$1" pin="8"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_24"/>
<wire x1="154.94" y1="-7.62" x2="132.08" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$123" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_25"/>
<pinref part="RN18" gate="G$1" pin="7"/>
<wire x1="132.08" y1="-10.16" x2="154.94" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$124" class="0">
<segment>
<pinref part="RN18" gate="G$1" pin="6"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_26"/>
<wire x1="154.94" y1="-12.7" x2="132.08" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$125" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_27"/>
<pinref part="RN18" gate="G$1" pin="5"/>
<wire x1="132.08" y1="-15.24" x2="154.94" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$126" class="0">
<segment>
<pinref part="RN19" gate="G$1" pin="8"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_28"/>
<wire x1="154.94" y1="-17.78" x2="132.08" y2="-17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$127" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_29"/>
<pinref part="RN19" gate="G$1" pin="7"/>
<wire x1="132.08" y1="-20.32" x2="154.94" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$128" class="0">
<segment>
<pinref part="RN19" gate="G$1" pin="6"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_30"/>
<wire x1="154.94" y1="-22.86" x2="132.08" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$129" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_31"/>
<pinref part="RN19" gate="G$1" pin="5"/>
<wire x1="132.08" y1="-25.4" x2="154.94" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GPIOC33" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="GPIO_33"/>
<wire x1="175.26" y1="-30.48" x2="172.72" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="RN15" gate="G$1" pin="2"/>
<wire x1="152.4" y1="-30.48" x2="172.72" y2="-30.48" width="0.1524" layer="91"/>
<junction x="172.72" y="-30.48"/>
</segment>
</net>
<net name="GPIOC29" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="GPIO_29"/>
<wire x1="175.26" y1="-20.32" x2="172.72" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="RN19" gate="G$1" pin="2"/>
<wire x1="170.18" y1="-20.32" x2="172.72" y2="-20.32" width="0.1524" layer="91"/>
<junction x="172.72" y="-20.32"/>
</segment>
</net>
<net name="GPIOC28" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="GPIO_28"/>
<wire x1="175.26" y1="-17.78" x2="172.72" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="RN19" gate="G$1" pin="1"/>
<wire x1="170.18" y1="-17.78" x2="172.72" y2="-17.78" width="0.1524" layer="91"/>
<junction x="172.72" y="-17.78"/>
</segment>
</net>
<net name="N$130" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_32"/>
<pinref part="RN15" gate="G$1" pin="8"/>
<wire x1="132.08" y1="-27.94" x2="137.16" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$131" class="0">
<segment>
<pinref part="RN15" gate="G$1" pin="7"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_33"/>
<wire x1="137.16" y1="-30.48" x2="132.08" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$132" class="0">
<segment>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_34"/>
<pinref part="RN15" gate="G$1" pin="6"/>
<wire x1="132.08" y1="-33.02" x2="137.16" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$133" class="0">
<segment>
<pinref part="RN15" gate="G$1" pin="5"/>
<pinref part="GPIO_C" gate="G$1" pin="GPIO_35"/>
<wire x1="137.16" y1="-35.56" x2="132.08" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Memory</description>
<plain>
<text x="-17.78" y="93.98" size="1.778" layer="91">Standard 64 bit wide tssop 54 sram. One like de1 would be good.</text>
</plain>
<instances>
<instance part="SDRAM" gate="G$1" x="-38.1" y="93.98"/>
<instance part="GND14" gate="1" x="-15.24" y="45.72"/>
<instance part="C27" gate="G$1" x="2.54" y="71.12"/>
</instances>
<busses>
<bus name="SDRAM:A[0..12],DQ[0..15],CS_N,RAS_N,CAS_N,WE_N,SDRAM_CLK,CKE,SDRAM_UDQM,SDRAM_LDQM,BA[0..1]">
<segment>
<wire x1="-71.12" y1="132.08" x2="-71.12" y2="60.96" width="0.762" layer="92"/>
<wire x1="-71.12" y1="60.96" x2="-71.12" y2="38.1" width="0.762" layer="92"/>
<wire x1="-71.12" y1="38.1" x2="-5.08" y2="38.1" width="0.762" layer="92"/>
<wire x1="-5.08" y1="38.1" x2="-5.08" y2="134.62" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="A0" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A0"/>
<wire x1="-71.12" y1="132.08" x2="-53.34" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A1"/>
<wire x1="-71.12" y1="129.54" x2="-53.34" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A2"/>
<wire x1="-71.12" y1="127" x2="-53.34" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A3"/>
<wire x1="-71.12" y1="124.46" x2="-53.34" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A4"/>
<wire x1="-71.12" y1="121.92" x2="-53.34" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A5"/>
<wire x1="-71.12" y1="119.38" x2="-53.34" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A6"/>
<wire x1="-71.12" y1="116.84" x2="-53.34" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A7"/>
<wire x1="-71.12" y1="114.3" x2="-53.34" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A8" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A8"/>
<wire x1="-71.12" y1="111.76" x2="-53.34" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A9" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A9"/>
<wire x1="-71.12" y1="109.22" x2="-53.34" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A10" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A10/AP"/>
<wire x1="-71.12" y1="106.68" x2="-53.34" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A11" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A11"/>
<wire x1="-71.12" y1="104.14" x2="-53.34" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A12" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="A12"/>
<wire x1="-71.12" y1="101.6" x2="-53.34" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BA0" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="BA0"/>
<wire x1="-71.12" y1="96.52" x2="-53.34" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BA1" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="BA1"/>
<wire x1="-71.12" y1="93.98" x2="-53.34" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDRAM_UDQM" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQM"/>
<wire x1="-71.12" y1="88.9" x2="-53.34" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDRAM_LDQM" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="LDQM"/>
<wire x1="-71.12" y1="86.36" x2="-53.34" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDRAM_CLK" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="CLK"/>
<wire x1="-71.12" y1="81.28" x2="-53.34" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CKE" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="CKE"/>
<wire x1="-71.12" y1="78.74" x2="-53.34" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CS_N" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="CS#"/>
<wire x1="-71.12" y1="73.66" x2="-53.34" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RAS_N" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="RAS#"/>
<wire x1="-71.12" y1="71.12" x2="-53.34" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAS_N" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="CAS#"/>
<wire x1="-71.12" y1="68.58" x2="-53.34" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="WE_N" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="WE#"/>
<wire x1="-71.12" y1="66.04" x2="-53.34" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ0" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ0"/>
<wire x1="-5.08" y1="132.08" x2="-22.86" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ1" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ1"/>
<wire x1="-5.08" y1="129.54" x2="-22.86" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ2" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ2"/>
<wire x1="-5.08" y1="127" x2="-22.86" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ3" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ3"/>
<wire x1="-5.08" y1="124.46" x2="-22.86" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ4" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ4"/>
<wire x1="-5.08" y1="121.92" x2="-22.86" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ5" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ5"/>
<wire x1="-5.08" y1="119.38" x2="-22.86" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ6" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ6"/>
<wire x1="-5.08" y1="116.84" x2="-22.86" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ7" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ7"/>
<wire x1="-5.08" y1="114.3" x2="-22.86" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ8" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ8"/>
<wire x1="-5.08" y1="111.76" x2="-22.86" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ9" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ9"/>
<wire x1="-5.08" y1="109.22" x2="-22.86" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ10" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ10"/>
<wire x1="-5.08" y1="106.68" x2="-22.86" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ11" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ11"/>
<wire x1="-5.08" y1="104.14" x2="-22.86" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ12" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ12"/>
<wire x1="-5.08" y1="101.6" x2="-22.86" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ13" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ13"/>
<wire x1="-5.08" y1="99.06" x2="-22.86" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ14" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ14"/>
<wire x1="-5.08" y1="96.52" x2="-22.86" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DQ15" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="DQ15"/>
<wire x1="-5.08" y1="93.98" x2="-22.86" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="VDD"/>
<wire x1="-22.86" y1="88.9" x2="-15.24" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="88.9" x2="-15.24" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="86.36" x2="-15.24" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="83.82" x2="-15.24" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="81.28" x2="-15.24" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="78.74" x2="-15.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="76.2" x2="-15.24" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="73.66" x2="-7.62" y2="73.66" width="0.1524" layer="91"/>
<pinref part="SDRAM" gate="G$1" pin="VDD@14"/>
<wire x1="-15.24" y1="86.36" x2="-22.86" y2="86.36" width="0.1524" layer="91"/>
<junction x="-15.24" y="86.36"/>
<pinref part="SDRAM" gate="G$1" pin="VDD@27"/>
<wire x1="-15.24" y1="83.82" x2="-22.86" y2="83.82" width="0.1524" layer="91"/>
<junction x="-15.24" y="83.82"/>
<pinref part="SDRAM" gate="G$1" pin="VDDQ"/>
<wire x1="-15.24" y1="81.28" x2="-22.86" y2="81.28" width="0.1524" layer="91"/>
<junction x="-15.24" y="81.28"/>
<pinref part="SDRAM" gate="G$1" pin="VDDQ@9"/>
<wire x1="-22.86" y1="78.74" x2="-15.24" y2="78.74" width="0.1524" layer="91"/>
<junction x="-15.24" y="78.74"/>
<pinref part="SDRAM" gate="G$1" pin="VDDQ@43"/>
<wire x1="-22.86" y1="76.2" x2="-15.24" y2="76.2" width="0.1524" layer="91"/>
<junction x="-15.24" y="76.2"/>
<pinref part="SDRAM" gate="G$1" pin="VDDQ@49"/>
<wire x1="-22.86" y1="73.66" x2="-15.24" y2="73.66" width="0.1524" layer="91"/>
<junction x="-15.24" y="73.66"/>
<label x="-12.7" y="73.66" size="1.778" layer="95"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="73.66" x2="2.54" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="SDRAM" gate="G$1" pin="VSS"/>
<wire x1="-22.86" y1="68.58" x2="-15.24" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="68.58" x2="-15.24" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="66.04" x2="-15.24" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="63.5" x2="-15.24" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="60.96" x2="-15.24" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="58.42" x2="-15.24" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="55.88" x2="-15.24" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SDRAM" gate="G$1" pin="VSS@28"/>
<wire x1="-22.86" y1="66.04" x2="-15.24" y2="66.04" width="0.1524" layer="91"/>
<junction x="-15.24" y="66.04"/>
<pinref part="SDRAM" gate="G$1" pin="VSS@41"/>
<wire x1="-22.86" y1="63.5" x2="-15.24" y2="63.5" width="0.1524" layer="91"/>
<junction x="-15.24" y="63.5"/>
<pinref part="SDRAM" gate="G$1" pin="VSSQ"/>
<wire x1="-22.86" y1="60.96" x2="-15.24" y2="60.96" width="0.1524" layer="91"/>
<junction x="-15.24" y="60.96"/>
<pinref part="SDRAM" gate="G$1" pin="VSSQ@12"/>
<wire x1="-22.86" y1="58.42" x2="-15.24" y2="58.42" width="0.1524" layer="91"/>
<junction x="-15.24" y="58.42"/>
<pinref part="SDRAM" gate="G$1" pin="VSSQ@46"/>
<wire x1="-22.86" y1="55.88" x2="-15.24" y2="55.88" width="0.1524" layer="91"/>
<junction x="-15.24" y="55.88"/>
<pinref part="SDRAM" gate="G$1" pin="VSSQ@52"/>
<wire x1="-22.86" y1="53.34" x2="-15.24" y2="53.34" width="0.1524" layer="91"/>
<junction x="-15.24" y="53.34"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="-15.24" y1="53.34" x2="-15.24" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="2.54" y1="66.04" x2="-15.24" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Controllers</description>
<plain>
<text x="116.84" y="81.28" size="1.778" layer="95">Consider adding a polyfuse on the 5V line?</text>
<text x="20.32" y="83.82" size="1.778" layer="95">Use a zener diode for basic 5V/3.3V conversion</text>
</plain>
<instances>
<instance part="USB.A" gate="G$1" x="134.62" y="53.34"/>
<instance part="USB.B" gate="G$1" x="134.62" y="17.78"/>
<instance part="GND9" gate="1" x="119.38" y="2.54"/>
<instance part="R15" gate="G$1" x="93.98" y="63.5"/>
<instance part="R16" gate="G$1" x="93.98" y="58.42"/>
<instance part="R17" gate="G$1" x="93.98" y="27.94"/>
<instance part="R18" gate="G$1" x="93.98" y="22.86"/>
<instance part="R19" gate="G$1" x="106.68" y="58.42" rot="R270"/>
<instance part="R20" gate="G$1" x="114.3" y="53.34" rot="R270"/>
<instance part="R21" gate="G$1" x="106.68" y="22.86" rot="R270"/>
<instance part="R22" gate="G$1" x="114.3" y="17.78" rot="R270"/>
<instance part="GND11" gate="1" x="33.02" y="58.42"/>
<instance part="PS/2" gate="G$1" x="50.8" y="68.58"/>
<instance part="ZD1" gate="G$1" x="25.4" y="73.66" rot="R90"/>
<instance part="ZD2" gate="G$1" x="25.4" y="63.5" rot="R90"/>
<instance part="R26" gate="G$1" x="33.02" y="76.2"/>
<instance part="R27" gate="G$1" x="33.02" y="66.04"/>
</instances>
<busses>
<bus name="USB1DP,USB1DM,USB2DP,USB2DM">
<segment>
<wire x1="76.2" y1="63.5" x2="76.2" y2="22.86" width="0.762" layer="92"/>
<label x="73.66" y="22.86" size="1.778" layer="95" rot="R90"/>
</segment>
</bus>
<bus name="PS2DAT,PS2CLK">
<segment>
<wire x1="17.78" y1="76.2" x2="17.78" y2="63.5" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="USB.B" gate="G$1" pin="GND"/>
<wire x1="132.08" y1="17.78" x2="119.38" y2="17.78" width="0.1524" layer="91"/>
<wire x1="119.38" y1="17.78" x2="119.38" y2="45.72" width="0.1524" layer="91"/>
<pinref part="USB.A" gate="G$1" pin="GND"/>
<wire x1="119.38" y1="45.72" x2="119.38" y2="48.26" width="0.1524" layer="91"/>
<wire x1="119.38" y1="48.26" x2="119.38" y2="53.34" width="0.1524" layer="91"/>
<wire x1="119.38" y1="53.34" x2="132.08" y2="53.34" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="119.38" y1="5.08" x2="119.38" y2="12.7" width="0.1524" layer="91"/>
<junction x="119.38" y="17.78"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="119.38" y1="12.7" x2="119.38" y2="17.78" width="0.1524" layer="91"/>
<wire x1="119.38" y1="48.26" x2="114.3" y2="48.26" width="0.1524" layer="91"/>
<junction x="119.38" y="48.26"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="106.68" y1="53.34" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="106.68" y1="17.78" x2="106.68" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="106.68" y1="12.7" x2="114.3" y2="12.7" width="0.1524" layer="91"/>
<wire x1="114.3" y1="12.7" x2="119.38" y2="12.7" width="0.1524" layer="91"/>
<junction x="114.3" y="12.7"/>
<junction x="119.38" y="12.7"/>
<wire x1="114.3" y1="48.26" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
<junction x="114.3" y="48.26"/>
</segment>
<segment>
<pinref part="ZD2" gate="G$1" pin="A"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="33.02" y1="60.96" x2="25.4" y2="60.96" width="0.1524" layer="91"/>
<pinref part="PS/2" gate="G$1" pin="3"/>
<wire x1="40.64" y1="71.12" x2="33.02" y2="71.12" width="0.1524" layer="91"/>
<pinref part="ZD1" gate="G$1" pin="A"/>
<wire x1="33.02" y1="71.12" x2="25.4" y2="71.12" width="0.1524" layer="91"/>
<wire x1="33.02" y1="71.12" x2="33.02" y2="60.96" width="0.1524" layer="91"/>
<junction x="33.02" y="71.12"/>
<junction x="33.02" y="60.96"/>
</segment>
</net>
<net name="5V0" class="0">
<segment>
<wire x1="116.84" y1="71.12" x2="121.92" y2="71.12" width="0.1524" layer="91"/>
<pinref part="USB.A" gate="G$1" pin="VBUS"/>
<wire x1="121.92" y1="71.12" x2="129.54" y2="71.12" width="0.1524" layer="91"/>
<wire x1="132.08" y1="55.88" x2="121.92" y2="55.88" width="0.1524" layer="91"/>
<wire x1="121.92" y1="55.88" x2="121.92" y2="71.12" width="0.1524" layer="91"/>
<junction x="121.92" y="71.12"/>
<wire x1="121.92" y1="55.88" x2="121.92" y2="20.32" width="0.1524" layer="91"/>
<junction x="121.92" y="55.88"/>
<pinref part="USB.B" gate="G$1" pin="VBUS"/>
<wire x1="121.92" y1="20.32" x2="132.08" y2="20.32" width="0.1524" layer="91"/>
<label x="116.84" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PS/2" gate="G$1" pin="4"/>
<wire x1="40.64" y1="68.58" x2="35.56" y2="68.58" width="0.1524" layer="91"/>
<label x="35.56" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="USB.A" gate="G$1" pin="D+"/>
<wire x1="132.08" y1="60.96" x2="116.84" y2="60.96" width="0.1524" layer="91"/>
<wire x1="116.84" y1="60.96" x2="116.84" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="116.84" y1="63.5" x2="106.68" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="99.06" y1="63.5" x2="106.68" y2="63.5" width="0.1524" layer="91"/>
<junction x="106.68" y="63.5"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="99.06" y1="58.42" x2="114.3" y2="58.42" width="0.1524" layer="91"/>
<pinref part="USB.A" gate="G$1" pin="D-"/>
<wire x1="114.3" y1="58.42" x2="132.08" y2="58.42" width="0.1524" layer="91"/>
<junction x="114.3" y="58.42"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="99.06" y1="27.94" x2="106.68" y2="27.94" width="0.1524" layer="91"/>
<wire x1="106.68" y1="27.94" x2="127" y2="27.94" width="0.1524" layer="91"/>
<wire x1="127" y1="27.94" x2="127" y2="25.4" width="0.1524" layer="91"/>
<junction x="106.68" y="27.94"/>
<pinref part="USB.B" gate="G$1" pin="D+"/>
<wire x1="127" y1="25.4" x2="132.08" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="USB.B" gate="G$1" pin="D-"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="132.08" y1="22.86" x2="114.3" y2="22.86" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="114.3" y1="22.86" x2="99.06" y2="22.86" width="0.1524" layer="91"/>
<junction x="114.3" y="22.86"/>
</segment>
</net>
<net name="USB1DP" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="88.9" y1="63.5" x2="83.82" y2="63.5" width="0.1524" layer="91"/>
<wire x1="76.2" y1="63.5" x2="83.82" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB1DM" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="88.9" y1="58.42" x2="83.82" y2="58.42" width="0.1524" layer="91"/>
<wire x1="76.2" y1="58.42" x2="83.82" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB2DP" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="88.9" y1="27.94" x2="83.82" y2="27.94" width="0.1524" layer="91"/>
<wire x1="76.2" y1="27.94" x2="83.82" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB2DM" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="88.9" y1="22.86" x2="83.82" y2="22.86" width="0.1524" layer="91"/>
<wire x1="76.2" y1="22.86" x2="83.82" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PS2DAT" class="0">
<segment>
<label x="25.4" y="76.2" size="1.778" layer="95"/>
<pinref part="ZD1" gate="G$1" pin="C"/>
<wire x1="25.4" y1="76.2" x2="17.78" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="25.4" y1="76.2" x2="27.94" y2="76.2" width="0.1524" layer="91"/>
<junction x="25.4" y="76.2"/>
</segment>
</net>
<net name="PS2CLK" class="0">
<segment>
<label x="25.4" y="66.04" size="1.778" layer="95"/>
<pinref part="ZD2" gate="G$1" pin="C"/>
<wire x1="25.4" y1="66.04" x2="17.78" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="27.94" y1="66.04" x2="25.4" y2="66.04" width="0.1524" layer="91"/>
<junction x="25.4" y="66.04"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="PS/2" gate="G$1" pin="5"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="40.64" y1="66.04" x2="38.1" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="PS/2" gate="G$1" pin="1"/>
<wire x1="38.1" y1="76.2" x2="40.64" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>DAC decoupling</description>
<plain>
</plain>
<instances>
<instance part="GND8" gate="1" x="2.54" y="68.58"/>
<instance part="C15" gate="G$1" x="-12.7" y="81.28"/>
<instance part="C16" gate="G$1" x="0" y="81.28"/>
<instance part="C17" gate="G$1" x="12.7" y="81.28"/>
<instance part="C18" gate="G$1" x="27.94" y="81.28"/>
</instances>
<busses>
</busses>
<nets>
<net name="3V3" class="0">
<segment>
<wire x1="-17.78" y1="88.9" x2="-12.7" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="88.9" x2="0" y2="88.9" width="0.1524" layer="91"/>
<wire x1="0" y1="88.9" x2="12.7" y2="88.9" width="0.1524" layer="91"/>
<wire x1="12.7" y1="88.9" x2="27.94" y2="88.9" width="0.1524" layer="91"/>
<wire x1="27.94" y1="88.9" x2="45.72" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="83.82" x2="-12.7" y2="88.9" width="0.1524" layer="91"/>
<junction x="-12.7" y="88.9"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="0" y1="83.82" x2="0" y2="88.9" width="0.1524" layer="91"/>
<junction x="0" y="88.9"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="12.7" y1="83.82" x2="12.7" y2="88.9" width="0.1524" layer="91"/>
<junction x="12.7" y="88.9"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="27.94" y1="83.82" x2="27.94" y2="88.9" width="0.1524" layer="91"/>
<junction x="27.94" y="88.9"/>
<label x="-7.62" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="-17.78" y1="71.12" x2="-12.7" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="-12.7" y1="71.12" x2="0" y2="71.12" width="0.1524" layer="91"/>
<wire x1="0" y1="71.12" x2="2.54" y2="71.12" width="0.1524" layer="91"/>
<wire x1="2.54" y1="71.12" x2="12.7" y2="71.12" width="0.1524" layer="91"/>
<junction x="2.54" y="71.12"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="12.7" y1="71.12" x2="27.94" y2="71.12" width="0.1524" layer="91"/>
<wire x1="27.94" y1="71.12" x2="45.72" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="76.2" x2="-12.7" y2="71.12" width="0.1524" layer="91"/>
<junction x="-12.7" y="71.12"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="0" y1="71.12" x2="0" y2="76.2" width="0.1524" layer="91"/>
<junction x="0" y="71.12"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="12.7" y1="76.2" x2="12.7" y2="71.12" width="0.1524" layer="91"/>
<junction x="12.7" y="71.12"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="27.94" y1="76.2" x2="27.94" y2="71.12" width="0.1524" layer="91"/>
<junction x="27.94" y="71.12"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Voltage regulation</description>
<plain>
<text x="-55.88" y="35.56" size="1.778" layer="91">TODO: RC delay calc</text>
</plain>
<instances>
<instance part="3.3V" gate="A" x="55.88" y="78.74"/>
<instance part="2.5V" gate="A" x="55.88" y="30.48"/>
<instance part="1.1V" gate="A" x="55.88" y="-15.24"/>
<instance part="L1" gate="G$1" x="81.28" y="81.28" rot="R90"/>
<instance part="L2" gate="G$1" x="81.28" y="33.02" rot="R90"/>
<instance part="L3" gate="G$1" x="81.28" y="-12.7" rot="R90"/>
<instance part="GND15" gate="1" x="33.02" y="-27.94"/>
<instance part="C21" gate="G$1" x="0" y="76.2"/>
<instance part="C22" gate="G$1" x="0" y="27.94"/>
<instance part="C23" gate="G$1" x="0" y="-17.78"/>
<instance part="C24" gate="G$1" x="104.14" y="76.2"/>
<instance part="C25" gate="G$1" x="104.14" y="27.94"/>
<instance part="C26" gate="G$1" x="104.14" y="-17.78"/>
<instance part="5V_IN_B" gate="G$1" x="-45.72" y="81.28" rot="R180"/>
<instance part="GND16" gate="1" x="-58.42" y="55.88"/>
<instance part="POWER_SW" gate="G$1" x="-17.78" y="76.2"/>
<instance part="R7" gate="G$1" x="5.08" y="53.34"/>
<instance part="C31" gate="G$1" x="15.24" y="45.72"/>
<instance part="C32" gate="G$1" x="15.24" y="60.96"/>
<instance part="R30" gate="G$1" x="5.08" y="48.26"/>
<instance part="PF1" gate="G$1" x="-30.48" y="88.9"/>
<instance part="5V_IN" gate="G$1" x="-78.74" y="38.1" rot="R180"/>
<instance part="C37" gate="G$1" x="88.9" y="-17.78"/>
<instance part="R36" gate="G$1" x="96.52" y="-20.32" rot="R90"/>
<instance part="R37" gate="G$1" x="96.52" y="-35.56" rot="R90"/>
<instance part="GND18" gate="1" x="93.98" y="-43.18"/>
<instance part="3.3/2.5/1.1/GND" gate="A" x="144.78" y="58.42"/>
</instances>
<busses>
</busses>
<nets>
<net name="5V0" class="0">
<segment>
<pinref part="2.5V" gate="A" pin="VIN"/>
<wire x1="38.1" y1="33.02" x2="0" y2="33.02" width="0.1524" layer="91"/>
<wire x1="0" y1="33.02" x2="-7.62" y2="33.02" width="0.1524" layer="91"/>
<pinref part="1.1V" gate="A" pin="VIN"/>
<wire x1="-7.62" y1="33.02" x2="-25.4" y2="33.02" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-12.7" x2="0" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="0" y1="-12.7" x2="-25.4" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-12.7" x2="-25.4" y2="33.02" width="0.1524" layer="91"/>
<wire x1="0" y1="30.48" x2="0" y2="33.02" width="0.1524" layer="91"/>
<junction x="0" y="33.02"/>
<wire x1="0" y1="-15.24" x2="0" y2="-12.7" width="0.1524" layer="91"/>
<junction x="0" y="-12.7"/>
<pinref part="C22" gate="G$1" pin="1"/>
<pinref part="C23" gate="G$1" pin="1"/>
<pinref part="3.3V" gate="A" pin="VIN"/>
<wire x1="0" y1="81.28" x2="38.1" y2="81.28" width="0.1524" layer="91"/>
<wire x1="0" y1="78.74" x2="0" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="81.28" x2="0" y2="81.28" width="0.1524" layer="91"/>
<junction x="0" y="81.28"/>
<wire x1="-7.62" y1="81.28" x2="-7.62" y2="78.74" width="0.1524" layer="91"/>
<junction x="-7.62" y="33.02"/>
<wire x1="-7.62" y1="78.74" x2="-7.62" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="50.8" x2="-7.62" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="1"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="0" y1="48.26" x2="0" y2="50.8" width="0.1524" layer="91"/>
<wire x1="0" y1="50.8" x2="0" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="50.8" x2="0" y2="50.8" width="0.1524" layer="91"/>
<junction x="-7.62" y="50.8"/>
<junction x="0" y="50.8"/>
<label x="-25.4" y="33.02" size="1.778" layer="95"/>
<pinref part="POWER_SW" gate="G$1" pin="3"/>
<wire x1="-12.7" y1="78.74" x2="-7.62" y2="78.74" width="0.1524" layer="91"/>
<junction x="-7.62" y="78.74"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<wire x1="124.46" y1="81.28" x2="104.14" y2="81.28" width="0.1524" layer="91"/>
<label x="116.84" y="81.28" size="1.778" layer="95"/>
<pinref part="L1" gate="G$1" pin="2"/>
<pinref part="3.3V" gate="A" pin="ADJ/BYP"/>
<wire x1="104.14" y1="81.28" x2="93.98" y2="81.28" width="0.1524" layer="91"/>
<wire x1="93.98" y1="81.28" x2="86.36" y2="81.28" width="0.1524" layer="91"/>
<wire x1="38.1" y1="73.66" x2="27.94" y2="73.66" width="0.1524" layer="91"/>
<wire x1="27.94" y1="73.66" x2="27.94" y2="55.88" width="0.1524" layer="91"/>
<wire x1="27.94" y1="55.88" x2="93.98" y2="55.88" width="0.1524" layer="91"/>
<wire x1="93.98" y1="55.88" x2="93.98" y2="81.28" width="0.1524" layer="91"/>
<junction x="93.98" y="81.28"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="104.14" y1="78.74" x2="104.14" y2="81.28" width="0.1524" layer="91"/>
<junction x="104.14" y="81.28"/>
<pinref part="3.3/2.5/1.1/GND" gate="A" pin="1"/>
<wire x1="142.24" y1="63.5" x2="124.46" y2="63.5" width="0.1524" layer="91"/>
<wire x1="124.46" y1="63.5" x2="124.46" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="2V5" class="0">
<segment>
<label x="116.84" y="33.02" size="1.778" layer="95"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="86.36" y1="33.02" x2="93.98" y2="33.02" width="0.1524" layer="91"/>
<pinref part="2.5V" gate="A" pin="ADJ/BYP"/>
<wire x1="93.98" y1="33.02" x2="104.14" y2="33.02" width="0.1524" layer="91"/>
<wire x1="104.14" y1="33.02" x2="127" y2="33.02" width="0.1524" layer="91"/>
<wire x1="38.1" y1="25.4" x2="27.94" y2="25.4" width="0.1524" layer="91"/>
<wire x1="27.94" y1="25.4" x2="27.94" y2="10.16" width="0.1524" layer="91"/>
<wire x1="27.94" y1="10.16" x2="93.98" y2="10.16" width="0.1524" layer="91"/>
<wire x1="93.98" y1="10.16" x2="93.98" y2="33.02" width="0.1524" layer="91"/>
<junction x="93.98" y="33.02"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="104.14" y1="30.48" x2="104.14" y2="33.02" width="0.1524" layer="91"/>
<junction x="104.14" y="33.02"/>
<pinref part="3.3/2.5/1.1/GND" gate="A" pin="2"/>
<wire x1="142.24" y1="60.96" x2="127" y2="60.96" width="0.1524" layer="91"/>
<wire x1="127" y1="60.96" x2="127" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="1V1" class="0">
<segment>
<pinref part="C37" gate="G$1" pin="1"/>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-15.24" x2="91.44" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-15.24" x2="96.52" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-12.7" x2="119.38" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="104.14" y1="-15.24" x2="104.14" y2="-12.7" width="0.1524" layer="91"/>
<junction x="104.14" y="-12.7"/>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="86.36" y1="-12.7" x2="91.44" y2="-12.7" width="0.1524" layer="91"/>
<label x="116.84" y="-12.7" size="1.778" layer="95"/>
<wire x1="91.44" y1="-12.7" x2="104.14" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-15.24" x2="91.44" y2="-12.7" width="0.1524" layer="91"/>
<junction x="91.44" y="-15.24"/>
<junction x="91.44" y="-12.7"/>
<pinref part="3.3/2.5/1.1/GND" gate="A" pin="3"/>
<wire x1="142.24" y1="58.42" x2="129.54" y2="58.42" width="0.1524" layer="91"/>
<wire x1="129.54" y1="58.42" x2="129.54" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-10.16" x2="119.38" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-10.16" x2="119.38" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$135" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="1"/>
<pinref part="1.1V" gate="A" pin="VOUT"/>
<wire x1="73.66" y1="-12.7" x2="76.2" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$136" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="1"/>
<pinref part="2.5V" gate="A" pin="VOUT"/>
<wire x1="73.66" y1="33.02" x2="76.2" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$137" class="0">
<segment>
<pinref part="3.3V" gate="A" pin="VOUT"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="76.2" y1="81.28" x2="73.66" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="1.1V" gate="A" pin="GND"/>
<wire x1="38.1" y1="-25.4" x2="33.02" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-25.4" x2="33.02" y2="5.08" width="0.1524" layer="91"/>
<pinref part="2.5V" gate="A" pin="GND"/>
<wire x1="33.02" y1="5.08" x2="33.02" y2="20.32" width="0.1524" layer="91"/>
<wire x1="33.02" y1="20.32" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<wire x1="33.02" y1="20.32" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<junction x="33.02" y="20.32"/>
<pinref part="3.3V" gate="A" pin="GND"/>
<wire x1="33.02" y1="48.26" x2="33.02" y2="68.58" width="0.1524" layer="91"/>
<wire x1="33.02" y1="68.58" x2="38.1" y2="68.58" width="0.1524" layer="91"/>
<pinref part="GND15" gate="1" pin="GND"/>
<junction x="33.02" y="-25.4"/>
<wire x1="33.02" y1="68.58" x2="15.24" y2="68.58" width="0.1524" layer="91"/>
<junction x="33.02" y="68.58"/>
<wire x1="15.24" y1="68.58" x2="0" y2="68.58" width="0.1524" layer="91"/>
<wire x1="0" y1="68.58" x2="0" y2="71.12" width="0.1524" layer="91"/>
<wire x1="0" y1="22.86" x2="0" y2="20.32" width="0.1524" layer="91"/>
<wire x1="0" y1="20.32" x2="15.24" y2="20.32" width="0.1524" layer="91"/>
<wire x1="15.24" y1="20.32" x2="33.02" y2="20.32" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-25.4" x2="0" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="0" y1="-25.4" x2="0" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="33.02" y1="48.26" x2="104.14" y2="48.26" width="0.1524" layer="91"/>
<wire x1="104.14" y1="48.26" x2="104.14" y2="71.12" width="0.1524" layer="91"/>
<junction x="33.02" y="48.26"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="33.02" y1="5.08" x2="104.14" y2="5.08" width="0.1524" layer="91"/>
<wire x1="104.14" y1="5.08" x2="104.14" y2="22.86" width="0.1524" layer="91"/>
<junction x="33.02" y="5.08"/>
<wire x1="38.1" y1="-25.4" x2="38.1" y2="-35.56" width="0.1524" layer="91"/>
<junction x="38.1" y="-25.4"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="38.1" y1="-35.56" x2="104.14" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-35.56" x2="104.14" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="15.24" y1="63.5" x2="15.24" y2="68.58" width="0.1524" layer="91"/>
<junction x="15.24" y="68.58"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="15.24" y1="40.64" x2="15.24" y2="20.32" width="0.1524" layer="91"/>
<junction x="15.24" y="20.32"/>
</segment>
<segment>
<pinref part="5V_IN_B" gate="G$1" pin="GND"/>
<wire x1="-43.18" y1="81.28" x2="-40.64" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="81.28" x2="-40.64" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="58.42" x2="-58.42" y2="58.42" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="5V_IN" gate="G$1" pin="5"/>
<wire x1="-73.66" y1="43.18" x2="-68.58" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="43.18" x2="-68.58" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="58.42" x2="-58.42" y2="58.42" width="0.1524" layer="91"/>
<junction x="-58.42" y="58.42"/>
</segment>
<segment>
<pinref part="R37" gate="G$1" pin="1"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="96.52" y1="-40.64" x2="93.98" y2="-40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="3.3/2.5/1.1/GND" gate="A" pin="4"/>
<wire x1="142.24" y1="55.88" x2="139.7" y2="55.88" width="0.1524" layer="91"/>
<wire x1="139.7" y1="55.88" x2="139.7" y2="40.64" width="0.1524" layer="91"/>
<label x="139.7" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="5V_IN_B" gate="G$1" pin="VBUS"/>
<wire x1="-35.56" y1="78.74" x2="-43.18" y2="78.74" width="0.1524" layer="91"/>
<label x="-33.02" y="81.28" size="1.778" layer="95"/>
<pinref part="PF1" gate="G$1" pin="1"/>
<wire x1="-35.56" y1="78.74" x2="-35.56" y2="88.9" width="0.1524" layer="91"/>
<pinref part="5V_IN" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="33.02" x2="-60.96" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="33.02" x2="-60.96" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="43.18" x2="-35.56" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="43.18" x2="-35.56" y2="78.74" width="0.1524" layer="91"/>
<junction x="-35.56" y="78.74"/>
</segment>
</net>
<net name="5V0FAST" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="10.16" y1="48.26" x2="15.24" y2="48.26" width="0.1524" layer="91"/>
<label x="0" y="43.18" size="1.778" layer="95"/>
<wire x1="15.24" y1="48.26" x2="20.32" y2="48.26" width="0.1524" layer="91"/>
<wire x1="20.32" y1="48.26" x2="20.32" y2="-17.78" width="0.1524" layer="91"/>
<junction x="15.24" y="48.26"/>
<pinref part="1.1V" gate="A" pin="EN"/>
<wire x1="38.1" y1="-17.78" x2="20.32" y2="-17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V0SLOW" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="10.16" y1="53.34" x2="15.24" y2="53.34" width="0.1524" layer="91"/>
<wire x1="15.24" y1="53.34" x2="15.24" y2="55.88" width="0.1524" layer="91"/>
<label x="2.54" y="55.88" size="1.778" layer="95"/>
<wire x1="15.24" y1="53.34" x2="22.86" y2="53.34" width="0.1524" layer="91"/>
<wire x1="22.86" y1="53.34" x2="22.86" y2="76.2" width="0.1524" layer="91"/>
<junction x="15.24" y="53.34"/>
<pinref part="3.3V" gate="A" pin="EN"/>
<wire x1="38.1" y1="76.2" x2="22.86" y2="76.2" width="0.1524" layer="91"/>
<wire x1="22.86" y1="53.34" x2="22.86" y2="27.94" width="0.1524" layer="91"/>
<junction x="22.86" y="53.34"/>
<pinref part="2.5V" gate="A" pin="EN"/>
<wire x1="38.1" y1="27.94" x2="22.86" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$139" class="0">
<segment>
<pinref part="PF1" gate="G$1" pin="2"/>
<wire x1="-22.86" y1="88.9" x2="-25.4" y2="88.9" width="0.1524" layer="91"/>
<pinref part="POWER_SW" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="88.9" x2="-22.86" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$145" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="1"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="96.52" y1="-25.4" x2="96.52" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-22.86" x2="88.9" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="88.9" y1="-25.4" x2="96.52" y2="-25.4" width="0.1524" layer="91"/>
<junction x="96.52" y="-25.4"/>
<pinref part="1.1V" gate="A" pin="ADJ/BYP"/>
<wire x1="38.1" y1="-20.32" x2="27.94" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-20.32" x2="27.94" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-38.1" x2="88.9" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="88.9" y1="-38.1" x2="88.9" y2="-25.4" width="0.1524" layer="91"/>
<junction x="88.9" y="-25.4"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>SD card</description>
<plain>
</plain>
<instances>
<instance part="U$2" gate="G$1" x="27.94" y="55.88"/>
<instance part="GND10" gate="1" x="-10.16" y="35.56"/>
<instance part="R23" gate="G$1" x="-7.62" y="91.44" rot="R90"/>
<instance part="R24" gate="G$1" x="2.54" y="91.44" rot="R90"/>
<instance part="R25" gate="G$1" x="12.7" y="91.44" rot="R90"/>
</instances>
<busses>
<bus name="SD_DETECT,SD_WRITEPROTECT,SD_CS,SD_MISO,SD_MOSI,SD_SCK,SD_DAT1,SD_DAT2">
<segment>
<wire x1="-20.32" y1="53.34" x2="-20.32" y2="78.74" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="VSS1"/>
<wire x1="15.24" y1="45.72" x2="-5.08" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="VSS2"/>
<wire x1="15.24" y1="43.18" x2="-5.08" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="15.24" y1="38.1" x2="-5.08" y2="38.1" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="-5.08" y1="38.1" x2="-10.16" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="43.18" x2="-5.08" y2="38.1" width="0.1524" layer="91"/>
<junction x="-5.08" y="38.1"/>
<wire x1="-5.08" y1="45.72" x2="-5.08" y2="43.18" width="0.1524" layer="91"/>
<junction x="-5.08" y="43.18"/>
<wire x1="-5.08" y1="45.72" x2="-5.08" y2="73.66" width="0.1524" layer="91"/>
<junction x="-5.08" y="45.72"/>
<pinref part="U$2" gate="G$1" pin="COMMON_SW"/>
<wire x1="-5.08" y1="73.66" x2="15.24" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="VDD"/>
<wire x1="15.24" y1="48.26" x2="-10.16" y2="48.26" width="0.1524" layer="91"/>
<label x="-10.16" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-17.78" y1="101.6" x2="-7.62" y2="101.6" width="0.1524" layer="91"/>
<label x="-15.24" y="101.6" size="1.778" layer="95"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="-7.62" y1="101.6" x2="2.54" y2="101.6" width="0.1524" layer="91"/>
<wire x1="2.54" y1="101.6" x2="12.7" y2="101.6" width="0.1524" layer="91"/>
<wire x1="12.7" y1="101.6" x2="15.24" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="96.52" x2="-7.62" y2="101.6" width="0.1524" layer="91"/>
<junction x="-7.62" y="101.6"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="2.54" y1="96.52" x2="2.54" y2="101.6" width="0.1524" layer="91"/>
<junction x="2.54" y="101.6"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="12.7" y1="96.52" x2="12.7" y2="101.6" width="0.1524" layer="91"/>
<junction x="12.7" y="101.6"/>
</segment>
</net>
<net name="SD_DETECT" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="CARD_DETECT"/>
<wire x1="-20.32" y1="76.2" x2="15.24" y2="76.2" width="0.1524" layer="91"/>
<label x="-17.78" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_WRITEPROTECT" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="WRITE_PROTECT"/>
<wire x1="-20.32" y1="71.12" x2="15.24" y2="71.12" width="0.1524" layer="91"/>
<label x="-17.78" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_CS" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="CS"/>
<wire x1="-20.32" y1="58.42" x2="12.7" y2="58.42" width="0.1524" layer="91"/>
<label x="-17.78" y="58.42" size="1.778" layer="95"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="12.7" y1="58.42" x2="15.24" y2="58.42" width="0.1524" layer="91"/>
<wire x1="12.7" y1="86.36" x2="12.7" y2="58.42" width="0.1524" layer="91"/>
<junction x="12.7" y="58.42"/>
</segment>
</net>
<net name="SD_MOSI" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DATA_IN"/>
<wire x1="-20.32" y1="63.5" x2="2.54" y2="63.5" width="0.1524" layer="91"/>
<label x="-17.78" y="63.5" size="1.778" layer="95"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="2.54" y1="63.5" x2="15.24" y2="63.5" width="0.1524" layer="91"/>
<wire x1="2.54" y1="86.36" x2="2.54" y2="63.5" width="0.1524" layer="91"/>
<junction x="2.54" y="63.5"/>
</segment>
</net>
<net name="SD_MISO" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DATA_OUT"/>
<wire x1="-20.32" y1="66.04" x2="-7.62" y2="66.04" width="0.1524" layer="91"/>
<label x="-17.78" y="66.04" size="1.778" layer="95"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="66.04" x2="15.24" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="86.36" x2="-7.62" y2="66.04" width="0.1524" layer="91"/>
<junction x="-7.62" y="66.04"/>
</segment>
</net>
<net name="SD_SCK" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="SCLK"/>
<wire x1="-20.32" y1="60.96" x2="15.24" y2="60.96" width="0.1524" layer="91"/>
<label x="-17.78" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_DAT1" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DAT1"/>
<wire x1="-20.32" y1="55.88" x2="15.24" y2="55.88" width="0.1524" layer="91"/>
<label x="-17.78" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_DAT2" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DAT2"/>
<wire x1="-20.32" y1="53.34" x2="15.24" y2="53.34" width="0.1524" layer="91"/>
<label x="-17.78" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>ADC</description>
<plain>
</plain>
<instances>
<instance part="ADC" gate="G$1" x="-10.16" y="81.28"/>
<instance part="JP1" gate="G$1" x="-63.5" y="81.28"/>
<instance part="R38" gate="G$1" x="-50.8" y="99.06" rot="R90"/>
<instance part="R39" gate="G$1" x="-45.72" y="99.06" rot="R270"/>
<instance part="C39" gate="G$1" x="-33.02" y="99.06"/>
</instances>
<busses>
<bus name="ADC:SCL,SDA">
<segment>
<wire x1="-53.34" y1="68.58" x2="-53.34" y2="91.44" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="SDA" class="0">
<segment>
<pinref part="ADC" gate="G$1" pin="SDA"/>
<wire x1="-53.34" y1="76.2" x2="-50.8" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="76.2" x2="-22.86" y2="76.2" width="0.1524" layer="91"/>
<junction x="-50.8" y="76.2"/>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="93.98" x2="-50.8" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="ADC" gate="G$1" pin="SCL"/>
<wire x1="-53.34" y1="78.74" x2="-45.72" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="78.74" x2="-22.86" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="78.74" x2="-45.72" y2="93.98" width="0.1524" layer="91"/>
<junction x="-45.72" y="78.74"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="ADC" gate="G$1" pin="VSS"/>
<label x="-48.26" y="83.82" size="1.778" layer="95"/>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="-22.86" y1="83.82" x2="-33.02" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="2"/>
<wire x1="-33.02" y1="83.82" x2="-55.88" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="93.98" x2="-33.02" y2="83.82" width="0.1524" layer="91"/>
<junction x="-33.02" y="83.82"/>
</segment>
</net>
<net name="AUDIO_IN" class="0">
<segment>
<pinref part="ADC" gate="G$1" pin="AIN"/>
<pinref part="JP1" gate="G$1" pin="1"/>
<wire x1="-55.88" y1="81.28" x2="-22.86" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V0" class="0">
<segment>
<pinref part="ADC" gate="G$1" pin="VCC"/>
<wire x1="-22.86" y1="86.36" x2="-38.1" y2="86.36" width="0.1524" layer="91"/>
<label x="-48.26" y="86.36" size="1.778" layer="95"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="86.36" x2="-48.26" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="101.6" x2="-38.1" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="101.6" x2="-38.1" y2="86.36" width="0.1524" layer="91"/>
<junction x="-38.1" y="86.36"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="106.68" x2="-50.8" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="106.68" x2="-50.8" y2="104.14" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="104.14" x2="-45.72" y2="106.68" width="0.1524" layer="91"/>
<junction x="-45.72" y="106.68"/>
<wire x1="-45.72" y1="106.68" x2="-27.94" y2="106.68" width="0.1524" layer="91"/>
<label x="-33.02" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Mounting</description>
<plain>
</plain>
<instances>
<instance part="H1" gate="G$1" x="-27.94" y="91.44"/>
<instance part="H2" gate="G$1" x="38.1" y="91.44"/>
<instance part="H3" gate="G$1" x="-27.94" y="50.8"/>
<instance part="H4" gate="G$1" x="38.1" y="48.26"/>
<instance part="H5" gate="G$1" x="-27.94" y="22.86"/>
<instance part="H6" gate="G$1" x="38.1" y="22.86"/>
<instance part="GND19" gate="1" x="15.24" y="5.08"/>
<instance part="H7" gate="G$1" x="38.1" y="12.7"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="15.24" y1="7.62" x2="-33.02" y2="7.62" width="0.1524" layer="91"/>
<junction x="15.24" y="7.62"/>
<wire x1="15.24" y1="7.62" x2="15.24" y2="12.7" width="0.1524" layer="91"/>
<wire x1="15.24" y1="12.7" x2="15.24" y2="22.86" width="0.1524" layer="91"/>
<wire x1="15.24" y1="22.86" x2="15.24" y2="48.26" width="0.1524" layer="91"/>
<wire x1="15.24" y1="48.26" x2="15.24" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="7.62" x2="-33.02" y2="22.86" width="0.1524" layer="91"/>
<pinref part="H5" gate="G$1" pin="MOUNT"/>
<wire x1="-33.02" y1="22.86" x2="-33.02" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="50.8" x2="-33.02" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="22.86" x2="-33.02" y2="22.86" width="0.1524" layer="91"/>
<junction x="-33.02" y="22.86"/>
<pinref part="H3" gate="G$1" pin="MOUNT"/>
<wire x1="-30.48" y1="50.8" x2="-33.02" y2="50.8" width="0.1524" layer="91"/>
<junction x="-33.02" y="50.8"/>
<pinref part="H1" gate="G$1" pin="MOUNT"/>
<wire x1="-30.48" y1="91.44" x2="-33.02" y2="91.44" width="0.1524" layer="91"/>
<pinref part="H2" gate="G$1" pin="MOUNT"/>
<wire x1="35.56" y1="91.44" x2="15.24" y2="91.44" width="0.1524" layer="91"/>
<pinref part="H4" gate="G$1" pin="MOUNT"/>
<wire x1="35.56" y1="48.26" x2="15.24" y2="48.26" width="0.1524" layer="91"/>
<junction x="15.24" y="48.26"/>
<pinref part="H6" gate="G$1" pin="MOUNT"/>
<wire x1="35.56" y1="22.86" x2="15.24" y2="22.86" width="0.1524" layer="91"/>
<junction x="15.24" y="22.86"/>
<pinref part="H7" gate="G$1" pin="MOUNT"/>
<wire x1="35.56" y1="12.7" x2="15.24" y2="12.7" width="0.1524" layer="91"/>
<junction x="15.24" y="12.7"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
