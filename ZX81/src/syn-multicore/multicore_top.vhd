
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity multicore_top is
	port (
-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= '0'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
	);
end entity;

architecture Behavior of multicore_top is

	-- Master clock
	signal clock_master		: std_logic;
	signal pll_locked			: std_logic;					-- PLL travado quando 1
	signal clock_video		: std_logic;
	signal clk65 				: std_ulogic;
	signal clock_cpu			: std_logic;

	-- Resets
	signal reset_interno_n	: std_logic;
	signal n_reset 			: std_ulogic := '1';

	-- Memory buses
	signal cpu_a				: std_logic_vector(15 downto 0);

	signal ram_a				: std_logic_vector(15 downto 0);		-- 64K
	signal ram_din				: std_logic_vector(7 downto 0);
	signal ram_dout			: std_logic_vector(7 downto 0);
	signal ram_cs_n			: std_logic;
	signal ram_oe_n			: std_logic;
	signal ram_we_n			: std_logic;

	signal rom_a				: std_logic_vector(12 downto 0);		-- 8K
	signal rom_dout			: std_logic_vector(7 downto 0);
	signal rom_cs_n			: std_logic;

	signal ramUDG_a			: std_logic_vector(10 downto 0);		-- 2K
	signal ramUDG_din			: std_logic_vector(7 downto 0);
	signal ramUDG_dout		: std_logic_vector(7 downto 0);
	signal ramUDG_cs_n		: std_logic;
	signal ramUDG_oe_n		: std_logic;
	signal ramUDG_we_n		: std_logic;

	-- Teclado e joystick
	signal kbd_col_i			: std_logic_vector(4 downto 0);
	signal kbd_col_o			: std_logic_vector(4 downto 0);
	signal joy_columns		: std_logic_vector(4 downto 0);
	signal teclasF				: std_logic_vector(12 downto 1);		-- Teclas de funções

   signal kbd_clk				: std_ulogic;
   signal kbd_data			: std_ulogic;
   signal video 				: std_ulogic;
   signal tape_in 			: std_ulogic;
   signal tape_out			: std_ulogic;
	signal hsync 				: std_ulogic;
	signal vsync 				: std_ulogic;
	signal config				: std_logic_vector(3 downto 0);

	-- Audio
	signal s_psg				: std_logic_vector(7 downto 0);
	signal audio_dac_s		: std_logic;

	-- Kolour
	signal Kolour_s 			: std_logic_vector(7 downto 0);
	signal bright_s 			: std_logic;
	signal hblank_s 			: std_logic;

	
	-- debug
	signal hexdata 			: std_logic_vector(15 downto 0);
	signal sdbg		 			: std_logic_vector(7 downto 0);
	

	


begin

	
	--------------------------------
	-- PLL
	--  50 MHz input
	--  13 MHz master clock output
	--------------------------------
	pll: entity work.pll1
	port map (
		inclk0		=> clock_50_i,				-- Clock 50 MHz externo
		c0				=> clock_master,			-- Saida 13 MHz
		locked		=> pll_locked				-- Sinal de travamento (1 = PLL travado e pronto)
	);

	
	clock_video <= clk65;
	
	-- Divide clock por 2
	process (clock_master)
	begin
		if rising_edge(clock_master) then
			if clk65='0' then
				clk65 <= '1';
			else
				clk65 <= '0';
			end if;
		end if;
	end process;

	-- Instancia ZX81
	zx01_inst : entity work.zx01
	port map (
		clock			=> clock_video,		-- in
		ophi			=> clock_cpu,			-- out
		ext_reset_n	=> n_reset,				-- in
		int_reset_n	=> reset_interno_n,	-- out
		videoRGB		=> video,				-- out
		videoCVBS	=> open,
		ohsync		=> hsync,				-- out
		ovsync		=> vsync,				-- out
		oBlank		=> hblank_s,
		tape_in		=> tape_in,				-- in
		tape_out		=> tape_out,			-- out

		psg_en		=> '1',					-- in
		usa_uk		=> '0',					-- in
		chr13			=> '0',					-- in
		v_inv			=> '1',					-- in
		config_o		=> config,				-- out

		cpu_a			=> cpu_a,

		ram_A			=> ram_a,				-- out
		ram_Din		=> ram_din,				-- out
		ram_Dout		=> ram_dout,			-- in
		ram_CE_n		=> ram_cs_n,			-- out
		ram_OE_n		=> ram_oe_n,			-- out
		ram_WE_n		=> ram_we_n,			-- out

		rom_A			=> rom_a,				-- out
		rom_D			=> rom_dout,			-- in
		rom_CE_n		=> rom_cs_n,			-- out

		ramUDG_A		=> ramUDG_A,
		ramUDG_Din	=> ramUDG_Din,
		ramUDG_Dout	=> ramUDG_Dout,
		ramUDG_CE_n	=> ramUDG_cs_n,
		ramUDG_OE_n	=> ramUDG_oe_n,
		ramUDG_WE_n	=> ramUDG_we_n,

		kbd_col		=> kbd_col_i,			-- in
		psg_chMix	=> s_psg,				-- out
		
		Kolour_o		=> Kolour_s,
		
		sdbg			=> sdbg
	);

--	-- Teclado
--	c_PS2_MatrixEncoder: entity work.PS2_MatrixEncoder
--	port map (
--		Clk		=> clock_cpu,
--		Reset_n	=> reset_interno_n,
--		PS2_Clk	=> PS2_CLK,
--		PS2_Data	=> PS2_DAT,
--		Key_Addr	=> cpu_a(15 downto 8),
--		Key_Data	=> kbd_col_o
--	);

	-- Teclado
	teclado: entity work.keyboard
	port map (
		CLK			=> clock_cpu,
		nRESET		=> reset_interno_n,
		PS2_CLK		=> ps2_clk_io,
		PS2_DATA		=> ps2_data_io,
		rows			=> cpu_a(15 downto 8),
		cols			=> kbd_col_o,
		teclasF		=> teclasF
	);

		-- Audio
	audioout: entity work.dac
	generic map (
		msbi_g		=> 7
	)
	port map (
		clk_i		=> clock_master,
		res_i		=> not n_reset,
		dac_i		=> s_psg,
		dac_o		=> audio_dac_s
	);
	--tapeout		=> tape_in,
	
	dac_l_o <= audio_dac_s;
	dac_r_o <= audio_dac_s;


	-- Glue
	n_reset <= (btn_n_i(3) or btn_n_i(4)) and not teclasF(11);

	-- RAM
	sram_addr_o		<= "000" & ram_a;
	sram_data_io	<= ram_din when ram_cs_n = '0' and ram_we_n = '0' else (others => 'Z');
	ram_dout			<= sram_data_io;
	sram_oe_n_o		<= ram_oe_n;
	sram_we_n_o		<= ram_we_n;

	-- RAM UDG
	ramUDG: entity work.spram
	generic map (
		addr_width_g	=> 11,
		data_width_g	=> 8
	)
	port map (
		clk_i		=> clock_cpu,			-- Importante: usar clock da CPU
		we_i		=> not ramUDG_we_n,
		addr_i	=> ramUDG_A,
		data_i	=> ramUDG_Din,
		data_o	=> ramUDG_Dout
	);

	-- ROM
	rom: entity work.tk85_rom
	port map (
		clk		=> clock_cpu,			-- Importante: usar clock da CPU
		addr		=> rom_a,
		data		=> rom_dout
	);

	-- config
	--LEDR(3 downto 0) <= config;

	-- Teclado e joystick

--	kbd_col_i	<= kbd_col_o;
	kbd_col_i	<= kbd_col_o and joy_columns;

	process (cpu_a, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i)
		variable jr1 : std_logic_vector(4 downto 0);
		variable jr2 : std_logic_vector(4 downto 0);
	begin
		-- 5 (left), 6 (down), 7 (up), 8 (right) and 0 (fire)
		if cpu_a(11) = '0' then
			jr1 := joy1_left_i & "1111";
		else
			jr1 := (others => '1');
		end if;
		if cpu_a(12) = '0' then
			jr2 := joy1_down_i & joy1_up_i & joy1_right_i & '1' & joy1_p6_i;
		else
			jr2 := (others => '1');
		end if;
		joy_columns <= jr1 and jr2;
	end process;


	-- Video
	
	bright_s <= Kolour_s(6);
	
	
	vga_B_o  <= "00000"	when hblank_s = '1' else bright_s & Kolour_s(0) & Kolour_s(0) & Kolour_s(0) & Kolour_s(0) when video = '1' else bright_s & Kolour_s(3) & Kolour_s(3) & Kolour_s(3) & Kolour_s(3);
	VGA_R_o  <= "00000"	when hblank_s = '1' else bright_s & Kolour_s(1) & Kolour_s(1) & Kolour_s(1) & Kolour_s(1) when video = '1' else bright_s & Kolour_s(4) & Kolour_s(4) & Kolour_s(4) & Kolour_s(4);
	VGA_G_o  <= "00000"	when hblank_s = '1' else bright_s & Kolour_s(2) & Kolour_s(2) & Kolour_s(2) & Kolour_s(2) when video = '1' else bright_s & Kolour_s(5) & Kolour_s(5) & Kolour_s(5) & Kolour_s(5);
	VGA_hsync_n_o <= not hsync;
	VGA_vsync_n_o <= not vsync;

	-- Debug
	
--	LEDG(0) <= ram_cs_n;
--	LEDG(1) <= ram_oe_n;
--	LEDG(2) <= ram_we_n;
--	LEDG(3) <= rom_cs_n;
--	LEDG(4) <= ramUDG_cs_n;
--	LEDG(5) <= teclasF(11);
--	LEDG(7) <= tape_in;
--





	

end architecture;
