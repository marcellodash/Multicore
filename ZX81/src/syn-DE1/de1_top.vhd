--
-- Terasic DE1 top-level
--

-- altera message_off 10540 10541

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Generic top-level entity for Altera DE1 board
entity de1_top is
	port (
		-- Clocks
		CLOCK_24       : in    std_logic_vector(1 downto 0);
		CLOCK_27       : in    std_logic_vector(1 downto 0);
		CLOCK_50       : in    std_logic;
		EXT_CLOCK      : in    std_logic;

		-- Switches
		SW             : in    std_logic_vector(9 downto 0);
		-- Buttons
		KEY            : in    std_logic_vector(3 downto 0);

		-- 7 segment displays
		HEX0           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX1           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX2           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX3           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		-- Red LEDs
		LEDR           : out   std_logic_vector(9 downto 0)		:= (others => '0');
		-- Green LEDs
		LEDG           : out   std_logic_vector(7 downto 0)		:= (others => '0');

		-- VGA
		VGA_R          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_G          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_B          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_HS         : out   std_logic									:= '1';
		VGA_VS         : out   std_logic									:= '1';

		-- Serial
		UART_RXD       : in    std_logic;
		UART_TXD       : out   std_logic									:= '1';

		-- PS/2 Keyboard
		PS2_CLK        : inout std_logic									:= '1';
		PS2_DAT        : inout std_logic									:= '1';

		-- I2C
		I2C_SCLK       : inout std_logic									:= '1';
		I2C_SDAT       : inout std_logic									:= '1';

		-- Audio
		AUD_XCK        : out   std_logic									:= '0';
		AUD_BCLK       : out   std_logic									:= '0';
		AUD_ADCLRCK    : out   std_logic									:= '0';
		AUD_ADCDAT     : in    std_logic;
		AUD_DACLRCK    : out   std_logic									:= '0';
		AUD_DACDAT     : out   std_logic									:= '0';

		-- SRAM
		SRAM_ADDR      : out   std_logic_vector(17 downto 0)		:= (others => '0');
		SRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => 'Z');
		SRAM_CE_N      : out   std_logic									:= '1';
		SRAM_OE_N      : out   std_logic									:= '1';
		SRAM_WE_N      : out   std_logic									:= '1';
		SRAM_UB_N      : out   std_logic									:= '1';
		SRAM_LB_N      : out   std_logic									:= '1';

		-- SDRAM
		DRAM_ADDR      : out   std_logic_vector(11 downto 0)		:= (others => '0');
		DRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => 'Z');
		DRAM_BA_0      : out   std_logic									:= '1';
		DRAM_BA_1      : out   std_logic									:= '1';
		DRAM_CAS_N     : out   std_logic									:= '1';
		DRAM_CKE       : out   std_logic									:= '1';
		DRAM_CLK       : out   std_logic									:= '1';
		DRAM_CS_N      : out   std_logic									:= '1';
		DRAM_LDQM      : out   std_logic									:= '1';
		DRAM_RAS_N     : out   std_logic									:= '1';
		DRAM_UDQM      : out   std_logic									:= '1';
		DRAM_WE_N      : out   std_logic									:= '1';

		-- Flash
		FL_ADDR        : out   std_logic_vector(21 downto 0)		:= (others => '0');
		FL_DQ          : inout std_logic_vector(7 downto 0)		:= (others => '0');
		FL_RST_N       : out   std_logic									:= '1';
		FL_OE_N        : out   std_logic									:= '1';
		FL_WE_N        : out   std_logic									:= '1';
		FL_CE_N        : out   std_logic									:= '1';

		-- SD card (SPI mode)
		SD_nCS         : out   std_logic									:= '1';
		SD_MOSI        : out   std_logic									:= '1';
		SD_SCLK        : out   std_logic									:= '1';
		SD_MISO        : in    std_logic;

		-- GPIO
		GPIO_0         : inout std_logic_vector(35 downto 0)		:= (others => 'Z');
		GPIO_1         : inout std_logic_vector(35 downto 0)		:= (others => 'Z')
	);
end entity;

architecture Behavior of de1_top is

	-- Master clock
	signal clock_master		: std_logic;
	signal pll_locked			: std_logic;					-- PLL travado quando 1
	signal clock_video		: std_logic;
	signal clk65 				: std_ulogic;
	signal clock_cpu			: std_logic;

	-- Resets
	signal reset_interno_n	: std_logic;
	signal n_reset 			: std_ulogic := '1';

	-- Memory buses
	signal cpu_a				: std_logic_vector(15 downto 0);

	signal ram_a				: std_logic_vector(15 downto 0);		-- 64K
	signal ram_din				: std_logic_vector(7 downto 0);
	signal ram_dout			: std_logic_vector(7 downto 0);
	signal ram_cs_n			: std_logic;
	signal ram_oe_n			: std_logic;
	signal ram_we_n			: std_logic;

	signal rom_a				: std_logic_vector(12 downto 0);		-- 8K
	signal rom_dout			: std_logic_vector(7 downto 0);
	signal rom_cs_n			: std_logic;

	signal ramUDG_a			: std_logic_vector(10 downto 0);		-- 2K
	signal ramUDG_din			: std_logic_vector(7 downto 0);
	signal ramUDG_dout		: std_logic_vector(7 downto 0);
	signal ramUDG_cs_n		: std_logic;
	signal ramUDG_oe_n		: std_logic;
	signal ramUDG_we_n		: std_logic;

	-- Teclado e joystick
	signal kbd_col_i			: std_logic_vector(4 downto 0);
	signal kbd_col_o			: std_logic_vector(4 downto 0);
	signal joy_columns		: std_logic_vector(4 downto 0);
	signal teclasF				: std_logic_vector(12 downto 1);		-- Teclas de funções

   signal kbd_clk				: std_ulogic;
   signal kbd_data			: std_ulogic;
   signal video 				: std_ulogic;
   signal tape_in 			: std_ulogic;
   signal tape_out			: std_ulogic;
	signal hsync 				: std_ulogic;
	signal vsync 				: std_ulogic;
	signal config				: std_logic_vector(3 downto 0);

	-- Audio
	signal s_psg				: std_logic_vector(7 downto 0);

	-- debug
	signal hexdata 			: std_logic_vector(15 downto 0);
	signal sdbg		 			: std_logic_vector(7 downto 0);

	-- Joystick (Ligacoes com GPIO padrao Minimig)
	alias J0_UP					: std_logic						is GPIO_1(34);
	alias J0_DOWN				: std_logic						is GPIO_1(32);
	alias J0_LEFT				: std_logic						is GPIO_1(30);
	alias J0_RIGHT				: std_logic						is GPIO_1(28);
	alias J0_BTN				: std_logic						is GPIO_1(35);
	alias J1_UP					: std_logic						is GPIO_1(24);	
	alias J1_DOWN				: std_logic						is GPIO_1(22);
	alias J1_LEFT				: std_logic						is GPIO_1(20);
	alias J1_RIGHT				: std_logic						is GPIO_1(23);
	alias J1_BTN				: std_logic						is GPIO_1(25);

begin

	
	--------------------------------
	-- PLL
	--  50 MHz input
	--  13 MHz master clock output
	--------------------------------
	pll: entity work.pll1
	port map (
		inclk0		=> CLOCK_50,				-- Clock 50 MHz externo
		c0				=> clock_master,			-- Saida 13 MHz
		locked		=> pll_locked				-- Sinal de travamento (1 = PLL travado e pronto)
	);

	-- Divide clock por 2
	process (clock_master)
	begin
		if rising_edge(clock_master) then
			if clk65='0' then
				clk65 <= '1';
			else
				clk65 <= '0';
			end if;
		end if;
	end process;

	-- Instancia ZX81
	zx01_inst : entity work.zx01
	port map (
		clock			=> clock_video,		-- in
		ophi			=> clock_cpu,			-- out
		ext_reset_n	=> n_reset,				-- in
		int_reset_n	=> reset_interno_n,	-- out
		videoRGB		=> video,				-- out
		videoCVBS	=> open,
		ohsync		=> hsync,				-- out
		ovsync		=> vsync,				-- out
		tape_in		=> tape_in,				-- in
		tape_out		=> tape_out,			-- out

		psg_en		=> SW(3),				-- in
		usa_uk		=> SW(2),				-- in
		chr13			=> SW(1),				-- in
		v_inv			=> SW(0),				-- in
		config_o		=> config,				-- out

		cpu_a			=> cpu_a,

		ram_A			=> ram_a,				-- out
		ram_Din		=> ram_din,				-- out
		ram_Dout		=> ram_dout,			-- in
		ram_CE_n		=> ram_cs_n,			-- out
		ram_OE_n		=> ram_oe_n,			-- out
		ram_WE_n		=> ram_we_n,			-- out

		rom_A			=> rom_a,				-- out
		rom_D			=> rom_dout,			-- in
		rom_CE_n		=> rom_cs_n,			-- out

		ramUDG_A		=> ramUDG_A,
		ramUDG_Din	=> ramUDG_Din,
		ramUDG_Dout	=> ramUDG_Dout,
		ramUDG_CE_n	=> ramUDG_cs_n,
		ramUDG_OE_n	=> ramUDG_oe_n,
		ramUDG_WE_n	=> ramUDG_we_n,

		kbd_col		=> kbd_col_i,			-- in
		psg_chMix	=> s_psg,				-- out
		sdbg			=> sdbg
	);

--	-- Teclado
--	c_PS2_MatrixEncoder: entity work.PS2_MatrixEncoder
--	port map (
--		Clk		=> clock_cpu,
--		Reset_n	=> reset_interno_n,
--		PS2_Clk	=> PS2_CLK,
--		PS2_Data	=> PS2_DAT,
--		Key_Addr	=> cpu_a(15 downto 8),
--		Key_Data	=> kbd_col_o
--	);

	-- Teclado
	teclado: entity work.keyboard
	port map (
		CLK			=> clock_cpu,
		nRESET		=> reset_interno_n,
		PS2_CLK		=> PS2_CLK,
		PS2_DATA		=> PS2_DAT,
		rows			=> cpu_a(15 downto 8),
		cols			=> kbd_col_o,
		teclasF		=> teclasF
	);

	-- Audio para o Tape
	audio : entity work.Audio_WM8731
	port map (
		reset_n		=> reset_interno_n,
		clock			=> CLOCK_24(1),
		K7feedback	=> SW(4),
		psg			=> s_psg,
		tapeout		=> tape_in,
		i2s_xck		=>	AUD_XCK,								-- Ligar nos pinos do TOP
		i2s_bclk		=> AUD_BCLK,
		i2s_adclrck	=> AUD_ADCLRCK,
		i2s_adcdat	=> AUD_ADCDAT,
		i2s_daclrck	=> AUD_DACLRCK,
		i2s_dacdat	=> AUD_DACDAT,
		
		i2c_sda		=> I2C_SDAT,							-- Ligar no pino I2C SDA
		i2c_scl		=> I2C_SCLK								-- Ligar no pino I2C SCL
	);

	-- Glue
	n_reset <= KEY(0) and not teclasF(11);

	-- RAM
	SRAM_ADDR				<= "00" & ram_a;
	SRAM_DQ(7 downto 0)	<= ram_din when ram_cs_n = '0' and ram_we_n = '0' else (others => 'Z');
	ram_dout					<= SRAM_DQ(7 downto 0);
	SRAM_DQ(15 downto 8)	<= (others => 'Z');
	SRAM_LB_N				<= ram_cs_n;
	SRAM_UB_N				<= '1';
	SRAM_CE_N				<= ram_cs_n;
	SRAM_OE_N				<= ram_oe_n;
	SRAM_WE_N				<= ram_we_n;

	-- RAM UDG
	ramUDG: entity work.spram
	generic map (
		addr_width_g	=> 11,
		data_width_g	=> 8
	)
	port map (
		clk_i		=> clock_cpu,			-- Importante: usar clock da CPU
		we_i		=> not ramUDG_we_n,
		addr_i	=> ramUDG_A,
		data_i	=> ramUDG_Din,
		data_o	=> ramUDG_Dout
	);

	-- ROM
	rom: entity work.tk85_rom
	port map (
		clk		=> clock_cpu,			-- Importante: usar clock da CPU
		addr		=> rom_a,
		data		=> rom_dout
	);

	-- config
	LEDR(3 downto 0) <= config;

	-- Teclado e joystick

--	kbd_col_i	<= kbd_col_o;
	kbd_col_i	<= kbd_col_o and joy_columns;

	process (cpu_a, J0_UP, J0_DOWN, J0_LEFT, J0_RIGHT, J0_BTN)
		variable jr1 : std_logic_vector(4 downto 0);
		variable jr2 : std_logic_vector(4 downto 0);
	begin
		-- 5 (left), 6 (down), 7 (up), 8 (right) and 0 (fire)
		if cpu_a(11) = '0' then
			jr1 := J0_LEFT & "1111";
		else
			jr1 := (others => '1');
		end if;
		if cpu_a(12) = '0' then
			jr2 := J0_DOWN & J0_UP & J0_RIGHT & '1' & J0_BTN;
		else
			jr2 := (others => '1');
		end if;
		joy_columns <= jr1 and jr2;
	end process;


	-- Video
	VGA_R  <= video & video & video & video;
	VGA_G  <= video & video & video & video;
	VGA_B  <= video & video & video & video;
	VGA_HS <= not hsync;
	VGA_VS <= not vsync;

	-- Debug
	
	LEDG(0) <= ram_cs_n;
	LEDG(1) <= ram_oe_n;
	LEDG(2) <= ram_we_n;
	LEDG(3) <= rom_cs_n;
	LEDG(4) <= ramUDG_cs_n;
	LEDG(5) <= teclasF(11);
	LEDG(7) <= tape_in;

	
--	hexdata(15 downto 12) <= "0000";
--	hexdata(11 downto  8) <= "0000";
--	hexdata(7  downto  4) <= sdbg(7 downto 4);
--	hexdata(3  downto  0) <= sdbg(3 downto 0);

	hexdata(15 downto 12) <= ram_a(15 downto 12);
	hexdata(11 downto  8) <= ram_a(11 downto 8);
	hexdata(7  downto  4) <= ram_a(7 downto 4);
	hexdata(3  downto  0) <= ram_a(3 downto 0);
	
	clock_video <= clk65 WHEN SW(9) = '0' else KEY(1);
--	clock_video <= clk65;

	-- Led display
	ld3: entity work.seg7 port map(
		D		=> hexdata(15 downto 12),
		Q		=> HEX3
	);

	ld2: entity work.seg7 port map(
		D		=> hexdata(11 downto 8),
		Q		=> HEX2
	);

	ld1: entity work.seg7 port map(
		D		=> hexdata(7 downto 4),
		Q		=> HEX1
	);

	ld0: entity work.seg7 port map(
		D		=> hexdata(3 downto 0),
		Q		=> HEX0
	);

end architecture;
