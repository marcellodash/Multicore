----------------------------------------------------------
--  modes97.vhd
--			Modes for ZX97
--			==============
--
--  04/29/97	Bodo Wenzel	Got from old top.vhd
--  11/14/97	Bodo Wenzel	Changed to POKE
--  11/26/97	Bodo Wenzel	ROM select
--  03/20/98	Bodo Wenzel	Paging of memory
--				and reading initial modes
--  03/23/98	Bodo Wenzel	Video inversion
--  01/28/99	Bodo Wenzel	New modes
----------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-- the inputs and outputs --------------------------------

entity modes97 is
	port (
		n_reset		: in  std_ulogic;
		n_modes		: in  std_ulogic;
		phi			: in  std_ulogic;
		v_inv			: in  std_ulogic;
		chr13			: in  std_ulogic;
		addr			: in  std_ulogic_vector(15 downto 0);
		data			: in  std_ulogic_vector(7 downto 0);
		n_mreq		: in  std_ulogic;
		n_wr			: in  std_ulogic;
		mode_reset	: out boolean;
		mode_v_inv	: out boolean;
		mode_chr13	: out std_ulogic;
		Kolour_o		: out std_logic_vector(7 downto 0)
	);
end;

-- the description of the logic --------------------------

architecture beh of modes97 is

	constant POKE : std_ulogic_vector(15 downto 0) := X"0007";

	signal poke7  : boolean;
	signal poke8  : boolean;
	
	signal Kolour_s : std_logic_vector(7 downto 0);

begin

	poke7 <= n_mreq = '0' and n_wr = '0' and addr = POKE;
	poke8 <= n_mreq = '0' and n_wr = '0' and addr = X"0008";
	
	Kolour_o <= Kolour_s;

	process (n_reset, phi)
	begin
		if n_reset='0' then
			mode_reset <= FALSE;
			Kolour_s <= "00111000"; --fundo branco, letra preta
		elsif rising_edge(phi) then
			if poke7 then
				mode_reset <= data(7) = '1';
			end if;
			if poke8 then
				Kolour_s <= to_stdlogicvector (data);
			end if;
		end if;
	end process;

	process (n_modes, phi, chr13)
	begin
		if n_modes = '0' then
			mode_chr13 <= chr13;
		elsif rising_edge(phi) then
			if poke7 then
				mode_chr13 <= data(1);
			end if;
		end if;
	end process;

	process (phi)
	begin
		if rising_edge(phi) then
			if n_modes = '0' then
				mode_v_inv <= v_inv = '1';
			elsif poke7 then
				mode_v_inv <= data(0) = '1';
			end if;
		end if;
	end process;
end;

-- end ---------------------------------------------------
