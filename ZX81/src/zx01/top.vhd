----------------------------------------------------------
--  top.vhd
--		Top level of the ZX97
--		=====================
--
--  04/10/97	Bodo Wenzel	Dummy creation
--  04/16/97	Bodo Wenzel	Filling with "life"
--  04/29/97	Bodo Wenzel	Dividing into modules
--  11/14/97	Bodo Wenzel	Knowledge from ZX81VID
--  11/25/97	Bodo Wenzel	Correcting errors
--  12/03/97	Bodo Wenzel	Additional LCD output
--  03/18/98	Bodo Wenzel	HRG
--  03/20/98	Bodo Wenzel	Paging of memory and
--				reading of initial modes
--  01/28/99	Bodo Wenzel	Improvements
--  04/10/02	Daniel Wallner	Added synchronous bus support
----------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-- the inputs and outputs --------------------------------

entity top is
	generic (
		synchronous	: boolean := false
	);
	port (
		clock			: in  std_ulogic;
		clock_2		: out std_ulogic;
		phi			: in  std_ulogic;
		n_reset		: out std_ulogic;
		n_modes		: out std_ulogic;
		a_mem_h		: out std_ulogic_vector(14 downto 13);
		a_mem_l		: out std_ulogic_vector(8 downto 0);
		d_mem_i		: in  std_ulogic_vector(7 downto 0);
		a_cpu			: in  std_ulogic_vector(15 downto 0);
		d_cpu_i		: in  std_ulogic_vector(7 downto 0);
		d_cpu_o		: out std_ulogic_vector(7 downto 0);
		oe_cpu		: out boolean;
		oe_mem		: out boolean;
		n_m1			: in  std_ulogic;
		n_mreq		: in  std_ulogic;
		n_iorq		: in  std_ulogic;
		n_wr			: in  std_ulogic;
		n_rd			: in  std_ulogic;
		n_rfsh		: in  std_ulogic;
		n_nmi			: out std_ulogic;
		n_halt		: in  std_ulogic;
		n_wait		: out std_ulogic;
		n_romcs		: out std_ulogic;
		n_ramcs		: out std_ulogic;
		kbd_col		: in  std_ulogic_vector(4 downto 0);
		usa_uk		: in  std_ulogic;
		video			: out std_ulogic;
		outhsync		: out std_ulogic;
		outvsync		: out std_ulogic;
		n_sync		: out std_ulogic;
		config		: in  std_ulogic_vector(3 downto 0);
		tape_in		: in  std_ulogic
	);
end;

-- the description of the logic --------------------------

architecture beh of top is

  signal i_n_reset:    std_ulogic;
  signal i_n_modes:    std_ulogic;
  signal mode_reset:   boolean;
  signal mode_v_inv:   boolean;
  signal mode_chr13:   std_ulogic;
  signal mode_ram:     std_ulogic_vector(1 downto 0);
  signal vsync:        boolean;
  signal nmi_enable:   boolean;
  signal fake_cpu:     boolean;
  signal video_addr:   std_ulogic_vector(8 downto 0);
  signal video_mem:    boolean;
  signal i_video:      std_ulogic;
  signal s_hsync:      std_ulogic;
  signal s_vsync:      std_ulogic;
  signal i_n_sync:     std_ulogic;
  signal d_kbd:        std_ulogic_vector(7 downto 0);
  signal d_kbd_enable: boolean;

begin

	c_res_clk : entity work.res_clk
	port map (
		clock			=> clock,
		mode_reset	=> mode_reset,
		n_reset		=> i_n_reset,
		n_modes		=> i_n_modes,
		clock_2		=> clock_2
	);

	c_modes97 : entity work.modes97
	port map (
		n_reset			=> i_n_reset,
		n_modes			=> i_n_modes,
		phi				=> phi,
		config			=> config,
		addr				=> a_cpu,
		data				=> d_cpu_i,
		n_mreq			=> n_mreq,
		n_wr				=> n_wr,
		mode_reset		=> mode_reset,
		mode_v_inv		=> mode_v_inv,
		mode_chr13		=> mode_chr13,
		mode_ram			=> mode_ram
	);


	c_video81 : entity work.video81
	generic map (
		synchronous	=> synchronous
	)
	port map (
		clock				=> clock,
		phi				=> phi,
		nmi_enable		=> nmi_enable,
		n_nmi				=> n_nmi,
		n_halt			=> n_halt,
		n_wait			=> n_wait,
		n_m1				=> n_m1,
		n_mreq			=> n_mreq,
		n_iorq			=> n_iorq,
		vsync				=> vsync,
		a_cpu				=> a_cpu(15 downto 13),
		d_mem				=> d_mem_i,
		fake_cpu			=> fake_cpu,
		mode_chr13		=> mode_chr13,
		video_addr		=> video_addr,
		video_mem		=> video_mem,
		mode_v_inv		=> mode_v_inv,
		video				=> i_video,
		ohsync			=> s_hsync,
		ovsync			=> s_vsync,
		n_sync			=> i_n_sync
	);

	c_io81 : entity work.io81
	port map (
		n_reset			=> i_n_reset,
		addr				=> a_cpu(1 downto 0),
		n_iorq			=> n_iorq,
		n_wr				=> n_wr,
		n_rd				=> n_rd,
		vsync				=> vsync,
		nmi_enable		=> nmi_enable,
		kbd_col			=> kbd_col,
		usa_uk			=> usa_uk,
		tape_in			=> tape_in,
		d_kbd				=> d_kbd,
		d_kbd_enable	=> d_kbd_enable
	);

 	c_busses: entity work.busses
	port map (
		mode_ram			=> mode_ram,
		a_cpu				=> a_cpu,
		video_addr		=> video_addr,
		video_mem		=> video_mem,
		a_mem_h			=> a_mem_h,
		a_mem_l			=> a_mem_l,
		fake_cpu			=> fake_cpu,
		d_kbd				=> d_kbd,
		d_kbd_enable	=> d_kbd_enable,
		d_mem_i			=> d_mem_i,
		d_cpu_o			=> d_cpu_o,
		oe_cpu			=> oe_cpu,
		oe_mem			=> oe_mem,
		n_m1				=> n_m1,
		n_mreq			=> n_mreq,
		n_iorq			=> n_iorq,
		n_wr				=> n_wr,
		n_rd				=> n_rd,
		n_rfsh			=> n_rfsh,
		n_romcs			=> n_romcs,
		n_ramcs			=> n_ramcs
	);

	n_reset <= i_n_reset;
	n_modes <= i_n_modes;

	video  <= i_video;
	n_sync <= i_n_sync;
  
	outhsync <= s_hsync;
	outvsync <= s_vsync;

end;

-- end ---------------------------------------------------
