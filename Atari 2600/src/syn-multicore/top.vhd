-- Port of the A2601 FPGA implementation for the Turbo Chameleon

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- -----------------------------------------------------------------------

entity top is
	port (
	
		-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);
		btn_oe_n_i			: in    std_logic;
		btn_clr_n_i			: in    std_logic;

		-- SRAM (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_ce_n_o			: out   std_logic_vector(1 downto 0)	:= (others => '1');
		sram_oe_n_o			: out   std_logic								:= '1';

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joystick
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p7_o			: out   std_logic								:= '1';
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p7_o			: out   std_logic								:= '1';
		joy2_p9_i			: in    std_logic;

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
--		hdmi_d_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
--		hdmi_clk_o			: out   std_logic								:= '0';
--		hdmi_cec_o			: out   std_logic								:= '0';

		-- Debug
		leds_n_o				: out   std_logic_vector(7 downto 0)	:= (others => '1')
		
	);
end entity;

-- -----------------------------------------------------------------------

architecture rtl of top is
	type state_t is (TEST_IDLE, TEST_FILL, TEST_FILL_W, TEST_CHECK, TEST_CHECK_W, TEST_ERROR);
	
-- System clocks
	signal vid_clk: std_logic := '0';
	signal sysclk : std_logic := '0';
	signal ena_1mhz : std_logic := '0';
	signal ena_1khz : std_logic := '0';

	signal reset_button_n : std_logic := '0';
	
-- Global signals
	signal reset : std_logic := '0';
	signal end_of_pixel : std_logic := '0';

-- RAM Test
	signal state : state_t := TEST_IDLE;
	signal noise_bits : unsigned(7 downto 0) := (others => '0');
	

-- 4 Port joystick adapter
	signal video_joystick_shift_reg : std_logic := '0';

-- LEDs
	signal led_green : std_logic := '0';
	signal led_red : std_logic := '0';

-- VGA
	signal currentX : unsigned(11 downto 0) := (others => '0');
	signal currentY : unsigned(11 downto 0) := (others => '0');
	signal hsync : std_logic := '0';
	signal vsync : std_logic := '0';
	
	signal red_reg : unsigned(4 downto 0) := (others => '0');
	signal grn_reg : unsigned(4 downto 0) := (others => '0');
	signal blu_reg : unsigned(4 downto 0) := (others => '0');
	
-- Sound
	signal sigma_l : std_logic := '0';
	signal sigma_r : std_logic := '0';
	signal sigmaL_reg : std_logic := '0';
	signal sigmaR_reg : std_logic := '0';

-- Docking station
	signal docking_ena : std_logic := '0';
	signal docking_irq : std_logic := '0';
	signal irq_n : std_logic := '0';
	
	signal docking_joystick1 : unsigned(5 downto 0) := (others => '0');
	signal docking_joystick2 : unsigned(5 downto 0) := (others => '0');
	signal docking_joystick3 : unsigned(5 downto 0) := (others => '0');
	signal docking_joystick4 : unsigned(5 downto 0) := (others => '0');
	
-- A2601
	signal audio: std_logic := '0';
   signal O_VSYNC: std_logic := '0';
   signal O_HSYNC: std_logic := '0';
	signal O_VIDEO_R: std_logic_vector(2 downto 0) := (others => '0');
	signal O_VIDEO_G: std_logic_vector(2 downto 0) := (others => '0');
	signal O_VIDEO_B: std_logic_vector(2 downto 0) := (others => '0');			
	signal res: std_logic := '0';
	signal p_l: std_logic := '0';
	signal p_r: std_logic := '0';
	signal p_a: std_logic := '0';
	signal p_u: std_logic := '0';
	signal p_d: std_logic := '0';
	signal p2_l: std_logic := '0';
	signal p2_r: std_logic := '0';
	signal p2_a: std_logic := '0';
	signal p2_u: std_logic := '0';
	signal p2_d: std_logic := '0';
	signal p_s: std_logic := '0';
	signal p_bs: std_logic;
	signal LED: std_logic_vector(2 downto 0);
	signal I_SW : std_logic_vector(2 downto 0) := (others => '0');
--	signal JOYSTICK_GND: std_logic;
--	signal JOYSTICK2_GND: std_logic;


begin

-- -----------------------------------------------------------------------
-- A2601 core
-- -----------------------------------------------------------------------
	a2601Instance : entity work.A2601NoFlash
		port map (
			vid_clk => vid_clk,
			audio => audio,
         O_VSYNC => O_VSYNC,
         O_HSYNC => O_HSYNC,
			O_VIDEO_R => O_VIDEO_R,
			O_VIDEO_G => O_VIDEO_G,
			O_VIDEO_B => O_VIDEO_B,
         res => res,
         p_l => p_l,
         p_r => p_r,
         p_a => p_a,
         p_u => p_u,
         p_d => p_d,
         p2_l => p2_l,
         p2_r => p2_r,
         p2_a => p2_a,
         p2_u => p2_u,
         p2_d => p2_d,
         p_s => p_s,
         p_bs => open,
			LED => leds_n_o(2 downto 0),
			I_SW => I_SW,
         JOYSTICK_GND => open,
			JOYSTICK2_GND => open
		);

	--process(red_reg, grn_reg, blu_reg, O_VIDEO_R, O_VIDEO_G, O_VIDEO_B, O_HSYNC, O_VSYNC, audio)
	--begin
		--if SW(9)='0' then
			-- VGA test
		--	red <= red_reg(3 downto 0);
		--	grn <= grn_reg(3 downto 0);
		--	blu <= blu_reg(3 downto 0);
		--	nHSync <= hsync;
		--	nVSync <= vsync;
		--	sigmaL <= sigmaL_reg;
		--	sigmaR <= sigmaR_reg;
		--else
			-- A2601
			vga_r_o <= O_VIDEO_R(2 downto 0);-- & "0";
			vga_g_o <= O_VIDEO_G(2 downto 0);-- & "0";
			vga_b_o <= O_VIDEO_B(2 downto 0);-- & "0";
			vga_hsync_n_o <= not O_HSYNC; --not O_HSYNC;
			vga_vsync_n_o <= not O_VSYNC; --not O_VSYNC;
			dac_l_o <= audio;
			dac_r_o <= audio;
	--	end if;
	--end process;
			
	res <= '0';
--	p_l <= reset_button_n;
--	p_r <= freeze_n;
--	p_u <= '1';
--	p_d <= '1';

	p_u <= joy1_up_i;
	p_d <= joy1_down_i;
	p_l <= joy1_left_i;
	p_r <= joy1_right_i;
	p_a <= joy1_p6_i;

	
	p2_u <= joy2_up_i;
	p2_d <= joy2_down_i;
	p2_l <= joy2_left_i;
	p2_r <= joy2_right_i;
	p2_a <= joy2_p6_i;

	p_s  <= not btn_n_i(1); --reset switch

	
	I_SW(2) <= not btn_n_i(2); -- select
	I_SW(1) <= not btn_n_i(3); -- difficult?
	I_SW(0) <= not btn_n_i(4); -- difficult?
	
	--LED => LEDG;
	
	-- JOYSTICK_GND <= '0';
	-- JOYSTICK2_GND <= '0';

	
-- -----------------------------------------------------------------------
-- Clocks and PLL
-- -----------------------------------------------------------------------
	pllInstance : entity work.pll1
		port map (
			inclk0 => clock_50_i,
			c0 => sysclk,
			c1 => vid_clk
		);
		

	

end architecture;
