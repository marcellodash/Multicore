-- A2601 Top Level Entity (ROM stored in on-chip RAM)
-- Copyright 2006, 2010 Retromaster
--
--  This file is part of A2601.
--
--  A2601 is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License,
--  or any later version.
--
--  A2601 is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with A2601.  If not, see <http://www.gnu.org/licenses/>.
--

-- This top level entity supports a single cartridge ROM stored in FPGA built-in
-- memory (such as Xilinx Spartan BlockRAM). To generate the required cart_rom
-- entity, use bin2vhdl.py found in the util directory.
--
-- For more information, see the A2601 Rev B Board Schematics and project
-- website at <http://retromaster.wordpress.org/a2601>.

library std;
use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity A2601NoFlash is
   port 
	(
			vid_clk: in std_logic;
			
			audio: out std_logic;		
         pcm_audio: out std_logic_vector(4 downto 0);
			
			colu : out std_logic_vector(6 downto 0);
			Hsyn : out std_logic;			
			Vsyn : out std_logic;			

         res: in std_logic;
         p_l: in std_logic;
         p_r: in std_logic;
         p_a: in std_logic;
         p_u: in std_logic;
         p_d: in std_logic;
         p2_l: in std_logic;
         p2_r: in std_logic;
         p2_a: in std_logic;
         p2_u: in std_logic;
         p2_d: in std_logic;
			
			paddle_0: in std_logic_vector(7 downto 0);
         paddle_1: in std_logic_vector(7 downto 0);
         paddle_2: in std_logic_vector(7 downto 0);
         paddle_3: in std_logic_vector(7 downto 0);
			paddle_ena: in std_logic;
			
         p_s: in std_logic;
         p_bs: out std_logic;			
			LED: out std_logic_vector(2 downto 0);
			
			I_SW :in std_logic_vector(3 downto 0);
			
			
         JOYSTICK_GND: out std_logic			;
			JOYSTICK2_GND: out std_logic	;
			
			cart_a : out std_logic_vector(13 downto 0);
			cart_d : in std_logic_vector(7 downto 0);
			bs_method : in std_logic_vector(7 downto 0);
			
			oLed : out std_logic_vector(2 downto 0);
			clk_tia: out std_logic;
			clk_tiax2: out std_logic;
			clk_tiax4: out std_logic;
			
			clk_dpc : in std_logic
			
			);
end A2601NoFlash;

architecture arch of A2601NoFlash is

    component A2601 is
    port(
			vid_clk: in std_logic;
         rst: in std_logic;
         d: inout std_logic_vector(7 downto 0);
         a: out std_logic_vector(12 downto 0);
         r: out std_logic;
         pa: inout std_logic_vector(7 downto 0);
         pb: inout std_logic_vector(7 downto 0);
         paddle_0: in std_logic_vector(7 downto 0);
         paddle_1: in std_logic_vector(7 downto 0);
         paddle_2: in std_logic_vector(7 downto 0);
         paddle_3: in std_logic_vector(7 downto 0);
         paddle_ena: in std_logic;
         inpt4: in std_logic;
         inpt5: in std_logic;
         colu: out std_logic_vector(6 downto 0);
         csyn: out std_logic;
         vsyn: out std_logic;
         hsyn: out std_logic;
         rgbx2: out std_logic_vector(23 downto 0);
         cv: out std_logic_vector(7 downto 0);
         au0: out std_logic;
         au1: out std_logic;
         av0: out std_logic_vector(3 downto 0);
         av1: out std_logic_vector(3 downto 0);
         ph0_out: out std_logic;
         ph1_out: out std_logic;
         pal: in std_logic;
			clk_tia: out std_logic;
			clk_tiax2: out std_logic;
			clk_tiax4: out std_logic
			);
    end component;

	COMPONENT VGA_SCANDBL
	PORT(
		I_R : IN std_logic_vector(2 downto 0);
		I_G : IN std_logic_vector(2 downto 0);
		I_B : IN std_logic_vector(1 downto 0);
		I_HSYNC : IN std_logic;
		I_VSYNC : IN std_logic;
		CLK : IN std_logic;
		CLK_X2 : IN std_logic;          
		O_R : OUT std_logic_vector(2 downto 0);
		O_G : OUT std_logic_vector(2 downto 0);
		O_B : OUT std_logic_vector(1 downto 0);
		O_HSYNC : OUT std_logic;
		O_VSYNC : OUT std_logic
		);
	END COMPONENT;	
	
	 component dac is
	 port(DACout: 	out std_logic;
			DACin:	in std_logic_vector(4 downto 0);
			Clk:		in std_logic;
			Reset:	in std_logic);
	 end component;	
	 
   component ram128x8 is
        port(clk: in std_logic;
             r: in std_logic;
             d_in: in std_logic_vector(7 downto 0);
             d_out: out std_logic_vector(7 downto 0);
             a: in std_logic_vector(6 downto 0));
    end component;

    component CartTable is
        port(clk: in std_logic;
             d: out std_logic_vector(10 downto 0);
             c: out std_logic_vector(6 downto 0);
             a: in std_logic_vector(6 downto 0));
    end component;
    
    signal d: std_logic_vector(7 downto 0);
    --signal cpu_d: std_logic_vector(7 downto 0);
    signal a: std_logic_vector(13 downto 0);
    signal pa: std_logic_vector(7 downto 0);
    signal pb: std_logic_vector(7 downto 0);
    signal inpt4: std_logic;
    signal inpt5: std_logic;
    signal au0: std_logic;
    signal au1: std_logic;
    signal av0: std_logic_vector(3 downto 0);
    signal av1: std_logic_vector(3 downto 0);

    signal audio_0: std_logic_vector(3 downto 0);
    signal audio_1: std_logic_vector(3 downto 0);

    signal rst: std_logic := '1';
    signal sys_clk_dvdr: unsigned(4 downto 0) := "00000";

    signal ph0: std_logic;
    signal ph1: std_logic;
	 
    signal rgbx2: std_logic_vector(23 downto 0);
    --signal hsyn: std_logic;
   -- signal vsyn: std_logic;
	 
	 signal ctrl_cntr: unsigned(3 downto 0);
    signal p_fn: std_logic;

		signal rst_cntr: unsigned(12 downto 0) := "0000000000000";
		   signal sc_clk: std_logic;
    signal sc_r: std_logic;
    signal sc_d_in: std_logic_vector(7 downto 0);
    signal sc_d_out: std_logic_vector(7 downto 0);
    signal sc_a: std_logic_vector(6 downto 0);

    subtype bss_type is std_logic_vector(2 downto 0);


    signal bank: std_logic_vector(3 downto 0) := "0000";
    signal tf_bank: std_logic_vector(1 downto 0);
    signal e0_bank: std_logic_vector(2 downto 0);
    signal e0_bank0: std_logic_vector(2 downto 0) := "000";
    signal e0_bank1: std_logic_vector(2 downto 0) := "000";
    signal e0_bank2: std_logic_vector(2 downto 0) := "000";

    signal cpu_a: std_logic_vector(12 downto 0);
    signal cpu_d: std_logic_vector(7 downto 0);
    signal cpu_r: std_logic;

    signal cart_info: std_logic_vector(10 downto 0) := "00000000000";
    signal cart_cntr: unsigned(6 downto 0) := "0000000";
    signal cart_max: std_logic_vector(6 downto 0);
    signal cart_vect: std_logic_vector(6 downto 0) := "0000000";

    signal cart_next: std_logic;
    signal cart_prev: std_logic;
    signal cart_swch: std_logic := '0';
    signal cart_next_l: std_logic;
    signal cart_prev_l: std_logic;
	 signal sel: std_logic;
--	 signal ctrl_cntr: unsigned(3 downto 0);
	--tmp
	signal cv:  std_logic_vector(7 downto 0);
	signal pcm_audio_s:  std_logic_vector(4 downto 0);
-- switches
	signal sw_toggle: std_logic_vector(3 downto 0) := "0111";
	
	signal sw_pressed: std_logic_vector(2 downto 0) := "000";
	
    constant BANK00: bss_type := "000";
    constant BANKF8: bss_type := "001";
    constant BANKF6: bss_type := "010";
    constant BANKFE: bss_type := "011";
    constant BANKE0: bss_type := "100";
    constant BANK3F: bss_type := "101";
    constant BANKF4: bss_type := "110";
    constant BANKP2: bss_type := "111";
	 
    signal bss: bss_type := BANK00; 	--bank switching method
    signal sc: std_logic := '0';		--superchip enabled or not
	 
     signal FE_latch: std_logic := '0';	
	  
	  --- DPC signals
	  type B3_type is array(2 downto 0) of std_logic_vector(7 downto 0);
	  type B8_type is array(0 to 7) of std_logic_vector(7 downto 0);
	  type B8_11_type is array(7 downto 0) of std_logic_vector(10 downto 0);
	  
	  signal soundAmplitudes : B8_type := (
						0 => x"00",
						1 => x"04",
						2 => x"05",
						3 => x"09",
						4 => x"06",
						5 => x"0a",
						6 => x"0b",
						7 => x"0f"
						);


		signal DpcMusicModes : B3_type := (	others => (others=>'0'));		
		signal DpcMusicFlags : B3_type := (	others => (others=>'0'));		
				
				
		signal DpcTops : B8_type := (	others => (others=>'0'));		
		signal DpcBottoms : B8_type := (	others => (others=>'0'));		
		signal DpcFlags : B8_type := (	others => (others=>'0'));		


		signal DpcCounters : B8_11_type := (	others => (others=>'0'));		
		
		signal DpcRandom	: std_logic_vector(7 downto 0) := x"01";
		signal DpcWrite	: std_logic := '0';
		
		signal DpcClocks : unsigned(15 downto 0) := (others=>'0');
		
		signal clk_music : unsigned(3 downto 0) := (others=>'0');	 -- 3 é o melhor
begin
	  
   -- ms_A2601: A2601
   --     port map(vid_clk, rst, cpu_d, cpu_a, cpu_r,pa, pb, 
	--			paddle_0, paddle_1, paddle_2, paddle_3, paddle_ena, 
	--			inpt4, inpt5, colu, open, vsyn, hsyn, rgbx2, cv, 
	--			au0, au1, av0, av1, ph0, ph1, '0', clk_tia, clk_tiax2);
				
    ms_A2601: work.A2601
    port map
	 (
		vid_clk		=> vid_clk,
		rst			=> rst,
		d				=> cpu_d,
		a				=> cpu_a,
		r				=> cpu_r,
		pa				=> pa,
		pb				=> pb,
		paddle_0		=> paddle_0,
		paddle_1		=> paddle_1,
		paddle_2		=> paddle_2,
		paddle_3		=> paddle_3,
		paddle_ena	=> paddle_ena,
		inpt4			=> inpt4,
		inpt5			=> inpt5,
		colu			=> colu,
		csyn			=> open,
		vsyn			=> vsyn,
		hsyn			=> hsyn,
		rgbx2			=> rgbx2,
		cv				=> cv,
		audio_0		=> audio_0,
		audio_1		=> audio_1,
		ph0_out		=> ph0,
		ph1_out		=> ph1,
		pal			=> '0',
		clk_tia		=> clk_tia,
		clk_tiax2	=> clk_tiax2,
		clk_tiax4	=> clk_tiax4
		);			
  
--	Inst_cart_rom: entity work.cart_riverraid PORT MAP(
--		clock => vid_clk,
--		q => d,
--		address => a(11 downto 0)
--	);		 

	--brd_CartTable: CartTable
   --     port map(ph0, cart_info, cart_max, std_logic_vector(cart_cntr));
	
	cart_a <= a;
	d <= cart_d;
	
	dac_inst: dac 
		port map(audio, pcm_audio_s, vid_clk, '0');	

	
 --     O_VIDEO_R(2 downto 0) <= rgbx2(23 downto 21);
  --    O_VIDEO_G(2 downto 0) <= rgbx2(15 downto 13);
   --   O_VIDEO_B(2 downto 0) <= rgbx2(7 downto 5);	
    --  O_HSYNC   <= hsyn;
     -- O_VSYNC   <= vsyn;	

--    cpu_d <= d when a(12) = '1' else "ZZZZZZZZ";
 process(ph0)
    begin
        if (ph0'event and ph0 = '1') then
			if res = '1' then
			rst <= '1';
			else
			rst <= '0';
			end if;
		end if;
            
    end process;

    -- Controller inputs sampling
    p_bs <= '0'; --- led0 ctrl_cntr(3);

    -- Only one controller port supported. -- FIXME -- does 2nd port work ?
    -- pa(3 downto 0) <= "1111";
	 
    process(ph0)
    begin
        if (ph0'event and ph0 = '1') then
            ctrl_cntr <= ctrl_cntr + 1;
            if (ctrl_cntr = "1111") then    -- p_bs
                p_fn <=  p_a;
                pb(0) <= not p_s; 
            elsif (ctrl_cntr = "0111") then
                pa(7 downto 4) <= p_r & p_l & p_d & p_u;
					 pa(3 downto 0) <= p2_r & p2_l & p2_d & p2_u;
                inpt4 <= p_a;
					 inpt5 <= p2_a;
					 
					 --switches
					 for i in 0 to 2
					 loop
						if I_SW(i) = '1' then
							if sw_pressed(i) = '0' then
								sw_pressed(i) <= '1';
								sw_toggle(i)  <= NOT(sw_toggle(i));
							end if;
						else
							sw_pressed(i) <= '0';
						end if;
					end loop;
					 sw_toggle(3) <= NOT(I_SW(3)); --momentary, not slide toggle -- should this be active low ?
            end if;

           -- pb(7) <= pa(7) or p_fn; --sw1
           -- pb(6) <= pa(6) or p_fn; --sw2
           -- pb(1) <= pa(4) or p_fn; --select
           -- pb(3) <= pa(5) or p_fn; --b/w / colour
        end if;
    end process;
	 pb(7) <= sw_toggle(0); -- p2/right difficulty
	 pb(6) <= sw_toggle(1); --p1/left difficulty
	 pb(1) <= sw_toggle(3);  --select 
	 pb(3) <= sw_toggle(2);  ---- b/w / colour
    pb(5) <= '1'; --nc ?
    pb(4) <= '1'; --nc
    pb(2) <= '1'; --nc
	 
    -- inpt5 <= '1'; --FIXME --remove line if 2p works
	 -- leds to show state of switches
	 -- led1 difficulty switch 1 on/off
	 -- led2 difficulty switch 2 on/off
	 -- led3 colour switch on/off
	 LED(1 downto 0) <= sw_toggle(1 downto 0);
	 LED(2) <= sw_toggle(2); --active low;
	 
	 JOYSTICK_GND <= '0';
	 JOYSTICK2_GND <= '0';
	 
    --auv0 <= ("0" & unsigned(av0)) when (au0 = '1') else "00000";
    --auv1 <= ("0" & unsigned(av1)) when (au1 = '1') else "00000";

    pcm_audio_s <= std_logic_vector(unsigned('0' & audio_0) + unsigned('0' & audio_1));
    pcm_audio <= pcm_audio_s;

    --process(vid_clk, sys_clk_dvdr)
    --begin
     --   if (vid_clk'event and vid_clk = '1') then
     --       sys_clk_dvdr <= sys_clk_dvdr + 1;
     --       if (sys_clk_dvdr = "11101") then
     --           rst <= '0';
     --       end if;
     --   end if;
    --end process;

    sc_ram128x8: ram128x8
        port map(sc_clk, sc_r, sc_d_in, sc_d_out, sc_a);

    -- This clock is phase shifted so that we can use Xilinx synchronous block RAM.
    sc_clk <= not ph1;
    sc_r <= '0' when cpu_a(12 downto 7) = "100000" else '1';
    sc_d_in <= cpu_d;
    sc_a <= cpu_a(6 downto 0);

	 
	 --with bss select 
-- 
-- a <=
--
--	  "00" 					& cpu_a(11 downto 0) when BANK00,
--     
--	  '0' & bank(0) 		& cpu_a(11 downto 0) when BANKF8,
--    
--	  bank(1 downto 0) 	& cpu_a(11 downto 0) when BANKF6,
--	  
--	  '0' & bank(0) 		& cpu_a(11 downto 0) when BANKP2,
-- 
--	  '0' & bank(0) 		& cpu_a(11 downto 0) when BANKFE,
--    
--	  "0" & e0_bank 		& cpu_a(9 downto 0)  when BANKE0,
-- 
--	  '0' & tf_bank 		& cpu_a(10 downto 0) when BANK3F,
--     
--	  "--------------" when others;

		 process(bss, DpcCounters, cpu_a, bank, e0_bank, tf_bank)
		 begin
		 
				if(bss = BANK00) then
					a <=	  "00" & cpu_a(11 downto 0) ;
					
				 elsif(bss = BANKF8) then
					a <=		  '0' & bank(0) & cpu_a(11 downto 0);
					
				elsif(bss = BANKF6) then
					a <=	  bank(1 downto 0) & cpu_a(11 downto 0);
					
				elsif(bss = BANKP2 and cpu_a >= "1" & x"008" and cpu_a <= "1" & x"017") then
					a <=  "100" & std_logic_vector(2047 - DpcCounters(to_integer(unsigned(cpu_a(2 downto 0))))(10 downto 0));
					
				elsif(bss = BANKP2) then
					a <= '0' & bank(0) & cpu_a(11 downto 0);
					
				elsif(bss = BANKFE) then
					a <= '0' & bank(0) & cpu_a(11 downto 0);
					
				elsif(bss = BANKE0) then
					a <= "0" & e0_bank & cpu_a(9 downto 0);
					
				elsif(bss = BANK3F) then
					a <= '0' & tf_bank & cpu_a(10 downto 0);
					
				else
					a <= (others=>'Z');
					
				end if;
				
		end process;	
		
    -- ROM and SC output
    process(cpu_a, d, sc_d_out, sc, DpcMusicModes, DpcMusicFlags, DpcFlags, DpcRandom, soundAmplitudes, bss)
	 variable ampI_v :std_logic_vector(2 downto 0); 
	 variable masked0_v :std_logic_vector(7 downto 0); 
	 variable masked1_v :std_logic_vector(7 downto 0); 
	 variable masked2_v :std_logic_vector(7 downto 0); 
	 variable newlow_v : integer;
    begin

			if (bss = BANKP2 and cpu_a >= "1" & x"008" and cpu_a <= "1" & x"00f")  then -- DPC READ - 0x1008 to 0x100f (read graphics from extra 2kb)
					cpu_d <= d;

			elsif (bss = BANKP2 and cpu_a >= "1" & x"010" and cpu_a <= "1" & x"017")  then -- DPC READ - 0x1010 to 0x1017 (read graphics from extra 2kb ANDed)
					cpu_d <= d and DpcFlags(to_integer(unsigned(cpu_a(2 downto 0))));
										
			elsif (bss = BANKP2 and cpu_a >= "1" & x"000" and cpu_a <= "1" & x"003") then -- DPC READ - 0x1000 to 0x1003 (random number)
					cpu_d <= DpcRandom;
													
			elsif (bss = BANKP2 and cpu_a >= "1" & x"004" and cpu_a <= "1" & x"007") then -- DPC READ - 0x1004 to 0x1007 (Sound)
									
					ampI_v := "000";
					
		--			masked0_v := DpcMusicModes(0) and DpcMusicFlags(0);
		--			if (masked0_v /= x"00") then ampI_v(0) := '1'; end if;
		--			
		--			masked1_v := DpcMusicModes(1) and DpcMusicFlags(1);
		--			if (masked1_v /= x"00") then ampI_v(1) := '1'; end if;
		--			
		--			masked2_v := DpcMusicModes(2) and DpcMusicFlags(2);
		--			if (masked2_v /= x"00") then ampI_v(2) := '1'; end if;
					
					
					if DpcMusicModes(0)(4) = '1' and DpcMusicFlags(0)(4) = '1' then ampI_v(0) := '1'; end if;
					if DpcMusicModes(1)(4) = '1' and DpcMusicFlags(1)(4) = '1' then ampI_v(1) := '1'; end if;
					if DpcMusicModes(2)(4) = '1' and DpcMusicFlags(2)(4) = '1' then ampI_v(2) := '1'; end if;
					
																
					cpu_d <= soundAmplitudes(to_integer(unsigned(ampI_v)));
															
			elsif (bss = BANKP2 and cpu_a >= "1" & x"038" and cpu_a <= "1" & x"03f") then -- DPC READ -  0x1038 to 0x103f (Flags)
					cpu_d <= DpcFlags(to_integer(unsigned(cpu_a(2 downto 0))));

			elsif (cpu_a(12 downto 7) = "100001" and sc = '1') then
					cpu_d <= sc_d_out;
				
			elsif (cpu_a(12 downto 7) = "100000" and sc = '1') then
					cpu_d <= "ZZZZZZZZ";
				
			elsif (cpu_a(12) = '1') then
					cpu_d <= d;
				
			else
					cpu_d <= "ZZZZZZZZ";
				
			end if;
			  
    end process;

    with cpu_a(11 downto 10) select e0_bank <=
        e0_bank0 when "00",
        e0_bank1 when "01",
        e0_bank2 when "10",
        "111" when "11",
        "---" when others;

    tf_bank <= bank(1 downto 0) when (cpu_a(11) = '0') else "11";

	  
		  
    bankswch: process(ph0)
	 variable w_index_v :integer; 
	 variable addr_v :std_logic_vector(12 downto 0); 
    begin
        if (ph0'event and ph0 = '1') then
            if (rst = '1') then
                bank <= "0000";
                e0_bank0 <= "000";
                e0_bank1 <= "000";
                e0_bank2 <= "000";
					 DpcRandom <= x"01";
            else
                case bss is
					 
                    when BANKF8 =>
                        if (cpu_a = "1" & X"FF8") then
                            bank <= "0000";
                        elsif (cpu_a = "1" & X"FF9") then
                            bank <= "0001";
                        end if;
								
                    when BANKF6 =>
                        if (cpu_a = "1" & X"FF6") then
                            bank <= "0000";
                        elsif (cpu_a = "1" & X"FF7") then
                            bank <= "0001";
                        elsif (cpu_a = "1" & X"FF8") then
                            bank <= "0010";
                        elsif (cpu_a = "1" & X"FF9") then
                            bank <= "0011";
                        end if;
								
							when BANKF4 =>
                        if (cpu_a = "1" & X"FF4") then
                            bank <= "0000";
                        elsif (cpu_a = "1" & X"FF5") then
                            bank <= "0001";
                        elsif (cpu_a = "1" & X"FF6") then
                            bank <= "0010";
                        elsif (cpu_a = "1" & X"FF7") then
                            bank <= "0011";
                        elsif (cpu_a = "1" & X"FF8") then
                            bank <= "0100";
                        elsif (cpu_a = "1" & X"FF9") then
                            bank <= "0101";
                        elsif (cpu_a = "1" & X"FFA") then
                            bank <= "0110";
                        elsif (cpu_a = "1" & X"FFB") then
                            bank <= "0111";
                        end if;
								
								
										
-------------------------------------------------
				   -- DPC - included by Victor Trucco - 25/05/2018	
					when BANKP2 =>
					
						if cpu_a /= addr_v then -- single execution for each cpu address
							addr_v	:= cpu_a;

								if (cpu_a(12) = '1' ) then -- A12 - HIGH

										w_index_v := to_integer(unsigned(cpu_a(2 downto 0)));
																		
										
										if (cpu_a(12 downto 6) = "1000000") then -- DPC READ - 0x1000 to 0x103F (1 0000 0000 0000 to 1 0000 0011 1111)
										

												-- Update flag register for selected data fetcher
												if (DpcCounters(w_index_v)(7 downto 0) = DpcTops(w_index_v)) then
														
														DpcFlags(w_index_v) <= x"ff";
														
												elsif (DpcCounters(w_index_v)(7 downto 0) = DpcBottoms(w_index_v)) then
													
														DpcFlags(w_index_v) <= x"00";
													
												end if;
							
										
												case cpu_a(5 downto 3)  is
														
														when "000" =>  -- 0x1000 to 0x1007 - random number and music fetcher
														
																if(cpu_a(2) = '0') then -- 0x1000 to 0x1003
																		-- random number read
																		DpcRandom <= DpcRandom(6 downto 0) & (not(DpcRandom(7) xor DpcRandom(5) xor DpcRandom(4) xor DpcRandom(3)));
																		--resultDPC <= DpcRandom;
																
															--	else -- 0x1004 to 0x1007
																
																		-- sound
																		
																		--ampI_v := "000";
																		--
																		--masked0_v := DpcMusicModes(0) and DpcMusicFlags(0);
																		--if (masked0_v /= x"00") then ampI_v(0) := '1'; end if;
																		--
																		--masked1_v := DpcMusicModes(1) and DpcMusicFlags(1);
																		--if (masked1_v /= x"00") then ampI_v(1) := '1'; end if;
																		--
																		--masked2_v := DpcMusicModes(2) and DpcMusicFlags(2);
																		--if (masked2_v /= x"00") then ampI_v(2) := '1'; end if;
		--
																		--resultDPC <= soundAmplitudes(to_integer(unsigned(ampI_v)));
																		
																end if;
														
										--				when "001" =>  -- 0x1008 to 0x100f - Graphics read
										--						DpcDisplayPtr <= "100" & std_logic_vector(2047 - DpcCounters(w_index_v)(10 downto 0));
										--			
										--				
										--				when "010" =>  -- 0x1010 to 0x1017 - Graphics read (ANDed with flag)
										--						DpcDisplayPtr <= "100" & std_logic_vector(2047 - DpcCounters(w_index_v)(10 downto 0));-- and DpcFlags(w_index_v));
											
										--				
										--				when "111" =>  -- 0x1038 to 0x103f - Return the current flag value
										--						resultDPC <= DpcFlags(w_index_v);
														
														when others => NULL;
												end case;
												
												-- Clock the selected data fetcher's counter if needed
												if (w_index_v < 5 or (w_index_v >= 5 and DpcMusicModes(5 - w_index_v)(4) = '1')) then
													DpcCounters(w_index_v) <= DpcCounters(w_index_v) - 1;
												end if;
										
										
										
										
										
										elsif (cpu_a(12 downto 6) = "1000001") then -- DPC WRITE - 0x1040 to 0x107F (1 0000 0100 0000 to 1 0000 0111 1111)
										
													case cpu_a(5 downto 3)  is
											
															when "000" => --0x1040 to 0x1047
																	-- DFx top count
																	DpcTops(w_index_v) <= cpu_d;
																	DpcFlags(w_index_v) <= x"00";

															when "001" => -- 0x1048 to 0x104F
																	-- DFx bottom count
																	DpcBottoms(w_index_v) <= cpu_d;

															when "010" => -- 0x1050 to 0x1057
																	-- DFx counter low
																	DpcCounters(w_index_v)(7 downto 0) <= cpu_d;

															when "011" => -- 0x1058 to 105F
																	-- DFx counter high 
																	DpcCounters(w_index_v)(10 downto 8) <= cpu_d(2 downto 0);
																	
																	if(w_index_v >= 5) then -- 0x105D to 0x105F
																		DpcMusicModes(5 - w_index_v) <= "000" & cpu_d(4) & "0000"; -- Music On or Off
																	end if;

															when "110" => -- 0x1070 to 0x1077
																	DpcRandom <= x"01";
												
															when others => NULL;
														
													end case;
														
										else
											
													-- bank switch F8 style
													if (cpu_a = "1" & X"FF8") then
														bank <= "0000";
													elsif (cpu_a = "1" & X"FF9") then
														bank <= "0001";
													end if;
											
										end if;
								
								
								
								
								
								else -- A12 LOW
									-- non cartridge access - e.g. sta wsync
									
								--	if (clk_dpc = '1') then
								--		
								--			DpcClocks <= DpcClocks + 1;
--
								--			if to_integer(unsigned(DpcClocks)) mod to_integer(unsigned(DpcTops(5))+1) > DpcBottoms(5) then
								--				DpcMusicFlags(0) <= x"ff";
								--			else
								--				DpcMusicFlags(0) <= x"00";
								--			end if;
								--			
								--			if to_integer(unsigned(DpcClocks)) mod to_integer(unsigned(DpcTops(6))+1) > DpcBottoms(6) then
								--				DpcMusicFlags(1) <= x"ff";
								--			else
								--				DpcMusicFlags(1) <= x"00";
								--			end if;
								--	
								--			if to_integer(unsigned(DpcClocks)) mod to_integer(unsigned(DpcTops(7))+1) > DpcBottoms(7) then
								--				DpcMusicFlags(2) <= x"ff";
								--			else
								--				DpcMusicFlags(2) <= x"00";
								--			end if;
										
											
									--		if to_integer(unsigned(DpcClocks)) mod to_integer(unsigned(DpcTops(5))+1) < DpcBottoms(5) then
									--			DpcMusicFlags(0) <= x"00";
									--		elsif to_integer(unsigned(DpcClocks)) mod to_integer(unsigned(DpcTops(5))+1) < DpcTops(5) then
									--			DpcMusicFlags(0) <= x"ff";
									--		end if;
									--		DpcCounters(5)(7 downto 0) <= std_logic_vector(unsigned(DpcClocks) mod to_integer(unsigned(DpcTops(5))+1))(7 downto 0);
									--	
									--		if to_integer(unsigned(DpcClocks)) mod to_integer(unsigned(DpcTops(6))+1) < DpcBottoms(6) then
									--			DpcMusicFlags(1) <= x"00";
									--		elsif to_integer(unsigned(DpcClocks)) mod to_integer(unsigned(DpcTops(6))+1) < DpcTops(6) then
									--			DpcMusicFlags(1) <= x"ff";
									--		end if;
									--		DpcCounters(6)(7 downto 0) <= std_logic_vector(unsigned(DpcClocks) mod to_integer(unsigned(DpcTops(6))+1))(7 downto 0);
									--		
									--		if to_integer(unsigned(DpcClocks)) mod to_integer(unsigned(DpcTops(7))+1) < DpcBottoms(7) then
									--			DpcMusicFlags(2) <= x"00";
									--		elsif to_integer(unsigned(DpcClocks)) mod to_integer(unsigned(DpcTops(7))+1) < DpcTops(7) then
									--			DpcMusicFlags(2) <= x"ff";
									--		end if;
									--		DpcCounters(7)(7 downto 0) <= std_logic_vector(unsigned(DpcClocks) mod to_integer(unsigned(DpcTops(7))+1))(7 downto 0);
							
								--	end if;
									
									--clk_music <= clk_music + 1;
									
									
									
								end if;
						end if;


-------------------------------------------


								
							-- BANK FE fixed by Victor Trucco - 24/05/2018		
                    when BANKFE =>					  
 								-- If was latched, check the 5th bit of the data bus for the bank-switch
 								if FE_latch = '1' then
 									if (cpu_d(5) = '1') then 
 										bank <= "0000";
 									else
 										bank <= "0001";
 									end if;
 								end if;
 
 								-- Access at 0x01fe trigger the latch, but on the next cpu cycle
 								if (cpu_a(12 downto 0) = "0000111111110" ) then -- 0x01FE
 									FE_latch <= '1';
 								else
 									FE_latch <= '0';
 								end if;
							
								
                    when BANKE0 =>
                        if (cpu_a(12 downto 4) = "1" & X"FE" and cpu_a(3) = '0') then
                            e0_bank0 <= cpu_a(2 downto 0);
                        elsif (cpu_a(12 downto 4) = "1" & X"FE" and cpu_a(3) = '1') then
                            e0_bank1 <= cpu_a(2 downto 0);
                        elsif (cpu_a(12 downto 4) = "1" & X"FF" and cpu_a(3) = '0') then
                            e0_bank2 <= cpu_a(2 downto 0);
                        end if;
								
                    when BANK3F =>
                        --if (cpu_a(12 downto 6) = "0000000") then
                        if (cpu_a = "0" & X"03F") then
                            bank(1 downto 0) <= cpu_d(1 downto 0);
                        end if;
								
                    when others =>
                        null;
                end case;
            end if;
        end if;
    end process;
	 
	process (clk_dpc)
	begin
	
			if rising_edge(clk_dpc) then
											
					DpcClocks <= DpcClocks + 1;

					if to_integer(unsigned(DpcClocks)) mod to_integer(unsigned(DpcTops(5))+1) > DpcBottoms(5) then
						DpcMusicFlags(0) <= x"ff";
					else
						DpcMusicFlags(0) <= x"00";
					end if;
					
					if to_integer(unsigned(DpcClocks)) mod to_integer(unsigned(DpcTops(6))+1) > DpcBottoms(6) then
						DpcMusicFlags(1) <= x"ff";
					else
						DpcMusicFlags(1) <= x"00";
					end if;

					if to_integer(unsigned(DpcClocks)) mod to_integer(unsigned(DpcTops(7))+1) > DpcBottoms(7) then
						DpcMusicFlags(2) <= x"ff";
					else
						DpcMusicFlags(2) <= x"00";
					end if;
					
			end if;
				
	end process;
	 
--	 process(bs_method)
--    begin
--	 
--      if(bs_method = X"04" or bs_method = X"08" ) then    -- 4k 
--        bss <= BANK00;
--		  
--      elsif(bs_method = X"10") then 
--        bss <= BANKF8;
--		  
--      elsif(bs_method = X"15") then -- with SC 
--        bss <= BANKF8;	
--		  sc <= '1';
--		  
--      elsif(bs_method = X"20") then 
--        bss <= BANKF6;
--		  
--		elsif(bs_method = X"25") then -- with SC
--        bss <= BANKF6;
--		  sc <= '1';
--		  
--      elsif(bs_method = X"40") then
--        bss <= BANKF4;
--		  
--      elsif(bs_method = X"45") then -- with SC
--        bss <= BANKF4;
--		  sc <= '1';
--
--		elsif(bs_method = X"50") then 
--			bss <= BANKFE;
--
--		elsif(bs_method = X"60") then 
--        bss <= BANKE0;
--
--		elsif(bs_method = X"65") then 
--        bss <= BANK3F;
--
--		else
--        bss <= BANK00;
--      end if;
--		
--    end process;
	
	 oLed <= bss;
	 
--	 process(bs_method) is
--	 begin
--		 case bs_method is
--			when X"04" =>		bss <= BANK00;
--			when X"08" =>     bss <= BANK00;
--			when X"10"=>      bss <= BANKF8;
--			when X"15"=>      bss <= BANKF8; sc <= '1';
--			when X"20"=>      bss <= BANKF6;
--			when X"25"=>      bss <= BANKF6; sc <= '1';
--			when X"40"=>      bss <= BANKF4;
--			when X"45"=>      bss <= BANKF4; sc <= '1';
--			when X"50" =>		bss <= BANKFE;
--			when X"60" =>     bss <= BANKE0;
--			when X"65"=>      bss <= BANK3F;
--			when OTHERS =>    bss <= BANK00;
--      end case;
--	end process;

	bss <= bs_method(2 downto 0);
	sc <= bs_method(4);

    --bss <= cart_info(3 downto 1);
    --sc <= cart_info(0);
    --cart_vect <= cart_info(10 downto 4);
	--LED <= bss;
    --cart_next <= (not pa(7)) and (not gsel);
    --cart_prev <= (not pa(6)) and (not gsel);
end arch;



