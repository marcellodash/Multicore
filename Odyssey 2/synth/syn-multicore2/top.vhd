

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;


entity top is
	port (
			-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o		: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o		: out   std_logic								:= '1';
		sram_oe_n_o		: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= 'Z';
		sd_sclk_o			: out   std_logic								:= 'Z';
		sd_mosi_o			: out   std_logic								:= 'Z';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= 'Z'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: in  std_logic	:= 'Z';
		stm_b13_io			: in  std_logic	:= 'Z';
		stm_b14_io			: out std_logic		:= 'Z';
		stm_b15_io			: in  std_logic	:= 'Z'
	);
end entity;

architecture Behavior of top is

	type config_array is array(natural range 15 downto 0) of std_logic_vector(7 downto 0);


	component sdram is 
	port
	(
		-- interface to the SDRAM IC
		sd_data      : inout   std_logic_vector(15 downto 0);
		sd_addr      : out   std_logic_vector(12 downto 0);
		sd_dqm       : out   std_logic_vector(1 downto 0);
		sd_cs        : out   std_logic;
		sd_ba        : out   std_logic_vector(1 downto 0);
		sd_we        : out   std_logic;
		sd_ras       : out   std_logic;
		sd_cas       : out   std_logic;

		-- system interface
		clk          : in std_logic;
		clkref       : in std_logic;
		init         : in std_logic;

		-- cpu interface
		din          : in std_logic_vector(15 downto 0);
		addr         : in std_logic_vector(24 downto 0);
		we           : in std_logic;
		oe           : in std_logic;
		ds           : in std_logic_vector(1 downto 0);
		dout         : out   std_logic_vector(15 downto 0)
	);
	end component;
		
	component osd is
	generic
	(
		OSD_VISIBLE 	: std_logic_vector(1 downto 0) := (others=>'0');
		OSD_X_OFFSET 	: std_logic_vector(9 downto 0) := (others=>'0');
		OSD_Y_OFFSET 	: std_logic_vector(9 downto 0) := (others=>'0');
		OSD_COLOR    	: std_logic_vector(2 downto 0) := (others=>'0')
	);
	port
	(
		-- OSDs pixel clock, should be synchronous to cores pixel clock to
		-- avoid jitter.
		pclk		: in std_logic;

		-- SPI interface
		sck		: in std_logic;
		ss			: in std_logic;
		sdi		: in std_logic;
		sdo		: out std_logic;

		-- VGA signals coming from core
		red_in 	: in std_logic_vector(4 downto 0);
		green_in : in std_logic_vector(4 downto 0);
		blue_in 	: in std_logic_vector(4 downto 0);
		hs_in		: in std_logic;
		vs_in		: in std_logic;
		
		-- VGA signals going to video connector
		red_out	: out std_logic_vector(4 downto 0);
		green_out: out std_logic_vector(4 downto 0);
		blue_out	: out std_logic_vector(4 downto 0);
		hs_out 	: out std_logic;
		vs_out 	: out std_logic;
		
		-- Data in
		data_in 	: in std_logic_vector(7 downto 0);
		
		--data pump to sram
		pump_active_o	: out std_logic;
		sram_a_o			: out std_logic_vector(18 downto 0);
		sram_d_o			: out std_logic_vector(7 downto 0);
		sram_we_n_o		: out std_logic;
		config_buffer_o: out config_array
	
	);
	end component;
	
	
	
	
	alias SPI_DI  	: std_logic is stm_b15_io;
	alias SPI_DO  	: std_logic is stm_b14_io; --o
	alias SPI_SCK  : std_logic is stm_b13_io;
	alias SPI_SS3  : std_logic is stm_b12_io;

	-- Reset signal
	signal reset_n				: std_logic;		-- Reset geral
	
	-- Master clock
	signal pll_reset			: std_logic;		-- Reset do PLL
	signal pll_locked			: std_logic;		-- PLL travado quando 1
	
	-- Master clock
	signal clk_28				: std_logic;
	signal sdram_clk			: std_logic := '0';
	signal sysclk 				: std_logic := '0';
	signal clk_en_s 			: std_logic;
	
	
	signal port_243b 			: std_logic_vector(7 downto 0);
	
	signal ram_a				: std_logic_vector(17 downto 0);		-- 512K
	signal ram_din				: std_logic_vector(15 downto 0);
	signal ram_dout			: std_logic_vector(15 downto 0);
	signal ram_cs				: std_logic;
	signal ram_oe				: std_logic;
	signal ram_we				: std_logic;
	signal rom_a				: std_logic_vector(13 downto 0);		-- 16K
	signal rom_dout			: std_logic_vector(7 downto 0);
	
	signal from_sram			: std_logic_vector(15 downto 0);
	signal to_sram				: std_logic_vector(15 downto 0);
	
		-- ram
	signal loader_ram_a		: std_logic_vector(17 downto 0);		-- 512K
	signal loader_to_sram	: std_logic_vector(15 downto 0);
	signal loader_from_sram	: std_logic_vector(15 downto 0);
	signal loader_ram_data	: std_logic_vector(15 downto 0);
	signal loader_ram_cs		: std_logic;
	signal loader_ram_oe		: std_logic;
	signal loader_ram_we		: std_logic;

	signal a2601_ram_a		: std_logic_vector(11 downto 0);	
	signal a2601_ram_dout	: std_logic_vector(7 downto 0);
	

	-- scanlines
	signal scanlines_en_s		: std_logic := '0';
	signal btn_scan_s				: std_logic;
	signal odd_line_s				: std_logic := '0';
	
	
	--rgb
	signal rgb_loader_out		: std_logic_vector(7 downto 0);
	signal rgb_odyssey_out		: std_logic_vector(7 downto 0);
	
	signal hsync_loader_out		: std_logic;
	signal vsync_loader_out		: std_logic;
	
	signal hsync_odyssey_out	: std_logic;
	signal vsync_odyssey_out	: std_logic;
	
	signal cart_a        		: std_logic_vector(12 downto 0);
	signal cart_d 					: std_logic_vector( 7 downto 0);
	signal cart_oe_n_s			: std_logic;
  
	signal odyssey_reset_n_s 	: std_logic;
          
	-- PS/2
	signal keyb_data_s 			: std_logic_vector(7 downto 0);
	signal keyb_valid_s 			: std_logic;
	--signal clk_keyb 				: std_logic;
	
	-- HDMI
	signal clk_module_vga		: std_logic;
	signal clk_vga					: std_logic;
	signal clk_dvi					: std_logic;
	signal color_s 				: std_logic_vector(3 downto 0);
	signal vga_color_s 			: std_logic_vector(3 downto 0);
	signal vga_hsync_n_s 		: std_logic;
	signal vga_vsync_n_s 		: std_logic;
	signal vga_blank_s 			: std_logic;
	
	signal vga_r_s				: std_logic_vector( 4 downto 0);
	signal vga_g_s				: std_logic_vector( 4 downto 0);
	signal vga_b_s				: std_logic_vector( 4 downto 0);
	
	signal tdms_r_s			: std_logic_vector( 9 downto 0);
	signal tdms_g_s			: std_logic_vector( 9 downto 0);
	signal tdms_b_s			: std_logic_vector( 9 downto 0);
	signal hdmi_p_s			: std_logic_vector( 3 downto 0);
	signal hdmi_n_s			: std_logic_vector( 3 downto 0);
	
	signal cnt_hor_s 				: std_logic_vector(8 downto 0);
	signal cnt_ver_s 				: std_logic_vector(8 downto 0);
	signal sound_hdmi_s			: std_logic_vector(15 downto 0);
	signal tdms_s					: std_logic_vector( 7 downto 0);
	
	signal loader_hor_s 			: std_logic_vector(8 downto 0);
	signal loader_ver_s 			: std_logic_vector(8 downto 0);
	signal vp_hor_s 				: std_logic_vector(8 downto 0);
	signal vp_ver_s 				: std_logic_vector(8 downto 0);
	signal vp_color_index_s 	: std_logic_vector(3 downto 0);
	signal vga_rgb_s 				: std_logic_vector(7 downto 0);

	signal vga_rgb_out_s 		: std_logic_vector(7 downto 0);
	
	signal snd_vec_s				: std_logic_vector(3 downto 0);
	signal audio_s 				: std_logic;
	
	-- OSD
	signal clock_div_q	: unsigned(7 downto 0) 				:= (others => '0');
	signal keys_s			: std_logic_vector( 7 downto 0) := (others => '1');		
	signal osd_s			: std_logic_vector( 7 downto 0) := (others => '1');	
	signal loader_s		: std_logic_vector( 7 downto 0) := "00111111"; --send the signal to open the loader on init	
	signal FKeys_s			: std_logic_vector(12 downto 1);
	signal pump_active_s	: std_logic								:= '0';
	signal sram_addr_s 	: std_logic_vector (18 downto 0) := (others=>'1');
	signal sram_data_s 	: std_logic_vector (7 downto 0) := (others=>'0');
	signal power_on_s		: std_logic_vector(15 downto 0)	:= (others => '1');
	signal addr_desloc	: std_logic_vector(15 downto 0)	:= (others => '0');	
	
	signal config_buffer_s : config_array;
	
	---------------------


	signal sdram_din  : std_logic_vector(15 downto 0);
	signal sdram_addr : std_logic_vector(24 downto 0);
	signal sdram_wr   : std_logic; 
	signal sdram_oe   : std_logic; 
	signal sdram_dout	: std_logic_vector(15 downto 0); 
	
	-- joystick
	signal joy1_s			: std_logic_vector(11 downto 0) := (others => '1'); --  MXYZ SACB RLDU
	signal joy2_s			: std_logic_vector(11 downto 0) := (others => '1'); --  MXYZ SACB RLDU
	signal joyP7_s			: std_logic;
		
begin

 	ps2 : work.ps2_intf port map 
	(
		sysclk,
		reset_n,
		ps2_clk_io,
		ps2_data_io,
		keyb_data_s,
		keyb_valid_s,
		open
	);
	
	--clk_keyb <= clk_28 when odyssey_reset_n_s = '0' else sysclk;
  

	pll: work.pll_odyssey port map (
		areset		=> pll_reset,				-- PLL Reset
		inclk0		=> clock_50_i,				-- Clock 50 MHz externo
		c0				=> sysclk,					
		c1				=> sdram_clk,					
		--c2				=> clk_28,		
		c3				=> clk_vga,			
		c4				=> clk_dvi,			
		locked		=> pll_locked				-- Sinal de travamento (1 = PLL travado e pronto)
	);
	
	
	pll_reset	<= '1' when btn_n_i(3) = '0' and btn_n_i(4) = '0' else '0';
	reset_n		<= not (pll_reset or not pll_locked);	-- System is reset by external reset switch or PLL being out of lock
	
	

	osd1 : osd 
	generic map
	(
		OSD_VISIBLE => "00", -- 00 - OSD start invisible, 01 - start visible
		OSD_COLOR => "001", -- RGB
		OSD_X_OFFSET => "0000010010", -- 50
		OSD_Y_OFFSET => "0000001111"  -- 15
	)
	port map
	(
		pclk        => clk_vga,

		-- spi for OSD
		sdi        => SPI_DI,
		sck        => SPI_SCK,
		ss         => SPI_SS3,
		sdo        => SPI_DO,
		
		red_in     => vga_rgb_out_s(7 downto 5) & vga_rgb_out_s(7 downto 6),
		green_in   => vga_rgb_out_s(4 downto 2) & vga_rgb_out_s(4 downto 3),
		blue_in    => vga_rgb_out_s(1 downto 0) & vga_rgb_out_s(1 downto 0) & vga_rgb_out_s(0),
		hs_in      => vga_hsync_n_s,
		vs_in      => vga_vsync_n_s,

		red_out    => vga_r_s,
		green_out  => vga_g_s,
		blue_out   => vga_b_s,
		hs_out     => open,
		vs_out     => open,

		data_in		=> osd_s,
		
		pump_active_o	=> pump_active_s,
		sram_a_o			=> sram_addr_s,
		sram_d_o			=> sram_data_s,
		sram_we_n_o		=> ram_we,
		config_buffer_o=> config_buffer_s
	);
            

	
--	sram2_addr_o   <= (sram_addr_s + addr_desloc) when ram_we = '0' else "0" & ram_a;
--	sram2_data_io  <= sram_data_s when ram_we = '0' else (others=>'Z');
--	from_sram(7 downto 0) 	  <= sram2_data_io;
 --  sram2_oe_n_o   <= '0';
--	sram2_we_n_o   <= ram_we;
	
	-- calculate the start address to save the incoming data
	-- check the size of the loaded file
	addr_desloc <= x"1800" when config_buffer_s(13) = x"08" else -- 2k
						x"1000" when config_buffer_s(13) = x"10" else -- 4k
						x"0000"; -- 8k

					
		sram_we_n_o <= '1';
		sram_oe_n_o <= '1';
						
 		sdram_clk_o <= sdram_clk;
 		--sdram_cke_o <= '1';
 		
 		
 		sdram_addr  <= "000000" & (sram_addr_s + addr_desloc) when ram_we = '0' else "0000000" & ram_a;
		sdram_din   <= "00000000" & sram_data_s when ram_we = '0' else (others=>'Z'); 
 		sdram_wr    <= (not ram_we);
 		sdram_oe    <= '0' when ram_we = '0' else (not cart_oe_n_s);
 	--	RAM_Dout 	<= sdram_dout(7 downto 0);
 	
	
--		sdram1 :  sdram 
--		port map
--		(
--			-- interface to the SDRAM IC
--			sd_data      => sdram_da_io ,
--			sd_addr      => sdram_ad_o  ,
--			sd_dqm       => sdram_dqm_o ,
--			sd_cs        => sdram_cs_o  ,
--			sd_ba        => sdram_ba_o  ,
--			sd_we        => sdram_we_o  ,
--			sd_ras       => sdram_ras_o ,
--			sd_cas       => sdram_cas_o ,
--
--			-- system interface
--			clk            => sdram_clk      ,
--			clkref         => clk_en_s       ,
--			init           => not pll_locked ,
--
--			-- cpu interface
--			din         => sdram_din   ,
--			addr        => sdram_addr  ,
--			we          => sdram_wr    ,
--			oe          => sdram_oe    ,
--			ds          => "01"		  , --just the lower 8 bits
--			dout        => from_sram  
--		);

 		sdram1 : entity work.dpSDRAM256Mb 
 		generic map (
 			freq_g			=> 35
 		)
 		port map
 		(
 			clock_i			=> sdram_clk,
 			reset_i			=> not pll_locked,
 			refresh_i		=> '1',
 			
 			-- Porta 0
 			port0_cs_i		=> sdram_oe or sdram_wr,
 			port0_oe_i		=> sdram_oe,
 			port0_we_i		=> sdram_wr,
 			port0_addr_i	=> sdram_addr,
 			port0_data_i	=> sdram_din(7 downto 0),
 			port0_data_o	=> from_sram(7 downto 0),
 			
 			-- Porta 1
 			port1_cs_i		=> '0',
 			port1_oe_i		=> '0',
 			port1_we_i		=> '0',
 			port1_addr_i	=> (others=>'0'),
 			port1_data_i	=> (others=>'0'),
 			port1_data_o	=> open,
 			
 			-- SD-RAM ports
 			mem_cke_o		=> sdram_cke_o,
 			mem_cs_n_o		=> sdram_cs_o,
 			mem_ras_n_o		=> sdram_ras_o,
 			mem_cas_n_o		=> sdram_cas_o,
 			mem_we_n_o		=> sdram_we_o,
 			mem_udq_o		=> sdram_dqm_o(1),
 			mem_ldq_o		=> sdram_dqm_o(0),
 			mem_ba_o			=> sdram_ba_o,
 			mem_addr_o		=> sdram_ad_o,
 			mem_data_io		=> sdram_da_io
 		);						
						
	
	--------------------------------------------------------------------------------

	
	odyssey2: work.top_odyssey2 port map (
  
	-- Clocks
		clk_s			=> sysclk,
		clk_en_o		=> clk_en_s,

		reset_n   => odyssey_reset_n_s,
		
		-- Buttons
		btn_n_i				=> btn_n_i,

		-- PS2
		keyb_data      => keyb_data_s, 
		keyb_valid     => keyb_valid_s,  
		osd_o				=> keys_s,
			
		-- Joystick
		-- joy_s format MXYZ SACB RLDU 
		
		joy1_up_i			=> joy1_s(0),
		joy1_down_i			=> joy1_s(1),
		joy1_left_i			=> joy1_s(2),
		joy1_right_i		=> joy1_s(3),
		joy1_p6_i			=> joy1_s(10) and joy1_s(9) and joy1_s(8) and joy1_s(7) and joy1_s(6) and joy1_s(5) and joy1_s(4),
		joy1_p9_i			=> joy1_s(10) and joy1_s(9) and joy1_s(8) and joy1_s(7) and joy1_s(6) and joy1_s(5) and joy1_s(4),
		
		joy2_up_i			=> joy2_s(0),
		joy2_down_i			=> joy2_s(1),
		joy2_left_i			=> joy2_s(2),
		joy2_right_i		=> joy2_s(3),
		joy2_p6_i			=> joy2_s(10) and joy2_s(9) and joy2_s(8) and joy2_s(7) and joy2_s(6) and joy2_s(5) and joy2_s(4),
		joy2_p9_i			=> joy2_s(10) and joy2_s(9) and joy2_s(8) and joy2_s(7) and joy2_s(6) and joy2_s(5) and joy2_s(4),

		-- Audio
		snd_vec_o			=> snd_vec_s,


		-- VGA
		vga_r_o				=> rgb_odyssey_out(7 downto 5),
		vga_g_o				=> rgb_odyssey_out(4 downto 2),
		vga_b_o(2 downto 1)=> rgb_odyssey_out(1 downto 0),
		vga_hsync_n_o		=> hsync_odyssey_out,
		vga_vsync_n_o		=> vsync_odyssey_out,
		
		color_index_o => vp_color_index_s,


		-- Debug
		leds_n_o				=> open,
		
		-- cartridge
		rom_a_s => cart_a,
		rom_d_s => cart_d,
		cart_psen_n_o	=> cart_oe_n_s,
		
		hpos_o     => vp_hor_s,
		vpos_o  	  => vp_ver_s
    
  );

	  
  dac_b : work.dac
    generic map (
      msbi_g => 7
    )
    port map (
      clk_i   => clk_module_vga,
      res_n_i => reset_n,
      dac_i   => "0" & snd_vec_s & "000",
      dac_o   => audio_s
    );
  --
  dac_l_o <= audio_s;
  dac_r_o <= audio_s;
	

	---------------------------------------------------
	
	

			

			ram_a				<= "00000" & cart_a;
			to_sram			<= (others=>'Z');
			cart_d			<= from_sram(7 downto 0);


			
			--vga_r_s  <= rgb_odyssey_out (7 downto 5);
			--vga_g_s  <= rgb_odyssey_out (4 downto 2);
			--vga_b_c  <= rgb_odyssey_out (1 downto 0) & rgb_odyssey_out (0);
			color_s <=  vp_color_index_s; --"0" & rgb_odyssey_out(7)& rgb_odyssey_out(4)& rgb_odyssey_out(1);
		--	vga_hsync_n_s <= hsync_odyssey_out;
		--	vga_vsync_n_s <= vsync_odyssey_out;
			
			odyssey_reset_n_s <= not pump_active_s;
			
			clk_module_vga <= sysclk;
			
			cnt_hor_s <= vp_hor_s;-- - 20;---25; 
			cnt_ver_s <= vp_ver_s;
			
			sound_hdmi_s <= "000" & snd_vec_s & "000000000";
			

	
	
	
--- Joystick read with sega 6 button support----------------------

	process(vga_hsync_n_s)
		variable state_v : unsigned(7 downto 0) := (others=>'0');
		variable j1_sixbutton_v : std_logic := '0';
		variable j2_sixbutton_v : std_logic := '0';
	begin
		if falling_edge(vga_hsync_n_s) then
		
			state_v := state_v + 1;
			
			case state_v is
				-- joy_s format MXYZ SACB RLDU
			
				when X"00" =>  
					joyP7_s <= '0';
					
				when X"01" =>
					joyP7_s <= '1';

				when X"02" => 
					joy1_s(3 downto 0) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- R, L, D, U
					joy2_s(3 downto 0) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- R, L, D, U
					joy1_s(5 downto 4) <= joy1_p9_i & joy1_p6_i; -- C, B
					joy2_s(5 downto 4) <= joy2_p9_i & joy2_p6_i; -- C, B					
					joyP7_s <= '0';
					j1_sixbutton_v := '0'; -- Assume it's not a six-button controller
					j2_sixbutton_v := '0'; -- Assume it's not a six-button controller

				when X"03" =>
					joy1_s(7 downto 6) <= joy1_p9_i & joy1_p6_i; -- Start, A
					joy2_s(7 downto 6) <= joy2_p9_i & joy2_p6_i; -- Start, A
					joyP7_s <= '1';
			
				when X"04" =>  
					joyP7_s <= '0';

				when X"05" =>
					if joy1_right_i = '0' and joy1_left_i = '0' and joy1_down_i = '0' and joy1_up_i = '0' then 
						j1_sixbutton_v := '1'; --it's a six button
					end if;
					
					if joy2_right_i = '0' and joy2_left_i = '0' and joy2_down_i = '0' and joy2_up_i = '0' then 
						j2_sixbutton_v := '1'; --it's a six button
					end if;
					
					joyP7_s <= '1';
					
				when X"06" =>
					if j1_sixbutton_v = '1' then
						joy1_s(11 downto 8) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- Mode, X, Y e Z
					end if;
					
					if j2_sixbutton_v = '1' then
						joy2_s(11 downto 8) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- Mode, X, Y e Z
					end if;
					
					joyP7_s <= '0';

				when others =>
					joyP7_s <= '1';
					
			end case;

		end if;
	end process;
	
	joyX_p7_o <= joyP7_s;
---------------------------
	
	
	-- VGA framebuffer
	vga: entity work.vga
	port map (
		I_CLK			=> clk_module_vga,
		I_CLK_VGA	=> clk_vga,
		I_COLOR		=> color_s,
		I_HCNT		=> cnt_hor_s,
		I_VCNT		=> cnt_ver_s,
		O_HSYNC		=> vga_hsync_n_s,
		O_VSYNC		=> vga_vsync_n_s,
		O_COLOR		=> vga_color_s,
		O_HCNT		=> open,
		O_VCNT		=> open,
		O_H			=> open,
		O_BLANK		=> vga_blank_s
	);
	
	
	
	process (vga_color_s)
	begin
		case vga_color_s is
			when "0000" => vga_rgb_s <= "00000000"; --X"000000"; -- 000 000 000
			when "0001" => vga_rgb_s <= "00000111"; --X"0e3dd4"; -- 000 001 110
			when "0010" => vga_rgb_s <= "00010000"; --X"00981b"; -- 000 100 000
			when "0011" => vga_rgb_s <= "00010111"; --X"00bbd9"; -- 000 101 110
			when "0100" => vga_rgb_s <= "11000000"; --X"c70008"; -- 110 000 000
			when "0101" => vga_rgb_s <= "11000010"; --X"cc16b3"; -- 110 000 101
			when "0110" => vga_rgb_s <= "10010000"; --X"9d8710"; -- 100 100 000
			when "0111" => vga_rgb_s <= "11111011"; --X"e1dee1"; -- 111 110 111
			when "1000" => vga_rgb_s <= "01001101"; --X"5f6e6b"; -- 010 011 011
			when "1001" => vga_rgb_s <= "01110111"; --X"6aa1ff"; -- 011 101 111
			when "1010" => vga_rgb_s <= "00111101"; --X"3df07a"; -- 001 111 011
			when "1011" => vga_rgb_s <= "00111111"; --X"31ffff"; -- 001 111 111
			when "1100" => vga_rgb_s <= "11101001"; --X"ff4255"; -- 111 010 010
			when "1101" => vga_rgb_s <= "11110011"; --X"ff98ff"; -- 111 100 111
			when "1110" => vga_rgb_s <= "11010101"; --X"d9ad5d"; -- 110 101 010
			when "1111" => vga_rgb_s <= "11111111"; --X"ffffff"; -- 111 111 111
		end case;
	end process;
	

	

	
	
	

	-- HDMI
 		inst_dvid: entity work.hdmi
 		generic map (
 			FREQ	=> 25200000,	-- pixel clock frequency 
 			FS		=> 48000,		-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
 			CTS	=> 25200,		-- CTS = Freq(pixclk) * N / (128 * Fs)
 			N		=> 6144			-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
 		) 
 		port map (
 			I_CLK_PIXEL		=> clk_vga,
			
			I_R				=> vga_r_s & vga_r_s(4 downto 2),
			I_G				=> vga_g_s & vga_g_s(4 downto 2),
			I_B				=> vga_b_s & vga_b_s(4 downto 2),
			
			I_BLANK			=> vga_blank_s,
			I_HSYNC			=> vga_hsync_n_s,
			I_VSYNC			=> vga_vsync_n_s,
			
			-- PCM audio
			I_AUDIO_ENABLE	=> '1',
			I_AUDIO_PCM_L 	=> sound_hdmi_s,
			I_AUDIO_PCM_R	=> sound_hdmi_s,

			-- TMDS parallel pixel synchronous outputs (serialize LSB first)
 			O_RED				=> tdms_r_s,
			O_GREEN			=> tdms_g_s,
			O_BLUE			=> tdms_b_s
		);
		

			hdmio: entity work.hdmi_out_altera
		port map (
			clock_pixel_i		=> clk_vga,
			clock_tdms_i		=> clk_dvi,
			red_i					=> tdms_r_s,
			green_i				=> tdms_g_s,
			blue_i				=> tdms_b_s,
			tmds_out_p			=> hdmi_p_s,
			tmds_out_n			=> hdmi_n_s
		);
 		
		
		tmds_o(7)	<= hdmi_p_s(2);	-- 2+		
		tmds_o(6)	<= hdmi_n_s(2);	-- 2-		
		tmds_o(5)	<= hdmi_p_s(1);	-- 1+			
		tmds_o(4)	<= hdmi_n_s(1);	-- 1-		
		tmds_o(3)	<= hdmi_p_s(0);	-- 0+		
		tmds_o(2)	<= hdmi_n_s(0);	-- 0-	
		tmds_o(1)	<= hdmi_p_s(3);	-- CLK+	
		tmds_o(0)	<= hdmi_n_s(3);	-- CLK-	


		vga_r_o			<= vga_r_s;
		vga_g_o			<= vga_g_s;
		vga_b_o			<= vga_b_s;
		vga_hsync_n_o	<= vga_hsync_n_s;
		vga_vsync_n_o	<= vga_vsync_n_s;

	
	---------------------------------
	-- scanlines
	btnscl: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> sysclk,
		button_i			=> btn_n_i(1) or btn_n_i(2),
		result_o			=> btn_scan_s
	);
	
	process (btn_scan_s)
	begin
		if falling_edge(btn_scan_s) then
			scanlines_en_s <= not scanlines_en_s;
		end if;
	end process;
	
	
	vga_rgb_out_s <= '0' & vga_rgb_s(6 downto 5) & '0' & vga_rgb_s(3 downto 2)& '0' & vga_rgb_s(0) when scanlines_en_s = '1' and odd_line_s = '1' else vga_rgb_s;
	
	
	
	process(vga_hsync_n_s,vga_vsync_n_s)
	begin
		if vga_vsync_n_s = '0' then
			odd_line_s <= '0';
		elsif rising_edge(vga_hsync_n_s) then
			odd_line_s <= not odd_line_s;
		end if;
	end process;
	
	process(clk_vga)
	begin
		if rising_edge(clk_vga) then 
			clock_div_q <= clock_div_q + 1;
		end if;
	end process;

	--start the microcontroller OSD menu after the power on
	process (clock_div_q)
	begin
		if rising_edge(clock_div_q(5)) then
			if power_on_s /= x"00" then
				power_on_s <= power_on_s - 1;
				osd_s <= loader_s;
			else
				osd_s <= keys_s;
			end if;
		end if;
	end process;


end architecture;
