-- Multicore 2 top level by Victor Trucco


---------------------------------------------------------------------------------
-- DE10_lite Top level for vectrex by Dar (darfpga@aol.fr) (27/12/2017)
-- http://darfpga.blogspot.fr
---------------------------------------------------------------------------------
-- Educational use only
-- Do not redistribute synthetized file with roms
-- Do not redistribute roms whatever the form
-- Use at your own risk
---------------------------------------------------------------------------------
-- Use vectrex_de10_lite.sdc to compile (Timequest constraints)
-- /!\
-- Don't forget to set device configuration mode with memory initialization 
--  (Assignments/Device/Pin options/Configuration mode)
---------------------------------------------------------------------------------
-- TODO :
--   sligt tune of characters drawings (too wide)
--   tune hblank to avoid persistance artifact on first 4 pixels of a line
---------------------------------------------------------------------------------
--
-- Main features :
--  PS2 keyboard input @gpio pins 35/34 (beware voltage translation/protection) 
--  Audio pwm output   @gpio pins 1/3 (beware voltage translation/protection) 
--
--  Uses 1 pll for 25/24MHz and 12.5/12MHz generation from 50MHz
--
--  Horizontal/vertical display selection at compilation 
--  3 or no intensity level selection at compilation
--
--  No external ram
--  FPGA ram usage as low as :
--
--		  336.000b ( 42Ko) without   cartridge, vertical display,   no intensity level (minestrom)
--		  402.000b ( 50Ko) with  8Ko cartridge, vertical display,   no intensity level
--	 	  599.000b ( 74ko) with 32Ko cartridge, vertical display,   no intensity level
--	 	  664.000b ( 82ko) with  8Ko cartridge, horizontal display, no intensity level
--		1.188.000b (146ko) with  8Ko cartridge, horizontal display, 3 intensity level

--  Tested cartridge:
--
--		berzerk          ( 4ko)
--		ripoff           ( 4ko)
--		scramble         ( 4ko)
--		spacewar         ( 4ko)
--		startrek         ( 4ko)
--		pole position    ( 8ko)
--		spike            ( 8ko)
--		webwars          ( 8ko)
--		frogger          (16Ko)
--		vecmania1        (32ko)
--		war of the robot (21ko)
--
-- Board key :
--   0 : reset game
--
-- Keyboard players inputs :
--
--   F3 : button
--   F2 : button
--   F1 : button 
--   SPACE       : button
--   RIGHT arrow : joystick right
--   LEFT  arrow : joystick  left
--   UP    arrow : joystick  up 
--   DOWN  arrow : joystick  down
--
-- Other details : see vectrex.vhd
-- For USB inputs and SGT5000 audio output see my other project: xevious_de10_lite
---------------------------------------------------------------------------------
-- Use tool\vectrex_unzip\make_vectrex_proms.bat to build vhdl rom files
--
--make_vhdl_prom 	exec_rom.bin vectrex_exec_prom.vhd (always needed)
--
--make_vhdl_prom 	scramble.bin vectrex_scramble_prom.vhd
--make_vhdl_prom 	berzerk.bin vectrex_berzerk_prom.vhd
--make_vhdl_prom 	frogger.bin vectrex_frogger_prom.vhd
--make_vhdl_prom 	spacewar.bin vectrex_spacewar_prom.vhd
--make_vhdl_prom 	polepos.bin vectrex_polepos_prom.vhd
--make_vhdl_prom 	ripoff.bin vectrex_ripoff_prom.vhd
--make_vhdl_prom 	spike.bin vectrex_spike_prom.vhd
--make_vhdl_prom 	startrek.bin vectrex_startrek_prom.vhd
--make_vhdl_prom 	vecmania1.bin vectrex_vecmania1_prom.vhd
--make_vhdl_prom 	webwars.bin vectrex_webwars_prom.vhd
--make_vhdl_prom 	wotr.bin vectrex_wotr_prom.vhd
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity top is
	port (
		-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= 'Z'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: in std_logic			:= 'Z';
		stm_b13_io			: in std_logic			:= 'Z';
		stm_b14_io			: out std_logic		:= 'Z';
		stm_b15_io			: in std_logic			:= 'Z'
	);
end entity;

architecture Behavior of top is
	
 signal clock_12p5  	: std_logic;
 signal clock_25  	: std_logic;
 signal clock_hdmi	: std_logic;
 signal reset     	: std_logic;
 signal power_on_reset     : std_logic := '0';
 
-- signal max3421e_clk : std_logic;
 
 signal vga_r_s 	: std_logic_vector(4 downto 0);
 signal vga_g_s 	: std_logic_vector(4 downto 0);
 signal vga_b_s 	: std_logic_vector(4 downto 0);
 signal r         : std_logic_vector(4 downto 0);
 signal g         : std_logic_vector(4 downto 0);
 signal b         : std_logic_vector(4 downto 0);
 signal csync     : std_logic;
 signal vga_hsync_n_s : std_logic;
 signal vga_vsync_n_s : std_logic;
 signal blankn    : std_logic;
 
 signal audio           : std_logic_vector( 9 downto 0);
 signal pwm_accumulator : std_logic_vector(12 downto 0);

 alias reset_n         : std_logic is btn_n_i(1);

-- signal ps2_clk         : std_logic;
-- signal ps2_dat         : std_logic;
-- signal pwm_audio_out_l : std_logic;
-- signal pwm_audio_out_r : std_logic;
 

 signal kbd_intr      : std_logic;
 signal kbd_scancode  : std_logic_vector(7 downto 0);
 signal joyHBCPPFRLDU : std_logic_vector(9 downto 0);
-- signal keys_HUA      : std_logic_vector(2 downto 0);


-- signal start : std_logic := '0';
-- signal usb_report : usb_report_t;
-- signal new_usb_report : std_logic := '0';

 signal pot_x : signed(7 downto 0);
 signal pot_y : signed(7 downto 0);
 signal pot2_x : signed(7 downto 0);
 signal pot2_y : signed(7 downto 0);
 signal pot_speed_cnt : std_logic_vector(15 downto 0);
 
 signal audio_dac_s		: std_logic;
 
 -- cartridges
 signal cpu_addr : std_logic_vector(15 downto 0);
 signal cart_do : std_logic_vector(7 downto 0);

 -- OSD
 signal pump_active_s 	 : std_logic := '0';
 signal osd_s  		 : std_logic_vector(7 downto 0);
 signal clock_div_q	: unsigned(7 downto 0) 				:= (others => '0');
 signal keys_s			: std_logic_vector( 7 downto 0) := (others => '1');		
 signal from_sram 	: std_logic_vector(7 downto 0) := (others=>'1');
 
 	-- joystick
	signal joy1_s			: std_logic_vector(11 downto 0) := (others => '1'); 	
	signal joy2_s			: std_logic_vector(11 downto 0) := (others => '1'); 
	signal joyP7_s			: std_logic;
	
	-- config string
	constant STRLEN		: integer := 0;
 
begin

reset <= (not reset_n) or pump_active_s or power_on_reset; 


-- Clock 25.175MHz for vectrex core with VGA output
-- changed to 25.200 Mhz for HDMI support
clocks : entity work.pll
port map
(
	inclk0 => clock_50_i,
	c0 => clock_25,
	c1 => clock_hdmi
);


process (reset, clock_25)
begin
	if reset='1' then
		clock_12p5 <= '0';
	else 
		if rising_edge(clock_25) then
			clock_12p5  <= not clock_12p5;
		end if;
	end if;
end process;



vectrex : entity work.vectrex
port map(
 clock_24  => clock_25,  
 clock_12  => clock_12p5,
 reset     => reset,
 
 video_r      => r(4 downto 1),
 video_g      => g(4 downto 1),
 video_b      => b(4 downto 1),
 video_csync  => csync,
 video_blankn => blankn,
 video_hs     => vga_hsync_n_s,
 video_vs     => vga_vsync_n_s,
 audio_out    => audio,
  
  -- joy_s format MXYZ SACB RLDU	
 rt_1      => joyHBCPPFRLDU(5) or not joy1_s(5), -- button 4
 lf_1      => joyHBCPPFRLDU(6) or not joy1_s(4), -- button 3
 dn_1      => joyHBCPPFRLDU(7) or not joy1_s(6), -- button 2
 up_1      => joyHBCPPFRLDU(4) or not joy1_s(7), -- button 1
 pot_x_1   => pot_x,
 pot_y_1   => pot_y,

 rt_2      => not joy2_s(5), -- button 4
 lf_2      => not joy2_s(4), -- button 3
 dn_2      => not joy2_s(6), -- button 2
 up_2      => not joy2_s(7), -- button 1
 pot_x_2   => pot2_x,
 pot_y_2   => pot2_y,
		 
 leds       => open,
 sw			=> (others=>'0'),
 
 speakjet_cmd => open,
 speakjet_rdy => '0',
 speakjet_pwm => '0',
 
 external_speech_mode => "00",  -- "00" : no speech synth. "01" : sp0256. "10" : speakjet.  

 -- cartridge
 cpu_addr_o	=> cpu_addr,
 cart_do 	=> from_sram
 
);



--sound_string <= "00" & audio & "000" & "00" & audio & "000";

-- get scancode from keyboard

keyboard : entity work.io_ps2_keyboard
port map (
  clk       => clock_25, -- use same clock as main core
  kbd_clk   => ps2_clk_io,
  kbd_dat   => ps2_data_io,
  interrupt => kbd_intr,
  scancode  => kbd_scancode
);

-- translate scancode to joystick
joystick : entity work.kbd_joystick
port map (
  clk          => clock_25, -- use same clock as main core
  kbdint       => kbd_intr,
  kbdscancode  => std_logic_vector(kbd_scancode), 
  joyHBCPPFRLDU => joyHBCPPFRLDU,
  keys_HUA     => open, --keys_HUA
  osd_o			=> keys_s
);


analog_pot: process(clock_12p5)
begin
	if rising_edge(clock_12p5) then
	
--		pot_speed_cnt <= pot_speed_cnt-1;
--		if pot_speed_cnt = X"0000" then
--			pot_speed_cnt <= X"FFFF";
--		
--			if joyHBCPPFRLDU(2) = '1' then 
--				if pot_x <  127 then pot_x <= pot_x+1; end if;
--			end if;
--			if joyHBCPPFRLDU(3) = '1' then 
--				if pot_x > -128 then pot_x <= pot_x-1; end if;
--			end if;
--			if joyHBCPPFRLDU(0) = '1' then 
--				if pot_y <  127 then pot_y <= pot_y+1; end if;
--			end if;
--			if joyHBCPPFRLDU(1) = '1' then 
--				if pot_y > -128 then pot_y <= pot_y-1; end if;
--			end if;
--		end if;

		pot_x <= X"00";
		pot_y <= X"00";
		
		
		
		if joyHBCPPFRLDU(0) = '1' or joy1_s(0) = '0' then pot_y <= X"7F"; end if;
		if joyHBCPPFRLDU(1) = '1' or joy1_s(1) = '0' then pot_y <= X"80"; end if;
		if joyHBCPPFRLDU(2) = '1' or joy1_s(2) = '0' then pot_x <= X"80"; end if;
		if joyHBCPPFRLDU(3) = '1' or joy1_s(3) = '0' then pot_x <= X"7F"; end if;

		if joy2_s(0) = '0' then pot2_y <= X"7F"; end if;
		if joy2_s(1) = '0' then pot2_y <= X"80"; end if;
		if joy2_s(2) = '0' then pot2_x <= X"80"; end if;
		if joy2_s(3) = '0' then pot2_x <= X"7F"; end if;

	end if;	
end process;	 

-- Audio
audioout: entity work.dac
generic map (
	msbi_g		=> 9
)
port map (
	clk_i		=> clock_25,
	res_i		=> reset,
	dac_i		=> std_logic_vector(audio),
	dac_o		=> audio_dac_s
);

dac_l_o <= audio_dac_s;
dac_r_o <= audio_dac_s;

--------------------------------------------------------------------
-- Select cartridge here, select right rom length within port map

--cart_do <= (others => '0');  -- no cartridge

cart_rom : entity work.vectrex_scramble_prom
--cart_rom : entity work.vectrex_berzerk_prom
--cart_rom : entity work.vectrex_spacewar_prom
--cart_rom : entity work.vectrex_frogger_prom
--cart_rom : entity work.vectrex_polepos_prom
--cart_rom : entity work.vectrex_ripoff_prom
--cart_rom : entity work.vectrex_spike_prom
--cart_rom : entity work.vectrex_startrek_prom
--cart_rom : entity work.vectrex_vecmania1_prom
--cart_rom : entity work.vectrex_webwars_prom
--cart_rom : entity work.vectrex_wotr_prom

port map(
 clk  => clock_25, --cpu_clock,
 addr => cpu_addr(11 downto 0), -- scramble,berzerk,ripoff,spacewar,startrek
-- addr => cpu_addr(12 downto 0), -- polepos,spike,webwars
-- addr => cpu_addr(13 downto 0), -- frogger
-- addr => cpu_addr(14 downto 0), -- vecmania,wotr
 data => cart_do
);
--------------------------------------------------------------------



-- adapt video to 4bits/color only
vga_r_s <= r when blankn = '1' else "00000";
vga_g_s <= g when blankn = '1' else "00000";
vga_b_s <= b when blankn = '1' else "00000";

-- synchro composite/ synchro horizontale
--vga_hs <= csync;
--vga_hsync_n_s <= hsync;
-- commutation rapide / synchro verticale
--vga_vs <= '1';
--vga_vsync_n_s <= vsync;


OSB_BLOCK: block 

	type config_array is array(natural range 15 downto 0) of std_logic_vector(7 downto 0);

	component osd is
	generic
	(
		OSD_VISIBLE 	: std_logic_vector(1 downto 0) := (others=>'0');
		OSD_X_OFFSET : std_logic_vector(9 downto 0) := (others=>'0');
		OSD_Y_OFFSET : std_logic_vector(9 downto 0) := (others=>'0');
		OSD_COLOR    : std_logic_vector(2 downto 0) := (others=>'0')
	);
	port
	(
		-- OSDs pixel clock, should be synchronous to cores pixel clock to
		-- avoid jitter.
		pclk		: in std_logic;

		-- SPI interface
		sck		: in std_logic;
		ss			: in std_logic;
		sdi		: in std_logic;
		sdo		: out std_logic;

		-- VGA signals coming from core
		red_in 	: in std_logic_vector(4 downto 0);
		green_in : in std_logic_vector(4 downto 0);
		blue_in 	: in std_logic_vector(4 downto 0);
		hs_in		: in std_logic;
		vs_in		: in std_logic;
		
		-- VGA signals going to video connector
		red_out	: out std_logic_vector(4 downto 0);
		green_out: out std_logic_vector(4 downto 0);
		blue_out	: out std_logic_vector(4 downto 0);
		hs_out 	: out std_logic;
		vs_out 	: out std_logic;
		
		-- external data in to the microcontroller
		data_in 	: in std_logic_vector(7 downto 0);
		conf_str : in std_logic_vector( (STRLEN * 8)-1 downto 0);
		
		-- data pump to sram
		pump_active_o	: out std_logic := '0';
		sram_a_o 		: out std_logic_vector(18 downto 0);
		sram_d_o 		: out std_logic_vector(7 downto 0);
		sram_we_n_o 	: out std_logic := '1';
		
		config_buffer_o: out config_array
	
	);
	end component;
		
		alias SPI_DI  : std_logic is stm_b15_io;
		alias SPI_DO  : std_logic is stm_b14_io;
		alias SPI_SCK : std_logic is stm_b13_io;
		alias SPI_SS3 : std_logic is stm_b12_io;
		
		signal vga_r_out_s : std_logic_vector(4 downto 0);
		signal vga_g_out_s : std_logic_vector(4 downto 0);
		signal vga_b_out_s : std_logic_vector(4 downto 0);
		
		signal sram_addr_s : std_logic_vector(18 downto 0) := (others=>'1');
		signal sram_data_s : std_logic_vector(7 downto 0);
		signal sram_we_s 	 : std_logic := '1';
		
		signal power_on_s		: std_logic_vector(18 downto 0)	:= (others => '1');
		
		signal config_buffer_s : config_array;

		
	begin
		
		
		osd1 : osd 
		generic map
		(
			OSD_VISIBLE => "00", -- 00 - OSD start invisible, 01 - start visible
			OSD_COLOR => "001", -- RGB
			OSD_X_OFFSET => "0000010010", -- 50
			OSD_Y_OFFSET => "0000001111"  -- 15
		)
		port map
		(
			pclk       => clock_25,

			-- spi for OSD
			sdi        => SPI_DI,
			sck        => SPI_SCK,
			ss         => SPI_SS3,
			sdo        => SPI_DO,
			
			red_in     => vga_r_s,
			green_in   => vga_g_s,
			blue_in    => vga_b_s,
			hs_in      => vga_hsync_n_s,
			vs_in      => vga_vsync_n_s,

			red_out    => vga_r_out_s,
			green_out  => vga_g_out_s,
			blue_out   => vga_b_out_s,
			hs_out     => open,
			vs_out     => open,

			data_in		=> osd_s,
			conf_str		=> (others=>'0'),
			
			pump_active_o	=> pump_active_s,
			sram_a_o			=> sram_addr_s,
			sram_d_o			=> sram_data_s,
			sram_we_n_o		=> sram_we_s,
			config_buffer_o=> config_buffer_s
		);
					

		
		sram_addr_o   <= sram_addr_s when sram_we_s = '0' else "000" & cpu_addr;
		sram_data_io  <= sram_data_s when sram_we_s = '0' else (others=>'Z');
		from_sram 	   <= sram_data_io;
		sram_oe_n_o   <= '0';
		sram_we_n_o   <= sram_we_s;


		--start the microcontroller OSD menu after the power on
		process (clock_25, keys_s, osd_s)
		begin
			if rising_edge(clock_25) then
				if pump_active_s = '1' then
					power_on_s <= (others=>'1');
				elsif power_on_s /= x"00" then
					power_on_s <= power_on_s - 1;
					power_on_reset <= '1';
				else
					power_on_reset <= '0';
				end if;
				
				--if joy1_s(7) = '0' then --start on sega 6 buttons
				--	osd_s <= "001" & keys_s(4 downto 0); --open or close the OSD
				--else
					osd_s <= keys_s;
				--end if;
				
				osd_s(4 downto 0) <= keys_s(4 downto 0) and joy1_s(4 downto 0);
				
			end if;
		end process;

	
		
		---------
	
	-- HDMI
	block_hdmi: block
	
		signal tdms_r_s			: std_logic_vector( 9 downto 0);
		signal tdms_g_s			: std_logic_vector( 9 downto 0);
		signal tdms_b_s			: std_logic_vector( 9 downto 0);
		signal hdmi_p_s			: std_logic_vector( 3 downto 0);
		signal hdmi_n_s			: std_logic_vector( 3 downto 0);
	
	begin
 		inst_dvid: entity work.hdmi
 		generic map (
 			FREQ	=> 25200000,	-- pixel clock frequency 
 			FS		=> 48000,		-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
 			CTS	=> 25200,		-- CTS = Freq(pixclk) * N / (128 * Fs)
 			N		=> 6144			-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
 		) 
 		port map (
 			I_CLK_PIXEL		=> clock_25,
			
			I_R				=> vga_r_out_s & vga_r_out_s(4 downto 2),
			I_G				=> vga_g_out_s & vga_g_out_s(4 downto 2),
			I_B				=> vga_b_out_s & vga_b_out_s(4 downto 2),
			
			I_BLANK			=> not blankn,
			I_HSYNC			=> vga_hsync_n_s,
			I_VSYNC			=> vga_vsync_n_s,
			
			-- PCM audio
			I_AUDIO_ENABLE	=> '1',
			I_AUDIO_PCM_L 	=> "0" & std_logic_vector(audio) & "00000",
			I_AUDIO_PCM_R	=> "0" & std_logic_vector(audio) & "00000",
			
			-- TMDS parallel pixel synchronous outputs (serialize LSB first)
 			O_RED				=> tdms_r_s,
			O_GREEN			=> tdms_g_s,
			O_BLUE			=> tdms_b_s
		);
		

			hdmio: entity work.hdmi_out_altera
		port map (
			clock_pixel_i		=> clock_25,
			clock_tdms_i		=> clock_hdmi,
			red_i					=> tdms_r_s,
			green_i				=> tdms_g_s,
			blue_i				=> tdms_b_s,
			tmds_out_p			=> hdmi_p_s,
			tmds_out_n			=> hdmi_n_s
		);
 		
		
		tmds_o(7)	<= hdmi_p_s(2);	-- 2+		
		tmds_o(6)	<= hdmi_n_s(2);	-- 2-		
		tmds_o(5)	<= hdmi_p_s(1);	-- 1+			
		tmds_o(4)	<= hdmi_n_s(1);	-- 1-		
		tmds_o(3)	<= hdmi_p_s(0);	-- 0+		
		tmds_o(2)	<= hdmi_n_s(0);	-- 0-	
		tmds_o(1)	<= hdmi_p_s(3);	-- CLK+	
		tmds_o(0)	<= hdmi_n_s(3);	-- CLK-	
		
	end block;
		
		vga_r_o			<= vga_r_out_s;
		vga_g_o			<= vga_g_out_s;
		vga_b_o			<= vga_b_out_s;
		vga_hsync_n_o	<= vga_hsync_n_s;
		vga_vsync_n_o	<= vga_vsync_n_s;
		
		
		
		
--- Joystick read with sega 6 button support----------------------

	process(vga_hsync_n_s, reset_n)
		variable state_v : unsigned(7 downto 0) := (others=>'0');
		variable j1_sixbutton_v : std_logic := '0';
		variable j2_sixbutton_v : std_logic := '0';
	begin
		if falling_edge(vga_hsync_n_s) then
		
			if reset_n = '0' then
				state_v := (others=>'0');
			else
				state_v := state_v + 1;
			end if;
			
			case state_v is
				-- joy_s format MXYZ SACB RLDU
			
				when X"00" =>  
					joyP7_s <= '0';
					
				when X"01" =>
					joyP7_s <= '1';

				when X"02" => 
					joy1_s(3 downto 0) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- R, L, D, U
					joy2_s(3 downto 0) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- R, L, D, U
					joy1_s(5 downto 4) <= joy1_p9_i & joy1_p6_i; -- C, B
					joy2_s(5 downto 4) <= joy2_p9_i & joy2_p6_i; -- C, B					
					joyP7_s <= '0';
					j1_sixbutton_v := '0'; -- Assume it's not a six-button controller
					j2_sixbutton_v := '0'; -- Assume it's not a six-button controller

				when X"03" =>
					joy1_s(7 downto 6) <= joy1_p9_i & joy1_p6_i; -- Start, A
					joy2_s(7 downto 6) <= joy2_p9_i & joy2_p6_i; -- Start, A
					joyP7_s <= '1';
			
				when X"04" =>  
					joyP7_s <= '0';

				when X"05" =>
					if joy1_right_i = '0' and joy1_left_i = '0' and joy1_down_i = '0' and joy1_up_i = '0' then 
						j1_sixbutton_v := '1'; --it's a six button
					end if;
					
					if joy2_right_i = '0' and joy2_left_i = '0' and joy2_down_i = '0' and joy2_up_i = '0' then 
						j2_sixbutton_v := '1'; --it's a six button
					end if;
					
					joyP7_s <= '1';
					
				when X"06" =>
					if j1_sixbutton_v = '1' then
						joy1_s(11 downto 8) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- Mode, X, Y e Z
					end if;
					
					if j2_sixbutton_v = '1' then
						joy2_s(11 downto 8) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- Mode, X, Y e Z
					end if;
					
					joyP7_s <= '0';

				when others =>
					joyP7_s <= '1';
					
			end case;

		end if;
	end process;
	
	joyX_p7_o <= joyP7_s;
---------------------------
		
end block;





end Behavior;
