library ieee;
use ieee.std_logic_1164.all,ieee.numeric_std.all;

entity vectrex_ripoff_prom is
port (
	clk  : in  std_logic;
	addr : in  std_logic_vector(11 downto 0);
	data : out std_logic_vector(7 downto 0)
);
end entity;

architecture prom of vectrex_ripoff_prom is
	type rom is array(0 to  4095) of std_logic_vector(7 downto 0);
	signal rom_data: rom := (
		X"67",X"20",X"47",X"43",X"45",X"20",X"31",X"39",X"38",X"32",X"80",X"0E",X"CD",X"FB",X"40",X"50",
		X"C0",X"52",X"49",X"50",X"2D",X"4F",X"46",X"46",X"80",X"F8",X"48",X"68",X"0D",X"30",X"20",X"20",
		X"80",X"00",X"7D",X"C8",X"3B",X"27",X"50",X"BD",X"F1",X"AA",X"BD",X"F1",X"BA",X"B6",X"C8",X"0F",
		X"81",X"0B",X"26",X"43",X"CC",X"FB",X"40",X"FD",X"C8",X"2A",X"CE",X"00",X"4A",X"BD",X"F3",X"8A",
		X"BD",X"0E",X"C1",X"7A",X"C8",X"C3",X"27",X"2F",X"20",X"F0",X"20",X"90",X"50",X"52",X"4F",X"47",
		X"52",X"41",X"4D",X"4D",X"45",X"44",X"20",X"42",X"59",X"80",X"E0",X"90",X"57",X"49",X"4C",X"4C",
		X"49",X"41",X"4D",X"20",X"48",X"41",X"57",X"4B",X"49",X"4E",X"53",X"80",X"90",X"C0",X"47",X"54",
		X"20",X"31",X"39",X"38",X"32",X"80",X"00",X"7F",X"CB",X"00",X"8E",X"C8",X"80",X"BD",X"F5",X"45",
		X"8E",X"C9",X"00",X"BD",X"F5",X"45",X"8E",X"CA",X"00",X"BD",X"F5",X"45",X"BD",X"F1",X"AF",X"86",
		X"20",X"97",X"D8",X"97",X"D9",X"8E",X"C9",X"14",X"C6",X"06",X"F7",X"C9",X"0F",X"F7",X"C9",X"2F",
		X"F7",X"C9",X"0A",X"BD",X"F5",X"52",X"8E",X"C9",X"34",X"C6",X"06",X"D7",X"D5",X"F7",X"C9",X"2A",
		X"BD",X"F5",X"52",X"86",X"30",X"B7",X"C9",X"1A",X"B7",X"C9",X"3A",X"86",X"2B",X"97",X"BE",X"CC",
		X"30",X"80",X"DD",X"C1",X"F7",X"C9",X"1B",X"F7",X"C9",X"3B",X"F7",X"C9",X"01",X"F7",X"C9",X"21",
		X"D7",X"CC",X"F7",X"C9",X"41",X"F7",X"C9",X"61",X"F7",X"C9",X"81",X"CC",X"C9",X"D0",X"FD",X"C9",
		X"10",X"CC",X"CA",X"04",X"FD",X"C9",X"12",X"CC",X"CA",X"04",X"FD",X"C9",X"30",X"CC",X"CA",X"38",
		X"FD",X"C9",X"32",X"86",X"02",X"97",X"CD",X"0C",X"CE",X"BD",X"0E",X"9B",X"BD",X"F1",X"AF",X"CC",
		X"C8",X"AF",X"DD",X"3D",X"CC",X"C9",X"50",X"DD",X"D2",X"0F",X"20",X"0F",X"22",X"03",X"93",X"8E",
		X"C9",X"A0",X"CE",X"01",X"34",X"86",X"27",X"BD",X"F6",X"83",X"C6",X"08",X"96",X"C6",X"85",X"40",
		X"27",X"0B",X"CC",X"80",X"14",X"8E",X"C9",X"B4",X"BD",X"F5",X"52",X"C6",X"04",X"D7",X"C7",X"D7",
		X"C8",X"7E",X"01",X"C2",X"00",X"00",X"07",X"00",X"00",X"00",X"00",X"F8",X"00",X"00",X"00",X"00",
		X"00",X"0F",X"00",X"00",X"00",X"00",X"F0",X"00",X"00",X"00",X"0F",X"0F",X"00",X"00",X"00",X"0F",
		X"F0",X"00",X"00",X"00",X"F0",X"0F",X"00",X"00",X"00",X"F0",X"F0",X"00",X"BD",X"F5",X"11",X"1F",
		X"89",X"84",X"01",X"C4",X"E0",X"97",X"AC",X"86",X"80",X"90",X"AC",X"39",X"0F",X"CB",X"0F",X"CA",
		X"8D",X"EA",X"2B",X"0F",X"97",X"AC",X"86",X"20",X"B7",X"C9",X"40",X"B7",X"C9",X"60",X"B7",X"C9",
		X"80",X"96",X"AC",X"FD",X"C9",X"43",X"FD",X"C9",X"63",X"FD",X"C9",X"83",X"96",X"CE",X"B7",X"C9",
		X"4A",X"B7",X"C9",X"6A",X"B7",X"C9",X"8A",X"7F",X"C9",X"41",X"0C",X"CB",X"7F",X"C9",X"61",X"0C",
		X"CB",X"0D",X"C6",X"2B",X"04",X"0D",X"D7",X"27",X"05",X"7F",X"C9",X"81",X"0C",X"CB",X"10",X"8E",
		X"C9",X"50",X"A6",X"31",X"2B",X"03",X"BD",X"0C",X"2F",X"31",X"A8",X"20",X"10",X"8C",X"C9",X"91",
		X"25",X"F0",X"86",X"FF",X"97",X"B5",X"BD",X"F3",X"4A",X"7C",X"C8",X"24",X"B6",X"C9",X"01",X"85",
		X"40",X"27",X"25",X"8E",X"C9",X"03",X"BD",X"F3",X"0C",X"8E",X"C9",X"0D",X"BD",X"0C",X"9A",X"24",
		X"10",X"7F",X"C9",X"0D",X"B6",X"C9",X"01",X"84",X"BF",X"B7",X"C9",X"01",X"BD",X"F3",X"54",X"20",
		X"24",X"D7",X"04",X"BD",X"F4",X"6E",X"20",X"1D",X"4D",X"2B",X"1A",X"8E",X"0C",X"DF",X"CE",X"CA",
		X"38",X"B6",X"C9",X"00",X"BD",X"F6",X"1F",X"8E",X"C9",X"03",X"BD",X"F3",X"0C",X"8E",X"CA",X"38",
		X"C6",X"0D",X"BD",X"F4",X"0E",X"B6",X"C9",X"21",X"85",X"40",X"27",X"25",X"8E",X"C9",X"23",X"BD",
		X"F3",X"0C",X"8E",X"C9",X"2D",X"BD",X"0C",X"9A",X"24",X"10",X"7F",X"C9",X"2D",X"B6",X"C9",X"21",
		X"84",X"BF",X"B7",X"C9",X"21",X"BD",X"F3",X"54",X"20",X"24",X"D7",X"04",X"BD",X"F4",X"6E",X"20",
		X"1D",X"4D",X"2B",X"1A",X"8E",X"0D",X"13",X"CE",X"CA",X"38",X"B6",X"C9",X"20",X"BD",X"F6",X"1F",
		X"8E",X"C9",X"23",X"BD",X"F3",X"0C",X"8E",X"CA",X"38",X"C6",X"0D",X"BD",X"F4",X"0E",X"CE",X"C9",
		X"A0",X"BD",X"0A",X"36",X"25",X"03",X"33",X"C8",X"14",X"30",X"C8",X"14",X"BF",X"C8",X"AC",X"A6",
		X"C4",X"2B",X"0D",X"30",X"42",X"BD",X"F3",X"0C",X"C6",X"0A",X"8E",X"0E",X"8E",X"BD",X"F4",X"0E",
		X"33",X"45",X"11",X"B3",X"C8",X"AC",X"25",X"E7",X"10",X"8E",X"C9",X"50",X"7F",X"C8",X"24",X"A6",
		X"31",X"85",X"40",X"27",X"1D",X"30",X"33",X"BD",X"F3",X"0C",X"30",X"3D",X"BD",X"0C",X"9A",X"24",
		X"0A",X"6F",X"3D",X"A6",X"31",X"84",X"BF",X"A7",X"31",X"20",X"07",X"D7",X"04",X"BD",X"F4",X"6E",
		X"20",X"45",X"4D",X"2B",X"42",X"F6",X"C8",X"CF",X"8E",X"0E",X"19",X"58",X"AE",X"85",X"85",X"08",
		X"26",X"02",X"30",X"03",X"A6",X"30",X"CE",X"CA",X"38",X"BD",X"F6",X"1F",X"30",X"33",X"BD",X"F3",
		X"0C",X"C6",X"0D",X"8E",X"CA",X"38",X"BD",X"F4",X"0E",X"7D",X"C8",X"CC",X"26",X"19",X"A6",X"31",
		X"84",X"10",X"27",X"13",X"10",X"BC",X"C8",X"D2",X"26",X"0D",X"86",X"FF",X"A7",X"36",X"6C",X"39",
		X"C6",X"15",X"30",X"36",X"BD",X"F4",X"0E",X"BD",X"F3",X"54",X"31",X"A8",X"20",X"10",X"8C",X"C9",
		X"92",X"10",X"25",X"FF",X"8A",X"8E",X"C9",X"10",X"10",X"AE",X"84",X"E6",X"A4",X"27",X"41",X"6A",
		X"2B",X"27",X"3B",X"6A",X"2C",X"27",X"37",X"EC",X"27",X"E3",X"21",X"28",X"02",X"8D",X"22",X"ED",
		X"27",X"A7",X"25",X"EC",X"29",X"E3",X"23",X"28",X"02",X"8D",X"16",X"ED",X"29",X"A7",X"26",X"34",
		X"10",X"30",X"25",X"C6",X"7F",X"D7",X"04",X"BD",X"F2",X"C1",X"BD",X"F3",X"54",X"35",X"10",X"20",
		X"0F",X"1F",X"03",X"B6",X"C8",X"C6",X"44",X"24",X"03",X"1F",X"30",X"39",X"35",X"40",X"6F",X"A4",
		X"31",X"2D",X"10",X"AC",X"02",X"26",X"B4",X"30",X"88",X"20",X"8C",X"C9",X"31",X"25",X"A9",X"7D",
		X"C8",X"D0",X"27",X"1F",X"CE",X"03",X"75",X"BD",X"F3",X"78",X"CC",X"20",X"F0",X"CE",X"C8",X"BE",
		X"BD",X"F3",X"7A",X"20",X"0E",X"40",X"E0",X"42",X"4F",X"4E",X"55",X"53",X"20",X"4C",X"45",X"56",
		X"45",X"4C",X"80",X"CC",X"7F",X"10",X"BD",X"F2",X"FC",X"CC",X"10",X"00",X"CE",X"C9",X"14",X"BD",
		X"F3",X"7A",X"B6",X"C8",X"C6",X"2A",X"0F",X"CC",X"7F",X"90",X"BD",X"F2",X"FC",X"CC",X"10",X"00",
		X"CE",X"C9",X"34",X"BD",X"F3",X"7A",X"BD",X"F2",X"A1",X"10",X"8E",X"C8",X"96",X"7D",X"C8",X"C4",
		X"26",X"07",X"B6",X"C9",X"0F",X"81",X"14",X"22",X"0A",X"8E",X"C8",X"9C",X"C6",X"08",X"BD",X"F5",
		X"3F",X"6F",X"A4",X"A6",X"A4",X"2A",X"10",X"EC",X"21",X"BD",X"F2",X"FC",X"8E",X"0E",X"25",X"CE",
		X"C8",X"9C",X"8D",X"31",X"BD",X"F3",X"54",X"31",X"23",X"7D",X"C8",X"C4",X"26",X"07",X"B6",X"C9",
		X"2F",X"81",X"14",X"22",X"0A",X"8E",X"C8",X"A4",X"C6",X"08",X"BD",X"F5",X"3F",X"6F",X"A4",X"A6",
		X"A4",X"2A",X"31",X"EC",X"21",X"BD",X"F2",X"FC",X"8E",X"0E",X"25",X"CE",X"C8",X"A4",X"8D",X"05",
		X"BD",X"F3",X"54",X"20",X"1F",X"A6",X"C4",X"AB",X"80",X"A7",X"C0",X"44",X"44",X"44",X"34",X"02",
		X"97",X"04",X"BD",X"F3",X"10",X"BD",X"F3",X"D6",X"35",X"02",X"97",X"04",X"BD",X"F3",X"10",X"A6",
		X"84",X"2A",X"E2",X"39",X"BD",X"0E",X"C1",X"7D",X"C8",X"C4",X"27",X"2E",X"BD",X"F2",X"72",X"CE",
		X"04",X"54",X"BD",X"F3",X"78",X"B6",X"C8",X"B5",X"26",X"07",X"B6",X"C8",X"11",X"10",X"26",X"FC",
		X"39",X"7A",X"C8",X"C3",X"26",X"08",X"7A",X"C8",X"C4",X"27",X"06",X"7F",X"C8",X"B5",X"7E",X"01",
		X"C6",X"7E",X"F0",X"00",X"40",X"E8",X"45",X"4E",X"44",X"80",X"BD",X"F1",X"AF",X"0C",X"D4",X"96",
		X"D5",X"91",X"D4",X"26",X"1F",X"0F",X"D4",X"96",X"D3",X"8B",X"20",X"81",X"B0",X"26",X"02",X"86",
		X"50",X"97",X"D3",X"10",X"9E",X"D2",X"A6",X"31",X"2B",X"0A",X"85",X"10",X"27",X"06",X"96",X"95",
		X"8A",X"02",X"97",X"95",X"10",X"8E",X"C9",X"50",X"0D",X"CC",X"27",X"0B",X"0A",X"CC",X"10",X"26",
		X"04",X"CE",X"0F",X"D0",X"7E",X"01",X"6C",X"6D",X"25",X"27",X"0D",X"6A",X"25",X"27",X"03",X"7E",
		X"07",X"E4",X"A6",X"31",X"84",X"7F",X"A7",X"31",X"6D",X"31",X"10",X"2B",X"03",X"36",X"CE",X"C8",
		X"96",X"DF",X"AC",X"8E",X"C9",X"10",X"EE",X"84",X"EC",X"02",X"DD",X"B2",X"6D",X"11",X"2B",X"3B",
		X"34",X"70",X"AE",X"13",X"CC",X"10",X"10",X"8D",X"16",X"35",X"70",X"24",X"2E",X"A6",X"11",X"8A",
		X"80",X"A7",X"11",X"86",X"28",X"A7",X"1F",X"DE",X"AC",X"BD",X"08",X"F6",X"7E",X"05",X"74",X"1F",
		X"03",X"A6",X"31",X"E6",X"30",X"10",X"AE",X"33",X"85",X"08",X"27",X"0A",X"86",X"09",X"BD",X"F6",
		X"01",X"1E",X"30",X"7E",X"F8",X"F3",X"1F",X"30",X"7E",X"F8",X"FF",X"6D",X"C4",X"27",X"0F",X"34",
		X"70",X"AE",X"45",X"CC",X"07",X"07",X"8D",X"D7",X"35",X"70",X"10",X"25",X"00",X"5A",X"33",X"4D",
		X"11",X"93",X"B2",X"25",X"E6",X"6D",X"11",X"2B",X"1D",X"A6",X"31",X"85",X"28",X"26",X"17",X"34",
		X"32",X"AE",X"13",X"10",X"AE",X"33",X"DC",X"D8",X"BD",X"F8",X"FF",X"35",X"32",X"24",X"07",X"8A",
		X"10",X"A7",X"31",X"BD",X"05",X"A8",X"DE",X"AC",X"33",X"43",X"DF",X"AC",X"30",X"88",X"20",X"8C",
		X"C9",X"31",X"10",X"25",X"FF",X"70",X"A6",X"31",X"85",X"20",X"10",X"27",X"00",X"73",X"6C",X"30",
		X"A6",X"3C",X"A1",X"30",X"10",X"26",X"02",X"8C",X"86",X"DF",X"A4",X"31",X"8A",X"08",X"A7",X"31",
		X"BD",X"01",X"5C",X"ED",X"3E",X"7E",X"07",X"E4",X"6F",X"C4",X"E6",X"31",X"CA",X"40",X"E7",X"31",
		X"86",X"80",X"97",X"95",X"6C",X"1E",X"0C",X"CA",X"30",X"04",X"4F",X"D6",X"CF",X"5C",X"BD",X"F8",
		X"7C",X"4F",X"D6",X"D7",X"BD",X"F8",X"7C",X"E6",X"31",X"C5",X"28",X"10",X"27",X"02",X"63",X"C5",
		X"08",X"26",X"02",X"8D",X"13",X"8D",X"09",X"86",X"BC",X"A4",X"85",X"A7",X"85",X"7E",X"07",X"F5",
		X"E6",X"3B",X"C4",X"3F",X"8E",X"C9",X"A0",X"39",X"A6",X"3B",X"2A",X"14",X"84",X"7F",X"A7",X"3B",
		X"96",X"D1",X"91",X"C8",X"24",X"0A",X"8D",X"E8",X"A6",X"85",X"84",X"DF",X"A7",X"85",X"0A",X"D1",
		X"39",X"BD",X"0A",X"36",X"24",X"09",X"A6",X"3A",X"91",X"CE",X"24",X"01",X"4C",X"A7",X"3A",X"85",
		X"08",X"26",X"5A",X"A6",X"3B",X"84",X"40",X"26",X"54",X"A6",X"24",X"2B",X"50",X"8E",X"C9",X"50",
		X"10",X"9F",X"AC",X"9C",X"AC",X"27",X"3E",X"A6",X"11",X"2B",X"3A",X"A6",X"04",X"2B",X"36",X"34",
		X"30",X"EC",X"33",X"1F",X"02",X"A0",X"13",X"E0",X"14",X"AE",X"13",X"DD",X"B2",X"CC",X"10",X"10",
		X"BD",X"F8",X"FF",X"35",X"30",X"24",X"1E",X"8D",X"9F",X"DC",X"B2",X"26",X"06",X"A6",X"30",X"8B",
		X"30",X"20",X"03",X"BD",X"F5",X"93",X"A7",X"23",X"86",X"90",X"A7",X"24",X"A6",X"1B",X"8A",X"40",
		X"A7",X"1B",X"7E",X"06",X"DE",X"30",X"88",X"20",X"8C",X"C9",X"91",X"25",X"B6",X"6D",X"24",X"2A",
		X"23",X"6A",X"24",X"2A",X"05",X"A6",X"23",X"7E",X"06",X"DE",X"6F",X"24",X"B6",X"C9",X"4B",X"84",
		X"BF",X"B7",X"C9",X"4B",X"B6",X"C9",X"6B",X"84",X"BF",X"B7",X"C9",X"6B",X"B6",X"C9",X"8B",X"84",
		X"BF",X"B7",X"C9",X"8B",X"EC",X"33",X"DD",X"B6",X"0F",X"B9",X"96",X"C6",X"85",X"02",X"26",X"06",
		X"03",X"B9",X"07",X"B6",X"07",X"B7",X"A6",X"31",X"85",X"10",X"27",X"58",X"6F",X"22",X"FC",X"C9",
		X"03",X"0D",X"B9",X"27",X"02",X"47",X"57",X"90",X"B6",X"D0",X"B7",X"DD",X"AE",X"BD",X"F5",X"84",
		X"97",X"AC",X"96",X"C6",X"2A",X"2E",X"D1",X"AC",X"23",X"02",X"D7",X"AC",X"FC",X"C9",X"23",X"0D",
		X"B9",X"27",X"02",X"47",X"57",X"90",X"B6",X"D0",X"B7",X"DD",X"B0",X"BD",X"F5",X"84",X"97",X"AD",
		X"D1",X"AD",X"24",X"02",X"D6",X"AD",X"7D",X"C9",X"21",X"2B",X"09",X"7D",X"C9",X"01",X"2B",X"0A",
		X"D1",X"AC",X"23",X"06",X"DC",X"AE",X"ED",X"A4",X"20",X"21",X"86",X"20",X"A7",X"22",X"DC",X"B0",
		X"ED",X"A4",X"20",X"17",X"85",X"08",X"26",X"07",X"E6",X"3B",X"2B",X"03",X"BD",X"0C",X"2F",X"EC",
		X"3E",X"0D",X"B9",X"27",X"02",X"47",X"57",X"90",X"B6",X"D0",X"B7",X"BD",X"F5",X"93",X"80",X"10",
		X"28",X"01",X"4A",X"84",X"3F",X"97",X"AE",X"A6",X"30",X"84",X"3F",X"90",X"AE",X"97",X"AD",X"BD",
		X"F5",X"84",X"84",X"3F",X"97",X"AF",X"97",X"AC",X"86",X"40",X"90",X"AC",X"84",X"3F",X"91",X"AC",
		X"22",X"02",X"97",X"AC",X"96",X"CD",X"91",X"AC",X"24",X"02",X"20",X"3C",X"96",X"AE",X"A7",X"30",
		X"A6",X"31",X"84",X"10",X"27",X"4C",X"10",X"9C",X"D2",X"26",X"47",X"34",X"20",X"8E",X"C9",X"03",
		X"E6",X"22",X"AE",X"85",X"10",X"AE",X"33",X"CC",X"30",X"30",X"BD",X"F8",X"FF",X"35",X"20",X"24",
		X"31",X"E6",X"22",X"8E",X"C9",X"01",X"A6",X"85",X"8A",X"C0",X"A7",X"85",X"86",X"28",X"8E",X"C9",
		X"0F",X"A7",X"85",X"BD",X"09",X"00",X"20",X"1A",X"86",X"20",X"91",X"AF",X"25",X"02",X"00",X"AD",
		X"0D",X"AD",X"2A",X"08",X"A6",X"30",X"90",X"CD",X"A7",X"30",X"20",X"06",X"A6",X"30",X"9B",X"CD",
		X"A7",X"30",X"E6",X"30",X"86",X"7F",X"BD",X"F6",X"01",X"ED",X"37",X"6F",X"36",X"6F",X"39",X"A6",
		X"3A",X"10",X"27",X"00",X"6F",X"97",X"AF",X"EC",X"36",X"BD",X"0A",X"6F",X"A6",X"31",X"85",X"08",
		X"27",X"16",X"BD",X"05",X"A0",X"3A",X"EC",X"01",X"1E",X"89",X"D3",X"AC",X"1E",X"89",X"10",X"29",
		X"01",X"0F",X"ED",X"01",X"ED",X"32",X"20",X"0F",X"EC",X"32",X"1E",X"89",X"D3",X"AC",X"28",X"03",
		X"BD",X"09",X"1D",X"1E",X"89",X"ED",X"32",X"A6",X"3A",X"97",X"AF",X"EC",X"38",X"1E",X"89",X"BD",
		X"0A",X"6F",X"A6",X"31",X"85",X"08",X"27",X"0E",X"EC",X"03",X"D3",X"AC",X"10",X"29",X"00",X"E1",
		X"ED",X"03",X"ED",X"34",X"20",X"0B",X"EC",X"34",X"D3",X"AC",X"28",X"03",X"BD",X"09",X"2D",X"ED",
		X"34",X"34",X"20",X"AE",X"3E",X"10",X"AE",X"33",X"CC",X"09",X"09",X"BD",X"F8",X"FF",X"35",X"20",
		X"10",X"25",X"00",X"BD",X"31",X"A8",X"20",X"10",X"8C",X"C9",X"91",X"10",X"25",X"FC",X"A8",X"7E",
		X"09",X"60",X"BD",X"05",X"A8",X"A6",X"31",X"8A",X"80",X"A7",X"31",X"8E",X"C9",X"A0",X"5F",X"E7",
		X"30",X"E7",X"3B",X"A6",X"84",X"10",X"2A",X"00",X"23",X"30",X"05",X"5C",X"C1",X"08",X"25",X"F3",
		X"86",X"7F",X"97",X"CC",X"B7",X"CB",X"00",X"D7",X"C4",X"8E",X"C9",X"14",X"8D",X"08",X"8E",X"C9",
		X"34",X"8D",X"03",X"7E",X"01",X"C2",X"CE",X"CB",X"EB",X"7E",X"F8",X"D8",X"8E",X"C9",X"50",X"A6",
		X"11",X"2A",X"B1",X"A6",X"05",X"26",X"AD",X"30",X"88",X"20",X"8C",X"C9",X"91",X"25",X"F0",X"96",
		X"CA",X"27",X"45",X"0C",X"CF",X"91",X"CB",X"26",X"18",X"96",X"C6",X"2A",X"04",X"03",X"C9",X"2B",
		X"10",X"0C",X"CF",X"96",X"D8",X"81",X"70",X"23",X"02",X"86",X"6E",X"8B",X"01",X"97",X"D8",X"97",
		X"D9",X"86",X"07",X"91",X"CF",X"22",X"21",X"96",X"D7",X"8B",X"01",X"19",X"97",X"D7",X"03",X"D0",
		X"1F",X"89",X"C4",X"0F",X"CA",X"30",X"84",X"F0",X"26",X"04",X"86",X"20",X"20",X"06",X"44",X"44",
		X"44",X"44",X"8A",X"30",X"DD",X"BF",X"0F",X"CF",X"0A",X"CF",X"2C",X"02",X"0F",X"CF",X"96",X"CF",
		X"4C",X"48",X"97",X"CE",X"0D",X"D7",X"26",X"02",X"0A",X"CE",X"86",X"80",X"97",X"CC",X"7E",X"09",
		X"60",X"A6",X"31",X"85",X"08",X"27",X"10",X"0A",X"C7",X"8A",X"80",X"A7",X"31",X"BD",X"05",X"A0",
		X"86",X"80",X"A7",X"85",X"7E",X"07",X"F5",X"E6",X"3B",X"10",X"2A",X"FF",X"27",X"8A",X"20",X"84",
		X"EF",X"A7",X"31",X"BD",X"05",X"A0",X"3A",X"86",X"40",X"A7",X"84",X"6F",X"3A",X"A6",X"30",X"8B",
		X"20",X"A7",X"3C",X"D7",X"B8",X"10",X"9F",X"B6",X"8E",X"C9",X"50",X"9C",X"B6",X"27",X"0C",X"A6",
		X"1B",X"84",X"3F",X"91",X"B8",X"26",X"04",X"84",X"7F",X"A7",X"1B",X"30",X"88",X"20",X"8C",X"C9",
		X"95",X"25",X"E8",X"7E",X"07",X"E4",X"A6",X"C4",X"EC",X"13",X"ED",X"41",X"4F",X"43",X"A7",X"C4",
		X"86",X"80",X"97",X"95",X"B6",X"C9",X"41",X"84",X"EF",X"B7",X"C9",X"41",X"B6",X"C9",X"61",X"84",
		X"EF",X"B7",X"C9",X"61",X"B6",X"C9",X"81",X"84",X"EF",X"B7",X"C9",X"81",X"39",X"DD",X"B3",X"EC",
		X"32",X"1E",X"89",X"DD",X"B1",X"30",X"34",X"A6",X"84",X"97",X"BB",X"20",X"0C",X"DD",X"B3",X"EC",
		X"34",X"DD",X"B1",X"30",X"33",X"A6",X"84",X"97",X"BB",X"96",X"C6",X"85",X"02",X"26",X"1E",X"86",
		X"14",X"A7",X"25",X"A6",X"30",X"8B",X"20",X"A7",X"30",X"6F",X"24",X"A6",X"31",X"8A",X"80",X"A7",
		X"31",X"BD",X"F5",X"11",X"84",X"0F",X"9B",X"BB",X"A7",X"84",X"DC",X"B1",X"39",X"DC",X"B3",X"39",
		X"96",X"1B",X"B7",X"C9",X"1C",X"96",X"1D",X"B7",X"C9",X"3C",X"96",X"0F",X"1F",X"89",X"58",X"58",
		X"58",X"58",X"84",X"F0",X"C4",X"F0",X"F7",X"C9",X"0B",X"B7",X"C9",X"2B",X"CE",X"C8",X"96",X"7D",
		X"C9",X"01",X"2B",X"29",X"7D",X"C9",X"21",X"2B",X"24",X"BE",X"C9",X"03",X"10",X"BE",X"C9",X"23",
		X"CC",X"10",X"10",X"BD",X"F8",X"FF",X"24",X"15",X"86",X"80",X"B7",X"C9",X"01",X"B7",X"C9",X"21",
		X"44",X"B7",X"C9",X"0F",X"B7",X"C9",X"2F",X"8E",X"C9",X"10",X"BD",X"08",X"F6",X"8E",X"C9",X"10",
		X"6D",X"1F",X"27",X"1F",X"6A",X"1F",X"10",X"26",X"01",X"30",X"6F",X"11",X"86",X"06",X"A7",X"1A",
		X"86",X"10",X"CE",X"F0",X"7F",X"8C",X"C9",X"10",X"27",X"05",X"86",X"30",X"CE",X"0F",X"80",X"EF",
		X"13",X"A7",X"10",X"A6",X"1B",X"85",X"30",X"27",X"12",X"85",X"10",X"27",X"14",X"6C",X"10",X"8D",
		X"55",X"24",X"0E",X"85",X"40",X"27",X"0A",X"6C",X"10",X"20",X"06",X"E6",X"0C",X"2D",X"EE",X"2E",
		X"06",X"A6",X"1B",X"85",X"20",X"27",X"0C",X"6A",X"10",X"8D",X"3B",X"24",X"06",X"85",X"40",X"27",
		X"02",X"6A",X"10",X"E6",X"10",X"86",X"7F",X"BD",X"F6",X"01",X"ED",X"17",X"A6",X"1B",X"85",X"40",
		X"27",X"08",X"A6",X"1A",X"81",X"0A",X"24",X"02",X"6C",X"1A",X"A6",X"1B",X"85",X"80",X"27",X"5B",
		X"A6",X"1C",X"85",X"80",X"26",X"55",X"10",X"AE",X"84",X"A6",X"A4",X"27",X"0E",X"31",X"2D",X"10",
		X"AC",X"02",X"26",X"F5",X"20",X"45",X"F6",X"C8",X"26",X"54",X"39",X"96",X"95",X"8A",X"01",X"97",
		X"95",X"EC",X"12",X"1E",X"89",X"ED",X"27",X"EC",X"14",X"ED",X"29",X"A6",X"1A",X"8B",X"0B",X"34",
		X"02",X"97",X"AF",X"EC",X"16",X"8D",X"18",X"ED",X"21",X"35",X"02",X"97",X"AF",X"EC",X"18",X"1E",
		X"89",X"8D",X"0C",X"ED",X"23",X"86",X"30",X"A7",X"2B",X"A7",X"2C",X"63",X"A4",X"20",X"0C",X"1D",
		X"DD",X"AC",X"D3",X"AC",X"0A",X"AF",X"26",X"FA",X"DD",X"AC",X"39",X"8D",X"B9",X"24",X"02",X"6A",
		X"1A",X"A6",X"1A",X"2F",X"63",X"97",X"AF",X"EC",X"16",X"8D",X"E4",X"EC",X"12",X"1E",X"89",X"D3",
		X"AC",X"28",X"1B",X"8D",X"4A",X"26",X"17",X"93",X"AC",X"34",X"06",X"A6",X"10",X"88",X"10",X"85",
		X"10",X"27",X"05",X"84",X"30",X"4C",X"20",X"02",X"8A",X"0F",X"A7",X"10",X"35",X"06",X"1E",X"89",
		X"ED",X"12",X"A6",X"1A",X"97",X"AF",X"E6",X"18",X"8D",X"B5",X"EC",X"14",X"D3",X"AC",X"28",X"1B",
		X"8D",X"1D",X"26",X"17",X"93",X"AC",X"34",X"06",X"A6",X"10",X"88",X"30",X"85",X"10",X"27",X"04",
		X"8A",X"0F",X"20",X"03",X"84",X"30",X"4C",X"A7",X"10",X"35",X"06",X"ED",X"14",X"20",X"0B",X"1F",
		X"03",X"96",X"C6",X"85",X"04",X"1F",X"30",X"39",X"6F",X"1A",X"8C",X"C9",X"30",X"27",X"0A",X"96",
		X"C6",X"2A",X"06",X"30",X"88",X"20",X"7E",X"09",X"B0",X"B6",X"C9",X"0B",X"B7",X"C9",X"0C",X"B6",
		X"C9",X"2B",X"B7",X"C9",X"2C",X"96",X"95",X"2B",X"4D",X"0D",X"90",X"10",X"26",X"00",X"63",X"0D",
		X"94",X"26",X"06",X"86",X"04",X"97",X"94",X"03",X"93",X"0A",X"94",X"96",X"95",X"85",X"01",X"27",
		X"0B",X"84",X"FE",X"97",X"95",X"CE",X"0B",X"F3",X"8D",X"3B",X"96",X"95",X"85",X"02",X"27",X"0F",
		X"0D",X"92",X"26",X"3E",X"0C",X"92",X"CE",X"0B",X"E9",X"8D",X"2A",X"96",X"95",X"20",X"33",X"96",
		X"95",X"84",X"FD",X"97",X"95",X"0F",X"92",X"0D",X"CC",X"26",X"27",X"86",X"03",X"97",X"91",X"CE",
		X"0C",X"13",X"8D",X"11",X"20",X"1C",X"84",X"7F",X"97",X"95",X"CE",X"0B",X"FD",X"8D",X"06",X"86",
		X"40",X"97",X"90",X"20",X"0D",X"EC",X"C1",X"10",X"83",X"FF",X"FF",X"27",X"04",X"8D",X"6C",X"20",
		X"F4",X"39",X"0D",X"90",X"27",X"0D",X"0A",X"90",X"26",X"5E",X"0F",X"95",X"CE",X"0C",X"27",X"8D",
		X"E4",X"20",X"55",X"0D",X"93",X"26",X"45",X"CE",X"0C",X"1D",X"8D",X"D9",X"96",X"C7",X"D6",X"C6",
		X"C5",X"40",X"27",X"01",X"48",X"C6",X"05",X"8D",X"42",X"96",X"95",X"85",X"02",X"27",X"0E",X"96",
		X"00",X"8B",X"10",X"81",X"A0",X"27",X"98",X"C6",X"00",X"8D",X"30",X"20",X"0A",X"0D",X"90",X"26",
		X"06",X"0D",X"91",X"27",X"10",X"0A",X"91",X"96",X"02",X"8B",X"20",X"81",X"F0",X"27",X"14",X"C6",
		X"02",X"8D",X"18",X"20",X"13",X"CC",X"00",X"08",X"8D",X"11",X"20",X"EB",X"CC",X"00",X"0A",X"8D",
		X"0A",X"20",X"C6",X"CC",X"00",X"09",X"8D",X"03",X"7E",X"01",X"C2",X"34",X"0E",X"BD",X"F1",X"AA",
		X"35",X"06",X"1E",X"89",X"BD",X"F2",X"56",X"35",X"88",X"10",X"00",X"00",X"01",X"38",X"07",X"0F",
		X"08",X"FF",X"FF",X"30",X"02",X"00",X"03",X"38",X"07",X"0F",X"09",X"FF",X"FF",X"1F",X"06",X"07",
		X"07",X"00",X"08",X"00",X"09",X"00",X"0A",X"10",X"0A",X"10",X"09",X"10",X"08",X"38",X"0C",X"00",
		X"0D",X"FF",X"FF",X"38",X"07",X"0A",X"01",X"00",X"00",X"0D",X"08",X"FF",X"FF",X"08",X"05",X"00",
		X"04",X"38",X"07",X"0E",X"0A",X"FF",X"FF",X"00",X"08",X"00",X"09",X"00",X"0A",X"FF",X"FF",X"86",
		X"C0",X"D6",X"C8",X"D0",X"D1",X"2F",X"02",X"86",X"E0",X"97",X"BD",X"5F",X"D7",X"BA",X"53",X"D7",
		X"B7",X"8E",X"C9",X"A0",X"A6",X"84",X"95",X"BD",X"26",X"1D",X"EC",X"33",X"A0",X"02",X"E0",X"03",
		X"BD",X"F5",X"84",X"97",X"B6",X"D1",X"B6",X"23",X"02",X"1F",X"98",X"91",X"B7",X"24",X"08",X"97",
		X"B7",X"9F",X"B8",X"D6",X"BA",X"D7",X"BB",X"0C",X"BA",X"30",X"05",X"96",X"C8",X"91",X"BA",X"22",
		X"D3",X"0D",X"B7",X"2B",X"20",X"9E",X"B8",X"A6",X"84",X"8A",X"20",X"A7",X"84",X"EC",X"02",X"ED",
		X"3E",X"96",X"BB",X"C6",X"05",X"3D",X"CA",X"80",X"E7",X"3B",X"96",X"C8",X"0C",X"D1",X"91",X"D1",
		X"24",X"02",X"97",X"D1",X"39",X"35",X"10",X"7E",X"06",X"6C",X"E6",X"84",X"CB",X"25",X"E7",X"84",
		X"25",X"09",X"2A",X"01",X"50",X"8E",X"0C",X"AE",X"1C",X"FE",X"39",X"1A",X"01",X"39",X"FF",X"40",
		X"00",X"00",X"C0",X"00",X"FF",X"00",X"40",X"00",X"00",X"C0",X"FF",X"C0",X"00",X"00",X"40",X"00",
		X"FF",X"00",X"C0",X"00",X"00",X"40",X"FF",X"40",X"40",X"00",X"C0",X"C0",X"FF",X"C0",X"40",X"00",
		X"40",X"C0",X"FF",X"C0",X"C0",X"00",X"40",X"40",X"FF",X"40",X"C0",X"00",X"C0",X"40",X"01",X"00",
		X"1E",X"24",X"FF",X"2A",X"E2",X"FF",X"94",X"00",X"FF",X"00",X"2A",X"FF",X"DC",X"18",X"FF",X"36",
		X"00",X"FF",X"30",X"DC",X"FF",X"BE",X"00",X"FF",X"1E",X"DC",X"FF",X"E2",X"DC",X"FF",X"42",X"00",
		X"FF",X"D0",X"DC",X"FF",X"CA",X"00",X"FF",X"24",X"18",X"FF",X"00",X"2A",X"FF",X"6C",X"00",X"FF",
		X"D6",X"E2",X"01",X"00",X"48",X"36",X"FF",X"88",X"00",X"FF",X"24",X"D0",X"FF",X"3C",X"00",X"FF",
		X"D0",X"42",X"FF",X"B2",X"00",X"FF",X"54",X"EE",X"FF",X"18",X"CA",X"FF",X"E8",X"CA",X"FF",X"AC",
		X"EE",X"FF",X"4E",X"00",X"FF",X"30",X"42",X"FF",X"C4",X"00",X"FF",X"DC",X"D0",X"FF",X"78",X"00",
		X"01",X"FF",X"58",X"00",X"00",X"3C",X"00",X"FF",X"9C",X"F6",X"FF",X"5A",X"F6",X"FF",X"BA",X"F6",
		X"FF",X"E2",X"1E",X"FF",X"1E",X"1E",X"FF",X"46",X"F6",X"FF",X"A6",X"F6",X"FF",X"64",X"F6",X"01",
		X"FF",X"58",X"00",X"00",X"3C",X"00",X"FF",X"9C",X"F6",X"FF",X"3C",X"F6",X"00",X"0A",X"00",X"FF",
		X"A6",X"F6",X"FF",X"64",X"1E",X"FF",X"9C",X"1E",X"FF",X"5A",X"F6",X"00",X"F6",X"00",X"FF",X"C4",
		X"F6",X"FF",X"64",X"F6",X"01",X"FF",X"58",X"00",X"00",X"3C",X"00",X"FF",X"A6",X"E2",X"FF",X"5A",
		X"0A",X"FF",X"88",X"14",X"FF",X"78",X"14",X"FF",X"A6",X"0A",X"FF",X"5A",X"E2",X"01",X"FF",X"58",
		X"00",X"00",X"3C",X"00",X"FF",X"B0",X"E2",X"FF",X"D8",X"14",X"FF",X"28",X"F6",X"FF",X"1E",X"0A",
		X"FF",X"00",X"14",X"FF",X"E2",X"0A",X"FF",X"D8",X"F6",X"FF",X"28",X"14",X"FF",X"50",X"E2",X"FF",
		X"B0",X"F6",X"FF",X"00",X"14",X"FF",X"50",X"F6",X"01",X"FF",X"58",X"00",X"00",X"D8",X"E2",X"FF",
		X"EC",X"00",X"FF",X"3C",X"00",X"FF",X"14",X"14",X"FF",X"C4",X"28",X"FF",X"EC",X"00",X"FF",X"3C",
		X"00",X"FF",X"14",X"EC",X"FF",X"C4",X"D8",X"FF",X"64",X"1E",X"FF",X"9C",X"1E",X"01",X"FF",X"58",
		X"00",X"00",X"3C",X"00",X"FF",X"BA",X"F6",X"FF",X"0A",X"F6",X"FF",X"1E",X"0A",X"FF",X"A6",X"28",
		X"FF",X"28",X"00",X"FF",X"14",X"E2",X"FF",X"EC",X"E2",X"FF",X"D8",X"00",X"FF",X"5A",X"28",X"FF",
		X"E2",X"0A",X"FF",X"F6",X"F6",X"FF",X"46",X"F6",X"01",X"0D",X"41",X"0D",X"60",X"0D",X"85",X"0D",
		X"9E",X"0D",X"C9",X"0D",X"EE",X"03",X"40",X"40",X"02",X"30",X"09",X"06",X"F7",X"09",X"00",X"F1",
		X"C0",X"C0",X"02",X"D0",X"D0",X"02",X"30",X"06",X"00",X"FA",X"12",X"00",X"EE",X"30",X"30",X"05",
		X"C0",X"00",X"02",X"30",X"0F",X"06",X"FA",X"09",X"F7",X"F1",X"40",X"00",X"07",X"30",X"B0",X"02",
		X"30",X"06",X"0F",X"F7",X"06",X"03",X"EB",X"D0",X"50",X"0E",X"00",X"40",X"02",X"30",X"09",X"FD",
		X"FA",X"12",X"FD",X"F1",X"00",X"C0",X"0C",X"40",X"E0",X"02",X"30",X"0F",X"06",X"FD",X"0C",X"F4",
		X"EE",X"C0",X"20",X"10",X"D0",X"C0",X"02",X"30",X"09",X"09",X"F1",X"0F",X"06",X"E8",X"30",X"40",
		X"09",X"00",X"C0",X"02",X"30",X"06",X"12",X"F7",X"FA",X"03",X"F4",X"00",X"40",X"FF",X"00",X"1E",
		X"00",X"FF",X"C4",X"D8",X"FF",X"00",X"50",X"FF",X"3C",X"D8",X"01",X"CC",X"02",X"10",X"BD",X"F7",
		X"A9",X"96",X"C6",X"84",X"7F",X"04",X"79",X"27",X"02",X"8A",X"80",X"97",X"C6",X"96",X"7A",X"4A",
		X"85",X"08",X"27",X"02",X"8A",X"40",X"84",X"47",X"9A",X"C6",X"97",X"C6",X"CC",X"FB",X"40",X"DD",
		X"2A",X"BD",X"F1",X"92",X"BD",X"F1",X"BA",X"BD",X"F1",X"F8",X"7E",X"F2",X"A5",X"0E",X"F5",X"FE",
		X"B6",X"1D",X"10",X"25",X"10",X"24",X"10",X"1C",X"10",X"23",X"10",X"22",X"10",X"1A",X"10",X"1B",
		X"10",X"98",X"9D",X"22",X"10",X"96",X"9B",X"20",X"10",X"1B",X"10",X"1D",X"10",X"98",X"9D",X"1F",
		X"20",X"1C",X"20",X"19",X"80",X"CD",X"EF",X"FF",X"FE",X"DC",X"BA",X"98",X"88",X"88",X"88",X"88",
		X"88",X"88",X"88",X"88",X"88",X"54",X"48",X"49",X"53",X"20",X"53",X"50",X"41",X"43",X"45",X"20",
		X"41",X"56",X"41",X"49",X"4C",X"41",X"42",X"4C",X"45",X"00",X"00",X"00",X"57",X"48",X"41",X"54",
		X"20",X"54",X"48",X"45",X"20",X"48",X"45",X"4C",X"4C",X"20",X"41",X"52",X"45",X"20",X"59",X"4F",
		X"55",X"20",X"4C",X"4F",X"4F",X"4B",X"49",X"4E",X"47",X"20",X"41",X"54",X"20",X"3F",X"3F",X"3F",
		X"00",X"00",X"00",X"0A",X"F1",X"0A",X"F6",X"00",X"F1",X"F6",X"FB",X"F6",X"19",X"00",X"FB",X"F6",
		X"F1",X"F6",X"F6",X"00",X"F1",X"0A",X"FB",X"0A",X"14",X"0A",X"2D",X"F6",X"05",X"32",X"BA",X"6E",
		X"28",X"A1",X"1E",X"EF",X"14",X"0A",X"14",X"37",X"F1",X"FB",X"E7",X"64",X"19",X"9C",X"E2",X"D8",
		X"14",X"F6",X"2D",X"0A",X"05",X"CA",X"BA",X"92",X"28",X"5F",X"1D",X"0B",X"14",X"F6",X"14",X"C9",
		X"F1",X"05",X"E8",X"99",X"19",X"64",X"E2",X"28",X"04",X"08",X"FC",X"0A",X"04",X"F6",X"04",X"02",
		X"06",X"FA",X"FE",X"00",X"00",X"01",X"01",X"03",X"03",X"3F",X"00",X"01",X"01",X"01",X"04",X"03",
		X"3F",X"01",X"01",X"01",X"02",X"04",X"04",X"3F",X"02",X"01",X"02",X"02",X"04",X"04",X"3E",X"02",
		X"02",X"03",X"03",X"04",X"04",X"3E",X"02",X"03",X"03",X"03",X"04",X"04",X"3E",X"02",X"03",X"04",
		X"04",X"05",X"04",X"3E",X"02",X"03",X"05",X"05",X"05",X"05",X"7C",X"02",X"06",X"03",X"07",X"04",
		X"08",X"05",X"09",X"FE",X"C6",X"FF",X"26",X"FF",X"7A",X"01",X"00",X"01",X"00",X"03",X"00",X"01",
		X"00",X"05",X"27",X"05",X"42",X"05",X"5D",X"05",X"60",X"20",X"01",X"00",X"03",X"20",X"00",X"00",
		X"03",X"20",X"01",X"00",X"04",X"24",X"01",X"00",X"02",X"24",X"01",X"00",X"01",X"0F",X"67",X"39");
begin
process(clk)
begin
	if rising_edge(clk) then
		data <= rom_data(to_integer(unsigned(addr)));
	end if;
end process;
end architecture;
