//============================================================================
// 
//  ZX80-ZX81 replica for MiST
//  Copyright (C) 2018 György Szombathelyi
//
//  Multicore 2 top - Victor Trucco - 2018
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//============================================================================

`default_nettype none

module zx8x
(
  // Clocks
	input wire	clock_50_i,

	// Buttons
	input wire [4:1]	btn_n_i,

	// SRAMs (AS7C34096)
	output wire	[18:0]sram_addr_o  = 18'b0000000000000000000,
	inout wire	[7:0]sram_data_io	= 8'bzzzzzzzz,
	output wire	sram_we_n_o		= 1'b1,
	output wire	sram_oe_n_o		= 1'b1,
		
	// SDRAM	(H57V256)
	output wire	[12:0]sdram_ad_o,
	inout wire	[15:0]sdram_da_io,
	output wire	[1:0]sdram_ba_o,
	output wire	[1:0]sdram_dqm_o,
	output wire	sdram_ras_o,
	output wire	sdram_cas_o,
	output wire	sdram_cke_o,
	output wire	sdram_clk_o,
	output wire	sdram_cs_o = 1'b1,
	output wire	sdram_we_o,

	// PS2
	inout wire	ps2_clk_io			= 1'bz,
	inout wire	ps2_data_io			= 1'bz,
	inout wire	ps2_mouse_clk_io  	= 1'bz,
	inout wire	ps2_mouse_data_io 	= 1'bz,

	// SD Card
	output wire	sd_cs_n_o			= 1'b1,
	output wire	sd_sclk_o			= 1'b0,
	output wire	sd_mosi_o			= 1'b0,
	input wire	sd_miso_i,

	// Joysticks
	input wire	joy1_up_i,
	input wire	joy1_down_i,
	input wire	joy1_left_i,
	input wire	joy1_right_i,
	input wire	joy1_p6_i,
	input wire	joy1_p9_i,
	input wire	joy2_up_i,
	input wire	joy2_down_i,
	input wire	joy2_left_i,
	input wire	joy2_right_i,
	input wire	joy2_p6_i,
	input wire	joy2_p9_i,
	output wire	joyX_p7_o			= 1'b1,

	// Audio
	output wire	dac_l_o				= 1'b0,
	output wire	dac_r_o				= 1'b0,
	input wire	ear_i,
	output wire	mic_o				= 1'b0,

		// VGA
	output wire	[4:0]vga_r_o,
	output wire	[4:0]vga_g_o,
	output wire	[4:0]vga_b_o,
	output wire	vga_hsync_n_o,
	output wire	vga_vsync_n_o,

		// HDMI
	output wire	[7:0]tmds_o				= 8'b00000000,

		//STM32
	input wire	stm_tx_i, //stm TX pin, so, is IN on the slave
	output wire	stm_rx_o, //stm RX pin, so, is OUT on the slave
	output wire	stm_rst_o			= 1'bZ, //'0' to hold the microcontroller reset line, to free the SD card
		
	inout wire	stm_b8_io,
	inout wire	stm_b9_io,
	inout wire	stm_b12_io,
	inout wire	stm_b13_io,
	inout wire	stm_b14_io,
	inout wire	stm_b15_io,
	inout wire	stm_a15_io
);


/*
//`include "build_id.v"
localparam CONF_STR = 
{
	"ZX8X;;",
	"F,O  P  ,Load tape;",
	"O4,Model,ZX80,ZX81;",
	"OAB,RAM size,1k,16k,32k,64k;",
	"O8,Swap joy axle,Off,On;",
	"O6,Video frequency,50Hz,60Hz;",
	"O7,Inverse video,Off,On;",
	"O9,Scanlines,Off,On;",
	"T0,Reset;",
	"V,v1.0.",`BUILD_DATE
};
*/

parameter STRLEN = 1;
parameter CONF_STR = 0;//{"P,zx8x.rom"};

////////////////////   CLOCKS   ///////////////////
wire clk_sys;
wire clk_vga;
wire clk_dvi;
wire clk_dvi_180;
	
wire locked;

pll pll
(
	.inclk0(clock_50_i),
	.c0(clk_sys), //52 MHz
	.c1( clk_vga ),
	.c2( clk_dvi ),
	.c3( clk_dvi_180 ),
	.locked()
);

/*
pll_genlock pll_genlock
(
	.inclk0(clock_50_i),
	.c0( clk_vga ),
	.c1( clk_dvi ),
	.c2( clk_dvi_180 ),
	.locked()
);
*/
reg  ce_cpu_p;
reg  ce_cpu_n;
reg  ce_13,ce_65,ce_psg;

always @(negedge clk_sys) begin
	reg [4:0] counter = 0;

	counter  <=  counter + 1'd1;
	ce_cpu_p <= !counter[3] & !counter[2:0];
	ce_cpu_n <=  counter[3] & !counter[2:0];
	ce_65    <= !counter[2:0];
	ce_13    <= !counter[1:0];
	ce_psg   <= !counter[4:0];
end

//////////////////   MIST ARM I/O   ///////////////////
wire [10:0] ps2_key;
wire [24:0] ps2_mouse;

wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire  [1:0] buttons;
wire  [1:0] switches;
wire        scandoubler_disable = 1'b0;
wire [31:0] status;

wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire [15:0] ioctl_dout;
wire        ioctl_download;
wire  [7:0] ioctl_index;


// include ROM download helper
// this receives a byte stream from the arm io controller via spi and 
// writes it into sdram
data_io data_io (
   
	// io controller spi interface
	.sdi        ( stm_b15_io ),
   .sck        ( stm_b13_io ),
   .ss         ( stm_b12_io ),

   .downloading ( ioctl_download ),  // signal indicating an active rom download
	.index       ( ioctl_index ), 
  
   // external ram interface
   .clk   ( clk_sys ),
   .wr    ( ioctl_wr ),
   .addr  ( ioctl_addr  ),
   .data  ( ioctl_dout )
);

///////////////////   CPU   ///////////////////
wire [15:0] addr;
wire  [7:0] cpu_din;
wire  [7:0] cpu_dout;
wire        nM1;
wire        nMREQ;
wire        nIORQ;
wire        nRD;
wire        nWR;
wire        nRFSH;
wire        nHALT;
wire        nINT = addr[6];
wire        nNMI;
wire        nWAIT;
reg       	reset;

T80pa cpu
(
	.RESET_n(~reset),
	.CLK(clk_sys),
	.CEN_p(ce_cpu_p),
	.CEN_n(ce_cpu_n),
	.WAIT_n(nWAIT),
	.INT_n(nINT),
	.NMI_n(nNMI),
	.BUSRQ_n(1),
	.M1_n(nM1),
	.MREQ_n(nMREQ),
	.IORQ_n(nIORQ),
	.RD_n(nRD),
	.WR_n(nWR),
	.RFSH_n(nRFSH),
	.HALT_n(nHALT),
	.A(addr),
	.DO(cpu_dout),
	.DI(cpu_din)
);

wire [7:0] io_dout = kbd_n ? (psg_sel ? psg_out : 8'hFF) : { tape_in, hz50, 1'b0, key_data[4:0] & ({5{addr[12]}} | {~joykeys[4:2],1'b1,~joykeys[0]}) & ({5{addr[11]}} | {~joykeys[1],4'b1111})  };

always_comb begin
	case({nMREQ, ~nM1 | nIORQ | nRD})
	    'b01: cpu_din = (~nM1 & nopgen) ? 8'h0 : mem_out;
	    'b10: cpu_din = io_dout;
	 default: cpu_din = 8'hFF;
	endcase
end

wire       tape_in = ~ear_i; // inverted???
reg        init_reset = 1;
reg        zx81;
reg  [1:0] mem_size; //00-1k, 01 - 16k 10 - 32k 11 - 64k
wire       hz50 = 1'b0; // 0 = 60Hz   1=50hz   ~status[6];
wire [4:0] joykeys = {~joy1_down_i, ~joy1_up_i, ~joy1_right_i, ~joy1_left_i, ~joy1_p6_i};

always @(posedge clk_sys) begin
	reg old_download;
	old_download <= ioctl_download;
	if(~ioctl_download && old_download && !ioctl_index) init_reset <= 0; //no index, its the ROM
	if(~ioctl_download && old_download && ioctl_index) tape_ready <= 1; // index, so its a tape file
	
	//reset <= !btn_n_i[1] | status[0] | (mod[1] & Fn[11]) | init_reset;
	
	reset <= !btn_n_i[1] | Fn[4];// | init_reset;
	
	if (reset) begin
		zx81 <= 1'b1; // 0=ZX80 1=ZX81 status[4];
		mem_size <= 2'b01; //16kb   status[11:10];
		tape_ready <= 0;
	end
end


wire sram_we = ~(ram_we | tapewrite_we);
wire sram_oe = ~(ram_e & (~nRFSH | (~nRD & ~nMREQ)) & ~ce_cpu_n & ~tapeloader);

assign sram_addr_o   = {"000", ram_a};
assign sram_data_io  = (sram_we == 1'b0) ? ram_in : 8'bZ;
assign ram_out 	   = sram_data_io;
assign sram_oe_n_o   = sram_oe;
assign sram_we_n_o   = sram_we;
	

/*
//////////////////   MEMORY   //////////////////
assign sdram_clk_o = clk_sys;

sdram ram
(
	.init(~locked),
	.clk(clk_sys),
	.wtbt(0),
	.dout(ram_out),
	.din (ram_in),
	.addr(ram_a),
	.we(ram_we | tapewrite_we),
	.rd(ram_e & (~nRFSH | (~nRD & ~nMREQ)) & ~ce_cpu_n & ~tapeloader),
	.ready(ram_ready),
	
	.SDRAM_DQ(sdram_da_io),    	// 16 bit bidirectional data bus
	.SDRAM_A(sdram_ad_o),     		// 13 bit multiplexed address bus
	.SDRAM_DQML(sdram_dqm_o[0]),  // two byte masks
	.SDRAM_DQMH(sdram_dqm_o[1]),  // 
	.SDRAM_BA(sdram_ba_o),    		// two banks
	.SDRAM_nCS(sdram_cs_o),   		// a single chip select
	.SDRAM_nWE(sdram_we_o),   		// write enable
	.SDRAM_nRAS(sdram_ras_o),  	// row address select
	.SDRAM_nCAS(sdram_cas_o),  	// columns address select
	.SDRAM_CKE(sdram_cke_o)   		// clock enable
);
*/

wire        ram_ready;
reg   [7:0] rom[12288];
wire [12:0] rom_a  = nRFSH ? addr[12:0] : { addr[12:9], ram_data_latch[5:0], row_counter };
wire [14:0] tape_load_addr = (&mem_size ? 15'h4000 : 15'h0) + ((zx81 == 1) ? tape_addr + 4'd8 : tape_addr-1'd1); // $4000(.o file ) $4009 (.p file)
wire [15:0] ram_a;
wire        ram_e_64k = &mem_size & (addr[13] | (addr[15] & nM1));
wire			rom_e  = ~addr[14] & (~addr[12] | zx81) & ~ram_e_64k;
wire        ram_e  = addr[14] | ram_e_64k;
wire        ram_we = ~nWR & ~nMREQ & ram_e;
wire  [7:0] ram_in = tapeloader ? tape_in_byte : cpu_dout;
wire  [7:0] rom_out;
wire  [7:0] ram_out;
wire  [7:0] mem_out;

always_comb begin
	casex({ tapeloader, rom_e, ram_e })
		'b110: mem_out = tape_loader_patch[addr - (zx81 ? 13'h0347 : 13'h0207)];
		'b010: mem_out = rom_out;
		'b001: mem_out = ram_out;
		default: mem_out = 8'd0;
	endcase

	casex({tapeloader, mem_size })
	   'b1XX: ram_a = tape_load_addr;
	   'b000: ram_a = { 6'b000000,             addr[9:0] }; //1k
		'b001: ram_a = { 2'b00,                addr[13:0] }; //16k
		'b010: ram_a = { 1'b0, addr[15] & nM1, addr[13:0] }; //32k
		'b011: ram_a = { addr[15] & nM1,       addr[14:0] }; //64k
	endcase
end

/*always @(posedge clk_sys) begin
	if (ioctl_wr & !ioctl_index) begin //if the index is 0, its the ROM
		rom[ioctl_addr[13:0]] <= ioctl_dout[7:0];
	end
end


always @(posedge clk_sys) begin
	rom_out <= rom[{ (zx81 ? rom_a[12] : 2'h2), rom_a[11:0] }];
end
*/

zx8xrom zx8xrom
(
    .CLK   (clk_sys),
    .ADDR  ({ (zx81 ? rom_a[12] : 2'h2), rom_a[11:0] }),
    .DATA  (rom_out)
);

////////////////////  TAPE  //////////////////////
reg   [7:0] tape_ram[16384];
reg         tapeloader, tapewrite_we;
reg  [13:0] tape_addr;
reg   [7:0] tape_in_byte;
reg         tape_ready;  // there is data in the tape memory
// patch the load ROM routines to loop until the memory is filled from $4000(.o file ) $4009 (.p file)
// xor a; loop: nop or scf, jr nc loop, jp h0207 (jp h0203 - ZX80)
reg   [7:0] tape_loader_patch[7] = '{8'haf, 8'h00, 8'h30, 8'hfd, 8'hc3, 8'h07, 8'h02};

always @(posedge clk_sys) begin
	if (ioctl_wr & ioctl_index) begin //if we have and index, its tape data
		tape_ram[ioctl_addr[13:0]] <= ioctl_dout[7:0];
	end
end

always @(posedge clk_sys) begin
	tape_in_byte <= tape_ram[tape_addr];
end

always @(posedge clk_sys) begin
	reg old_nM1;
	
	old_nM1 <= nM1;
	tapewrite_we <= 0;
	
	if (~nM1 & old_nM1 & tape_ready) begin
		if (zx81) begin
			if (addr == 16'h0347) begin
				tape_loader_patch[1] <= 8'h00; //nop
				tape_loader_patch[5] <= 8'h07; //0207h
				tape_addr <= 14'h0;
				tapeloader <= 1;
			end
			if (addr >= 16'h03c3 || addr < 16'h0347) begin
				tapeloader <= 0;
			end
		end else begin
			if (addr == 16'h0207) begin
				tape_loader_patch[1] <= 8'h00; //nop
				tape_loader_patch[5] <= 8'h03; //0203h
				tape_addr <= 14'h0;
				tapeloader <= 1;
			end
			if (addr >= 16'h024d || addr < 16'h0207) begin
				tapeloader <= 0;
			end
		end
	end

	if (tapeloader & ce_cpu_p) begin
		if (tape_addr != ioctl_addr[13:0]) begin
			tape_addr <= tape_addr + 1'h1;
			tapewrite_we <= 1;
		end else begin
			tape_loader_patch[1] <= 8'h37; //scf
		end
	end
end

////////////////////  VIDEO //////////////////////
// Based on the schematic:
// http://searle.hostei.com/grant/zx80/zx80.html

// character generation
wire      nopgen = addr[15] & ~mem_out[6] & nHALT;
wire      data_latch_enable = nRFSH & ce_cpu_n & ~nMREQ;
reg [7:0] ram_data_latch;
reg       nopgen_store;
reg [2:0] row_counter;
reg inverse_video = 1'b0; // status[7]
wire      shifter_start = nMREQ & nopgen_store & ce_cpu_p & (~zx81 | ~NMIlatch);
reg [7:0] shifter_reg;
wire      video_out = (~inverse_video ^ shifter_reg[7] ^ inverse) & !back_porch_counter & csync;
reg       inverse;

reg[4:0]  back_porch_counter = 1;

always @(posedge clk_sys) begin
	reg old_csync;
	reg old_shifter_start;
	
	old_csync <= csync;
	old_shifter_start <= shifter_start;

	if (data_latch_enable) begin
		ram_data_latch <= mem_out;
		nopgen_store <= nopgen;
	end

	if (nMREQ & ce_cpu_p) inverse <= 0;

	if (~old_shifter_start & shifter_start) begin
		shifter_reg <= (~nM1 & nopgen) ? 8'h0 : mem_out;
		inverse <= ram_data_latch[7];
	end else if (ce_65) begin
		shifter_reg <= { shifter_reg[6:0], 1'b0 };
	end

	if (old_csync & ~csync)	row_counter <= row_counter + 1'd1;
	if (~vsync) row_counter <= 0;

	if (~old_csync & csync) back_porch_counter <= 1;
   if (ce_65 && back_porch_counter) back_porch_counter <= back_porch_counter + 1'd1;

	end

// ZX80 sync generator
reg ic11,ic18,ic19_1,ic19_2;
//wire csync = ic19_2; //ZX80 original
wire csync = vsync & ~hsync;
wire vsync = ic11;

always @(posedge clk_sys) begin

	reg old_nM1;
	old_nM1 <= nM1;

	if (~(nIORQ | nWR) & (~zx81 | ~NMIlatch)) ic11 <= 1;
	if (~kbd_n & (~zx81 | ~NMIlatch)) ic11 <= 0;

	if (~nIORQ) ic18 <= 1;
	if (~ic19_2) ic18 <= 0;

	if (old_nM1 & ~nM1) begin
		ic19_1 <= ~ic18;
		ic19_2 <= ic19_1;
	end
	if (~ic11) ic19_2 <= 0;
end

// ZX81 upgrade
// http://searle.hostei.com/grant/zx80/zx80nmi.html

wire      hsync = (sync_counter >= 16 && sync_counter <= 31);
reg       NMIlatch;
reg [7:0] sync_counter = 0;

assign nWAIT = ~(nHALT & ~nNMI)    | ~zx81;
assign nNMI  = ~(NMIlatch & hsync) | ~zx81;

always @(posedge clk_sys) begin
	reg       old_cpu_n;

	old_cpu_n <= ce_cpu_n;

	if (old_cpu_n & ~ce_cpu_n) begin
		sync_counter <= sync_counter + 1'd1;
	   if (sync_counter == 8'd206 | (~nM1 & ~nIORQ)) sync_counter <= 0;
	end

	if (zx81) begin
		if (~nIORQ & ~nWR & (addr[0] ^ addr[1])) NMIlatch <= addr[1];
	end
end

wire       v_sd_out, HS_sd_out, VS_sd_out;

scandoubler scandoubler
(
	.clk(clk_sys),
	.ce_2pix(ce_13),

	.scanlines(1'b0),//status[9]),

	.v_in(video_out),
	.csync(csync),

	.v_out(v_sd_out),
	.hs_out(HS_sd_out),
	.vs_out(VS_sd_out)

);

wire [4:0] R_out,G_out,B_out;





assign vga_r_o = R_out;
assign vga_g_o = G_out;
assign vga_b_o = B_out;
assign vga_hsync_n_o = genlock_hs_s;
assign vga_vsync_n_o = genlock_vs_s;


////////////////////  SOUND //////////////////////
wire [7:0] psg_out;
wire       psg_sel = ~nIORQ & &addr[3:0]; //xF
wire [7:0] psg_ch_a, psg_ch_b, psg_ch_c;

YM2149 psg
(
	.CLK(clk_sys),
	.CE(ce_psg),
	.RESET(reset),
	.BDIR(psg_sel & ~nWR),
	.BC(psg_sel & (&addr[7:6] ^ nWR)),
	.DI(cpu_dout),
	.DO(psg_out),
	.CHANNEL_A(psg_ch_a),
	.CHANNEL_B(psg_ch_b),
	.CHANNEL_C(psg_ch_c)
);

wire [8:0] audio_l = { 1'b0, psg_ch_a } + { 1'b0, psg_ch_c };
wire [8:0] audio_r = { 1'b0, psg_ch_b } + { 1'b0, psg_ch_c };

sigma_delta_dac #(7) dac_l
(
	.CLK(clk_sys),
	.RESET(reset),
	.DACin(audio_l[8:1]),
	.DACout(dac_l_o)
);

sigma_delta_dac #(7) dac_r
(
	.CLK(clk_sys),
	.RESET(reset),
	.DACin(audio_r[8:1]),
	.DACout(dac_r_o)
);
////////////////////   Keyboard   /////////////////////

wire kbd_n = nIORQ | nRD | addr[0];

wire [11:1] Fn;
wire  [2:0] mod;
wire  [4:0] key_data;

keyboard kbd
(  
		.CLK(clk_sys),
		.nRESET(~reset),
		
		// PS/2 interface
		.PS2_CLK(ps2_clk_io),
		.PS2_DATA(ps2_data_io),
		
		// CPU address bus (row)
		.rows	(addr[15:8]),
		
		// Column outputs to ULA
		.cols		(key_data),
		
		.teclasF	(Fn),
		.osd_o	(keys_s)
);

//-----------------------------------------------------------
// 
// start the microcontroller OSD after the power on
//

reg stm_rst_s = 1'bz;
reg power_on_reset = 1'b1;
reg [7:0] osd_start_s = 8'b11111111;
wire [7:0] keys_s;
reg [24:0] power_on_s = 25'd21000000;
wire reset_n = btn_n_i[2];

reg [1:0] edge_s = 2'b00;

/*
always @(posedge clk_sys) 
begin

		if (reset_n == 1'b0) 
			begin
				power_on_s <= 25'd21000000;
				stm_rst_s <= 1'bz; // release the microcontroller reset line
				osd_start_s <= 8'b11111111;
				power_on_reset <= 1'b1;
			end
		else if (power_on_s == 25'd10) 
			osd_start_s <=  8'b00111111; // CMD 0x01 - send the ROM
		else if (power_on_s == 25'b0) 
			power_on_reset <= 1'b0;
	
		
		if (power_on_s != 25'b0)
			power_on_s <= power_on_s - 1;
		
		
		if (ioctl_download == 1'b1 && osd_start_s == 8'b00111111 )
			osd_start_s <= 8'b11111111;
		
		
		edge_s <= {edge_s[0], ioctl_download};
		
		//if (edge_s == 2'b10) 	stm_rst_s <= 1'b0; // hold the microcontroller on reset, to free the SD card

end
*/



assign stm_rst_o = stm_rst_s;	



// include the on screen display
osd #(	.STRLEN 			( STRLEN ),
			.OSD_COLOR 		( 3'b001 ), //RGB
			.OSD_X_OFFSET 	( 10'd18 ),
			.OSD_Y_OFFSET 	( 10'd15 )
	) 
	osd (
   .pclk       ( clk_vga     ),
			
   // spi for OSD
   .sdi        ( stm_b15_io   ),
   .SCK        ( stm_b13_io   ),
   .ss         ( stm_b12_io   ),
	.sdo        ( stm_b14_io   ),

   .red_in     ( genlock_r_s ),
   .green_in   ( genlock_g_s ),
   .blue_in    ( genlock_b_s ),
   .hs_in      ( genlock_hs_s ),
   .vs_in      ( genlock_vs_s ),

   .red_out    ( R_out      ),
   .green_out  ( G_out      ),
   .blue_out   ( B_out      ),
   .hs_out     (     ),
   .vs_out     (     ),
	
	.data_in		( keys_s & osd_start_s ), //combine the start CMD and the keyboard CMD
	.conf_str	( CONF_STR		)
			
);


	/////////////////////////////////////////////
	// Genlock and HDMI
	
	// HDMI
	wire [9:0] tdms_r_s;
	wire [9:0] tdms_g_s;
	wire [9:0] tdms_b_s;
	wire [9:0] hdmi_p_s;
	wire [9:0] hdmi_n_s;
		
	// Genlock
	wire [4:0] genlock_r_s;
	wire [4:0] genlock_g_s;
	wire [4:0] genlock_b_s;	
	wire genlock_blank_s;
	wire genlock_hs_s;
	wire genlock_vs_s;
	
	genlock_top # 
	(
		.desloc_x_i ( 9'd45 ), //55 e 5
		.desloc_y_i ( 9'd0 ),
		.pixel_size_i ( 8 )
	)
	genlock
	(
	
		.CLOCK_SYNC	( clk_dvi ),
		.CLOCK_VGA	( clk_vga ),
		.CLOCK_PIXEL( clk_dvi_180 ),
		
		.rgb_15 		( (video_out) ? 15'b111111111111111 : 15'b0 ),
		.hsync_15 	( hsync ),
		.vsync_15 	( vsync ),
				
		// OUTs
		.VGA_R		( genlock_r_s ),
		.VGA_G		( genlock_g_s ),
		.VGA_B		( genlock_b_s ),
		.VGA_HS		( genlock_hs_s ),
		.VGA_VS		( genlock_vs_s ),
		.VGA_BLANK	( genlock_blank_s ),
		
		// to external SDRAM 
		.SDRAM_AD	( sdram_ad_o ),
		.SDRAM_DA	( sdram_da_io ),

		.SDRAM_BA	( sdram_ba_o ),
		.SDRAM_DQM	( sdram_dqm_o ),
 
		.SDRAM_RAS	( sdram_ras_o ),
		.SDRAM_CAS	( sdram_cas_o ),
		.SDRAM_CKE	( sdram_cke_o ),
		.SDRAM_CLK	( sdram_clk_o ),
		.SDRAM_CS	( sdram_cs_o ),
		.SDRAM_WE	( sdram_we_o )
	
	);

	hdmi # (
		.FREQ	( 25200000 ),		// pixel clock frequency = 25.2MHz
		.FS	( 48000 ),			// audio sample rate - should be 32000, 41000 or 48000 = 48KHz
		.CTS	( 25200 ),			// CTS = Freq(pixclk) * N / (128 * Fs)
		.N		( 6144 )				// N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
	)
	inst_dvid
	(
		.I_CLK_PIXEL	( clk_vga ),

		.I_R				( { R_out[4:0], R_out[4:2] }), 
		.I_G				( { G_out[4:0], G_out[4:2] }),
		.I_B				( { B_out[4:0], B_out[4:2] }),
		
		.I_BLANK			( genlock_blank_s ),
		.I_HSYNC			( genlock_hs_s ),
		.I_VSYNC			( genlock_vs_s ),
		
		.I_AUDIO_ENABLE( 1'b1 ),
		.I_AUDIO_PCM_L ( {1'b0, audio_l, 6'b0000000} ),
		.I_AUDIO_PCM_R	( {1'b0, audio_r, 6'b0000000} ),
		
		// TMDS parallel pixel synchronous outputs (serialize LSB first)
		.O_RED			( tdms_r_s ),
		.O_GREEN			( tdms_g_s ),
		.O_BLUE			( tdms_b_s )
	);

		hdmi_out_altera  hdmi_out
		(
			.clock_pixel_i		( clk_vga ),
			.clock_tdms_i		( clk_dvi ),
			.red_i				( tdms_r_s ),
			.green_i				( tdms_g_s ),
			.blue_i				( tdms_b_s ),
			.tmds_out_p			( hdmi_p_s ),
			.tmds_out_n			( hdmi_n_s )
		);
		
		
assign tmds_o[7] = hdmi_p_s[2];	// 2+		
assign tmds_o[6] = hdmi_n_s[2];	// 2-		
assign tmds_o[5] = hdmi_p_s[1];	// 1+			
assign tmds_o[4] = hdmi_n_s[1];	// 1-		
assign tmds_o[3] = hdmi_p_s[0];	// 0+		
assign tmds_o[2] = hdmi_n_s[0];	// 0-	
assign tmds_o[1] = hdmi_p_s[3];	// CLK+	
assign tmds_o[0] = hdmi_n_s[3];	// CLK-	





endmodule
