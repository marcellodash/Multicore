--
--
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity mc1000 is
	port (
		clock_i				: in    std_logic;
		reset_i				: in    std_logic;
		-- Options
		opt_ram64_i			: in    std_logic;								-- '1' = 64K
		opt_graph_ch_i		: in    std_logic;								-- '1' = enable graphic chars
		opt_keyboard_i		: in    std_logic;								-- '1' = New keyboard scheme
		-- RAM memory
		ram_addr_o			: out   std_logic_vector(15 downto 0);
		ram_data_from_i	: in    std_logic_vector( 7 downto 0);
		ram_data_to_o		: out   std_logic_vector( 7 downto 0);
		ram_cs_o				: out   std_logic;
		ram_oe_o				: out   std_logic;
		ram_we_o				: out   std_logic;
		-- ROM memory
		rom_addr_o			: out   std_logic_vector(13 downto 0);
		rom_data_from_i	: in    std_logic_vector( 7 downto 0);
		-- VRAM memory
		vram_addr_o			: out   std_logic_vector(12 downto 0);
		vram_data_from_i	: in    std_logic_vector( 7 downto 0);
		vram_data_to_o		: out   std_logic_vector( 7 downto 0);
		vram_oe_o			: out   std_logic;
		vram_we_o			: out   std_logic;
		-- PS/2 keyboard
		ps2_clk_io			: inout std_logic;
		ps2_data_io			: inout std_logic;
		-- K7
		mic_o					: out   std_logic;
		ear_i					: in    std_logic;
		-- Joystick (order: Fire, Up, Down, Left, Right)
		joy1_i				: in    std_logic_vector(4 downto 0);
		joy2_i				: in    std_logic_vector(4 downto 0);
		-- Audio
		audio_psg_o			: out   std_logic_vector( 7 downto 0);
		-- Video
		video_mode80_o		: out   std_logic;								-- 32 ou 80 colunas
		video_col_idx_o	: out   std_logic_vector( 3 downto 0);
		video_80_pixel_o	: out   std_logic;
--		video_r_o			: out   std_logic_vector( 7 downto 0);
--		video_g_o			: out   std_logic_vector( 7 downto 0);
--		video_b_o			: out   std_logic_vector( 7 downto 0);
		video_hs_o			: out   std_logic;
		video_vs_o			: out   std_logic;
		video_hb_o			: out   std_logic;
		video_vb_o			: out   std_logic;
		cnt_hor_o			: out std_logic_vector( 8 downto 0);
		cnt_ver_o			: out std_logic_vector( 7 downto 0);
		-- Debug
		D_clk_src_i			: in    std_logic_vector(2 downto 0);
		D_clk_manual_i		: in    std_logic;
--		D_rom_c000_o		: out   std_logic;
--		D_rom_cs_o			: out   std_logic;
--		D_rows_o				: out   std_logic_vector( 7 downto 0);
--		D_80col_en_o		: out   std_logic;
--		D_crtc_vram_bank_o : out  std_logic;
		D_cpu_addr_o		: out   std_logic_vector(15 downto 0)
	);
end entity;

architecture Behavior of mc1000 is

	-- Clocks
	signal clock_14m_en_s		: std_logic					:= '0';
	signal clock_crtc_en_s		: std_logic					:= '0';
	signal clock_cpu_s			: std_logic;
	signal clock_psg_s			: std_logic;
	signal clk_cnt_s				: unsigned(21 downto 0)	:= (others => '0');

	-- Reset
	signal reset_n_s				: std_logic;

	-- CPU
	signal cpu_addr_s				: std_logic_vector(15 downto 0);
	signal cpu_data_from_s		: std_logic_vector( 7 downto 0);
	signal cpu_data_to_s			: std_logic_vector( 7 downto 0);
	signal cpu_int_n_s			: std_logic;
	signal cpu_nmi_n_s			: std_logic;
	signal cpu_iorq_n_s			: std_logic;
	signal cpu_m1_n_s				: std_logic;
	signal cpu_rd_n_s				: std_logic;
	signal cpu_wr_n_s				: std_logic;
	signal cpu_mreq_n_s			: std_logic;
--	signal cpu_rfsh_n_s			: std_logic;
	signal cpu_wait_n_s			: std_logic;

	-- Seletores
	signal rom_c000_q				: std_logic								:= '1';
	signal rom_en_s				: std_logic;
	signal rom_cs_s				: std_logic;
	signal ram16_en_s				: std_logic;
	signal ram48_en_s				: std_logic;

	-- Portas
	signal port12_en_s			: std_logic;
	signal port20_en_s			: std_logic;
	signal port40_en_s			: std_logic;
	signal port60_en_s			: std_logic;
	signal port80_en_s			: std_logic;
	signal port80_r				: std_logic_vector( 7 downto 0)	:= (others => '0');

	-- PSG
	signal psg_data_from_s		: std_logic_vector( 7 downto 0);
	signal psg_bdir_s				: std_logic;
	signal psg_bc1_s				: std_logic;
	signal psg_port_b_s			: std_logic_vector( 7 downto 0);

	-- Keyboard
	signal keyb_rows_s			: std_logic_vector( 7 downto 0);
	signal keyb_cols_s			: std_logic_vector( 7 downto 0);
	signal keyb_cols_new_s		: std_logic_vector( 7 downto 0);
	signal ear_joy_s				: std_logic_vector( 7 downto 0);

	-- VDG
	signal vram_vdg_en_s			: std_logic;
	signal vdg_gate_s				: std_logic;
	signal vdg_addr_s				: std_logic_vector(12 downto 0);
	signal vdg_data_to_s			: std_logic_vector( 7 downto 0);
	signal vdg_oe_s				: std_logic;
	signal vdg_temp_s				: std_logic;
	signal vdg_new6_s				: std_logic;
	signal vdg_hs_n_s				: std_logic;
	signal vdg_fs_n_s				: std_logic;
	signal vdg_an_g_s				: std_logic;
	signal vdg_an_s_s				: std_logic;
	signal vdg_intn_s				: std_logic;
	signal vdg_gm_s				: std_logic_vector( 2 downto 0);
	signal vdg_css_s				: std_logic;
	signal vdg_inv_s				: std_logic;
--	signal vdg_r_s					: std_logic_vector( 7 downto 0);
--	signal vdg_g_s					: std_logic_vector( 7 downto 0);
--	signal vdg_b_s					: std_logic_vector( 7 downto 0);
	signal vdg_hs_s				: std_logic;
	signal vdg_vs_s				: std_logic;
	signal vdg_hb_s				: std_logic;
	signal vdg_vb_s				: std_logic;

	-- CRTC
	signal vram_crtc_en_s		: std_logic;
	signal crtc_addr15_en_s		: std_logic;
	signal crtc_enable_s			: std_logic;
	signal crtc_cs_n_s			: std_logic;
	signal crtc_data_from_s		: std_logic_vector( 7 downto 0);
	signal crtc_vram_bank_q		: std_logic;
	signal crtc_ma_s				: std_logic_vector(13 downto 0);
	signal crtc_ra_s				: std_logic_vector( 4 downto 0);
	signal crtc_hsync_s			: std_logic;
	signal crtc_vsync_s			: std_logic;
	signal crtc_de_s				: std_logic;
	signal crtc_de_dly_s			: std_logic;
	signal crtc_cursor_en_s		: std_logic;
	signal crtc_cursor_en_dly_s: std_logic;
	signal crtc_video_data_s	: std_logic_vector( 7 downto 0);
	signal crtc_video_pixel_s	: std_logic;

	-- VRAM
	signal vram_addr_s			: std_logic_vector(10 downto 0);
	signal vram_data_from_s		: std_logic_vector( 7 downto 0);
	signal vram_we_s				: std_logic;
	signal crtc_romchar_addr_s	: std_logic_vector(10 downto 0);
	signal crtc_romchar_data_from_s	: std_logic_vector( 7 downto 0);

begin

	 -- CPU
	cpu: entity work.T80a
	generic map (
		Mode			=> 0
	)
	port map (
		CLK_n			=> clock_cpu_s,
		RESET_n		=> reset_n_s,
		A				=> cpu_addr_s,
		Din			=> cpu_data_to_s,
		Dout			=> cpu_data_from_s,
		WAIT_n		=> cpu_wait_n_s,
		INT_n			=> cpu_int_n_s,
		NMI_n			=> cpu_nmi_n_s,
		M1_n			=> cpu_m1_n_s,
		MREQ_n		=> cpu_mreq_n_s,
		IORQ_n		=> cpu_iorq_n_s,
		RD_n			=> cpu_rd_n_s,
		WR_n			=> cpu_wr_n_s,
		RFSH_n		=> open,--cpu_rfsh_n_s,
		HALT_n		=> open,
		BUSRQ_n		=> '1',
		BUSAK_n		=> open
	);

	-- PSG
	psg: entity work.YM2149
	port map (
		clock_i				=> clock_psg_s,
		clock_en_i			=> '1',
		reset_i				=> reset_i,
		sel_n_i				=> '1',
		ayymmode_i			=> '1',
		-- data bus
		data_i				=> cpu_data_from_s,
		data_o				=> psg_data_from_s,
		-- control
		a9_l_i				=> '0',
		a8_i					=> '1',
		bdir_i				=> psg_bdir_s,
		bc1_i					=> psg_bc1_s,
		bc2_i					=> '1',
		-- I/O ports
		port_a_i				=> (others => '0'),
		port_a_o				=> keyb_rows_s,
		port_b_i				=> psg_port_b_s,
		port_b_o				=> open,
		-- audio channels out
		audio_ch_a_o		=> open,
		audio_ch_b_o		=> open,
		audio_ch_c_o		=> open,
		audio_ch_mix_o		=> audio_psg_o
	);

	-- VDG
	mc6847_inst: entity work.mc6847
	port map (
		clock_i				=> clock_i,
		clock_en_i			=> clock_14m_en_s,
		reset_i				=> reset_i,
		da0_o					=> open,
		vr_addr_o			=> vdg_addr_s,
		vr_data_from_i		=> vdg_data_to_s,
		vr_oe_o				=> vdg_oe_s,
		hs_n_o				=> vdg_hs_n_s,
		fs_n_o				=> vdg_fs_n_s,
		an_g_i				=> vdg_an_g_s,
		an_s_i				=> vdg_an_s_s,
		intn_ext_i			=> vdg_intn_s,
		gm_i					=> vdg_gm_s,
		css_i					=> vdg_css_s,
		inv_i					=> vdg_inv_s,
		video_col_idx_o	=> video_col_idx_o,
		video_r_o			=> open,--vdg_r_s,
		video_g_o			=> open,--vdg_g_s,
		video_b_o			=> open,--vdg_b_s,
		video_hs_o			=> vdg_hs_s,
		video_vs_o			=> vdg_vs_s,
		video_hb_o			=> vdg_hb_s,
		video_vb_o			=> vdg_vb_s,
		cnt_hor_o			=> cnt_hor_o,
		cnt_ver_o			=> cnt_ver_o
	);

	-- CRTC
	mc6845_inst: entity work.mc6845
	port map (
		clock_i			=> clock_i,
		clock_en_i		=> clock_crtc_en_s,			-- 1.79 MHz (14.31818 / 8)
		reset_n_i		=> reset_n_s,
		-- Bus interface
		cs_n_i			=> crtc_cs_n_s,
		read_writen_i	=> cpu_wr_n_s,
		reg_sel_i		=> cpu_addr_s(0),
		data_i			=> cpu_data_from_s,
		data_o			=> crtc_data_from_s,
		-- Display interface
		vsync_o			=> crtc_vsync_s,
		hsync_o			=> crtc_hsync_s,
		data_enable_o	=> crtc_de_s,
		cursor_en_o		=> crtc_cursor_en_s,
		lpstb_i			=> '0',
		-- Memory interface
		ma_o				=> crtc_ma_s,
		ra_o				=> crtc_ra_s
	);

	-- VRAM
--	vram_inst: entity work.spram
--	generic map (
--		addr_width_g	=> 11,
--		data_width_g	=> 8
--	)
--	port map (
--		clk_i				=> clock_i,
--		addr_i			=> vram_addr_s,
--		data_i			=> cpu_data_from_s,
--		data_o			=> vram_data_from_s,
--		we_i				=> vram_we_s
--	);

	-- ROM char CRTC
--	crtc_romchar: entity work.mc6845_romchar
--	port map (
--		clk		=> clock_i,
--		addr		=> crtc_romchar_addr_s,
--		data		=> crtc_romchar_data_from_s
--	);

	-- Teclado
	keyb: entity work.keyboard
	generic map (
		clkfreq_g	=> 28572			-- 28.571429 MHz
	)
	port map (
		clock_i		=> clock_i,
		enable_i		=> not opt_keyboard_i,
		reset_i		=> reset_i,
		-- PS/2 interface
		ps2_clk_io	=> ps2_clk_io,
		ps2_data_io	=> ps2_data_io,
		-- Row
		rows_i		=> keyb_rows_s,
		-- Column
		cols_o		=> keyb_cols_s,
		FKeys_o		=> open
	);

	keyb_new: entity work.keyboard_new
	generic map (
		clkfreq_g	=> 28572			-- 28.571429 MHz
	)
	port map (
		clock_i		=> clock_i,
		enable_i		=> opt_keyboard_i,
		reset_i		=> reset_i,
		-- PS/2 interface
		ps2_clk_io	=> ps2_clk_io,
		ps2_data_io	=> ps2_data_io,
		-- Row
		rows_i		=> keyb_rows_s,
		-- Column
		cols_o		=> keyb_cols_new_s,
		FKeys_o		=> open
	);

	-- Glue
	reset_n_s	<= not reset_i;

	-- Gerar clocks e int_n
	process (clock_i)
		variable counter_v	: unsigned(3 downto 0)	:= (others => '0');
		variable duty_v		: std_logic					:= '1';
		variable counterH_v	: unsigned(16 downto 0)	:= (others => '0');
		variable counterL_v	: unsigned(7 downto 0)	:= (others => '0');
	begin
		if rising_edge(clock_i) then
			clk_cnt_s			<= clk_cnt_s + 1;
			counter_v			:= counter_v + 1;
			clock_14m_en_s 	<= counter_v(0);		-- 14.318181 MHz (14.285.714,5)
			clock_psg_s			<= counter_v(3);		-- 1.79 MHz
			-- Clock Enable MC6845 = 1.79 MHz
			clock_crtc_en_s	<= '0';
			if counter_v = 0  then
				clock_crtc_en_s	<= '1';
			end if;
			-- interrupcao do Z80
			if duty_v = '1' then
				counterH_v := counterH_v + 1;
				if counterH_v = 98522 then				-- duracao em '1' de 35 ns * 98522 = 3448270 ns = 3,448 ms (290,02 Hz)
					counterH_v := (others => '0');
					duty_v := '0';
				end if;
			else
				counterL_v := counterL_v + 1;
				if counterL_v = 252 then					-- duracao em '0' de 35 ns * 251 = 8820 ns = 8,82 us
					counterL_v := (others => '0');
					duty_v := '1';
				end if;
			end if;
			cpu_int_n_s <= duty_v;
		end if;
	end process;

	-- CPU
	-- Clock da CPU
	with D_clk_src_i select
	clock_cpu_s <=
		clk_cnt_s(2)	when "000",			-- 3579545 Hz
		clk_cnt_s(3)	when "001",			-- 1789772 Hz
		clk_cnt_s(7)	when "010",			--  111860 Hz
		clk_cnt_s(21)	when "011",			--       6 Hz
		D_clk_manual_i	when others;

	cpu_nmi_n_s		<= '1';
	cpu_wait_n_s	<= '0' when vram_vdg_en_s = '1'  and vdg_gate_s = '1'	else 				-- Pausa CPU quando VDG acessando VRAM junto com CPU
							'0' when vram_crtc_en_s = '1' and crtc_de_s  = '1'	else 				-- Pausa CPU quando CRTC acessando VRAM junto com CPU
							'1';

	-- MUX de dados
	cpu_data_to_s	<=
							rom_data_from_i			when rom_cs_s = '1'			else
							vram_data_from_i			when vram_vdg_en_s = '1'	else
							vram_data_from_s			when vram_crtc_en_s = '1'	else				-- Antes de ram16_en_s e ram48_en_s
							ram_data_from_i			when ram16_en_s = '1'		else
							ram_data_from_i			when ram48_en_s = '1'		else
							psg_data_from_s			when port40_en_s = '1'		else
							crtc_data_from_s			when crtc_cs_n_s = '0'		else
							(others => '1');

	-- Sinais selecao memorias
	ram16_en_s		<= '1' when cpu_mreq_n_s = '0' and cpu_addr_s(15 downto 14) = "00"									else '0';
	rom_en_s			<= '1' when cpu_mreq_n_s = '0' and cpu_addr_s(15 downto 14) = "11"									else '0';
	ram48_en_s		<= '1' when cpu_mreq_n_s = '0' and rom_en_s = '0' and vram_vdg_en_s = '0' and opt_ram64_i = '1'	else '0';

	-- Flag da "ROM na RAM"
	process (reset_i, rom_en_s)
	begin
		if reset_i = '1' then
			rom_c000_q <= '1';
		elsif falling_edge(rom_en_s) then
			rom_c000_q <= '0';
		end if;
	end process;

	rom_addr_o		<= cpu_addr_s(13 downto 0);
	rom_cs_s			<= rom_en_s or rom_c000_q;

	ram_addr_o		<= cpu_addr_s;
	ram_data_to_o	<= cpu_data_from_s;
	ram_cs_o			<= ram16_en_s or ram48_en_s;
	ram_oe_o			<= not cpu_rd_n_s;
	ram_we_o			<= not cpu_wr_n_s;

	-- Sinais selecao portas I/O
	port20_en_s	<= '1'	when cpu_addr_s(7 downto 5) = "001" and cpu_m1_n_s = '1' and cpu_iorq_n_s = '0'	else '0';
	port40_en_s	<= '1'	when cpu_addr_s(7 downto 5) = "010" and cpu_m1_n_s = '1' and cpu_iorq_n_s = '0'	else '0';
	port60_en_s	<= '1'	when cpu_addr_s(7 downto 5) = "011" and cpu_m1_n_s = '1' and cpu_iorq_n_s = '0'	else '0';
	port80_en_s	<= '1'	when cpu_addr_s(7 downto 5) = "100" and cpu_m1_n_s = '1' and cpu_iorq_n_s = '0'	else '0';

	-- Escrita porta 80
	process (reset_i, port80_en_s)
	begin
		if reset_i = '1' then
			port80_r	<= (others => '0');
		elsif falling_edge(port80_en_s) then
			port80_r	<= cpu_data_from_s;
		end if;
	end process;

	-- PSG
--	psg_addr_wr_en_s	<= '1'	when port20_en_s = '1' and cpu_wr_n_s = '0'		else '0';		-- Escrita na porta 20 seta endereco registrador
--	psg_reg_rd_en_s	<= '1'	when port40_en_s = '1' and cpu_rd_n_s = '0'		else '0';		-- Leitura na porta 40 le valor registrador
--	psg_reg_wr_en_s	<= '1'	when port60_en_s = '1' and cpu_wr_n_s = '0'		else '0';		-- Escrita na porta 60 seta valor registrador
	psg_bc1_s	<= port20_en_s or port40_en_s;
	psg_bdir_s	<= port20_en_s or port60_en_s;

	psg_port_b_s	<= keyb_cols_s     and ear_joy_s		when opt_keyboard_i = '0'	else
							keyb_cols_new_s and ear_joy_s;

	process (keyb_rows_s, ear_i, joy1_i, joy2_i)
	begin
		if keyb_rows_s(0) = '0' then
			ear_joy_s	<= ear_i & "11" & not joy2_i(0) & not joy2_i(1) & not joy2_i(2) & not joy2_i(3) & not joy2_i(4);
		elsif keyb_rows_s(1) = '0' then
			ear_joy_s	<= ear_i & "1" & not joy1_i(4) & not joy1_i(0) & not joy1_i(1) & not joy1_i(2) & not joy1_i(3) & "1";
		else
			ear_joy_s	<= ear_i & "1111111";
		end if;
	end process;
	
	-- K7
	mic_o			<= keyb_rows_s(7);

	-- VDG
	vram_vdg_en_s	<= '1' when cpu_mreq_n_s = '0' and cpu_addr_s(15 downto 13) = "100" and port80_r(0) = '0'		else '0';	-- 0x8000 a 0x9FFF

	vram_addr_o		<= cpu_addr_s(12 downto 0)		when vdg_gate_s = '0' and vram_vdg_en_s = '1'	else vdg_addr_s;
	vram_data_to_o	<= cpu_data_from_s;
	vram_we_o		<= not cpu_wr_n_s 				when vdg_gate_s = '0' and vram_vdg_en_s = '1'	else '0';
	vram_oe_o		<= not cpu_rd_n_s					when vdg_gate_s = '0' and vram_vdg_en_s = '1'	else vdg_oe_s;


	vdg_data_to_s	<= vram_data_from_i(7) & vdg_new6_s & vram_data_from_i(5 downto 0);
	vdg_gate_s		<= vdg_fs_n_s and vdg_hs_n_s;				-- 1 quando VDG acessa VRAM
	vdg_inv_s		<= vram_data_from_i(7);
	vdg_an_g_s		<= port80_r(7);
	vdg_an_s_s		<= port80_r(6)		when opt_graph_ch_i = '0'	else vdg_temp_s or port80_r(6);
	vdg_intn_s		<= port80_r(5);
	vdg_gm_s			<= port80_r(4 downto 2);
	vdg_css_s		<= port80_r(1);
	vdg_temp_s		<= not(port80_r(7) or port80_r(6)) and not(vram_data_from_i(6) xor vram_data_from_i(5));
	vdg_new6_s		<= vram_data_from_i(6)	when opt_graph_ch_i = '0'	else
							(vdg_temp_s and vram_data_from_i(7)) or (not vdg_temp_s and vram_data_from_i(6));

	-- CRTC
	crtc_cs_n_s			<= '0'	when cpu_addr_s(7 downto 1) = "0001000" and cpu_m1_n_s = '1' and cpu_iorq_n_s = '0'			else '1';		-- I/O ports 10 and 11
	port12_en_s			<= '1'	when cpu_addr_s(7 downto 0) = X"12"     and cpu_m1_n_s = '1' and cpu_iorq_n_s = '0'			else '0';
	vram_crtc_en_s		<= '1'	when cpu_addr_s(15 downto 11) = "00100" and cpu_mreq_n_s = '0' and crtc_vram_bank_q = '1'	else '0';		-- 0x2000 a 0x27FF
	crtc_addr15_en_s	<= '1'	when cpu_addr_s = X"000F"               and cpu_mreq_n_s = '0' and cpu_wr_n_s = '0'			else '0';

	vram_addr_s	<= cpu_addr_s(10 downto 0)	when vram_crtc_en_s = '1' and crtc_de_s = '0'	else crtc_ma_s(10 downto 0);
	vram_we_s	<= not cpu_wr_n_s				when vram_crtc_en_s = '1' and crtc_de_s = '0'	else '0';
	
	-- Escrita porta 12
	process (reset_i, port12_en_s)
	begin
		if reset_i = '1' then
			crtc_vram_bank_q	<= '0';
		elsif falling_edge(port12_en_s) then
			crtc_vram_bank_q	<= cpu_data_from_s(0);
		end if;
	end process;

	-- Escrita memória endereco 0x000F
	process (reset_i, crtc_addr15_en_s)
	begin
		if reset_i = '1' then
			crtc_enable_s	<= '0';
		elsif falling_edge(crtc_addr15_en_s) then
			crtc_enable_s	<= cpu_data_from_s(0);
		end if;
	end process;

	crtc_romchar_addr_s	<= vram_data_from_s(6 downto 0) & crtc_ra_s(3 downto 0);

	-- Atraso DE e cursor
	process (clock_i, clock_crtc_en_s)
	begin
		if rising_edge(clock_i) and clock_crtc_en_s = '1' then
			crtc_de_dly_s <= crtc_de_s;
			crtc_cursor_en_dly_s	<= crtc_cursor_en_s;
		end if;
	end process;

	-- Geracao dot pixel do CRTC
	process (clock_i, clock_14m_en_s)
		variable counter_v	: unsigned(2 downto 0);
	begin
		if rising_edge(clock_i) and clock_14m_en_s = '1' then
			if counter_v = 0 then
				crtc_video_data_s <= crtc_romchar_data_from_s;
			else
				crtc_video_data_s <= crtc_video_data_s(6 downto 0) & '0';
			end if;
			counter_v := counter_v + 1;
		end if;
	end process;

	crtc_video_pixel_s <= crtc_video_data_s(7) xor crtc_cursor_en_dly_s	when crtc_de_dly_s = '1'		else '0';
	video_mode80_o	<= crtc_enable_s;

	-- Saida video
	video_80_pixel_o	<= crtc_video_pixel_s;
--	video_r_o	<= vdg_r_s		when crtc_enable_s = '0'	else (others => '0');
--	video_g_o	<= vdg_g_s		when crtc_enable_s = '0'	else (others => crtc_video_pixel_s);
--	video_b_o	<= vdg_b_s		when crtc_enable_s = '0'	else (others => '0');
	video_hs_o	<= vdg_hs_s		when crtc_enable_s = '0'	else not crtc_hsync_s;
	video_vs_o	<= vdg_vs_s		when crtc_enable_s = '0'	else not crtc_vsync_s;
	video_hb_o	<= vdg_hb_s		when crtc_enable_s = '0'	else '0';
	video_vb_o	<= vdg_vb_s		when crtc_enable_s = '0'	else '0';

	-- Debug
	D_cpu_addr_o	<= cpu_addr_s;
--	D_rom_cs_o		<= rom_cs_s;
--	D_rom_c000_o	<= rom_c000_q;
--	D_rows_o			<= keyb_rows_s;
--	D_crtc_vram_bank_o	<= crtc_vram_bank_q;
--	D_80col_en_o	<= crtc_enable_s;

end architecture;