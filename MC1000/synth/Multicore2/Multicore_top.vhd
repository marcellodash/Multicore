

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity multicore_top is
	port (
-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= '0'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
	);
end entity;

use work.mc6847_col_pack.all;

architecture Behavior of multicore_top is

	
	-- Clocks
	signal clock_master_s	: std_logic;
	signal clock_vga_s		: std_logic;
	signal clock_dvi_s		: std_logic;

	-- Reset
	signal pll_locked_s		: std_logic;
	signal reset_s				: std_logic;

	-- Memorias
	signal ram_addr_s			: std_logic_vector(15 downto 0);
	signal ram_data_from_s	: std_logic_vector( 7 downto 0);
	signal ram_data_to_s		: std_logic_vector( 7 downto 0);
	signal ram_cs_s			: std_logic;
	signal ram_oe_s			: std_logic;
	signal ram_we_s			: std_logic;
	signal rom_addr_s			: std_logic_vector(13 downto 0);
	signal rom_data_from_s	: std_logic_vector( 7 downto 0);
	-- VRAM
	signal vram_addr_s		: std_logic_vector(12 downto 0);
	signal vram_data_from_s	: std_logic_vector( 7 downto 0);
	signal vram_data_to_s	: std_logic_vector( 7 downto 0);
	signal vram_oe_s			: std_logic;
	signal vram_we_s			: std_logic;

	-- K7
	signal mic_s				: std_logic;
	signal ear_e_s				: std_logic;
	signal ear_en_s			: std_logic 	:= '1';
	signal btn_ear_s			: std_logic 	:= '1';
	

	-- Audio
	signal psg_s				: std_logic_vector( 7 downto 0);
	signal audio_dac_s				: std_logic;

	-- Video
	signal video_mode80_s	: std_logic;
	signal video_col_idx_s	: std_logic_vector( 3 downto 0);
	signal video_15khz_s		: std_logic_vector( 3 downto 0);
	signal video_31khz_s		: std_logic_vector( 3 downto 0);
	signal video_80_pixel_s	: std_logic;
--	signal video_r_s			: std_logic_vector( 7 downto 0);
--	signal video_g_s			: std_logic_vector( 7 downto 0);
--	signal video_b_s			: std_logic_vector( 7 downto 0);
	signal video_hs_s			: std_logic;
	signal video_vs_s			: std_logic;
--	signal video_hb_s			: std_logic;
--	signal video_vb_s			: std_logic;

	-- Joystick (Minimig standard)
	signal joy1_s				: std_logic_vector(4 downto 0);
	signal joy2_s				: std_logic_vector(4 downto 0);

	-- HDMI
	signal rgb_col_s			: std_logic_vector( 3 downto 0);		-- 15KHz
	signal rgb_hsync_n_s		: std_logic;								-- 15KHz
	signal rgb_vsync_n_s		: std_logic;								-- 15KHz
	signal cnt_hor_s			: std_logic_vector( 8 downto 0);
	signal cnt_ver_s			: std_logic_vector( 7 downto 0);
	signal vga_col_s			: std_logic_vector( 3 downto 0);
	signal vga_r_s				: std_logic_vector( 3 downto 0);
	signal vga_g_s				: std_logic_vector( 3 downto 0);
	signal vga_b_s				: std_logic_vector( 3 downto 0);
	signal vga_hsync_n_s		: std_logic;
	signal vga_vsync_n_s		: std_logic;
	signal vga_blank_s		: std_logic;
	signal sound_hdmi_s		: std_logic_vector(15 downto 0);

		
	signal tdms_r_s			: std_logic_vector( 9 downto 0);
	signal tdms_g_s			: std_logic_vector( 9 downto 0);
	signal tdms_b_s			: std_logic_vector( 9 downto 0);
	
	-- HDMI
	signal hdmi_31khz_s		: std_logic_vector( 7 downto 0);
	signal hdmi_r_s			: std_logic_vector( 3 downto 0);
	signal hdmi_g_s			: std_logic_vector( 3 downto 0);
	signal hdmi_b_s			: std_logic_vector( 3 downto 0);
	signal hdmi_p_s			: std_logic_vector( 3 downto 0);
	signal hdmi_n_s			: std_logic_vector( 3 downto 0);
	signal hdmi_hsync_n_s	: std_logic;
	signal hdmi_vsync_n_s	: std_logic;
	signal hdmi_blank_s		: std_logic;
	
	-- scanlines
	signal scanlines_en_s		: std_logic := '0';
	signal btn_scan_s				: std_logic;
	signal odd_line_s				: std_logic := '0';
	signal vga_r_out_s			: std_logic_vector( 2 downto 0);
	signal vga_g_out_s			: std_logic_vector( 2 downto 0);
	signal vga_b_out_s			: std_logic_vector( 2 downto 0);
	
	
	
	-- Debug
	signal D_cpu_addr_s		: std_logic_vector(15 downto 0);
	signal D_display_s		: std_logic_vector(15 downto 0);

	
begin

-- PLL
	pll: entity work.pll1
	port map (
		inclk0	=> clock_50_i,
		c0			=> clock_master_s,			-- 28.571429 MHz
		c1			=> clock_vga_s,
		c2			=> clock_dvi_s,
		locked	=> pll_locked_s
	);

	mc1000_inst: entity work.mc1000
	port map (
		clock_i				=> clock_master_s,
		reset_i				=> reset_s,
		-- Options
		opt_ram64_i			=> '1',
		opt_graph_ch_i		=> '1',
		opt_keyboard_i		=> '0',
		-- RAM memory
		ram_addr_o			=> ram_addr_s,
		ram_data_from_i	=> ram_data_from_s,
		ram_data_to_o		=> ram_data_to_s,
		ram_cs_o				=> ram_cs_s,
		ram_oe_o				=> ram_oe_s,
		ram_we_o				=> ram_we_s,
		-- ROM memory
		rom_addr_o			=> rom_addr_s,
		rom_data_from_i	=> rom_data_from_s,
		-- VRAM memory
		vram_addr_o			=> vram_addr_s,
		vram_data_from_i	=> vram_data_from_s,
		vram_data_to_o		=> vram_data_to_s,
		vram_oe_o			=> vram_oe_s,
		vram_we_o			=> vram_we_s,
		-- PS/2 Keyboard
		ps2_clk_io			=> ps2_clk_io,
		ps2_data_io			=> ps2_data_io,
		-- K7
		mic_o					=> mic_s,
		ear_i					=> ear_e_s,
		-- Joystick (order: Fire, Up, Down, Left, Right)
		joy1_i				=> joy1_s,
		joy2_i				=> joy2_s,
		-- Audio
		audio_psg_o			=> psg_s,
		-- Video
		video_mode80_o		=> video_mode80_s,
		video_col_idx_o	=> video_col_idx_s,
		video_80_pixel_o	=> video_80_pixel_s,
--		video_r_o			=> video_r_s,
--		video_g_o			=> video_g_s,
--		video_b_o			=> video_b_s,
		video_hs_o			=> video_hs_s,
		video_vs_o			=> video_vs_s,
		video_hb_o			=> open,--video_hb_s,
		video_vb_o			=> open,--video_vb_s,
		cnt_hor_o			=> cnt_hor_s,
		cnt_ver_o			=> cnt_ver_s,
		-- Debug
		D_clk_src_i			=> "000",
		D_clk_manual_i		=> '0',
--		D_rom_c000_o		=> LEDR(0),
--		D_rom_cs_o			=> LEDR(1),
--		D_rows_o				=> LEDG,
--		D_80col_en_o		=> LEDG(0),
--		D_crtc_vram_bank_o => LEDG(1),
		D_cpu_addr_o		=> D_cpu_addr_s
	);

	-- ROM 16K
	rom: entity work.mc1000_rom
	port map (
		clock			=> clock_master_s,
		address		=> rom_addr_s,
		q				=> rom_data_from_s
	);
	
			-- SRAM
	sram0: entity work.dpSRAM_5128
	port  map (
		clk				=> clock_master_s,
		-- Port 0
		porta0_addr		=> "100000" & vram_addr_s,
		porta0_ce		=> '1',
		porta0_oe		=> vram_oe_s,
		porta0_we		=> vram_we_s,
		porta0_din		=> vram_data_to_s,
		porta0_dout		=> vram_data_from_s,
		-- Port 1
		porta1_addr		=> "000" & ram_addr_s,
		porta1_ce		=> ram_cs_s,
		porta1_oe		=> ram_oe_s,
		porta1_we		=> ram_we_s,
		porta1_din		=> ram_data_to_s,
		porta1_dout		=> ram_data_from_s,
		-- SRAM in board
		sram_addr		=> sram_addr_o,
		sram_data		=> sram_data_io,
		sram_ce_n		=> open,
		sram_oe_n		=> sram_oe_n_o,
		sram_we_n		=> sram_we_n_o
	);
	
	btnEar: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clock_master_s,
		button_i			=> btn_n_i(1),
		result_o			=> btn_ear_s
	);
	
	process (btn_ear_s)
	begin
		if falling_edge(btn_ear_s) then
			ear_en_s <= not ear_en_s;
		end if;
	end process;
	
	
	
	-- Audio
	audioout: entity work.dac
	generic map (
		msbi_g		=> 7
	)
	port map (
		clk_i		=> clock_master_s,
		res_i		=> reset_s,
		dac_i		=> psg_s,
		dac_o		=> audio_dac_s
	);
	

	-- Glue
	reset_s <= '1' when pll_locked_s = '0' or (btn_n_i(3) = '0' and btn_n_i(4) = '0')	else '0';

	-- Joystick
	-- ordem: Fire, Up, Down, Left, Right
	joy1_s <= not (joy1_p6_i & joy1_up_i & joy1_down_i & joy1_left_i & joy1_right_i);
	joy2_s <= not (joy2_p6_i & joy2_up_i & joy2_down_i & joy2_left_i & joy2_right_i);

	-- Ear
	ear_e_s <= ear_i;-- or ear_en_s;

	-- Video
	video_15khz_s	<= video_col_idx_s	when video_mode80_s = '0'	else video_80_pixel_s & "000";
	
	
	
	
	-----------------------------------------------------------------------------
	-- VGA Output
	-----------------------------------------------------------------------------

		
	-- Audio
	dac_l_o	<= audio_dac_s;
	dac_r_o	<= audio_dac_s;
	

	
	process (clock_master_s)
		variable vga_col_v						: natural range 0 to 15;
		variable vga_r_v, vga_g_v, vga_b_v	: rgb_val_t;
	begin
		if rising_edge(clock_master_s) then
			vga_col_v	:= to_integer(unsigned(video_31khz_s));
--			vga_col_v	:= to_integer(unsigned(video_col_idx_s));
			vga_r_v	:= rgb_table_c(vga_col_v)(r_c);
			vga_g_v	:= rgb_table_c(vga_col_v)(g_c);
			vga_b_v	:= rgb_table_c(vga_col_v)(b_c);
			vga_r_s		<= std_logic_vector(to_unsigned(vga_r_v, 8))(7 downto 4);
			vga_g_s		<= std_logic_vector(to_unsigned(vga_g_v, 8))(7 downto 4);
			vga_b_s		<= std_logic_vector(to_unsigned(vga_b_v, 8))(7 downto 4);
		end if;
	end process;
	
	process (clock_master_s)
		variable vga_col_v						: natural range 0 to 15;
		variable vga_r_v, vga_g_v, vga_b_v	: rgb_val_t;
	begin
		if rising_edge(clock_master_s) then
			vga_col_v	:= to_integer(unsigned(hdmi_31khz_s));
--			vga_col_v	:= to_integer(unsigned(video_col_idx_s));
			vga_r_v	:= rgb_table_c(vga_col_v)(r_c);
			vga_g_v	:= rgb_table_c(vga_col_v)(g_c);
			vga_b_v	:= rgb_table_c(vga_col_v)(b_c);
			hdmi_r_s		<= std_logic_vector(to_unsigned(vga_r_v, 8))(7 downto 4);
			hdmi_g_s		<= std_logic_vector(to_unsigned(vga_g_v, 8))(7 downto 4);
			hdmi_b_s		<= std_logic_vector(to_unsigned(vga_b_v, 8))(7 downto 4);
		end if;
	end process;
	


	
		-- VGA framebuffer
		vga: entity work.vga
		port map (
			I_CLK			=> clock_master_s,
			I_CLK_VGA	=> clock_vga_s,
			I_COLOR		=> video_15khz_s(3 downto 1),
			I_HCNT		=> cnt_hor_s,
			I_VCNT		=> cnt_ver_s(7 downto 0),
			O_HSYNC		=> hdmi_hsync_n_s,
			O_VSYNC		=> hdmi_vsync_n_s,
			O_COLOR		=> hdmi_31khz_s(3 downto 1),
			O_HCNT		=> open,
			O_VCNT		=> open,
			O_H			=> open,
			O_BLANK		=> hdmi_blank_s
		);
	


		
		
		--- vga
	
		-- Scandoubler
		dblscan: entity work.scandoubler
		generic map (
			hSyncLength_g	=> 31,
			vSyncLength_g	=> 11,
			ramBits_g		=> 11,
			addr_width_g	=> 4,
			hsync_pol_g		=> '0',
			vsync_pol_g		=> '0'
		)
		port map (
			clock_i			=> clock_master_s,
			enable_i			=> '1',
			scanlines_en_i	=> '0',
			video_i			=> video_15khz_s,
			hsync_i			=> video_hs_s,
			vsync_i			=> video_vs_s,
			video_o			=> video_31khz_s,
			hsync_o			=> vga_hsync_n_s,
			vsync_o			=> vga_vsync_n_s
		);


		vga_r_o			<= vga_r_out_s & "00";
		vga_g_o			<= vga_g_out_s & "00";
		vga_b_o			<= vga_b_out_s & "00";
		vga_hsync_n_o	<= vga_hsync_n_s;
		vga_vsync_n_o	<= vga_vsync_n_s;
	
	
	-- HDMI
 		inst_dvid: entity work.hdmi
 		generic map (
 			FREQ	=> 25000000,	-- pixel clock frequency 
 			FS		=> 48000,		-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
 			CTS	=> 25000,		-- CTS = Freq(pixclk) * N / (128 * Fs)
 			N		=> 6144			-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
 		) 
 		port map (
 			I_CLK_PIXEL		=> clock_vga_s,
			
			I_R				=> hdmi_r_s & hdmi_r_s ,
			I_G				=> hdmi_g_s & hdmi_g_s ,
			I_B				=> hdmi_b_s & hdmi_b_s ,
			
			I_BLANK			=> hdmi_blank_s,
			I_HSYNC			=> hdmi_hsync_n_s,
			I_VSYNC			=> hdmi_vsync_n_s,
			
			-- PCM audio
			I_AUDIO_ENABLE	=> '1',
			I_AUDIO_PCM_L 	=> sound_hdmi_s,
			I_AUDIO_PCM_R	=> sound_hdmi_s,

			-- TMDS parallel pixel synchronous outputs (serialize LSB first)
 			O_RED				=> tdms_r_s,
			O_GREEN			=> tdms_g_s,
			O_BLUE			=> tdms_b_s
		);
		

			hdmio: entity work.hdmi_out_altera
		port map (
			clock_pixel_i		=> clock_vga_s,
			clock_tdms_i		=> clock_dvi_s,
			red_i					=> tdms_r_s,
			green_i				=> tdms_g_s,
			blue_i				=> tdms_b_s,
			tmds_out_p			=> hdmi_p_s,
			tmds_out_n			=> hdmi_n_s
		);
 		
		
		tmds_o(7)	<= hdmi_p_s(2);	-- 2+		
		tmds_o(6)	<= hdmi_n_s(2);	-- 2-		
		tmds_o(5)	<= hdmi_p_s(1);	-- 1+			
		tmds_o(4)	<= hdmi_n_s(1);	-- 1-		
		tmds_o(3)	<= hdmi_p_s(0);	-- 0+		
		tmds_o(2)	<= hdmi_n_s(0);	-- 0-	
		tmds_o(1)	<= hdmi_p_s(3);	-- CLK+	
		tmds_o(0)	<= hdmi_n_s(3);	-- CLK-	
	

	---------------------------------
	-- scanlines
	btnscl: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clock_master_s,
		button_i			=> btn_n_i(1) or btn_n_i(2),
		result_o			=> btn_scan_s
	);
	
	process (btn_scan_s)
	begin
		if falling_edge(btn_scan_s) then
			scanlines_en_s <= not scanlines_en_s;
		end if;
	end process;
	
	
	--vga_r_out_s <= '0' & vga_r_s(2 downto 1) when scanlines_en_s = '1' and odd_line_s = '1' else vga_r_s(3 downto 1);
	--vga_g_out_s <= '0' & vga_g_s(2 downto 1) when scanlines_en_s = '1' and odd_line_s = '1' else vga_g_s(3 downto 1);
	--vga_b_out_s <= '0' & vga_b_s(2 downto 1) when scanlines_en_s = '1' and odd_line_s = '1' else vga_b_s(3 downto 1);
	
	vga_r_out_s <=  vga_r_s(3 downto 1) - 2 when vga_r_s(3 downto 1) > 1 and scanlines_en_s = '1' and odd_line_s = '1' else vga_r_s(3 downto 1);
	vga_g_out_s <=  vga_g_s(3 downto 1) - 2 when vga_g_s(3 downto 1) > 1 and scanlines_en_s = '1' and odd_line_s = '1' else vga_g_s(3 downto 1);
	vga_b_out_s <=  vga_b_s(3 downto 1) - 2 when vga_b_s(3 downto 1) > 1 and scanlines_en_s = '1' and odd_line_s = '1' else vga_b_s(3 downto 1);

	
	process(vga_hsync_n_s,vga_vsync_n_s)
	begin
		if vga_vsync_n_s = '0' then
			odd_line_s <= '0';
		elsif rising_edge(vga_hsync_n_s) then
			odd_line_s <= not odd_line_s;
		end if;
	end process;
	
	
	
end architecture;

