------------------------------------------------------------------------------
-- GALAXIAN TOP level implementation 
--
------------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
  use ieee.numeric_std.all;

entity de2_top is
port(
 CLOCK_50  : in std_logic;
-- clock_27  : in std_logic;
-- ext_clock : in std_logic;
-- ledr      : out std_logic_vector(17 downto 0);
-- ledg      : out std_logic_vector(8 downto 0);
 KEY       : in std_logic_vector(3 downto 0);
 SW        : in std_logic_vector(17 downto 0);

-- dram_ba_0  : out std_logic;
-- dram_ba_1  : out std_logic;
-- dram_ldqm  : out std_logic;
-- dram_udqm  : out std_logic;
-- dram_ras_n : out std_logic;
-- dram_cas_n : out std_logic;
-- dram_cke   : out std_logic;
-- dram_clk   : out std_logic;
-- dram_we_n  : out std_logic;
-- dram_cs_n  : out std_logic;
-- dram_dq    : inout std_logic_vector(15 downto 0);
-- dram_addr  : out std_logic_vector(11 downto 0);
--
-- fl_addr  : out std_logic_vector(21 downto 0);
-- fl_ce_n  : out std_logic;
-- fl_oe_n  : out std_logic;
-- fl_dq    : inout std_logic_vector(7 downto 0);
-- fl_rst_n : out std_logic;
-- fl_we_n  : out std_logic;
--
-- hex0 : out std_logic_vector(6 downto 0);
-- hex1 : out std_logic_vector(6 downto 0);
-- hex2 : out std_logic_vector(6 downto 0);
-- hex3 : out std_logic_vector(6 downto 0);
-- hex4 : out std_logic_vector(6 downto 0);
-- hex5 : out std_logic_vector(6 downto 0);
-- hex6 : out std_logic_vector(6 downto 0);
-- hex7 : out std_logic_vector(6 downto 0);

 PS2_CLK : inout std_logic;
 PS2_DAT : inout std_logic;

-- uart_txd : out std_logic;
-- uart_rxd : in std_logic;
--
-- lcd_rw   : out std_logic;
-- lcd_en   : out std_logic;
-- lcd_rs   : out std_logic;
-- lcd_data : out std_logic_vector(7 downto 0);
-- lcd_on   : out std_logic;
-- lcd_blon : out std_logic;
 
-- sram_addr : out std_logic_vector(17 downto 0);
-- sram_dq   : inout std_logic_vector(15 downto 0);
-- sram_we_n : out std_logic;
-- sram_oe_n : out std_logic;
-- sram_ub_n : out std_logic;
-- sram_lb_n : out std_logic;
-- sram_ce_n : out std_logic;
 
-- otg_addr   : out std_logic_vector(1 downto 0);
-- otg_cs_n   : out std_logic;
-- otg_rd_n   : out std_logic;
-- otg_wr_n   : out std_logic;
-- otg_rst_n  : out std_logic;
-- otg_data   : inout std_logic_vector(15 downto 0);
-- otg_int0   : in std_logic;
-- otg_int1   : in std_logic;
-- otg_dack0_n: out std_logic;
-- otg_dack1_n: out std_logic;
-- otg_dreq0  : in std_logic;
-- otg_dreq1  : in std_logic;
-- otg_fspeed : inout std_logic;
-- otg_lspeed : inout std_logic;
-- 
-- tdi : in std_logic;
-- tcs : in std_logic;
-- tck : in std_logic;
-- tdo : out std_logic;
 
 VGA_R     : out std_logic_vector(9 downto 0);
 VGA_G     : out std_logic_vector(9 downto 0);
 VGA_B     : out std_logic_vector(9 downto 0);
 VGA_CLK   : out std_logic;
 VGA_BLANK : out std_logic;
 VGA_HS    : out std_logic;
 VGA_VS    : out std_logic;
 VGA_SYNC  : out std_logic;

 I2C_SCLK : out std_logic;
 I2C_SDAT : inout std_logic;
 
-- td_clk27 : in std_logic;
-- td_reset : out std_logic;
-- td_data  : in std_logic_vector(7 downto 0);
-- td_hs    : in std_logic;
-- td_vs    : in std_logic;

 AUD_ADCLRCK : out std_logic;
 AUD_ADCDAT  : in std_logic;
 AUD_DACLRCK : out std_logic;
 AUD_DACDAT  : out std_logic;
 AUD_XCK     : out std_logic;
 AUD_BCLK    : out std_logic;
 
-- enet_data  : inout std_logic_vector(15 downto 0);
-- enet_clk   : out std_logic;
-- enet_cmd   : out std_logic;
-- enet_cs_n  : out std_logic;
-- enet_int   : in std_logic;
-- enet_rd_n  : out std_logic;
-- enet_wr_n  : out std_logic;
-- enet_rst_n : out std_logic;
-- 
-- irda_txd : out std_logic;
-- irda_rxd : in std_logic;
-- 
-- sd_dat  : inout std_logic;
-- sd_dat3 : out std_logic;
-- sd_cmd  : out std_logic;
-- sd_clk  : out std_logic;
-- 
	GPIO_0  : inout std_logic_vector(35 downto 0)
-- gpio_1  : inout std_logic_vector(35 downto 0)
	);
end;

architecture RTL of de2_top is
	signal W_CLK_24M          : std_logic := '0';
	signal W_CLK_18M          : std_logic := '0';
	signal W_CLK_12M          : std_logic := '0';
	signal W_CLK_6M           : std_logic := '0';

	signal P1_CSJUDLR         : std_logic_vector( 6 downto 0) := (others => '0'); -- player 1 controls
	signal P2_CSJUDLR         : std_logic_vector( 6 downto 0) := (others => '0'); -- player 2 controls

	signal W_H_SYNC           : std_logic := '0';
	signal W_V_SYNC           : std_logic := '0';
	signal W_CMPBLK           : std_logic := '0';
	signal W_SDAT_A           : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_SDAT_B           : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_VID              : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_R                : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_G                : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_B                : std_logic_vector( 2 downto 0) := (others => '0');

	signal W_VGA              : std_logic_vector( 7 downto 0) := (others => '0');

	signal ps2_codeready      : std_logic := '1';
	signal ps2_scancode       : std_logic_vector( 9 downto 0) := (others => '0');
	
	signal I_RESET					: std_logic := '0';
begin
	inst_clocks : entity work.CLOCKGEN
	port map(
		CLKIN_IN   => CLOCK_50,
		RST_IN     => I_RESET,
		O_CLK_24M  => W_CLK_24M,
		O_CLK_18M  => W_CLK_18M,
		O_CLK_12M  => W_CLK_12M,
		O_CLK_06M  => W_CLK_6M
	);

	inst_galaxian : entity work.galaxian
	port map(
		W_CLK_18M  => W_CLK_18M,
		W_CLK_12M  => W_CLK_12M,
		W_CLK_6M   => W_CLK_6M,

		P1_CSJUDLR => P1_CSJUDLR,
		P2_CSJUDLR => P2_CSJUDLR,

		I_RESET    => I_RESET,
		W_R        => W_R,
		W_G        => W_G,
		W_B        => W_B,
		W_H_SYNC   => W_H_SYNC,
		W_V_SYNC   => W_V_SYNC,
		W_SDAT_A   => W_SDAT_A,
		W_SDAT_B   => W_SDAT_B,
		O_CMPBL    => W_CMPBLK
	);

	inst_vga : entity work.VGA_SCANCONV
	port map(
		CLK        => W_CLK_6M,
		CLK_X4     => W_CLK_24M,
		--	input
		I_VIDEO    => W_VID,
		I_HSYNC    => W_H_SYNC,
		I_VSYNC    => not W_V_SYNC,
		I_CMPBLK_N => W_CMPBLK,
		--	output
		O_VIDEO    => W_VGA,
		O_HSYNC    => VGA_HS,
		O_VSYNC    => VGA_VS,
		O_CMPBLK_N => open
	);

	W_VID <= W_R & W_G & W_B(2 downto 1);

	VGA_R(9 downto 6) <= W_VGA(7 downto 5) & "0";
	VGA_G(9 downto 6) <= W_VGA(4 downto 2) & "0";
	VGA_B(9 downto 6) <= W_VGA(1 downto 0) & W_VGA( 0) & "0";


	VGA_CLK   <= W_CLK_24M;
	VGA_BLANK <= '1';
	VGA_SYNC  <= '0';
 

--	VGA_R(9 downto 6) <= W_R & "0";
--	VGA_G(9 downto 6) <= W_G & "0";
--	VGA_B(9 downto 6) <= W_B & "0";	
--	VGA_HS <= W_H_SYNC;
--	VGA_VS <= W_V_SYNC;

	GPIO_0(0) <= W_H_SYNC;
	GPIO_0(1) <= W_V_SYNC;

	inst_dacl : entity work.dac
	port map(
		clk_i   => W_CLK_24M,
		res_i   => I_RESET,
		dac_i   => W_SDAT_A,
		dac_o   => OPEN--O_AUDIO_L
	);

	inst_dacr : entity work.dac
	port map(
		clk_i   => W_CLK_24M,
		res_i   => I_RESET,
		dac_i   => W_SDAT_B,
		dac_o   => OPEN--O_AUDIO_R
	);

	-----------------------------------------------------------------------------
	-- Keyboard - active low buttons
	-----------------------------------------------------------------------------
	inst_kbd : entity work.Keyboard
	port map (
		Reset     => I_RESET,
		Clock     => W_CLK_18M,
		PS2Clock  => PS2_CLK,
		PS2Data   => PS2_DAT,
		CodeReady => ps2_codeready,  --: out STD_LOGIC;
		ScanCode  => ps2_scancode    --: out STD_LOGIC_VECTOR(9 downto 0)
	);

-- ScanCode(9)          : 1 = Extended  0 = Regular
-- ScanCode(8)          : 1 = Break     0 = Make
-- ScanCode(7 downto 0) : Key Code
	process(W_CLK_18M)
	begin
		if rising_edge(W_CLK_18M) then
			if I_RESET = '1' then
				P1_CSJUDLR <= (others=>'0');
				P2_CSJUDLR <= (others=>'0');
			elsif (ps2_codeready = '1') then
				case (ps2_scancode(7 downto 0)) is
					when x"05" =>	P1_CSJUDLR(6) <= not ps2_scancode(8);     -- P1 coin "F1"
					when x"04" =>	P2_CSJUDLR(6) <= not ps2_scancode(8);     -- P2 coin "F3"

					when x"06" =>	P1_CSJUDLR(5) <= not ps2_scancode(8);     -- P1 start "F2"
					when x"0c" =>	P2_CSJUDLR(5) <= not ps2_scancode(8);     -- P2 start "F4"

					when x"43" =>	P1_CSJUDLR(4) <= not ps2_scancode(8);     -- P1 jump "I"
										P2_CSJUDLR(4) <= not ps2_scancode(8);     -- P2 jump "I"

					when x"75" =>	P1_CSJUDLR(3) <= not ps2_scancode(8);     -- P1 up arrow
										P2_CSJUDLR(3) <= not ps2_scancode(8);     -- P2 up arrow

					when x"72" =>	P1_CSJUDLR(2) <= not ps2_scancode(8);     -- P1 down arrow
										P2_CSJUDLR(2) <= not ps2_scancode(8);     -- P2 down arrow

					when x"6b" =>	P1_CSJUDLR(1) <= not ps2_scancode(8);     -- P1 left arrow
										P2_CSJUDLR(1) <= not ps2_scancode(8);     -- P2 left arrow

					when x"74" =>	P1_CSJUDLR(0) <= not ps2_scancode(8);     -- P1 right arrow
										P2_CSJUDLR(0) <= not ps2_scancode(8);     -- P2 right arrow

					when others => null;
				end case;
			end if;
		end if;
	end process;

end RTL;
