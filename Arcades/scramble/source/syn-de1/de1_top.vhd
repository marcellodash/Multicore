--
-- TBBlue / ZX Spectrum Next project
-- Copyright (c) 2015 - Fabio Belavenuto & Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
--
-- Terasic DE1 top-level
--

-- altera message_off 10540 10541

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

-- Generic top-level entity for Altera DE1 board
entity de1_top is
	generic (
		usar_sdram		: boolean	:= false
	);
	port (
		-- Clocks
		CLOCK_24       : in    std_logic_vector(1 downto 0);
		CLOCK_27       : in    std_logic_vector(1 downto 0);
		CLOCK_50       : in    std_logic;
		EXT_CLOCK      : in    std_logic;

		-- Switches
		SW             : in    std_logic_vector(9 downto 0);
		-- Buttons
		KEY            : in    std_logic_vector(3 downto 0);

		-- 7 segment displays
		HEX0           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX1           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX2           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX3           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		-- Red LEDs
		LEDR           : out   std_logic_vector(9 downto 0)		:= (others => '0');
		-- Green LEDs
		LEDG           : out   std_logic_vector(7 downto 0)		:= (others => '0');

		-- VGA
		VGA_R          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_G          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_B          : out   std_logic_vector(3 downto 0)		:= (others => '0');
		VGA_HS         : out   std_logic									:= '1';
		VGA_VS         : out   std_logic									:= '1';

		-- Serial
		UART_RXD       : in    std_logic;
		UART_TXD       : out   std_logic									:= '1';

		-- PS/2 Keyboard
		PS2_CLK        : inout std_logic									:= '1';
		PS2_DAT        : inout std_logic									:= '1';

		-- I2C
		I2C_SCLK       : inout std_logic									:= '1';
		I2C_SDAT       : inout std_logic									:= '1';

		-- Audio
		AUD_XCK        : out   std_logic									:= '0';
		AUD_BCLK       : out   std_logic									:= '0';
		AUD_ADCLRCK    : out   std_logic									:= '0';
		AUD_ADCDAT     : in    std_logic;
		AUD_DACLRCK    : out   std_logic									:= '0';
		AUD_DACDAT     : out   std_logic									:= '0';

		-- SRAM
		SRAM_ADDR      : out   std_logic_vector(17 downto 0)		:= (others => '0');
		SRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => 'Z');
		SRAM_CE_N      : out   std_logic									:= '1';
		SRAM_OE_N      : out   std_logic									:= '1';
		SRAM_WE_N      : out   std_logic									:= '1';
		SRAM_UB_N      : out   std_logic									:= '1';
		SRAM_LB_N      : out   std_logic									:= '1';

		-- SDRAM
		DRAM_ADDR      : out   std_logic_vector(11 downto 0)		:= (others => '0');
		DRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => 'Z');
		DRAM_BA_0      : out   std_logic									:= '1';
		DRAM_BA_1      : out   std_logic									:= '1';
		DRAM_CAS_N     : out   std_logic									:= '1';
		DRAM_CKE       : out   std_logic									:= '0';
		DRAM_CLK       : out   std_logic									:= '1';
		DRAM_CS_N      : out   std_logic									:= '1';
		DRAM_LDQM      : out   std_logic									:= '1';
		DRAM_RAS_N     : out   std_logic									:= '1';
		DRAM_UDQM      : out   std_logic									:= '1';
		DRAM_WE_N      : out   std_logic									:= '1';

		-- Flash
		FL_ADDR        : out   std_logic_vector(21 downto 0)		:= (others => '0');
		FL_DQ          : inout std_logic_vector(7 downto 0)		:= (others => '0');
		FL_RST_N       : out   std_logic									:= '1';
		FL_OE_N        : out   std_logic									:= '1';
		FL_WE_N        : out   std_logic									:= '1';
		FL_CE_N        : out   std_logic									:= '1';

		-- SD card (SPI mode)
		SD_nCS         : out   std_logic									:= '1';
		SD_MOSI        : out   std_logic									:= '1';
		SD_SCLK        : out   std_logic									:= '1';
		SD_MISO        : in    std_logic;

		-- GPIO
		GPIO_0         : inout std_logic_vector(35 downto 0)		:= (others => 'Z');
		GPIO_1         : inout std_logic_vector(35 downto 0)		:= (others => 'Z')
	);
end entity;

architecture Behavior of de1_top is

	-- ASMI (Altera specific component)
	component cyclone_asmiblock
	port (
		dclkin      : in    std_logic;      -- DCLK
		scein       : in    std_logic;      -- nCSO
		sdoin       : in    std_logic;      -- ASDO
		oe          : in    std_logic;      --(1=disable(Hi-Z))
		data0out    : out   std_logic       -- DATA0
	);
	end component;

	-- Master clock
	signal clock_master		: std_logic;
	signal clock_sdram		: std_logic;
--	signal clock_video		: std_logic;
	signal pll_locked			: std_logic;
	signal memory_clock			: std_logic;
	signal vid_clk: std_logic := '0';
	signal sysclk : std_logic := '0';
	signal atari_clk: std_logic;

	-- Resets
	signal poweron_s			: std_logic;
	signal hard_reset_s		: std_logic;
	signal soft_reset_s		: std_logic;
	signal reset_s				: std_logic;

	-- Memory buses
	signal vram_a				: std_logic_vector(18 downto 0);
	signal vram_dout			: std_logic_vector(7 downto 0);
	signal vram_cs				: std_logic;
	signal vram_oe				: std_logic;
	
	signal ram_a				: std_logic_vector(18 downto 0);		-- 512K
	signal ram_din				: std_logic_vector(7 downto 0);
	signal ram_dout			: std_logic_vector(7 downto 0);
	signal ram_cs				: std_logic;
	signal ram_oe				: std_logic;
	signal ram_we				: std_logic;
	signal rom_a				: std_logic_vector(13 downto 0);		-- 16K
	signal rom_dout			: std_logic_vector(7 downto 0);

	-- Audio
	signal s_ear				: std_logic;
	signal s_spk				: std_logic;
	signal s_mic				: std_logic;
	signal s_psg				: unsigned(9 downto 0);
	signal s_fm					: signed(12 downto 0);

	-- Keyboard
	signal kb_rows				: std_logic_vector(7 downto 0);
	signal kb_columns			: std_logic_vector(4 downto 0);
	signal FKeys_s				: std_logic_vector(12 downto 1);

	-- SPI and EPCS
	signal spi_mosi_s			: std_logic;
	signal spi_sclk_s			: std_logic;
	signal flash_miso_s		: std_logic;
	signal flash_cs_n_s		: std_logic;

	-- Video and scandoubler
	signal rgb_r				: std_logic;
	signal rgb_g				: std_logic;
	signal rgb_b				: std_logic;
	signal rgb_i				: std_logic;
	signal rgb_hs_n			: std_logic;
	signal rgb_vs_n			: std_logic;
--	signal rgb_cs_n			: std_logic;
--	signal rgb_hb_n			: std_logic;
--	signal rgb_vb_n			: std_logic;
	signal ulap_en				: std_logic;
	signal rgb_ulap			: std_logic_vector(7 downto 0);
	signal rgb_comb			: std_logic_vector(7 downto 0);
	signal rgb_out				: std_logic_vector(7 downto 0);
	signal scandbl_en			: std_logic;
	signal hsync_out			: std_logic;
	signal vsync_out			: std_logic;
	signal s_scanlines		: std_logic := '0';

	-- Joystick (Minimig standard)
	signal s_joy0				: std_logic_vector(5 downto 0);
	signal s_joy1				: std_logic_vector(5 downto 0);
	alias J0_UP					: std_logic						is GPIO_0(17);
	alias J0_DOWN				: std_logic						is GPIO_0(18);
	alias J0_LEFT				: std_logic						is GPIO_0(19);
	alias J0_RIGHT				: std_logic						is GPIO_0(4);
	alias J0_BTN				: std_logic						is GPIO_0(5);
	alias J0_BTN2				: std_logic						is GPIO_0(6);
	alias J0_MMB				: std_logic						is GPIO_0(7);
	alias J1_UP					: std_logic						is GPIO_0(8);
	alias J1_DOWN				: std_logic						is GPIO_0(9);
	alias J1_LEFT				: std_logic						is GPIO_0(10);
	alias J1_RIGHT				: std_logic						is GPIO_0(11);
	alias J1_BTN				: std_logic						is GPIO_0(12);
	alias J1_BTN2				: std_logic						is GPIO_0(13);
	alias J1_MMB				: std_logic						is GPIO_0(14);

	-- Mouse
	signal mouse_x				: std_logic_vector(7 downto 0);
	signal mouse_y				: std_logic_vector(7 downto 0);
	signal mouse_bts			: std_logic_vector(2 downto 0);
	signal mouse_wheel		: std_logic_vector(3 downto 0);
	alias  MOUSE_PS2_CLK		: std_logic						is GPIO_0(15);
	alias  MOUSE_PS2_DAT		: std_logic						is GPIO_0(16);

	-- Debug
	signal D_display	: std_logic_vector(15 downto 0);
--	signal D_cpu_a		: std_logic_vector(15 downto 0);
	signal s_cpu_a				: std_logic_vector(15 downto 0);
	signal s_cpu_d				: std_logic_vector(7 downto 0);
	signal s_cpu_iorq			: std_logic;
	signal s_cpu_mreq			: std_logic;
	signal s_cpu_rd			: std_logic;
	signal s_cpu_wr			: std_logic;
	signal s_cpu_m1			: std_logic;
--	signal s_ear_key			: std_logic;
--	signal contador			: unsigned(7 downto 0)		:= (others=>'0');
--	signal D_keyb_valid		: std_logic;
--	signal D_keyb_data		: std_logic_vector(7 downto 0);

	
	--HDMI
	
	signal counter				: unsigned(3 downto 0) := "0000";				-- Contador para dividir o clock master
	
	signal VideoR		: std_logic_vector(1 downto 0);
	signal VideoG		: std_logic_vector(1 downto 0);
	signal VideoB		: std_logic_vector(1 downto 0);
	signal Hsync		: std_logic;
	signal Vsync		: std_logic;
	signal Sblank		: std_logic;
	signal clk7			: std_logic;
	signal clk14		: std_logic;
		
	alias VR2			: std_logic						is GPIO_0(22);	
	alias VR1			: std_logic						is GPIO_0(24);	
	alias VR0			: std_logic						is GPIO_0(26);	

	alias VG2			: std_logic						is GPIO_0(28);	
	alias VG1			: std_logic						is GPIO_0(30);	
	alias VG0			: std_logic						is GPIO_0(32);	
	
	alias VB1			: std_logic						is GPIO_0(34);	
	alias VB0			: std_logic						is GPIO_0(35);

	alias VHS			: std_logic						is GPIO_0(23);
	alias VVS			: std_logic						is GPIO_0(25);
	
	alias VSND			: std_logic						is GPIO_0(27);
	alias VBLANK		: std_logic						is GPIO_0(29);
	alias VCLK			: std_logic						is GPIO_0(31);
	alias VCLK140		: std_logic						is GPIO_0(33);
	
	-- RTC
	alias RTC_SCL		: std_logic						is GPIO_0(0); -- CLOCK
	alias RTC_SDA		: std_logic						is GPIO_0(1); -- DADOS
	
	alias RTC_SCL_bkp		: std_logic						is GPIO_0(2); -- CLOCK
	alias RTC_SDA_bkp		: std_logic						is GPIO_0(3); -- DADOS
	
	signal scl : std_logic;
	signal sda : std_logic;
	
	signal s_others : std_logic_vector (7 downto 0);
	
	-- A2601
	signal audio: std_logic := '0';
   signal O_VSYNC: std_logic := '0';
   signal O_HSYNC: std_logic := '0';
	signal O_VIDEO_R: std_logic_vector(2 downto 0) := (others => '0');
	signal O_VIDEO_G: std_logic_vector(2 downto 0) := (others => '0');
	signal O_VIDEO_B: std_logic_vector(2 downto 0) := (others => '0');			
	signal res: std_logic := '0';
	signal p_l: std_logic := '0';
	signal p_r: std_logic := '0';
	signal p_a: std_logic := '0';
	signal p_u: std_logic := '0';
	signal p_d: std_logic := '0';
	signal p2_l: std_logic := '0';
	signal p2_r: std_logic := '0';
	signal p2_a: std_logic := '0';
	signal p2_u: std_logic := '0';
	signal p2_d: std_logic := '0';
	signal p_s: std_logic := '0';
	signal p_bs: std_logic;
	signal LED: std_logic_vector(2 downto 0);
	signal I_SW : std_logic_vector(2 downto 0) := (others => '0');
	
	signal cart_a: std_logic_vector(11 downto 0);
	signal cart_d : std_logic_vector(7 downto 0);
	
	-- ram
	signal loader_ram_a				: std_logic_vector(18 downto 0);		-- 512K
	signal loader_ram_din			: std_logic_vector(7 downto 0);
	signal loader_ram_dout			: std_logic_vector(7 downto 0);
	signal loader_ram_cs				: std_logic;
	signal loader_ram_oe				: std_logic;
	signal loader_ram_we				: std_logic;

	signal a2601_ram_a				: std_logic_vector(11 downto 0);	
	signal a2601_ram_dout			: std_logic_vector(7 downto 0);
	
	
begin



	scramble : entity work.SCRAMBLE_TOP 
  port map (
  
    O_STRATAFLASH_ADDR    => open,
    B_STRATAFLASH_DATA    => open,
    O_STRATAFLASH_CE_L    => open,
    O_STRATAFLASH_OE_L    => open,
    O_STRATAFLASH_WE_L    => open,
    O_STRATAFLASH_BYTE    => open,
	 
    -- disable other onboard devices
    O_LCD_RW              => open,
    O_LCD_E               => open,
    O_SPI_ROM_CS          => open,
    O_SPI_ADC_CONV        => open,
    O_SPI_DAC_CS          => open,
    O_PLATFORMFLASH_OE    => open,
    --
    O_VIDEO_R             => VGA_R,
    O_VIDEO_G              => VGA_G,
    O_VIDEO_B              => VGA_B,
    O_HSYNC                => VGA_HS,
    O_VSYNC                => VGA_VS,
    --
    O_AUDIO_L              => open,
    O_AUDIO_R             => open,
    --
    I_SW                 => SW (3 downto 0), -- active high
    I_BUTTON             => SW(7 downto 4),
    O_LED                  => open,
    --
    I_RESET               => not key(0),
    I_CLK_REF             => CLOCK_50

    );

end architecture;
