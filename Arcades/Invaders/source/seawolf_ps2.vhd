-- Sea Wolf top level for
-- external ram and ps/2 keyboard interface
-- No sound
--
-- Version : 0242
--
-- Copyright (c) 2002 Daniel Wallner (jesus@opencores.org)
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- Please report bugs to the author, but before you do so, please
-- make sure that this is not a derivative work and that
-- you have the latest version of this file.
--
-- The latest version of this file can be found at:
--	http://www.fpgaarcade.com
--
-- Limitations :
--
-- File history :
--
--	0242 : First release
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity seawolf_ps2 is
	port(
		Rst_n	: in std_logic;
		Clk		: in std_logic;
		PS2_Clk	: in std_logic;
		PS2_Data: in std_logic;
		OE_n	: out std_logic;
		WE_n	: out std_logic;
		RAMCS_n	: out std_logic;
		ROMCS_n	: out std_logic;
		PGM_n	: out std_logic;
		A		: out std_logic_vector(16 downto 0);
		D		: inout std_logic_vector(7 downto 0);
		CVBS	: out std_logic;
		Video_o : out std_logic;
		HSync           : out std_logic;
		VSync           : out std_logic
		);
end seawolf_ps2;

architecture rtl of seawolf_ps2 is


	component swrom
	port(
		Clk	: in std_logic;
		A	: in std_logic_vector(11 downto 0);
		D	: out std_logic_vector(7 downto 0));
	end component;

	component ps2kbd
	port(
		Rst_n		: in std_logic;
		Clk			: in std_logic;
		Tick1us		: in std_logic;
		PS2_Clk		: in std_logic;
		PS2_Data	: in std_logic;
		Press		: out std_logic;
		Release		: out std_logic;
		Reset		: out std_logic;
		ScanCode	: out std_logic_vector(7 downto 0));
	end component;

	signal	Rst_n_s			: std_logic;

	signal	DIP				: std_logic_vector(8 downto 1);

	signal	RWE_n			: std_logic;
	signal	WE_n_r0			: std_logic;
	signal	WE_n_r			: std_logic;
	signal	CSync			: std_logic;
	signal	Video			: std_logic;

	signal	AD				: std_logic_vector(15 downto 0);
	signal	RAB				: std_logic_vector(12 downto 0);
	signal	RWD				: std_logic_vector(7 downto 0);
	signal	DR				: std_logic_vector(7 downto 0);
	signal	IB				: std_logic_vector(7 downto 0);

	signal	Buttons			: std_logic_vector(5 downto 0);
	signal	Commutator		: std_logic_vector(5 downto 1);

	signal	Tick1us			: std_logic;

	signal	PS2_Sample		: std_logic;
	signal	PS2_Data_s		: std_logic;

	signal	ScanCode	: std_logic_vector(7 downto 0);
	signal	Press		: std_logic;
	signal	Release		: std_logic;
	signal	Reset		: std_logic;

begin

	DIP <= "00000000";
	RAMCS_n	<= '0';
	ROMCS_n	<= '1';
	PGM_n <= '1';
	WE_n <= RWE_n or WE_n_r;
	OE_n <= not (RWE_n and WE_n_r);
	A(16 downto 13) <= "0000";
	D <= DR when WE_n_r0 = '0' else (others => 'Z');

	Commutator <= Buttons(5) & "0" & (Buttons(4) and Buttons(5)) & "11";

	CVBS <= '0' when CSync = '1'
		else 'Z' when Video = '0'
		else '1';

	Video_o <= Video;
		
	core : work.seawolf
		port map(
			Rst_n => Rst_n,
			Clk => Clk,
			Coin => Buttons(0),
			Start => Buttons(1),
			Trig => Buttons(2),
			Erase => Buttons(3),
			Commutator => Commutator,
			DIP => DIP,
			RDB => D,
			IB => IB,
			RWD => RWD,
			RAB => RAB,
			AD => AD,
			Explosion => open,
			Periscope => open,
			SoundCtrl => open,
			Rst_n_s => Rst_n_s,
			RWE_n => RWE_n,
			CSync => CSync,
			Video => Video,
			HSync => HSync,
			VSync => VSync
			);

	u_ROM: swrom
		port map(
			Clk => Clk,
			A => AD(11 downto 0),
			D => IB);

	-- Glue
	process (Rst_n_s, Clk)
		variable cnt : unsigned(3 downto 0);
	begin
		if Rst_n_s = '0' then
			cnt := "0000";
			Tick1us <= '0';
			DR <= (others => '0');
			A(12 downto 0) <= (others => '0');
			WE_n_r <= '1';
			WE_n_r0 <= '1';
		elsif Clk'event and Clk = '1' then
			DR <= RWD;
			A(12 downto 0) <= RAB;
			WE_n_r <= WE_n_r0;
			WE_n_r0 <= RWE_n;
			Tick1us <= '0';
			if cnt = 9 then
				Tick1us <= '1';
				cnt := "0000";
			else
				cnt := cnt + 1;
			end if;
		end if;
	end process;

	-- Keyboard decoder

	kbd : ps2kbd
		port map(
			Rst_n => Rst_n_s,
			Clk => Clk,
			Tick1us => Tick1us,
			PS2_Clk => PS2_Clk,
			PS2_Data => PS2_Data,
			Press => Press,
			Release => Release,
			Reset => Reset,
			ScanCode => ScanCode);

	process (Clk, Rst_n_s)
	begin
		if Rst_n_s = '0' then
			Buttons <= (others => '0');
		elsif Clk'event and Clk = '1' then
			if (Press or Release) = '1' then
				if ScanCode = x"21" then	-- c
					Buttons(0) <= Release;
				end if;
				if ScanCode = x"16" or ScanCode = x"69" then	-- 1
					Buttons(1) <= Release;
				end if;
				if ScanCode = x"29" then	-- Space
					Buttons(2) <= Release;
				end if;
				if ScanCode = x"12" then	-- e
					Buttons(3) <= Release;
				end if;
				if ScanCode = x"6b" then	-- Left
					Buttons(4) <= Release;
				end if;
				if ScanCode = x"74" then	-- Right
					Buttons(5) <= Release;
				end if;
			end if;
			if Reset = '1' then
				Buttons <= (others => '0');
			end if;
		end if;
	end process;

end;
