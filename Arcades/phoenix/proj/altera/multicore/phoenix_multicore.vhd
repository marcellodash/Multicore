

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.ALL;
use ieee.numeric_std.all;

entity phoenix_multicore is
generic
(
  C_hdmi_generic_serializer: boolean := true; -- serializer type: false: vendor-specific, true: generic=vendor-agnostic
  C_hdmi_audio: boolean := true; -- HDMI generator type: false: video only, true: video+audio capable
  PLEIADS : boolean := false
);
port
(
  -- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);
		btn_oe_n_i			: in    std_logic;
		btn_clr_n_i			: in    std_logic;

		-- SRAM (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_ce_n_o			: out   std_logic_vector(1 downto 0)	:= (others => '1');
		sram_oe_n_o			: out   std_logic								:= '1';

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joystick
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p7_o			: out   std_logic								:= '1';
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p7_o			: out   std_logic								:= '1';
		joy2_p9_i			: in    std_logic;

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
--		hdmi_d_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
--		hdmi_clk_o			: out   std_logic								:= '0';
--		hdmi_cec_o			: out   std_logic								:= '0';

		-- Debug
		leds_n_o				: out   std_logic_vector(7 downto 0)	:= (others => '1')
	
);
end;

architecture struct of phoenix_multicore is
  signal clk_pixel, clk_pixel_shift: std_logic;

  signal S_audio: std_logic_vector(11 downto 0);
  signal sound_string : std_logic_vector(31 downto 0);

  signal S_vga_r, S_vga_g, S_vga_b: std_logic_vector(1 downto 0);
  signal S_vga_r8, S_vga_g8, S_vga_b8: std_logic_vector(7 downto 0);
  signal S_vga_vsync, S_vga_hsync: std_logic;
  signal S_vga_vblank, S_vga_blank: std_logic;

  signal dvid_red, dvid_green, dvid_blue, dvid_clock: std_logic_vector(1 downto 0);
  signal S_hdmi_pd0, S_hdmi_pd1, S_hdmi_pd2: std_logic_vector(9 downto 0);
  signal tmds_d: std_logic_vector(3 downto 0);
  signal tx_in: std_logic_vector(29 downto 0);
  signal hdmi_dp, hdmi_dn: std_logic_vector(2 downto 0);
  signal hdmi_clkp, hdmi_clkn: std_logic;

  signal kbd_intr      : std_logic;
  signal kbd_scancode  : std_logic_vector(7 downto 0);
  signal JoyPCFRLDU    : std_logic_vector(7 downto 0);

  signal coin         : std_logic;
  signal player_start : std_logic_vector(1 downto 0);
  signal button_left, button_right, button_protect, button_fire: std_logic;

  signal reset        : std_logic;
  signal clock_stable : std_logic;
  signal dip_switch   : std_logic_vector(7 downto 0) := (others => '0');
  -- alias  audio_select : std_logic_vector(2 downto 0) is sw(10 downto 8);
  
  signal dac_s         : std_logic;
	
begin
  G_sdr: if C_hdmi_generic_serializer or not C_hdmi_audio generate
  clkgen_sdr: entity work.clk_50_250_25MHz
  port map(
      inclk0 => clock_50_i, c0 => clk_pixel_shift, c1 => clk_pixel,
      locked => clock_stable
  );
  --clk_pixel_shift <= clock_50;
  --clk_pixel <= clock_50;
  end generate;

  reset <= not clock_stable or not btn_n_i(4);
 -- dip_switch(3 downto 0) <= sw(4 downto 1);

  -- get scancode from keyboard
  keybord : entity work.io_ps2_keyboard
  port map (
    clk       => clk_pixel,
    kbd_clk   => ps2_clk_io,
    kbd_dat   => ps2_data_io,
    interrupt => kbd_intr,
    scancode  => kbd_scancode
  );

  -- translate scancode to joystick
  Joystick : entity work.kbd_joystick
  port map (
    clk         => clk_pixel,
    kbdint      => kbd_intr,
    kbdscancode => std_logic_vector(kbd_scancode),
    JoyPCFRLDU  => JoyPCFRLDU
  );

  -- joystick to inputs
  coin            <= not JoyPCFRLDU(7); -- F3 : Add coin
  player_start(1) <= not JoyPCFRLDU(6); -- F2 : Start 2 Players
  player_start(0) <= not JoyPCFRLDU(5); -- F1 : Start 1 Player
  button_fire     <= not JoyPCFRLDU(4); -- SPACE : Fire
  button_right    <= not JoyPCFRLDU(3); -- RIGHT arrow : Right
  button_left     <= not JoyPCFRLDU(2); -- LEFT arrow  : Left
  button_protect  <= not JoyPCFRLDU(0); -- UP arrow : Protection

  phoenix : entity work.phoenix
  generic map
  (
    C_audio => true,
    C_vga => true
  )
  port map
  (
    clk_pixel    => clk_pixel,
    reset        => reset,
    dip_switch   => dip_switch,
    btn_coin     => coin and (not btn_n_i(3)),
    btn_player_start(0) => player_start(0) and (not btn_n_i(1)),
    btn_player_start(1) => player_start(1) and (not btn_n_i(2)),
    btn_left     => button_left and not joy1_left_i,
    btn_right    => button_right and not joy1_right_i,
    btn_barrier  => button_protect and (not joy1_down_i),
    btn_fire     => button_fire and (not joy1_p6_i),
    vga_r        => S_vga_r,
    vga_g        => S_vga_g,
    vga_b        => S_vga_b,
    vga_hsync    => S_vga_hsync,
    vga_vsync    => S_vga_vsync,
    vga_blank    => S_vga_blank,
    -- audio_select => audio_select,
    audio        => S_audio
  );

  
 vga_r_o(2 downto 0) <= S_vga_r & '0' when S_vga_blank='0' else (others=>'0'); 
 vga_g_o(2 downto 0) <= S_vga_g & '0' when S_vga_blank='0' else (others=>'0'); 
 vga_b_o(1 downto 0) <= S_vga_b when S_vga_blank='0' else (others=>'0'); 
 vga_hsync_n_o <= not S_vga_hsync;
 vga_vsync_n_o <= not S_vga_vsync;


  -- some debugging with LEDs
  leds_n_o(0) <= not S_vga_blank;
  leds_n_o(5) <= S_vga_r(1); -- when game works, changing color on
  leds_n_o(6) <= S_vga_g(1); -- large area of the screen should
  leds_n_o(7) <= S_vga_b(1); -- also be "visible" on RGB indicator LEDs
  
  G_hdmi_video_only: if not C_hdmi_audio generate
  vga2dvi_converter: entity work.vga2dvid
  generic map
  (
    C_ddr     => false,
    C_depth   => 2 -- 2bpp (2 bit per pixel)
  )
  port map
  (
    clk_pixel => clk_pixel, -- 25 MHz
    clk_shift => clk_pixel_shift, -- 250 MHz

    in_red   => S_vga_r,
    in_green => S_vga_g,
    in_blue  => S_vga_b,

    in_blank => S_vga_blank,
    in_hsync => not S_vga_hsync,
    in_vsync => not S_vga_vsync,

    -- single-ended output ready for differential buffers
    out_red   => dvid_red,
    out_green => dvid_green,
    out_blue  => dvid_blue,
    out_clock => dvid_clock
  );
  -- true differential pins defined in constraints
  --hdmi_d <= dvid_red(0) & dvid_green(0) & dvid_blue(0);
  --hdmi_clk <= dvid_clock(0);

  -- GPIO "differential" output buffering for HDMI
  hdmi_output: entity work.hdmi_out
  port map
  (
    tmds_in_rgb    => dvid_red(0) & dvid_green(0) & dvid_blue(0),
    tmds_out_rgb_p => hdmi_dp,   -- D2+ red  D1+ green  D0+ blue
    tmds_out_rgb_n => hdmi_dn,   -- D2- red  D1- green  D0+ blue
    tmds_in_clk    => dvid_clock(0),
    tmds_out_clk_p => hdmi_clkp, -- CLK+ clock
    tmds_out_clk_n => hdmi_clkn  -- CLK- clock
  );
  end generate;

  G_hdmi_video_audio: if C_hdmi_audio generate
    S_vga_r8 <= S_vga_r & S_vga_r(0) & S_vga_r(0) & S_vga_r(0) & S_vga_r(0) & S_vga_r(0) & S_vga_r(0);
    S_vga_g8 <= S_vga_g & S_vga_g(0) & S_vga_g(0) & S_vga_g(0) & S_vga_g(0) & S_vga_g(0) & S_vga_g(0);
    S_vga_b8 <= S_vga_b & S_vga_b(0) & S_vga_b(0) & S_vga_b(0) & S_vga_b(0) & S_vga_b(0) & S_vga_b(0);
    av_hdmi_out: entity work.av_hdmi
    generic map
    (
      FREQ => 25000000,
      FS => 48000,
      CTS => 25000,
      N => 6144
    )
    port map
    (
      I_CLK_PIXEL    => clk_pixel,
      I_R            => S_vga_r8,
      I_G            => S_vga_g8,
      I_B            => S_vga_b8,
      I_BLANK        => S_vga_blank,
      I_HSYNC        => not S_vga_hsync,
      I_VSYNC        => not S_vga_vsync,
      I_AUDIO_ENABLE => '1',--sw(0),
      I_AUDIO_PCM_L  => S_audio & "0000",
      I_AUDIO_PCM_R  => S_audio & "0000",
      O_TMDS_PD0     => S_HDMI_PD0,
      O_TMDS_PD1     => S_HDMI_PD1,
      O_TMDS_PD2     => S_HDMI_PD2
    );

    -- tx_in <= S_HDMI_PD2 & S_HDMI_PD1 & S_HDMI_PD0; -- this would be normal bit order, but
    -- generic serializer follows vendor specific serializer style
    tx_in <=  S_HDMI_PD2(0) & S_HDMI_PD2(1) & S_HDMI_PD2(2) & S_HDMI_PD2(3) & S_HDMI_PD2(4) & S_HDMI_PD2(5) & S_HDMI_PD2(6) & S_HDMI_PD2(7) & S_HDMI_PD2(8) & S_HDMI_PD2(9) &
              S_HDMI_PD1(0) & S_HDMI_PD1(1) & S_HDMI_PD1(2) & S_HDMI_PD1(3) & S_HDMI_PD1(4) & S_HDMI_PD1(5) & S_HDMI_PD1(6) & S_HDMI_PD1(7) & S_HDMI_PD1(8) & S_HDMI_PD1(9) &
              S_HDMI_PD0(0) & S_HDMI_PD0(1) & S_HDMI_PD0(2) & S_HDMI_PD0(3) & S_HDMI_PD0(4) & S_HDMI_PD0(5) & S_HDMI_PD0(6) & S_HDMI_PD0(7) & S_HDMI_PD0(8) & S_HDMI_PD0(9);

    generic_serializer_inst: entity work.serializer_generic
    PORT MAP
    (
        tx_in => tx_in,
        tx_inclock => CLK_PIXEL_SHIFT, -- NOTE: generic serializer needs CLK_PIXEL x10
        tx_syncclock => CLK_PIXEL,
        tx_out => tmds_d
    );
    -- GPIO "differential" output buffering for HDMI
    hdmi_output: entity work.hdmi_out
    port map
    (
      tmds_in_rgb    => tmds_d(2 downto 0),
      tmds_out_rgb_p => hdmi_dp,   -- D2+ red  D1+ green  D0+ blue
      tmds_out_rgb_n => hdmi_dn,   -- D2- red  D1- green  D0+ blue
      tmds_in_clk    => tmds_d(3),
      tmds_out_clk_p => hdmi_clkp, -- CLK+ clock
      tmds_out_clk_n => hdmi_clkn  -- CLK- clock
    );
  end generate;

-- gpio_0(16) <= hdmi_clkp;
-- gpio_0(17) <= hdmi_clkn;
-- gpio_0(18) <= hdmi_dp(0);
-- gpio_0(19) <= hdmi_dn(0);
-- gpio_0(20) <= hdmi_dp(1);
-- gpio_0(21) <= hdmi_dn(1);
-- gpio_0(22) <= hdmi_dp(2);
-- gpio_0(23) <= hdmi_dn(2);

--		vga_hsync_n_o	<= hdmi_dp(2);	-- 2+		10
--		vga_vsync_n_o	<= hdmi_dn(2);	-- 2-		11
--		vga_b_o(2)		<= hdmi_dp(1);	-- 1+		144	
--		vga_b_o(1)		<= hdmi_dn(1);	-- 1-		143
--		vga_r_o(0)		<= hdmi_dp(0);	-- 0+		133
--		vga_g_o(2)		<= hdmi_dn(0);	-- 0-		132
--		vga_r_o(1)		<= hdmi_clkp;	-- CLK+	113
--		vga_r_o(2)		<= hdmi_clkn;	-- CLK-	112

  sound_string <= "0000" & S_audio & "0000" & S_audio;
  
  dac : work.dac
  port map
  (
		clk_i   => clk_pixel,
		res_n_i => not reset,
		dac_i   => S_audio(11 downto 5),
		dac_o   => dac_s
	);
	
	dac_l_o <= dac_s;
	dac_r_o <= dac_s;

  


end struct;
