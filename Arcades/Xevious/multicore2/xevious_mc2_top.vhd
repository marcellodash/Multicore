---------------------------------------------------------------------------------
-- Multicore 2 Top level for Xevious 
-- Victor Trucco 2018
-- Based on DE2 Top by Dar (darfpga@aol.fr) (December 2016)
-- http://darfpga.blogspot.fr
---------------------------------------------------------------------------------
-- Educational use only
-- Do not redistribute synthetized file with roms
-- Do not redistribute roms whatever the form
-- Use at your own risk
---------------------------------------------------------------------------------
--
-- Main features :
--  PS2 keyboard input
--  68Ko external SRAM (send to SRAM by the STM32
--
-- Uses 1 pll for 18MHz and 11MHz generation from 50MHz
--
-- Board key :
--      0 : reset
--
-- Keyboard inputs :
--   F3 : Add coin
--   F2 : Start 2 players
--   F1 : Start 1 player
--   SPACE       : Fire  
--   RIGHT arrow : Move right 
--   LEFT  arrow : Move left
--   UP    arrow : Move up 
--   DOWN  arrow : Move down
--   CTRL        : Launch bomb        
--
-- Dip switch and other details : see xevious.vhd

---------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.ALL;
use ieee.numeric_std.all;

entity xevious_mc2 is
port(
 -- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= 'Z';
		sd_sclk_o			: out   std_logic								:= 'Z';
		sd_mosi_o			: out   std_logic								:= 'Z';
		sd_miso_i			: in    std_logic								:= 'Z';

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= 'Z'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
);
end xevious_mc2;

architecture struct of xevious_mc2 is

	function to_slv(s: string) return std_logic_vector is 
        constant ss: string(1 to s'length) := s; 
        variable answer: std_logic_vector(1 to 8 * s'length); 
        variable p: integer; 
        variable c: integer; 
    begin 
        for i in ss'range loop
            p := 8 * i;
            c := character'pos(ss(i));
            answer(p - 7 to p) := std_logic_vector(to_unsigned(c,8)); 
        end loop; 
        return answer; 
    end function; 
	 
 signal clock_18  : std_logic;
 signal clock_11  : std_logic;
 signal clk  : std_logic := '0';

 signal r         : std_logic_vector(3 downto 0);
 signal g         : std_logic_vector(3 downto 0);
 signal b         : std_logic_vector(3 downto 0);
 signal video_clk : std_logic;
 signal csync     : std_logic;
 signal blankn    : std_logic;
 
 signal audio        : std_logic_vector(10 downto 0);
 signal pwm_accumulator : std_logic_vector(12 downto 0);
 signal sound_string : std_logic_vector(31 downto 0);
 signal reset        : std_logic;
 
 signal  reset_n      : std_logic;
 
 signal kbd_intr      : std_logic;
 signal kbd_scancode  : std_logic_vector(7 downto 0);
 signal joyBCPPFRLDU   : std_logic_vector(8 downto 0);
 
 -- SRAM
	signal cpu_addr_s : std_logic_vector(16 downto 0);
	signal rom_data_s : std_logic_vector(7 downto 0);
	signal sram_addr_s : std_logic_vector(18 downto 0);	
	signal sram_data_s : std_logic_vector(7 downto 0);
	signal sram_we_n_s : std_logic := '1';
 
 -- Video
	signal video_r_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal video_g_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal video_b_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal video_hsync_n_s		: std_logic								:= '1';
	signal video_vsync_n_s		: std_logic								:= '1';
	signal vga_hsync_n_s		: std_logic								:= '1';
	signal vga_vsync_n_s		: std_logic								:= '1';
	
	
	 -- OSD
	 signal pump_active_s 	 : std_logic := '0';
	 signal osd_s  		 : std_logic_vector(7 downto 0) := "00111111";
	 signal clock_div_q	: unsigned(7 downto 0) 				:= (others => '0');
	 signal keys_s			: std_logic_vector( 7 downto 0) := (others => '1');	
	 signal power_on_reset     : std_logic := '0';
	 
	 -- debounces
	 signal btn_coin_s 	 	: std_logic;
	 signal btn_start1_s 	 : std_logic;
 	 signal btn_start2_s 	 : std_logic;
	 
	 -- joysticks
	 signal joy1_s : std_logic_vector(11 downto 0);
	 signal joy2_s : std_logic_vector(11 downto 0);
	 signal joyP7_s : std_logic;
	 
	 -- HDMI
	signal clk_vga : std_logic;
	signal clk_dvi : std_logic;
	signal clk_dvi_180 : std_logic;
	
	signal video15_r_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal video15_g_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal video15_b_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	
	signal genlock_r_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal genlock_g_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal genlock_b_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal genlock_hs_s				: std_logic;
	signal genlock_vs_s				: std_logic;
	signal genlock_blank_s			: std_logic;
		
	signal pcm_audio_s				: std_logic_vector(4 downto 0);
	
	signal tdms_r_s					: std_logic_vector( 9 downto 0);
	signal tdms_g_s					: std_logic_vector( 9 downto 0);
	signal tdms_b_s					: std_logic_vector( 9 downto 0);
	signal hdmi_p_s					: std_logic_vector( 3 downto 0);
	signal hdmi_n_s					: std_logic_vector( 3 downto 0);

	signal ena_vidgen_s				: std_logic;
	signal clock_9						: std_logic := '0';
		
	 begin

reset_n <= btn_n_i(3) or btn_n_i(4);
--reset <= power_on_reset or pump_active_s or (not btn_n_i(2));
reset <= pump_active_s or power_on_reset or (not reset_n);

-- Clock 18MHz for xevious core and wm8731, 11MHz for keyboard
clk_11_18 : entity work.pll1
port map(
 inclk0 => clock_50_i,
 c0 => clock_11,
 c1 => clock_18,
 c2 => clk_vga,
 c3 => clk_dvi,
 c4 => clk_dvi_180,
 locked => open --pll_locked
);



-- see README to use FPGA RAM instead of external SRAM
xevious : entity work.xevious
port map(
 clock_18     => clock_18,
 reset        => reset,
-- tv15Khz_mode => tv15Khz_mode,
	ena_vidgen_o => ena_vidgen_s,
 video_r      => r,
 video_g      => g,
 video_b      => b,
 video_csync  => csync,
 video_blankn => blankn,
 video_hs     => video_hsync_n_s,
 video_vs     => video_vsync_n_s,
 audio        => audio,
 
 rom_bus_addr_o => cpu_addr_s,
 rom_bus_do     => rom_data_s,
 
 b_test       => '1',
 b_svce       => '1', 
 coin         => not btn_n_i(3),
 start2       => not btn_n_i(2),
 start1       => not btn_n_i(1),
 up           => (not joy1_s(0)) or joyBCPPFRLDU(0),
 down         => (not joy1_s(1)) or joyBCPPFRLDU(1),
 left         => (not joy1_s(2)) or joyBCPPFRLDU(2),
 right        => (not joy1_s(3)) or joyBCPPFRLDU(3),
 fire         => (not joy1_s(6)) or joyBCPPFRLDU(4),
 bomb         => (not joy1_s(4)) or joyBCPPFRLDU(8)
);



--- Joystick read with sega 6 button support----------------------

	process(vga_hsync_n_s)
		variable state_v : unsigned(7 downto 0) := (others=>'0');
		variable j1_sixbutton_v : std_logic := '0';
		variable j2_sixbutton_v : std_logic := '0';
	begin
		if falling_edge(vga_hsync_n_s) then
		
			state_v := state_v + 1;
			
			case state_v is
				-- joy_s format MXYZ SACB RLDU
			
				when X"00" =>  
					joyP7_s <= '0';
					
				when X"01" =>
					joyP7_s <= '1';

				when X"02" => 
					joy1_s(3 downto 0) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- R, L, D, U
					joy2_s(3 downto 0) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- R, L, D, U
					joy1_s(5 downto 4) <= joy1_p9_i & joy1_p6_i; -- C, B
					joy2_s(5 downto 4) <= joy2_p9_i & joy2_p6_i; -- C, B					
					joyP7_s <= '0';
					j1_sixbutton_v := '0'; -- Assume it's not a six-button controller
					j2_sixbutton_v := '0'; -- Assume it's not a six-button controller

				when X"03" =>
					if joy1_right_i = '0' and joy1_left_i = '0' then -- it's a megadrive controller
								joy1_s(7 downto 6) <= joy1_p9_i & joy1_p6_i; -- Start, A
					else
								joy1_s(7 downto 4) <= '1' & joy1_p9_i & '1'  & joy1_p6_i; -- read A/B as master System
					end if;
							
					if joy2_right_i = '0' and joy2_left_i = '0' then -- it's a megadrive controller
								joy2_s(7 downto 6) <=  joy2_p9_i & joy2_p6_i; -- Start, A
					else
								joy2_s(7 downto 4) <= '1' & joy2_p9_i & '1' & joy2_p6_i; -- read A/B as master System
					end if;
					
										
					joyP7_s <= '1';
			
				when X"04" =>  
					joyP7_s <= '0';

				when X"05" =>
					if joy1_right_i = '0' and joy1_left_i = '0' and joy1_down_i = '0' and joy1_up_i = '0' then 
						j1_sixbutton_v := '1'; --it's a six button
					end if;
					
					if joy2_right_i = '0' and joy2_left_i = '0' and joy2_down_i = '0' and joy2_up_i = '0' then 
						j2_sixbutton_v := '1'; --it's a six button
					end if;
					
					joyP7_s <= '1';
					
				when X"06" =>
					if j1_sixbutton_v = '1' then
						joy1_s(11 downto 8) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- Mode, X, Y e Z
					end if;
					
					if j2_sixbutton_v = '1' then
						joy2_s(11 downto 8) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- Mode, X, Y e Z
					end if;
					
					joyP7_s <= '0';

				when others =>
					joyP7_s <= '1';
					
			end case;

		end if;
	end process;
	
	joyX_p7_o <= joyP7_s;
---------------------------


video_r_s <= r & "0" when blankn = '1' else (others=>'0');
video_g_s <= g & "0" when blankn = '1' else (others=>'0');
video_b_s <= b & "0" when blankn = '1' else (others=>'0');


-- synchro composite/ synchro horizontale
--vga_hsync_n_o <= csync
-- vga_hs <= csync when tv15Khz_mode = '1' else hsync;
-- commutation rapide / synchro verticale
--vga_vsync_n_o <= '1';
-- vga_vs <= '1'   when tv15Khz_mode = '1' else vsync;

sound_string <= "00" & audio & "000" & "00" & audio & "000";

-- pwm sound output

process(clock_18)
begin
  if rising_edge(clock_18) then
    pwm_accumulator  <=  std_logic_vector(unsigned('0' & pwm_accumulator(11 downto 0)) + unsigned('0' & audio));
  end if;
end process;

dac_l_o <= pwm_accumulator(12);
dac_r_o <= pwm_accumulator(12); 

-- get scancode from keyboard
keyboard : entity work.io_ps2_keyboard
port map (
  clk       => clock_11,
  kbd_clk   => ps2_clk_io,
  kbd_dat   => ps2_data_io,
  interrupt => kbd_intr,
  scancode  => kbd_scancode
);

-- translate scancode to joystick
joystick : entity work.kbd_joystick
port map (
  clk         => clock_11,
  kbdint      => kbd_intr,
  kbdscancode => std_logic_vector(kbd_scancode), 
  joyBCPPFRLDU  => joyBCPPFRLDU
);



-- get scancode from keyboard
process (reset, clock_11)
begin
		if rising_edge(clock_11) then
				clk  <= not clk;
		end if;

end process;


OSB_BLOCK: block 

	type config_array is array(natural range 15 downto 0) of std_logic_vector(7 downto 0);

		component osd is
		generic
		(
			STRLEN 		 : integer := 0;
			OSD_X_OFFSET : std_logic_vector(9 downto 0) := (others=>'0');
			OSD_Y_OFFSET : std_logic_vector(9 downto 0) := (others=>'0');
			OSD_COLOR    : std_logic_vector(2 downto 0) := (others=>'0')
		);
		port
		(
			-- OSDs pixel clock, should be synchronous to cores pixel clock to
			-- avoid jitter.
			pclk		: in std_logic;

			-- SPI interface
			sck		: in std_logic;
			ss			: in std_logic;
			sdi		: in std_logic;
			sdo		: out std_logic;

			-- VGA signals coming from core
			red_in 	: in std_logic_vector(4 downto 0);
			green_in : in std_logic_vector(4 downto 0);
			blue_in 	: in std_logic_vector(4 downto 0);
			hs_in		: in std_logic;
			vs_in		: in std_logic;
			
			-- VGA signals going to video connector
			red_out	: out std_logic_vector(4 downto 0);
			green_out: out std_logic_vector(4 downto 0);
			blue_out	: out std_logic_vector(4 downto 0);
			hs_out 	: out std_logic;
			vs_out 	: out std_logic;
			
			-- external data in to the microcontroller
			data_in 	: in std_logic_vector(7 downto 0);
			conf_str : in std_logic_vector( (STRLEN * 8)-1 downto 0);
			
			-- data pump to sram
			pump_active_o	: out std_logic := '0';
			sram_a_o 		: out std_logic_vector(18 downto 0);
			sram_d_o 		: out std_logic_vector(7 downto 0);
			sram_we_n_o 	: out std_logic := '1';
			
			config_buffer_o: out config_array
		);
		end component;
		
		alias SPI_DI  : std_logic is stm_b15_io;
		alias SPI_DO  : std_logic is stm_b14_io;
		alias SPI_SCK : std_logic is stm_b13_io;
		alias SPI_SS3 : std_logic is stm_b12_io;
		
		signal vga_r_out_s : std_logic_vector(3 downto 0);
		signal vga_g_out_s : std_logic_vector(3 downto 0);
		signal vga_b_out_s : std_logic_vector(3 downto 0);
		
		signal sram_addr_s : std_logic_vector(18 downto 0) := (others=>'1');
		signal sram_data_s : std_logic_vector(7 downto 0);
		signal sram_we_s 	 : std_logic := '1';
		
		signal power_on_s		: std_logic_vector(15 downto 0)	:= (others => '1');
		
		signal config_buffer_s : config_array;

		-- config string
		constant STRLEN		: integer := 13;
		constant CONF_STR		: std_logic_vector((STRLEN * 8)-1 downto 0) := to_slv("P,xevious.dat");
		
	begin
		
		
		osd1 : osd 
		generic map
		(
			STRLEN => STRLEN,
			OSD_COLOR => "001", -- RGB
			OSD_X_OFFSET => "0000010010", -- 50
			OSD_Y_OFFSET => "0000001111"  -- 15
		)
		port map
		(
			pclk       => clock_11,

			-- spi for OSD
			sdi        => SPI_DI,
			sck        => SPI_SCK,
			ss         => SPI_SS3,
			sdo        => SPI_DO,
			
			red_in     => video_r_s,
			green_in   => video_g_s,
			blue_in    => video_b_s,
			hs_in      => video_hsync_n_s,
			vs_in      => video_vsync_n_s,

			red_out    => video15_r_s,
			green_out  => video15_g_s,
			blue_out   => video15_b_s,
			hs_out     => vga_hsync_n_s,
			vs_out     => vga_vsync_n_s,

			data_in		=> osd_s,
			conf_str		=> CONF_STR,
						
			pump_active_o	=> pump_active_s,
			sram_a_o			=> sram_addr_s,
			sram_d_o			=> sram_data_s,
			sram_we_n_o		=> sram_we_n_s,
			config_buffer_o=> config_buffer_s		
		);
			

		
		sram_addr_o   <= sram_addr_s when pump_active_s = '1' else "00" & cpu_addr_s;
		sram_data_io  <= sram_data_s when pump_active_s = '1' else (others=>'Z');
		rom_data_s 	   <= sram_data_io;
		sram_oe_n_o   <= '0';
		sram_we_n_o   <= sram_we_n_s;


		--start the microcontroller OSD menu after the power on
		process (clock_11, reset_n, osd_s)
		begin
			if rising_edge(clock_11) then
				if reset_n = '0' then
					power_on_s <= (others=>'1');
				elsif power_on_s /= x"0000" then
					power_on_s <= power_on_s - 1;
					power_on_reset <= '1';
					osd_s <= "00111111";
				else
					power_on_reset <= '0';
					
				end if;
				
				if pump_active_s = '1' and osd_s <= "00111111" then
					osd_s <= "11111111";
				end if;
				
			end if;
		end process;

	end block;	
	
	process (clock_18)
		begin
			if rising_edge(clock_18) then
				clock_9 <= not clock_9;
				
			end if;
		end process;
	
	---------
	-- HDMI
		
	genlock :	entity work.genlock_top 
	generic map
	(
		desloc_x_i => to_signed(0,9), -- 20  -- quando o numero diminui, move o video para a direita
		desloc_y_i => to_signed(0,9)--, -- 0
		--pixel_size_i => 1 --9 --8 fica grande demais
	)
	port map
	(
	
		CLOCK_SYNC	=> clk_dvi,
		CLOCK_VGA	=> clk_vga,
		CLOCK_PIXEL	=> clk_dvi_180,
		
		clock_system=> ena_vidgen_s and clock_18, -- video clock
			
		rgb_15 		=> video15_r_s & video15_g_s & video15_b_s,
		hsync_15 	=> vga_hsync_n_s,
		vsync_15 	=> vga_vsync_n_s,
				
		-- OUTs
		VGA_R			=> genlock_r_s,
		VGA_G			=> genlock_g_s,
		VGA_B			=> genlock_b_s,
		VGA_HS		=> genlock_hs_s,
		VGA_VS		=> genlock_vs_s,
		VGA_BLANK	=> genlock_blank_s,
		
		-- to external SDRAM 
		SDRAM_AD		=> sdram_ad_o,
		SDRAM_DA		=> sdram_da_io,

		SDRAM_BA		=> sdram_ba_o,
		SDRAM_DQM	=> sdram_dqm_o,

		SDRAM_RAS	=> sdram_ras_o,
		SDRAM_CAS	=> sdram_cas_o,
		SDRAM_CKE	=> sdram_cke_o,
		SDRAM_CLK	=> sdram_clk_o,
		SDRAM_CS		=> sdram_cs_o,
		SDRAM_WE		=> sdram_we_o
	
	);
	
	vga_r_o <= genlock_r_s;
	vga_g_o <= genlock_g_s;
	vga_b_o <= genlock_b_s;
	vga_hsync_n_o	<= genlock_hs_s;
	vga_vsync_n_o	<= genlock_vs_s;
	
	


	-- HDMI
	inst_dvid: entity work.hdmi
	generic map 
	(
		FREQ	=> 25200000,-- pixel clock frequency = 25.2MHz
		FS		=> 48000,	-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
		CTS	=> 25200,	-- CTS = Freq(pixclk) * N / (128 * Fs)
		N		=> 6144		-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
	)
	port map
	(
		I_CLK_PIXEL		=> clk_vga,

		I_R				=> genlock_r_s(4 downto 0) & genlock_r_s(4 downto 2), 
		I_G				=> genlock_g_s(4 downto 0) & genlock_g_s(4 downto 2),
		I_B				=> genlock_b_s(4 downto 0) & genlock_b_s(4 downto 2),
		
		I_BLANK			=> genlock_blank_s,
		I_HSYNC			=> genlock_hs_s,
		I_VSYNC			=> genlock_vs_s,
		
		I_AUDIO_ENABLE	=> '1',
		I_AUDIO_PCM_L 	=> "000" & audio & "00",
		I_AUDIO_PCM_R	=> "000" & audio & "00",
		
		-- TMDS parallel pixel synchronous outputs (serialize LSB first)
		O_RED				=> tdms_r_s,
		O_GREEN			=> tdms_g_s,
		O_BLUE			=> tdms_b_s
	);

		hdmi_io: entity work.hdmi_out_altera
		port map (
			clock_pixel_i		=> clk_vga,
			clock_tdms_i		=> clk_dvi,
			red_i					=> tdms_r_s,
			green_i				=> tdms_g_s,
			blue_i				=> tdms_b_s,
			tmds_out_p			=> hdmi_p_s,
			tmds_out_n			=> hdmi_n_s
		);
		
		
		tmds_o(7)	<= hdmi_p_s(2);	-- 2+		
		tmds_o(6)	<= hdmi_n_s(2);	-- 2-		
		tmds_o(5)	<= hdmi_p_s(1);	-- 1+			
		tmds_o(4)	<= hdmi_n_s(1);	-- 1-		
		tmds_o(3)	<= hdmi_p_s(0);	-- 0+		
		tmds_o(2)	<= hdmi_n_s(0);	-- 0-	
		tmds_o(1)	<= hdmi_p_s(3);	-- CLK+	
		tmds_o(0)	<= hdmi_n_s(3);	-- CLK-	



end struct;
